# `ll2l`

The `ll2l` ("Low-Level Tool") project is an experimental framework for static binary analysis.
Binary analysis is a subset of program analysis which focuses on programs in their native (machine/binary) representation.

Currently, `ll2l` provides a set of libaries which are in a work-in-progress state, and is not yet fit for any serious use.

# Requirements

`ll2l` requires OCaml 4.08 or newer.
Additionally, the following OCaml packages are required, available through `opam`:

- `bitstring`
- `core` (>= 0.14.0)
- `dune` (>= 2.7.1)
- `ppx_bitstring`
- `ppx_compare`
- `ppx_hash`
- `ppx_jane`
- `ppx_let`
- `ppx_sexp_conv`
- `stdio` (>= 0.14.0)
- `zarith`

# Instructions

To compile the project, run `dune build` from the root directory.
To install to your local `opam` switch, run `dune install`.

If you run `dune install`, then you can run the top-level program `ll2l`, which provides some functionality for interacting with the various libraries that comprise the project.
