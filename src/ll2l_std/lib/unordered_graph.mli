open Core_kernel

module type S = sig
  include Graph.S

  val hash : t -> int
end

module type Solution = Graph.Solution

(* imperative style graph data structure which
 * is implemented using hash tables/sets
 * 
 * this structure is useful for performance
 * reasons; lookup, insertion, and deletion
 * are best-case O(1) and worst-case O(log(n)) *)

module Make : functor (Vertex : S) (Edge : S) -> sig
  type vertex = Vertex.t

  type edge = Edge.t

  type edges = private (vertex, edge Hash_set.t) Hashtbl.t

  type m = private (vertex, edges) Hashtbl.t

  type t = private {succs: m; preds: m}

  module Graph : module type of Graph.Make (Vertex) (Edge)

  module Path : module type of Graph.Path

  val create : ?size:int -> unit -> t

  val is_empty : t -> bool

  val vertices : t -> vertex list

  val iter_vertices : t -> f:(vertex -> unit) -> unit

  val fold_vertices : t -> init:'acc -> f:('acc -> vertex -> 'acc) -> 'acc

  val has_vertex : t -> vertex -> bool

  val has_succ_edge : t -> vertex -> edge -> bool

  val has_pred_edge : t -> vertex -> edge -> bool

  val has_edge : t -> vertex -> edge -> vertex -> bool

  val has_some_edge : t -> vertex -> vertex -> bool

  val successors : t -> vertex -> (vertex * edge list) list

  val predecessors : t -> vertex -> (vertex * edge list) list

  val has_successors : t -> vertex -> bool

  val has_predecessors : t -> vertex -> bool

  val iter_succs : t -> vertex -> f:(vertex -> edge -> unit) -> unit

  val iter_succs_ignore_edges : t -> vertex -> f:(vertex -> unit) -> unit

  val iter_preds : t -> vertex -> f:(vertex -> edge -> unit) -> unit

  val iter_preds_ignore_edges : t -> vertex -> f:(vertex -> unit) -> unit

  val fold_succs :
    t -> vertex -> init:'acc -> f:('acc -> vertex -> edge -> 'acc) -> 'acc

  val fold_succs_ignore_edges :
    t -> vertex -> init:'acc -> f:('acc -> vertex -> 'acc) -> 'acc

  val fold_preds :
    t -> vertex -> init:'acc -> f:('acc -> vertex -> edge -> 'acc) -> 'acc

  val fold_preds_ignore_edges :
    t -> vertex -> init:'acc -> f:('acc -> vertex -> 'acc) -> 'acc

  val find_successor :
    t -> vertex -> f:(vertex -> edge -> bool) -> (vertex * edge) option

  val find_predecessor :
    t -> vertex -> f:(vertex -> edge -> bool) -> (vertex * edge) option

  val find_vertex : t -> f:(vertex -> bool) -> vertex option

  val insert_vertex : t -> vertex -> unit

  val insert_edge : t -> vertex -> edge -> vertex -> unit

  val insert_edge_symmetric : t -> vertex -> edge -> vertex -> unit

  val remove_vertex : t -> vertex -> unit

  val remove_edge : t -> vertex -> edge -> vertex -> unit

  val remove_edge_symmetric : t -> vertex -> edge -> vertex -> unit

  val filter_preds : t -> vertex -> f:(vertex -> edge -> bool) -> unit

  val filter_succs : t -> vertex -> f:(vertex -> edge -> bool) -> unit

  val filter_vertices : t -> f:(vertex -> bool) -> unit

  val exists_dfs : ?rev:bool -> t -> vertex -> f:(vertex -> bool) -> bool

  val exists_bfs : ?rev:bool -> t -> vertex -> f:(vertex -> bool) -> bool

  val traverse_dfs_pre : ?rev:bool -> t -> vertex -> vertex list

  val traverse_dfs_post : ?rev:bool -> t -> vertex -> vertex list

  val iter_dfs :
       ?rev:bool
    -> t
    -> vertex
    -> f:(vertex -> vertex -> [`Back | `Fwd] -> unit)
    -> unit

  val traverse_bfs : ?rev:bool -> t -> vertex -> vertex list

  val topological_sort : t -> vertex list option

  val is_cyclic : t -> bool

  val make_dag : ?rev:bool -> t -> vertex -> t

  val strongly_connected_components : t -> vertex list list

  val shortest_path :
       ?weight:(vertex -> edge -> vertex -> int)
    -> ?rev:bool
    -> t
    -> vertex
    -> vertex
    -> Path.t option

  type dominators = Graph.dominators

  val dominators : ?compute_df:bool -> t -> vertex -> dominators

  val post_dominators : ?compute_df:bool -> t -> vertex -> dominators

  val to_graph : t -> Graph.t

  val of_graph : Graph.t -> t

  module Fixpoint : Solution

  val fixpoint :
       ?steps:int
    -> ?rev:bool
    -> ?step:(int -> vertex -> 'd -> 'd -> 'd)
    -> init:(vertex, 'd) Fixpoint.t
    -> start:vertex
    -> equal:('d -> 'd -> bool)
    -> merge:('d -> 'd -> 'd)
    -> f:(vertex -> 'd -> 'd)
    -> t
    -> (vertex, 'd) Fixpoint.t
end
