open Core_kernel

type error = [`BadAddr | `BadPerms | `BadAccess | `BadLength]

val string_of_error : error -> string

module Env : sig
  type t

  val empty : t

  val put :
    t -> endian:Endian.t -> addr:Addr.t -> bytes:int -> value:Bitvec.t -> t

  val get :
    t -> endian:Endian.t -> addr:Addr.t -> bytes:int -> Bitvec.t option

  val to_alist : t -> (Addr.t * Bitvec.t) list

  val iter : t -> f:(key:Addr.t -> data:Bitvec.t -> unit) -> unit

  val fold :
    t -> init:'a -> f:(key:Addr.t -> data:Bitvec.t -> 'a -> 'a) -> 'a

  val map : t -> f:(key:Addr.t -> data:Bitvec.t -> Bitvec.t) -> t
end

module Segment : sig
  type t

  val to_bytes : t -> bytes

  val length : t -> int

  val address : t -> Addr.t

  val end_address : t -> Addr.t

  val last_address : t -> Addr.t

  val within : t -> Addr.t -> bool

  val is_readable : t -> bool

  val is_writable : t -> bool

  val is_executable : t -> bool

  val read_byte : t -> Addr.t -> (int, error) Result.t

  val read_bytes : t -> Addr.t -> int -> (bytes, error) Result.t

  val read_int16 : t -> Addr.t -> Endian.t -> (int, error) Result.t

  val read_int32 : t -> Addr.t -> Endian.t -> (int32, error) Result.t

  val read_int64 : t -> Addr.t -> Endian.t -> (int64, error) Result.t

  val write_byte : t -> Addr.t -> int -> (unit, error) Result.t

  val write_bytes : t -> Addr.t -> bytes -> (unit, error) Result.t

  val write_int16 : t -> Addr.t -> int -> Endian.t -> (unit, error) Result.t

  val write_int32 :
    t -> Addr.t -> int32 -> Endian.t -> (unit, error) Result.t

  val write_int64 :
    t -> Addr.t -> int64 -> Endian.t -> (unit, error) Result.t
end

type t

val create : unit -> t

val make_env : t -> Env.t

val num_segments : t -> int

val segments : t -> Segment.t list

val iter : t -> f:(Addr.t -> Segment.t -> unit) -> unit

val fold : t -> init:'a -> f:('a -> Addr.t -> Segment.t -> 'a) -> 'a

val address_ranges : t -> (Addr.t * Addr.t) list

val total_length : t -> int

val find_segment : t -> Addr.t -> Segment.t option

val add_segment :
  t -> Addr.t -> int -> r:bool -> w:bool -> x:bool -> (unit, error) Result.t

val add_segment_from_bigstring :
     t
  -> Addr.t
  -> Bigstring.t
  -> r:bool
  -> w:bool
  -> x:bool
  -> (unit, error) Result.t

val read_byte : t -> Addr.t -> (int, error) Result.t

val read_bytes : t -> Addr.t -> int -> (bytes, error) Result.t

val read_int16 : t -> Addr.t -> Endian.t -> (int, error) Result.t

val read_int32 : t -> Addr.t -> Endian.t -> (int32, error) Result.t

val read_int64 : t -> Addr.t -> Endian.t -> (int64, error) Result.t

val write_byte : t -> Addr.t -> int -> (unit, error) Result.t

val write_bytes : t -> Addr.t -> bytes -> (unit, error) Result.t

val write_int16 : t -> Addr.t -> int -> Endian.t -> (unit, error) Result.t

val write_int32 : t -> Addr.t -> int32 -> Endian.t -> (unit, error) Result.t

val write_int64 : t -> Addr.t -> int64 -> Endian.t -> (unit, error) Result.t
