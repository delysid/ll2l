(* this module is borrowed from BAP's bitvec library *)

open Base

type t

type 'a m

type modulus

external ( mod ) : 'a m -> modulus -> 'a = "%apply"

val modulus : int -> modulus

val m1 : modulus

val m8 : modulus

val m16 : modulus

val m32 : modulus

val m64 : modulus

val compare : t -> t -> int

val signed_compare : t -> t -> modulus -> int

val equal : t -> t -> bool

val hash : t -> int

val sexp_of_t : t -> Sexp.t

val t_of_sexp : Sexp.t -> t

val one : t

val zero : t

val ones : t m

val bool : bool -> t

val int : int -> t m

val int32 : int32 -> t m

val int64 : int64 -> t m

val bigint : t -> t m

val append : int -> int -> t -> t -> t

val extract : hi:int -> lo:int -> t -> t

val setbit : t -> int -> t

val popcount : t -> int

val select : int list -> t -> t

val repeat : int -> times:int -> t -> t

val concat : int -> t list -> t

val succ : t -> t m

val nsucc : t -> int -> t m

val pred : t -> t m

val npred : t -> int -> t m

val neg : t -> t m

val lnot : t -> t m

val msb : t -> bool m

val lsb : t -> bool m

val abs : t -> t m

val add : t -> t -> t m

val sub : t -> t -> t m

val mul : t -> t -> t m

val div : t -> t -> t m

val sdiv : t -> t -> t m

val rem : t -> t -> t m

val srem : t -> t -> t m

val smod : t -> t -> t m

val nth : t -> int -> bool m

val clz : t -> int -> int

val cto : t -> int -> int

val logand : t -> t -> t m

val logor : t -> t -> t m

val logxor : t -> t -> t m

val lshift : t -> t -> t m

val rshift : t -> t -> t m

val arshift : t -> t -> t m

val gcd : t -> t -> t m

val lcm : t -> t -> t m

val gcdext : t -> t -> (t * t * t) m

val to_binary : t -> string

val to_string : t -> string

val of_binary : string -> t

val of_string : string -> t

val fits_int : t -> bool

val fits_int32 : t -> bool

val fits_int64 : t -> bool

val to_int : t -> int

val to_int32 : t -> int32

val to_int64 : t -> int64

val to_bigint : t -> Z.t

module type S = sig
  type nonrec t = t

  type 'a m

  val bool : bool -> t

  val int : int -> t m

  val int32 : int32 -> t m

  val int64 : int64 -> t m

  val bigint : Z.t -> t m

  val to_int : t -> int

  val to_int32 : t -> int32

  val to_int64 : t -> int64

  val zero : t

  val one : t

  val ones : t m

  val succ : t -> t m

  val nsucc : t -> int -> t m

  val pred : t -> t m

  val npred : t -> int -> t m

  val neg : t -> t m

  val lnot : t -> t m

  val abs : t -> t m

  val add : t -> t -> t m

  val sub : t -> t -> t m

  val mul : t -> t -> t m

  val div : t -> t -> t m

  val sdiv : t -> t -> t m

  val rem : t -> t -> t m

  val srem : t -> t -> t m

  val smod : t -> t -> t m

  val nth : t -> int -> bool m

  val msb : t -> bool m

  val lsb : t -> bool m

  val logand : t -> t -> t m

  val logor : t -> t -> t m

  val logxor : t -> t -> t m

  val lshift : t -> t -> t m

  val rshift : t -> t -> t m

  val arshift : t -> t -> t m

  val gcd : t -> t -> t m

  val lcm : t -> t -> t m

  val gcdext : t -> t -> (t * t * t) m

  val compare : t -> t -> int

  val signed_compare : t -> t -> int

  val equal : t -> t -> bool

  val hash : t -> int

  val sexp_of_t : t -> Sexp.t

  val t_of_sexp : Sexp.t -> t

  val ( < ) : t -> t -> bool

  val ( > ) : t -> t -> bool

  val ( <= ) : t -> t -> bool

  val ( >= ) : t -> t -> bool

  val ( <$ ) : t -> t -> bool

  val ( >$ ) : t -> t -> bool

  val ( <=$ ) : t -> t -> bool

  val ( >=$ ) : t -> t -> bool

  val ( = ) : t -> t -> bool

  val ( <> ) : t -> t -> bool

  val ( !$ ) : string -> t

  val ( !! ) : int -> t m

  val ( ~- ) : t -> t m

  val ( ~~ ) : t -> t m

  val ( + ) : t -> t -> t m

  val ( - ) : t -> t -> t m

  val ( * ) : t -> t -> t m

  val ( / ) : t -> t -> t m

  val ( /$ ) : t -> t -> t m

  val ( % ) : t -> t -> t m

  val ( %$ ) : t -> t -> t m

  val ( %^ ) : t -> t -> t m

  val ( land ) : t -> t -> t m

  val ( lor ) : t -> t -> t m

  val ( lxor ) : t -> t -> t m

  val ( lsl ) : t -> t -> t m

  val ( lsr ) : t -> t -> t m

  val ( asr ) : t -> t -> t m

  val ( ++ ) : t -> int -> t m

  val ( -- ) : t -> int -> t m

  type comparator_witness

  val comparator : (t, comparator_witness) Comparator.t
end

module type Modulus = sig
  val modulus : modulus
end

module M64 : Modulus

module Make (M : Modulus) : sig
  include S with type 'a m = 'a
end

module Syntax : sig
  val ( !$ ) : string -> t

  val ( !! ) : int -> t m

  val ( ~- ) : t -> t m

  val ( ~~ ) : t -> t m

  val ( + ) : t -> t -> t m

  val ( - ) : t -> t -> t m

  val ( * ) : t -> t -> t m

  val ( / ) : t -> t -> t m

  val ( /$ ) : t -> t -> t m

  val ( % ) : t -> t -> t m

  val ( %$ ) : t -> t -> t m

  val ( %^ ) : t -> t -> t m

  val ( land ) : t -> t -> t m

  val ( lor ) : t -> t -> t m

  val ( lxor ) : t -> t -> t m

  val ( lsl ) : t -> t -> t m

  val ( lsr ) : t -> t -> t m

  val ( asr ) : t -> t -> t m

  val ( ++ ) : t -> int -> t m

  val ( -- ) : t -> int -> t m

  val ( < ) : t -> t -> bool

  val ( > ) : t -> t -> bool

  val ( <= ) : t -> t -> bool

  val ( >= ) : t -> t -> bool

  val ( = ) : t -> t -> bool

  val ( <> ) : t -> t -> bool
end

include module type of Syntax

val max : t -> t -> t

val min : t -> t -> t

val min_unsigned_value : t

val max_unsigned_value : int -> t

val min_signed_value : int -> t

val max_signed_value : int -> t
