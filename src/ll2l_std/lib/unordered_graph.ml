open Core_kernel

module type S = sig
  include Graph.S

  val hash : t -> int
end

module type Solution = Graph.Solution

module Make (Vertex : S) (Edge : S) = struct
  type vertex = Vertex.t

  type edge = Edge.t

  module Graph = Graph.Make (Vertex) (Edge)
  module Path = Graph.Path

  module V = struct
    include Vertex
    include Comparator.Make (Vertex)
  end

  module E = struct
    include Edge
    include Comparator.Make (Edge)
  end

  type edges = Hash_set.M(E).t Hashtbl.M(V).t

  type m = edges Hashtbl.M(V).t

  type t = {succs: m; preds: m}

  let create ?(size = 0) () =
    { succs= Hashtbl.create (module V) ~size
    ; preds= Hashtbl.create (module V) ~size }

  let is_empty g = Hashtbl.is_empty g.succs

  let vertices g = Hashtbl.keys g.succs

  let iter_vertices g ~f = Hashtbl.iter_keys g.succs ~f

  let fold_vertices g ~init ~f =
    Hashtbl.fold g.succs ~init ~f:(fun ~key ~data acc -> f acc key)

  let has_vertex g v = Hashtbl.mem g.succs v

  let has_succ_edge g v e =
    match Hashtbl.find g.succs v with
    | None -> false
    | Some m -> Hashtbl.exists m ~f:(fun es -> Hash_set.mem es e)

  let has_pred_edge g v e =
    match Hashtbl.find g.preds v with
    | None -> false
    | Some m -> Hashtbl.exists m ~f:(fun es -> Hash_set.mem es e)

  let has_edge g v1 e v2 =
    match Hashtbl.find g.succs v1 with
    | None -> false
    | Some m -> (
      match Hashtbl.find m v2 with
      | Some es -> Hash_set.mem es e
      | None -> false )

  let has_some_edge g v1 v2 =
    match Hashtbl.find g.succs v1 with
    | None -> false
    | Some m -> (
      match Hashtbl.find m v2 with
      | Some es -> not (Hash_set.is_empty es)
      | None -> false )

  let collect m v =
    match Hashtbl.find m v with
    | None -> []
    | Some m' ->
        Hashtbl.to_alist m'
        |> List.map ~f:(fun (v, es) -> (v, Hash_set.to_list es))

  let successors g v = collect g.succs v

  let predecessors g v = collect g.preds v

  let has_successors g v =
    match Hashtbl.find g.succs v with
    | None -> false
    | Some m -> not (Hashtbl.is_empty m)

  let has_predecessors g v =
    match Hashtbl.find g.preds v with
    | None -> false
    | Some m -> not (Hashtbl.is_empty m)

  let iter_succs g v ~f =
    match Hashtbl.find g.succs v with
    | None -> ()
    | Some m' ->
        Hashtbl.iteri m' ~f:(fun ~key ~data -> Hash_set.iter data ~f:(f key))

  let iter_succs_ignore_edges g v ~f =
    match Hashtbl.find g.succs v with
    | None -> ()
    | Some m' -> Hashtbl.iter_keys m' ~f

  let iter_preds g v ~f =
    match Hashtbl.find g.preds v with
    | None -> ()
    | Some m' ->
        Hashtbl.iteri m' ~f:(fun ~key ~data -> Hash_set.iter data ~f:(f key))

  let iter_preds_ignore_edges g v ~f =
    match Hashtbl.find g.preds v with
    | None -> ()
    | Some m' -> Hashtbl.iter_keys m' ~f

  let fold_succs g v ~init ~f =
    match Hashtbl.find g.succs v with
    | None -> init
    | Some m' ->
        Hashtbl.fold m' ~init ~f:(fun ~key ~data acc ->
            Hash_set.fold data ~init:acc ~f:(fun acc e -> f acc key e) )

  let fold_succs_ignore_edges g v ~init ~f =
    match Hashtbl.find g.succs v with
    | None -> init
    | Some m' -> Hashtbl.fold m' ~init ~f:(fun ~key ~data:_ acc -> f acc key)

  let fold_preds g v ~init ~f =
    match Hashtbl.find g.preds v with
    | None -> init
    | Some m' ->
        Hashtbl.fold m' ~init ~f:(fun ~key ~data acc ->
            Hash_set.fold data ~init:acc ~f:(fun acc e -> f acc key e) )

  let fold_preds_ignore_edges g v ~init ~f =
    match Hashtbl.find g.preds v with
    | None -> init
    | Some m' -> Hashtbl.fold m' ~init ~f:(fun ~key ~data:_ acc -> f acc key)

  let find m v f =
    collect m v |> Sequence.of_list
    |> Sequence.map ~f:(fun (v, es) ->
           Sequence.(of_list es |> map ~f:(fun e -> (v, e))) )
    |> Sequence.concat
    |> Sequence.find ~f:(fun (v, e) -> f v e)

  let find_successor g v ~f = find g.succs v f

  let find_predecessor g v ~f = find g.preds v f

  let find_vertex g ~f = vertices g |> List.find ~f

  let insert_vertex' m v =
    if not (Hashtbl.mem m v) then
      Hashtbl.set m ~key:v ~data:(Hashtbl.create (module V))

  let insert_vertex g v = insert_vertex' g.succs v; insert_vertex' g.preds v

  let insert_edge' m v1 e v2 =
    let m' = Hashtbl.find_exn m v1 in
    match Hashtbl.find m' v2 with
    | Some es -> Hash_set.add es e
    | None ->
        let es = Hash_set.create (module E) in
        Hash_set.add es e;
        Hashtbl.set m' ~key:v2 ~data:es

  let insert_edge g v1 e v2 =
    insert_vertex g v1;
    insert_vertex g v2;
    insert_edge' g.succs v1 e v2;
    insert_edge' g.preds v2 e v1

  let insert_edge_symmetric g v1 e v2 =
    insert_edge g v1 e v2; insert_edge g v2 e v1

  let remove_vertex g v =
    let f m = Hashtbl.remove m v in
    f g.succs; f g.preds; Hashtbl.iter g.succs ~f; Hashtbl.iter g.preds ~f

  let remove_edge' m v1 e v2 =
    match Hashtbl.find m v1 with
    | None -> ()
    | Some m' -> (
      match Hashtbl.find m' v2 with
      | None -> ()
      | Some es ->
          Hash_set.remove es e;
          if Hash_set.is_empty es then Hashtbl.remove m' v2 )

  let remove_edge g v1 e v2 =
    remove_edge' g.succs v1 e v2;
    remove_edge' g.preds v2 e v1

  let remove_edge_symmetric g v1 e v2 =
    remove_edge g v1 e v2; remove_edge g v2 e v1

  let filter' m v ~f =
    match Hashtbl.find m v with
    | None -> ()
    | Some m' ->
        Hashtbl.filteri_inplace m' ~f:(fun ~key ~data ->
            Hash_set.filter_inplace data ~f:(f key);
            Hash_set.is_empty data |> not )

  let filter_preds g v ~f = filter' g.preds v ~f

  let filter_succs g v ~f = filter' g.succs v ~f

  let filter_vertices g ~f =
    Hashtbl.fold g.succs ~init:[] ~f:(fun ~key:v ~data:_ removed ->
        if f v then removed else v :: removed )
    |> List.iter ~f:(remove_vertex g)

  let exists_dfs ?(rev = false) g start ~f =
    let s = Hash_set.create (module V) in
    let dir = if rev then predecessors g else successors g in
    let rec aux v =
      if f v then true
      else
        let rec aux2 = function
          | [] -> false
          | (v, _) :: vs -> (
            match Hash_set.strict_add s v with
            | Error _ -> aux2 vs
            | Ok () -> aux v || aux2 vs )
        in
        aux2 (dir v)
    in
    Hash_set.add s start; aux start

  let exists_bfs ?(rev = false) g start ~f =
    let q = Queue.singleton start in
    let s = Hash_set.create (module V) in
    let dir = if rev then predecessors g else successors g in
    let rec aux () =
      match Queue.dequeue q with
      | None -> false
      | Some v ->
          if f v then true
          else
            let next =
              List.map (dir v) ~f:fst
              |> List.filter_map ~f:(fun v ->
                     Hash_set.strict_add s v |> Result.ok
                     |> Option.map ~f:(fun _ -> v) )
            in
            Queue.enqueue_all q next; aux ()
    in
    Hash_set.add s start; aux ()

  let traverse_dfs_pre ?(rev = false) g start =
    let s = Hash_set.create (module V) in
    let dir =
      if rev then fold_preds_ignore_edges g else fold_succs_ignore_edges g
    in
    let rec aux v res =
      let res = v :: res in
      dir v ~init:res ~f:(fun res v ->
          match Hash_set.strict_add s v with
          | Error _ -> res
          | Ok () -> aux v res )
    in
    Hash_set.add s start;
    aux start [] |> List.rev

  let traverse_dfs_post ?(rev = false) g start =
    let dir =
      if rev then fold_preds_ignore_edges g else fold_succs_ignore_edges g
    in
    let s = Hash_set.create (module V) in
    let rec aux v res =
      let res =
        dir v ~init:res ~f:(fun res v ->
            match Hash_set.strict_add s v with
            | Error _ -> res
            | Ok () -> aux v res )
      in
      v :: res
    in
    Hash_set.add s start;
    aux start [] |> List.rev

  let iter_dfs ?(rev = false) g start ~f =
    let s = Hash_set.create (module V) in
    let dir =
      if rev then iter_preds_ignore_edges g else iter_succs_ignore_edges g
    in
    let rec aux v =
      dir v ~f:(fun v' ->
          match Hash_set.strict_add s v' with
          | Error _ -> f v v' `Back
          | Ok () ->
              f v v' `Fwd;
              aux v' )
    in
    Hash_set.add s start;
    aux start |> ignore

  let traverse_bfs ?(rev = false) g start =
    let q = Queue.singleton start in
    let s = Hash_set.create (module V) in
    let dir = if rev then predecessors g else successors g in
    let rec aux res =
      match Queue.dequeue q with
      | None -> res
      | Some v ->
          let next =
            List.map (dir v) ~f:fst
            |> List.filter_map ~f:(fun v ->
                   Hash_set.strict_add s v |> Result.ok
                   |> Option.map ~f:(fun _ -> v) )
          in
          Queue.enqueue_all q next;
          aux (List.rev next @ res)
    in
    Hash_set.add s start;
    aux [start] |> List.rev

  let topological_sort g =
    let open Option.Let_syntax in
    let sp = Hash_set.of_list (module V) (vertices g) in
    let st = Hash_set.create (module V) in
    let rec visit v res =
      if not (Hash_set.mem sp v) then Some res
      else if Hash_set.mem st v then None
      else (
        Hash_set.add st v;
        let rec aux res = function
          | [] -> Some res
          | (v, _) :: vs ->
              let%bind res = visit v res in
              aux res vs
        in
        let%map res = aux res (successors g v) in
        Hash_set.remove st v; Hash_set.remove sp v; v :: res )
    in
    let rec loop res =
      match Hash_set.find sp ~f:(fun _ -> true) with
      | None -> Some res
      | Some v ->
          let%bind res = visit v res in
          loop res
    in
    loop []

  let is_cyclic g = Option.is_none (topological_sort g)

  (* remove back edges based on the reverse post-order *)
  let make_dag ?(rev = false) g start =
    let nodes = traverse_dfs_post g start ~rev |> Array.of_list in
    Array.rev_inplace nodes;
    let rnodes = Hashtbl.create (module V) in
    Array.iteri nodes ~f:(fun i n -> Hashtbl.set rnodes ~key:n ~data:i);
    let dag = create () in
    let dir = if rev then iter_preds g else iter_succs g in
    Array.iter nodes ~f:(fun v ->
        let n = Hashtbl.find_exn rnodes v in
        dir v ~f:(fun v' e ->
            let n' = Hashtbl.find_exn rnodes v' in
            if n' > n then
              if rev then insert_edge dag v' e v else insert_edge dag v e v' ) );
    dag

  (* Tarjan's SCC algorithm *)

  let strongly_connected_components g =
    let c = ref 0 in
    let s = Stack.create () in
    let idx = Hashtbl.create (module V) in
    let lowlink = Hashtbl.create (module V) in
    let onstack = Hash_set.create (module V) in
    let sccs = Vector.create () in
    let rec strong_connect v =
      Hashtbl.set idx v !c;
      Hashtbl.set lowlink v !c;
      incr c;
      Stack.push s v;
      Hash_set.add onstack v;
      iter_succs_ignore_edges g v ~f:(fun v' ->
          match Hashtbl.find idx v' with
          | None ->
              strong_connect v';
              let i = Hashtbl.find_exn lowlink v in
              let i' = Hashtbl.find_exn lowlink v' in
              Hashtbl.set lowlink v (min i i')
          | Some i' when Hash_set.mem onstack v' ->
              let i = Hashtbl.find_exn lowlink v in
              Hashtbl.set lowlink v (min i i')
          | _ -> () );
      if Hashtbl.find_exn lowlink v = Hashtbl.find_exn idx v then
        let rec loop res =
          match Stack.pop s with
          | None -> res
          | Some v' ->
              Hash_set.remove onstack v';
              let res = v' :: res in
              if V.compare v v' = 0 then res else loop res
        in
        let scc = loop [] |> List.dedup_and_sort ~compare:V.compare in
        if not (List.is_empty scc) then Vector.push_back sccs scc
    in
    iter_vertices g ~f:(fun v ->
        if not (Hashtbl.mem idx v) then strong_connect v );
    Vector.rev_inplace sccs;
    Vector.to_list sccs

  (* BAP implementation of Dijkstra's algorithm *)

  let shortest_path ?(weight = fun _ _ _ -> 1) ?(rev = false) g v1 v2 =
    let cmp (n, _, _) (m, _, _) = Int.compare n m in
    let adj = if rev then iter_preds g else iter_succs g in
    let visited = Hash_set.create (module V) in
    let dist = Hashtbl.create (module V) in
    let q = Pairing_heap.create ~cmp () in
    let rec loop () =
      match Pairing_heap.pop q with
      | None -> None
      | Some (w, v, p) when V.compare v v2 = 0 -> Some (Path.create p w)
      | Some (w, v, p) ->
          if not (Hash_set.mem visited v) then update v w p;
          loop ()
    and update v w p =
      Hash_set.add visited v;
      adj v ~f:(fun v' e ->
          let dev = w + weight v e v' in
          match Hashtbl.find dist v' with
          | Some w when w < dev -> ()
          | _ ->
              Hashtbl.set dist v' dev;
              Pairing_heap.add q (dev, v', (v, e, v') :: p) )
    in
    Pairing_heap.add q (0, v1, []);
    Hashtbl.set dist v1 0;
    loop ()

  type dominators = Graph.dominators

  (* A Simple, Fast Dominance Algorithm (Cooper, Harvey, Kennedy [2001]) *)

  let dominators_aux ?(rev = false) ?(compute_df = true) g start =
    let post = traverse_dfs_post g start ~rev |> Array.of_list in
    let dir = if rev then successors g else predecessors g in
    let nums = Hashtbl.create (module V) in
    Array.iteri post ~f:(fun i v -> Hashtbl.set nums v i);
    Array.rev_inplace post;
    let num = Hashtbl.find nums in
    let rec intersect idom v1 v2 =
      match V.compare v1 v2 with
      | 0 -> Some v1
      | _ ->
          let open Option.Let_syntax in
          let%bind n1 = num v1 in
          let%bind n2 = num v2 in
          let rec aux v1 n1 v2 n2 =
            if n1 >= n2 then return (v1, n1)
            else
              let%bind v1' = Hashtbl.find idom v1 in
              let%bind n1' = num v1' in
              aux v1' n1' v2 n2
          in
          let%bind v1, n1 = aux v1 n1 v2 n2 in
          let%bind v2, _ = aux v2 n2 v1 n1 in
          intersect idom v1 v2
    in
    let idom = Hashtbl.create (module V) in
    Hashtbl.set idom start start;
    let rec compute_idom () =
      if
        Array.fold post ~init:false ~f:(fun changed v ->
            if V.compare v start = 0 then changed
            else
              match dir v with
              | [] -> changed
              | vs -> (
                match
                  List.fold vs ~init:None ~f:(fun acc (v, _) ->
                      match Hashtbl.find idom v with
                      | None -> acc
                      | Some _ -> (
                        match acc with
                        | None -> Some v
                        | Some v' -> intersect idom v v' ) )
                with
                | None -> changed
                | Some v' -> (
                  match Hashtbl.find idom v with
                  | Some v'' when V.compare v' v'' = 0 -> changed
                  | _ -> Hashtbl.set idom v v'; true ) ) )
      then compute_idom ()
    in
    compute_idom ();
    Hashtbl.remove idom start;
    let idoms = Hashtbl.create (module V) in
    Array.iter post ~f:(fun v ->
        match Hashtbl.find idom v with
        | None -> ()
        | Some v' ->
            let s =
              match Hashtbl.find idoms v' with
              | None -> Hash_set.create (module V)
              | Some s -> s
            in
            Hash_set.add s v; Hashtbl.set idoms v' s );
    let doms = Hashtbl.create (module V) in
    Array.iter post ~f:(fun v ->
        let rec aux runner =
          ( match Hashtbl.find doms v with
          | None ->
              let s = Hash_set.create (module V) in
              Hash_set.add s runner; Hashtbl.set doms v s
          | Some s -> Hash_set.add s runner );
          match Hashtbl.find idom runner with
          | None -> ()
          | Some runner' -> (
            match V.compare runner runner' with
            | 0 -> ()
            | _ -> aux runner' )
        in
        aux v );
    let df = Hashtbl.create (module V) in
    if compute_df then
      Array.iter post ~f:(fun v ->
          match dir v with
          | [] | [_] -> ()
          | l -> (
            match Hashtbl.find idom v with
            | None -> ()
            | Some v' ->
                List.iter l ~f:(fun (runner, _) ->
                    let rec aux runner =
                      match V.compare runner v' with
                      | 0 -> ()
                      | _ -> (
                          ( match Hashtbl.find df runner with
                          | None ->
                              let s = Hash_set.create (module V) in
                              Hash_set.add s v; Hashtbl.set df runner s
                          | Some s -> Hash_set.add s v );
                          match Hashtbl.find idom runner with
                          | None -> ()
                          | Some runner' -> aux runner' )
                    in
                    aux runner ) ) );
    let inv_doms = Hashtbl.create (module V) in
    Hashtbl.iteri doms ~f:(fun ~key ~data ->
        Hash_set.iter data ~f:(fun v ->
            let s =
              match Hashtbl.find inv_doms v with
              | None -> Hash_set.create (module V)
              | Some s -> s
            in
            Hash_set.add s key; Hashtbl.set inv_doms v s ) );
    object (self)
      method immediate_dominator_of = Hashtbl.find idom

      method immediately_dominated_by v =
        match Hashtbl.find idoms v with
        | None -> []
        | Some s -> Hash_set.to_list s

      method is_immediately_dominating v1 v2 =
        match Hashtbl.find idoms v1 with
        | None -> false
        | Some s -> Hash_set.mem s v2

      method dominators v =
        match Hashtbl.find doms v with
        | None -> []
        | Some s -> Hash_set.to_list s

      method dominating v =
        match Hashtbl.find inv_doms v with
        | None -> []
        | Some s -> Hash_set.to_list s

      method is_dominating v1 v2 =
        match Hashtbl.find doms v2 with
        | None -> false
        | Some s -> Hash_set.mem s v1

      method strict_dominators v =
        match Hashtbl.find doms v with
        | None -> []
        | Some s ->
            Hash_set.to_list s
            |> List.filter ~f:(fun v' -> not (V.compare v' v = 0))

      method strictly_dominating v =
        match Hashtbl.find inv_doms v with
        | None -> []
        | Some s ->
            Hash_set.to_list s
            |> List.filter ~f:(fun v' -> not (V.compare v' v = 0))

      method is_strictly_dominating v1 v2 =
        match V.compare v1 v2 with
        | 0 -> false
        | _ -> self#is_dominating v1 v2

      method dominance_frontier v =
        if not compute_df then failwith "dominance frontier was not computed"
        else
          match Hashtbl.find df v with
          | None -> []
          | Some s -> Hash_set.to_list s

      method is_in_dominance_frontier v1 v2 =
        if not compute_df then failwith "dominance frontier was not computed"
        else
          match Hashtbl.find df v1 with
          | None -> false
          | Some s -> Hash_set.mem s v2
    end

  let dominators ?(compute_df = true) g start =
    dominators_aux g start ~compute_df

  let post_dominators ?(compute_df = true) g start =
    dominators_aux g start ~rev:true ~compute_df

  let to_graph g =
    fold_vertices g ~init:Graph.empty ~f:(fun g' v ->
        let g' =
          fold_succs g v ~init:g' ~f:(fun g' v' e ->
              Graph.insert_edge g' v e v' )
        in
        let g' =
          fold_preds g v ~init:g' ~f:(fun g' v' e ->
              Graph.insert_edge g' v' e v )
        in
        g' )

  let of_graph g =
    let g' = create () in
    Graph.iter_vertices g ~f:(fun v ->
        Graph.iter_succs g v ~f:(fun v' e -> insert_edge g' v e v');
        Graph.iter_preds g v ~f:(fun v' e -> insert_edge g' v' e v) );
    g'

  (* BAP implementation of Kildall's algorithm,
   * except the user must provide a starting node *)

  module Fixpoint = struct
    type ('n, 'd) t =
      | Solution :
          { steps: int option
          ; iters: int
          ; default: 'd
          ; approx: ('n, 'd, _) Map.t }
          -> ('n, 'd) t

    (* initial approximation of a solution where the `default`
     * parameter defines the default value of all variables
     * unspecified in the initial constraints *)
    let create constraints default =
      Solution {steps= Some 0; iters= 0; default; approx= constraints}

    (* total number of iterations made to obtain the solution *)
    let iterations (Solution {iters}) = iters

    (* default value assigned to all variables not in the
     * map of approximations (usually a 'top' or 'bottom' value) *)
    let default (Solution {default}) = default

    (* approximate value of `n` *)
    let get (Solution {approx; default}) n =
      match Map.find approx n with
      | None -> default
      | Some x -> x

    (* enumerates all non-trivial values in the solution *)
    let enum (Solution {approx}) = Map.to_sequence approx

    let is_subset ~equal (Solution {approx= m1}) ~of_:s2 =
      Map.for_alli m1 ~f:(fun ~key ~data -> equal (get s2 key) data)

    let equal ~equal (Solution {approx= m1; default= d1} as s1)
        (Solution {approx= m2; default= d2} as s2) =
      equal d1 d2
      && Map.length m1 = Map.length m2
      && is_subset ~equal s1 ~of_:s2
      && is_subset ~equal s2 ~of_:s1

    let is_fixpoint (Solution {steps; iters}) =
      match steps with
      | None -> iters > 0
      | Some steps -> iters < steps

    (* map a solution `s` to a new solution `s'` according to `f` *)
    let derive (Solution s) ~f default =
      let f ~key ~data = f key data in
      create (Map.filter_mapi ~f s.approx) default

    type ('a, 'b) step = Step of 'a | Done of 'b

    let continue x = Step x

    (* `steps` is an upper bound on the number of iterations
     * `rev` will determine whether the graph is traversed in reverse order
     * `step` is called every time a new value of a variable is obtained
     * `init` is the initial approximation of a solution
     * `start` is the starting node in the graph
     * `equal` compares two approximations for equivalence
     * `merge` is called when two paths merge (usually 'join' or 'meet')
     * `f` is the transfer function
     * `g` is the graph to operate over *)
    let compute (type d) ?steps ?(rev = false) ?step
        ~init:(Solution {approx= init; iters; default}) ~start ~equal ~merge
        ~f g : (vertex, d) t =
      let nodes = traverse_dfs_post g start ~rev |> Array.of_list in
      let dir = if rev then predecessors g else successors g in
      Array.rev_inplace nodes;
      let rnodes = Hashtbl.create (module V) in
      Array.iteri nodes ~f:(fun i n -> Hashtbl.set rnodes ~key:n ~data:i);
      let succs =
        Array.map nodes ~f:(fun n ->
            let ns = Hash_set.create (module Int) in
            dir n |> Sequence.of_list |> Sequence.map ~f:fst
            |> Sequence.iter ~f:(fun n ->
                   match Hashtbl.find rnodes n with
                   | None -> ()
                   | Some i -> Hash_set.add ns i );
            ns )
      in
      let user_step =
        match step with
        | None -> fun visits _ _ x -> (visits, x)
        | Some step ->
            fun visits n x x' ->
              let i =
                match Map.find visits n with
                | None -> 1
                | Some x -> x + 1
              in
              let visits = Map.set visits ~key:n ~data:i in
              (visits, step i nodes.(n) x x')
      in
      let get approx n : d =
        match Map.find approx n with
        | Some x -> x
        | None -> default
      in
      let step visits works approx =
        match Set.min_elt works with
        | None -> Done approx
        | Some n ->
            let works = Set.remove works n in
            let out = f nodes.(n) (get approx n) in
            succs.(n)
            |> Hash_set.fold ~init:(visits, works, approx)
                 ~f:(fun (visits, works, approx) n ->
                   let ap = get approx n in
                   let ap' = merge out ap in
                   let visits, ap' = user_step visits n ap ap' in
                   if equal ap ap' then (visits, works, approx)
                   else
                     ( visits
                     , Set.add works n
                     , Map.set approx ~key:n ~data:ap' ) )
            |> continue
      in
      let can_iter iters =
        match steps with
        | None -> true
        | Some steps -> iters < steps
      in
      let make_solution iters approx =
        Solution
          { steps
          ; iters
          ; default
          ; approx=
              Map.fold approx ~init ~f:(fun ~key:n ~data approx ->
                  Map.set approx ~key:nodes.(n) ~data ) }
      in
      let rec loop visits iters works approx =
        if can_iter iters then
          match step visits works approx with
          | Done approx -> make_solution iters approx
          | Step (visits, works, approx) ->
              loop visits (iters + 1) works approx
        else make_solution iters approx
      in
      let works = List.init (Array.length nodes) ident in
      let approx =
        Map.fold init ~init:Int.Map.empty ~f:(fun ~key:node ~data approx ->
            match Hashtbl.find rnodes node with
            | None -> approx
            | Some n -> Map.set approx ~key:n ~data )
      in
      loop Int.Map.empty iters (Int.Set.of_list works) approx
  end

  let fixpoint = Fixpoint.compute
end
