(* this module is borrowed + extended from BAP *)

open Core_kernel

type 'a t [@@deriving sexp]

val create : ?capacity:int -> unit -> 'a t

val copy : 'a t -> 'a t

val clear : 'a t -> unit

val capacity : 'a t -> int

val append : 'a t -> 'a t -> unit

val push_back : 'a t -> 'a -> unit

val insert : 'a t -> int -> 'a -> unit option

val insert_exn : 'a t -> int -> 'a -> unit

val remove : 'a t -> int -> unit option

val remove_exn : 'a t -> int -> unit

val cut_back : 'a t -> int -> unit

val pop_back : 'a t -> unit option

val pop_back_exn : 'a t -> unit

val get : 'a t -> int -> 'a option

val get_exn : 'a t -> int -> 'a

val back : 'a t -> 'a option

val back_exn : 'a t -> 'a

val set : 'a t -> int -> 'a -> unit option

val set_exn : 'a t -> int -> 'a -> unit

val set_back : 'a t -> 'a -> unit option

val set_back_exn : 'a t -> 'a -> unit

val findi : 'a t -> f:(int -> 'a -> bool) -> (int * 'a) option

val iteri : 'a t -> f:(int -> 'a -> unit) -> unit

val foldi : 'a t -> init:'b -> f:(int -> 'b -> 'a -> 'b) -> 'b

val index : ?equal:('a -> 'a -> bool) -> 'a t -> 'a -> int option

val index_exn : ?equal:('a -> 'a -> bool) -> 'a t -> 'a -> int

val index_with :
  ?equal:('a -> 'a -> bool) -> default:int -> 'a t -> 'a -> int

include Container.S1 with type 'a t := 'a t

val of_array : 'a array -> 'a t

val to_array : 'a t -> 'a array

val map_to_array : 'a t -> f:('a -> 'b) -> 'b array

val of_list : 'a list -> 'a t

val to_list : 'a t -> 'a list

val filteri : 'a t -> f:(int -> 'a -> bool) -> 'a t

val filter : 'a t -> f:('a -> bool) -> 'a t

val filteri_inplace : 'a t -> f:(int -> 'a -> bool) -> unit

val filter_inplace : 'a t -> f:('a -> bool) -> unit

val filter_mapi : 'a t -> f:(int -> 'a -> 'b option) -> 'b t

val filter_map : 'a t -> f:('a -> 'b option) -> 'b t

val swap : 'a t -> int -> int -> unit option

val swap_exn : 'a t -> int -> int -> unit

val rev_inplace : 'a t -> unit

val rev : 'a t -> 'a t

val sort : ?pos:int -> ?len:int -> 'a t -> compare:('a -> 'a -> int) -> unit
