(* this module is borrowed from BAP's bitvec library *)

open Base

type t = Z.t

type modulus = {m: Z.t} [@@unboxed]

type 'a m = modulus -> 'a

external ( mod ) : 'a m -> modulus -> 'a = "%apply"

let norm {m} x =
  if Z.sign x < 0 || Z.geq x m then Z.(x land m) else x
  [@@inline]

let modulus w = {m= Z.((one lsl w) - one)}

let m1 = modulus 1

let m8 = modulus 8

let m16 = modulus 16

let m32 = modulus 32

let m64 = modulus 64

let compare x y = if phys_equal x y then 0 else Z.compare x y [@@inline]

let equal x y = compare x y = 0 [@@inline]

let hash x = Z.hash x [@@inline]

let one = Z.one

let zero = Z.zero

let ones m = norm m @@ Z.minus_one

let bool x = if x then one else zero

let int x m = norm m @@ Z.of_int x [@@inline]

let int32 x m = norm m @@ Z.of_int32 x [@@inline]

let int64 x m = norm m @@ Z.of_int64 x [@@inline]

let bigint x m = norm m @@ x [@@inline]

let append w1 w2 x y =
  let w = w1 + w2 in
  let ymask = Z.((one lsl w2) - one) in
  let zmask = Z.((one lsl w) - one) in
  let ypart = Z.(y land ymask) in
  let xpart = Z.(x lsl w2) in
  Z.(xpart lor ypart land zmask)
  [@@inline]

let extract ~hi ~lo x = Z.extract x lo (hi - lo + 1) [@@inline]

let setbit x n = Z.(x lor (one lsl n)) [@@inline]

let popcount x = Z.popcount x [@@inline]

let select bits x =
  let rec aux n y = function
    | [] -> y
    | v :: vs ->
        let y = if Z.testbit x v then setbit y n else y in
        aux (n + 1) y vs
  in
  aux 0 zero bits

let repeat m ~times:n x =
  let mask = Z.((one lsl m) - one) in
  let x = Z.(x land mask) in
  let rec aux i y =
    if i >= n then y
    else
      let off = i * m in
      aux (i + 1) Z.(y lor (x lsl off))
  in
  aux 0 zero

let concat m xs =
  let mask = Z.((one lsl m) - one) in
  List.fold_left xs ~init:zero ~f:(fun y x ->
      Z.((y lsl m) lor (x land mask)))

let succ x m = norm m @@ Z.succ x [@@inline]

let nsucc x n m = norm m @@ Z.(x + of_int n) [@@inline]

let pred x m = norm m @@ Z.pred x [@@inline]

let npred x n m = norm m @@ Z.(x - of_int n) [@@inline]

let neg x m = norm m @@ Z.neg x [@@inline]

let lnot x m = norm m @@ Z.lognot x [@@inline]

let msb x m = Z.(equal m.m (norm m x lor (m.m asr 1))) [@@inline]

let lsb x _ = Z.is_odd x [@@inline]

let abs x m = if msb x m then neg x m else x [@@inline]

let add x y m = norm m @@ Z.add x y [@@inline]

let sub x y m = norm m @@ Z.sub x y [@@inline]

let mul x y m = norm m @@ Z.mul x y [@@inline]

let div x y m =
  if Z.(equal y zero) then ones m else norm m @@ Z.div x y
  [@@inline]

let sdiv x y m =
  match (msb x m, msb y m) with
  | false, false -> div x y m
  | true, false -> neg (div (neg x m) y m) m
  | false, true -> neg (div x (neg y m) m) m
  | true, true -> div (neg x m) (neg y m) m
  [@@inline]

let rem x y m =
  if Z.(equal y zero) then x else norm m @@ Z.rem x y
  [@@inline]

let srem x y m =
  match (msb x m, msb y m) with
  | false, false -> rem x y m
  | true, false -> neg (rem (neg x m) y m) m
  | false, true -> neg (rem x (neg y m) m) m
  | true, true -> neg (rem (neg x m) (neg y m) m) m
  [@@inline]

let smod s t m =
  let u = rem s t m in
  if Z.(equal u zero) then u
  else
    match (msb s m, msb t m) with
    | false, false -> u
    | true, false -> add (neg u m) t m
    | false, true -> add u t m
    | true, true -> neg u m
  [@@inline]

let nth x n _ = Z.testbit x n [@@inline]

let clz x n =
  let rec aux i =
    if i < 0 then n else if Z.testbit x i then n - i - 1 else aux (i - 1)
  in
  aux (n - 1)

let cto x n =
  let rec aux i =
    if i >= n then n else if not (Z.testbit x i) then i else aux (i + 1)
  in
  aux 0

let logand x y m = norm m @@ Z.logand x y [@@inline]

let logor x y m = norm m @@ Z.logor x y [@@inline]

let logxor x y m = norm m @@ Z.logxor x y [@@inline]

let to_int_fast x m =
  if Caml.Obj.is_int (Caml.Obj.repr x) then Caml.Obj.magic x
  else Z.to_int @@ Z.signed_extract x 0 (min m Sys.int_size_in_bits)
  [@@inline]

let shift n {m} ~overshift ~in_bounds =
  let w = Z.numbits m in
  if Z.(lt n (of_int w)) then in_bounds w (to_int_fast n w) else overshift
  [@@inline]

let lshift x n m =
  shift n m ~overshift:zero ~in_bounds:(fun _ n ->
      norm m @@ Z.shift_left x n)
  [@@inline]

let rshift x n m =
  shift n m ~overshift:zero ~in_bounds:(fun _ n ->
      norm m @@ Z.shift_right x n)
  [@@inline]

let arshift x n m =
  let msb = msb x m in
  shift n m
    ~overshift:(if msb then ones m else zero)
    ~in_bounds:(fun w n ->
      let x = Z.(x asr n) in
      norm m
      @@
      if msb then
        let n = w - n in
        let y = ones mod m in
        Z.((y lsl n) lor x)
      else x)
  [@@inline]

let gcd x y m =
  if Z.(equal x zero) then y
  else if Z.(equal y zero) then x
  else norm m @@ Z.gcd x y
  [@@inline]

let lcm x y m =
  if Z.(equal x zero) || Z.(equal y zero) then zero else norm m @@ Z.lcm x y
  [@@inline]

let gcdext x y m =
  if Z.(equal x zero) then (x, zero, one)
  else if Z.(equal y zero) then (x, one, zero)
  else
    let g, a, b = Z.gcdext x y in
    (norm m g, norm m a, norm m b)
  [@@inline]

let to_binary = Z.to_bits

let to_string = Z.format "%X"

let of_binary = Z.of_bits

let of_string x =
  let r = Z.of_string x in
  if Z.sign r < 0 then
    invalid_arg (x ^ " - invalid bitv string, sign is not expected")
  else r

let sexp_of_t x = Sexp.Atom (to_string x)

let t_of_sexp = function
  | Sexp.Atom x -> of_string x
  | _ -> invalid_arg "expected atom"

let signed_compare x y m =
  match (msb x m, msb y m) with
  | true, true -> compare y x
  | false, false -> compare x y
  | true, false -> -1
  | false, true -> 1
  [@@inline]

let fits_int = Z.fits_int

let fits_int32 = Z.fits_int32

let fits_int64 = Z.fits_int64

let max_uint = Z.((one lsl Sys.int_size_in_bits) - one)

let max_sint = Z.of_int Int.max_value

let max_uint32 = Z.((one lsl 32) - one)

let max_sint32 = Z.((one lsl 31) - one)

let max_uint64 = Z.((one lsl 64) - one)

let max_sint64 = Z.((one lsl 63) - one)

let convert tname size convert max_signed max_unsigned x =
  if Z.(leq x max_signed) then convert x
  else if Z.(leq x max_unsigned) then convert (Z.signed_extract x 0 size)
  else failwith (to_string x ^ " doesn't fit the " ^ tname ^ " type")

let to_int x =
  convert "int" Sys.int_size_in_bits Z.to_int max_sint max_uint x
  [@@inline]

let to_int32 x =
  convert "int32" 32 Z.to_int32 max_sint32 max_uint32 x
  [@@inline]

let to_int64 x =
  convert "int64" 64 Z.to_int64 max_sint64 max_uint64 x
  [@@inline]

let to_bigint x = x [@@inline]

module type S = sig
  type nonrec t = t

  type 'a m

  val bool : bool -> t

  val int : int -> t m

  val int32 : int32 -> t m

  val int64 : int64 -> t m

  val bigint : Z.t -> t m

  val to_int : t -> int

  val to_int32 : t -> int32

  val to_int64 : t -> int64

  val zero : t

  val one : t

  val ones : t m

  val succ : t -> t m

  val nsucc : t -> int -> t m

  val pred : t -> t m

  val npred : t -> int -> t m

  val neg : t -> t m

  val lnot : t -> t m

  val abs : t -> t m

  val add : t -> t -> t m

  val sub : t -> t -> t m

  val mul : t -> t -> t m

  val div : t -> t -> t m

  val sdiv : t -> t -> t m

  val rem : t -> t -> t m

  val srem : t -> t -> t m

  val smod : t -> t -> t m

  val nth : t -> int -> bool m

  val msb : t -> bool m

  val lsb : t -> bool m

  val logand : t -> t -> t m

  val logor : t -> t -> t m

  val logxor : t -> t -> t m

  val lshift : t -> t -> t m

  val rshift : t -> t -> t m

  val arshift : t -> t -> t m

  val gcd : t -> t -> t m

  val lcm : t -> t -> t m

  val gcdext : t -> t -> (t * t * t) m

  val compare : t -> t -> int

  val signed_compare : t -> t -> int

  val equal : t -> t -> bool

  val hash : t -> int

  val sexp_of_t : t -> Sexp.t

  val t_of_sexp : Sexp.t -> t

  val ( < ) : t -> t -> bool

  val ( > ) : t -> t -> bool

  val ( <= ) : t -> t -> bool

  val ( >= ) : t -> t -> bool

  val ( <$ ) : t -> t -> bool

  val ( >$ ) : t -> t -> bool

  val ( <=$ ) : t -> t -> bool

  val ( >=$ ) : t -> t -> bool

  val ( = ) : t -> t -> bool

  val ( <> ) : t -> t -> bool

  val ( !$ ) : string -> t

  val ( !! ) : int -> t m

  val ( ~- ) : t -> t m

  val ( ~~ ) : t -> t m

  val ( + ) : t -> t -> t m

  val ( - ) : t -> t -> t m

  val ( * ) : t -> t -> t m

  val ( / ) : t -> t -> t m

  val ( /$ ) : t -> t -> t m

  val ( % ) : t -> t -> t m

  val ( %$ ) : t -> t -> t m

  val ( %^ ) : t -> t -> t m

  val ( land ) : t -> t -> t m

  val ( lor ) : t -> t -> t m

  val ( lxor ) : t -> t -> t m

  val ( lsl ) : t -> t -> t m

  val ( lsr ) : t -> t -> t m

  val ( asr ) : t -> t -> t m

  val ( ++ ) : t -> int -> t m

  val ( -- ) : t -> int -> t m

  type comparator_witness

  (* note, this will be an UNSIGNED comparison,
   * since normed bitvectors in Z do not directly
   * map to two's complement form *)
  val comparator : (t, comparator_witness) Comparator.t
end

module type Modulus = sig
  val modulus : modulus
end

module M64 = struct
  let modulus = m64
end

module Make (M : Modulus) : S with type 'a m = 'a = struct
  type 'a m = 'a

  let m = M.modulus

  let bool x = bool x [@@inline]

  let int x = int x mod m [@@inline]

  let int32 x = int32 x mod m [@@inline]

  let int64 x = int64 x mod m [@@inline]

  let bigint x = bigint x mod m [@@inline]

  let to_int = to_int

  let to_int32 = to_int32

  let to_int64 = to_int64

  let zero = zero

  let one = one

  let ones = ones mod m

  let succ x = succ x mod m [@@inline]

  let nsucc x n = nsucc x n mod m [@@inline]

  let pred x = pred x mod m [@@inline]

  let npred x n = npred x n mod m [@@inline]

  let neg x = neg x mod m [@@inline]

  let lnot x = lnot x mod m [@@inline]

  let abs x = abs x mod m [@@inline]

  let add x y = add x y mod m [@@inline]

  let sub x y = sub x y mod m [@@inline]

  let mul x y = mul x y mod m [@@inline]

  let div x y = div x y mod m [@@inline]

  let sdiv x y = sdiv x y mod m [@@inline]

  let rem x y = rem x y mod m [@@inline]

  let srem x y = srem x y mod m [@@inline]

  let smod x y = smod x y mod m [@@inline]

  let nth x y = nth x y mod m [@@inline]

  let msb x = msb x mod m [@@inline]

  let lsb x = lsb x mod m [@@inline]

  let logand x y = logand x y mod m [@@inline]

  let logor x y = logor x y mod m [@@inline]

  let logxor x y = logxor x y mod m [@@inline]

  let lshift x y = lshift x y mod m [@@inline]

  let rshift x y = rshift x y mod m [@@inline]

  let arshift x y = arshift x y mod m [@@inline]

  let gcd x y = gcd x y mod m [@@inline]

  let lcm x y = lcm x y mod m [@@inline]

  let gcdext x y = gcdext x y mod m [@@inline]

  let ( !$ ) x = of_string x [@@inline]

  let ( !! ) x = int x [@@inline]

  let ( ~- ) x = neg x [@@inline]

  let ( ~~ ) x = lnot x [@@inline]

  let ( + ) x y = add x y [@@inline]

  let ( - ) x y = sub x y [@@inline]

  let ( * ) x y = mul x y [@@inline]

  let ( / ) x y = div x y [@@inline]

  let ( /$ ) x y = sdiv x y [@@inline]

  let ( % ) x y = rem x y [@@inline]

  let ( %$ ) x y = smod x y [@@inline]

  let ( %^ ) x y = srem x y [@@inline]

  let ( land ) x y = logand x y [@@inline]

  let ( lor ) x y = logor x y [@@inline]

  let ( lxor ) x y = logxor x y [@@inline]

  let ( lsl ) x y = lshift x y [@@inline]

  let ( lsr ) x y = rshift x y [@@inline]

  let ( asr ) x y = arshift x y [@@inline]

  let ( ++ ) x n = nsucc x n [@@inline]

  let ( -- ) x n = npred x n [@@inline]

  module T = struct
    type nonrec t = t

    let compare = compare

    let signed_compare x y = signed_compare x y m

    let equal = equal

    let hash = hash

    let sexp_of_t = sexp_of_t

    let t_of_sexp = t_of_sexp

    let ( < ) x y = compare x y < 0

    let ( > ) x y = compare x y > 0

    let ( <= ) x y = compare x y <= 0

    let ( >= ) x y = compare x y >= 0

    let ( <$ ) x y = Int.(signed_compare x y < 0)

    let ( >$ ) x y = Int.(signed_compare x y > 0)

    let ( <=$ ) x y = Int.(signed_compare x y <= 0)

    let ( >=$ ) x y = Int.(signed_compare x y >= 0)

    let ( = ) x y = compare x y = 0

    let ( <> ) x y = compare x y <> 0
  end

  include T
  include Comparable.Make (T)
end
[@@inline]

module Syntax = struct
  let ( !$ ) x = of_string x [@@inline]

  let ( !! ) x m = int x m [@@inline]

  let ( ~- ) x m = neg x m [@@inline]

  let ( ~~ ) x m = lnot x m [@@inline]

  let ( + ) x y m = add x y m [@@inline]

  let ( - ) x y m = sub x y m [@@inline]

  let ( * ) x y m = mul x y m [@@inline]

  let ( / ) x y m = div x y m [@@inline]

  let ( /$ ) x y m = sdiv x y m [@@inline]

  let ( % ) x y m = rem x y m [@@inline]

  let ( %$ ) x y m = smod x y m [@@inline]

  let ( %^ ) x y m = srem x y m [@@inline]

  let ( land ) x y m = logand x y m [@@inline]

  let ( lor ) x y m = logor x y m [@@inline]

  let ( lxor ) x y m = logxor x y m [@@inline]

  let ( lsl ) x y m = lshift x y m [@@inline]

  let ( lsr ) x y m = rshift x y m [@@inline]

  let ( asr ) x y m = arshift x y m [@@inline]

  let ( ++ ) x n m = nsucc x n m [@@inline]

  let ( -- ) x n m = npred x n m [@@inline]

  let ( < ) x y = compare x y < 0 [@@inline]

  let ( > ) x y = compare x y > 0 [@@inline]

  let ( <= ) x y = compare x y <= 0 [@@inline]

  let ( >= ) x y = compare x y >= 0 [@@inline]

  let ( = ) x y = compare x y = 0 [@@inline]

  let ( <> ) x y = compare x y <> 0 [@@inline]
end

include Syntax

let max x y = if x > y then x else y [@@inline]

let min x y = if x < y then x else y [@@inline]

let min_unsigned_value = zero

let max_unsigned_value size = ones mod modulus size [@@inline]

let min_signed_value size =
  let m = modulus size in
  (one lsl (int Int.(size - 1) mod m)) mod m

let max_signed_value size =
  let m = modulus size in
  pred ((one lsl (int Int.(size - 1) mod m)) mod m) mod m
