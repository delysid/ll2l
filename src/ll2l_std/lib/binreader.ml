open Core_kernel

type t = {data: Bigstring.t; len: int; mutable pos: int}

let from_bigstring data =
  {data; len= Bigstring.length data; pos= 0}
  [@@inline]

let from_bytes bytes = from_bigstring (Bigstring.of_bytes bytes) [@@inline]

let position t = t.pos [@@inline]

let seek t p = if p < 0 || p >= t.len then None else Some (t.pos <- p)

let skip t n = if t.pos + n >= t.len then None else Some (t.pos <- t.pos + n)

let peek8 t =
  Option.try_with (fun () -> Bigstring.get_uint8 t.data ~pos:t.pos)
  [@@inline]

let read8 t =
  let open Option.Let_syntax in
  let%map b = peek8 t in
  t.pos <- t.pos + 1;
  b

let peek16 t endian =
  Option.try_with (fun () ->
      match endian with
      | `LE -> Bigstring.get_uint16_le t.data ~pos:t.pos
      | `BE -> Bigstring.get_uint16_be t.data ~pos:t.pos)
  [@@inline]

let read16 t endian =
  let open Option.Let_syntax in
  let%map b = peek16 t endian in
  t.pos <- t.pos + 2;
  b

let peek32 t endian =
  Option.try_with (fun () ->
      match endian with
      | `LE -> Bigstring.get_int32_t_le t.data ~pos:t.pos
      | `BE -> Bigstring.get_int32_t_be t.data ~pos:t.pos)
  [@@inline]

let read32 t endian =
  let open Option.Let_syntax in
  let%map b = peek32 t endian in
  t.pos <- t.pos + 4;
  b

let peek64 t endian =
  Option.try_with (fun () ->
      match endian with
      | `LE -> Bigstring.get_int64_t_le t.data ~pos:t.pos
      | `BE -> Bigstring.get_int64_t_be t.data ~pos:t.pos)
  [@@inline]

let read64 t endian =
  let open Option.Let_syntax in
  let%map b = peek64 t endian in
  t.pos <- t.pos + 8;
  b

let peek_bytes t n =
  Option.try_with (fun () -> Bigstring.to_bytes t.data ~pos:t.pos ~len:n)
  [@@inline]

let read_bytes t n =
  let open Option.Let_syntax in
  let%map b = peek_bytes t n in
  t.pos <- t.pos + n;
  b

let peek_string ?(trunc = false) ?(size = None) t =
  let open Option.Let_syntax in
  match size with
  | Some n ->
      let%map b = peek_bytes t n in
      let s = Bytes.to_string b in
      if trunc then
        match String.index s '\x00' with
        | None -> s
        | Some i -> String.subo s ~len:i
      else s
  | None ->
      let pos' = t.pos in
      let rec loop res =
        let%bind b = read8 t in
        let%bind b = Char.of_int b in
        if Char.(equal b '\x00') then return (List.rev res)
        else loop (b :: res)
      in
      let result =
        match loop [] with
        | None -> None
        | Some chars -> return Bytes.(of_char_list chars |> to_string)
      in
      t.pos <- pos';
      result

let read_string ?(trunc = false) ?(size = None) t =
  let open Option.Let_syntax in
  let%map s = peek_string t ~trunc ~size in
  let n =
    match size with
    | Some n -> if trunc then min (String.length s + 1) n else n
    | None -> String.length s + 1
  in
  t.pos <- t.pos + n;
  s

let peek_uleb128 t =
  let open Option.Let_syntax in
  let pos' = t.pos in
  let rec loop v s i =
    let%bind b = read8 t in
    let slice = Int64.(of_int b land 0x7FL) in
    if s >= 64 || Int64.((slice lsl s) lsr s <> slice) then return (0L, i)
    else
      let v = Int64.(v + (slice lsl s)) in
      let s = s + 7 in
      if b < 128 then return (v, i + 1) else loop v s (i + 1)
  in
  let result = loop 0L 0 0 in
  t.pos <- pos';
  result

let read_uleb128 t =
  let open Option.Let_syntax in
  let%map b, n = peek_uleb128 t in
  t.pos <- t.pos + n;
  b

let peek_sleb128 t =
  let open Option.Let_syntax in
  let pos' = t.pos in
  let rec loop v s i =
    let%bind b = read8 t in
    let v = Int64.(((of_int b land 0x7FL) lsl s) lor v) in
    let s = s + 7 in
    if b < 128 then return (v, s, i + 1, b) else loop v s (i + 1)
  in
  let result =
    match loop 0L 0 0 with
    | None -> None
    | Some (v, s, i, b) ->
        let v =
          if s < 64 && b land 0x40 <> 0 then Int64.((-1L lsl s) lor v) else v
        in
        Some (v, i)
  in
  t.pos <- pos';
  result

let read_sleb128 t =
  let open Option.Let_syntax in
  let%map b, n = peek_sleb128 t in
  t.pos <- t.pos + n;
  b
