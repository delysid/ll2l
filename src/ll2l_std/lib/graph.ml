open Core_kernel

module type S = sig
  type t

  val compare : t -> t -> int

  val sexp_of_t : t -> Sexp.t

  val t_of_sexp : Sexp.t -> t
end

module type Solution = sig
  type ('n, 'd) t

  val create : ('n, 'd, _) Map.t -> 'd -> ('n, 'd) t

  val equal : equal:('d -> 'd -> bool) -> ('n, 'd) t -> ('n, 'd) t -> bool

  val iterations : ('n, 'd) t -> int

  val enum : ('n, 'd) t -> ('n * 'd) Sequence.t

  val default : ('n, 'd) t -> 'd

  val is_fixpoint : ('n, 'd) t -> bool

  val get : ('n, 'd) t -> 'n -> 'd

  val derive : ('n, 'd) t -> f:('n -> 'd -> 'a option) -> 'a -> ('n, 'a) t
end

module Make (Vertex : S) (Edge : S) = struct
  type vertex = Vertex.t

  type edge = Edge.t

  module Path = struct
    type nonrec edge = Vertex.t * Edge.t * Vertex.t
    [@@deriving compare, sexp]

    type t = {edges: edge array; weight: int} [@@deriving compare, sexp]

    let create edges weight = {edges= Array.of_list_rev edges; weight}

    let weight t = t.weight [@@inline]

    let length t = Array.length t.edges [@@inline]

    let edges t =
      let len = length t in
      Sequence.(range 0 len |> map ~f:(fun i -> t.edges.(i)))

    let edges_rev t =
      let len = length t in
      Sequence.(range 0 len |> map ~f:(fun i -> t.edges.(len - i - 1)))

    let start t = t.edges.(0) [@@inline]

    let finish t = t.edges.(length t - 1) [@@inline]
  end

  module V = struct
    include Vertex
    include Comparator.Make (Vertex)
  end

  module E = struct
    include Edge
    include Comparator.Make (Edge)
  end

  type edges = Set.M(E).t Map.M(V).t

  type m = edges Map.M(V).t

  type t = {succs: m; preds: m}

  let empty' = Map.empty (module V)

  let empty = {succs= empty'; preds= empty'}

  let is_empty g = Map.is_empty g.succs

  let vertices g = Map.keys g.succs

  let iter_vertices g ~f = Map.iter_keys g.succs ~f

  let fold_vertices g ~init ~f =
    Map.fold g.succs ~init ~f:(fun ~key ~data acc -> f acc key)

  let has_vertex g v = Map.mem g.succs v

  let has_succ_edge g v e =
    match Map.find g.succs v with
    | None -> false
    | Some m -> Map.exists m ~f:(fun es -> Set.mem es e)

  let has_pred_edge g v e =
    match Map.find g.preds v with
    | None -> false
    | Some m -> Map.exists m ~f:(fun es -> Set.mem es e)

  let has_edge g v1 e v2 =
    match Map.find g.succs v1 with
    | None -> false
    | Some m -> (
      match Map.find m v2 with
      | Some es -> Set.mem es e
      | None -> false )

  let has_some_edge g v1 v2 =
    match Map.find g.succs v1 with
    | None -> false
    | Some m -> (
      match Map.find m v2 with
      | Some es -> not (Set.is_empty es)
      | None -> false )

  let collect m v =
    match Map.find m v with
    | None -> []
    | Some m' ->
        Map.to_alist m' |> List.map ~f:(fun (v, es) -> (v, Set.to_list es))

  let successors g v = collect g.succs v

  let predecessors g v = collect g.preds v

  let has_successors g v =
    match Map.find g.succs v with
    | None -> false
    | Some m -> not (Map.is_empty m)

  let has_predecessors g v =
    match Map.find g.preds v with
    | None -> false
    | Some m -> not (Map.is_empty m)

  let iter_succs g v ~f =
    match Map.find g.succs v with
    | None -> ()
    | Some m' -> Map.iteri m' ~f:(fun ~key ~data -> Set.iter data ~f:(f key))

  let iter_succs_ignore_edges g v ~f =
    match Map.find g.succs v with
    | None -> ()
    | Some m' -> Map.iter_keys m' ~f

  let iter_preds g v ~f =
    match Map.find g.preds v with
    | None -> ()
    | Some m' -> Map.iteri m' ~f:(fun ~key ~data -> Set.iter data ~f:(f key))

  let iter_preds_ignore_edges g v ~f =
    match Map.find g.preds v with
    | None -> ()
    | Some m' -> Map.iter_keys m' ~f

  let fold_succs g v ~init ~f =
    match Map.find g.succs v with
    | None -> init
    | Some m' ->
        Map.fold m' ~init ~f:(fun ~key ~data acc ->
            Set.fold data ~init:acc ~f:(fun acc e -> f acc key e) )

  let fold_succs_ignore_edges g v ~init ~f =
    match Map.find g.succs v with
    | None -> init
    | Some m' -> Map.fold m' ~init ~f:(fun ~key ~data:_ acc -> f acc key)

  let fold_preds g v ~init ~f =
    match Map.find g.preds v with
    | None -> init
    | Some m' ->
        Map.fold m' ~init ~f:(fun ~key ~data acc ->
            Set.fold data ~init:acc ~f:(fun acc e -> f acc key e) )

  let fold_preds_ignore_edges g v ~init ~f =
    match Map.find g.preds v with
    | None -> init
    | Some m' -> Map.fold m' ~init ~f:(fun ~key ~data:_ acc -> f acc key)

  let find m v f =
    collect m v |> Sequence.of_list
    |> Sequence.map ~f:(fun (v, es) ->
           Sequence.(of_list es |> map ~f:(fun e -> (v, e))) )
    |> Sequence.concat
    |> Sequence.find ~f:(fun (v, e) -> f v e)

  let find_successor g v ~f = find g.succs v f

  let find_predecessor g v ~f = find g.preds v f

  let find_vertex g ~f = vertices g |> List.find ~f

  let insert_vertex' m v =
    match Map.add m ~key:v ~data:empty' with
    | `Ok m -> m
    | `Duplicate -> m

  let insert_vertex g v =
    let succs = insert_vertex' g.succs v in
    let preds = insert_vertex' g.preds v in
    {succs; preds}

  let make_edge e = Set.singleton (module E) e

  let insert_edge' m v1 e v2 =
    let m' = Map.find_exn m v1 in
    let m' =
      match Map.find m' v2 with
      | Some es -> Map.set m' ~key:v2 ~data:Set.(add es e)
      | None -> Map.set m' ~key:v2 ~data:(make_edge e)
    in
    Map.set m ~key:v1 ~data:m'

  let insert_edge g v1 e v2 =
    let g = insert_vertex g v1 in
    let g = insert_vertex g v2 in
    let succs = insert_edge' g.succs v1 e v2 in
    let preds = insert_edge' g.preds v2 e v1 in
    {succs; preds}

  let insert_edge_symmetric g v1 e v2 =
    let g = insert_edge g v1 e v2 in
    insert_edge g v2 e v1

  let remove_vertex g v =
    let f m = Map.remove m v in
    {succs= Map.map (f g.succs) ~f; preds= Map.map (f g.preds) ~f}

  let remove_edge' m v1 e v2 =
    match Map.find m v1 with
    | None -> m
    | Some m' -> (
      match Map.find m' v2 with
      | None -> m
      | Some es ->
          let es = Set.remove es e in
          let m' =
            if Set.is_empty es then Map.remove m' v2
            else Map.set m' ~key:v2 ~data:es
          in
          Map.set m ~key:v1 ~data:m' )

  let remove_edge g v1 e v2 =
    let succs = remove_edge' g.succs v1 e v2 in
    let preds = remove_edge' g.preds v2 e v1 in
    {succs; preds}

  let remove_edge_symmetric g v1 e v2 =
    let g = remove_edge g v1 e v2 in
    remove_edge g v2 e v1

  let filter' m v ~f =
    match Map.find m v with
    | None -> m
    | Some m' ->
        let data =
          Map.filter_mapi m' ~f:(fun ~key ~data ->
              let es = Set.filter data ~f:(f key) in
              if Set.is_empty es then None else Some es )
        in
        Map.set m ~key:v ~data

  let filter_preds g v ~f =
    let preds = filter' g.preds v ~f in
    {g with preds}

  let filter_succs g v ~f =
    let succs = filter' g.succs v ~f in
    {g with succs}

  let filter_vertices g ~f =
    let removed = Vector.create () in
    Map.iter_keys g.succs ~f:(fun v ->
        if not (f v) then Vector.push_back removed v );
    Vector.fold removed ~init:g ~f:remove_vertex

  let exists_dfs ?(rev = false) g start ~f =
    let dir = if rev then predecessors g else successors g in
    let rec aux v s =
      if f v then (true, s)
      else
        let rec aux2 s = function
          | [] -> (false, s)
          | (v, _) :: vs ->
              if Set.mem s v then aux2 s vs
              else
                let res, s = aux v Set.(add s v) in
                if res then (res, s) else aux2 s vs
        in
        aux2 s (dir v)
    in
    aux start Set.(singleton (module V) start) |> fst

  let exists_bfs ?(rev = false) g start ~f =
    let q = Queue.singleton start in
    let dir = if rev then predecessors g else successors g in
    let rec aux s =
      match Queue.dequeue q with
      | Some v ->
          if f v then true
          else
            let next =
              List.map (dir v) ~f:fst
              |> List.filter ~f:(fun v -> not (Set.mem s v))
            in
            let s = List.fold next ~init:s ~f:(fun s v -> Set.add s v) in
            Queue.enqueue_all q next; aux s
      | None -> false
    in
    aux Set.(singleton (module V) start)

  let traverse_dfs_pre ?(rev = false) g start =
    let dir =
      if rev then fold_preds_ignore_edges g else fold_succs_ignore_edges g
    in
    let rec aux v s res =
      let res = v :: res in
      dir v ~init:(s, res) ~f:(fun (s, res) v ->
          if Set.mem s v then (s, res) else aux v Set.(add s v) res )
    in
    aux start Set.(singleton (module V) start) [] |> snd |> List.rev

  let traverse_dfs_post ?(rev = false) g start =
    let dir =
      if rev then fold_preds_ignore_edges g else fold_succs_ignore_edges g
    in
    let rec aux v s res =
      let s, res =
        dir v ~init:(s, res) ~f:(fun (s, res) v ->
            if Set.mem s v then (s, res) else aux v Set.(add s v) res )
      in
      (s, v :: res)
    in
    aux start Set.(singleton (module V) start) [] |> snd |> List.rev

  let iter_dfs ?(rev = false) g start ~f =
    let dir =
      if rev then fold_preds_ignore_edges g else fold_succs_ignore_edges g
    in
    let rec aux v s =
      dir v ~init:s ~f:(fun s v' ->
          if Set.mem s v' then (
            f v v' `Back;
            s )
          else (
            f v v' `Fwd;
            aux v' Set.(add s v') ) )
    in
    aux start Set.(singleton (module V) start) |> ignore

  let traverse_bfs ?(rev = false) g start =
    let q = Queue.singleton start in
    let dir = if rev then predecessors g else successors g in
    let rec aux s res =
      match Queue.dequeue q with
      | Some v ->
          let next =
            List.map (dir v) ~f:fst
            |> List.filter ~f:(fun v -> not (Set.mem s v))
          in
          let s = List.fold next ~init:s ~f:Set.add in
          Queue.enqueue_all q next;
          aux s (List.rev next @ res)
      | None -> (s, res)
    in
    aux Set.(singleton (module V) start) [start] |> snd |> List.rev

  let topological_sort g =
    let open Option.Let_syntax in
    let rec visit v sp st res =
      if not Set.(mem sp v) then Some (sp, res)
      else if Set.mem st v then None
      else
        let st = Set.add st v in
        let rec aux sp res = function
          | [] -> Some (sp, res)
          | (v, _) :: vs ->
              let%bind sp, res = visit v sp st res in
              aux sp res vs
        in
        let%map sp, res = aux sp res (successors g v) in
        (Set.remove sp v, v :: res)
    in
    let st = Set.empty (module V) in
    let rec loop sp res =
      match Set.choose sp with
      | Some v ->
          let%bind sp, res = visit v sp st res in
          loop sp res
      | None -> Some res
    in
    loop Set.(of_list (module V) (vertices g)) []

  let is_cyclic g = Option.is_none (topological_sort g)

  (* remove back edges based on the reverse post-order *)
  let make_dag ?(rev = false) g start =
    let nodes = traverse_dfs_post g start ~rev |> Array.of_list in
    Array.rev_inplace nodes;
    let rnodes =
      Array.foldi nodes ~init:empty' ~f:(fun i m n -> Map.set m n i)
    in
    let dir = if rev then fold_preds g else fold_succs g in
    Array.fold nodes ~init:empty ~f:(fun dag v ->
        let n = Map.find_exn rnodes v in
        dir v ~init:dag ~f:(fun dag v' e ->
            let n' = Map.find_exn rnodes v' in
            if n' > n then
              if rev then insert_edge dag v' e v else insert_edge dag v e v'
            else dag ) )

  (* Tarjan's SCC algorithm *)

  let strongly_connected_components g =
    let c = ref 0 in
    let s = Stack.create () in
    let idx = ref (Map.empty (module V)) in
    let lowlink = ref (Map.empty (module V)) in
    let onstack = ref (Set.empty (module V)) in
    let sccs = Vector.create () in
    let rec strong_connect v =
      idx := Map.set !idx v !c;
      lowlink := Map.set !lowlink v !c;
      incr c;
      Stack.push s v;
      onstack := Set.add !onstack v;
      iter_succs_ignore_edges g v ~f:(fun v' ->
          match Map.find !idx v' with
          | None ->
              strong_connect v';
              let i = Map.find_exn !lowlink v in
              let i' = Map.find_exn !lowlink v' in
              lowlink := Map.set !lowlink v (min i i')
          | Some i' when Set.mem !onstack v' ->
              let i = Map.find_exn !lowlink v in
              lowlink := Map.set !lowlink v (min i i')
          | _ -> () );
      if Map.find_exn !lowlink v = Map.find_exn !idx v then
        let rec loop res =
          match Stack.pop s with
          | None -> res
          | Some v' ->
              onstack := Set.remove !onstack v';
              let res = v' :: res in
              if V.compare v v' = 0 then res else loop res
        in
        let scc = loop [] |> List.dedup_and_sort ~compare:V.compare in
        if not (List.is_empty scc) then Vector.push_back sccs scc
    in
    iter_vertices g ~f:(fun v ->
        if not (Map.mem !idx v) then strong_connect v );
    Vector.rev_inplace sccs;
    Vector.to_list sccs

  (* BAP implementation of Dijkstra's algorithm *)

  let shortest_path ?(weight = fun _ _ _ -> 1) ?(rev = false) g v1 v2 =
    let cmp (n, _, _) (m, _, _) = Int.compare n m in
    let adj = if rev then iter_preds g else iter_succs g in
    let visited = ref (Set.empty (module V)) in
    let dist = ref (Map.empty (module V)) in
    let q = Pairing_heap.create ~cmp () in
    let rec loop () =
      match Pairing_heap.pop q with
      | None -> None
      | Some (w, v, p) when V.compare v v2 = 0 -> Some (Path.create p w)
      | Some (w, v, p) ->
          if not (Set.mem !visited v) then update v w p;
          loop ()
    and update v w p =
      visited := Set.add !visited v;
      adj v ~f:(fun v' e ->
          let dev = w + weight v e v' in
          match Map.find !dist v' with
          | Some w when w < dev -> ()
          | _ ->
              dist := Map.set !dist v' dev;
              Pairing_heap.add q (dev, v', (v, e, v') :: p) )
    in
    Pairing_heap.add q (0, v1, []);
    dist := Map.set !dist v1 0;
    loop ()

  type dominators =
    < dominance_frontier: vertex -> vertex list
    ; dominators: vertex -> vertex list
    ; dominating: vertex -> vertex list
    ; immediate_dominator_of: vertex -> vertex option
    ; immediately_dominated_by: vertex -> vertex list
    ; is_dominating: vertex -> vertex -> bool
    ; is_immediately_dominating: vertex -> vertex -> bool
    ; is_in_dominance_frontier: vertex -> vertex -> bool
    ; is_strictly_dominating: vertex -> vertex -> bool
    ; strict_dominators: vertex -> vertex list
    ; strictly_dominating: vertex -> vertex list >

  (* A Simple, Fast Dominance Algorithm (Cooper, Harvey, Kennedy [2001]) *)

  let dominators_aux ?(compute_df = true) ?(rev = false) g start =
    let post = traverse_dfs_post g start ~rev |> Array.of_list in
    let dir = if rev then successors g else predecessors g in
    let nums =
      Array.foldi post ~init:empty' ~f:(fun i m v -> Map.set m v i)
    in
    Array.rev_inplace post;
    let num = Map.find nums in
    let rec intersect idom v1 v2 =
      match V.compare v1 v2 with
      | 0 -> Some v1
      | _ ->
          let open Option.Let_syntax in
          let%bind n1 = num v1 in
          let%bind n2 = num v2 in
          let rec aux v1 n1 v2 n2 =
            if n1 >= n2 then return (v1, n1)
            else
              let%bind v1' = Map.find idom v1 in
              let%bind n1' = num v1' in
              aux v1' n1' v2 n2
          in
          let%bind v1, n1 = aux v1 n1 v2 n2 in
          let%bind v2, _ = aux v2 n2 v1 n1 in
          intersect idom v1 v2
    in
    let rec compute_idom idom =
      let idom, changed =
        Array.fold post ~init:(idom, false) ~f:(fun (idom, changed) v ->
            if V.compare v start = 0 then (idom, changed)
            else
              match dir v with
              | [] -> (idom, changed)
              | vs -> (
                match
                  List.fold vs ~init:None ~f:(fun acc (v, _) ->
                      match Map.find idom v with
                      | None -> acc
                      | Some _ -> (
                        match acc with
                        | None -> Some v
                        | Some v' -> intersect idom v v' ) )
                with
                | None -> (idom, changed)
                | Some v' -> (
                  match Map.find idom v with
                  | Some v'' when V.compare v' v'' = 0 -> (idom, changed)
                  | _ -> (Map.set idom v v', true) ) ) )
      in
      if changed then compute_idom idom else idom
    in
    let st = Set.singleton (module V) in
    let idom = Map.singleton (module V) start start in
    let idom = compute_idom idom in
    let idom = Map.remove idom start in
    let idoms =
      Array.fold post ~init:empty' ~f:(fun idoms v ->
          match Map.find idom v with
          | None -> idoms
          | Some v' -> (
            match Map.find idoms v' with
            | None -> Map.set idoms v' (st v)
            | Some s -> Map.set idoms v' (Set.add s v) ) )
    in
    let doms =
      Array.fold post ~init:empty' ~f:(fun doms v ->
          let rec aux doms runner =
            let doms =
              match Map.find doms v with
              | None -> Map.set doms v (st runner)
              | Some s -> Map.set doms v (Set.add s runner)
            in
            match Map.find idom runner with
            | None -> doms
            | Some runner' -> (
              match V.compare runner runner' with
              | 0 -> doms
              | _ -> aux doms runner' )
          in
          aux doms v )
    in
    let df =
      if not compute_df then empty'
      else
        Array.fold post ~init:empty' ~f:(fun df v ->
            match dir v with
            | [] | [_] -> df
            | l -> (
              match Map.find idom v with
              | None -> df
              | Some v' ->
                  List.fold l ~init:df ~f:(fun df (runner, _) ->
                      let rec aux df runner =
                        match V.compare runner v' with
                        | 0 -> df
                        | _ -> (
                            let df =
                              match Map.find df runner with
                              | None -> Map.set df runner (st v)
                              | Some s -> Map.set df runner (Set.add s v)
                            in
                            match Map.find idom runner with
                            | None -> df
                            | Some runner' -> aux df runner' )
                      in
                      aux df runner ) ) )
    in
    let inv_doms =
      Map.fold doms ~init:empty' ~f:(fun ~key ~data acc ->
          Set.fold data ~init:acc ~f:(fun acc v ->
              match Map.find acc v with
              | None -> Map.set acc v (st key)
              | Some s -> Map.set acc v (Set.add s key) ) )
    in
    object (self)
      method immediate_dominator_of = Map.find idom

      method immediately_dominated_by v =
        match Map.find idoms v with
        | None -> []
        | Some s -> Set.to_list s

      method is_immediately_dominating v1 v2 =
        match Map.find idoms v1 with
        | None -> false
        | Some s -> Set.mem s v2

      method dominators v =
        match Map.find doms v with
        | None -> []
        | Some s -> Set.to_list s

      method dominating v =
        match Map.find inv_doms v with
        | None -> []
        | Some s -> Set.to_list s

      method is_dominating v1 v2 =
        match Map.find doms v2 with
        | None -> false
        | Some s -> Set.mem s v1

      method strict_dominators v =
        match Map.find doms v with
        | None -> []
        | Some s -> Set.(to_list (remove s v))

      method strictly_dominating v =
        match Map.find inv_doms v with
        | None -> []
        | Some s -> Set.(to_list (remove s v))

      method is_strictly_dominating v1 v2 =
        match V.compare v1 v2 with
        | 0 -> false
        | _ -> self#is_dominating v1 v2

      method dominance_frontier v =
        if not compute_df then failwith "dominance frontier was not computed"
        else
          match Map.find df v with
          | None -> []
          | Some s -> Set.to_list s

      method is_in_dominance_frontier v1 v2 =
        if not compute_df then failwith "dominance frontier was not computed"
        else
          match Map.find df v1 with
          | None -> false
          | Some s -> Set.mem s v2
    end

  let dominators ?(compute_df = true) g start =
    dominators_aux g start ~compute_df

  let post_dominators ?(compute_df = true) g start =
    dominators_aux g start ~rev:true ~compute_df

  (* BAP implementation of Kildall's algorithm,
   * except the user must provide a starting node *)

  module Fixpoint = struct
    type ('n, 'd) t =
      | Solution :
          { steps: int option
          ; iters: int
          ; default: 'd
          ; approx: ('n, 'd, _) Map.t }
          -> ('n, 'd) t

    (* initial approximation of a solution where the `default`
     * parameter defines the default value of all variables
     * unspecified in the initial constraints *)
    let create constraints default =
      Solution {steps= Some 0; iters= 0; default; approx= constraints}

    (* total number of iterations made to obtain the solution *)
    let iterations (Solution {iters}) = iters

    (* default value assigned to all variables not in the
     * map of approximations (usually a 'top' or 'bottom' value) *)
    let default (Solution {default}) = default

    (* approximate value of `n` *)
    let get (Solution {approx; default}) n =
      match Map.find approx n with
      | None -> default
      | Some x -> x

    (* enumerates all non-trivial values in the solution *)
    let enum (Solution {approx}) = Map.to_sequence approx

    let is_subset ~equal (Solution {approx= m1}) ~of_:s2 =
      Map.for_alli m1 ~f:(fun ~key ~data -> equal (get s2 key) data)

    let equal ~equal (Solution {approx= m1; default= d1} as s1)
        (Solution {approx= m2; default= d2} as s2) =
      equal d1 d2
      && Map.length m1 = Map.length m2
      && is_subset ~equal s1 ~of_:s2
      && is_subset ~equal s2 ~of_:s1

    let is_fixpoint (Solution {steps; iters}) =
      match steps with
      | None -> iters > 0
      | Some steps -> iters < steps

    (* map a solution `s` to a new solution `s'` according to `f` *)
    let derive (Solution s) ~f default =
      let f ~key ~data = f key data in
      create (Map.filter_mapi ~f s.approx) default

    type ('a, 'b) step = Step of 'a | Done of 'b

    let continue x = Step x

    (* `steps` is an upper bound on the number of iterations
     * `rev` will determine whether the graph is traversed in reverse order
     * `step` is called every time a new value of a variable is obtained
     * `init` is the initial approximation of a solution
     * `start` is the starting node in the graph
     * `equal` compares two approximations for equivalence
     * `merge` is called when two paths merge (usually 'join' or 'meet')
     * `f` is the transfer function
     * `g` is the graph to operate over *)
    let compute (type d) ?steps ?(rev = false) ?step
        ~init:(Solution {approx= init; iters; default}) ~start ~equal ~merge
        ~f g : (vertex, d) t =
      let nodes = traverse_dfs_post g start ~rev |> Array.of_list in
      let dir = if rev then predecessors g else successors g in
      Array.rev_inplace nodes;
      let rnodes =
        Array.foldi nodes
          ~init:(Map.empty (module V))
          ~f:(fun i rnodes n -> Map.set rnodes ~key:n ~data:i)
      in
      let succs =
        Array.map nodes ~f:(fun n ->
            dir n |> Sequence.of_list |> Sequence.map ~f:fst
            |> Sequence.fold ~init:Int.Set.empty ~f:(fun ns n ->
                   match Map.find rnodes n with
                   | None -> ns
                   | Some i -> Set.add ns i ) )
      in
      let user_step =
        match step with
        | None -> fun visits _ _ x -> (visits, x)
        | Some step ->
            fun visits n x x' ->
              let i =
                match Map.find visits n with
                | None -> 1
                | Some x -> x + 1
              in
              let visits = Map.set visits ~key:n ~data:i in
              (visits, step i nodes.(n) x x')
      in
      let get approx n : d =
        match Map.find approx n with
        | Some x -> x
        | None -> default
      in
      let step visits works approx =
        match Set.min_elt works with
        | None -> Done approx
        | Some n ->
            let works = Set.remove works n in
            let out = f nodes.(n) (get approx n) in
            succs.(n)
            |> Set.fold ~init:(visits, works, approx)
                 ~f:(fun (visits, works, approx) n ->
                   let ap = get approx n in
                   let ap' = merge out ap in
                   let visits, ap' = user_step visits n ap ap' in
                   if equal ap ap' then (visits, works, approx)
                   else
                     ( visits
                     , Set.add works n
                     , Map.set approx ~key:n ~data:ap' ) )
            |> continue
      in
      let can_iter iters =
        match steps with
        | None -> true
        | Some steps -> iters < steps
      in
      let make_solution iters approx =
        Solution
          { steps
          ; iters
          ; default
          ; approx=
              Map.fold approx ~init ~f:(fun ~key:n ~data approx ->
                  Map.set approx ~key:nodes.(n) ~data ) }
      in
      let rec loop visits iters works approx =
        if can_iter iters then
          match step visits works approx with
          | Done approx -> make_solution iters approx
          | Step (visits, works, approx) ->
              loop visits (iters + 1) works approx
        else make_solution iters approx
      in
      let works = List.init (Array.length nodes) ident in
      let approx =
        Map.fold init ~init:Int.Map.empty ~f:(fun ~key:node ~data approx ->
            match Map.find rnodes node with
            | None -> approx
            | Some n -> Map.set approx ~key:n ~data )
      in
      loop Int.Map.empty iters (Int.Set.of_list works) approx
  end

  let fixpoint = Fixpoint.compute
end
