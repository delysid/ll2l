open Core_kernel

module type S = sig
  type t

  val compare : t -> t -> int

  val sexp_of_t : t -> Sexp.t

  val t_of_sexp : Sexp.t -> t
end

module type Solution = sig
  type ('n, 'd) t

  val create : ('n, 'd, _) Map.t -> 'd -> ('n, 'd) t

  val equal : equal:('d -> 'd -> bool) -> ('n, 'd) t -> ('n, 'd) t -> bool

  val iterations : ('n, 'd) t -> int

  val enum : ('n, 'd) t -> ('n * 'd) Sequence.t

  val default : ('n, 'd) t -> 'd

  val is_fixpoint : ('n, 'd) t -> bool

  val get : ('n, 'd) t -> 'n -> 'd

  val derive : ('n, 'd) t -> f:('n -> 'd -> 'a option) -> 'a -> ('n, 'a) t
end

(* persistent, immutable graph data structure,
 * which is implemented using maps and sets.
 *
 * this structure is useful if you want to keep
 * multiple copies of the same graph and make
 * smaller modifications to each. *)

module Make : functor (Vertex : S) (Edge : S) -> sig
  type t

  type vertex = Vertex.t

  type edge = Edge.t

  module Path : sig
    type nonrec edge = vertex * edge * vertex [@@deriving compare, sexp]

    type t [@@deriving compare, sexp]

    val create : edge list -> int -> t

    val weight : t -> int

    val length : t -> int

    val edges : t -> edge Sequence.t

    val edges_rev : t -> edge Sequence.t

    val start : t -> edge

    val finish : t -> edge
  end

  val empty : t

  val is_empty : t -> bool

  val vertices : t -> vertex list

  val iter_vertices : t -> f:(vertex -> unit) -> unit

  val fold_vertices : t -> init:'acc -> f:('acc -> vertex -> 'acc) -> 'acc

  val has_vertex : t -> vertex -> bool

  val has_succ_edge : t -> vertex -> edge -> bool

  val has_pred_edge : t -> vertex -> edge -> bool

  val has_edge : t -> vertex -> edge -> vertex -> bool

  val has_some_edge : t -> vertex -> vertex -> bool

  val successors : t -> vertex -> (vertex * edge list) list

  val predecessors : t -> vertex -> (vertex * edge list) list

  val has_successors : t -> vertex -> bool

  val has_predecessors : t -> vertex -> bool

  val iter_succs : t -> vertex -> f:(vertex -> edge -> unit) -> unit

  val iter_succs_ignore_edges : t -> vertex -> f:(vertex -> unit) -> unit

  val iter_preds : t -> vertex -> f:(vertex -> edge -> unit) -> unit

  val iter_preds_ignore_edges : t -> vertex -> f:(vertex -> unit) -> unit

  val fold_succs :
    t -> vertex -> init:'acc -> f:('acc -> vertex -> edge -> 'acc) -> 'acc

  val fold_succs_ignore_edges :
    t -> vertex -> init:'acc -> f:('acc -> vertex -> 'acc) -> 'acc

  val fold_preds :
    t -> vertex -> init:'acc -> f:('acc -> vertex -> edge -> 'acc) -> 'acc

  val fold_preds_ignore_edges :
    t -> vertex -> init:'acc -> f:('acc -> vertex -> 'acc) -> 'acc

  val find_successor :
    t -> vertex -> f:(vertex -> edge -> bool) -> (vertex * edge) option

  val find_predecessor :
    t -> vertex -> f:(vertex -> edge -> bool) -> (vertex * edge) option

  val find_vertex : t -> f:(vertex -> bool) -> vertex option

  val insert_vertex : t -> vertex -> t

  val insert_edge : t -> vertex -> edge -> vertex -> t

  val insert_edge_symmetric : t -> vertex -> edge -> vertex -> t

  val remove_vertex : t -> vertex -> t

  val remove_edge : t -> vertex -> edge -> vertex -> t

  val remove_edge_symmetric : t -> vertex -> edge -> vertex -> t

  val filter_preds : t -> vertex -> f:(vertex -> edge -> bool) -> t

  val filter_succs : t -> vertex -> f:(vertex -> edge -> bool) -> t

  val filter_vertices : t -> f:(vertex -> bool) -> t

  val exists_dfs : ?rev:bool -> t -> vertex -> f:(vertex -> bool) -> bool

  val exists_bfs : ?rev:bool -> t -> vertex -> f:(vertex -> bool) -> bool

  val traverse_dfs_pre : ?rev:bool -> t -> vertex -> vertex list

  val traverse_dfs_post : ?rev:bool -> t -> vertex -> vertex list

  val iter_dfs :
       ?rev:bool
    -> t
    -> vertex
    -> f:(vertex -> vertex -> [`Back | `Fwd] -> unit)
    -> unit

  val traverse_bfs : ?rev:bool -> t -> vertex -> vertex list

  val topological_sort : t -> vertex list option

  val is_cyclic : t -> bool

  val make_dag : ?rev:bool -> t -> vertex -> t

  val strongly_connected_components : t -> vertex list list

  val shortest_path :
       ?weight:(vertex -> edge -> vertex -> int)
    -> ?rev:bool
    -> t
    -> vertex
    -> vertex
    -> Path.t option

  type dominators =
    < dominance_frontier: vertex -> vertex list
    ; dominators: vertex -> vertex list
    ; dominating: vertex -> vertex list
    ; immediate_dominator_of: vertex -> vertex option
    ; immediately_dominated_by: vertex -> vertex list
    ; is_dominating: vertex -> vertex -> bool
    ; is_immediately_dominating: vertex -> vertex -> bool
    ; is_in_dominance_frontier: vertex -> vertex -> bool
    ; is_strictly_dominating: vertex -> vertex -> bool
    ; strict_dominators: vertex -> vertex list
    ; strictly_dominating: vertex -> vertex list >

  val dominators : ?compute_df:bool -> t -> vertex -> dominators

  val post_dominators : ?compute_df:bool -> t -> vertex -> dominators

  module Fixpoint : Solution

  val fixpoint :
       ?steps:int
    -> ?rev:bool
    -> ?step:(int -> vertex -> 'd -> 'd -> 'd)
    -> init:(vertex, 'd) Fixpoint.t
    -> start:vertex
    -> equal:('d -> 'd -> bool)
    -> merge:('d -> 'd -> 'd)
    -> f:(vertex -> 'd -> 'd)
    -> t
    -> (vertex, 'd) Fixpoint.t
end
