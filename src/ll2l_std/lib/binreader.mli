open Core_kernel

type t

val from_bigstring : Bigstring.t -> t

val from_bytes : Bytes.t -> t

val position : t -> int

val seek : t -> int -> unit option

val skip : t -> int -> unit option

val peek8 : t -> int option

val read8 : t -> int option

val peek16 : t -> Endian.t -> int option

val read16 : t -> Endian.t -> int option

val peek32 : t -> Endian.t -> int32 option

val read32 : t -> Endian.t -> int32 option

val peek64 : t -> Endian.t -> int64 option

val read64 : t -> Endian.t -> int64 option

val peek_bytes : t -> int -> bytes option

val read_bytes : t -> int -> bytes option

val peek_string : ?trunc:bool -> ?size:int option -> t -> string option

val read_string : ?trunc:bool -> ?size:int option -> t -> string option

val peek_uleb128 : t -> (int64 * int) option

val read_uleb128 : t -> int64 option

val peek_sleb128 : t -> (int64 * int) option

val read_sleb128 : t -> int64 option
