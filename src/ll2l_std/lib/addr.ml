open Core_kernel
open Bitvec
include Make (M64)
module Map = Map.Make (Make (M64))
module Set = Set.Make (Make (M64))
