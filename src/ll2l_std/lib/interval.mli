(* this is a port of LLVM's ConstantRange class,
 * made to work with Bitvec instead of APInt *)

open Core_kernel

type t [@@deriving sexp]

val create_empty : size:int -> t

val create_full : size:int -> t

val create_single : value:Bitvec.t -> size:int -> t

val boolean_full : t

val boolean_true : t

val boolean_false : t

val boolean : bool -> t

val boolean_empty : t

val create : lo:Bitvec.t -> hi:Bitvec.t -> size:int -> t

val create_non_empty : lo:Bitvec.t -> hi:Bitvec.t -> size:int -> t

val lower : t -> Bitvec.t

val upper : t -> Bitvec.t

val size_of : t -> int

val is_empty : t -> bool

val is_full : t -> bool

val is_wrapped : t -> bool

val is_wrapped_hi : t -> bool

val is_sign_wrapped : t -> bool

val is_sign_wrapped_hi : t -> bool

val single_of : t -> Bitvec.t option

val single_missing_of : t -> Bitvec.t option

val is_single : t -> bool

val is_strictly_smaller_than : t -> t -> bool

val is_larger_than : t -> Bitvec.t -> bool

val is_all_negative : t -> bool

val is_all_non_negative : t -> bool

val unsigned_max : t -> Bitvec.t

val unsigned_min : t -> Bitvec.t

val signed_max : t -> Bitvec.t

val signed_min : t -> Bitvec.t

val equal : t -> t -> bool

val contains_value : t -> Bitvec.t -> bool

val contains : t -> t -> bool

val subtract : t -> Bitvec.t -> t

type preferred_range = Smallest | Unsigned | Signed

val intersect : ?range:preferred_range -> t -> t -> t

val union : ?range:preferred_range -> t -> t -> t

val inverse : t -> t

val difference : t -> t -> t

val zext : t -> size:int -> t

val sext : t -> size:int -> t

val trunc : t -> size:int -> t

val add : t -> t -> t

val sub : t -> t -> t

val mul : t -> t -> t

val smax : t -> t -> t

val umax : t -> t -> t

val smin : t -> t -> t

val umin : t -> t -> t

val udiv : t -> t -> t

val sdiv : t -> t -> t

val abs : ?int_min_is_poison:bool -> t -> t

val urem : t -> t -> t

val srem : t -> t -> t

val lnot : t -> t

val neg : t -> t

val ( land ) : t -> t -> t

val ( lor ) : t -> t -> t

val ( lxor ) : t -> t -> t

val ( lsl ) : t -> t -> t

val ( lsr ) : t -> t -> t

val ( asr ) : t -> t -> t

val extract : t -> hi:int -> lo:int -> t

val concat : t -> t -> t

val to_string : t -> string

val ( + ) : t -> t -> t

val ( - ) : t -> t -> t

val ( * ) : t -> t -> t

val ( / ) : t -> t -> t

val ( /$ ) : t -> t -> t

val ( % ) : t -> t -> t

val ( %^ ) : t -> t -> t

val ( ~~ ) : t -> t

val ( ~- ) : t -> t

val ( & ) : t -> t -> t

val ( ^ ) : t -> t -> t

val ( << ) : t -> t -> t

val ( >> ) : t -> t -> t

val ( >>> ) : t -> t -> t
