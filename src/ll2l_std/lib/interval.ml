(* this is a port of LLVM's ConstantRange class,
 * made to work with Bitvec instead of APInt *)

open Core_kernel

type t = {lo: Bitvec.t; hi: Bitvec.t; size: int} [@@deriving sexp]

let create_empty ~size =
  let lo = Bitvec.min_unsigned_value in
  {lo; hi= lo; size}

let create_full ~size =
  let lo = Bitvec.max_unsigned_value size in
  {lo; hi= lo; size}

let create_single ~value ~size =
  {lo= value; hi= Bitvec.(succ value mod modulus size); size}

let boolean_full = {lo= Bitvec.one; hi= Bitvec.one; size= 1}

let boolean_true = {lo= Bitvec.one; hi= Bitvec.zero; size= 1}

let boolean_false = {lo= Bitvec.zero; hi= Bitvec.one; size= 1}

let boolean = function
  | true -> boolean_true
  | false -> boolean_false

let boolean_empty = {lo= Bitvec.zero; hi= Bitvec.zero; size= 1}

let create ~lo ~hi ~size =
  assert (Bitvec.(lo <> hi || lo = max_unsigned_value size || lo = zero));
  {lo; hi; size}

let create_non_empty ~lo ~hi ~size =
  if Bitvec.(lo = hi) then create_full ~size else {lo; hi; size}

let lower t = t.lo [@@inline]

let upper t = t.hi [@@inline]

let size_of t = t.size [@@inline]

let is_empty t = Bitvec.(t.lo = t.hi && t.lo = min_unsigned_value) [@@inline]

let is_full t =
  Bitvec.(t.lo = t.hi && t.lo = max_unsigned_value t.size)
  [@@inline]

let is_wrapped t = Bitvec.(t.lo > t.hi && t.hi <> zero) [@@inline]

let is_wrapped_hi t = Bitvec.(t.lo > t.hi) [@@inline]

let is_sign_wrapped t =
  let m = Bitvec.modulus t.size in
  Bitvec.signed_compare t.lo t.hi m > 0
  && Bitvec.(t.hi <> min_signed_value t.size)

let is_sign_wrapped_hi t =
  let m = Bitvec.modulus t.size in
  Bitvec.signed_compare t.lo t.hi m > 0

let single_of t =
  if Bitvec.(t.hi = succ t.lo mod modulus t.size) then Some t.lo else None

let single_missing_of t =
  if Bitvec.(t.lo = succ t.hi mod modulus t.size) then Some t.hi else None

let is_single t = Option.is_some (single_of t) [@@inline]

let is_strictly_smaller_than t1 t2 =
  assert (t1.size = t2.size);
  if is_full t1 then false
  else if is_full t2 then true
  else
    let m = Bitvec.modulus t1.size in
    Bitvec.((t1.hi - t1.lo) mod m < (t2.hi - t2.lo) mod m)

let is_larger_than t max_size =
  let m = Bitvec.modulus t.size in
  if is_full t then Bitvec.(max_unsigned_value t.size > pred max_size mod m)
  else Bitvec.((t.hi - t.lo) mod m > max_size)

let is_all_negative t =
  if is_empty t then true
  else if is_full t then false
  else
    not
      ( is_sign_wrapped_hi t
      || Bitvec.(signed_compare t.hi zero (modulus t.size)) > 0 )

let is_all_non_negative t =
  (not (is_sign_wrapped t))
  && Bitvec.(signed_compare t.lo zero (modulus t.size)) >= 0

let unsigned_max t =
  let m = Bitvec.modulus t.size in
  if is_full t || is_wrapped_hi t then Bitvec.max_unsigned_value t.size
  else Bitvec.(pred t.hi mod m)

let unsigned_min t = if is_full t || is_wrapped t then Bitvec.zero else t.lo

let signed_max t =
  let m = Bitvec.modulus t.size in
  if is_full t || is_sign_wrapped_hi t then Bitvec.max_signed_value t.size
  else Bitvec.(pred t.hi mod m)

let signed_min t =
  if is_full t || is_sign_wrapped t then Bitvec.min_signed_value t.size
  else t.lo

let equal t1 t2 =
  if t1.size = t2.size then
    (is_full t1 && is_full t2)
    || (is_empty t1 && is_empty t2)
    || Bitvec.(t1.lo = t2.lo && t1.hi = t2.hi)
  else false

let contains_value t v =
  if Bitvec.(t.lo = t.hi) then is_full t
  else if is_wrapped_hi t then Bitvec.(t.lo <= v && v < t.hi)
  else Bitvec.(t.lo <= v || v < t.hi)

let contains t1 t2 =
  if is_full t1 || is_empty t2 then true
  else if is_empty t1 || is_full t2 then false
  else if not (is_wrapped_hi t1) then
    if is_wrapped_hi t2 then false
    else Bitvec.(t1.lo <= t2.lo && t2.hi <= t1.hi)
  else if not (is_wrapped_hi t2) then
    Bitvec.(t2.hi <= t1.hi || t1.lo <= t2.lo)
  else Bitvec.(t2.hi <= t1.hi && t1.lo <= t2.lo)

let subtract t v =
  if Bitvec.(t.lo = t.hi) then t
  else
    let m = Bitvec.modulus t.size in
    {t with lo= Bitvec.((t.lo - v) mod m); hi= Bitvec.((t.hi - v) mod m)}

type preferred_range = Smallest | Unsigned | Signed

let preferred_range t1 t2 = function
  | Unsigned ->
      if (not (is_wrapped t1)) && is_wrapped t2 then t1
      else if is_wrapped t1 && not (is_wrapped t2) then t2
      else if is_strictly_smaller_than t1 t2 then t1
      else t2
  | Signed ->
      if (not (is_sign_wrapped t1)) && is_sign_wrapped t2 then t1
      else if is_sign_wrapped t1 && not (is_sign_wrapped t2) then t2
      else if is_strictly_smaller_than t1 t2 then t1
      else t2
  | Smallest -> if is_strictly_smaller_than t1 t2 then t1 else t2

let rec intersect ?(range = Smallest) t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_full t2 then t1
  else if is_empty t2 && is_full t1 then t2
  else if (not (is_wrapped_hi t1)) && is_wrapped_hi t2 then
    intersect t2 t1 ~range
  else if (not (is_wrapped_hi t1)) && not (is_wrapped_hi t2) then
    if Bitvec.(t1.lo < t2.lo) then
      if Bitvec.(t1.hi <= t2.lo) then create_empty ~size
      else if Bitvec.(t1.hi < t2.hi) then create ~lo:t2.lo ~hi:t1.hi ~size
      else t2
    else if Bitvec.(t1.hi < t2.hi) then t1
    else if Bitvec.(t1.lo < t2.hi) then create ~lo:t1.lo ~hi:t2.hi ~size
    else create_empty ~size
  else if is_wrapped_hi t1 && not (is_wrapped_hi t2) then
    if Bitvec.(t2.lo < t1.hi) then
      if Bitvec.(t2.hi < t1.hi) then t2
      else if Bitvec.(t2.hi <= t1.lo) then create ~lo:t2.lo ~hi:t1.hi ~size
      else preferred_range t1 t2 range
    else if Bitvec.(t2.lo < t1.lo) then
      if Bitvec.(t2.hi <= t1.lo) then create_empty ~size
      else create ~lo:t1.lo ~hi:t2.hi ~size
    else t2
  else if Bitvec.(t2.hi < t1.hi) then
    if Bitvec.(t2.lo < t1.hi) then preferred_range t1 t2 range
    else if Bitvec.(t2.lo < t1.lo) then create ~lo:t1.lo ~hi:t2.hi ~size
    else t2
  else if Bitvec.(t2.hi <= t1.lo) then
    if Bitvec.(t2.lo < t1.lo) then t1 else create ~lo:t2.lo ~hi:t1.hi ~size
  else preferred_range t1 t2 range

let rec union ?(range = Smallest) t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_full t2 then t2
  else if is_empty t2 || is_full t1 then t1
  else if (not (is_wrapped_hi t1)) && is_wrapped_hi t2 then
    union t2 t1 ~range
  else if (not (is_wrapped_hi t1)) && not (is_wrapped_hi t2) then
    if Bitvec.(t2.hi < t1.lo || t1.hi < t2.lo) then
      preferred_range
        (create ~lo:t1.lo ~hi:t2.hi ~size)
        (create ~lo:t2.lo ~hi:t1.hi ~size)
        range
    else
      let m = Bitvec.modulus size in
      let lo = Bitvec.min t2.lo t1.lo in
      let hi =
        if Bitvec.(pred t2.hi mod m > pred t1.hi mod m) then t2.hi else t1.hi
      in
      if Bitvec.(lo = zero && hi = zero) then create_full ~size
      else create ~lo ~hi ~size
  else if not (is_wrapped_hi t2) then
    if Bitvec.(t2.hi <= t1.hi || t2.lo >= t1.lo) then t1
    else if Bitvec.(t2.lo <= t1.hi && t1.lo <= t2.hi) then create_full ~size
    else if Bitvec.(t1.hi < t2.lo && t2.hi < t1.lo) then
      preferred_range
        (create ~lo:t1.lo ~hi:t2.hi ~size)
        (create ~lo:t2.lo ~hi:t1.hi ~size)
        range
    else if Bitvec.(t1.hi < t2.lo && t1.lo <= t2.hi) then
      create ~lo:t2.lo ~hi:t1.hi ~size
    else (
      assert (Bitvec.(t2.lo <= t1.hi && t2.hi < t1.lo));
      create ~lo:t1.lo ~hi:t2.hi ~size )
  else if Bitvec.(t2.lo <= t1.hi || t1.lo <= t2.hi) then create_full ~size
  else
    let lo = Bitvec.min t2.lo t1.lo in
    let hi = Bitvec.max t2.hi t1.hi in
    create ~lo ~hi ~size

let inverse t =
  let size = t.size in
  if is_full t then create_empty ~size
  else if is_empty t then create_full ~size
  else create ~lo:t.hi ~hi:t.lo ~size

let difference t1 t2 = intersect t1 (inverse t2) [@@inline]

let zext t ~size =
  if is_empty t then create_empty ~size
  else (
    assert (t.size < size);
    if is_full t || is_wrapped_hi t then
      let lo = if Bitvec.(t.hi = zero) then t.lo else Bitvec.zero in
      create ~lo ~hi:Bitvec.(setbit zero t.size) ~size
    else create ~lo:t.lo ~hi:t.hi ~size )

let sext t ~size =
  if is_empty t then create_empty ~size
  else (
    assert (t.size < size);
    let m = Bitvec.modulus size in
    let sext v =
      let mask = Bitvec.((one lsl (int Int.(t.size - 1) mod m)) mod m) in
      Bitvec.(((v lxor mask mod m) - mask) mod m)
    in
    if Bitvec.(t.hi = min_signed_value t.size) then
      create ~lo:(sext t.lo) ~hi:(sext t.hi) ~size
    else if is_full t || is_sign_wrapped_hi t then
      create
        ~lo:
          Bitvec.(
            lnot
              ( pred
                  ( (one lsl (int Int.(size - (size - t.size + 1)) mod m))
                  mod m )
              mod m )
            mod m)
        ~hi:Bitvec.((one lsl (int Int.(t.size - 1) mod m)) mod m)
        ~size
    else create ~lo:(sext t.lo) ~hi:(sext t.hi) ~size )

let trunc t ~size =
  assert (t.size > size);
  let default un lower_div upper_div =
    let lower_div, upper_div =
      let lz = Bitvec.clz lower_div t.size in
      if t.size - lz > size then
        let m = Bitvec.modulus t.size in
        let adj =
          Bitvec.(
            lower_div
            land (pred ((one lsl (int Int.(size - 1) mod m)) mod m) mod m)
            mod m)
        in
        (Bitvec.((lower_div - adj) mod m), Bitvec.((upper_div - adj) mod m))
      else (lower_div, upper_div)
    in
    let w = t.size - Bitvec.clz upper_div t.size in
    if w <= size then union (create ~lo:lower_div ~hi:upper_div ~size) un
    else if w = size + 1 then
      let m = Bitvec.modulus t.size in
      let upper_div =
        Bitvec.(
          upper_div
          land (lnot ((one lsl (int size mod m)) mod m) mod m)
          mod m)
      in
      if Bitvec.(upper_div < lower_div) then
        union (create ~lo:lower_div ~hi:upper_div ~size) un
      else create_full ~size
    else create_full ~size
  in
  if is_empty t then create_empty ~size
  else if is_full t then create_full ~size
  else if is_wrapped_hi t then
    let lz = Bitvec.clz t.hi t.size in
    if t.size - lz > size || Bitvec.cto t.hi t.size = size then
      create_full ~size
    else
      let un =
        create
          ~lo:Bitvec.(max_unsigned_value size)
          ~hi:(Bitvec.extract ~hi:(size - 1) ~lo:0 t.hi)
          ~size
      in
      let upper_div = Bitvec.max_unsigned_value t.size in
      if Bitvec.(t.lo = upper_div) then un else default un t.lo upper_div
  else default (create_empty ~size) t.lo t.hi

let add t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else if is_full t1 || is_full t2 then create_full ~size
  else
    let m = Bitvec.modulus size in
    let lo = Bitvec.((t1.lo + t2.lo) mod m) in
    let hi = Bitvec.(pred ((t1.hi + t2.hi) mod m) mod m) in
    if Bitvec.(lo = hi) then create_full ~size
    else
      let x = create ~lo ~hi ~size in
      if is_strictly_smaller_than x t1 || is_strictly_smaller_than x t2 then
        create_full ~size
      else x

let sub t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else if is_full t1 || is_full t2 then create_full ~size
  else
    let m = Bitvec.modulus size in
    let lo = Bitvec.(succ ((t1.lo - t2.hi) mod m) mod m) in
    let hi = Bitvec.((t1.hi - t2.lo) mod m) in
    if Bitvec.(lo = hi) then create_full ~size
    else
      let x = create ~lo ~hi ~size in
      if is_strictly_smaller_than x t1 || is_strictly_smaller_than x t2 then
        create_full ~size
      else x

let mul t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let min1 = unsigned_min t1 in
    let max1 = unsigned_max t1 in
    let min2 = unsigned_min t2 in
    let max2 = unsigned_max t2 in
    let m2 = Bitvec.modulus (size * 2) in
    let result =
      create
        ~lo:Bitvec.(min1 * min2 mod m2)
        ~hi:Bitvec.(succ (max1 * max2 mod m2) mod m2)
        ~size:(size * 2)
    in
    let ur = trunc result size in
    let m = Bitvec.modulus size in
    if
      (not (is_wrapped_hi ur))
      && ( Bitvec.(signed_compare ur.hi zero m) >= 0
         || Bitvec.(ur.hi = min_signed_value size) )
    then ur
    else
      let min1 = signed_min t1 in
      let max1 = signed_max t1 in
      let min2 = signed_min t2 in
      let max2 = signed_max t2 in
      let p =
        [| Bitvec.(min1 * min2 mod m2)
         ; Bitvec.(min1 * max2 mod m2)
         ; Bitvec.(max1 * min2 mod m2)
         ; Bitvec.(max1 * max2 mod m2) |]
      in
      let compare x y = Bitvec.signed_compare x y m2 in
      let lo = Array.min_elt p ~compare in
      let hi = Array.max_elt p ~compare in
      let lo, hi =
        match (lo, hi) with
        | None, _ | _, None -> assert false
        | Some lo, Some hi -> (lo, Bitvec.(succ hi mod m2))
      in
      let result = create ~lo ~hi ~size:(size * 2) in
      let sr = trunc result size in
      if is_strictly_smaller_than ur sr then ur else sr

let smax t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let m = Bitvec.modulus size in
    let min1 = signed_min t1 in
    let max1 = signed_max t1 in
    let min2 = signed_min t2 in
    let max2 = signed_max t2 in
    let lo = if Bitvec.signed_compare min1 min2 m > 0 then min1 else min2 in
    let hi = if Bitvec.signed_compare max1 max2 m > 0 then max1 else max2 in
    let hi = Bitvec.(succ hi mod m) in
    create_non_empty ~lo ~hi ~size

let umax t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let m = Bitvec.modulus size in
    let min1 = unsigned_min t1 in
    let max1 = unsigned_max t1 in
    let min2 = unsigned_min t2 in
    let max2 = unsigned_max t2 in
    let lo = Bitvec.max min1 min2 in
    let hi = Bitvec.max max1 max2 in
    let hi = Bitvec.(succ hi mod m) in
    create_non_empty ~lo ~hi ~size

let smin t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let m = Bitvec.modulus size in
    let min1 = signed_min t1 in
    let max1 = signed_max t1 in
    let min2 = signed_min t2 in
    let max2 = signed_max t2 in
    let lo = if Bitvec.signed_compare min1 min2 m < 0 then min1 else min2 in
    let hi = if Bitvec.signed_compare max1 max2 m < 0 then max1 else max2 in
    let hi = Bitvec.(succ hi mod m) in
    create_non_empty ~lo ~hi ~size

let umin t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let m = Bitvec.modulus size in
    let min1 = unsigned_min t1 in
    let max1 = unsigned_max t1 in
    let min2 = unsigned_min t2 in
    let max2 = unsigned_max t2 in
    let lo = Bitvec.min min1 min2 in
    let hi = Bitvec.min max1 max2 in
    let hi = Bitvec.(succ hi mod m) in
    create_non_empty ~lo ~hi ~size

let rec udiv t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  if is_empty t1 || is_empty t2 || Bitvec.(unsigned_max t2 = zero) then
    create_empty ~size
  else
    let m = Bitvec.modulus size in
    let lo = Bitvec.(unsigned_min t1 / unsigned_max t2 mod m) in
    let umin2 = unsigned_min t2 in
    let umin2 =
      if Bitvec.(umin2 = zero) then
        if Bitvec.(t2.hi = one) then t2.lo else Bitvec.one
      else umin2
    in
    let hi = Bitvec.(succ (unsigned_max t1 / umin2 mod m) mod m) in
    create_non_empty ~lo ~hi ~size

let sdiv t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  let signed_min' = Bitvec.min_signed_value size in
  let unsigned_max' = Bitvec.max_unsigned_value size in
  let pos_filter = create ~lo:Bitvec.one ~hi:signed_min' ~size in
  let neg_filter = create ~lo:signed_min' ~hi:Bitvec.zero ~size in
  let pos_l = intersect t1 pos_filter in
  let neg_l = intersect t1 neg_filter in
  let pos_r = intersect t2 pos_filter in
  let neg_r = intersect t2 neg_filter in
  let pos_res = create_empty ~size in
  let pos_res =
    if not (is_empty pos_l || is_empty pos_r) then
      create
        ~lo:Bitvec.(pos_l.lo /$ (pred pos_r.hi mod m) mod m)
        ~hi:Bitvec.(succ (pred pos_l.hi mod m /$ pos_r.lo mod m) mod m)
        ~size
    else pos_res
  in
  let pos_res =
    if not (is_empty neg_l || is_empty neg_r) then
      let lo = Bitvec.(pred neg_l.hi mod m /$ neg_r.lo mod m) in
      if Bitvec.(neg_l.lo = signed_min' && neg_r.hi = zero) then
        let pos_res =
          if Bitvec.(neg_r.lo = unsigned_max') then
            let adj =
              if Bitvec.(t2.lo = unsigned_max') then t2.hi
              else Bitvec.(pred neg_r.hi mod m)
            in
            union pos_res
              (create ~lo
                 ~hi:Bitvec.(succ (neg_l.lo /$ (pred adj mod m) mod m) mod m)
                 ~size)
          else pos_res
        in
        let signed_min'' = Bitvec.(succ signed_min' mod m) in
        if Bitvec.(neg_l.hi <> signed_min'') then
          let adj =
            if Bitvec.(t1.hi = signed_min'') then t1.lo
            else Bitvec.(succ neg_l.lo mod m)
          in
          union pos_res
            (create ~lo
               ~hi:Bitvec.(succ (adj /$ (pred neg_r.hi mod m) mod m) mod m)
               ~size)
        else pos_res
      else
        union pos_res
          (create ~lo
             ~hi:
               Bitvec.(succ (neg_l.lo /$ (pred neg_r.hi mod m) mod m) mod m)
             ~size)
    else pos_res
  in
  let neg_res = create_empty ~size in
  let neg_res =
    if not (is_empty pos_l || is_empty neg_r) then
      create
        ~lo:Bitvec.(pred pos_l.hi mod m /$ (pred neg_r.hi mod m) mod m)
        ~hi:Bitvec.(succ (pos_l.lo /$ neg_r.lo mod m) mod m)
        ~size
    else neg_res
  in
  let neg_res =
    if not (is_empty neg_l || is_empty pos_r) then
      union neg_res
        (create
           ~lo:Bitvec.(neg_l.lo /$ pos_r.lo mod m)
           ~hi:
             Bitvec.(
               succ (pred neg_l.hi mod m /$ (pred pos_r.hi mod m) mod m)
               mod m)
           ~size)
    else neg_res
  in
  let res = union neg_res pos_res ~range:Signed in
  if contains_value t1 Bitvec.zero && not (is_empty pos_r && is_empty neg_r)
  then union res (create_single ~value:Bitvec.zero ~size)
  else res

let urem t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  if is_empty t1 || is_empty t2 || Bitvec.(unsigned_max t2 = zero) then
    create_empty ~size
  else if Bitvec.(unsigned_max t1 < unsigned_min t2) then t1
  else
    let hi =
      Bitvec.(
        succ (min (unsigned_max t1) (pred (unsigned_max t1) mod m)) mod m)
    in
    create_non_empty ~lo:Bitvec.zero ~hi ~size

let abs ?(int_min_is_poison = false) t =
  let size = t.size in
  let m = Bitvec.modulus size in
  if is_empty t then create_empty ~size
  else if is_sign_wrapped_hi t then
    let lo =
      if
        Bitvec.(signed_compare t.hi zero m) > 0
        || Bitvec.(signed_compare t.lo zero m) <= 0
      then Bitvec.zero
      else Bitvec.(min t.lo (succ (~-(t.hi) mod m) mod m))
    in
    if int_min_is_poison then
      create ~lo ~hi:Bitvec.(min_signed_value size) ~size
    else create ~lo ~hi:Bitvec.(succ (min_signed_value size) mod m) ~size
  else
    let smin' = signed_min t in
    let smax' = signed_max t in
    let tst = int_min_is_poison && Bitvec.(smin' = min_signed_value size) in
    if tst && Bitvec.(smax' = min_signed_value size) then create_empty ~size
    else
      let smin' = Bitvec.(succ smin' mod m) in
      if Bitvec.(signed_compare smin' zero m) >= 0 then t
      else if Bitvec.(signed_compare smax' zero m) < 0 then
        create
          ~lo:Bitvec.(~-smax' mod m)
          ~hi:Bitvec.(succ (~-smin' mod m) mod m)
          ~size
      else
        create ~lo:Bitvec.zero
          ~hi:Bitvec.(succ (max (~-smin' mod m) smax') mod m)
          ~size

let srem t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let abs2 = abs t2 in
    let min_abs2 = unsigned_min abs2 in
    let max_abs2 = unsigned_max abs2 in
    if Bitvec.(max_abs2 = zero) then create_empty ~size
    else
      let min_abs2 =
        if Bitvec.(min_abs2 = zero) then Bitvec.(succ min_abs2 mod m)
        else min_abs2
      in
      let min1 = signed_min t1 in
      let max1 = signed_max t1 in
      if Bitvec.(signed_compare min1 zero m) >= 0 then
        if Bitvec.(max1 < min_abs2) then t1
        else
          create ~lo:Bitvec.zero
            ~hi:Bitvec.(succ (min max1 (pred max_abs2 mod m)) mod m)
            ~size
      else if Bitvec.(signed_compare max1 zero m) < 0 then
        if Bitvec.(min1 < ~-min_abs2 mod m) then t1
        else
          create
            ~lo:Bitvec.(max min1 (succ (~-max_abs2 mod m) mod m))
            ~hi:Bitvec.one ~size
      else
        create
          ~lo:Bitvec.(max min1 (succ (~-max_abs2 mod m) mod m))
          ~hi:Bitvec.(succ (min max1 (pred max_abs2 mod m)) mod m)
          ~size

let lnot t =
  let size = t.size in
  if is_empty t then t
  else if is_wrapped t then create_full ~size
  else sub (create_single ~value:Bitvec.(max_unsigned_value t.size) ~size) t

let neg t =
  if is_empty t then t
  else sub (create_single ~value:Bitvec.zero ~size:t.size) t

let ( land ) t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    match (single_of t1, single_of t2) with
    | Some v1, Some v2 ->
        create_single ~value:Bitvec.(v1 land v2 mod m) ~size
    | _ ->
        create_non_empty ~lo:Bitvec.zero
          ~hi:Bitvec.(succ (min (unsigned_max t2) (unsigned_max t1)) mod m)
          ~size

let ( lor ) t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    match (single_of t1, single_of t2) with
    | Some v1, Some v2 -> create_single ~value:Bitvec.(v1 lor v2 mod m) ~size
    | _ ->
        create_non_empty
          ~lo:Bitvec.(succ (max (unsigned_min t2) (unsigned_min t1)) mod m)
          ~hi:Bitvec.zero ~size

let ( lxor ) t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    match (single_of t1, single_of t2) with
    | Some v1, Some v2 ->
        create_single ~value:Bitvec.(v1 lxor v2 mod m) ~size
    | None, Some v when Bitvec.(v = max_unsigned_value size) -> lnot t1
    | Some v, None when Bitvec.(v = max_unsigned_value size) -> lnot t2
    | _ -> create_full ~size

let ( lsl ) t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let max1 = unsigned_max t1 in
    let max2 = unsigned_max t2 in
    if Bitvec.(max2 = zero) then t1
    else if Bitvec.(max2 > int (clz max1 size) mod m) then create_full ~size
    else
      let min1 = unsigned_min t1 in
      let min2 = unsigned_min t2 in
      let lo = Bitvec.((min1 lsl min2) mod m) in
      let hi = Bitvec.(succ ((max1 lsl max2) mod m) mod m) in
      create ~lo ~hi ~size

let ( lsr ) t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let max1 = unsigned_max t1 in
    let max2 = unsigned_max t2 in
    let min1 = unsigned_min t1 in
    let min2 = unsigned_min t2 in
    let lo = Bitvec.(succ ((max1 lsr min2) mod m) mod m) in
    let hi = Bitvec.((min1 lsr max2) mod m) in
    create ~lo ~hi ~size

let ( asr ) t1 t2 =
  assert (t1.size = t2.size);
  let size = t1.size in
  let m = Bitvec.modulus size in
  if is_empty t1 || is_empty t2 then create_empty ~size
  else
    let max1 = signed_max t1 in
    let max2 = signed_max t2 in
    let min1 = signed_min t1 in
    let min2 = signed_min t2 in
    let pos_max = Bitvec.(succ ((max1 asr min2) mod m) mod m) in
    let pos_min = Bitvec.((min1 asr max2) mod m) in
    let neg_max = Bitvec.(succ ((max1 asr max2) mod m) mod m) in
    let neg_min = Bitvec.((min1 asr min2) mod m) in
    let lo, hi =
      if Bitvec.(signed_compare min1 zero m) >= 0 then (pos_min, pos_max)
      else if Bitvec.(signed_compare max1 zero m) < 0 then (neg_min, neg_max)
      else (neg_min, pos_max)
    in
    create_non_empty ~lo ~hi ~size

let extract t ~hi ~lo =
  let size = t.size in
  assert (lo >= 0 && hi < size);
  let s = hi - lo + 1 in
  if s = size then t
  else
    let m = Bitvec.modulus size in
    let t' =
      if lo = 0 then t
      else t lsr create_single ~value:Bitvec.(int lo mod m) ~size
    in
    trunc t' ~size:s

let concat t1 t2 =
  let s1 = t1.size in
  let s2 = t2.size in
  let size = s1 + s2 in
  let t1' = zext t1 ~size in
  let t2' = zext t2 ~size in
  let t1' =
    t1' lsl create_single ~value:Bitvec.(int s2 mod modulus size) ~size
  in
  add t1' t2'

let to_string t =
  if equal t boolean_false then "false"
  else if equal t boolean_true then "true"
  else if equal t boolean_full then "true/false"
  else if is_full t then Printf.sprintf "full:%d" t.size
  else if is_empty t then Printf.sprintf "empty:%d" t.size
  else
    match single_of t with
    | Some v -> Printf.sprintf "0x%s:%d" (Bitvec.to_string v) t.size
    | None ->
        Printf.sprintf "[0x%s, 0x%s):%d" (Bitvec.to_string t.lo)
          (Bitvec.to_string t.hi) t.size

let ( + ) t1 t2 = add t1 t2 [@@inline]

let ( - ) t1 t2 = sub t1 t2 [@@inline]

let ( * ) t1 t2 = mul t1 t2 [@@inline]

let ( / ) t1 t2 = udiv t1 t2 [@@inline]

let ( /$ ) t1 t2 = sdiv t1 t2 [@@inline]

let ( % ) t1 t2 = urem t1 t2 [@@inline]

let ( %^ ) t1 t2 = srem t1 t2 [@@inline]

let ( ~~ ) t = lnot t [@@inline]

let ( ~- ) t = neg t [@@inline]

let ( & ) t1 t2 = t1 land t2 [@@inline]

let ( ^ ) t1 t2 = t1 lxor t2 [@@inline]

let ( << ) t1 t2 = t1 lsl t2 [@@inline]

let ( >> ) t1 t2 = t1 lsr t2 [@@inline]

let ( >>> ) t1 t2 = t1 asr t2 [@@inline]
