open Core_kernel

type error = [`BadAddr | `BadPerms | `BadAccess | `BadLength]

let string_of_error = function
  | `BadAddr -> "bad address"
  | `BadPerms -> "bad permissions"
  | `BadAccess -> "bad access"
  | `BadLength -> "bad length"

module Env = struct
  type t = Bitvec.t Addr.Map.t

  let empty = Addr.Map.empty

  let put env ~endian ~addr ~bytes ~value =
    let reorder l =
      match endian with
      | `LE -> l
      | `BE -> List.rev l
    in
    List.init bytes ~f:(fun i ->
        let lo = i lsl 3 in
        let hi = lo + 7 in
        Bitvec.extract value ~hi ~lo )
    |> reorder
    |> List.foldi ~init:env ~f:(fun i env data ->
           let key = Addr.(addr + int i) in
           Map.set env ~key ~data )

  let get env ~endian ~addr ~bytes =
    let open Option.Let_syntax in
    let rec aux i res =
      if i >= bytes then
        match endian with
        | `BE -> return (List.rev res)
        | `LE -> return res
      else
        let%bind v = Map.find env Addr.(addr + int i) in
        aux (succ i) (v :: res)
    in
    let%map l = aux 0 [] in
    Bitvec.concat 8 l

  let to_alist env = Map.to_alist env [@@inline]

  let iter = Map.iteri

  let fold = Map.fold

  let map = Map.mapi
end

module Segment = struct
  type t =
    {addr: Addr.t; data: Bigstring.t; len: int; r: bool; w: bool; x: bool}

  let to_bytes seg =
    (* use at your own risk, the segment may be
     * larger than the OCaml runtime will allow *)
    Bigstring.to_bytes seg.data
    [@@inline]

  let length seg = seg.len [@@inline]

  let address seg = seg.addr [@@inline]

  let end_address seg = Addr.(seg.addr + int (length seg)) [@@inline]

  let last_address seg = Addr.(pred (end_address seg)) [@@inline]

  let within seg addr =
    Addr.(addr >= seg.addr && addr <= last_address seg)
    [@@inline]

  let is_readable seg = seg.r [@@inline]

  let is_writable seg = seg.w [@@inline]

  let is_executable seg = seg.x [@@inline]

  let validate_addr seg addr =
    Result.ok_if_true ~error:`BadAddr (within seg addr)
    [@@inline]

  let validate_access seg addr len =
    Result.ok_if_true ~error:`BadAccess
      Addr.(addr >= seg.addr && addr + int len <= end_address seg)
    [@@inline]

  let validate_read seg = Result.ok_if_true ~error:`BadPerms seg.r [@@inline]

  let validate_write seg =
    Result.ok_if_true ~error:`BadPerms seg.w
    [@@inline]

  let read_byte seg addr =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%map _ = validate_read seg in
    let pos = Addr.(to_int (addr - seg.addr)) in
    Bigstring.get seg.data pos |> Char.to_int

  let read_bytes seg addr len =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%bind _ = validate_write seg in
    (* truncate len if necessary *)
    let diff = Addr.(to_int (end_address seg - addr)) in
    let len = min len diff in
    if len <= 0 then Error `BadLength
    else
      let%map _ = validate_access seg addr len in
      let pos = Addr.(to_int (addr - seg.addr)) in
      Bigstring.to_bytes seg.data ~pos ~len

  let read_int16 seg addr endian =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%bind _ = validate_read seg in
    let%map _ = validate_access seg addr 2 in
    let pos = Addr.(to_int (addr - seg.addr)) in
    match endian with
    | `LE -> Bigstring.get_uint16_le seg.data ~pos
    | `BE -> Bigstring.get_uint16_be seg.data ~pos

  let read_int32 seg addr endian =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%bind _ = validate_read seg in
    let%map _ = validate_access seg addr 4 in
    let pos = Addr.(to_int (addr - seg.addr)) in
    match endian with
    | `LE -> Bigstring.get_int32_t_le seg.data ~pos
    | `BE -> Bigstring.get_int32_t_be seg.data ~pos

  let read_int64 seg addr endian =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%bind _ = validate_read seg in
    let%map _ = validate_access seg addr 8 in
    let pos = Addr.(to_int (addr - seg.addr)) in
    match endian with
    | `LE -> Bigstring.get_int64_t_le seg.data ~pos
    | `BE -> Bigstring.get_int64_t_be seg.data ~pos

  let write_byte seg addr b =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%map _ = validate_write seg in
    let pos = Addr.(to_int (addr - seg.addr)) in
    Bigstring.set_uint8_exn seg.data pos (b land 0xFF)

  let write_bytes seg addr b =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%bind _ = validate_write seg in
    (* truncate len if necessary *)
    let diff = Addr.(to_int (end_address seg - addr)) in
    let len = min Bytes.(length b) diff in
    if len <= 0 then Error `BadLength
    else
      let%map _ = validate_access seg addr len in
      let pos = Addr.(to_int (addr - seg.addr)) in
      for i = 0 to len - 1 do
        Bigstring.set seg.data (pos + i) Bytes.(get b i)
      done

  let write_int16 seg addr v endian =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%bind _ = validate_write seg in
    let%map _ = validate_access seg addr 2 in
    let pos = Addr.(to_int (addr - seg.addr)) in
    match endian with
    | `LE -> Bigstring.set_uint16_le_exn seg.data ~pos v
    | `BE -> Bigstring.set_uint16_le_exn seg.data ~pos v

  let write_int32 seg addr v endian =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%bind _ = validate_write seg in
    let%map _ = validate_access seg addr 4 in
    let pos = Addr.(to_int (addr - seg.addr)) in
    match endian with
    | `LE -> Bigstring.set_int32_t_le seg.data ~pos v
    | `BE -> Bigstring.set_int32_t_be seg.data ~pos v

  let write_int64 seg addr v endian =
    let open Result.Let_syntax in
    let%bind _ = validate_addr seg addr in
    let%bind _ = validate_write seg in
    let%map _ = validate_access seg addr 8 in
    let pos = Addr.(to_int (addr - seg.addr)) in
    match endian with
    | `LE -> Bigstring.set_int64_t_le seg.data ~pos v
    | `BE -> Bigstring.set_int64_t_be seg.data ~pos v
end

type t = {mutable segments: Segment.t Addr.Map.t}

let create () = {segments= Addr.Map.empty} [@@inline]

let make_env mem =
  let module B = Bitvec.Make (struct
    let modulus = Bitvec.modulus 8
  end) in
  Map.fold mem.segments ~init:Env.empty ~f:(fun ~key ~data env ->
      (* segment must be readable *)
      if not data.r then env
      else
        Bigstring.to_bytes data.data
        |> Bytes.foldi ~init:env ~f:(fun i env c ->
               (* we're adding one byte at a time since we
                * can't assume a uniform endianness *)
               let v = Char.to_int c |> B.int in
               Env.put env `LE Addr.(key + int i) 1 v ) )

let num_segments mem = Map.length mem.segments [@@inline]

let segments mem = Map.to_alist mem.segments |> List.map ~f:snd [@@inline]

let iter mem ~f =
  Map.iteri mem.segments ~f:(fun ~key ~data -> f key data)
  [@@inline]

let fold mem ~init ~f =
  Map.fold mem.segments ~init ~f:(fun ~key ~data acc -> f acc key data)
  [@@inline]

let address_ranges mem =
  let f (base, seg) = (base, Segment.end_address seg) in
  Map.to_alist mem.segments |> List.map ~f
  [@@inline]

let total_length mem =
  let f len seg = len + Segment.length seg in
  segments mem |> List.fold ~init:0 ~f
  [@@inline]

let find_segment mem addr =
  let open Option.Let_syntax in
  let%bind _, seg = Map.closest_key mem.segments `Less_or_equal_to addr in
  Option.some_if (Segment.within seg addr) seg

(* 1. we assume flat addressing
 * 2. segments are not allowed to overlap *)
let is_bad_chunk mem addr len =
  let last = Addr.(pred (addr + int len)) in
  match (find_segment mem addr, find_segment mem last) with
  | Some _, _ | _, Some _ -> true
  | _ -> false

let add_segment mem addr len ~r ~w ~x =
  let open Result.Let_syntax in
  let%bind _ = Result.ok_if_true ~error:`BadPerms (r || w || x) in
  let%map _ =
    Result.ok_if_true ~error:`BadAddr (not (is_bad_chunk mem addr len))
  in
  let data = Bigstring.init len ~f:(fun _ -> '\x00') in
  let seg = Segment.{addr; data; len; r; w; x} in
  mem.segments <- Map.set mem.segments ~key:addr ~data:seg

let add_segment_from_bigstring mem addr data ~r ~w ~x =
  let open Result.Let_syntax in
  let%bind _ = Result.ok_if_true ~error:`BadPerms (r || w || x) in
  let len = Bigstring.length data in
  let%map _ =
    Result.ok_if_true ~error:`BadAddr (not (is_bad_chunk mem addr len))
  in
  let seg = Segment.{addr; data; len; r; w; x} in
  mem.segments <- Map.set mem.segments ~key:addr ~data:seg

let seg_for_read mem addr =
  match find_segment mem addr with
  | Some seg -> if Segment.is_readable seg then Ok seg else Error `BadPerms
  | None -> Error `BadAddr

let read_byte mem addr =
  let open Result.Let_syntax in
  let%map seg = seg_for_read mem addr in
  let pos = Addr.(to_int (addr - seg.addr)) in
  Bigstring.get seg.data pos |> Char.to_int

let read_bytes mem addr len =
  let open Result.Let_syntax in
  let%bind seg = seg_for_read mem addr in
  (* truncate len if necessary *)
  let diff = Addr.(to_int (Segment.end_address seg - addr)) in
  let len = min len diff in
  if len <= 0 then Error `BadLength
  else
    let%map _ = Segment.validate_access seg addr len in
    let pos = Addr.(to_int (addr - seg.addr)) in
    Bigstring.to_bytes seg.data ~pos ~len

let read_int16 mem addr endian =
  let open Result.Let_syntax in
  let%bind seg = seg_for_read mem addr in
  let%map _ = Segment.validate_access seg addr 2 in
  let pos = Addr.(to_int (addr - seg.addr)) in
  match endian with
  | `LE -> Bigstring.get_uint16_le seg.data ~pos
  | `BE -> Bigstring.get_uint16_be seg.data ~pos

let read_int32 mem addr endian =
  let open Result.Let_syntax in
  let%bind seg = seg_for_read mem addr in
  let%map _ = Segment.validate_access seg addr 4 in
  let pos = Addr.(to_int (addr - seg.addr)) in
  match endian with
  | `LE -> Bigstring.get_int32_t_le seg.data ~pos
  | `BE -> Bigstring.get_int32_t_be seg.data ~pos

let read_int64 mem addr endian =
  let open Result.Let_syntax in
  let%bind seg = seg_for_read mem addr in
  let%map _ = Segment.validate_access seg addr 8 in
  let pos = Addr.(to_int (addr - seg.addr)) in
  match endian with
  | `LE -> Bigstring.get_int64_t_le seg.data ~pos
  | `BE -> Bigstring.get_int64_t_be seg.data ~pos

let seg_for_write mem addr =
  match find_segment mem addr with
  | Some seg -> if Segment.is_writable seg then Ok seg else Error `BadPerms
  | None -> Error `BadAddr

let write_byte mem addr b =
  let open Result.Let_syntax in
  let%map seg = seg_for_write mem addr in
  let pos = Addr.(to_int (addr - seg.addr)) in
  Bigstring.set_uint8_exn seg.data pos (b land 0xFF)

let write_bytes mem addr b =
  let open Result.Let_syntax in
  let%bind seg = seg_for_write mem addr in
  (* truncate len if necessary *)
  let diff = Addr.(to_int (Segment.end_address seg - addr)) in
  let len = min Bytes.(length b) diff in
  if len <= 0 then Error `BadLength
  else
    let%map _ = Segment.validate_access seg addr len in
    let pos = Addr.(to_int (addr - seg.addr)) in
    for i = 0 to len - 1 do
      Bigstring.set seg.data (pos + i) Bytes.(get b i)
    done

let write_int16 mem addr v endian =
  let open Result.Let_syntax in
  let%bind seg = seg_for_write mem addr in
  let%map _ = Segment.validate_access seg addr 2 in
  let pos = Addr.(to_int (addr - seg.addr)) in
  match endian with
  | `LE -> Bigstring.set_uint16_le_exn seg.data ~pos v
  | `BE -> Bigstring.set_uint16_le_exn seg.data ~pos v

let write_int32 mem addr v endian =
  let open Result.Let_syntax in
  let%bind seg = seg_for_write mem addr in
  let%map _ = Segment.validate_access seg addr 4 in
  let pos = Addr.(to_int (addr - seg.addr)) in
  match endian with
  | `LE -> Bigstring.set_int32_t_le seg.data ~pos v
  | `BE -> Bigstring.set_int32_t_be seg.data ~pos v

let write_int64 mem addr v endian =
  let open Result.Let_syntax in
  let%bind seg = seg_for_write mem addr in
  let%map _ = Segment.validate_access seg addr 8 in
  let pos = Addr.(to_int (addr - seg.addr)) in
  match endian with
  | `LE -> Bigstring.set_int64_t_le seg.data ~pos v
  | `BE -> Bigstring.set_int64_t_be seg.data ~pos v
