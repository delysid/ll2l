open Core_kernel

type t = [`LE | `BE] [@@deriving equal, compare, sexp]

let to_string = function
  | `LE -> "le"
  | `BE -> "be"

let of_string = function
  | "le" -> Some `LE
  | "be" -> Some `BE
  | _ -> None
