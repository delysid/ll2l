(* this module is borrowed + extended from BAP *)

open Core_kernel
module A = Option_array

type 'a t = {mutable size: int; mutable data: 'a A.t} [@@deriving sexp]

let default_capacity = 16

let create ?(capacity = default_capacity) () =
  let len = Int.(ceil_pow2 (max capacity 1)) in
  {size= 0; data= A.create ~len}

let capacity vec = A.length vec.data [@@inline]

let copy vec = {size= vec.size; data= A.copy vec.data} [@@inline]

(* if the new size is negative then that's a bug, so we should catch it ASAP *)
let set_size vec n =
  assert (n >= 0);
  vec.size <- max n 0
  [@@inline]

let clear vec =
  let n = vec.size - 1 in
  for i = 0 to n do
    (* it's important for us to clear these cells so that
     * the GC can do its job sooner rather than later *)
    A.unsafe_set_none vec.data i
  done;
  set_size vec 0

let append vec vec' =
  if vec'.size <= 0 then ()
  else
    let new_size = vec.size + vec'.size in
    let new_capacity = Int.(ceil_pow2 (max new_size 1)) in
    let data =
      A.init new_capacity ~f:(fun i ->
          if i < vec.size then A.unsafe_get vec.data i
          else
            let i' = i - vec.size in
            if i' < vec'.size then A.unsafe_get vec'.data i' else None )
    in
    vec.data <- data;
    set_size vec new_size

let grow vec =
  let n = capacity vec in
  let data =
    A.init (vec.size * 2) ~f:(fun i ->
        if i < n then A.unsafe_get vec.data i else None )
  in
  vec.data <- data

let push_back vec x =
  if capacity vec = vec.size then grow vec;
  A.unsafe_set_some vec.data vec.size x;
  set_size vec (vec.size + 1)

let insert vec n x =
  if n = vec.size then Some (push_back vec x)
  else if n >= 0 && n < vec.size then (
    if capacity vec = vec.size then grow vec;
    ( if n < vec.size then
      let rec loop i =
        if i <= n then ()
        else
          let i' = i - 1 in
          let x = A.unsafe_get_some_exn vec.data i' in
          A.unsafe_set_some vec.data i x;
          loop i'
      in
      loop vec.size );
    A.unsafe_set_some vec.data n x;
    Some (set_size vec (vec.size + 1)) )
  else None

let insert_exn vec n x =
  match insert vec n x with
  | Some () -> ()
  | None -> invalid_arg "index out of bounds"

let remove vec n =
  if n >= 0 && n < vec.size then (
    let rec loop i =
      let i' = i + 1 in
      if i' >= vec.size then ()
      else
        let x = A.unsafe_get_some_exn vec.data i' in
        A.unsafe_set_some vec.data i x;
        loop i'
    in
    loop n;
    A.unsafe_set_none vec.data (vec.size - 1);
    Some (set_size vec (vec.size - 1)) )
  else None

let remove_exn vec n =
  match remove vec n with
  | Some () -> ()
  | None -> invalid_arg "index out of bounds"

let cut_back vec n =
  if n <= 0 then ()
  else
    let n = min n vec.size in
    let rec loop i =
      if i >= vec.size then ()
      else (
        A.unsafe_set_none vec.data i;
        loop (i + 1) )
    in
    loop (vec.size - n);
    set_size vec (vec.size - n)

let pop_back vec =
  if vec.size > 0 then (
    let new_size = vec.size - 1 in
    A.set_none vec.data new_size;
    Some (set_size vec new_size) )
  else None

let pop_back_exn vec =
  match pop_back vec with
  | Some () -> ()
  | None -> invalid_arg "index out of bounds"

let get vec n =
  if n >= 0 && n < vec.size then Some (A.unsafe_get_some_exn vec.data n)
  else None

let get_exn vec n =
  if n >= 0 && n < vec.size then A.unsafe_get_some_exn vec.data n
  else invalid_arg "index out of bounds"

let back vec = get vec (vec.size - 1) [@@inline]

let back_exn vec = get_exn vec (vec.size - 1) [@@inline]

let set vec n x =
  if n >= 0 && n < vec.size then Some (A.unsafe_set_some vec.data n x)
  else None

let set_exn vec n x =
  if n >= 0 && n < vec.size then A.unsafe_set_some vec.data n x
  else invalid_arg "index out of bounds"

let set_back vec x = set vec (vec.size - 1) x [@@inline]

let set_back_exn vec x = set_exn vec (vec.size - 1) x [@@inline]

include struct
  open Container.Continue_or_stop

  let fold_until vec ~init ~f ~finish =
    let rec loop i init =
      if i < vec.size then
        match f init (A.unsafe_get_some_exn vec.data i) with
        | Stop x -> x
        | Continue r -> loop (i + 1) r
      else finish init
    in
    loop 0 init

  let fold_result vec ~init ~f =
    let rec loop i init =
      if i < vec.size then
        match f init (A.unsafe_get_some_exn vec.data i) with
        | Error _ as e -> e
        | Ok r -> loop (i + 1) r
      else Ok init
    in
    loop 0 init
end

let foldi vec ~init ~f =
  let rec loop i init =
    if i < vec.size then
      loop (i + 1) (f i init (A.unsafe_get_some_exn vec.data i))
    else init
  in
  loop 0 init

let fold vec ~init ~f = foldi vec ~init ~f:(fun _ acc x -> f acc x)

let iteri vec ~f =
  let rec loop i =
    if i < vec.size then (
      f i (A.unsafe_get_some_exn vec.data i);
      loop (i + 1) )
  in
  loop 0

let iter vec ~f = iteri vec ~f:(fun _ x -> f x)

let find_mapi vec ~f =
  with_return (fun {return} ->
      iteri vec ~f:(fun i x ->
          match f i x with
          | None -> ()
          | hay -> return hay );
      None )

let findi vec ~f =
  with_return (fun {return} ->
      iteri vec ~f:(fun i x -> if f i x then return (Some (i, x)));
      None )

let peq = Poly.equal

let index_with ?(equal = peq) ~default vec x : int =
  with_return (fun {return} ->
      iteri vec ~f:(fun i y -> if equal x y then return i);
      default )

let index ?equal vec x : int option =
  let n = index_with ~default:(-1) ?equal vec x in
  if n < 0 then None else Some n

let index_exn ?equal vec x : int =
  let n = index_with ~default:(-1) ?equal vec x in
  if n < 0 then raise Caml.Not_found else n

let length vec = vec.size [@@inline]

module C = Container.Make (struct
  type nonrec 'a t = 'a t

  let fold = fold

  let iter = `Custom iter

  let length = `Custom length
end)

let mem = C.mem

let is_empty vec = length vec = 0 [@@inline]

let exists = C.exists

let for_all = C.for_all

let count = C.count

let sum = C.sum

let find = C.find

let find_map = C.find_map

let min_elt = C.min_elt

let max_elt = C.max_elt

let of_array arr =
  let size = Array.length arr in
  let len = max Int.(ceil_pow2 (max size 1)) default_capacity in
  let data =
    A.init len ~f:(fun i ->
        if i < size then Some (Array.unsafe_get arr i) else None )
  in
  {size; data}

let to_array vec =
  Array.init vec.size ~f:(fun i -> A.unsafe_get_some_exn vec.data i)

let map_to_array vec ~f =
  Array.init vec.size ~f:(fun i -> f (A.unsafe_get_some_exn vec.data i))

let of_list l =
  let vec = create () in
  List.iter l ~f:(push_back vec);
  vec

let to_list vec =
  List.init vec.size (fun i -> A.unsafe_get_some_exn vec.data i)
  [@@inline]

let filteri vec ~f =
  let n = length vec in
  let module B = Bitvec.Make (struct
    let modulus = Bitvec.modulus n
  end) in
  let bs = ref B.zero in
  for i = 0 to n - 1 do
    if f i (A.unsafe_get_some_exn vec.data i) then bs := Bitvec.setbit !bs i
  done;
  let bs = !bs in
  let n' = Bitvec.popcount bs in
  let new_capacity = Int.(ceil_pow2 (max n' 1)) in
  let data =
    let rec next_bit i =
      if i >= n then None else if B.nth bs i then Some i else next_bit (i + 1)
    in
    let j = ref 0 in
    A.init new_capacity ~f:(fun _ ->
        Option.(
          next_bit !j
          >>= fun i ->
          j := i + 1;
          A.unsafe_get vec.data i) )
  in
  {size= n'; data}

let filter vec ~f = filteri vec ~f:(fun _ x -> f x) [@@inline]

let filteri_inplace vec ~f =
  let n = length vec in
  let module B = Bitvec.Make (struct
    let modulus = Bitvec.modulus n
  end) in
  let bs = ref B.zero in
  for i = 0 to n - 1 do
    if f i (A.unsafe_get_some_exn vec.data i) then bs := Bitvec.setbit !bs i
  done;
  let bs = !bs in
  let n' = Bitvec.popcount bs in
  let j = ref 0 in
  (* move all the elements which satisfy `f` to the beginning *)
  for i = 0 to n' - 1 do
    if !j < n then (
      while !j < n && B.nth bs !j |> not do
        incr j
      done;
      if !j < n then (
        A.(unsafe_set_some vec.data i (unsafe_get_some_exn vec.data !j));
        incr j ) )
  done;
  (* invalidate everything after *)
  for i = n' to n - 1 do
    A.unsafe_set_none vec.data i
  done;
  set_size vec n'

let filter_inplace vec ~f =
  filteri_inplace vec ~f:(fun _ x -> f x)
  [@@inline]

let filter_mapi vec ~f =
  let new_vec = create () in
  iteri vec ~f:(fun i x -> Option.iter (f i x) ~f:(push_back new_vec));
  new_vec

let filter_map vec ~f = filter_mapi vec ~f:(fun _ x -> f x) [@@inline]

let swap vec i j =
  let open Option.Let_syntax in
  let%bind tmp = get vec i in
  let%bind j' = get vec j in
  let%bind _ = set vec i j' in
  set vec j tmp

let swap_exn vec i j =
  let tmp = get_exn vec i in
  set_exn vec i (get_exn vec j);
  set_exn vec j tmp

let rev_inplace vec =
  let i = ref 0 in
  let j = ref (length vec - 1) in
  while !i < !j do
    swap_exn vec !i !j; incr i; decr j
  done

let rev vec =
  let vec' = copy vec in
  rev_inplace vec'; vec'

(* this is taken from the Array module in Jane Street's Base library *)

module Sort = struct
  (* For the sake of speed we could use unsafe get/set throughout, but speed
     tests don't show a significant improvement. *)
  let get = A.get_some_exn

  let set = A.set_some

  let swap arr i j =
    let tmp = get arr i in
    set arr i (get arr j);
    set arr j tmp

  module type Sort = sig
    val sort :
         'a A.t
      -> compare:('a -> 'a -> int)
      -> left:int (* leftmost index of sub-array to sort *)
      -> right:int (* rightmost index of sub-array to sort *)
      -> unit
  end

  (* http://en.wikipedia.org/wiki/Insertion_sort *)
  module Insertion_sort : Sort = struct
    let sort arr ~compare ~left ~right =
      (* loop invariant: [arr] is sorted from [left] to [pos - 1], inclusive *)
      for pos = left + 1 to right do
        (* loop invariants: 1. the subarray arr[left .. i-1] is sorted 2. the
           subarray arr[i+1 .. pos] is sorted and contains only elements > v
           3. arr[i] may be thought of as containing v Note that this does
           not allocate a closure, but is left in the for loop for the
           readability of the documentation. *)
        let rec loop arr ~left ~compare i v =
          let i_next = i - 1 in
          if i_next >= left && compare (get arr i_next) v > 0 then (
            set arr i (get arr i_next);
            loop arr ~left ~compare i_next v )
          else i
        in
        let v = get arr pos in
        let final_pos = loop arr ~left ~compare pos v in
        set arr final_pos v
      done
  end

  (* http://en.wikipedia.org/wiki/Heapsort *)
  module Heap_sort : Sort = struct
    (* loop invariant: root's children are both either roots of max-heaps or
       > right *)
    let rec heapify arr ~compare root ~left ~right =
      let relative_root = root - left in
      let left_child = (2 * relative_root) + left + 1 in
      let right_child = (2 * relative_root) + left + 2 in
      let largest =
        if
          left_child <= right
          && compare (get arr left_child) (get arr root) > 0
        then left_child
        else root
      in
      let largest =
        if
          right_child <= right
          && compare (get arr right_child) (get arr largest) > 0
        then right_child
        else largest
      in
      if largest <> root then (
        swap arr root largest;
        heapify arr ~compare largest ~left ~right )

    let build_heap arr ~compare ~left ~right =
      (* Elements in the second half of the array are already heaps of size
         1. We move through the first half of the array from back to front
         examining the element at hand, and the left and right children,
         fixing the heap property as we go. *)
      for i = (left + right) / 2 downto left do
        heapify arr ~compare i ~left ~right
      done

    let sort arr ~compare ~left ~right =
      build_heap arr ~compare ~left ~right;
      (* loop invariants: 1. the subarray arr[left ... i] is a max-heap H 2.
         the subarray arr[i+1 ... right] is sorted (call it S) 3. every
         element of H is less than every element of S *)
      for i = right downto left + 1 do
        swap arr left i;
        heapify arr ~compare left ~left ~right:(i - 1)
      done
  end

  (* http://en.wikipedia.org/wiki/Introsort *)
  module Intro_sort : sig
    include Sort

    val five_element_sort :
         'a A.t
      -> compare:('a -> 'a -> int)
      -> int
      -> int
      -> int
      -> int
      -> int
      -> unit
  end = struct
    let five_element_sort arr ~compare m1 m2 m3 m4 m5 =
      let compare_and_swap i j =
        if compare (get arr i) (get arr j) > 0 then swap arr i j
      in
      (* Optimal 5-element sorting network: {v
         1--o-----o-----o--------------1 | | |
         2--o-----|--o--|-----o--o-----2 | | | | |
         3--------o--o--|--o--|--o-----3 | | |
         4-----o--------o--o--|-----o--4 | | |
         5-----o--------------o-----o--5 v} *)
      compare_and_swap m1 m2;
      compare_and_swap m4 m5;
      compare_and_swap m1 m3;
      compare_and_swap m2 m3;
      compare_and_swap m1 m4;
      compare_and_swap m3 m4;
      compare_and_swap m2 m5;
      compare_and_swap m2 m3;
      compare_and_swap m4 m5

    (* choose pivots for the array by sorting 5 elements and examining the
       center three elements. The goal is to choose two pivots that will
       either: - break the range up into 3 even partitions or - eliminate a
       commonly appearing element by sorting it into the center partition by
       itself To this end we look at the center 3 elements of the 5 and
       return pairs of equal elements or the widest range *)
    let choose_pivots arr ~compare ~left ~right =
      let sixth = (right - left) / 6 in
      let m1 = left + sixth in
      let m2 = m1 + sixth in
      let m3 = m2 + sixth in
      let m4 = m3 + sixth in
      let m5 = m4 + sixth in
      five_element_sort arr ~compare m1 m2 m3 m4 m5;
      let m2_val = get arr m2 in
      let m3_val = get arr m3 in
      let m4_val = get arr m4 in
      if compare m2_val m3_val = 0 then (m2_val, m3_val, true)
      else if compare m3_val m4_val = 0 then (m3_val, m4_val, true)
      else (m2_val, m4_val, false)

    let dual_pivot_partition arr ~compare ~left ~right =
      let pivot1, pivot2, pivots_equal =
        choose_pivots arr ~compare ~left ~right
      in
      (* loop invariants: 1. left <= l < r <= right 2. l <= p <= r 3. l <= x
         < p implies arr[x] >= pivot1 and arr[x] <= pivot2 4. left <= x < l
         implies arr[x] < pivot1 5. r < x <= right implies arr[x] > pivot2 *)
      let rec loop l p r =
        let pv = get arr p in
        if compare pv pivot1 < 0 then (
          swap arr p l;
          cont (l + 1) (p + 1) r )
        else if compare pv pivot2 > 0 then (
          (* loop invariants: same as those of the outer loop *)
          let rec scan_backwards r =
            if r > p && compare (get arr r) pivot2 > 0 then
              scan_backwards (r - 1)
            else r
          in
          let r = scan_backwards r in
          swap arr r p;
          cont l p (r - 1) )
        else cont l (p + 1) r
      and cont l p r = if p > r then (l, r) else loop l p r in
      let l, r = cont left left right in
      (l, r, pivots_equal)

    let rec intro_sort arr ~max_depth ~compare ~left ~right =
      let len = right - left + 1 in
      (* This takes care of some edge cases, such as left > right or very
         short arrays, since Insertion_sort.sort handles these cases
         properly. Thus we don't need to make sure that left and right are
         valid in recursive calls. *)
      if len <= 32 then Insertion_sort.sort arr ~compare ~left ~right
      else if max_depth < 0 then Heap_sort.sort arr ~compare ~left ~right
      else
        let max_depth = max_depth - 1 in
        let l, r, middle_sorted =
          dual_pivot_partition arr ~compare ~left ~right
        in
        intro_sort arr ~max_depth ~compare ~left ~right:(l - 1);
        if not middle_sorted then
          intro_sort arr ~max_depth ~compare ~left:l ~right:r;
        intro_sort arr ~max_depth ~compare ~left:(r + 1) ~right

    let log10_of_3 = Caml.log10 3.

    let log3 x = Caml.log10 x /. log10_of_3

    let sort arr ~compare ~left ~right =
      let len = right - left + 1 in
      (* XXX: the original code doesn't have this check, but there have
       * been some cases when len = 0 and we get a NaN exception *)
      if len > 0 then
        let heap_sort_switch_depth =
          (* with perfect 3-way partitioning, this is the recursion depth *)
          Int.of_float (log3 (Int.to_float len))
        in
        intro_sort arr ~max_depth:heap_sort_switch_depth ~compare ~left
          ~right
  end
end

let sort ?pos ?len vec ~compare =
  let pos, len =
    Ordered_collection_common.get_pos_len_exn () ?pos ?len
      ~total_length:(length vec)
  in
  Sort.Intro_sort.sort vec.data ~compare ~left:pos ~right:(pos + len - 1)
