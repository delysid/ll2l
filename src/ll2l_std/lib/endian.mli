open Core_kernel

type t = [`LE | `BE] [@@deriving equal, compare, sexp]

val to_string : t -> string

val of_string : string -> t option
