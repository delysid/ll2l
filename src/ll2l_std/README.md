# `ll2l-std`

This library acts as the "standard library" for `ll2l`.
It provides a variety of data structures and algorithms for general use, such as graphs, bitvectors, dynamically-sized arrays, integer interval arithmetic, and so on.

Several of these modules are adapted from other projects, such as [BAP](https://github.com/BinaryAnalysisPlatform/bap) and [LLVM](https://llvm.org/).
