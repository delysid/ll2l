open Core_kernel
open Stdio
open Ll2l_std
module G = Unordered_graph.Make (String) (Int)

let edges =
  [ ("start", 5, "A")
  ; ("start", 2, "B")
  ; ("A", 4, "C")
  ; ("A", 2, "D")
  ; ("B", 8, "A")
  ; ("B", 7, "D")
  ; ("C", 6, "D")
  ; ("C", 3, "finish")
  ; ("D", 1, "finish") ]

let print_edges edges =
  printf "{%s}\n"
    ( String.concat ~sep:", " @@ List.concat
    @@ List.map edges ~f:(fun (v, es) ->
           List.map es ~f:(fun e -> "(" ^ v ^ ", " ^ Int.to_string e ^ ")"))
    )

let print_graph g =
  let vs = G.vertices g in
  List.iter vs ~f:(fun v ->
      printf "\"%s\":\n" v;
      let succs = G.successors g v in
      printf "  succs: ";
      print_edges succs;
      let preds = G.predecessors g v in
      printf "  preds: "; print_edges preds)

let print_path p =
  Sequence.iter (G.Path.edges p) ~f:(fun (v, e, v') ->
      printf "(%s, %d, %s)\n" v e v')

let () =
  let g = G.create () in
  List.iter edges ~f:(fun (v1, e, v2) -> G.insert_edge g v1 e v2);
  print_graph g;
  let pre_dfs = G.traverse_dfs_pre g "start" in
  let post_dfs = G.traverse_dfs_post g "start" in
  let bfs = G.traverse_bfs g "start" in
  let topo = G.topological_sort g in
  printf "\n";
  printf "DFS: \"finish\" reachable from \"B\"? %s\n"
    (Bool.to_string
       (G.exists_dfs g "B" ~f:(fun v -> String.equal v "finish")));
  printf "DFS: \"A\" reachable from \"C\"? %s\n"
    (Bool.to_string (G.exists_dfs g "C" ~f:(fun v -> String.equal v "A")));
  printf "BFS: \"finish\" reachable from \"B\"? %s\n"
    (Bool.to_string
       (G.exists_bfs g "B" ~f:(fun v -> String.equal v "finish")));
  printf "BFS: \"A\" reachable from \"C\"? %s\n"
    (Bool.to_string (G.exists_bfs g "C" ~f:(fun v -> String.equal v "A")));
  printf "\nDFS pre-order: %s" (String.concat ~sep:", " pre_dfs);
  printf "\nDFS post-order: %s" (String.concat ~sep:", " post_dfs);
  printf "\nBFS: %s" (String.concat ~sep:", " bfs);
  printf "\n\nSCCs:\n";
  List.iter (G.strongly_connected_components g) ~f:(fun scc ->
      printf "    {%s}\n" (String.concat scc ~sep:", "));
  printf "\nshortest path from start to finish:\n\n";
  ( match G.shortest_path g "start" "finish" ~weight:(fun _ w _ -> w) with
  | None -> ()
  | Some p -> print_path p );
  printf "\n\ntopological sort: %s"
    ( match topo with
    | Some vs -> String.concat ~sep:", " vs
    | None -> "not a DAG" );
  printf "\n";
  G.remove_vertex g "A";
  G.remove_edge g "start" 2 "B";
  printf "\n";
  print_graph g
