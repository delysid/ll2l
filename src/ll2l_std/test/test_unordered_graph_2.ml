open Core_kernel
open Stdio
open Ll2l_std
module G = Unordered_graph.Make (Int) (Unit)

let edges = [(1, 2); (2, 3); (2, 4); (2, 6); (3, 5); (4, 5); (5, 2)]

let print_edges edges =
  printf "{%s}\n"
    ( String.concat ~sep:", "
    @@ List.map edges ~f:(fun (v, _) -> Int.to_string v) )

let print_graph g =
  let vs = G.vertices g |> List.sort ~compare:Int.compare in
  List.iter vs ~f:(fun v ->
      printf "%s:\n" (Int.to_string v);
      let succs = G.successors g v in
      printf "  succs: ";
      print_edges succs;
      let preds = G.predecessors g v in
      printf "  preds: "; print_edges preds; printf "\n" )

let () =
  let g = G.create () in
  List.iter edges ~f:(fun (v1, v2) -> G.insert_edge g v1 () v2);
  print_graph g;
  let post_dfs = G.traverse_dfs_post g 1 |> List.rev in
  printf "\nDFS reverse post-order: %s\n\n"
    (String.concat ~sep:", " @@ List.map post_dfs ~f:Int.to_string);
  let d = G.dominators g 1 in
  List.iter (G.vertices g) ~f:(fun v ->
      printf "%s:\n" (Int.to_string v);
      let doms = d#dominators v in
      let idoms = d#immediately_dominated_by v in
      let df = d#dominance_frontier v in
      printf "dominated by: {%s}\n"
        ( String.concat ~sep:", "
        @@ List.map doms ~f:(fun v -> Int.to_string v) );
      printf "immediately dominating: {%s}\n"
        ( String.concat ~sep:", "
        @@ List.map idoms ~f:(fun v -> Int.to_string v) );
      ( match d#immediate_dominator_of v with
      | None -> ()
      | Some v' -> printf "immediately dominated by: %s\n" (Int.to_string v')
      );
      printf "dominance frontier: {%s}\n"
        (String.concat ~sep:", " @@ List.map df ~f:(fun v -> Int.to_string v));
      printf "\n" );
  let d = G.post_dominators g 5 in
  List.iter (G.vertices g) ~f:(fun v ->
      printf "%s:\n" (Int.to_string v);
      let doms = d#dominators v in
      let idoms = d#immediately_dominated_by v in
      let df = d#dominance_frontier v in
      printf "post-dominated by: {%s}\n"
        ( String.concat ~sep:", "
        @@ List.map doms ~f:(fun v -> Int.to_string v) );
      printf "immediately post-dominating: {%s}\n"
        ( String.concat ~sep:", "
        @@ List.map idoms ~f:(fun v -> Int.to_string v) );
      ( match d#immediate_dominator_of v with
      | None -> ()
      | Some v' ->
          printf "immediately post-dominated by: %s\n" (Int.to_string v') );
      printf "post-dominance frontier: {%s}\n"
        (String.concat ~sep:", " @@ List.map df ~f:(fun v -> Int.to_string v));
      printf "\n" );
  printf "\n\nSCCs:\n";
  List.iter (G.strongly_connected_components g) ~f:(fun scc ->
      printf "    {%s}\n"
        (String.concat (List.map scc ~f:Int.to_string) ~sep:", ") );
  let dag = G.make_dag g 1 in
  print_graph dag;
  printf "is cyclic: %s\n" (G.is_cyclic dag |> Bool.to_string);
  let post_dfs = G.traverse_dfs_post dag 1 |> List.rev in
  printf "\nDFS reverse post-order: %s\n\n"
    (String.concat ~sep:", " @@ List.map post_dfs ~f:Int.to_string)
