open Core_kernel
open Stdio
open Ll2l_std

let () =
  (* initialize *)
  let vec = Vector.create () in
  printf "length = %d\n" (Vector.length vec);
  printf "capacity = %d\n" (Vector.capacity vec);
  printf "\n";
  (* insert to the back *)
  for i = 1 to 50 do
    Vector.push_back vec (30 - i)
  done;
  Vector.iteri vec ~f:(fun i x -> printf "index %d: %d\n" i x);
  printf "length = %d\n" (Vector.length vec);
  printf "capacity = %d\n" (Vector.capacity vec);
  printf "\n";
  (* remove from the back *)
  Vector.pop_back_exn vec;
  Vector.iteri vec ~f:(fun i x -> printf "index %d: %d\n" i x);
  printf "length = %d\n" (Vector.length vec);
  printf "capacity = %d\n" (Vector.capacity vec);
  printf "\n";
  (* remove from an arbitrary index *)
  Vector.remove_exn vec 20;
  Vector.iteri vec ~f:(fun i x -> printf "index %d: %d\n" i x);
  printf "length = %d\n" (Vector.length vec);
  printf "capacity = %d\n" (Vector.capacity vec);
  printf "\n";
  (* trim from the back *)
  Vector.cut_back vec 20;
  Vector.iteri vec ~f:(fun i x -> printf "index %d: %d\n" i x);
  printf "length = %d\n" (Vector.length vec);
  printf "capacity = %d\n" (Vector.capacity vec);
  printf "\n";
  (* sort in ascending order *)
  Vector.sort vec ~compare:Int.compare;
  Vector.iteri vec ~f:(fun i x -> printf "index %d: %d\n" i x);
  printf "length = %d\n" (Vector.length vec);
  printf "capacity = %d\n" (Vector.capacity vec);
  printf "\n";
  (* filter out odd numbers *)
  Vector.filter_inplace vec ~f:(fun x -> Int.rem x 2 = 0);
  Vector.iteri vec ~f:(fun i x -> printf "index %d: %d\n" i x);
  printf "length = %d\n" (Vector.length vec);
  printf "capacity = %d\n" (Vector.capacity vec);
  printf "\n";
  (* insert at an arbitrary index *)
  Vector.insert_exn vec 3 7;
  Vector.insert_exn vec 9 101;
  Vector.insert_exn vec 0 65535;
  Vector.iteri vec ~f:(fun i x -> printf "index %d: %d\n" i x);
  printf "length = %d\n" (Vector.length vec);
  printf "capacity = %d\n" (Vector.capacity vec)
