open Core_kernel
open Stdio
open Ll2l_std

let max_signed sz =
  let m = Bitvec.modulus sz in
  let sh = sz - 1 in
  let sh = Bitvec.(int sh mod m) in
  Bitvec.((((one lsl sh) mod m) - one) mod m)

let () =
  let i = Bitvec.(int 42 mod m32) in
  let j = Bitvec.(int 37 mod m32) in
  let max_32 = Bitvec.(int32 0xFFFFFFFFl mod m32) in
  let b = Bitvec.(i >= j) in
  printf "0x%s %s 0x%s 0x%s 0x%016LX 0x%s 0x%s\n" (Bitvec.to_string i)
    (Bool.to_string b) (Bitvec.to_string max_32)
    Bitvec.(to_string (succ max_32 mod m32))
    (Bitvec.to_int64 max_32)
    Bitvec.(to_string (ones mod m32))
    (Bitvec.to_string (max_signed 32));
  let i =
    Bitvec.(of_string "0x800000001000000010000000100000001000000010000000")
  in
  printf "%d\n" (Bitvec.popcount i)
