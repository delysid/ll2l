open Core_kernel
open Stdio
open Ll2l_std

let do_print i1 i2 =
  printf "%s + %s = %s\n" (Interval.to_string i1) (Interval.to_string i2)
    Interval.(to_string (add i1 i2));
  printf "%s * %s = %s\n" (Interval.to_string i1) (Interval.to_string i2)
    Interval.(to_string (mul i1 i2));
  printf "%s intersect %s = %s\n" (Interval.to_string i1)
    (Interval.to_string i2)
    Interval.(to_string (intersect i1 i2));
  printf "%s union %s = %s\n" (Interval.to_string i1) (Interval.to_string i2)
    Interval.(to_string (union i1 i2));
  printf "inverse of %s = %s\n" (Interval.to_string i1)
    Interval.(to_string (inverse i1));
  printf "inverse of %s = %s\n" (Interval.to_string i2)
    Interval.(to_string (inverse i2));
  printf "-%s = %s\n" (Interval.to_string i1) Interval.(to_string (neg i1));
  printf "-%s = %s\n" (Interval.to_string i2) Interval.(to_string (neg i2))

let () =
  let i1 =
    Interval.create ~lo:Bitvec.one ~hi:Bitvec.(int 5 mod modulus 32) ~size:32
  in
  let i2 =
    Interval.create
      ~lo:Bitvec.(int 3 mod modulus 32)
      ~hi:Bitvec.(int 7 mod modulus 32)
      ~size:32
  in
  do_print i1 i2

let () =
  let i1 =
    Interval.create ~lo:Bitvec.zero
      ~hi:Bitvec.(int 2 mod modulus 64)
      ~size:64
  in
  let i2 =
    Interval.create_single ~value:Bitvec.(int 3 mod modulus 64) ~size:64
  in
  printf "%s << %s = %s\n" (Interval.to_string i1) (Interval.to_string i2)
    Interval.(to_string (i1 << i2))

let () =
  let i = Interval.create_full ~size:32 in
  let i2 =
    Interval.create_single ~value:Bitvec.(int 3 mod modulus 64) ~size:64
  in
  let zx = Interval.zext i ~size:64 in
  printf "zext(%s, 64) = %s\n" (Interval.to_string i) Interval.(to_string zx);
  printf "%s << %s = %s\n" (Interval.to_string zx) (Interval.to_string i2)
    Interval.(to_string (zx << i2))

let () =
  let i1 =
    Interval.create
      ~lo:Bitvec.(int 0x6C30 mod modulus 64)
      ~hi:Bitvec.(int 0x6C31 mod modulus 64)
      ~size:64
  in
  printf "trunc(%s, 32) = %s\n" (Interval.to_string i1)
    Interval.(to_string (trunc i1 ~size:32))

let () =
  let i1 =
    Interval.create ~lo:Bitvec.zero
      ~hi:Bitvec.(int 0xF001 mod modulus 64)
      ~size:64
  in
  printf "trunc(%s, 32) = %s\n" (Interval.to_string i1)
    Interval.(to_string (trunc i1 ~size:32));
  printf "trunc(trunc(%s, 32), 16) = %s\n" (Interval.to_string i1)
    Interval.(to_string (trunc (trunc i1 ~size:32) ~size:16));
  printf "trunc(trunc(trunc(%s, 32), 16), 8) = %s\n" (Interval.to_string i1)
    Interval.(to_string (trunc (trunc (trunc i1 ~size:32) ~size:16) ~size:8))

let () =
  let i1 = Interval.create_full ~size:8 in
  let i2 =
    Interval.create_single
      ~value:Bitvec.(int 0xFFFFFFBF mod modulus 32)
      ~size:32
  in
  let i1 = Interval.sext i1 ~size:32 in
  printf "%s + %s = %s\n" (Interval.to_string i1) (Interval.to_string i2)
    Interval.(to_string (add i1 i2))

let () =
  let i1 = Interval.create_single ~value:Bitvec.zero ~size:16 in
  let i2 =
    Interval.create_single ~value:Bitvec.(int 0xA080 mod modulus 16) ~size:16
  in
  let iu = Interval.union i1 i2 ~range:Unsigned in
  printf "union(%s, %s) = %s\n" (Interval.to_string i1)
    (Interval.to_string i2) (Interval.to_string iu)
