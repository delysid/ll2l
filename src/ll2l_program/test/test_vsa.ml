open Core_kernel
open Stdio
open Ll2l_image
open Ll2l_ir
open Ll2l_program
open Ll2l_std

let () =
  let img = Load_image.load_exn Sys.argv.(1) in
  let Disasm.{p} = Disasm.run img in
  let subs = Program.subroutines p in
  let bitv =
    Map.map p.canonical_values ~f:(fun Il.{value; size} ->
        Interval.create_single ~value ~size |> Vsa.Bitv_lattice.interval )
  in
  let memory =
    Il.(
      Var_map.singleton default_mem
        (Vsa.Mem_lattice.of_concrete_env (Memory.make_env p.img.mem)))
  in
  let entry = Vsa.Env.(Env {bitv; memory}) in
  let init = Vsa.create ~entry in
  subs#subroutines
  |> List.iter ~f:(fun sub ->
         let start = Ir.Sub.entry_of sub in
         printf
           "==========================================================\n";
         Vsa.(vsa (init ~start) p.ir ~subs ~start)
         |> Ir.Cfg.Fixpoint.enum
         |> Sequence.iter ~f:(fun (l, env) ->
                match env with
                | Vsa.Env.Bottom ->
                    printf "%s = bottom\n\n" (Ir.Label.to_string l)
                | Vsa.Env.(Env {bitv; memory}) ->
                    printf "bitv:\n\n";
                    if Map.is_empty bitv then
                      printf "%s = top\n\n" (Ir.Label.to_string l)
                    else (
                      printf "%s =\n" (Ir.Label.to_string l);
                      Map.iteri bitv ~f:(fun ~key ~data ->
                          match data with
                          | Vsa.Bitv_lattice.Interval i
                            when not (Interval.is_full i) ->
                              printf "    %s -> %s\n" key.name
                                (Interval.to_string i)
                          | _ -> () ) );
                    (* printf "\n";
                     * printf "memory:\n\n";
                     * if Map.is_empty memory then
                     *   printf "%s = top\n\n" (Ir.Label.to_string l)
                     * else (
                     *   printf "%s = \n" (Ir.Label.to_string l);
                     *   Map.iteri memory ~f:(fun ~key ~data ->
                     *       printf "    %s:\n" key.name;
                     *       match data with
                     *       | Vsa.Mem_lattice.Mem m ->
                     *           Map.iteri m ~f:(fun ~key ~data ->
                     *               printf "        %s -> %s\n"
                     *                 (Bitvec.to_string key)
                     *                 (Vsa.Bitv_lattice.to_string data))
                     *       | _ -> ()) ); *)
                    printf "\n" ) )
