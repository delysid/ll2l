open Core_kernel
open Stdio
open Ll2l_arch
open Ll2l_image
open Ll2l_ir
open Ll2l_program
open Ll2l_std
module B = Ir.Block
module C = Ir.Ctrl
module D = Ir.Def
module L = Ir.Label
module M = Memory

type t = {jumps: (L.t, Addr.Set.t) Hashtbl.t} [@@unboxed]

let create () = {jumps= Hashtbl.create (module L)}

let alist_of_jumps t =
  Hashtbl.to_alist t.jumps |> List.map ~f:(fun (l, s) -> (l, Set.to_list s))

let insert_jump t (img : Image.t) l a =
  if Image.is_valid_pc img a then
    let s =
      match Hashtbl.find t.jumps l with
      | None -> Addr.Set.singleton a
      | Some s -> Set.add s a
    in
    Hashtbl.set t.jumps l s

let eval (p : Program.t) mem_env t v blk =
  let l = B.label_of blk in
  let Ir.{g; defs} = Ir.slice_backward p.ir l Il.Var_set.(singleton v) in
  let mem_env = Il.Var_map.singleton Il.default_mem mem_env in
  let state = Ir_eval.create ~var_env:p.canonical_values ~mem_env () in
  let rec eval state l' =
    match Ir.block p.ir l' with
    | None -> [state]
    | Some blk -> (
        let init = state in
        let state =
          match Hashtbl.find defs l' with
          | None -> init
          | Some s ->
              B.foldi_defs blk ~init ~f:(fun state i (dst, src) ->
                  if not (Set.mem s i) then state
                  else Ir_eval.eval_def state ~dst ~src )
        in
        match Ir.Slice_graph.successors g l' with
        | [] -> [state]
        | succs ->
            List.map succs ~f:fst |> List.map ~f:(eval state) |> List.concat
        )
  in
  Ir.Slice_graph.iter_vertices g ~f:(fun l' ->
      (* the graph has no back-edges, so start from the root nodes *)
      if not (Ir.Slice_graph.has_predecessors g l') then
        List.iter (eval state l') ~f:(fun state ->
            Option.iter (Map.find state.var_env v) ~f:(fun c ->
                insert_jump t p.img l Il.(c.value) ) ) )

let () =
  let img = Load_image.load_exn Sys.argv.(1) in
  let Disasm.{p} = Disasm.run img in
  let t = create () in
  let mem_env = Memory.make_env p.img.mem in
  List.iter (Ir.blocks_of p.ir) ~f:(fun blk ->
      match B.ctrl_of blk with
      | C.Call (C.Indirect (Il.Var v), _, _)
       |C.Jmp (C.Indirect (Il.Var v), _) -> (
          eval p mem_env t v blk;
          let l = B.label_of blk in
          printf "-------------------------------------------\n";
          printf "results for %s at %s" v.name (L.to_string l);
          ( match Ir.addr_of_block p.ir l with
          | None -> ()
          | Some addr -> printf " (0x%s)" (Bitvec.to_string addr) );
          printf "\n\n";
          match Hashtbl.find t.jumps l with
          | None -> ()
          | Some s ->
              printf "found %s: {%s}\n" (L.to_string l)
                (String.concat ~sep:", "
                   (List.map (Set.to_list s) ~f:(fun a ->
                        Printf.sprintf "0x%s" (Bitvec.to_string a) ) ) ) )
      | _ -> () )
