# `ll2l-program`

This library provides a composite data structure which represents a translated binary program.
In particular, the `Disasm` module implements a disassembler which attempts to recover the instructions and control-flow graph (CFG) of an input program.
Currently, indirect control-flow targets are resolved in an ad-hoc fashion.
Future work may focus on more principled/generalizable approaches to this problem.

Programs are stored both as an intermediate representation provided by the `Ir` module (see the `ll2l-ir` library) as well as an assembly representation (provided by the `Code` module).
