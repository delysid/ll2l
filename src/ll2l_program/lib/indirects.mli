open Core_kernel
open Ll2l_arch
open Ll2l_ir
open Ll2l_std

type indirects_result =
  {targets: (Ir.Label.t * Addr.t * Arch.ec) list; loads: Addr.t list}

val analyze :
     ?check_bounds:bool
  -> Program.t
  -> Ir.Label.t Hash_set.t
  -> indirects_result
