open Core_kernel
open Ll2l_arch
open Ll2l_std

module Edge = struct
  type t = Direct | Indirect | Fallthrough | Return | Delay
  [@@deriving equal, compare, hash, sexp]

  let to_string = function
    | Direct -> "direct"
    | Indirect -> "indirect"
    | Fallthrough -> "fallthrough"
    | Return -> "return"
    | Delay -> "delay"
end

module Block = struct
  type t = {addrs: Addr.Set.t} [@@unboxed]

  let is_valid b = not (Set.is_empty b.addrs) [@@inline]

  let has_addr b a = Set.mem b.addrs a [@@inline]

  let addrs_of b = Set.to_list b.addrs [@@inline]

  let for_all b ~f = Set.for_all b.addrs ~f [@@inline]

  let start_addr b = Set.min_elt b.addrs [@@inline]

  let start_addr_exn b = Set.min_elt_exn b.addrs [@@inline]

  let iter b ~f = Set.iter b.addrs ~f [@@inline]

  let fold b ~init ~f = Set.fold b.addrs ~init ~f [@@inline]
end

module Cfg = Unordered_graph.Make (Addr) (Edge)

type t = {cfg: Cfg.t; instrs: (Addr.t, Arch.ei) Hashtbl.t}

let create () =
  {cfg= Cfg.create (); instrs= Hashtbl.create (module Addr)}
  [@@inline]

let cfg_of c = c.cfg [@@inline]

let addrs_of c = Cfg.vertices c.cfg [@@inline]

let has_instr c a = Hashtbl.mem c.instrs a [@@inline]

let instr_of_addr c a = Hashtbl.find c.instrs a [@@inline]

let instr_of_addr_exn c a = Hashtbl.find_exn c.instrs a [@@inline]

let iteri_instrs c ~f = Hashtbl.iteri c.instrs ~f [@@inline]

let iter_instrs c ~f =
  iteri_instrs c ~f:(fun ~key ~data -> f data)
  [@@inline]

let fold_instrs c ~init ~f =
  Hashtbl.fold c.instrs ~init ~f:(fun ~key ~data acc -> f acc ~key ~data)
  [@@inline]

let insert_instr c a instr =
  Cfg.insert_vertex c.cfg a;
  Hashtbl.set c.instrs a instr
  [@@inline]

let insert_edge c a e a' =
  if not (has_instr c a && has_instr c a') then None
  else Some (Cfg.insert_edge c.cfg a e a')

let remove_addr c a =
  Cfg.remove_vertex c.cfg a; Hashtbl.remove c.instrs a
  [@@inline]

let filter_addrs c ~f =
  Cfg.filter_vertices c.cfg ~f;
  Hashtbl.filter_keys_inplace c.instrs ~f
  [@@inline]

let remove_edge c a e a' = Cfg.remove_edge c.cfg a e a' [@@inline]

let synchronize c =
  let f = has_instr c in
  Cfg.filter_vertices c.cfg ~f
  [@@inline]

type blocks = Block.t Addr.Map.t

type subs = blocks Addr.Map.t

let blocks_of ?(starts = Addr.Set.empty) c =
  let e = Addr.Set.empty in
  let l, s =
    let addrs = Array.of_list (addrs_of c) in
    Array.sort addrs ~compare:Addr.compare;
    Array.fold addrs ~init:([], e) ~f:(fun (l, s) a ->
        if Set.is_empty s then (l, Set.add s a)
        else
          let module E = Edge in
          let preds = Cfg.predecessors c.cfg a in
          let succs = Cfg.successors c.cfg a in
          let is_start =
            List.is_empty preds
            || List.exists preds ~f:(fun (_, es) ->
                   List.exists es ~f:(function
                     | E.Direct | E.Indirect | E.Return -> true
                     | _ -> false ) )
            || Set.mem starts a
          in
          let is_delay_end, has_delay =
            let f (_, es) = List.mem es E.Delay ~equal:E.equal in
            let is_delay_end =
              match List.find preds ~f with
              | None -> false
              | Some (v, _) -> (
                  List.filter (Cfg.successors c.cfg v) ~f
                  |> List.map ~f:fst |> Addr.Set.of_list |> Set.max_elt
                  |> function
                  | None -> false
                  | Some v -> Addr.(v = a) )
            in
            (is_delay_end, List.exists succs ~f)
          in
          let has_direct =
            List.exists succs ~f:(fun (_, es) ->
                List.mem es E.Direct ~equal:E.equal )
          in
          let is_term =
            is_delay_end
            || ((not has_delay) && has_direct)
            || List.is_empty succs
          in
          if is_start then (s :: l, Addr.Set.singleton a)
          else if is_term then (Set.add s a :: l, e)
          else (l, Set.add s a) )
  in
  (* filter out empty blocks *)
  List.rev_filter_map (s :: l) ~f:(fun addrs ->
      if Set.is_empty addrs then None
      else Some (Set.min_elt_exn addrs, Block.{addrs}) )
  |> Addr.Map.of_alist_exn
