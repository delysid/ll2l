open Core_kernel
open Ll2l_image
open Ll2l_ir
open Ll2l_std

type t =
  { img: Image.t
  ; code: Code.t
  ; ir: Ir.t
  ; canonical_values: Il.const Il.Var_map.t }

val subroutines : ?starts:Addr.Set.t -> t -> Ir.subs

val native_subroutines :
  ?starts:Addr.Set.t -> ?subs:Ir.subs option -> t -> Code.subs
