open Ll2l_image
open Ll2l_std

type disasm_result = {p: Program.t; starts: Addr.Set.t}

val run :
     ?use_code_pointers:bool
  -> ?spec:bool
  -> ?noreturn_analysis:bool
  -> Image.t
  -> disasm_result
