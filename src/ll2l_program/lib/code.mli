open Core_kernel
open Ll2l_arch
open Ll2l_std

module Edge : sig
  type t = Direct | Indirect | Fallthrough | Return | Delay
  [@@deriving equal, compare, hash, sexp]

  val to_string : t -> string
end

module Block : sig
  type t

  val is_valid : t -> bool

  val has_addr : t -> Addr.t -> bool

  val addrs_of : t -> Addr.t list

  val for_all : t -> f:(Addr.t -> bool) -> bool

  val start_addr : t -> Addr.t option

  val start_addr_exn : t -> Addr.t

  val iter : t -> f:(Addr.t -> unit) -> unit

  val fold : t -> init:'a -> f:('a -> Addr.t -> 'a) -> 'a
end

(* since this CFG is mutable, it is possible for it
 * to go out of sync with the instruction table *)
module Cfg : module type of Unordered_graph.Make (Addr) (Edge)

type t = private {cfg: Cfg.t; instrs: (Addr.t, Arch.ei) Hashtbl.t}

val create : unit -> t

val cfg_of : t -> Cfg.t

val addrs_of : t -> Addr.t list

val has_instr : t -> Addr.t -> bool

val instr_of_addr : t -> Addr.t -> Arch.ei option

val instr_of_addr_exn : t -> Addr.t -> Arch.ei

val iteri_instrs : t -> f:(key:Addr.t -> data:Arch.ei -> unit) -> unit

val iter_instrs : t -> f:(Arch.ei -> unit) -> unit

val fold_instrs :
  t -> init:'a -> f:('a -> key:Addr.t -> data:Arch.ei -> 'a) -> 'a

val insert_instr : t -> Addr.t -> Arch.ei -> unit

val insert_edge : t -> Addr.t -> Edge.t -> Addr.t -> unit option

val remove_addr : t -> Addr.t -> unit

val filter_addrs : t -> f:(Addr.t -> bool) -> unit

val remove_edge : t -> Addr.t -> Edge.t -> Addr.t -> unit

val synchronize : t -> unit

type blocks = Block.t Addr.Map.t

type subs = blocks Addr.Map.t

val blocks_of : ?starts:Addr.Set.t -> t -> blocks
