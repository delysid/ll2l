open Core_kernel
open Ll2l_image
open Ll2l_ir
open Ll2l_std
module B = Ir.Block
module E = Ir.Edge
module L = Ir.Label
module S = Ir.Sub

type t =
  { img: Image.t
  ; code: Code.t
  ; ir: Ir.t
  ; canonical_values: Il.const Il.Var_map.t }

let subroutines ?(starts = Addr.Set.empty) p =
  let ir = p.ir in
  let cfg = Ir.cfg_of ir in
  let subs = Vector.create () in
  let l2sub = Hashtbl.create (module L) in
  let append_sub sub l =
    let scfg = S.cfg_of sub in
    Ir.Cfg.insert_vertex scfg l;
    Hashtbl.set l2sub l sub
  in
  let call_targets blk =
    match B.ctrl_of blk with
    | Call _ ->
        B.label_of blk |> Ir.Cfg.successors cfg
        |> List.filter_map ~f:(fun (l, es) ->
               Option.some_if
                 (List.exists es ~f:(function
                   | E.Direct | E.Indirect -> true
                   | _ -> false ) )
                 l )
    | _ -> []
  in
  (* a pre-defined set of entry points can also indicate function starts *)
  Set.iter starts ~f:(fun a ->
      match Ir.blocks_of_addr ir a with
      | [] -> ()
      | blk :: _ ->
          let l = B.label_of blk in
          let sub = S.create l in
          Vector.push_back subs sub; append_sub sub l );
  (* get the initial subroutine entries by examining called blocks *)
  let called = Hash_set.create (module L) in
  Ir.iter_blocks ir ~f:(fun blk ->
      let calls = call_targets blk in
      let l = B.label_of blk in
      Ir.Cfg.iter_succs cfg l ~f:(fun l' e ->
          if
            List.mem calls l' ~equal:L.equal
            && (not (Hash_set.mem called l'))
            && not (Hashtbl.mem l2sub l')
          then (
            let sub = S.create l' in
            Vector.push_back subs sub;
            append_sub sub l';
            Hash_set.add called l' ) ) );
  (* expand existing subroutine *)
  let rec expand ?l sub =
    let scfg = S.cfg_of sub in
    let l, ok =
      match l with
      | None -> (Ir.Cfg.vertices scfg |> List.hd_exn, true)
      | Some l -> (l, Hashtbl.mem l2sub l |> not)
    in
    if ok then (
      append_sub sub l;
      let addr = Ir.addr_of_block_exn ir l in
      let is_entry = Set.mem starts addr in
      let is_called = Hash_set.mem called l in
      ( if not (is_entry || is_called) then
        (* expand backwards *)
        let entry = S.entry_of sub in
        let start = Ir.addr_of_block_exn ir entry in
        Ir.Cfg.iter_preds cfg l ~f:(fun l' e ->
            let addr' = Ir.addr_of_block_exn ir l' in
            match e with
            | Direct when Addr.(addr' > start) ->
                (* heuristic for catching stubs that end in a tail call *)
                let rec is_outside = function
                  | [] -> true
                  | (l'', es) :: rest ->
                      let f = function
                        | E.Direct | E.Fallthrough -> true
                        | _ -> false
                      in
                      if List.exists es ~f then
                        match Hashtbl.find l2sub l'' with
                        | Some sub' when phys_equal sub sub' -> false
                        | _ -> is_outside rest
                      else is_outside rest
                in
                if not (is_outside (Ir.Cfg.predecessors cfg l')) then
                  expand sub ~l:l'
            | _ -> expand sub ~l:l' ) );
      (* expand forwards *)
      let blk = Ir.block_exn ir l in
      let start = Ir.addr_of_block_exn ir l in
      let calls = call_targets blk in
      Ir.Cfg.iter_succs cfg l ~f:(fun l' e ->
          if
            let addr' = Ir.addr_of_block_exn ir l' in
            not
              (* don't expand to call targets *)
              ( List.mem calls l' ~equal:L.equal
              (* another heuristic for catching tail calls *)
              || ((is_entry || is_called) && Addr.(addr' < start)) )
          then expand sub ~l:l' ) )
  in
  (* update the entry point of a subroutine *)
  let update_entry sub =
    let scfg = S.cfg_of sub in
    let ls = Ir.Cfg.vertices scfg in
    (* ignore subroutines with no blocks, we will filter them out later *)
    if List.is_empty ls |> not then
      match
        List.find ls ~f:(fun l' ->
            Hash_set.mem called l'
            || Ir.addr_of_block_exn ir l' |> Set.mem starts )
      with
      | Some l -> S.set_entry_exn sub l
      | None ->
          (* the subroutine isn't called, so assume that the
           * entry must be associated with the lowest address *)
          List.map ls ~f:(fun l -> (Ir.addr_of_block_exn ir l, l))
          |> List.sort ~compare:(fun (a, _) (a', _) -> Addr.compare a a')
          |> List.hd_exn |> snd |> S.set_entry_exn sub
  in
  let explore l =
    if not (Hashtbl.mem l2sub l) then (
      let sub = S.create l in
      Vector.push_back subs sub; expand sub ~l; update_entry sub )
  in
  (* expand initial subroutines *)
  Vector.iter subs ~f:(fun sub -> expand sub; update_entry sub);
  (* discover remaining subroutines, start from highest addresses *)
  Ir.blocks_of ir
  |> List.sort ~compare:(fun blk blk' ->
         let a = B.label_of blk |> Ir.addr_of_block_exn ir in
         let a' = B.label_of blk' |> Ir.addr_of_block_exn ir in
         Addr.compare a' a )
  |> List.iter ~f:(fun blk -> B.label_of blk |> explore);
  (* fix tail calls *)
  let stubs = Vector.create () in
  Vector.iter subs ~f:(fun sub ->
      let scfg = S.cfg_of sub in
      let start = S.entry_of sub |> Ir.addr_of_block_exn ir in
      let rec loop () =
        let rec find_stub = function
          | [] -> None
          | l :: rest ->
              let addr = Ir.addr_of_block_exn ir l in
              let succ_in_sub () =
                Ir.Cfg.(
                  find_successor cfg l ~f:(fun l' _ -> has_vertex scfg l'))
                |> Option.is_some
              in
              let pred_in_sub () =
                Ir.Cfg.(
                  find_predecessor cfg l ~f:(fun l' _ -> has_vertex scfg l'))
                |> Option.is_some
              in
              let is_entry = L.equal (S.entry_of sub) l in
              if is_entry || Addr.(addr < start) then
                if succ_in_sub () |> not then Some l else find_stub rest
              else if (not is_entry) && Addr.(addr > start) then
                if pred_in_sub () |> not then Some l else find_stub rest
              else find_stub rest
        in
        Ir.Cfg.vertices scfg |> find_stub
        |> Option.iter ~f:(fun l ->
               Vector.push_back stubs l;
               Hashtbl.remove l2sub l;
               Ir.Cfg.remove_vertex scfg l;
               update_entry sub;
               loop () )
      in
      loop () );
  (* expand stub blocks *)
  Vector.sort stubs ~compare:(fun l l' ->
      let a = Ir.addr_of_block_exn ir l in
      let a' = Ir.addr_of_block_exn ir l' in
      Addr.compare a' a );
  Vector.iter stubs ~f:explore;
  (* get rid of empty subroutines *)
  Vector.filter_inplace subs ~f:(fun sub ->
      S.cfg_of sub |> Ir.Cfg.is_empty |> not );
  (* create local CFGs *)
  Vector.iter subs ~f:(fun sub ->
      let scfg = S.cfg_of sub in
      Ir.Cfg.vertices scfg
      |> List.iter ~f:(fun l ->
             Ir.Cfg.iter_succs cfg l ~f:(fun l' e ->
                 if Ir.Cfg.has_vertex scfg l' then
                   Ir.Cfg.insert_edge scfg l e l' );
             Ir.Cfg.iter_preds cfg l ~f:(fun l' e ->
                 if Ir.Cfg.has_vertex scfg l' then
                   Ir.Cfg.insert_edge scfg l' e l ) ) );
  (* return the mapping of entries to subroutines *)
  let subs = Vector.to_list subs in
  object (self)
    method subroutines = subs

    method sub_of_block l = Hashtbl.find l2sub l

    method sub_of_block_exn l = Hashtbl.find_exn l2sub l

    method sub_of_entry l =
      match self#sub_of_block l with
      | Some sub as s when L.equal l (S.entry_of sub) -> s
      | _ -> None

    method sub_of_entry_exn l =
      let sub = self#sub_of_block_exn l in
      if L.equal l (S.entry_of sub) then sub
      else
        L.to_string l
        |> Printf.sprintf "label %s is not an entry point"
        |> invalid_arg
  end

let native_subroutines ?(starts = Addr.Set.empty) ?(subs = None) p =
  let subs =
    match subs with
    | None -> subroutines p ~starts
    | Some subs -> subs
  in
  let blocks = Code.blocks_of p.code ~starts in
  List.fold subs#subroutines ~init:(blocks, []) ~f:(fun (blocks, acc) sub ->
      let cfg = S.cfg_of sub in
      let sub_blocks, blocks =
        (* all addresses of the block must be part of the subroutine's CFG.
         * we purge these from the remaining search space. *)
        Map.partition_tf blocks
          ~f:
            (Code.Block.for_all ~f:(fun a ->
                 match Ir.blocks_of_addr p.ir a with
                 | [] -> false
                 | ir_blocks ->
                     List.for_all ir_blocks ~f:(fun blk ->
                         Ir.Block.label_of blk |> Ir.Cfg.has_vertex cfg ) )
            )
      in
      let acc =
        if Map.is_empty sub_blocks then acc
        else (S.entry_of sub |> Ir.addr_of_block_exn p.ir, sub_blocks) :: acc
      in
      (blocks, acc) )
  |> snd |> Addr.Map.of_alist_exn
