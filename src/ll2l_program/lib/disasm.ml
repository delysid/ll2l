open Core_kernel
open Ll2l_arch
open Ll2l_image
open Ll2l_ir
open Ll2l_std
module B = Ir.Block
module C = Ir.Ctrl
module E = Ir.Edge
module CE = Code.Edge
module L = Ir.Label
module Lset = Ir.Label_set
module R = Image.Reloc
module S = Ir.Sub

module Reason = struct
  (* describes why we discovered a potential jump target *)
  type t =
    | Entry
    | Direct
    | Indirect
    | Fallthrough
    | Return
    | Delay
    | Global
    | Literal
    | Speculated
  [@@deriving equal, compare, sexp]

  include Comparable.Make (struct
    type nonrec t = t

    let equal = equal

    let compare = compare

    let sexp_of_t = sexp_of_t

    let t_of_sexp = t_of_sexp
  end)

  let to_string = function
    | Entry -> "entry"
    | Direct -> "direct"
    | Indirect -> "indirect"
    | Fallthrough -> "fallthrough"
    | Return -> "return"
    | Delay -> "delay"
    | Global -> "global"
    | Literal -> "literal"
    | Speculated -> "speculated"

  let of_code_edge = function
    | CE.Direct -> Direct
    | CE.Indirect -> Indirect
    | CE.Fallthrough -> Fallthrough
    | CE.Return -> Return
    | CE.Delay -> Delay
end

module Reasons = Set.Make (Reason)

type t =
  { (* pending jump targets *)
    new_targets: (Addr.t * Arch.ec * Reason.t) Stack.t
  ; (* pending edges for the native CFG *)
    new_edges: (Addr.t * CE.t * Addr.t) Stack.t
  ; (* observed literals in the code *)
    new_literals: Addr.t Stack.t
  ; (* code pointers harvested from memory *)
    code_pointers: (Addr.t * Arch.et) Stack.t
  ; (* record of all potential jump targets that we've seen so far *)
    seen: (Addr.t, Reasons.t) Hashtbl.t
  ; (* address we discovered to be data that was used by an instruction *)
    data: Addr.t Hash_set.t
  ; (* not a valid instruction *)
    invalid: Addr.t Hash_set.t
  ; (* blocks where we discovered an indirect edge *)
    indirect: L.t Hash_set.t
  ; (* pending indirect edges *)
    indirect_edges: (Addr.t * L.t) Stack.t
  ; (* table mapping instruction end addresses to instruction start addresses
     * (useful for finding overlapping bytes) *)
    instrs_end: (Addr.t, Addr.Set.t) Hashtbl.t
  ; (* the representation of the program *)
    program: Program.t
  ; (* used for lifting instruction semantics *)
    il_ctx: Il.Context.t }

let create ?(use_code_pointers = false) (img : Image.t) =
  let r = Reason.Entry in
  let new_targets = Stack.create () in
  let seen = Hashtbl.create (module Addr) in
  (* the highest addresses will be explored first,
   * which may have an effect on approximating 
   * the function boundaries (better or worse?) *)
  Map.iteri (Image.entries img) ~f:(fun ~key:a ~data:Arch.(T arch) ->
      let decode_ctx = Arch.create_decode_ctx arch in
      Stack.push new_targets (a, Arch.(C (arch, decode_ctx)), r);
      Hashtbl.set seen a Reasons.(singleton r) );
  let code_pointers =
    if use_code_pointers then
      Image.code_pointers img |> Map.to_alist |> Stack.of_list
    else Stack.create ()
  in
  let canonical_values =
    Map.fold img.canonical_values ~init:Il.Var_map.empty
      ~f:(fun ~key ~data m ->
        let Image.{value; size} = data in
        let key = Il.{name= key; size= Some size; hint= Il.Var_hint.Reg} in
        let data = Il.{value; size} in
        Map.set m ~key ~data )
  in
  { new_targets
  ; new_edges= Stack.create ()
  ; new_literals= Stack.create ()
  ; code_pointers
  ; seen
  ; data= Hash_set.create (module Addr)
  ; invalid= Hash_set.create (module Addr)
  ; indirect= Hash_set.create (module L)
  ; indirect_edges= Stack.create ()
  ; instrs_end= Hashtbl.create (module Addr)
  ; program= {code= Code.create (); ir= Ir.create (); img; canonical_values}
  ; il_ctx= Il.Context.create () }

let next_target t = Stack.pop t.new_targets [@@inline]

let has_new_targets t = not (Stack.is_empty t.new_targets) [@@inline]

let new_edge t a e a' = Stack.push t.new_edges (a, e, a') [@@inline]

let new_indirect_edge t a l = Stack.push t.indirect_edges (a, l) [@@inline]

let new_literal t a = Stack.push t.new_literals a [@@inline]

let has_seen t a = Hashtbl.mem t.seen a [@@inline]

let entry_addrs t =
  Hashtbl.to_alist t.seen
  |> List.filter ~f:(fun (_, reasons) -> Reasons.mem reasons Entry)
  |> List.map ~f:fst |> Addr.Set.of_list

let has_seen_reason t a r =
  match Hashtbl.find t.seen a with
  | None -> false
  | Some s -> Set.mem s r

let set_seen t a r =
  let s =
    match Hashtbl.find t.seen a with
    | None -> Reasons.singleton r
    | Some s -> Set.add s r
  in
  Hashtbl.set t.seen a s

let reason_of t a = Hashtbl.find t.seen a [@@inline]

let is_data t a = Hash_set.mem t.data a [@@inline]

let mark_data t a = Hash_set.add t.data a [@@inline]

let is_invalid t a = Hash_set.mem t.invalid a [@@inline]

let mark_invalid t a = Hash_set.add t.invalid a [@@inline]

let new_target ?(enqueue = true) t ~addr ~arch_ctx ~reason =
  let open Option.Let_syntax in
  (* check if we should even consider this
   * address as a potential instruction *)
  let%map _ =
    let arch =
      let Arch.(C (arch, _)) = arch_ctx in
      Some Arch.(T arch)
    in
    Option.some_if
      ( (not (is_data t addr))
      && (not (is_invalid t addr))
      && Image.is_valid_pc t.program.img addr ~arch )
      ()
  in
  ( match Hashtbl.find t.seen addr with
  | Some s ->
      (* allow a delay slot to be rediscovered ONCE,
       * but only if it's the target of another jump *)
      ( match reason with
      | Reason.Delay | Reason.Global | Reason.Literal -> ()
      | _ when not (Set.mem s Reason.Delay && Set.length s = 1) -> ()
      | _ -> if enqueue then Stack.push t.new_targets (addr, arch_ctx, reason)
      );
      Hashtbl.set t.seen addr (Set.add s reason)
  | None ->
      Hashtbl.set t.seen addr (Reasons.singleton reason);
      if enqueue then Stack.push t.new_targets (addr, arch_ctx, reason) );
  addr

(* does this instruction overlap with another instruction? *)
let has_overlap t ~arch ~instr ~addr =
  let endaddr = Arch.instr_end_addr arch instr ~addr in
  let endaddr_rev = Addr.(addr - int (Arch.max_instr_length arch)) in
  (* check if any other instructions end at the end address *)
  let more_than_one =
    match Hashtbl.find t.instrs_end endaddr with
    | None -> false
    | Some s -> Set.length s > 1
  in
  (* find an instruction that starts or ends within the instruction
     boundary *)
  let rec loop_fwd addr =
    if Addr.(addr >= endaddr) then false
    else
      match
        ( Code.instr_of_addr t.program.code addr
        , Hashtbl.find t.instrs_end addr )
      with
      | None, None -> Addr.succ addr |> loop_fwd
      | _ -> true
  in
  (* find an instruction which subsumes the instruction boundary *)
  let rec loop_bwd addr' =
    if Addr.(addr' <= endaddr_rev) then false
    else
      match Code.instr_of_addr t.program.code addr' with
      | Some Arch.(I (arch, instr))
        when Addr.(Arch.instr_end_addr arch instr ~addr:addr' > addr) -> true
      | _ when Addr.(addr' = zero) -> false
      | _ -> Addr.pred addr' |> loop_bwd
  in
  more_than_one || Addr.succ addr |> loop_fwd || Addr.pred addr |> loop_bwd

let overlaps t ~arch ~instr ~addr =
  let endaddr = Arch.instr_end_addr arch instr ~addr in
  let endaddr_rev = Addr.(addr - int (Arch.max_instr_length arch)) in
  (* all other instructions which end at the end address *)
  let s =
    match Hashtbl.find t.instrs_end endaddr with
    | None -> Addr.Set.empty
    | Some s -> Set.remove s addr
  in
  (* all instructions which start or end within the instruction boundary *)
  let rec loop_fwd s addr =
    if Addr.(addr >= endaddr) then s
    else
      let s =
        match Code.instr_of_addr t.program.code addr with
        | None -> s
        | Some _ -> Set.add s addr
      in
      let s =
        match Hashtbl.find t.instrs_end addr with
        | None -> s
        | Some s' -> Set.union s s'
      in
      Addr.succ addr |> loop_fwd s
  in
  (* all instructions which subsume the instruction boundary *)
  let rec loop_bwd s addr' =
    if Addr.(addr' <= endaddr_rev) then s
    else
      let s =
        match Code.instr_of_addr t.program.code addr' with
        | Some Arch.(I (arch, instr))
          when Addr.(Arch.instr_end_addr arch instr ~addr:addr' > addr) ->
            Set.add s addr'
        | _ -> s
      in
      if Addr.(addr' = zero) then s else Addr.pred addr' |> loop_bwd s
  in
  let s = Addr.succ addr |> loop_fwd s in
  Addr.pred addr |> loop_bwd s

(* read the maximum bytes needed for decoding an instruction *)
let fetch_instr_bytes t Arch.(T arch) ~addr =
  let max_len = Arch.max_instr_length arch in
  Memory.read_bytes t.program.img.mem addr max_len

(* decode and lift an instruction, while also checking for well-formedness *)
let rec translate t ~arch ~decode_ctx ~addr ~reason =
  let open Option.Let_syntax in
  let%bind bytes = Result.ok (fetch_instr_bytes t Arch.(T arch) ~addr) in
  let%bind instr = Result.ok (Arch.decode arch decode_ctx bytes) in
  let endaddr = Arch.instr_end_addr arch instr ~addr in
  (* did we actually decode any bytes? *)
  let%bind _ = Option.some_if Addr.(endaddr > addr) () in
  (* make sure that the boundaries of the instruction
   * are not subsumed by the boundaries of a relocation *)
  let%bind _ =
    match Map.closest_key t.program.img.relocs `Less_than endaddr with
    | Some (_, rel) ->
        Option.some_if
          ((not (R.within rel addr)) || R.within2 rel ~addr ~endaddr)
          ()
    | _ -> return ()
  in
  (* prevent spurious overlaps resulting from global data and literals *)
  let%bind _ =
    match reason with
    | Reason.Global | Reason.Literal ->
        Option.some_if (not (has_overlap t ~arch ~instr ~addr)) ()
    | _ -> return ()
  in
  (* decode the delay slots *)
  let%bind delay =
    match reason with
    | Reason.Delay -> return []
    | _ ->
        let sz = Arch.delay_slot_size arch instr in
        let rec aux n off res =
          if n >= sz then return (List.rev res)
          else
            let a' = Addr.(endaddr + int off) in
            let%bind i' =
              translate t ~arch ~decode_ctx ~addr:a' ~reason:Delay
            in
            let len = Arch.instr_length arch i' in
            aux (succ n) (off + len) ((i', a') :: res)
        in
        aux 0 0 []
  in
  (* lift the instruction *)
  let%map _ =
    match reason with
    | Reason.Delay -> return ()
    | _ ->
        let delay, da = List.unzip delay in
        (* get the raw semantics first *)
        let%bind il =
          Arch.lift arch instr addr t.il_ctx ~delay |> Result.ok
        in
        (* skip to the end address of the final delay slot, if it exists *)
        let endaddr =
          if List.is_empty delay then endaddr
          else
            let off =
              List.fold delay ~init:0 ~f:(fun len instr ->
                  len + Arch.instr_length arch instr )
            in
            Addr.(endaddr + int off)
        in
        let word_sz = Arch.word_size_of arch in
        (* reify to graphical representation (IR) *)
        Result.ok (Ir.lift t.program.ir il ~word_sz ~endaddr ~delay:da)
  in
  (* register the instruction *)
  Code.insert_instr t.program.code addr Arch.(I (arch, instr));
  let s =
    match Hashtbl.find t.instrs_end endaddr with
    | Some s -> Set.add s addr
    | None -> Addr.Set.singleton addr
  in
  Hashtbl.set t.instrs_end endaddr s;
  instr

(* keep other data structures consistent with IR *)
let synchronize t =
  let f = Ir.has_addr t.program.ir in
  (* update the CFG of actual instruction pointers *)
  Code.filter_addrs t.program.code ~f;
  (* update the tables of instruction data *)
  Hashtbl.filter_map_inplace t.instrs_end ~f:(fun s ->
      let s = Set.filter s ~f in
      Option.some_if (not (Set.is_empty s)) s )

(* remove this address from the set of valid instructions
 * and propagate to any predecessors in the CFG *)
let invalidate t ~addr =
  let ir = t.program.ir in
  let code = t.program.code in
  Ir.invalidate_addr ir addr
  |> Option.iter ~f:(fun (r : Ir.invalidate_result) ->
         Set.iter r.invalidated ~f:(fun addr' ->
             mark_invalid t addr';
             Code.instr_of_addr code addr'
             |> Option.iter ~f:(fun Arch.(I (arch, instr)) ->
                    let endaddr =
                      Arch.instr_end_addr arch instr ~addr:addr'
                    in
                    Hashtbl.find t.instrs_end endaddr
                    |> Option.iter ~f:(fun s ->
                           let s = Set.remove s addr' in
                           if Set.is_empty s then
                             Hashtbl.remove t.instrs_end endaddr
                           else Hashtbl.set t.instrs_end endaddr s ) );
             Code.remove_addr code addr' ) )

(* gather jump targets from the IR that we lifted *)
let explore_block ?(enqueue = true) t ~arch ~decode_ctx ~instr ~blk ~addr =
  let word_sz = Arch.word_size_of arch in
  let endaddr = Arch.instr_end_addr arch instr ~addr in
  let switch_arch () =
    match B.ctrl_of blk with
    | C.Call (_, _, Some (Some s)) | C.Jmp (_, Some (Some s)) -> (
      try Arch.of_string s
      with Invalid_argument _ ->
        (* we shouldn't continue, since the lifter
         * gave us an invalid ISA to switch to,
         * which is indicative of a programmer error *)
        failwith
          (Printf.sprintf "0x%s: switch from %s to invalid arch %s"
             (Bitvec.to_string addr) (Arch.to_string arch) s ) )
    | _ -> Arch.(T arch)
  in
  let dest_opt dest reason =
    match dest with
    | C.Indirect Il.(Const c) when c.size = word_sz -> (
      match reason with
      | Reason.Direct ->
          let Arch.(T arch') = switch_arch () in
          let decode_ctx = Arch.switch_decode_ctx arch arch' decode_ctx in
          new_target t ~addr:c.value
            ~arch_ctx:Arch.(C (arch', decode_ctx))
            ~reason ~enqueue
      | _ ->
          let decode_ctx = Arch.clone_decode_ctx arch decode_ctx in
          new_target t ~addr:c.value
            ~arch_ctx:Arch.(C (arch, decode_ctx))
            ~reason ~enqueue )
    | _ -> None
  in
  let dest_edge d e =
    Reason.of_code_edge e |> dest_opt d |> Option.iter ~f:(new_edge t addr e)
  in
  (* explore control flow *)
  let ret = Option.iter ~f:(fun dr -> dest_edge dr CE.Return) in
  let noreturn_addr = ref None in
  ( match B.ctrl_of blk with
  | C.Call (d, dr, _) ->
      let noreturn =
        let instr = Some (addr, Arch.(I (arch, instr))) in
        match d with
        | C.Indirect Il.(Const c) ->
            let dst, _ = Arch.addr_of_arch Arch.(T arch) c.value in
            Image.is_known_noreturn t.program.img ~dst ~instr
        | C.Indirect Il.(Load {loc= Const c; bytes})
          when bytes = word_sz lsr 3 ->
            Image.is_known_noreturn t.program.img ~dst:c.value ~instr
              ~indirect:true
        | _ -> false
      in
      dest_edge d CE.Direct;
      if not (noreturn || Image.is_stub_addr t.program.img addr) then ret dr
      else (
        Ir.map_ctrl_direct t.program.ir (B.label_of blk) ~f:(function
          | C.Call (d, _, s) -> C.Call (d, None, s)
          | x -> x )
        |> ignore;
        match dr with
        | Some (C.Indirect Il.(Const c)) -> noreturn_addr := Some c.value
        | _ -> () )
  | C.Jmp (d, _) ->
      (* should we treat this as a fallthrough? *)
      let is_terminator =
        if Arch.is_terminator_instr arch instr then true
        else
          Ir.blocks_of_addr t.program.ir addr
          |> List.exists ~f:(fun blk' ->
                 if L.equal (B.label_of blk) (B.label_of blk') then false
                 else
                   match B.ctrl_of blk' with
                   | C.Call _ -> true
                   | C.(JmpIf (_, Indirect _, _)) -> true
                   | C.(Jmp (Indirect e, _)) -> (
                     match e with
                     | Il.Const c -> Addr.(endaddr <> c.value)
                     | _ -> true )
                   | _ -> false )
      in
      if is_terminator then dest_edge d CE.Direct
      else dest_edge d CE.Fallthrough
  | C.JmpIf (_, dt, df) ->
      dest_edge dt CE.Direct;
      dest_edge df CE.Fallthrough
  | C.Special (_, dr) -> ret dr );
  (* mark delay slot edges/reasons *)
  ( match Arch.delay_slot_size arch instr with
  | n when n <= 0 -> ()
  | n ->
      let rec aux i off =
        if i >= n then ()
        else
          let a = Addr.(endaddr + int off) in
          match Code.instr_of_addr t.program.code a with
          | None -> ()
          | Some Arch.(I (arch, instr)) ->
              new_edge t addr CE.Delay a;
              set_seen t a Delay;
              aux (i + 1) (off + Arch.instr_length arch instr)
      in
      aux 0 0 );
  (* find simple constants in the definitions *)
  if enqueue then
    let new_literal value =
      let value', _ = Arch.addr_of_arch Arch.(T arch) value in
      if Bitvec.(value' <> addr) then
        match !noreturn_addr with
        (* in the case that we found a known noreturn call,
         * the return address will still be written to some
         * destination. however, we shouldn't investigate it. *)
        | Some addr when Bitvec.(addr = value') -> ()
        | _ -> new_literal t value
    in
    B.iter_defs blk ~f:(fun (_, src) ->
        match src with
        | Il.Const c when c.size = word_sz -> new_literal c.value
        | Il.Store s -> (
          match s.src with
          | Il.Const c when c.size = word_sz -> new_literal c.value
          | _ -> () )
        | _ -> () )

(* check for overlaps stemming from literals and
 * harvested global data, since these are more likely
 * to be spurious than other jump targets *)
let remove_global_and_literal_overlaps t =
  Hashtbl.iteri t.seen ~f:(fun ~key ~data ->
      match Code.instr_of_addr t.program.code key with
      | None -> ()
      | Some Arch.(I (arch, instr)) ->
          if
            (not (Reasons.mem data Entry))
            && Reasons.(mem data Global || mem data Literal)
            && has_overlap t ~arch ~instr ~addr:key
          then invalidate t ~addr:key )

(* all addresses discovered via a mandatory
 * fallthrough must have a predecessor *)
let remove_spurious_fallthroughs t =
  let s = Reasons.(singleton Fallthrough) in
  let is_mandatory reasons =
    (* here we say that the target is a mandatory fallthrough
     * if we discovered it for only that reason *)
    Set.equal reasons s
  in
  Hashtbl.fold t.seen ~init:[] ~f:(fun ~key ~data addrs ->
      if Ir.has_addr t.program.ir key && is_mandatory data then key :: addrs
      else addrs )
  |> List.sort ~compare:Addr.compare
  |> List.iter ~f:(fun addr ->
         let preds = Code.(Cfg.predecessors (cfg_of t.program.code) addr) in
         let f (_, edges) = List.exists edges ~f:CE.(equal Fallthrough) in
         if not @@ List.exists preds ~f then invalidate t ~addr )

(* check for unresolved constant targets in the IR
 * since if the address already existed then the
 * destination would be to another block *)
let remove_dead_ends t =
  Ir.fold_blocks t.program.ir ~init:[] ~f:(fun addrs blk ->
      let addr = B.addr_of blk in
      let dsts =
        match B.ctrl_of blk with
        | C.Call (d, _, _) -> [d]
        | C.Jmp (d, _) -> [d]
        | C.JmpIf (_, dt, df) -> [dt; df]
        | _ -> []
      in
      let f = function
        (* Ir.resolve_const_indirects should have
         * turned this into a Direct dest, so
         * the instruction was likely invalid *)
        | C.Indirect (Il.Const _) -> true
        | _ -> false
      in
      if List.exists dsts ~f then addr :: addrs else addrs )
  |> List.iter ~f:(fun addr -> invalidate t ~addr)

(* construct the native code CFG *)
let update_code_cfg t =
  Stack.iter t.new_edges (fun (a, e, a') ->
      if not (is_invalid t a' || is_data t a') then
        Code.insert_edge t.program.code a e a' |> ignore );
  Stack.clear t.new_edges

(* fix delay slot targets in the IR *)
let fix_delay_targets t =
  let ir = t.program.ir in
  let cfg = Ir.cfg_of ir in
  Code.fold_instrs t.program.code ~init:[]
    ~f:(fun delays ~key:addr ~data:Arch.(I (arch, instr)) ->
      if
        Arch.delay_slot_size arch instr > 0
        && not (Arch.is_conditional_branch arch instr)
      then
        Ir.blocks_of_addr ir addr |> List.hd
        |> function
        | None -> delays
        | Some instr_blk -> (
            Arch.instr_end_addr arch instr ~addr
            |> Ir.blocks_of_addr ir |> List.find ~f:B.is_delay
            |> function
            | None -> delays
            | Some delay_blk ->
                let pred_blks =
                  B.label_of instr_blk
                  |> Ir.Cfg.fold_preds cfg ~init:[] ~f:(fun acc l _ ->
                         Ir.block ir l
                         |> function
                         | Some blk when B.is_delay blk |> not -> blk :: acc
                         | _ -> acc )
                in
                (instr_blk, delay_blk, pred_blks) :: delays )
      else delays )
  |> List.iter ~f:(fun (instr_blk, delay_blk, pred_blks) ->
         let instr_lbl = B.label_of instr_blk in
         let delay_lbl = B.label_of delay_blk in
         (* for all edges from the predecessor to the instruction,
          * redirect them to the delay slot instead *)
         List.fold pred_blks ~init:Lset.empty ~f:(fun visited pred_blk ->
             let pred_lbl = B.label_of pred_blk in
             if Set.mem visited pred_lbl then visited
             else (
               Ir.map_ctrl_direct t.program.ir pred_lbl ~f:(function
                 | C.(Call (d, Some (Direct l), s)) when L.equal l instr_lbl
                   -> C.(Call (d, Some (Direct delay_lbl), s))
                 | C.(Jmp (Direct l, s)) when L.equal l instr_lbl ->
                     C.(Jmp (Direct delay_lbl, s))
                 | C.(JmpIf (e, Direct l1, Direct l2))
                   when L.equal l1 instr_lbl && L.equal l2 instr_lbl ->
                     C.(JmpIf (e, Direct delay_lbl, Direct delay_lbl))
                 | C.(JmpIf (e, Direct l, d)) when L.equal l instr_lbl ->
                     C.(JmpIf (e, Direct delay_lbl, d))
                 | C.(JmpIf (e, d, Direct l)) when L.equal l instr_lbl ->
                     C.(JmpIf (e, d, Direct delay_lbl))
                 | C.(Special (s, Some (Direct l))) when L.equal l instr_lbl
                   -> C.(Special (s, Some (Direct delay_lbl)))
                 | ctrl -> ctrl )
               |> ignore;
               Set.add visited pred_lbl ) )
         |> ignore )

(* mark the literals for exploration *)
let enqueue_literals t =
  let changed =
    let Arch.(T arch) = t.program.img.arch in
    Stack.fold t.new_literals ~init:false ~f:(fun changed addr ->
        (* we'll have more luck if we assume the default arch *)
        let addr, _ = Arch.addr_of_arch Arch.(T arch) addr in
        let decode_ctx = Arch.create_decode_ctx arch in
        let arch_ctx = Arch.(C (arch, decode_ctx)) in
        let changed' =
          new_target t ~addr ~arch_ctx ~reason:Literal |> Option.is_some
        in
        changed || changed' )
  in
  Stack.clear t.new_literals;
  changed

(* mark the code pointers for exploration.
 * this should only happen once! *)
let enqueue_code_pointers t =
  let changed =
    Stack.fold t.code_pointers ~init:false
      ~f:(fun changed (addr, Arch.(T arch)) ->
        let decode_ctx = Arch.create_decode_ctx arch in
        let arch_ctx = Arch.(C (arch, decode_ctx)) in
        let changed' =
          new_target t ~addr ~arch_ctx ~reason:Global |> Option.is_some
        in
        changed || changed' )
  in
  Stack.clear t.code_pointers;
  changed

(* analyze indirect control flow *)
let analyze_indirect ?(enqueue = true) ?(check_bounds = true) t =
  let Indirects.{targets; loads} =
    Indirects.analyze t.program t.indirect ~check_bounds
  in
  let ok =
    List.fold targets ~init:false ~f:(fun ok (l, addr, arch_ctx) ->
        Option.(
          value ~default:ok
            ( Ir.addr_of_block t.program.ir l
            >>= fun src ->
            new_target t ~addr ~arch_ctx ~reason:Indirect ~enqueue
            >>| fun _ ->
            new_edge t src CE.Indirect addr;
            new_indirect_edge t addr l;
            true )) )
  in
  if ok then List.iter loads ~f:(mark_data t);
  ok

(* we queued up some indirect edges to be added to the CFG *)
let mark_indirect_edges t =
  let ir = t.program.ir in
  Stack.iter t.indirect_edges ~f:(fun (addr, src) ->
      Ir.blocks_of_addr ir addr
      |> List.find ~f:(fun blk -> B.is_delay blk |> not)
      |> Option.iter ~f:(fun blk ->
             let dst = B.label_of blk in
             Ir.insert_edge_indirect ir ~src ~dst |> ignore ) );
  Stack.clear t.indirect_edges

(* find calls to blocks which are post-dominated
 * by a call to a noreturn function, and then
 * propagate this information to all call sites
 * impacted by this. we keep iterating until
 * a fixed point is reached.
 *
 * XXX: can we avoid recomputing function boundaries on each iteration?
 * a look at the profiling results shows that, while doing so would
 * present a performance improvement, the majority of the time is being
 * spent calculating the dominators. *)
let analyze_noreturn t =
  let ir = t.program.ir in
  let img = t.program.img in
  let code = t.program.code in
  let cfg = Ir.cfg_of ir in
  let changed = ref true in
  (* we need to insert a fake `exit` node into each
   * subroutine. the real exit nodes will all have
   * incoming edges to this node. this is required
   * to correclty compute the dominators. *)
  let dummy_exit = Ir.new_label ir in
  let starts = entry_addrs t in
  while !changed do
    changed := false;
    (* dominators are computed for each (approximated) subroutine,
     * since our analysis is based on a caller/callee relationship. *)
    let subs = Program.subroutines t.program ~starts in
    List.iter subs#subroutines ~f:(fun sub ->
        let scfg = S.cfg_of sub in
        Ir.Cfg.fold_vertices scfg ~init:[] ~f:(fun acc l ->
            if Ir.Cfg.has_successors scfg l then acc else l :: acc )
        |> List.iter ~f:(fun l ->
               Ir.Cfg.insert_edge scfg l E.Indirect dummy_exit ) );
    (* find all noreturns *)
    let noreturn_blks =
      Ir.Cfg.fold_vertices cfg ~init:[] ~f:(fun acc l ->
          match Ir.addr_of_block ir l with
          | Some addr when not (Image.is_stub_addr img addr) -> (
            match Ir.block_exn ir l |> B.ctrl_of with
            | Call (_, None, _) | Special (_, None) -> l :: acc
            | _ -> acc )
          | _ -> acc )
    in
    (* compute post dominators on-demand *)
    let post_doms =
      let m = Hashtbl.create (module L) in
      fun l ->
        let open Option.Let_syntax in
        let%map sub = subs#sub_of_block l in
        let entry = S.entry_of sub in
        let scfg = S.cfg_of sub in
        match Hashtbl.find m entry with
        | Some post_doms -> post_doms
        | None ->
            let post_doms =
              Ir.Cfg.post_dominators scfg dummy_exit ~compute_df:false
            in
            Hashtbl.set m entry post_doms;
            post_doms
    in
    (* find all blocks that are post-dominated by the noreturns,
     * up until the entry point of the corresponding subroutine *)
    let sinks =
      List.fold noreturn_blks ~init:Lset.empty ~f:(fun acc l ->
          match post_doms l with
          | None -> acc
          | Some post_doms ->
              post_doms#strictly_dominating l
              |> Lset.of_list |> Set.union acc )
    in
    (* now, find all nodes which call a node in the set of sinks.
     * we also collect the corresponding return nodes which
     * were only discovered as the result of a return (ignoring
     * observed literals and global data).
     * we could use the dominators for this purpose, but
     * observing the `reasons` gives us a bit more precision. *)
    let calls, rets =
      Ir.Cfg.fold_vertices cfg ~init:([], []) ~f:(fun (calls, rets) l ->
          match Ir.block_exn ir l |> B.ctrl_of with
          | Call (Direct l', Some (Direct lr), _) when Set.mem sinks l' ->
              let rets =
                if
                  let ar = Ir.addr_of_block_exn ir lr in
                  let sr = Hashtbl.find_exn t.seen ar in
                  Reasons.(
                    equal
                      (remove (remove sr Global) Literal)
                      (singleton Return))
                then lr :: rets
                else rets
              in
              (l :: calls, rets)
          | _ -> (calls, rets) )
    in
    (* mark all the collected calls as noreturn *)
    List.iter calls ~f:(fun l ->
        Ir.map_ctrl_direct ir l ~f:(function
          | Call (d, Some (Direct lr), s) ->
              let a = Ir.addr_of_block_exn ir l in
              let ar = Ir.addr_of_block_exn ir lr in
              Code.remove_edge code a CE.Return ar;
              changed := true;
              Call (d, None, s)
          | c -> c )
        |> ignore );
    (* compute dominators on-demand *)
    let doms =
      let m = Hashtbl.create (module L) in
      fun l ->
        let open Option.Let_syntax in
        let%map sub = subs#sub_of_block l in
        let entry = S.entry_of sub in
        let scfg = S.cfg_of sub in
        match Hashtbl.find m entry with
        | Some doms -> doms
        | None ->
            let doms = Ir.Cfg.dominators scfg entry ~compute_df:false in
            Hashtbl.set m entry doms; doms
    in
    (* finally, we need to invalidate nodes that were
     * made unreachable by discovering this noreturn *)
    let invalids = Vector.create () in
    List.iter rets ~f:(fun l ->
        match doms l with
        | None -> ()
        | Some doms ->
            (* traverse the dominator tree and collect unreachable nodes *)
            let wl = Stack.singleton l and lret = l in
            let check l =
              Option.(
                value ~default:false
                  ( Ir.addr_of_block ir l
                  >>= fun a ->
                  Hashtbl.find t.seen a
                  >>= fun s ->
                  some_if
                    (* we should stop the traversal when we
                     * encounter a node that should be reachable
                     * regardless of being in this dominator tree. *)
                    ( ( Reasons.(mem s Entry)
                      || Ir.Cfg.find_predecessor cfg l ~f:(fun lp _ ->
                             doms#is_dominating lret lp |> not )
                         |> is_some )
                    |> not )
                    ()
                  >>| fun _ ->
                  Vector.push_back invalids a;
                  true ))
            in
            while Stack.is_empty wl |> not do
              let l = Stack.pop_exn wl in
              if check l then
                doms#immediately_dominated_by l
                |> List.iter ~f:(Stack.push wl)
            done );
    Vector.sort invalids ~compare:(fun a a' -> Addr.compare a' a);
    let visited = Hash_set.create (module Addr) in
    Vector.iter invalids ~f:(fun addr ->
        Hash_set.strict_add visited addr
        |> Result.iter ~f:(fun _ -> invalidate t ~addr) );
    changed := !changed || Hash_set.is_empty visited |> not
  done

(* preliminary pruning of bad speculated instructions *)
let remove_speculative_errors t =
  let code = t.program.code in
  let cfg = Code.cfg_of code in
  Hashtbl.iteri t.seen ~f:(fun ~key:addr ~data ->
      if Reasons.mem data Speculated then
        match Code.instr_of_addr code addr with
        | None -> ()
        | Some Arch.(I (arch, instr)) ->
            if
              (* check if we have an overlap with an instruction
               * that was not discovered speculatively. this is
               * a very strong hint for spurious instructions. *)
              overlaps t ~arch ~instr ~addr
              |> Set.filter ~f:(fun addr' ->
                     has_seen t addr'
                     && has_seen_reason t addr' Speculated |> not )
              |> Set.is_empty |> not
            then invalidate t ~addr
            else if
              (* check if we had a fallthrough edge to an instruction
               * that was not discovered speculatively. this relies
               * on some assumptions about how targets can be reached. *)
              Arch.must_fallthrough_instr arch instr
              && Code.Cfg.find_successor cfg addr ~f:(fun addr' e ->
                     if
                       has_seen t addr'
                       && has_seen_reason t addr' Speculated |> not
                     then
                       match e with
                       | CE.Fallthrough -> true
                       | _ -> false
                     else false )
                 |> Option.is_some
            then invalidate t ~addr )

(* speculatively disassemble the unexplored regions
 * and try to classify all false positives *)
let speculate t =
  let ir = t.program.ir in
  let code = t.program.code in
  let img = t.program.img in
  let code_pointers = Image.code_pointers img in
  (* collect all valid unexplored regions *)
  let regions =
    Memory.fold img.mem ~init:[] ~f:(fun regions addr seg ->
        if Memory.Segment.is_executable seg then
          let limit = Memory.Segment.end_address seg in
          let rec loop regions start addr =
            let new_regions () =
              if Addr.(start = addr) then regions
              else (start, Bitvec.min addr limit) :: regions
            in
            if Addr.(addr >= limit) then new_regions ()
            else
              match Code.instr_of_addr code addr with
              | None ->
                  let addr' = Addr.succ addr in
                  if is_data t addr || is_invalid t addr then
                    loop (new_regions ()) addr' addr'
                  else loop regions start addr'
              | Some Arch.(I (arch, instr)) ->
                  let addr' = Arch.instr_end_addr arch instr ~addr in
                  loop (new_regions ()) addr' addr'
          in
          loop regions addr addr
        else regions )
  in
  (* speculatively disassemble the regions
   * 
   * NOTE: handling interworking (e.g. ARM/Thumb) is tricky
   * as it would involve duplicating the work for each
   * discovered address. currently our data structures are not
   * designed to handle such a situation, so we will assume that
   * all addresses are speculated with the default arch. *)
  let Arch.(T arch) = img.arch in
  List.iter regions ~f:(fun (addr, limit) ->
      let decode_ctx = Arch.create_decode_ctx arch in
      let rec loop addr =
        if Addr.(addr < limit) then (
          ( if
            Image.is_valid_pc img addr
              ~arch:(Some Arch.(T arch))
              ~check_seg:false
          then
            match translate t ~arch ~decode_ctx ~addr ~reason:Speculated with
            | Some instr
              when Addr.(Arch.instr_end_addr arch instr ~addr <= limit) ->
                set_seen t addr Speculated;
                Ir.iter_blocks_of_addr ir addr ~f:(fun blk ->
                    explore_block t ~arch ~decode_ctx ~instr ~blk ~addr
                      ~enqueue:false )
            | Some _ -> invalidate t ~addr
            | _ -> mark_invalid t addr );
          Addr.succ addr |> loop )
      in
      loop addr );
  (* preliminary pruning of invalid instruction chains *)
  Ir.resolve_const_indirects ir |> ignore;
  update_code_cfg t;
  synchronize t;
  remove_spurious_fallthroughs t;
  remove_speculative_errors t;
  remove_dead_ends t;
  (* gather conflicts between instructions *)
  let overlap = Hashtbl.create (module Addr) in
  Hashtbl.iteri t.seen ~f:(fun ~key:addr ~data:reasons ->
      if Reasons.mem reasons Speculated then
        Code.instr_of_addr code addr
        |> Option.iter ~f:(fun Arch.(I (arch, instr)) ->
               let o = overlaps t ~arch ~instr ~addr in
               if Set.is_empty o |> not then Hashtbl.set overlap addr o ) );
  (* give a score for a particular address *)
  let evaluate addr =
    let code_cfg = Code.cfg_of code in
    let visited = Hash_set.create (module Addr) in
    let rec aux addr =
      match Hash_set.strict_add visited addr with
      | Error _ ->
          (* give points for back-edges *)
          1
      | Ok () ->
          (* aggregate score of predecessors *)
          let score_preds =
            Code.Cfg.fold_preds_ignore_edges code_cfg addr ~init:0
              ~f:(fun score addr -> score + aux addr)
          in
          (* extra points if we have an outgoing edge to an
           * address that was not speculatively discovered *)
          let score_succs =
            Code.Cfg.fold_succs_ignore_edges code_cfg addr ~init:0
              ~f:(fun score addr ->
                if
                  has_seen t addr && has_seen_reason t addr Speculated |> not
                then score + 2
                else score )
          in
          (* extra points for alignment *)
          let score_align =
            match Code.instr_of_addr code addr with
            | None -> 0
            | Some Arch.(I (arch, _)) ->
                let align = Arch.word_size_of arch lsr 3 in
                if Addr.(rem addr (int align) = zero) then 1 else 0
          in
          (* extra points for accessing global data *)
          let score_data_access =
            match Ir.blocks_of_addr ir addr with
            | [] -> 0
            | blks ->
                if
                  let is_access = function
                    | Il.Const c ->
                        Memory.find_segment img.mem c.value |> Option.is_some
                    | _ -> false
                  in
                  List.exists blks ~f:(fun blk ->
                      B.alist_of_defs blk
                      |> List.exists ~f:(fun (_, (_, src)) ->
                             match src with
                             | Il.Store s ->
                                 is_access s.r.loc || is_access s.src
                             | Il.Load r -> is_access r.loc
                             | _ -> false ) )
                then 1
                else 0
          in
          (* extra points for appearing in global data *)
          let score_global = if Map.mem code_pointers addr then 1 else 0 in
          (* extra points for fitting against another instruction *)
          let score_fitting =
            let bwd =
              (* instructions that end where this instruction starts *)
              match Hashtbl.find t.instrs_end addr with
              | Some s ->
                  let s =
                    Set.filter s ~f:(fun a ->
                        has_seen t a && Ir.has_addr ir a )
                  in
                  if Set.is_empty s then 0
                  else if
                    (* bias towards instructions that were not speculated *)
                    Set.exists s ~f:(fun a ->
                        has_seen_reason t a Speculated |> not )
                  then 2
                  else 1
              | _ -> 0
            in
            let fwd =
              (* instructions that start where this instruction ends *)
              let Arch.(I (arch, instr)) =
                Code.instr_of_addr_exn code addr
              in
              let endaddr = Arch.instr_end_addr arch instr ~addr in
              if Ir.has_addr ir endaddr && has_seen t endaddr then
                (* bias towards instructions that were not speculated *)
                if has_seen_reason t endaddr Speculated then 1 else 2
              else 0
            in
            bwd + fwd
          in
          (* final score *)
          score_preds + score_succs + score_align + score_data_access
          + score_global + score_fitting + 1
    in
    aux addr
  in
  (* get all unresolved conflicts *)
  let unresolved () =
    Hashtbl.to_alist overlap
    |> List.map ~f:(fun (a, s) ->
           Set.to_list s |> List.map ~f:(fun a' -> (a, a')) )
    |> List.concat
    |> List.filter ~f:(fun (a, a') -> Ir.(has_addr ir a && has_addr ir a'))
    |> List.dedup_and_sort ~compare:(fun (a1, a1') (a2, a2') ->
           match Addr.compare a1 a2 with
           | 0 -> Addr.compare a1' a2'
           | n -> n )
  in
  (* iterate until fixpoint *)
  let changed = ref true in
  let u = unresolved () |> ref in
  while !changed do
    changed := false;
    (* use the evaluation function when iterating over
     * each conflict, and prune the instruction with
     * the lower score, keeping ties *)
    List.iter !u ~f:(fun (a, a') ->
        if Ir.(has_addr ir a && has_addr ir a') then
          let s = evaluate a in
          let s' = evaluate a' in
          if s > s' then (
            changed := true;
            invalidate t a' )
          else if s < s' then (
            changed := true;
            invalidate t a ) );
    (* check to see if anything changed *)
    if !changed then
      u :=
        List.filter !u ~f:(fun (a1, a2) ->
            Ir.(has_addr ir a1 && has_addr ir a2) )
  done;
  (* TODO: find a way to break ties with the remaining conflicts *)
  (* make sure that we connect indirect edges 
   * from the newly discovered targets *)
  if analyze_indirect t ~enqueue:false ~check_bounds:false then (
    mark_indirect_edges t; update_code_cfg t; synchronize t );
  ()

(* depth-first recursive traversal disassembler *)
let explore ?(spec = false) ?(noreturn_analysis = false) t =
  (* main loop *)
  let rec loop () =
    match next_target t with
    | Some (addr, Arch.(C (arch, decode_ctx)), reason) ->
        (* attempt to decode and lift the instruction at this address *)
        ( match translate t ~arch ~decode_ctx ~addr ~reason with
        | None -> mark_invalid t addr
        | Some instr ->
            (* explore the generated IR blocks of this instruction *)
            Ir.iter_blocks_of_addr t.program.ir addr ~f:(fun blk ->
                explore_block t ~arch ~decode_ctx ~instr ~blk ~addr ) );
        loop ()
    | None ->
        (* run some passes to update the IR/code CFG
         * and then prune invalid instruction chains *)
        Ir.resolve_const_indirects t.program.ir |> ignore;
        mark_indirect_edges t;
        update_code_cfg t;
        synchronize t;
        remove_global_and_literal_overlaps t;
        remove_spurious_fallthroughs t;
        remove_dead_ends t;
        fix_delay_targets t;
        if analyze_indirect t then loop ()
        else if enqueue_literals t then loop ()
        else if enqueue_code_pointers t then loop ()
  in
  loop ();
  if spec then speculate t;
  if noreturn_analysis then analyze_noreturn t;
  t.program

type disasm_result = {p: Program.t; starts: Addr.Set.t}

let run ?(use_code_pointers = false) ?(spec = false)
    ?(noreturn_analysis = false) img =
  let t = create img ~use_code_pointers in
  if has_new_targets t then
    let p = explore t ~spec ~noreturn_analysis in
    let starts = entry_addrs t in
    {p; starts}
  else failwith "no valid addresses to explore"
