open Core_kernel
open Ll2l_arch
open Ll2l_image
open Ll2l_ir
open Ll2l_std
module B = Ir.Block
module C = Ir.Ctrl
module E = Ir.Edge
module L = Ir.Label
module G = Ir.Slice_graph

(* match common patterns for jump table access *)
let jump_tab_idx e Arch.(T arch) =
  let word_sz = Arch.word_size_of arch in
  match e with
  (* AMD64 table-relative jump, assuming that
   * the base hasn't been resolved yet *)
  | Il.(
      Binop
        ( ADD
        , Cast
            ( SEXT
            , csz
            , Load
                { bytes
                ; loc=
                    Binop
                      ( ADD
                      , Var vb1
                      , Binop (MUL, Var vi, Const {value= stride}) ) } )
        , Var vb2 ))
    when Il.equal_var vb1 vb2 && csz = word_sz
         && bytes = word_sz lsr 4
         && Bitvec.to_int stride = word_sz lsr 4 -> (Some vi, None)
  | Il.(
      Binop
        ( ADD
        , Cast
            ( SEXT
            , csz
            , Load
                { bytes
                ; loc=
                    Binop
                      ( ADD
                      , Var vb
                      , Binop (MUL, Var vi, Const {value= stride}) ) } )
        , Const {value= b2} ))
    when csz = word_sz
         && bytes = word_sz lsr 4
         && Bitvec.to_int stride = word_sz lsr 4 -> (Some vi, Some vb)
  (* absolute address loaded from table (base is unresolved) *)
  | Il.(
      Load
        { bytes
        ; loc= Binop (ADD, Var vb, Binop (MUL, Var vi, Const {value= stride}))
        })
    when bytes = word_sz lsr 3 && Bitvec.to_int stride = word_sz lsr 3 ->
      (Some vi, None)
  (* XXX: where did this pattern come from? *)
  | Il.(
      Load
        { bytes
        ; loc=
            Binop
              ( ADD
              , Var vb
              , Cast
                  ( LOW
                  , lsz
                  , Binop
                      (MUL, Cast (SEXT, esz, Var vi), Const {value= stride})
                  ) ) })
    when lsz = word_sz
         && esz = word_sz lsl 1
         && bytes = word_sz lsr 3
         && Bitvec.to_int stride = word_sz lsr 3 -> (Some vi, None)
  (* common on IA-32 *)
  | Il.(
      Load
        { bytes
        ; loc=
            Binop (ADD, Binop (MUL, Var vi, Const {value= stride}), Const _)
        })
    when bytes = word_sz lsr 3 && Bitvec.to_int stride = word_sz lsr 3 ->
      (Some vi, None)
  | Il.(
      Load
        { bytes
        ; loc=
            Binop (ADD, Binop (SHL, Var vi, Const {value= stride}), Const _)
        })
    when bytes = word_sz lsr 3 && Bitvec.to_int stride = word_sz lsr 4 ->
      (Some vi, None)
  (* Thumb TBH/TBB *)
  | Il.(
      Binop
        ( ADD
        , Const {value= b1}
        , Binop
            ( MUL
            , Cast
                ( ZEXT
                , csz
                , Load {loc= Binop (ADD, Const {value= b2}, Var vi)} )
            , Const _ ) ))
    when csz = word_sz && Bitvec.equal b1 b2 -> (Some vi, None)
  | Il.(
      Binop
        ( ADD
        , Const {value= b1}
        , Binop
            ( MUL
            , Cast
                ( ZEXT
                , csz
                , Load
                    { loc=
                        Binop
                          ( ADD
                          , Const {value= b2}
                          , Binop (SHL, Var vi, Const _) ) } )
            , Const _ ) ))
    when csz = word_sz && Bitvec.equal b1 b2 -> (Some vi, None)
  (* special case with Thumb where we already have
   * an upper-bound on the size of the table
   * XXX: probably not necessary to check the size of the stride *)
  | Il.(
      Binop (ADD, Const _, Binop (SHL, Binop (SUB, Const _, Var vi), Const _)))
    -> (Some vi, None)
  (* no load from memory, just base + index*scale (common on ARM) *)
  | Il.(Binop (ADD, Const _, Binop (SHL, Var vi, Const {value= stride})))
    when Bitvec.to_int stride = word_sz lsr 4 -> (Some vi, None)
  | Il.(Binop (ADD, Const _, Binop (MUL, Var vi, Const {value= stride})))
    when Bitvec.to_int stride = word_sz lsr 3 -> (Some vi, None)
  (* common patterns on MIPS64 *)
  | Il.(
      Binop
        ( ADD
        , Load
            { bytes
            ; loc=
                Binop
                  ( ADD
                  , _
                  , Binop
                      ( SHL
                      , Cast (ZEXT, csz, Extract (hi, 0, Var vi))
                      , Const {value= stride} ) ) }
        , _ ))
    when 1 lsl Bitvec.to_int stride = word_sz lsr 3
         && csz = word_sz
         && bytes = word_sz lsr 3
         && succ hi = word_sz lsr 1 -> (Some vi, None)
  | Il.(
      Binop
        ( ADD
        , Load
            { bytes
            ; loc=
                Binop
                  ( ADD
                  , _
                  , Binop
                      ( SHL
                      , Binop (AND, Var vi, Const _)
                      , Const {value= stride} ) ) }
        , _ ))
    when 1 lsl Bitvec.to_int stride = word_sz lsr 3 && bytes = word_sz lsr 3
    -> (Some vi, None)
  | Il.(
      Binop
        ( ADD
        , Load
            { bytes
            ; loc=
                Binop (ADD, Var _, Binop (SHL, Var vi, Const {value= stride}))
            }
        , _ ))
    when bytes = word_sz lsr 3 && 1 lsl Bitvec.to_int stride = word_sz lsr 3
    -> (Some vi, None)
  | _ -> (None, None)

let is_var_relevant Arch.(T arch) canonical_values (v : Il.var) =
  Map.mem canonical_values v |> not
  && Arch.is_stack_pointer_var arch v |> not
  &&
  match v.hint with
  | Il.Var_hint.Mem -> false
  | _ -> true

(* build an expression for the jump target by traversing our
 * backwards slice results, since it is quite likely that the
 * jump target is materialized over an arbitrary number of operations.
 * also, try to find a variable which represents the index into a jump table. *)
let build_exp_target ir l e arch canonical_values =
  let vs = Il.(vars_of e |> Var_set.of_list) in
  let Ir.{g; defs} = Ir.slice_backward ir l vs in
  let e = ref e in
  let idx = ref None in
  let rec build_exp l v i =
    match Ir.block ir l with
    | None -> false
    | Some blk -> (
        let default () =
          G.predecessors g l
          |> List.exists ~f:(fun (l', _) -> build_exp l' v None)
        in
        match Hashtbl.find defs l with
        | None -> default ()
        | Some _ -> (
            let d =
              match i with
              | None -> B.last_def_of_var blk v
              | Some i -> B.last_def_of_var_before blk v i
            in
            match d with
            | Some (i, (_, e')) -> (
                e := Il.substitute_expr !e (Il.Var v, e');
                match jump_tab_idx !e arch with
                | Some vi, vb -> (
                    idx := Some (vi, l);
                    match vb with
                    | None -> true
                    | Some vb -> build_exp l vb (Some i) )
                | None, _ ->
                    Il.vars_of e'
                    |> List.filter ~f:(is_var_relevant arch canonical_values)
                    |> List.for_all ~f:(fun v' -> build_exp l v' (Some i)) )
            | None -> default () ) )
  in
  Set.iter vs ~f:(fun v -> build_exp l v None |> ignore);
  (!e, !idx)

(* build an expression which represents the constraints on the table index. *)
let build_exp_bnds ir g defs l e arch canonical_values =
  let e = ref e in
  let astart = Ir.addr_of_block_exn ir l in
  let rec build_exp l v i =
    match Ir.block ir l with
    | None -> false
    | Some blk -> (
        let default () =
          G.predecessors g l
          |> List.exists ~f:(fun (l', _) -> build_exp l' v None)
        in
        match Hashtbl.find defs l with
        | None -> default ()
        | Some _ -> (
            let d =
              match i with
              | None -> B.last_def_of_var blk v
              | Some i -> B.last_def_of_var_before blk v i
            in
            match d with
            | Some (i, (_, e')) -> (
                e := Il.substitute_expr !e (Il.Var v, e');
                (* XXX: actually check if this has anything to do with the
                   index. *)
                match e' with
                (* this is a spectacularly dumb hack! *)
                | Il.Binop (EQ, Var v', Const {value})
                  when Addr.(astart = Ir.addr_of_block_exn ir l)
                       && Bitvec.(value = one || value = zero) ->
                    ( if Bitvec.(value = one) then e := Il.Var v'
                    else e := Il.(Unop (NOT, Var v')) );
                    build_exp l v' (Some i)
                | Il.Binop (EQ, _, _)
                 |Il.Binop (NEQ, _, _)
                 |Il.Binop (LT, _, _)
                 |Il.Binop (LEQ, _, _)
                 |Il.Binop (GT, _, _)
                 |Il.Binop (GEQ, _, _)
                 |Il.Cast (ZEXT, _, Binop (EQ, _, _))
                 |Il.Cast (ZEXT, _, Binop (NEQ, _, _))
                 |Il.Cast (ZEXT, _, Binop (LT, _, _))
                 |Il.Cast (ZEXT, _, Binop (LEQ, _, _))
                 |Il.Cast (ZEXT, _, Binop (GT, _, _))
                 |Il.Cast (ZEXT, _, Binop (GEQ, _, _)) -> true
                | _ ->
                    Il.vars_of e'
                    |> List.filter ~f:(is_var_relevant arch canonical_values)
                    |> List.for_all ~f:(fun v' -> build_exp l v' (Some i)) )
            | None -> default () ) )
  in
  Il.vars_of !e |> List.iter ~f:(fun v -> build_exp l v None |> ignore);
  !e

(* find the constraints on a table index *)
let analyze_bnds ir dummy_start l vi size arch canonical_values =
  let Ir.{g; defs} =
    Ir.slice_backward ir l Il.Var_set.(singleton vi) ~max_depth:32
  in
  (* insert a fake entry node into the graph *)
  G.vertices g
  |> List.filter ~f:(fun l -> G.has_predecessors g l |> not)
  |> List.iter ~f:(fun l -> G.insert_edge g dummy_start () l);
  (* find the conditional jump that dominates the index *)
  let doms = G.dominators g dummy_start ~compute_df:false in
  let rec find_dom l =
    let open Option.Let_syntax in
    let%bind l' = doms#immediate_dominator_of l in
    let%bind blk' = Ir.block ir l' in
    match B.ctrl_of blk' with
    | JmpIf (e, _, Direct lf) -> return (e, l', L.equal lf l)
    | _ -> find_dom l'
  in
  match find_dom l with
  | None -> Interval.create_empty ~size
  | Some (e, l', is_neg) ->
      let e = build_exp_bnds ir g defs l' e arch canonical_values in
      let module B = Bitvec.Make (struct
        let modulus = Bitvec.modulus size
      end) in
      let rec eval = function
        | Il.Const {value; size} -> Interval.create_single ~value ~size
        | Il.(Binop (EQ, _, Const {value})) ->
            Interval.(create_single ~value ~size)
        | Il.(Binop (NEQ, _, Const {value})) ->
            Interval.(create_single ~value ~size |> inverse)
        | Il.(Binop (LT, _, Const {value})) ->
            Interval.create ~lo:B.zero ~hi:value ~size
        | Il.(Binop (LEQ, _, Const {value})) ->
            Interval.create ~lo:B.zero ~hi:B.(succ value) ~size
        | Il.(Binop (GT, _, Const {value})) ->
            Interval.(create ~lo:B.zero ~hi:B.(succ value) ~size |> inverse)
        | Il.(Binop (GEQ, _, Const {value})) ->
            Interval.(create ~lo:B.zero ~hi:value ~size |> inverse)
        | Il.(Binop (AND, e1, e2)) ->
            Interval.intersect (eval e1) (eval e2) ~range:Unsigned
        | Il.(Binop (OR, e1, e2)) ->
            Interval.union (eval e1) (eval e2) ~range:Unsigned
        | Il.(Unop (NOT, e)) -> eval e |> Interval.inverse
        | Il.(Cast (ZEXT, _, e)) -> eval e
        | _ -> Interval.create_full ~size
      in
      let bnds = eval e in
      if is_neg then Interval.inverse bnds else bnds

let max_bnds = Bitvec.int 0xFFFF

let scan_jumptab ?(check_bounds = true) ir e l l' vi size edges loads
    (eval_state : Ir_eval.t) arch var_env dummy_start =
  let m = Bitvec.modulus size in
  if check_bounds then (
    let bnds =
      match e with
      (* Thumb special case: the LHS of the subtraction
       * upper-bounds the size of the table *)
      | Il.(
          Binop
            ( ADD
            , Const _
            , Binop (SHL, Binop (SUB, Const {value}, Var _), Const _) )) ->
          Interval.create ~lo:Bitvec.zero ~hi:Bitvec.(succ value mod m) ~size
      | _ -> analyze_bnds ir dummy_start l' vi size arch var_env
    in
    if
      Interval.(is_empty bnds || is_larger_than bnds Bitvec.(max_bnds mod m))
      |> not
    then
      let n = Interval.lower bnds |> ref in
      let n' = Interval.upper bnds in
      while Bitvec.(!n <> n') do
        let var_env = Map.set eval_state.var_env vi Il.{value= !n; size} in
        let eval_state = {eval_state with var_env} in
        let res = Ir_eval.eval_expr eval_state e in
        ( match res.e with
        | Il.Const {value} ->
            Hashtbl.add_multi edges l value;
            Set.iter res.loads ~f:(Hash_set.add loads)
        | _ -> () );
        n := Bitvec.(succ !n mod m)
      done )
  else
    (* assume that the targets have been discovered independently *)
    let n = ref Bitvec.zero in
    let cont = ref true in
    while !cont do
      let var_env = Map.set eval_state.var_env vi Il.{value= !n; size} in
      let eval_state = {eval_state with var_env} in
      let res = Ir_eval.eval_expr eval_state e in
      ( match res.e with
      | Il.Const {value} ->
          if Ir.has_addr ir value then (
            Hashtbl.add_multi edges l value;
            Set.iter res.loads ~f:(Hash_set.add loads) )
          else cont := false
      | _ -> cont := false );
      n := Bitvec.(succ !n mod m)
    done

type indirects_result =
  {targets: (Ir.Label.t * Addr.t * Arch.ec) list; loads: Addr.t list}

(* quick and dirty indirect control transfer analysis.
 * NOTE: this analysis is totally unsound, it just happens to work sometimes :) *)
let analyze ?(check_bounds = true) (p : Program.t) visited =
  let ir = p.ir in
  let code = p.code in
  let img = p.img in
  let var_env = p.canonical_values in
  let mem_env =
    Il.Var_map.singleton Il.default_mem (Memory.make_env img.mem)
  in
  let eval_state = Ir_eval.create ~var_env ~mem_env () in
  let edges = Hashtbl.create (module L) in
  let loads = Hash_set.create (module Addr) in
  let dummy_start = Ir.new_label ir in
  Ir.iter_blocks ir ~f:(fun blk ->
      let l = B.label_of blk in
      match Hash_set.strict_add visited l with
      | Error _ -> ()
      | Ok () ->
          let addr = B.addr_of blk in
          Code.instr_of_addr code addr
          |> Option.iter ~f:(fun Arch.(I (arch, _)) ->
                 let ctrl = B.ctrl_of blk in
                 Option.iter
                   ( match ctrl with
                   | Jmp (Indirect e, _) | Call (Indirect e, _, _) ->
                       let e, idx =
                         (* check if the expression already matches
                          * the shape of a table lookup *)
                         match jump_tab_idx e Arch.(T arch) with
                         | Some idx, _ -> (e, Some (idx, l))
                         | None, _ ->
                             build_exp_target ir l e Arch.(T arch) var_env
                       in
                       Some (e, idx)
                   | _ -> None )
                   ~f:(fun (e, idx) ->
                     match idx with
                     | Some (({name; size= Some size} as vi), l') ->
                         scan_jumptab ir e l l' vi size edges loads
                           eval_state
                           Arch.(T arch)
                           var_env dummy_start ~check_bounds
                     | _ -> (
                         let res = Ir_eval.eval_expr eval_state e in
                         match res.e with
                         | Il.Const {value}
                           when Set.for_all res.loads ~f:(fun a ->
                                    match Memory.find_segment img.mem a with
                                    | None -> false
                                    | Some seg ->
                                        Memory.Segment.is_writable seg |> not )
                           ->
                             Hashtbl.add_multi edges l value;
                             Set.iter res.loads ~f:(Hash_set.add loads)
                         | _ -> () ) ) ) );
  { targets=
      Hashtbl.fold edges ~init:[] ~f:(fun ~key ~data acc ->
          let addr = Ir.addr_of_block_exn ir key in
          let Arch.(I (arch, _)) = Code.instr_of_addr_exn code addr in
          (* we still need the corrected address here, but only switch if the
           * corresponding `ctrl` indicated that a switch is possible *)
          let switch =
            match Ir.block_exn ir key |> B.ctrl_of with
            | C.Call (_, _, Some _) | C.Jmp (_, Some _) -> true
            | _ -> false
          in
          List.fold data ~init:acc ~f:(fun acc addr ->
              let addr, Arch.(T arch') =
                Arch.addr_of_arch Arch.(T arch) addr
              in
              let Arch.(T arch) =
                if switch then Arch.(T arch') else Arch.(T arch)
              in
              let arch_ctx = Arch.(C (arch, create_decode_ctx arch)) in
              (key, addr, arch_ctx) :: acc ) )
  ; loads= Hash_set.to_list loads }
