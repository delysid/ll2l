open Core_kernel

type version = {major: int; minor: int}

module File_header = struct
  type characteristics =
    | RELOCS_STRIPPED
    | EXECUTABLE_IMAGE
    | LINE_NUMS_STRIPPED
    | LOCAL_SYMS_STRIPPED
    | AGGRESSIVE_WS_TRIM
    | LARGE_ADDRESS_AWARE
    | BYTES_REVERSED_LO
    | MACHINE_32BIT
    | DEBUG_STRIPPED
    | REMOVABLE_FROM_SWAP
    | NET_RUN_FROM_SWAP
    | SYSTEM
    | DLL
    | UP_SYSTEM_ONLY
    | BYTES_REVERSED_HI
  [@@deriving equal]

  type machine =
    | I386
    | R3000
    | R4000
    | R10000
    | WCEMIPSV2
    | ALPHA
    | SH3
    | SH3DSP
    | SH3E
    | SH4
    | SH5
    | ARM
    | THUMB
    | AM33
    | PowerPC
    | PowerPCFP
    | IA64
    | MIPS16
    | ALPHA64
    | MIPSFPU
    | MIPSFPU16
    | TRICORE
    | CEF
    | EBC
    | AMD64
    | M32R
    | CEE

  type t =
    { machine: machine option
    ; number_of_sections: int
    ; time_date_stamp: int32
    ; pointer_to_symbol_table: int32
    ; number_of_symbols: int32
    ; size_of_optional_header: int
    ; characteristics: characteristics list }

  let has_characteristic file_hdr c =
    List.mem file_hdr.characteristics c equal_characteristics
    [@@inline]
end

module Symbol = struct
  type base_type =
    | BT_NULL
    | BT_VOID
    | BT_CHAR
    | BT_SHORT
    | BT_INT
    | BT_LONG
    | BT_FLOAT
    | BT_DOUBLE
    | BT_STRUCT
    | BT_UNION
    | BT_ENUM
    | BT_MOE
    | BT_BYTE
    | BT_WORD
    | BT_UINT
    | BT_DWORD

  type derived_type = DT_NULL | DT_POINTER | DT_FUNCTION | DT_ARRAY

  type storage_class =
    | C_END_OF_FUNCTION
    | C_NULL
    | C_AUTOMATIC
    | C_EXTERNAL
    | C_STATIC
    | C_REGISTER
    | C_EXTERNAL_DEF
    | C_LABEL
    | C_UNDEFINED_LABEL
    | C_MEMBER_OF_STRUCT
    | C_ARGUMENT
    | C_STRUCT_TAG
    | C_MEMBER_OF_UNION
    | C_UNION_TAG
    | C_TYPE_DEFINITION
    | C_UNDEFINED_STATIC
    | C_ENUM_TAG
    | C_MEMBER_OF_ENUM
    | C_REGISTER_PARAM
    | C_BIT_FIELD
    | C_FAR_EXTERNAL
    | C_BLOCK
    | C_FUNCTION
    | C_END_OF_STRUCT
    | C_FILE
    | C_SECTION
    | C_WEAK_EXTERNAL
    | C_CLR_TOKEN

  module Function_definition = struct
    type t =
      { tag_index: int32
      ; total_size: int32
      ; pointer_to_line_number: int32
      ; pointer_to_next_function: int32 }
  end

  module Begin_and_end_function = struct
    type t = {line_number: int; pointer_to_next_function: int32}
  end

  module Weak_external = struct
    type characteristics = NOLIBRARY | LIBRARY [@@deriving equal]

    type t = {tag_index: int32; characteristics: characteristics list}

    let has_characteristic we c =
      List.mem we.characteristics c equal_characteristics
      [@@inline]

    let is_alias we =
      has_characteristic we NOLIBRARY && has_characteristic we LIBRARY
      [@@inline]
  end

  module Section_definition = struct
    type selection =
      | NODUPLICATES
      | ANY
      | SAME_SIZE
      | EXACT_MATCH
      | ASSOCIATIVE of int
      | LARGEST
      | NEWEST

    type t =
      { length: int32
      ; number_of_relocations: int
      ; number_of_line_numbers: int
      ; checksum: int32
      ; selection: selection option }
  end

  type aux_symbols =
    | FunctionDefinitions of Function_definition.t array
    | BeginAndEndFunctions of Begin_and_end_function.t array
    | WeakExternals of Weak_external.t array
    | Files of string array
    | SectionDefinitions of Section_definition.t array

  type t =
    { name: string
    ; value: int32
    ; section_number: int
    ; base_type: base_type
    ; derived_type: derived_type option
    ; storage_class: storage_class option
    ; number_of_aux_symbols: int
    ; aux_symbols: aux_symbols option }

  let is_function_definition sym =
    match sym.storage_class with
    | Some C_EXTERNAL when sym.section_number > 0 -> (
      match sym.derived_type with
      | Some DT_FUNCTION -> true
      | _ -> false )
    | _ -> false

  let is_begin_and_end_function sym =
    match sym.storage_class with
    | Some C_FUNCTION -> true
    | _ -> false
    [@@inline]

  let is_weak_external sym =
    match sym.storage_class with
    | Some C_EXTERNAL -> sym.section_number = 0 && Int32.(sym.value = 0l)
    | _ -> false
    [@@inline]

  let is_file sym =
    match sym.storage_class with
    | Some C_FILE -> true
    | _ -> false
    [@@inline]

  let is_section_definition sym =
    match sym.storage_class with
    | Some C_STATIC -> sym.number_of_aux_symbols > 0
    | _ -> false
    [@@inline]
end

module Section = struct
  type characteristics =
    | CNT_CODE
    | CNT_INITIALIZED_DATA
    | CNT_UNINITIALIZED_DATA
    | LNK_OTHER
    | LNK_INFO
    | LNK_REMOVE
    | LNK_COMDAT
    | NO_DEFER_SPEC_EXC
    | MEM_FARDATA
    | MEM_PURGEABLE
    | MEM_LOCKED
    | MEM_PRELOAD
    | ALIGN_1BYTES
    | ALIGN_2BYTES
    | ALIGN_8BYTES
    | ALIGN_128BYTES
    | LNK_NRELOC_OVFL
    | MEM_DISCARDABLE
    | MEM_NOT_CACHED
    | MEM_NOT_PAGED
    | MEM_SHARED
    | MEM_EXECUTE
    | MEM_READ
    | MEM_WRITE
  [@@deriving equal]

  type t =
    { name: string
    ; virtual_size: int32
    ; virtual_address: int32
    ; size_of_raw_data: int32
    ; pointer_to_raw_data: int32
    ; pointer_to_relocations: int32
    ; pointer_to_line_numbers: int32
    ; number_of_relocations: int
    ; number_of_line_numbers: int
    ; characteristics: characteristics list }

  let is_rva_within sec rva =
    Int32.(
      rva >= sec.virtual_address
      && rva < sec.virtual_address + sec.virtual_size)
    [@@inline]

  let is_file_offset_within sec offset =
    let endaddr = Int32.(sec.pointer_to_raw_data + sec.size_of_raw_data) in
    Option.(
      value ~default:false
        ( Int32.of_int64 offset
        >>| fun off ->
        Int32.(off >= sec.pointer_to_raw_data && off < endaddr) ))
    [@@inline]

  let has_characteristic sec c =
    List.mem sec.characteristics c equal_characteristics
    [@@inline]

  let is_align_4bytes sec =
    has_characteristic sec ALIGN_1BYTES
    && has_characteristic sec ALIGN_2BYTES
    [@@inline]

  let is_align_16bytes sec =
    has_characteristic sec ALIGN_1BYTES
    && has_characteristic sec ALIGN_8BYTES
    [@@inline]

  let is_align_32bytes sec =
    has_characteristic sec ALIGN_2BYTES
    && has_characteristic sec ALIGN_8BYTES
    [@@inline]

  let is_align_64bytes sec =
    is_align_4bytes sec && has_characteristic sec ALIGN_8BYTES
    [@@inline]

  let is_align_256bytes sec =
    has_characteristic sec ALIGN_1BYTES
    && has_characteristic sec ALIGN_128BYTES
    [@@inline]

  let is_align_512bytes sec =
    has_characteristic sec ALIGN_2BYTES
    && has_characteristic sec ALIGN_128BYTES
    [@@inline]

  let is_align_1024bytes sec =
    is_align_4bytes sec && has_characteristic sec ALIGN_128BYTES
    [@@inline]

  let is_align_2048bytes sec =
    has_characteristic sec ALIGN_8BYTES
    && has_characteristic sec ALIGN_128BYTES
    [@@inline]

  let is_align_4096bytes sec =
    is_align_16bytes sec && has_characteristic sec ALIGN_128BYTES
    [@@inline]

  let is_align_8192bytes sec =
    is_align_32bytes sec && has_characteristic sec ALIGN_128BYTES
    [@@inline]
end

module Export = struct
  type t = {virtual_address: int32; forwarded: bool; name: string}
end

module Export_directory = struct
  type t =
    { time_date_stamp: int32
    ; version: version
    ; name: string
    ; base: int32
    ; exports: Export.t array }
end

module Import = struct
  type id = Name of string | Ordinal of int

  type t = {virtual_address: int32; size: int; id: id}
end

module Import_directory = struct
  type descriptor =
    { time_date_stamp: int32
    ; forwarder_chain: int32
    ; name: string
    ; imports: Import.t array }

  type t = {descriptors: descriptor array}
end

module Base_reloc = struct
  type kind = HIGH | LOW | HIGHLOW | DIR64

  let get_size = function
    | HIGH | LOW -> 2
    | HIGHLOW -> 4
    | DIR64 -> 8

  type t = {kind: kind; virtual_address: int32; value: int64; offset: int64}
end

module Base_reloc_directory = struct
  type block =
    {virtual_address: int32; size: int32; relocs: Base_reloc.t array}

  type t = {blocks: block array}
end

module Unwind_code = struct
  type t = {frame_offset: int; code: int; info: int}
end

module Scope_record = struct
  type t =
    { begin_address: int32
    ; end_address: int32
    ; handler_address: int32
    ; jump_target: int32 }
end

module Exception_handler = struct
  type t = {exception_handler: int32; scope_table: Scope_record.t Array.t}
end

module Chain_info = struct
  type t =
    {(* this is an RVA to another RUNTIME_FUNCTION *)
     function_entry: int32}
end

module Unwind_info = struct
  type extra = Handler of Exception_handler.t | Chain of Chain_info.t

  type t =
    { offset: int32
    ; version: int
    ; flags: int
    ; size_of_prolog: int
    ; frame_register: int
    ; frame_offset: int
    ; unwind_codes: Unwind_code.t Array.t
    ; extra: extra option }
end

module Runtime_function = struct
  type t =
    {begin_address: int32; end_address: int32; unwind_info: Unwind_info.t}
end

module Exception_directory = struct
  type t = {functions: Runtime_function.t array}
end

module Tls_directory = struct
  type t =
    { start_address_of_raw_data: int64
    ; end_address_of_raw_data: int64
    ; address_of_index: int64
    ; address_of_callbacks: int64
    ; size_of_zero_fill: int32
    ; characteristics: int32
    ; callbacks: int64 array }
end

module Data_directory = struct
  type default = {virtual_address: int32; size: int32}

  let is_rva_within entry rva =
    Int32.(
      rva >= entry.virtual_address
      && rva < entry.virtual_address + entry.size)
    [@@inline]

  type t =
    { export: (default * Export_directory.t) option
    ; import: (default * Import_directory.t) option
    ; resource: default option
    ; exceptions: (default * Exception_directory.t) option
    ; security: default option
    ; base_reloc: (default * Base_reloc_directory.t) option
    ; debug: default option
    ; architecture: default option
    ; global_ptr: default option
    ; tls: (default * Tls_directory.t) option
    ; load_config: default option
    ; bound_import: default option
    ; iat: default option
    ; delay_import: default option
    ; com_descriptor: default option }
end

module Dll_characteristics = struct
  type t =
    | DYNAMIC_BASE
    | FORCE_INTEGRITY
    | NX_COMPAT
    | NO_ISOLATION
    | NO_SEH
    | NO_BIND
    | WDM_DRIVER
    | TERMINAL_SERVER_AWARE
  [@@deriving equal]
end

type t =
  { kind: [`PE32 | `PE64]
  ; lfanew: int32
  ; file_header: File_header.t
  ; linker_version: version
  ; size_of_code: int32
  ; size_of_initialized_data: int32
  ; size_of_uninitialized_data: int32
  ; address_of_entry_point: int32
  ; base_of_code: int32
  ; base_of_data: int32 option
  ; image_base: int64
  ; section_alignment: int32
  ; file_alignment: int32
  ; operating_system_version: version
  ; image_version: version
  ; subsystem_version: version
  ; win32_version_value: int32
  ; size_of_image: int32
  ; size_of_headers: int32
  ; checksum: int32
  ; subsystem: int
  ; dll_characteristics: Dll_characteristics.t list
  ; size_of_stack_reserve: int64
  ; size_of_stack_commit: int64
  ; size_of_heap_reserve: int64
  ; size_of_heap_commit: int64
  ; loader_flags: int32
  ; number_of_rva_and_sizes: int32
  ; data_directory: Data_directory.t
  ; sections: Section.t array
  ; symbols: Symbol.t array }

let total_header_size pe =
  let lfanew = Int.of_int32_exn pe.lfanew in
  let signature_size = 4 in
  let file_header_size = 24 in
  let optional_header_size = pe.file_header.size_of_optional_header in
  let section_header_size = 40 in
  let number_of_sections = pe.file_header.number_of_sections in
  let section_table_size = number_of_sections * section_header_size in
  lfanew + signature_size + file_header_size + optional_header_size
  + section_table_size

let has_dll_characteristic pe c =
  List.mem pe.dll_characteristics c Dll_characteristics.equal
  [@@inline]

let va_of_rva pe rva =
  Int64.(pe.image_base + (of_int32 rva land 0xFFFFFFFFL))
  [@@inline]

let section_of_rva pe rva =
  Array.find pe.sections (fun sec -> Section.is_rva_within sec rva)
  [@@inline]

let section_of_va pe va =
  let open Option.Let_syntax in
  let%bind rva = Int32.of_int64 Int64.(va - pe.image_base) in
  section_of_rva pe rva
  [@@inline]

let section_of_file_offset pe offset =
  Array.find pe.sections (fun sec ->
      Section.is_file_offset_within sec offset)
  [@@inline]

let section_of_rva pe rva = section_of_va pe (va_of_rva pe rva) [@@inline]

let file_offset_of_section_and_va pe (sec : Section.t) va =
  let base = va_of_rva pe sec.virtual_address in
  Int64.(va - base + of_int32 sec.pointer_to_raw_data)
  [@@inline]

let file_offset_of_section_and_rva pe (sec : Section.t) rva =
  file_offset_of_section_and_va pe sec (va_of_rva pe rva)
  [@@inline]

let file_offset_of_rva pe rva =
  let open Option.Let_syntax in
  let%map sec = section_of_rva pe rva in
  file_offset_of_section_and_rva pe sec rva
  [@@inline]

let file_offset_of_va pe va =
  let open Option.Let_syntax in
  let%bind rva = Int32.of_int64 Int64.(va - pe.image_base) in
  file_offset_of_rva pe rva
  [@@inline]

let rva_of_file_offset pe offset =
  let open Option.Let_syntax in
  let%bind sec = section_of_file_offset pe offset in
  let%map off = Int32.of_int64 offset in
  Int32.(off - sec.pointer_to_raw_data + sec.virtual_address)
  [@@inline]

let va_of_file_offset pe offset =
  let open Option.Let_syntax in
  let%map rva = rva_of_file_offset pe offset in
  Int64.(pe.image_base + (of_int32 rva land 0xFFFFFFFFL))
  [@@inline]
