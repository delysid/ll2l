open Core_kernel

val is_pe_file : string -> bool

val from_bigstring_exn : Bigstring.t -> Pe.t

val from_bigstring : Bigstring.t -> Pe.t option
