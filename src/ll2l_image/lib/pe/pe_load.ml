open Core_kernel
open Ll2l_arch
open Ll2l_std
open Pe
module M = Memory

let arch_of pe =
  match pe.file_header.machine with
  | Some File_header.I386 -> Arch.of_string "ia32"
  | Some File_header.AMD64 -> Arch.of_string "amd64"
  | None -> invalid_arg "no architecture specified"
  | _ -> invalid_arg "unsupported architecture"

let load_sections pe data mem =
  let sections =
    Array.filter pe.sections ~f:(fun sec ->
        List.mem sec.characteristics Section.MEM_DISCARDABLE
          ~equal:Section.equal_characteristics
        |> not)
  in
  Array.iter sections ~f:(fun sec ->
      let data =
        let pos, len =
          (* MSDN (on the PointerToRawData field):
           * "The file pointer to the first page of the
           *  section within the COFF file.
           *  For executable images, this must be a
           *  multiple of FileAlignment from the optional header." *)
          let size = Int32.min sec.size_of_raw_data sec.virtual_size in
          let offset =
            let a = pe.file_alignment in
            Int32.(sec.pointer_to_raw_data / a * a)
          in
          ( Int.(of_int32_exn offset land 0xFFFFFFFF)
          , Int.(of_int32_exn size land 0xFFFFFFFF) )
        in
        Bigstring.sub data ~pos ~len
      in
      let addr = Addr.int64 @@ va_of_rva pe sec.virtual_address in
      let r, w, x =
        let init = (false, false, false) in
        List.fold sec.characteristics ~init ~f:(fun (r, w, x) c ->
            match c with
            | Section.MEM_READ -> (true, w, x)
            | Section.MEM_WRITE -> (r, true, x)
            | Section.MEM_EXECUTE -> (r, w, true)
            | _ -> (r, w, x))
      in
      match M.add_segment_from_bigstring mem addr data ~r ~w ~x with
      | Error `BadAddr ->
          invalid_arg
            (Printf.sprintf "bad section address 0x%016LX for section %s"
               (Addr.to_int64 addr) sec.name)
      | _ -> ())

let collect_base_relocs pe =
  let init = Addr.Map.empty in
  match pe.data_directory.base_reloc with
  | None -> init
  | Some (_, dir) ->
      Array.fold dir.blocks ~init ~f:(fun m blk ->
          Array.fold blk.relocs ~init:m ~f:(fun m rel ->
              let reloc =
                Image.Reloc.
                  { addr= Addr.int64 @@ va_of_rva pe rel.virtual_address
                  ; size= Base_reloc.get_size rel.kind
                  ; value= Some rel.value
                  ; name= None
                  ; offset= Some rel.offset }
              in
              Addr.Map.set m ~key:reloc.addr ~data:reloc))

let collect_iat_relocs pe =
  let init = Addr.Map.empty in
  match pe.data_directory.import with
  | None -> init
  | Some (_, dir) ->
      Array.fold dir.descriptors ~init ~f:(fun m desc ->
          Array.fold desc.imports ~init:m ~f:(fun m rel ->
              let name =
                match rel.id with
                | Name name -> Some name
                | Ordinal _ -> None
              in
              let addr = Addr.int64 @@ va_of_rva pe rel.virtual_address in
              let reloc =
                Image.Reloc.
                  {addr; size= rel.size; value= None; name; offset= None}
              in
              Addr.Map.set m ~key:addr ~data:reloc))

let from_bigstring_exn data =
  let pe = Pe_parse.from_bigstring_exn data in
  let arch = arch_of pe in
  let mem = M.create () in
  load_sections pe data mem;
  let relocs =
    Map.merge_skewed (collect_base_relocs pe) (collect_iat_relocs pe)
      ~combine:(fun ~key v _ -> v)
  in
  let img =
    Image.
      { meta= PE pe
      ; endian= `LE
      ; mem
      ; start= None
      ; relocs
      ; arch
      ; canonical_values= String.Map.empty }
  in
  let validate_addr a = Option.some_if Image.(is_valid_pc img a) a in
  let start =
    va_of_rva pe pe.address_of_entry_point |> Addr.int64 |> validate_addr
  in
  {img with start}

let from_bigstring data =
  Option.try_with (fun () -> from_bigstring_exn data)
  [@@inline]
