open Core_kernel
open Bitstring
open Stdio
open Ll2l_std
open Pe

let parse_file_characteristics = function
  | 0 -> Some File_header.RELOCS_STRIPPED
  | 1 -> Some File_header.EXECUTABLE_IMAGE
  | 2 -> Some File_header.LINE_NUMS_STRIPPED
  | 3 -> Some File_header.LOCAL_SYMS_STRIPPED
  | 4 -> Some File_header.AGGRESSIVE_WS_TRIM
  | 5 -> Some File_header.LARGE_ADDRESS_AWARE
  | 7 -> Some File_header.BYTES_REVERSED_LO
  | 8 -> Some File_header.MACHINE_32BIT
  | 9 -> Some File_header.DEBUG_STRIPPED
  | 10 -> Some File_header.REMOVABLE_FROM_SWAP
  | 11 -> Some File_header.NET_RUN_FROM_SWAP
  | 12 -> Some File_header.SYSTEM
  | 13 -> Some File_header.DLL
  | 14 -> Some File_header.UP_SYSTEM_ONLY
  | 15 -> Some File_header.BYTES_REVERSED_HI
  | _ -> None

let parse_file_machine = function
  | 0x014C -> Some File_header.I386
  | 0x0162 -> Some File_header.R3000
  | 0x0166 -> Some File_header.R4000
  | 0x0168 -> Some File_header.R10000
  | 0x0169 -> Some File_header.WCEMIPSV2
  | 0x0184 -> Some File_header.ALPHA
  | 0x01A2 -> Some File_header.SH3
  | 0x01A3 -> Some File_header.SH3DSP
  | 0x01A4 -> Some File_header.SH3E
  | 0x01A6 -> Some File_header.SH4
  | 0x01A8 -> Some File_header.SH5
  | 0x01C0 -> Some File_header.ARM
  | 0x01C2 -> Some File_header.THUMB
  | 0x01D3 -> Some File_header.AM33
  | 0x01F0 -> Some File_header.PowerPC
  | 0x01F1 -> Some File_header.PowerPCFP
  | 0x0200 -> Some File_header.IA64
  | 0x0266 -> Some File_header.MIPS16
  | 0x0284 -> Some File_header.ALPHA64
  | 0x0366 -> Some File_header.MIPSFPU
  | 0x0466 -> Some File_header.MIPSFPU16
  | 0x0520 -> Some File_header.TRICORE
  | 0x0CEF -> Some File_header.CEF
  | 0x0EBC -> Some File_header.EBC
  | 0x8664 -> Some File_header.AMD64
  | 0x9041 -> Some File_header.M32R
  | 0xC0EE -> Some File_header.CEE
  | _ -> None

let parse_symbol_base_type = function
  | 0 -> Symbol.BT_NULL
  | 1 -> Symbol.BT_VOID
  | 2 -> Symbol.BT_CHAR
  | 3 -> Symbol.BT_SHORT
  | 4 -> Symbol.BT_INT
  | 5 -> Symbol.BT_LONG
  | 6 -> Symbol.BT_FLOAT
  | 7 -> Symbol.BT_DOUBLE
  | 8 -> Symbol.BT_STRUCT
  | 9 -> Symbol.BT_UNION
  | 10 -> Symbol.BT_ENUM
  | 11 -> Symbol.BT_MOE
  | 12 -> Symbol.BT_BYTE
  | 13 -> Symbol.BT_WORD
  | 14 -> Symbol.BT_UINT
  | 15 -> Symbol.BT_DWORD
  | _ -> invalid_arg "bad symbol base type"

let parse_symbol_derived_type = function
  | 0 -> Some Symbol.DT_NULL
  | 1 -> Some Symbol.DT_POINTER
  | 2 -> Some Symbol.DT_FUNCTION
  | 3 -> Some Symbol.DT_ARRAY
  | _ -> None

let parse_symbol_storage_class = function
  | 0xff -> Some Symbol.C_END_OF_FUNCTION
  | 0 -> Some Symbol.C_NULL
  | 1 -> Some Symbol.C_AUTOMATIC
  | 2 -> Some Symbol.C_EXTERNAL
  | 3 -> Some Symbol.C_STATIC
  | 4 -> Some Symbol.C_REGISTER
  | 5 -> Some Symbol.C_EXTERNAL_DEF
  | 6 -> Some Symbol.C_LABEL
  | 7 -> Some Symbol.C_UNDEFINED_LABEL
  | 8 -> Some Symbol.C_MEMBER_OF_STRUCT
  | 9 -> Some Symbol.C_ARGUMENT
  | 10 -> Some Symbol.C_STRUCT_TAG
  | 11 -> Some Symbol.C_MEMBER_OF_UNION
  | 12 -> Some Symbol.C_UNION_TAG
  | 13 -> Some Symbol.C_TYPE_DEFINITION
  | 14 -> Some Symbol.C_UNDEFINED_STATIC
  | 15 -> Some Symbol.C_ENUM_TAG
  | 16 -> Some Symbol.C_MEMBER_OF_ENUM
  | 17 -> Some Symbol.C_REGISTER_PARAM
  | 18 -> Some Symbol.C_BIT_FIELD
  | 100 -> Some Symbol.C_BLOCK
  | 101 -> Some Symbol.C_FUNCTION
  | 102 -> Some Symbol.C_END_OF_STRUCT
  | 103 -> Some Symbol.C_FILE
  | 104 -> Some Symbol.C_SECTION
  | 105 -> Some Symbol.C_WEAK_EXTERNAL
  | 107 -> Some Symbol.C_CLR_TOKEN
  | _ -> None

let parse_weak_external_characteristics = function
  | 0 -> Some Symbol.Weak_external.NOLIBRARY
  | 1 -> Some Symbol.Weak_external.LIBRARY
  | _ -> None

let parse_section_definition_selection number = function
  | 1 -> Some Symbol.Section_definition.NODUPLICATES
  | 2 -> Some Symbol.Section_definition.ANY
  | 3 -> Some Symbol.Section_definition.SAME_SIZE
  | 4 -> Some Symbol.Section_definition.EXACT_MATCH
  | 5 -> Some (Symbol.Section_definition.ASSOCIATIVE number)
  | 6 -> Some Symbol.Section_definition.LARGEST
  | 7 -> Some Symbol.Section_definition.NEWEST
  | _ -> None

let parse_section_characteristics = function
  | 5 -> Some Section.CNT_CODE
  | 6 -> Some Section.CNT_INITIALIZED_DATA
  | 7 -> Some Section.CNT_UNINITIALIZED_DATA
  | 8 -> Some Section.LNK_OTHER
  | 9 -> Some Section.LNK_INFO
  | 11 -> Some Section.LNK_REMOVE
  | 12 -> Some Section.LNK_COMDAT
  | 14 -> Some Section.NO_DEFER_SPEC_EXC
  | 15 -> Some Section.MEM_FARDATA
  | 17 -> Some Section.MEM_PURGEABLE
  | 18 -> Some Section.MEM_LOCKED
  | 19 -> Some Section.MEM_PRELOAD
  | 20 -> Some Section.ALIGN_1BYTES
  | 21 -> Some Section.ALIGN_2BYTES
  | 22 -> Some Section.ALIGN_8BYTES
  | 23 -> Some Section.ALIGN_128BYTES
  | 24 -> Some Section.LNK_NRELOC_OVFL
  | 25 -> Some Section.MEM_DISCARDABLE
  | 26 -> Some Section.MEM_NOT_CACHED
  | 27 -> Some Section.MEM_NOT_PAGED
  | 28 -> Some Section.MEM_SHARED
  | 29 -> Some Section.MEM_EXECUTE
  | 30 -> Some Section.MEM_READ
  | 31 -> Some Section.MEM_WRITE
  | _ -> None

let parse_dll_characteristics = function
  | 6 -> Some Dll_characteristics.DYNAMIC_BASE
  | 7 -> Some Dll_characteristics.FORCE_INTEGRITY
  | 8 -> Some Dll_characteristics.NX_COMPAT
  | 9 -> Some Dll_characteristics.NO_ISOLATION
  | 10 -> Some Dll_characteristics.NO_SEH
  | 11 -> Some Dll_characteristics.NO_BIND
  | 13 -> Some Dll_characteristics.WDM_DRIVER
  | 15 -> Some Dll_characteristics.TERMINAL_SERVER_AWARE
  | _ -> None

let parse_flags f n v =
  let rec aux res b v =
    if b = n then List.rev res
    else
      let res =
        if Int64.(bit_and v 1L = 1L) then
          match f b with
          | Some flg -> flg :: res
          | None -> res
        else res
      in
      aux res (b + 1) Int64.(v lsr 1)
  in
  aux [] 0 v

let dos_header_size = 64

let file_header_size = 24

let max_nt_header_size = 260

let section_header_size = 40

let symbol_size = 18

let parse_dos_header bits =
  match%bitstring bits with
  | {|
     0x5a4d: 16: littleendian;
     pad1: 64;
     pad2: 64;
     pad3: 64;
     pad4: 64;
     pad5: 64;
     pad6: 64;
     pad7: 64;
     pad8: 16;
     e_lfanew: 32: littleendian
     |}
    -> e_lfanew
  | {| _ |} -> invalid_arg "bad dos header"

let parse_file_header bits =
  match%bitstring bits with
  | {|
     machine: 16: littleendian;
     number_of_sections: 16: littleendian;
     time_date_stamp: 32: littleendian;
     pointer_to_symbol_table: 32: littleendian;
     number_of_symbols: 32: littleendian;
     size_of_optional_header: 16: littleendian;
     characteristics: 16: littleendian;
     rest: -1: bitstring
     |}
    ->
      let file_header =
        File_header.
          { machine= parse_file_machine machine
          ; number_of_sections
          ; time_date_stamp
          ; pointer_to_symbol_table
          ; number_of_symbols
          ; size_of_optional_header
          ; characteristics=
              parse_flags parse_file_characteristics 16
                (Int64.of_int characteristics) }
      in
      (file_header, rest)
  | {| _ |} -> invalid_arg "bad file header"

let parse_data_directory bits =
  match%bitstring bits with
  | {|
     export_virtual_address: 32: littleendian;
     export_size: 32: littleendian;
     import_virtual_address: 32: littleendian;
     import_size: 32: littleendian;
     resource_virtual_address: 32: littleendian;
     resource_size: 32: littleendian;
     exception_virtual_address: 32: littleendian;
     exception_size: 32: littleendian;
     security_virtual_address: 32: littleendian;
     security_size: 32: littleendian;
     base_reloc_virtual_address: 32: littleendian;
     base_reloc_size: 32: littleendian;
     debug_virtual_address: 32: littleendian;
     debug_size: 32: littleendian;
     architecture_virtual_address: 32: littleendian;
     architecture_size: 32: littleendian;
     global_ptr_virtual_address: 32: littleendian;
     global_ptr_size: 32: littleendian;
     tls_virtual_address: 32: littleendian;
     tls_size: 32: littleendian;
     load_config_virtual_address: 32: littleendian;
     load_config_size: 32: littleendian;
     bound_import_virtual_address: 32: littleendian;
     bound_import_size: 32: littleendian;
     iat_virtual_address: 32: littleendian;
     iat_size: 32: littleendian;
     delay_import_virtual_address: 32: littleendian;
     delay_import_size: 32: littleendian;
     com_descriptor_virtual_address: 32: littleendian;
     com_descriptor_size: 32: littleendian;
     _: -1: bitstring
     |}
    ->
      Data_directory.
        ( {virtual_address= export_virtual_address; size= export_size}
        , {virtual_address= import_virtual_address; size= import_size}
        , {virtual_address= resource_virtual_address; size= resource_size}
        , {virtual_address= exception_virtual_address; size= exception_size}
        , {virtual_address= security_virtual_address; size= security_size}
        , {virtual_address= base_reloc_virtual_address; size= base_reloc_size}
        , {virtual_address= debug_virtual_address; size= debug_size}
        , { virtual_address= architecture_virtual_address
          ; size= architecture_size }
        , {virtual_address= global_ptr_virtual_address; size= global_ptr_size}
        , {virtual_address= tls_virtual_address; size= tls_size}
        , { virtual_address= load_config_virtual_address
          ; size= load_config_size }
        , { virtual_address= bound_import_virtual_address
          ; size= bound_import_size }
        , {virtual_address= iat_virtual_address; size= iat_size}
        , { virtual_address= delay_import_virtual_address
          ; size= delay_import_size }
        , { virtual_address= com_descriptor_virtual_address
          ; size= com_descriptor_size } )
  | {| _ |} -> invalid_arg "bad data directory"

let parse_nt_header lfanew bits =
  match%bitstring bits with
  | {|
     0x00004550l: 32: littleendian;
     rest: -1: bitstring
     |}
    -> (
      let file_header, rest = parse_file_header rest in
      match%bitstring rest with
      | {|
        0x010b: 16: littleendian;
        major_linker_version: 8;
        minor_linker_version: 8;
        size_of_code: 32: littleendian;
        size_of_initialized_data: 32: littleendian;
        size_of_uninitialized_data: 32: littleendian;
        address_of_entry_point: 32: littleendian;
        base_of_code: 32: littleendian;
        base_of_data: 32: littleendian;
        image_base: 32: littleendian;
        section_alignment: 32: littleendian;
        file_alignment: 32: littleendian;
        major_operating_system_version: 16: littleendian;
        minor_operating_system_version: 16: littleendian;
        major_image_version: 16: littleendian;
        minor_image_version: 16: littleendian;
        major_subsystem_version: 16: littleendian;
        minor_subsystem_version: 16: littleendian;
        win32_version_value: 32: littleendian;
        size_of_image: 32: littleendian;
        size_of_headers: 32: littleendian;
        checksum: 32: littleendian;
        subsystem: 16: littleendian;
        dll_characteristics: 16: littleendian;
        size_of_stack_reserve: 32: littleendian;
        size_of_stack_commit: 32: littleendian;
        size_of_heap_reserve: 32: littleendian;
        size_of_heap_commit: 32: littleendian;
        loader_flags: 32: littleendian;
        number_of_rva_and_sizes: 32: littleendian;
        rest2: -1: bitstring
        |}
        ->
          let hdr =
            { kind= `PE32
            ; lfanew
            ; file_header
            ; linker_version=
                {major= major_linker_version; minor= minor_linker_version}
            ; size_of_code
            ; size_of_initialized_data
            ; size_of_uninitialized_data
            ; address_of_entry_point
            ; base_of_code
            ; base_of_data= Some base_of_data
            ; image_base= Int64.of_int32 image_base
            ; section_alignment
            ; file_alignment
            ; operating_system_version=
                { major= major_operating_system_version
                ; minor= minor_operating_system_version }
            ; image_version=
                {major= major_image_version; minor= minor_image_version}
            ; subsystem_version=
                { major= major_subsystem_version
                ; minor= minor_subsystem_version }
            ; win32_version_value
            ; size_of_image
            ; size_of_headers
            ; checksum
            ; subsystem
            ; dll_characteristics=
                parse_flags parse_dll_characteristics 16
                  (Int64.of_int dll_characteristics)
            ; size_of_stack_reserve= Int64.of_int32 size_of_stack_reserve
            ; size_of_stack_commit= Int64.of_int32 size_of_stack_commit
            ; size_of_heap_reserve= Int64.of_int32 size_of_heap_reserve
            ; size_of_heap_commit= Int64.of_int32 size_of_heap_commit
            ; loader_flags
            ; number_of_rva_and_sizes
            ; data_directory=
                { export= None
                ; import= None
                ; resource= None
                ; exceptions= None
                ; security= None
                ; base_reloc= None
                ; debug= None
                ; architecture= None
                ; global_ptr= None
                ; tls= None
                ; load_config= None
                ; bound_import= None
                ; iat= None
                ; delay_import= None
                ; com_descriptor= None }
            ; sections= [||]
            ; symbols= [||] }
          in
          let data_directory = parse_data_directory rest2 in
          (hdr, data_directory)
      | {|
        0x020b: 16: littleendian;
        major_linker_version: 8;
        minor_linker_version: 8;
        size_of_code: 32: littleendian;
        size_of_initialized_data: 32: littleendian;
        size_of_uninitialized_data: 32: littleendian;
        address_of_entry_point: 32: littleendian;
        base_of_code: 32: littleendian;
        image_base: 64: littleendian;
        section_alignment: 32: littleendian;
        file_alignment: 32: littleendian;
        major_operating_system_version: 16: littleendian;
        minor_operating_system_version: 16: littleendian;
        major_image_version: 16: littleendian;
        minor_image_version: 16: littleendian;
        major_subsystem_version: 16: littleendian;
        minor_subsystem_version: 16: littleendian;
        win32_version_value: 32: littleendian;
        size_of_image: 32: littleendian;
        size_of_headers: 32: littleendian;
        checksum: 32: littleendian;
        subsystem: 16: littleendian;
        dll_characteristics: 16: littleendian;
        size_of_stack_reserve: 64: littleendian;
        size_of_stack_commit: 64: littleendian;
        size_of_heap_reserve: 64: littleendian;
        size_of_heap_commit: 64: littleendian;
        loader_flags: 32: littleendian;
        number_of_rva_and_sizes: 32: littleendian;
        rest: -1: bitstring
        |}
        ->
          let hdr =
            { kind= `PE64
            ; lfanew
            ; file_header
            ; linker_version=
                {major= major_linker_version; minor= minor_linker_version}
            ; size_of_code
            ; size_of_initialized_data
            ; size_of_uninitialized_data
            ; address_of_entry_point
            ; base_of_code
            ; base_of_data= None
            ; image_base
            ; section_alignment
            ; file_alignment
            ; operating_system_version=
                { major= major_operating_system_version
                ; minor= minor_operating_system_version }
            ; image_version=
                {major= major_image_version; minor= minor_image_version}
            ; subsystem_version=
                { major= major_subsystem_version
                ; minor= minor_subsystem_version }
            ; win32_version_value
            ; size_of_image
            ; size_of_headers
            ; checksum
            ; subsystem
            ; dll_characteristics=
                parse_flags parse_dll_characteristics 16
                  (Int64.of_int dll_characteristics)
            ; size_of_stack_reserve
            ; size_of_stack_commit
            ; size_of_heap_reserve
            ; size_of_heap_commit
            ; loader_flags
            ; number_of_rva_and_sizes
            ; data_directory=
                { export= None
                ; import= None
                ; resource= None
                ; exceptions= None
                ; security= None
                ; base_reloc= None
                ; debug= None
                ; architecture= None
                ; global_ptr= None
                ; tls= None
                ; load_config= None
                ; bound_import= None
                ; iat= None
                ; delay_import= None
                ; com_descriptor= None }
            ; sections= [||]
            ; symbols= [||] }
          in
          let data_directory = parse_data_directory rest in
          (hdr, data_directory)
      | {| _ |} -> invalid_arg "bad nt header" )
  | {| _ |} -> invalid_arg "bad nt header signature"

let parse_section_header bits =
  match%bitstring bits with
  | {|
     name: 64: string;
     virtual_size: 32: littleendian;
     virtual_address: 32: littleendian;
     size_of_raw_data: 32: littleendian;
     pointer_to_raw_data: 32: littleendian;
     pointer_to_relocations: 32: littleendian;
     pointer_to_line_numbers: 32: littleendian;
     number_of_relocations: 16: littleendian;
     number_of_line_numbers: 16: littleendian;
     characteristics: 32: littleendian
     |}
    ->
      Section.
        { name= ""
        ; virtual_size
        ; virtual_address
        ; size_of_raw_data
        ; pointer_to_raw_data
        ; pointer_to_relocations
        ; pointer_to_line_numbers
        ; number_of_relocations
        ; number_of_line_numbers
        ; characteristics=
            parse_flags parse_section_characteristics 32
              (Int64.of_int32 characteristics) }
  | {| _ |} -> invalid_arg "bad section header"

type table = {offset: int64; entry_size: int; num_entries: int}

let get_chunks tbl data =
  let pos = Int.of_int64_exn tbl.offset in
  let rec aux res n =
    if n >= tbl.num_entries then List.rev res
    else
      let pos = pos + (n * tbl.entry_size) in
      let buf = Bigstring.to_bytes data ~pos ~len:tbl.entry_size in
      aux (buf :: res) (n + 1)
  in
  aux [] 0

let get_string pos data =
  let open Option.Let_syntax in
  let%map pos' = Bigstring.find ~pos '\x00' data in
  let len = pos' - pos in
  Bigstring.to_string ~pos ~len data

let bitstring_of_bytes b = (b, 0, Bytes.length b * 8)

let parse_aux_function_definition bits =
  match%bitstring bits with
  | {|
     tag_index: 32: littleendian;
     total_size: 32: littleendian;
     pointer_to_line_number: 32: littleendian;
     pointer_to_next_function: 32: littleendian;
     _: -1: bitstring
     |}
    ->
      Symbol.Function_definition.
        { tag_index
        ; total_size
        ; pointer_to_line_number
        ; pointer_to_next_function }
  | {| _ |} -> invalid_arg "bad aux function definition"

let parse_aux_begin_and_end_function bits =
  match%bitstring bits with
  | {|
     pad1: 32;
     line_number: 16: littleendian;
     pad2: 48;
     pointer_to_next_function: 32: littleendian;
     _: -1: bitstring
     |}
    -> Symbol.Begin_and_end_function.{line_number; pointer_to_next_function}
  | {| _ |} -> invalid_arg "bad aux begin and end function"

let parse_aux_weak_external bits =
  match%bitstring bits with
  | {|
     tag_index: 32: littleendian;
     characteristics: 32: littleendian;
     _: -1: bitstring
     |}
    ->
      Symbol.Weak_external.
        { tag_index
        ; characteristics=
            parse_flags parse_weak_external_characteristics 32
              (Int64.of_int32 characteristics) }
  | {| _ |} -> invalid_arg "bad aux weak external"

let parse_aux_section_definition bits =
  match%bitstring bits with
  | {|
     length: 32: littleendian;
     number_of_relocations: 16: littleendian;
     number_of_line_numbers: 16: littleendian;
     checksum: 32: littleendian;
     number: 16: littleendian;
     selection: 8;
     _: -1: bitstring
     |}
    ->
      Symbol.Section_definition.
        { length
        ; number_of_relocations
        ; number_of_line_numbers
        ; checksum
        ; selection= parse_section_definition_selection number selection }
  | {| _ |} -> invalid_arg "bad aux section definition"

let parse_aux_symbols offset i data (sym : Symbol.t) =
  if sym.number_of_aux_symbols = 0 then None
  else
    let tbl =
      { offset= Int64.(offset + of_int Int.((i + 1) * symbol_size))
      ; entry_size= symbol_size
      ; num_entries= sym.number_of_aux_symbols }
    in
    if Symbol.is_function_definition sym then
      let arr =
        List.map (get_chunks tbl data) ~f:(fun buf ->
            let bits = bitstring_of_bytes buf in
            parse_aux_function_definition bits)
        |> Array.of_list
      in
      Some (Symbol.FunctionDefinitions arr)
    else if Symbol.is_begin_and_end_function sym then
      let arr =
        List.map (get_chunks tbl data) ~f:(fun buf ->
            let bits = bitstring_of_bytes buf in
            parse_aux_begin_and_end_function bits)
        |> Array.of_list
      in
      Some (Symbol.BeginAndEndFunctions arr)
    else if Symbol.is_weak_external sym then
      let arr =
        List.map (get_chunks tbl data) ~f:(fun buf ->
            let bits = bitstring_of_bytes buf in
            parse_aux_weak_external bits)
        |> Array.of_list
      in
      Some (Symbol.WeakExternals arr)
    else if Symbol.is_file sym then
      let arr =
        List.map (get_chunks tbl data) ~f:(fun buf ->
            let file = Bytes.to_string buf in
            Option.(
              value ~default:file
                ( String.index file '\x00'
                >>| fun i -> String.subo file ~len:i )))
        |> Array.of_list
      in
      Some (Symbol.Files arr)
    else if Symbol.is_section_definition sym then
      let arr =
        List.map (get_chunks tbl data) ~f:(fun buf ->
            let bits = bitstring_of_bytes buf in
            parse_aux_section_definition bits)
        |> Array.of_list
      in
      Some (Symbol.SectionDefinitions arr)
    else None

let parse_symbol offset i data bits =
  match%bitstring bits with
  | {|
     short_name: 32: littleendian;
     long_name: 32: littleendian;
     value: 32: littleendian;
     section_number: 16: littleendian;
     typ: 16: littleendian;
     storage_class: 8;
     number_of_aux_symbols: 8
     |}
    ->
      let sym =
        Symbol.
          { name= ""
          ; value
          ; section_number
          ; base_type= parse_symbol_base_type (typ land 0xF)
          ; derived_type= parse_symbol_derived_type (typ lsr 4)
          ; storage_class= parse_symbol_storage_class storage_class
          ; number_of_aux_symbols
          ; aux_symbols= None }
      in
      let aux_symbols = parse_aux_symbols offset i data sym in
      ({sym with aux_symbols}, short_name, long_name)
  | {| _ |} -> invalid_arg "bad symbol"

let is_pe_file filename =
  try
    In_channel.with_file filename ~f:(fun file ->
        let buf = Bytes.create dos_header_size in
        In_channel.really_input_exn file ~buf ~pos:0 ~len:dos_header_size;
        let e_lfanew = parse_dos_header (bitstring_of_bytes buf) in
        let buf = Bytes.create max_nt_header_size in
        In_channel.seek file (Int64.of_int32 e_lfanew);
        In_channel.really_input_exn file ~buf ~pos:0 ~len:max_nt_header_size;
        ignore @@ parse_nt_header e_lfanew (bitstring_of_bytes buf);
        true)
  with _ -> false

let default_parse_directory (entry : Data_directory.default) =
  if Int32.(entry.virtual_address <> 0l && entry.size <> 0l) then Some entry
  else None

let export_entry_size = 40

let parse_export_entry data offset =
  let pos = Int.of_int64_exn offset in
  let buf = Bigstring.to_bytes data ~pos ~len:export_entry_size in
  let bits = bitstring_of_bytes buf in
  match%bitstring bits with
  | {|
     _: 32;
     time_date_stamp: 32: littleendian;
     major_version: 16: littleendian;
     minor_version: 16: littleendian;
     name: 32: littleendian;
     base: 32: littleendian;
     number_of_functions: 32: littleendian;
     number_of_names: 32: littleendian;
     address_of_functions: 32: littleendian;
     address_of_names: 32: littleendian;
     address_of_name_ordinals: 32: littleendian
     |}
    ->
      let export_dir =
        Export_directory.
          { time_date_stamp
          ; version= {major= major_version; minor= minor_version}
          ; name= ""
          ; base
          ; exports= [||] }
      in
      ( export_dir
      , name
      , number_of_functions
      , number_of_names
      , address_of_functions
      , address_of_names
      , address_of_name_ordinals )
  | {| _ |} -> invalid_arg "bad export entry"

let get_int16 data offset =
  let pos = Int.of_int64_exn offset in
  let buf = Bigstring.to_bytes data ~pos ~len:2 in
  let bits = bitstring_of_bytes buf in
  match%bitstring bits with
  | {| v: 16: littleendian |} -> v

let get_int32 data offset =
  let pos = Int.of_int64_exn offset in
  let buf = Bigstring.to_bytes data ~pos ~len:4 in
  let bits = bitstring_of_bytes buf in
  match%bitstring bits with
  | {| v: 32: littleendian |} -> v

let get_int64 data offset =
  let pos = Int.of_int64_exn offset in
  let buf = Bigstring.to_bytes data ~pos ~len:8 in
  let bits = bitstring_of_bytes buf in
  match%bitstring bits with
  | {| v: 64: littleendian |} -> v

let is_valid_entry (entry : Data_directory.default) =
  Int32.(entry.virtual_address <> 0l && entry.size <> 0l)
  [@@inline]

let parse_exports data pe (entry : Data_directory.default) =
  let open Option.Let_syntax in
  let%bind _ = Option.some_if (is_valid_entry entry) () in
  let get_offset = file_offset_of_rva pe in
  let%bind offset = get_offset entry.virtual_address in
  let ( export_dir
      , name_rva
      , number_of_functions
      , number_of_names
      , address_of_functions
      , address_of_names
      , address_of_name_ordinals ) =
    parse_export_entry data offset
  in
  let dir_name =
    Option.value ~default:""
      (let%bind offset = get_offset name_rva in
       get_string (Int.of_int64_exn offset) data)
  in
  let export_dir = {export_dir with name= dir_name} in
  let%map export_dir =
    if Int32.(number_of_names > 0l) then
      let%bind functions_offset = get_offset address_of_functions in
      let%bind names_offset = get_offset address_of_names in
      let%bind name_ordinals_offset = get_offset address_of_name_ordinals in
      let rec get_exports res i =
        if Int32.(i >= number_of_names) then return (List.rev res)
        else
          let name_offset = Int64.(names_offset + (of_int32 i * 4L)) in
          let name_rva = get_int32 data name_offset in
          let%bind name_offset = get_offset name_rva in
          let%bind name = get_string (Int.of_int64_exn name_offset) data in
          let ordinal_offset =
            Int64.(name_ordinals_offset + (of_int32 i * 2L))
          in
          let ordinal = get_int16 data ordinal_offset in
          let function_offset =
            Int64.(functions_offset + (of_int ordinal * 4L))
          in
          let function_rva = get_int32 data function_offset in
          let forwarded = Data_directory.is_rva_within entry function_rva in
          let export =
            Export.{virtual_address= function_rva; forwarded; name}
          in
          get_exports (export :: res) Int32.(i + 1l)
      in
      let%map exports = get_exports [] 0l in
      {export_dir with exports= Array.of_list exports}
    else return export_dir
  in
  (entry, export_dir)

let import_desc_size = 20

let parse_import_desc data offset =
  let pos = Int.of_int64_exn offset in
  let buf = Bigstring.to_bytes data ~pos ~len:import_desc_size in
  let bits = bitstring_of_bytes buf in
  match%bitstring bits with
  | {|
     original_first_thunk: 32: littleendian;
     time_date_stamp: 32: littleendian;
     forwarder_chain: 32: littleendian;
     name: 32: littleendian;
     first_thunk: 32: littleendian
     |}
    ->
      ( original_first_thunk
      , time_date_stamp
      , forwarder_chain
      , name
      , first_thunk )
  | {| _ |} -> invalid_arg "bad import descriptor"

let parse_imports data pe (entry : Data_directory.default) =
  let open Option.Let_syntax in
  let%bind _ = Option.some_if (is_valid_entry entry) () in
  let thunk_size, thunk_mask =
    match pe.kind with
    | `PE32 -> (4L, 0x0000000080000000L)
    | `PE64 -> (8L, 0x8000000000000000L)
  in
  let get_offset = file_offset_of_rva pe in
  let%bind offset = get_offset entry.virtual_address in
  let rec get_descriptors res i =
    let desc_offset = Int64.(offset + (i * of_int import_desc_size)) in
    let ( original_first_thunk
        , time_date_stamp
        , forwarder_chain
        , name_rva
        , first_thunk ) =
      parse_import_desc data desc_offset
    in
    if Int32.(original_first_thunk = 0l && name_rva = 0l && first_thunk = 0l)
    then return (List.rev res)
    else
      let%bind name_offset = get_offset name_rva in
      let%bind name = get_string (Int.of_int64_exn name_offset) data in
      let%bind thunk_offset =
        if Int32.(original_first_thunk <> 0l) then
          get_offset original_first_thunk
        else if Int32.(first_thunk <> 0l) then get_offset first_thunk
        else None
      in
      let rec get_imports res i =
        let offset = Int64.(i * thunk_size) in
        let thunk_offset = Int64.(thunk_offset + offset) in
        let thunk =
          match pe.kind with
          | `PE32 -> Int64.of_int32 (get_int32 data thunk_offset)
          | `PE64 -> get_int64 data thunk_offset
        in
        if Int64.(thunk = 0L) then return (List.rev res)
        else
          let%bind id =
            if Int64.(thunk land thunk_mask <> 0L) then
              return (Import.Ordinal Int.(of_int64_trunc thunk land 0xFFFF))
            else
              let%bind name_offset =
                get_offset (Int32.of_int64_trunc thunk)
              in
              let name_offset = Int64.(name_offset + 2L) in
              let%map name =
                get_string (Int.of_int64_exn name_offset) data
              in
              Import.Name name
          in
          let virtual_address =
            Int32.(first_thunk + of_int64_trunc offset)
          in
          let import =
            Import.{virtual_address; size= Int.of_int64_trunc thunk_size; id}
          in
          get_imports (import :: res) Int64.(i + 1L)
      in
      let%bind imports = get_imports [] 0L in
      let desc =
        Import_directory.
          { time_date_stamp
          ; forwarder_chain
          ; name
          ; imports= Array.of_list imports }
      in
      get_descriptors (desc :: res) Int64.(i + 1L)
  in
  let%map descriptors = get_descriptors [] 0L in
  let dir = Import_directory.{descriptors= Array.of_list descriptors} in
  (entry, dir)

let parse_reloc_type = function
  | 1 -> Some Base_reloc.HIGH
  | 2 -> Some Base_reloc.LOW
  | 3 -> Some Base_reloc.HIGHLOW
  | 10 -> Some Base_reloc.DIR64
  | _ -> None

let parse_base_relocs data pe (entry : Data_directory.default) =
  let open Option.Let_syntax in
  let%bind _ = Option.some_if (is_valid_entry entry) () in
  let get_offset = file_offset_of_rva pe in
  let%bind offset = get_offset entry.virtual_address in
  let size64 = Int64.(of_int32 entry.size land 0xFFFFFFFFL) in
  let end64 = Int64.(offset + size64) in
  let rec get_blocks res offset =
    if Int64.(offset >= end64) then return (List.rev res)
    else
      let base_addr = get_int32 data offset in
      let block_size = get_int32 data Int64.(offset + 4L) in
      let reloc_count = Int32.((block_size - 8l) / 2l) in
      let block_offset = Int64.(offset + 8L) in
      let rec get_relocs res i =
        if Int32.(i >= reloc_count) then return (List.rev res)
        else
          let block_offset = Int64.(block_offset + (of_int32 i * 2L)) in
          let reloc_data = get_int16 data block_offset in
          let reloc_offset = reloc_data land 0xFFF in
          let reloc_addr = Int32.(base_addr + of_int_exn reloc_offset) in
          let%bind value_offset = get_offset reloc_addr in
          let res =
            Option.value ~default:res
              (let kind = (reloc_data lsr 12) land 0xf in
               let%map kind = parse_reloc_type kind in
               let value, offset =
                 match kind with
                 | Base_reloc.HIGH ->
                     let value = get_int16 data value_offset in
                     let value = Int64.(of_int value land 0xFFFFL) in
                     ( value
                     , Int64.(
                         value - ((pe.image_base land 0xFFFF0000L) lsr 16))
                     )
                 | Base_reloc.LOW ->
                     let value = get_int16 data value_offset in
                     let value = Int64.(of_int value land 0xFFFFL) in
                     (value, Int64.(value - (pe.image_base land 0xFFFFL)))
                 | Base_reloc.HIGHLOW ->
                     let value = get_int32 data value_offset in
                     let value = Int64.(of_int32 value land 0xFFFFFFFFL) in
                     (value, Int64.(value - (pe.image_base land 0xFFFFFFFFL)))
                 | Base_reloc.DIR64 ->
                     let value = get_int64 data value_offset in
                     (value, Int64.(pe.image_base - value))
               in
               let reloc =
                 Base_reloc.
                   {kind; virtual_address= reloc_addr; value; offset}
               in
               reloc :: res)
          in
          get_relocs res Int32.(i + 1l)
      in
      let%bind relocs = get_relocs [] 0l in
      let block =
        Base_reloc_directory.
          { virtual_address= base_addr
          ; size= block_size
          ; relocs= Array.of_list relocs }
      in
      let offset = Int64.(offset + (of_int32 block_size land 0xFFFFFFFFL)) in
      get_blocks (block :: res) offset
  in
  let%map blocks = get_blocks [] offset in
  let reloc_dir = Base_reloc_directory.{blocks= Array.of_list blocks} in
  (entry, reloc_dir)

let runtime_function_size = 12

let parse_exception data pe (entry : Data_directory.default) =
  let open Option.Let_syntax in
  let%bind _ = Option.some_if (is_valid_entry entry) () in
  let get_offset = file_offset_of_rva pe in
  let%bind offset = get_offset entry.virtual_address in
  let size64 = Int64.(of_int32 entry.size land 0xFFFFFFFFL) in
  let end64 = Int64.(offset + size64) in
  let rec get_functions res offset =
    if Int64.(offset >= end64) then return (List.rev res)
    else
      let begin_address = get_int32 data offset in
      let end_address = get_int32 data Int64.(offset + 4L) in
      let unwind_data = get_int32 data Int64.(offset + 8L) in
      let%bind unwind_offset = get_offset unwind_data in
      let unwind_offset = Int64.to_int_exn unwind_offset in
      let version_flags = Bigstring.get_uint8 data ~pos:unwind_offset in
      let version = version_flags land 7 in
      let flags = version_flags lsr 3 in
      let size_of_prolog =
        Bigstring.get_uint8 data ~pos:(unwind_offset + 1)
      in
      let count_of_codes =
        Bigstring.get_uint8 data ~pos:(unwind_offset + 2)
      in
      let frame_info = Bigstring.get_uint8 data ~pos:(unwind_offset + 3) in
      let frame_register = frame_info land 0xF in
      let frame_offset = (frame_info lsr 4) * 16 in
      let rec get_codes res offset i =
        if i >= count_of_codes then return (List.rev res)
        else
          let frame_offset = Bigstring.get_uint8 data ~pos:offset in
          let code_and_info = Bigstring.get_uint8 data ~pos:(offset + 1) in
          let code = code_and_info land 0xF in
          let info = code_and_info lsr 4 in
          let unwind_code = Unwind_code.{frame_offset; code; info} in
          get_codes (unwind_code :: res) (offset + 2) (i + 1)
      in
      let unwind_offset = unwind_offset + 4 in
      let%bind unwind_codes = get_codes [] unwind_offset 0 in
      let unwind_offset = unwind_offset + (count_of_codes * 2) in
      let unwind_offset =
        if count_of_codes mod 2 <> 0 then unwind_offset + 2
        else unwind_offset
      in
      let unwind_offset = Int64.of_int unwind_offset in
      let extra =
        if flags land 4 <> 0 then
          let function_entry = get_int32 data unwind_offset in
          let chain_info = Chain_info.{function_entry} in
          Some (Unwind_info.Chain chain_info)
        else if flags land 2 <> 0 then
          let exception_handler = get_int32 data unwind_offset in
          let uhandler =
            Exception_handler.{exception_handler; scope_table= [||]}
          in
          Some (Unwind_info.Handler uhandler)
        else if flags land 1 <> 0 then
          let exception_handler = get_int32 data unwind_offset in
          let count = get_int32 data Int64.(unwind_offset + 4L) in
          let offset = Int64.(unwind_offset + 4L) in
          let rec get_data res i =
            if Int32.(i >= count) then return (List.rev res)
            else
              let offset = Int64.(offset + (of_int32 i * 16L)) in
              let begin_address = get_int32 data offset in
              let end_address = get_int32 data Int64.(offset + 4L) in
              let handler_address = get_int32 data Int64.(offset + 8L) in
              let jump_target = get_int32 data Int64.(offset + 12L) in
              let scope_record =
                Scope_record.
                  {begin_address; end_address; handler_address; jump_target}
              in
              get_data (scope_record :: res) Int32.(i + 1l)
          in
          let%bind exception_data = get_data [] 0l in
          let ehandler =
            Exception_handler.
              {exception_handler; scope_table= Array.of_list exception_data}
          in
          Some (Unwind_info.Handler ehandler)
        else None
      in
      let unwind_info =
        Unwind_info.
          { offset= unwind_data
          ; version
          ; flags
          ; size_of_prolog
          ; frame_register
          ; frame_offset
          ; unwind_codes= Array.of_list unwind_codes
          ; extra }
      in
      let runtime_function =
        Runtime_function.{begin_address; end_address; unwind_info}
      in
      let next_offset = Int64.(offset + of_int runtime_function_size) in
      get_functions (runtime_function :: res) next_offset
  in
  let%map functions = get_functions [] offset in
  let exception_dir =
    Exception_directory.{functions= Array.of_list functions}
  in
  (entry, exception_dir)

let parse_tls_directory data pe (tls_dir : Data_directory.default) =
  let open Option.Let_syntax in
  let%bind _ =
    Option.some_if
      Int32.(tls_dir.virtual_address <> 0l && tls_dir.size <> 0l)
      ()
  in
  let get_offset = file_offset_of_rva pe in
  let%bind offset = get_offset tls_dir.virtual_address in
  let is64 =
    match pe.kind with
    | `PE64 -> true
    | `PE32 -> false
  in
  let r = Binreader.from_bigstring data in
  let r3264 () =
    if is64 then Binreader.read64 r `LE
    else
      let%map b = Binreader.read32 r `LE in
      Int64.of_int32 b
  in
  let%bind _ = Binreader.seek r Int64.(to_int_exn offset) in
  let%bind start_address_of_raw_data = r3264 () in
  let%bind end_address_of_raw_data = r3264 () in
  let%bind address_of_index = r3264 () in
  let%bind address_of_callbacks = r3264 () in
  let%bind size_of_zero_fill = Binreader.read32 r `LE in
  let%bind characteristics = Binreader.read32 r `LE in
  let%bind offset =
    Int64.(
      to_int32_exn ((address_of_callbacks - pe.image_base) land 0xFFFFFFFFL))
    |> get_offset
  in
  let callbacks = Vector.create () in
  let rec loop () =
    let%bind addr = r3264 () in
    if Int64.(addr = zero) then return ()
    else (
      Vector.push_back callbacks addr;
      loop () )
  in
  let%bind _ = Binreader.seek r Int64.(to_int_exn offset) in
  let%map _ = loop () in
  let dir =
    Tls_directory.
      { start_address_of_raw_data
      ; end_address_of_raw_data
      ; address_of_index
      ; address_of_callbacks
      ; size_of_zero_fill
      ; characteristics
      ; callbacks= Vector.to_array callbacks }
  in
  (tls_dir, dir)

let from_bigstring_exn data =
  let e_lfanew =
    let hdr = Bigstring.to_bytes data ~len:dos_header_size in
    let bits = bitstring_of_bytes hdr in
    parse_dos_header bits
  in
  let ( pe
      , ( export_dir
        , import_dir
        , resource_dir
        , exception_dir
        , security_dir
        , base_reloc_dir
        , debug_dir
        , architecture_dir
        , global_ptr_dir
        , tls_dir
        , load_config_dir
        , bound_import_dir
        , iat_dir
        , delay_import_dir
        , com_descriptor_dir ) ) =
    let hdr =
      Bigstring.to_bytes data
        ~pos:(Int.of_int32_exn e_lfanew)
        ~len:max_nt_header_size
    in
    let bits = bitstring_of_bytes hdr in
    parse_nt_header e_lfanew bits
  in
  let sections =
    let tbl =
      { offset=
          Int64.(
            of_int32 e_lfanew + of_int file_header_size
            + of_int pe.file_header.size_of_optional_header)
      ; entry_size= section_header_size
      ; num_entries= pe.file_header.number_of_sections }
    in
    List.mapi (get_chunks tbl data) ~f:(fun i buf ->
        let bits = bitstring_of_bytes buf in
        let scn = parse_section_header bits in
        let name =
          let pos = Int.of_int64_exn tbl.offset + (tbl.entry_size * i) in
          get_string pos data
        in
        match name with
        | None -> scn
        | Some name ->
            let name =
              if (not (String.is_empty name)) && Char.(name.[0] = '/') then
                let offset = Int.of_string (String.subo name ~pos:1) in
                if offset = 0 then name
                else
                  let pos =
                    Int.of_int32_exn pe.file_header.pointer_to_symbol_table
                    + Int.of_int32_exn pe.file_header.number_of_symbols
                      * symbol_size
                    + offset
                  in
                  match get_string pos data with
                  | Some name when not (String.is_empty name) -> name
                  | _ -> name
              else name
            in
            {scn with name})
    |> Array.of_list
  in
  let symbols =
    let tbl =
      { offset= Int64.of_int32 pe.file_header.pointer_to_symbol_table
      ; entry_size= symbol_size
      ; num_entries= Int.of_int32_exn pe.file_header.number_of_symbols }
    in
    let aux_indices = Hash_set.create (module Int) in
    List.filter_mapi (get_chunks tbl data) ~f:(fun i buf ->
        if Hash_set.mem aux_indices i then None
        else
          let bits = bitstring_of_bytes buf in
          let sym, short, long = parse_symbol tbl.offset i data bits in
          for idx = i to i + sym.number_of_aux_symbols do
            Hash_set.add aux_indices idx
          done;
          Some (sym, short, long, i))
    |> List.map ~f:(fun (sym, short, long, i) ->
           if Int32.(short <> 0l) then
             let pos = Int.of_int64_exn tbl.offset + (tbl.entry_size * i) in
             let name = Bigstring.to_string ~pos ~len:8 data in
             let name =
               Option.(
                 value ~default:name
                   ( String.index name '\x00'
                   >>| fun i -> String.subo name ~len:i ))
             in
             Symbol.{sym with name}
           else
             let pos =
               Int.of_int64_exn tbl.offset
               + (tbl.entry_size * tbl.num_entries)
               + Int.of_int32_exn long
             in
             Option.(
               value ~default:sym
                 (get_string pos data >>| fun name -> {sym with name})))
    |> Array.of_list
  in
  let pe = {pe with sections} in
  let data_directory =
    Data_directory.
      { export= parse_exports data pe export_dir
      ; import= parse_imports data pe import_dir
      ; resource= default_parse_directory resource_dir
      ; exceptions= parse_exception data pe exception_dir
      ; security= default_parse_directory security_dir
      ; base_reloc= parse_base_relocs data pe base_reloc_dir
      ; debug= default_parse_directory debug_dir
      ; architecture= default_parse_directory architecture_dir
      ; global_ptr= default_parse_directory global_ptr_dir
      ; tls= parse_tls_directory data pe tls_dir
      ; load_config= default_parse_directory load_config_dir
      ; bound_import= default_parse_directory bound_import_dir
      ; iat= default_parse_directory iat_dir
      ; delay_import= default_parse_directory delay_import_dir
      ; com_descriptor= default_parse_directory com_descriptor_dir }
  in
  {pe with data_directory; symbols}

let from_bigstring data = Option.try_with (fun () -> from_bigstring_exn data)
