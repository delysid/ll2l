open Core_kernel
open Ll2l_arch
open Ll2l_std

module Meta : sig
  type t = PE of Pe.t | ELF of Elf.t | MachO of Mach_o.t
end

module Reloc : sig
  type t =
    { addr: Addr.t
    ; size: int
    ; value: int64 option
    ; name: string option
    ; offset: int64 option }

  val within : t -> Addr.t -> bool

  val within2 : t -> addr:Addr.t -> endaddr:Addr.t -> bool
end

type canonical_value = {value: Bitvec.t; size: int}

type t =
  { (* format-specific information *)
    meta: Meta.t
  ; (* endianness of the file *)
    endian: Endian.t
  ; (* the loaded memory segments *)
    mem: Memory.t
  ; (* main entry point *)
    start: Addr.t option
  ; (* relocatable data *)
    relocs: Reloc.t Addr.Map.t
  ; (* default instruction set *)
    arch: Arch.et
  ; (* specific register values at startup *)
    canonical_values: canonical_value String.Map.t }

val total_header_size : t -> int

val virtual_base : t -> Addr.t option

val find_intersecting_reloc : t -> Addr.t -> Reloc.t option

(* check three things:
 * 1. is the address divisible by the ISA's minimum instruction length?
 * 2. does the address point to an executable segment?
 * 3. does the address intersect with a known relocation? *)
val is_valid_pc :
     ?check_seg:bool
  -> ?check_reloc:bool
  -> ?arch:Arch.et option
  -> t
  -> Addr.t
  -> bool

(* collect a set of addresses which are known to point
 * to executable code. this is particularly useful for
 * implementing the disassembler (see Ll2l_program library) *)
val entries : t -> Arch.et Addr.Map.t

(* sweep all loaded segments and try to find all word-sized
 * values in the raw data which point to an executable segment.
 * these are potential instruction pointers, and most likely
 * contains a good number of false positives.
 *
 * an example of where this proves useful is finding all
 * the pointers in virtual method tables for C++ binaries,
 * since typically these pointers are referenced nowhere else,
 * making recovering these functions accurately with a disassembler
 * otherwise impossible without this knowledge. *)
val code_pointers : t -> Arch.et Addr.Map.t

val is_stub_addr : t -> Addr.t -> bool

(* check if a particular destination is known to point
 * to a symbol which represents a noreturn function *)
val is_known_noreturn :
     ?instr:(Addr.t * Arch.ei) option
  -> ?indirect:bool
  -> t
  -> dst:Addr.t
  -> bool
