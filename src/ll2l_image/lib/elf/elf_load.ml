open Core_kernel
open Ll2l_arch
open Ll2l_std
open Elf
module M = Memory

let mips_arch_of elf =
  let estr = Endian.to_string elf.endian in
  let str =
    match Int32.(elf.flags land 0xF0000000l) with
    | 0x00000000l (* MIPS 1 *) | 0x10000000l (* MIPS 2 *) | 0x50000000l ->
        "mips32"
    | 0x20000000l
    (* MIPS 3 *)
     |0x30000000l
    (* MIPS 4 *)
     |0x40000000l
    (* MIPS 5 *)
     |0x60000000l -> "mips64"
    | 0x70000000l -> "mips32r2"
    | 0x80000000l -> "mips64r2"
    | 0x90000000l -> "mips32r6"
    | 0xA0000000l -> "mips64r6"
    | _ -> invalid_arg "invalid MIPS architecture"
  in
  Arch.of_string (str ^ estr)

let arm_arch_of elf =
  let estr =
    match elf.endian with
    | `LE -> "le"
    | `BE -> "be"
  in
  (* XXX: can ELF select armv8/armv7thumb? *)
  let is_thumb = Int64.(elf.entry land one = one) in
  let str = if is_thumb then "thumbv7" else "armv7" in
  Arch.of_string (str ^ estr)

let riscv_arch_of elf =
  if Int32.(elf.flags land 0x0008l <> zero) then
    failwith "RISC-V RVE ABI is unsupported"
  else
    let c_str =
      if Int32.(elf.flags land 0x0001l <> zero) then ".c" else ""
    in
    let f_str =
      match Int32.(elf.flags land 0x0006l) with
      | 0x0006l -> ".fq"
      | 0x0004l -> ".fd"
      | 0x0002l -> ".fs"
      | _ -> ""
    in
    let base_str =
      match elf.eclass with
      | `ELF32 -> "riscv32"
      | `ELF64 -> "riscv64"
    in
    Arch.of_string (base_str ^ c_str ^ f_str)

let arch_of elf =
  match elf.machine with
  | Machine.I386 -> Arch.of_string "ia32"
  | Machine.X86_64 -> Arch.of_string "amd64"
  | Machine.MIPS | Machine.MIPS_RS3_LE -> mips_arch_of elf
  | Machine.ARM -> arm_arch_of elf
  | Machine.RISCV -> riscv_arch_of elf
  | _ -> invalid_arg "unsupported architecture"

let load_segments elf data mem =
  Array.iter elf.segments ~f:(fun seg ->
      match seg.typ with
      | Segment.T_LOAD -> (
          let data =
            let pos, len =
              (Int.of_int64_exn seg.offset, Int.of_int64_exn seg.filesz)
            in
            let data = Bigstring.sub data ~pos ~len in
            if Int64.(seg.memsz > seg.filesz) then
              (* if the size of the segment in memory exceeds
               * its size in the file, then the ELF spec says
               * that we pad the remaining bytes with zeroes
               *
               * e.g. the .bss section (indicated by SHT_NOBITS)
               * does not exactly correspond to the contents of
               * the file, but rather the contents in memory
               * when the file is actually loaded *)
              let diff = Int64.(to_int_exn (seg.memsz - seg.filesz)) in
              let data' = Bigstring.init diff ~f:(fun _ -> '\x00') in
              Bigstring.concat [data; data']
            else data
          in
          let r, w, x =
            let init = (false, false, false) in
            List.fold seg.flags ~init ~f:(fun (r, w, x) f ->
                match f with
                | Segment.F_R -> (true, w, x)
                | Segment.F_W -> (r, true, x)
                | Segment.F_X -> (r, w, true)
                | _ -> (r, w, x))
          in
          if x && not (Array.is_empty elf.sections) then
            (* executable PT_LOAD segments sometimes include
             * sections that are not executable, so we should
             * have the option of using the section header
             * information instead, as this can generally
             * lead to better results when disassembling
             *
             * TODO: make this a user-defined parameter *)
            Array.iter elf.sections ~f:(fun sec ->
                if
                  Int64.(sec.size > zero)
                  && Int64.(sec.offset >= seg.offset)
                  && Int64.(sec.offset + sec.size <= seg.offset + seg.filesz)
                then
                  let w, x =
                    List.fold sec.flags ~init:(false, false)
                      ~f:(fun (w, x) f ->
                        match f with
                        | Section.F_WRITE -> (true, x)
                        | Section.F_EXECINSTR -> (w, true)
                        | _ -> (w, x))
                  in
                  let pos = Int64.(to_int_exn (sec.offset - seg.offset)) in
                  let len = Int64.to_int_exn sec.size in
                  let addr = Addr.int64 sec.addr in
                  let data = Bigstring.sub data ~pos ~len in
                  match
                    M.add_segment_from_bigstring mem addr data ~r ~w ~x
                  with
                  | Error `BadAddr ->
                      invalid_arg
                        (Printf.sprintf
                           "bad segment address 0x%016LX for section %s"
                           (Addr.to_int64 addr) sec.name)
                  | _ -> ()
                else ())
          else
            let addr = Addr.int64 seg.vaddr in
            match M.add_segment_from_bigstring mem addr data ~r ~w ~x with
            | Error `BadAddr ->
                invalid_arg
                  (Printf.sprintf "bad segment address 0x%016LX"
                     (Addr.to_int64 addr))
            | _ -> () )
      | _ -> ())

let read_rel_i386 mem (rel : Reloc.t) endian = function
  | Reloc.I386_RELATIVE -> (
    match M.read_int32 mem (Addr.int64 rel.offset) endian with
    | Ok value -> Some Int64.(of_int32 value land 0xFFFFFFFFL)
    | Error _ -> None )
  | _ -> None

let read_rel_x86_64 mem (rel : Reloc.t) endian = function
  | Reloc.X86_64_RELATIVE -> rel.addend
  | _ -> None

let read_rel_mips sz mem (rel : Reloc.t) endian = function
  | Reloc.MIPS_IMPLICIT_RELATIVE -> (
    match sz with
    | 32 -> (
      match M.read_int32 mem (Addr.int64 rel.offset) endian with
      | Ok value -> Some Int64.(of_int32 value land 0xFFFFFFFL)
      | Error _ -> None )
    | 64 -> (
      match M.read_int64 mem (Addr.int64 rel.offset) endian with
      | Ok value -> Some value
      | Error _ -> None )
    | _ -> None )
  | _ -> None

let read_rel_arm mem (rel : Reloc.t) endian = function
  | Reloc.ARM_RELATIVE -> (
    match M.read_int32 mem (Addr.int64 rel.offset) endian with
    | Ok value -> Some Int64.(of_int32 value land 0xFFFFFFFFL)
    | Error _ -> None )
  | _ -> None

let collect_relocs elf mem arch =
  let size =
    match elf.eclass with
    | `ELF32 -> 32
    | `ELF64 -> 64
  in
  Array.fold elf.relocs ~init:Addr.Map.empty ~f:(fun m rel ->
      let value =
        match rel.typ with
        | Some (Reloc.I386 typ) -> read_rel_i386 mem rel elf.endian typ
        | Some (Reloc.X86_64 typ) -> read_rel_x86_64 mem rel elf.endian typ
        | Some (Reloc.Mips typ) ->
            let Arch.(T arch) = arch in
            let sz = Arch.word_size_of arch in
            read_rel_mips sz mem rel elf.endian typ
        | Some (Reloc.Arm typ) -> read_rel_arm mem rel elf.endian typ
        | _ -> None
      in
      let name =
        match rel.symbol with
        | Some sym -> Some sym.name
        | None -> None
      in
      let key = Addr.int64 rel.offset in
      let reloc =
        Image.Reloc.{addr= key; size; value; name; offset= rel.addend}
      in
      Map.set m ~key ~data:reloc)

let collect_canonical_values elf arch =
  let Arch.(T arch) = arch in
  let default = String.Map.empty in
  match arch with
  | Arch.MIPS _ ->
      Array.find_map elf.dynamic ~f:(fun dyn ->
          match dyn.tag with
          | Some Dyn.T_PLTGOT ->
              let word_sz = Arch.word_size_of arch in
              let m = Bitvec.modulus word_sz in
              let v = Int64.(dyn.value + 0x7FF0L) in
              let v = Bitvec.(int64 v mod m) in
              let cv = Image.{value= v; size= word_sz} in
              Some (String.Map.singleton "GP" cv)
          | _ -> None)
      |> Option.value ~default
  | Arch.RISCV _ ->
      Array.find_map elf.symbols ~f:(fun sym ->
          match sym.name with
          | "__global_pointer$" ->
              let word_sz = Arch.word_size_of arch in
              let m = Bitvec.modulus word_sz in
              let v = Bitvec.(int64 sym.value mod m) in
              let cv = Image.{value= v; size= word_sz} in
              Some (String.Map.singleton "GP" cv)
          | _ -> None)
      |> Option.value ~default
  | _ -> default

let from_bigstring_exn data =
  let elf = Elf_parse.from_bigstring_exn data in
  let arch = arch_of elf in
  let mem = M.create () in
  load_segments elf data mem;
  let relocs = collect_relocs elf mem arch in
  let img =
    Image.
      { meta= ELF elf
      ; endian= elf.endian
      ; mem
      ; start= None
      ; relocs
      ; arch
      ; canonical_values= collect_canonical_values elf arch }
  in
  let validate_addr a =
    let a, arch = Arch.addr_of_arch arch a in
    Option.some_if Image.(is_valid_pc img a) (a, arch)
  in
  let start =
    match validate_addr (Addr.int64 elf.entry) with
    | None -> None
    | Some (a, _) -> Some a
  in
  {img with start}

let from_bigstring data =
  Option.try_with (fun () -> from_bigstring_exn data)
  [@@inline]
