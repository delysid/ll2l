open Core_kernel

val is_elf : bytes -> bool

val is_elf_file : string -> bool

val from_bigstring_exn : Bigstring.t -> Elf.t

val from_bigstring : Bigstring.t -> Elf.t option
