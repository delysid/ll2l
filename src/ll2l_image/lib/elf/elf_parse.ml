open Core_kernel
open Bitstring
open Stdio
open Ll2l_std
open Elf

let parse_e_class = function
  | 1 -> `ELF32
  | 2 -> `ELF64
  | n -> invalid_arg (sprintf "bad e_class %d" n)

let parse_e_data = function
  | 1 -> `LE
  | 2 -> `BE
  | n -> invalid_arg (sprintf "bad e_data %d" n)

let parse_e_osabi = function
  | 0 -> Osabi.SYSV
  | 1 -> Osabi.HPUX
  | 2 -> Osabi.NETBSD
  | 3 -> Osabi.LINUX
  | 4 -> Osabi.HURD
  | 5 -> Osabi.OSABI_86OPEN
  | 6 -> Osabi.SOLARIS
  | 7 -> Osabi.AIX
  | 8 -> Osabi.IRIX
  | 9 -> Osabi.FREEBSD
  | 10 -> Osabi.TRU64
  | 11 -> Osabi.MODESTO
  | 12 -> Osabi.OPENBSD
  | 13 -> Osabi.OPENVMS
  | 14 -> Osabi.NSK
  | 15 -> Osabi.AROS
  | 97 -> Osabi.ARM
  | 255 -> Osabi.STANDALONE
  | n -> Osabi.EXT n

let parse_e_type = function
  | 0 -> Type.NONE
  | 1 -> Type.REL
  | 2 -> Type.EXEC
  | 3 -> Type.DYN
  | 4 -> Type.CORE
  | n -> Type.EXT n

let parse_e_machine = function
  | 0 -> Machine.NONE
  | 1 -> Machine.M32
  | 2 -> Machine.SPARC
  | 3 -> Machine.I386
  | 4 -> Machine.M68K
  | 5 -> Machine.M88K
  | 6 -> Machine.I486
  | 7 -> Machine.I80860
  | 8 -> Machine.MIPS
  | 9 -> Machine.S370
  | 10 -> Machine.MIPS_RS3_LE
  | 15 -> Machine.PARISC
  | 17 -> Machine.VPP500
  | 18 -> Machine.SPARC32PLUS
  | 19 -> Machine.I80960
  | 20 -> Machine.PPC
  | 21 -> Machine.PPC64
  | 22 -> Machine.S390
  | 36 -> Machine.V800
  | 37 -> Machine.FR20
  | 38 -> Machine.RH32
  | 39 -> Machine.RCE
  | 40 -> Machine.ARM
  | 41 -> Machine.ALPHA_STD
  | 42 -> Machine.SH
  | 43 -> Machine.SPARCV9
  | 44 -> Machine.TRICORE
  | 45 -> Machine.ARC
  | 46 -> Machine.H8_300
  | 47 -> Machine.H8_300H
  | 48 -> Machine.H8S
  | 49 -> Machine.H8_500
  | 50 -> Machine.IA_64
  | 51 -> Machine.MIPS_X
  | 52 -> Machine.COLDFIRE
  | 53 -> Machine.M68HC12
  | 54 -> Machine.MMA
  | 55 -> Machine.PCP
  | 56 -> Machine.NCPU
  | 57 -> Machine.NDR1
  | 58 -> Machine.STARCORE
  | 59 -> Machine.ME16
  | 60 -> Machine.ST100
  | 61 -> Machine.TINYJ
  | 62 -> Machine.X86_64
  | 63 -> Machine.PDSP
  | 66 -> Machine.FX66
  | 67 -> Machine.ST9PLUS
  | 68 -> Machine.ST7
  | 69 -> Machine.M68HC16
  | 70 -> Machine.M68HC11
  | 71 -> Machine.M68HC08
  | 72 -> Machine.M68HC05
  | 73 -> Machine.SVX
  | 74 -> Machine.ST19
  | 75 -> Machine.VAX
  | 76 -> Machine.CRIS
  | 77 -> Machine.JAVELIN
  | 78 -> Machine.FIREPATH
  | 79 -> Machine.ZSP
  | 80 -> Machine.MMIX
  | 81 -> Machine.HUANY
  | 82 -> Machine.PRISM
  | 83 -> Machine.AVR
  | 84 -> Machine.FR30
  | 85 -> Machine.D10V
  | 86 -> Machine.D30V
  | 87 -> Machine.V850
  | 88 -> Machine.M32R
  | 89 -> Machine.MN10300
  | 90 -> Machine.MN10200
  | 91 -> Machine.PJ
  | 92 -> Machine.OPENRISC
  | 93 -> Machine.ARC_A5
  | 94 -> Machine.XTENSA
  | 95 -> Machine.VIDEOCORE
  | 96 -> Machine.TM_GPP
  | 97 -> Machine.NS32K
  | 98 -> Machine.TPC
  | 99 -> Machine.SNP1K
  | 100 -> Machine.ST200
  | 101 -> Machine.IP2K
  | 102 -> Machine.MAX
  | 103 -> Machine.CR
  | 104 -> Machine.F2MC16
  | 105 -> Machine.MSP430
  | 106 -> Machine.BLACKFIN
  | 107 -> Machine.SE_C33
  | 108 -> Machine.SEP
  | 109 -> Machine.ARCA
  | 110 -> Machine.UNICORE
  | 183 -> Machine.AARCH64
  | 243 -> Machine.RISCV
  | 0x9026 -> Machine.ALPHA
  | n -> Machine.EXT n

let parse_sh_type = function
  | 0l -> Section.T_NULL
  | 1l -> Section.T_PROGBITS
  | 2l -> Section.T_SYMTAB
  | 3l -> Section.T_STRTAB
  | 4l -> Section.T_RELA
  | 5l -> Section.T_HASH
  | 6l -> Section.T_DYNAMIC
  | 7l -> Section.T_NOTE
  | 8l -> Section.T_NOBITS
  | 9l -> Section.T_REL
  | 10l -> Section.T_SHLIB
  | 11l -> Section.T_DYNSYM
  | 14l -> Section.T_INIT_ARRAY
  | 15l -> Section.T_FINI_ARRAY
  | 16l -> Section.T_PREINIT_ARRAY
  | 17l -> Section.T_GROUP
  | 18l -> Section.T_SYMTAB_SHNDX
  | n -> Section.T_EXT n

let parse_sh_flag = function
  | 0 -> Section.F_WRITE
  | 1 -> Section.F_ALLOC
  | 2 -> Section.F_EXECINSTR
  | n -> Section.F_EXT n

let parse_p_type = function
  | 0l -> Segment.T_NULL
  | 1l -> Segment.T_LOAD
  | 2l -> Segment.T_DYNAMIC
  | 3l -> Segment.T_INTERP
  | 4l -> Segment.T_NOTE
  | 5l -> Segment.T_SHLIB
  | 6l -> Segment.T_PHDR
  | 7l -> Segment.T_TLS
  | n -> Segment.T_EXT n

let parse_p_flag = function
  | 0 -> Segment.F_X
  | 1 -> Segment.F_W
  | 2 -> Segment.F_R
  | n -> Segment.F_EXT n

let parse_st_bind = function
  | 0 -> Symbol.B_LOCAL
  | 1 -> Symbol.B_GLOBAL
  | 2 -> Symbol.B_WEAK
  | n -> Symbol.B_EXT n

let parse_st_type = function
  | 0 -> Symbol.T_NOTYPE
  | 1 -> Symbol.T_OBJECT
  | 2 -> Symbol.T_FUNC
  | 3 -> Symbol.T_SECTION
  | 4 -> Symbol.T_FILE
  | 5 -> Symbol.T_COMMON
  | 6 -> Symbol.T_TLS
  | 7 -> Symbol.T_NUM
  | n -> Symbol.T_EXT n

let parse_st_visibility = function
  | 0 -> Symbol.V_DEFAULT
  | 1 -> Symbol.V_INTERNAL
  | 2 -> Symbol.V_HIDDEN
  | 3 -> Symbol.V_PROTECTED
  | 4 -> Symbol.V_EXPORTED
  | 5 -> Symbol.V_SINGLETON
  | 6 -> Symbol.V_ELIMINATE
  | n -> invalid_arg (sprintf "bad st_visibility %d" n)

let parse_r_type_i386 = function
  | 0 -> Reloc.I386_NONE
  | 1 -> Reloc.I386_32
  | 2 -> Reloc.I386_PC32
  | 3 -> Reloc.I386_GOT32
  | 4 -> Reloc.I386_PLT32
  | 5 -> Reloc.I386_COPY
  | 6 -> Reloc.I386_GLOB_DAT
  | 7 -> Reloc.I386_JMP_SLOT
  | 8 -> Reloc.I386_RELATIVE
  | 9 -> Reloc.I386_GOTOFF
  | 10 -> Reloc.I386_GOTPC
  | 14 -> Reloc.I386_TLS_TPOFF
  | 15 -> Reloc.I386_TLS_IE
  | 16 -> Reloc.I386_TLS_GOTIE
  | 17 -> Reloc.I386_TLS_LE
  | 18 -> Reloc.I386_TLS_GD
  | 19 -> Reloc.I386_TLS_LDM
  | 24 -> Reloc.I386_TLS_GD_32
  | 25 -> Reloc.I386_TLS_GD_PUSH
  | 26 -> Reloc.I386_TLS_GD_CALL
  | 27 -> Reloc.I386_TLS_GD_POP
  | 28 -> Reloc.I386_TLS_LDM_32
  | 29 -> Reloc.I386_TLS_LDM_PUSH
  | 30 -> Reloc.I386_TLS_LDM_CALL
  | 31 -> Reloc.I386_TLS_LDM_POP
  | 32 -> Reloc.I386_TLS_LDO_32
  | 33 -> Reloc.I386_TLS_IE_32
  | 34 -> Reloc.I386_TLS_LE_32
  | 35 -> Reloc.I386_TLS_TRPMOD32
  | 36 -> Reloc.I386_TLS_DTMOFF32
  | 37 -> Reloc.I386_TLS_TPOFF32
  | 42 -> Reloc.I386_IRELATIVE
  | n -> invalid_arg (sprintf "bad i386 r_type %d" n)

let parse_r_type_arm = function
  | 0 -> Reloc.ARM_NONE
  | 1 -> Reloc.ARM_PC24
  | 2 -> Reloc.ARM_ABS32
  | 3 -> Reloc.ARM_REL32
  | 4 -> Reloc.ARM_PC13
  | 5 -> Reloc.ARM_ABS16
  | 6 -> Reloc.ARM_ABS12
  | 7 -> Reloc.ARM_THM_ABS5
  | 8 -> Reloc.ARM_ABS8
  | 9 -> Reloc.ARM_SBREL32
  | 10 -> Reloc.ARM_THM_PC22
  | 11 -> Reloc.ARM_THM_PC8
  | 12 -> Reloc.ARM_AMP_VCALL9
  | 13 -> Reloc.ARM_SWI24
  | 14 -> Reloc.ARM_THM_SWI8
  | 15 -> Reloc.ARM_XPC25
  | 16 -> Reloc.ARM_THM_XPC22
  | 17 -> Reloc.ARM_TLS_DTPMOD32
  | 18 -> Reloc.ARM_TLS_DTPOFF32
  | 19 -> Reloc.ARM_TLS_TPOFF32
  | 20 -> Reloc.ARM_COPY
  | 21 -> Reloc.ARM_GLOB_DAT
  | 22 -> Reloc.ARM_JMP_SLOT
  | 23 -> Reloc.ARM_RELATIVE
  | 24 -> Reloc.ARM_GOTOFF
  | 25 -> Reloc.ARM_GOTPC
  | 26 -> Reloc.ARM_GOT32
  | 27 -> Reloc.ARM_PLT32
  | 100 -> Reloc.ARM_GNU_VTENTRY
  | 101 -> Reloc.ARM_GNU_VTINHERIT
  | 160 -> Reloc.ARM_IRELATIVE
  | 250 -> Reloc.ARM_RSBREL32
  | 251 -> Reloc.ARM_THM_RPC22
  | 252 -> Reloc.ARM_RREL32
  | 253 -> Reloc.ARM_RABS32
  | 254 -> Reloc.ARM_RPC24
  | 255 -> Reloc.ARM_RBASE
  | n -> invalid_arg (sprintf "bad arm r_type %d" n)

let parse_r_type_ia_64 = function
  | 0 -> Reloc.IA_64_NONE
  | 0x21 -> Reloc.IA_64_IMM14
  | 0x22 -> Reloc.IA_64_IMM22
  | 0x23 -> Reloc.IA_64_IMM64
  | 0x24 -> Reloc.IA_64_DIR32MSB
  | 0x25 -> Reloc.IA_64_DIR32LSB
  | 0x26 -> Reloc.IA_64_DIR64MSB
  | 0x27 -> Reloc.IA_64_DIR64LSB
  | 0x2A -> Reloc.IA_64_GPREL22
  | 0x2B -> Reloc.IA_64_GPREL64I
  | 0x2C -> Reloc.IA_64_GPREL32MSB
  | 0x2D -> Reloc.IA_64_GPREL32LSB
  | 0x2E -> Reloc.IA_64_GPREL64MSB
  | 0x2F -> Reloc.IA_64_GPREL64LSB
  | 0x32 -> Reloc.IA_64_LTOFF22
  | 0x33 -> Reloc.IA_64_LTOFF64I
  | 0x3A -> Reloc.IA_64_PLTOFF22
  | 0x3B -> Reloc.IA_64_PLTOFF64I
  | 0x3E -> Reloc.IA_64_PLTOFF64MSB
  | 0x3F -> Reloc.IA_64_PLTOFF64LSB
  | 0x43 -> Reloc.IA_64_FPTR64I
  | 0x44 -> Reloc.IA_64_FPTR32MSB
  | 0x45 -> Reloc.IA_64_FPTR32LSB
  | 0x46 -> Reloc.IA_64_FPTR64MSB
  | 0x47 -> Reloc.IA_64_FPTR64LSB
  | 0x48 -> Reloc.IA_64_PCREL60B
  | 0x49 -> Reloc.IA_64_PCREL21B
  | 0x4A -> Reloc.IA_64_PCREL21M
  | 0x4B -> Reloc.IA_64_PCREL21F
  | 0x4C -> Reloc.IA_64_PCREL32MSB
  | 0x4D -> Reloc.IA_64_PCREL32LSB
  | 0x4E -> Reloc.IA_64_PCREL64MSB
  | 0x4F -> Reloc.IA_64_PCREL64LSB
  | 0x52 -> Reloc.IA_64_LTOFF_FPTR22
  | 0x53 -> Reloc.IA_64_LTOFF_FPTR64I
  | 0x54 -> Reloc.IA_64_LTOFF_FPTR32MSB
  | 0x55 -> Reloc.IA_64_LTOFF_FPTR32LSB
  | 0x56 -> Reloc.IA_64_LTOFF_FPTR64MSB
  | 0x57 -> Reloc.IA_64_LTOFF_FPTR64LSB
  | 0x5C -> Reloc.IA_64_SEGREL32MSB
  | 0x5D -> Reloc.IA_64_SEGREL32LSB
  | 0x5E -> Reloc.IA_64_SEGREL64MSB
  | 0x5F -> Reloc.IA_64_SEGREL64LSB
  | 0x64 -> Reloc.IA_64_SECREL32MSB
  | 0x65 -> Reloc.IA_64_SECREL32LSB
  | 0x66 -> Reloc.IA_64_SECREL64MSB
  | 0x67 -> Reloc.IA_64_SECREL64LSB
  | 0x6C -> Reloc.IA_64_REL32MSB
  | 0x6D -> Reloc.IA_64_REL32LSB
  | 0x6E -> Reloc.IA_64_REL64MSB
  | 0x6F -> Reloc.IA_64_REL64LSB
  | 0x74 -> Reloc.IA_64_LTV32MSB
  | 0x75 -> Reloc.IA_64_LTV32LSB
  | 0x76 -> Reloc.IA_64_LTV64MSB
  | 0x77 -> Reloc.IA_64_LTV64LSB
  | 0x79 -> Reloc.IA_64_PCREL21BI
  | 0x7A -> Reloc.IA_64_PCREL22
  | 0x7B -> Reloc.IA_64_PCREL64I
  | 0x80 -> Reloc.IA_64_IPLTMSB
  | 0x81 -> Reloc.IA_64_IPLTLSB
  | 0x85 -> Reloc.IA_64_SUB
  | 0x86 -> Reloc.IA_64_LTOFF22X
  | 0x87 -> Reloc.IA_64_LDXMOV
  | 0x91 -> Reloc.IA_64_TPREL14
  | 0x92 -> Reloc.IA_64_TPREL22
  | 0x93 -> Reloc.IA_64_TPREL64I
  | 0x96 -> Reloc.IA_64_TPREL64MSB
  | 0x97 -> Reloc.IA_64_TPREL64LSB
  | 0x9A -> Reloc.IA_64_LTOFF_TPREL22
  | 0xA6 -> Reloc.IA_64_DTPMOD64MSB
  | 0xA7 -> Reloc.IA_64_DTPMOD64LSB
  | 0xAA -> Reloc.IA_64_LTOFF_DTPMOD22
  | 0xB1 -> Reloc.IA_64_DTPREL14
  | 0xB2 -> Reloc.IA_64_DTPREL22
  | 0xB3 -> Reloc.IA_64_DTPREL64I
  | 0xB4 -> Reloc.IA_64_DTPREL32MSB
  | 0xB5 -> Reloc.IA_64_DTPREL32LSB
  | 0xB6 -> Reloc.IA_64_DTPREL64MSB
  | 0xB7 -> Reloc.IA_64_DTPREL64LSB
  | 0xBA -> Reloc.IA_64_LTOFF_DTPREL22
  | n -> invalid_arg (sprintf "bad ia64 r_type %d" n)

let parse_r_type_mips = function
  | 0 -> Reloc.MIPS_NONE
  | 1 -> Reloc.MIPS_16
  | 2 -> Reloc.MIPS_32
  | 3 -> Reloc.MIPS_REL32
  | 4 -> Reloc.MIPS_26
  | 5 -> Reloc.MIPS_HI16
  | 6 -> Reloc.MIPS_LO16
  | 7 -> Reloc.MIPS_GPREL16
  | 8 -> Reloc.MIPS_LITERAL
  | 9 -> Reloc.MIPS_GOT16
  | 10 -> Reloc.MIPS_PC16
  | 11 -> Reloc.MIPS_CALL16
  | 12 -> Reloc.MIPS_GPREL32
  | 21 -> Reloc.MIPS_GOTHI16
  | 22 -> Reloc.MIPS_GOTLO16
  | 30 -> Reloc.MIPS_CALLHI16
  | 31 -> Reloc.MIPS_CALLLO16
  | 51 -> Reloc.MIPS_GLOB_DAT
  | 126 -> Reloc.MIPS_COPY
  | 127 -> Reloc.MIPS_JMP_SLOT
  | 255 -> Reloc.MIPS_IMPLICIT_RELATIVE
  | n -> invalid_arg (sprintf "bad mips r_type %d" n)

let parse_r_type_ppc = function
  | 0 -> Reloc.PPC_NONE
  | 1 -> Reloc.PPC_ADDR32
  | 2 -> Reloc.PPC_ADDR24
  | 3 -> Reloc.PPC_ADDR16
  | 4 -> Reloc.PPC_ADDR16_LO
  | 5 -> Reloc.PPC_ADDR16_HI
  | 6 -> Reloc.PPC_ADDR16_HA
  | 7 -> Reloc.PPC_ADDR14
  | 8 -> Reloc.PPC_ADDR14_BRTAKEN
  | 9 -> Reloc.PPC_ADDR14_BRNTAKEN
  | 10 -> Reloc.PPC_REL24
  | 11 -> Reloc.PPC_REL14
  | 12 -> Reloc.PPC_REL14_BRTAKEN
  | 13 -> Reloc.PPC_REL14_BRNTAKEN
  | 14 -> Reloc.PPC_GOT16
  | 15 -> Reloc.PPC_GOT16_LO
  | 16 -> Reloc.PPC_GOT16_HI
  | 17 -> Reloc.PPC_GOT16_HA
  | 18 -> Reloc.PPC_PLTREL24
  | 19 -> Reloc.PPC_COPY
  | 20 -> Reloc.PPC_GLOB_DAT
  | 21 -> Reloc.PPC_JMP_SLOT
  | 22 -> Reloc.PPC_RELATIVE
  | 23 -> Reloc.PPC_LOCAL24PC
  | 24 -> Reloc.PPC_UADDR32
  | 25 -> Reloc.PPC_UADDR16
  | 26 -> Reloc.PPC_REL32
  | 27 -> Reloc.PPC_PLT32
  | 28 -> Reloc.PPC_PLTREL32
  | 29 -> Reloc.PPC_PLT16_LO
  | 30 -> Reloc.PPC_PLT16_HI
  | 31 -> Reloc.PPC_PLT16_HA
  | 32 -> Reloc.PPC_SDAREL16
  | 33 -> Reloc.PPC_SECTOFF
  | 34 -> Reloc.PPC_SECTOFF_LO
  | 35 -> Reloc.PPC_SECTOFF_HI
  | 36 -> Reloc.PPC_SECTOFF_HA
  | 67 -> Reloc.PPC_TLS
  | 68 -> Reloc.PPC_DTPMOD32
  | 69 -> Reloc.PPC_TPREL16
  | 70 -> Reloc.PPC_TPREL16_LO
  | 71 -> Reloc.PPC_TPREL16_HI
  | 72 -> Reloc.PPC_TPREL16_HA
  | 73 -> Reloc.PPC_TPREL32
  | 74 -> Reloc.PPC_DTPREL16
  | 75 -> Reloc.PPC_DTPREL16_LO
  | 76 -> Reloc.PPC_DTPREL16_HI
  | 77 -> Reloc.PPC_DTPREL16_HA
  | 78 -> Reloc.PPC_DTPREL32
  | 79 -> Reloc.PPC_GOT_TLSGD16
  | 80 -> Reloc.PPC_GOT_TLSGD16_LO
  | 81 -> Reloc.PPC_GOT_TLSGD16_HI
  | 82 -> Reloc.PPC_GOT_TLSGD16_HA
  | 83 -> Reloc.PPC_GOT_TLSLD16
  | 84 -> Reloc.PPC_GOT_TLSLD16_LO
  | 85 -> Reloc.PPC_GOT_TLSLD16_HI
  | 86 -> Reloc.PPC_GOT_TLSLD16_HA
  | 87 -> Reloc.PPC_GOT_TPREL16
  | 88 -> Reloc.PPC_GOT_TPREL16_LO
  | 89 -> Reloc.PPC_GOT_TPREL16_HI
  | 90 -> Reloc.PPC_GOT_TPREL16_HA
  | 101 -> Reloc.PPC_EMB_NADDR32
  | 102 -> Reloc.PPC_EMB_NADDR16
  | 103 -> Reloc.PPC_EMB_NADDR16_LO
  | 104 -> Reloc.PPC_EMB_NADDR16_HI
  | 105 -> Reloc.PPC_EMB_NADDR16_HA
  | 106 -> Reloc.PPC_EMB_SDAI16
  | 107 -> Reloc.PPC_EMB_SDA2I16
  | 108 -> Reloc.PPC_EMB_SDA2REL
  | 109 -> Reloc.PPC_EMB_SDA21
  | 110 -> Reloc.PPC_EMB_MRKREF
  | 111 -> Reloc.PPC_EMB_RELSEC16
  | 112 -> Reloc.PPC_EMB_RELST_LO
  | 113 -> Reloc.PPC_EMB_RELST_HI
  | 114 -> Reloc.PPC_EMB_RELST_HA
  | 115 -> Reloc.PPC_EMB_BIT_FLD
  | 116 -> Reloc.PPC_EMB_RELSDA
  | n -> invalid_arg (sprintf "bad ppc r_type %d" n)

let parse_r_type_ppc64 = function
  | 38 -> Reloc.PPC64_ADDR64
  | 39 -> Reloc.PPC64_ADDR16_HIGHER
  | 40 -> Reloc.PPC64_ADDR16_HIGHERA
  | 41 -> Reloc.PPC64_ADDR16_HIGHEST
  | 42 -> Reloc.PPC64_ADDR16_HIGHESTA
  | 43 -> Reloc.PPC64_UADDR64
  | 44 -> Reloc.PPC64_REL64
  | 45 -> Reloc.PPC64_PLT64
  | 46 -> Reloc.PPC64_PLTREL64
  | 47 -> Reloc.PPC64_TOC16
  | 48 -> Reloc.PPC64_TOC16_LO
  | 49 -> Reloc.PPC64_TOC16_HI
  | 50 -> Reloc.PPC64_TOC16_HA
  | 51 -> Reloc.PPC64_TOC
  | 68 -> Reloc.PPC64_DTPMOD64
  | 73 -> Reloc.PPC64_TPREL64
  | 78 -> Reloc.PPC64_DTPREL64
  | n -> invalid_arg (sprintf "bad ppc64 r_type %d" n)

let parse_r_type_sparc = function
  | 0 -> Reloc.SPARC_NONE
  | 1 -> Reloc.SPARC_8
  | 2 -> Reloc.SPARC_16
  | 3 -> Reloc.SPARC_32
  | 4 -> Reloc.SPARC_DISP8
  | 5 -> Reloc.SPARC_DISP16
  | 6 -> Reloc.SPARC_DISP32
  | 7 -> Reloc.SPARC_WDISP30
  | 8 -> Reloc.SPARC_WDISP22
  | 9 -> Reloc.SPARC_HI22
  | 10 -> Reloc.SPARC_22
  | 11 -> Reloc.SPARC_13
  | 12 -> Reloc.SPARC_LO10
  | 13 -> Reloc.SPARC_GOT10
  | 14 -> Reloc.SPARC_GOT13
  | 15 -> Reloc.SPARC_GOT22
  | 16 -> Reloc.SPARC_PC10
  | 17 -> Reloc.SPARC_PC22
  | 18 -> Reloc.SPARC_WPLT30
  | 19 -> Reloc.SPARC_COPY
  | 20 -> Reloc.SPARC_GLOB_DAT
  | 21 -> Reloc.SPARC_JMP_SLOT
  | 22 -> Reloc.SPARC_RELATIVE
  | 23 -> Reloc.SPARC_UA32
  | 24 -> Reloc.SPARC_PLT32
  | 25 -> Reloc.SPARC_HIPLT22
  | 26 -> Reloc.SPARC_LOPLT10
  | 27 -> Reloc.SPARC_PCPLT32
  | 28 -> Reloc.SPARC_PCPLT22
  | 29 -> Reloc.SPARC_PCPLT10
  | 30 -> Reloc.SPARC_10
  | 31 -> Reloc.SPARC_11
  | 32 -> Reloc.SPARC_64
  | 33 -> Reloc.SPARC_OLO10
  | 34 -> Reloc.SPARC_HH22
  | 35 -> Reloc.SPARC_HM10
  | 36 -> Reloc.SPARC_LM22
  | 37 -> Reloc.SPARC_PC_HH22
  | 38 -> Reloc.SPARC_PC_HM10
  | 39 -> Reloc.SPARC_PC_LM22
  | 40 -> Reloc.SPARC_WDISP16
  | 41 -> Reloc.SPARC_WDISP19
  | 42 -> Reloc.SPARC_GLOB_JMP
  | 43 -> Reloc.SPARC_7
  | 44 -> Reloc.SPARC_5
  | 45 -> Reloc.SPARC_6
  | 46 -> Reloc.SPARC_DISP64
  | 47 -> Reloc.SPARC_PLT64
  | 48 -> Reloc.SPARC_HIX22
  | 49 -> Reloc.SPARC_LOX10
  | 50 -> Reloc.SPARC_H44
  | 51 -> Reloc.SPARC_M44
  | 52 -> Reloc.SPARC_L44
  | 53 -> Reloc.SPARC_REGISTER
  | 54 -> Reloc.SPARC_UA64
  | 55 -> Reloc.SPARC_UA16
  | 56 -> Reloc.SPARC_TLS_GD_HI22
  | 57 -> Reloc.SPARC_TLS_GD_LO10
  | 58 -> Reloc.SPARC_TLS_GD_ADD
  | 59 -> Reloc.SPARC_TLS_GD_CALL
  | 60 -> Reloc.SPARC_TLS_LDM_HI22
  | 61 -> Reloc.SPARC_TLS_LDM_LO10
  | 62 -> Reloc.SPARC_TLS_LDM_ADD
  | 63 -> Reloc.SPARC_TLS_LDM_CALL
  | 64 -> Reloc.SPARC_TLS_LDO_HIX22
  | 65 -> Reloc.SPARC_TLS_LDO_LOX10
  | 66 -> Reloc.SPARC_TLS_LDO_ADD
  | 67 -> Reloc.SPARC_TLS_IE_HI22
  | 68 -> Reloc.SPARC_TLS_IE_LO10
  | 69 -> Reloc.SPARC_TLS_IE_LD
  | 70 -> Reloc.SPARC_TLS_IE_LDX
  | 71 -> Reloc.SPARC_TLS_IE_ADD
  | 72 -> Reloc.SPARC_TLS_LE_HIX22
  | 73 -> Reloc.SPARC_TLS_LE_LOX10
  | 74 -> Reloc.SPARC_TLS_DTPMOD32
  | 75 -> Reloc.SPARC_TLS_DTPMOD64
  | 76 -> Reloc.SPARC_TLS_DTPOFF32
  | 77 -> Reloc.SPARC_TLS_DTPOFF64
  | 78 -> Reloc.SPARC_TLS_TPOFF32
  | 79 -> Reloc.SPARC_TLS_TPOFF64
  | n -> invalid_arg (sprintf "bad sparc r_type %d" n)

let parse_r_type_x86_64 = function
  | 0 -> Reloc.X86_64_NONE
  | 1 -> Reloc.X86_64_64
  | 2 -> Reloc.X86_64_PC32
  | 3 -> Reloc.X86_64_GOT32
  | 4 -> Reloc.X86_64_PLT32
  | 5 -> Reloc.X86_64_COPY
  | 6 -> Reloc.X86_64_GLOB_DAT
  | 7 -> Reloc.X86_64_JMP_SLOT
  | 8 -> Reloc.X86_64_RELATIVE
  | 9 -> Reloc.X86_64_GOTPCREL
  | 10 -> Reloc.X86_64_32
  | 11 -> Reloc.X86_64_32S
  | 12 -> Reloc.X86_64_16
  | 13 -> Reloc.X86_64_PC16
  | 14 -> Reloc.X86_64_8
  | 15 -> Reloc.X86_64_PC8
  | 16 -> Reloc.X86_64_DTPMOD64
  | 17 -> Reloc.X86_64_DTPOFF64
  | 18 -> Reloc.X86_64_TPOFF64
  | 19 -> Reloc.X86_64_TLSGD
  | 20 -> Reloc.X86_64_TLSLD
  | 21 -> Reloc.X86_64_DTPOFF32
  | 22 -> Reloc.X86_64_GOTTPOFF
  | 23 -> Reloc.X86_64_TPOFF32
  | 37 -> Reloc.X86_64_IRELATIVE
  | n -> invalid_arg (sprintf "bad x86-64 r_type %d" n)

let parse_r_type_armv8 = function
  | 0 -> Reloc.ARMV8_NONE
  | 257 -> Reloc.ARMV8_ABS64
  | 258 -> Reloc.ARMV8_ABS32
  | 259 -> Reloc.ARMV8_ABS16
  | 260 -> Reloc.ARMV8_PREL64
  | 261 -> Reloc.ARMV8_PREL32
  | 262 -> Reloc.ARMV8_PREL16
  | 307 -> Reloc.ARMV8_GOTREL64
  | 308 -> Reloc.ARMV8_GOTREL32
  | 1024 -> Reloc.ARMV8_COPY
  | 1025 -> Reloc.ARMV8_GLOB_DAT
  | 1026 -> Reloc.ARMV8_JMP_SLOT
  | n -> invalid_arg (sprintf "bad armv8 r_type %d" n)

let parse_r_type_riscv = function
  | 0 -> Reloc.RISCV_NONE
  | 1 -> Reloc.RISCV_32
  | 2 -> Reloc.RISCV_64
  | 3 -> Reloc.RISCV_RELATIVE
  | 4 -> Reloc.RISCV_COPY
  | 5 -> Reloc.RISCV_JMP_SLOT
  | 6 -> Reloc.RISCV_TLS_DTPMOD32
  | 7 -> Reloc.RISCV_TLS_DTPMOD64
  | 8 -> Reloc.RISCV_TLS_DTPREL32
  | 9 -> Reloc.RISCV_TLS_DTPREL64
  | 10 -> Reloc.RISCV_TLS_TPREL32
  | 11 -> Reloc.RISCV_TLS_TPREL64
  | 16 -> Reloc.RISCV_BRANCH
  | 17 -> Reloc.RISCV_JAL
  | 18 -> Reloc.RISCV_CALL
  | 19 -> Reloc.RISCV_CALL_PLT
  | 20 -> Reloc.RISCV_GOT_HI20
  | 21 -> Reloc.RISCV_TLS_GOT_HI20
  | 22 -> Reloc.RISCV_TLS_GD_HI20
  | 23 -> Reloc.RISCV_PCREL_HI20
  | 24 -> Reloc.RISCV_PCREL_LO12_I
  | 25 -> Reloc.RISCV_PCREL_LO12_S
  | 26 -> Reloc.RISCV_HI20
  | 27 -> Reloc.RISCV_LO12_I
  | 28 -> Reloc.RISCV_LO12_S
  | 29 -> Reloc.RISCV_TPREL_HI20
  | 30 -> Reloc.RISCV_TPREL_LO12_I
  | 31 -> Reloc.RISCV_TPREL_LO12_S
  | 32 -> Reloc.RISCV_TPREL_ADD
  | 33 -> Reloc.RISCV_ADD8
  | 34 -> Reloc.RISCV_ADD16
  | 35 -> Reloc.RISCV_ADD32
  | 36 -> Reloc.RISCV_ADD64
  | 37 -> Reloc.RISCV_SUB8
  | 38 -> Reloc.RISCV_SUB16
  | 39 -> Reloc.RISCV_SUB32
  | 40 -> Reloc.RISCV_SUB64
  | 41 -> Reloc.RISCV_GNU_VTINHERIT
  | 42 -> Reloc.RISCV_GNU_VTENTRY
  | 43 -> Reloc.RISCV_ALIGN
  | 44 -> Reloc.RISCV_RVC_BRANCH
  | 45 -> Reloc.RISCV_RVC_JUMP
  | 46 -> Reloc.RISCV_RVC_LUI
  | 47 -> Reloc.RISCV_GPREL_I
  | 48 -> Reloc.RISCV_GPREL_S
  | 49 -> Reloc.RISCV_TPREL_I
  | 50 -> Reloc.RISCV_TPREL_S
  | 51 -> Reloc.RISCV_RELAX
  | 52 -> Reloc.RISCV_SUB6
  | 53 -> Reloc.RISCV_SET6
  | 54 -> Reloc.RISCV_SET8
  | 55 -> Reloc.RISCV_SET16
  | 56 -> Reloc.RISCV_SET32
  | 57 -> Reloc.RISCV_32_PCREL
  | 58 -> Reloc.RISCV_IRELATIVE
  | n -> invalid_arg (sprintf "bad risc-v reloc type %d" n)

let parse_r_type r_type = function
  | Machine.I386 -> Some (Reloc.I386 (parse_r_type_i386 r_type))
  | Machine.ARM -> Some (Reloc.Arm (parse_r_type_arm r_type))
  | Machine.IA_64 -> Some (Reloc.IA64 (parse_r_type_ia_64 r_type))
  | Machine.MIPS -> Some (Reloc.Mips (parse_r_type_mips r_type))
  | Machine.PPC -> Some (Reloc.Ppc (parse_r_type_ppc r_type))
  | Machine.PPC64 -> Some (Reloc.Ppc64 (parse_r_type_ppc64 r_type))
  | Machine.SPARC -> Some (Reloc.Sparc (parse_r_type_sparc r_type))
  | Machine.X86_64 -> Some (Reloc.X86_64 (parse_r_type_x86_64 r_type))
  | Machine.RISCV -> Some (Reloc.Riscv (parse_r_type_riscv r_type))
  | _ -> None

let dt_loos = 0x6000600DL

let dt_hios = 0x6FFFF000L

let dt_loproc = 0x70000000L

let dt_hiproc = 0x7FFFFFFFL

let parse_dyn_tag = function
  | 0L -> Some Dyn.T_NULL
  | 1L -> Some Dyn.T_NEEDED
  | 2L -> Some Dyn.T_PLTRELSZ
  | 3L -> Some Dyn.T_PLTGOT
  | 4L -> Some Dyn.T_HASH
  | 5L -> Some Dyn.T_STRTAB
  | 6L -> Some Dyn.T_SYMTAB
  | 7L -> Some Dyn.T_RELA
  | 8L -> Some Dyn.T_RELASZ
  | 9L -> Some Dyn.T_RELAENT
  | 10L -> Some Dyn.T_STRSZ
  | 11L -> Some Dyn.T_SYMENT
  | 12L -> Some Dyn.T_INIT
  | 13L -> Some Dyn.T_FINI
  | 14L -> Some Dyn.T_SONAME
  | 15L -> Some Dyn.T_RPATH
  | 16L -> Some Dyn.T_SYMBOLIC
  | 17L -> Some Dyn.T_REL
  | 18L -> Some Dyn.T_RELSZ
  | 19L -> Some Dyn.T_RELENT
  | 20L -> Some Dyn.T_PLTREL
  | 21L -> Some Dyn.T_DEBUG
  | 22L -> Some Dyn.T_TEXTREL
  | 23L -> Some Dyn.T_JMPREL
  | 24L -> Some Dyn.T_BIND_NOW
  | 25L -> Some Dyn.T_INIT_ARRAY
  | 26L -> Some Dyn.T_FINI_ARRAY
  | 27L -> Some Dyn.T_INIT_ARRAYSZ
  | 28L -> Some Dyn.T_FINI_ARRAYSZ
  | 29L -> Some Dyn.T_RUNPATH
  | 30L -> Some Dyn.T_FLAGS
  | 32L -> Some Dyn.T_PREINIT_ARRAY
  | 33L -> Some Dyn.T_PREINIT_ARRAYSZ
  | n when Int64.(n >= dt_loos && n <= dt_hios) -> Some (Dyn.T_OS n)
  | n when Int64.(n >= dt_hiproc && n <= dt_loproc) -> Some (Dyn.T_PROC n)
  | 0x6FFFFEF5L -> Some Dyn.T_GNU_HASH
  | 0x6FFFFEF6L -> Some Dyn.T_TLSDESC_PLT
  | 0x6FFFFEF7L -> Some Dyn.T_TLSDESC_GOT
  | 0x6FFFFEF8L -> Some Dyn.T_GNU_CONFLICT
  | 0x6FFFFEF9L -> Some Dyn.T_GNU_LIBLIST
  | 0x6FFFFEFAL -> Some Dyn.T_CONFIG
  | 0x6FFFFEFBL -> Some Dyn.T_DEPAUDIT
  | 0x6FFFFEFCL -> Some Dyn.T_AUDIT
  | 0x6FFFFEFDL -> Some Dyn.T_PLTPAD
  | 0x6FFFFEFEL -> Some Dyn.T_MOVETAB
  | 0x6FFFFEFFL -> Some Dyn.T_SYMINFO
  | 0x6FFFFFF0L -> Some Dyn.T_VERSYM
  | 0x6FFFFFF9L -> Some Dyn.T_RELACOUNT
  | 0x6FFFFFFAL -> Some Dyn.T_RELCOUNT
  | 0x6FFFFFFBL -> Some Dyn.T_FLAGS_1
  | 0x6FFFFFFCL -> Some Dyn.T_VERDEF
  | 0x6FFFFFFDL -> Some Dyn.T_VERDEFNUM
  | 0x6FFFFFFEL -> Some Dyn.T_VERNEED
  | 0x6FFFFFFFL -> Some Dyn.T_VERNEEDNUM
  | _ -> None

let endian_of = function
  | `LE -> LittleEndian
  | `BE -> BigEndian

let parse_flags f n v =
  let rec aux res b v =
    if b = n then List.rev res
    else
      let res = if Int64.(bit_and v 1L = 1L) then f b :: res else res in
      aux res (b + 1) Int64.(v lsr 1)
  in
  aux [] 0 v

let parse_elf_ident bits =
  match%bitstring bits with
  | {|
     0x7f: 8;
     "ELF": 24: string;
     ei_class: 8;
     ei_data: 8;
     ei_version: 8;
     ei_osabi: 8;
     ei_abiversion: 8;
     pad: 56;
     rest: -1: bitstring
     |}
    ->
      ( parse_e_class ei_class
      , parse_e_data ei_data
      , ei_version
      , parse_e_osabi ei_osabi
      , ei_abiversion
      , rest )
  | {| _ |} -> invalid_arg "bad elf ident"

type table = {offset: int64; entry_size: int; num_entries: int}

let parse_elf_ehdr bits =
  let e_class, e_data, e_version, e_osabi, e_abiversion, rest =
    parse_elf_ident bits
  in
  let endian = endian_of e_data in
  match e_class with
  | `ELF32 -> (
      match%bitstring rest with
      | {|
        e_type: 16: endian (endian);
        e_machine: 16: endian (endian);
        e_version: 32: endian (endian);
        e_entry: 32: endian (endian);
        e_phoff: 32: endian (endian);
        e_shoff: 32: endian (endian);
        e_flags: 32: endian (endian);
        e_ehsize: 16: endian (endian);
        e_phentsize: 16: endian (endian);
        e_phnum: 16: endian (endian);
        e_shentsize: 16: endian (endian);
        e_shnum: 16: endian (endian);
        e_shstrndx: 16: endian (endian)
        |}
        ->
          let elf =
            { eclass= e_class
            ; endian= e_data
            ; etype= parse_e_type e_type
            ; machine= parse_e_machine e_machine
            ; version= e_version
            ; abiversion= e_abiversion
            ; osabi= e_osabi
            ; entry= Int64.of_int32 e_entry
            ; flags= e_flags
            ; ehsize= e_ehsize
            ; phentsize= e_phentsize
            ; shentsize= e_shentsize
            ; segments= [||]
            ; sections= [||]
            ; relocs= [||]
            ; symbols= [||]
            ; dynamic= [||]
            ; eh_frame= None
            ; plt= Int64.Map.empty }
          in
          let seg_table =
            { offset= Int64.of_int32 e_phoff
            ; entry_size= e_phentsize
            ; num_entries= e_phnum }
          in
          let sec_table =
            { offset= Int64.of_int32 e_shoff
            ; entry_size= e_shentsize
            ; num_entries= e_shnum }
          in
          (elf, e_shstrndx, seg_table, sec_table)
      | {| _ |} -> invalid_arg "bad elf header" )
  | `ELF64 -> (
      match%bitstring rest with
      | {|
        e_type: 16: endian (endian);
        e_machine: 16: endian (endian);
        e_version: 32: endian (endian);
        e_entry: 64: endian (endian);
        e_phoff: 64: endian (endian);
        e_shoff: 64: endian (endian);
        e_flags: 32: endian (endian);
        e_ehsize: 16: endian (endian);
        e_phentsize: 16: endian (endian);
        e_phnum: 16: endian (endian);
        e_shentsize: 16: endian (endian);
        e_shnum: 16: endian (endian);
        e_shstrndx: 16: endian (endian)
        |}
        ->
          let elf =
            { eclass= e_class
            ; endian= e_data
            ; etype= parse_e_type e_type
            ; machine= parse_e_machine e_machine
            ; version= e_version
            ; abiversion= e_abiversion
            ; osabi= e_osabi
            ; entry= e_entry
            ; flags= e_flags
            ; ehsize= e_ehsize
            ; phentsize= e_phentsize
            ; shentsize= e_shentsize
            ; segments= [||]
            ; sections= [||]
            ; relocs= [||]
            ; symbols= [||]
            ; dynamic= [||]
            ; eh_frame= None
            ; plt= Int64.Map.empty }
          in
          let seg_table =
            {offset= e_phoff; entry_size= e_phentsize; num_entries= e_phnum}
          in
          let sec_table =
            {offset= e_shoff; entry_size= e_shentsize; num_entries= e_shnum}
          in
          (elf, e_shstrndx, seg_table, sec_table)
      | {| _ |} -> invalid_arg "bad elf header" )

let parse_elf_phdr e_class endian bits =
  match e_class with
  | `ELF32 -> (
      match%bitstring bits with
      | {|
        p_type: 32: endian (endian);
        p_offset: 32: endian (endian);
        p_vaddr: 32: endian (endian);
        p_paddr: 32: endian (endian);
        p_filesz: 32: endian (endian);
        p_memsz: 32: endian (endian);
        p_flags: 32: endian (endian);
        p_align: 32: endian (endian)
        |}
        ->
          Segment.
            { typ= parse_p_type p_type
            ; flags= parse_flags parse_p_flag 32 (Int64.of_int32 p_flags)
            ; offset= Int64.of_int32 p_offset
            ; vaddr= Int64.of_int32 p_vaddr
            ; paddr= Int64.of_int32 p_paddr
            ; filesz= Int64.of_int32 p_filesz
            ; memsz= Int64.of_int32 p_memsz
            ; align= Int64.of_int32 p_align }
      | {| _ |} -> invalid_arg "bad program header" )
  | `ELF64 -> (
      match%bitstring bits with
      | {|
        p_type: 32: endian (endian);
        p_flags: 32: endian (endian);
        p_offset: 64: endian (endian);
        p_vaddr: 64: endian (endian);
        p_paddr: 64: endian (endian);
        p_filesz: 64: endian (endian);
        p_memsz: 64: endian (endian);
        p_align: 64: endian (endian)
        |}
        ->
          Segment.
            { typ= parse_p_type p_type
            ; flags= parse_flags parse_p_flag 32 (Int64.of_int32 p_flags)
            ; offset= p_offset
            ; vaddr= p_vaddr
            ; paddr= p_paddr
            ; filesz= p_filesz
            ; memsz= p_memsz
            ; align= p_align }
      | {| _ |} -> invalid_arg "bad program header" )

let parse_elf_shdr e_class endian bits =
  match e_class with
  | `ELF32 -> (
      match%bitstring bits with
      | {|
        sh_name: 32: endian (endian);
        sh_type: 32: endian (endian);
        sh_flags: 32: endian (endian);
        sh_addr: 32: endian (endian);
        sh_offset: 32: endian (endian);
        sh_size: 32: endian (endian);
        sh_link: 32: endian (endian);
        sh_info: 32: endian (endian);
        sh_addralign: 32: endian (endian);
        sh_entsize: 32: endian (endian)
        |}
        ->
          let shdr =
            Section.
              { name= ""
              ; kind= parse_sh_type sh_type
              ; flags= parse_flags parse_sh_flag 32 (Int64.of_int32 sh_flags)
              ; addr= Int64.of_int32 sh_addr
              ; offset= Int64.of_int32 sh_offset
              ; size= Int64.of_int32 sh_size
              ; link= sh_link
              ; info= sh_info
              ; addralign= Int64.of_int32 sh_addralign
              ; entsize= Int64.of_int32 sh_entsize }
          in
          (sh_name, shdr)
      | {| _ |} -> invalid_arg "bad section header" )
  | `ELF64 -> (
      match%bitstring bits with
      | {|
        sh_name: 32: endian (endian);
        sh_type: 32: endian (endian);
        sh_flags: 64: endian (endian);
        sh_addr: 64: endian (endian);
        sh_offset: 64: endian (endian);
        sh_size: 64: endian (endian);
        sh_link: 32: endian (endian);
        sh_info: 32: endian (endian);
        sh_addralign: 64: endian (endian);
        sh_entsize: 64: endian (endian)
        |}
        ->
          let shdr =
            Section.
              { name= ""
              ; kind= parse_sh_type sh_type
              ; flags= parse_flags parse_sh_flag 64 sh_flags
              ; addr= sh_addr
              ; offset= sh_offset
              ; size= sh_size
              ; link= sh_link
              ; info= sh_info
              ; addralign= sh_addralign
              ; entsize= sh_entsize }
          in
          (sh_name, shdr)
      | {| _ |} -> invalid_arg "bad section header" )

let bitstring_of_bytes b = (b, 0, Bytes.length b * 8)

let get_int32 endian data pos =
  let buf = Bigstring.to_bytes data ~pos ~len:4 in
  let bits = bitstring_of_bytes buf in
  match%bitstring bits with
  | {| v: 32: endian (endian) |} -> v

let get_int64 endian data pos =
  let buf = Bigstring.to_bytes data ~pos ~len:8 in
  let bits = bitstring_of_bytes buf in
  match%bitstring bits with
  | {| v: 64: endian (endian) |} -> v

let sym_addr e_class endian data txt (sec : Section.t option) offset =
  let addr_off =
    match e_class with
    | `ELF32 -> 4
    | `ELF64 -> 8
  in
  let addr =
    match e_class with
    | `ELF32 -> get_int32 endian data (addr_off + offset) |> Int64.of_int32
    | `ELF64 -> get_int64 endian data (addr_off + offset)
  in
  match sec with
  | None -> addr
  | Some sec ->
      if Int64.(sec.addr = zero) && Int64.(sec.offset > txt) then
        Int64.(sec.offset - txt + addr)
      else addr

let parse_elf_sym e_class endian bits sections data offset =
  let txt =
    match
      Array.find sections ~f:(fun (sec : Section.t) ->
          String.equal sec.name ".text" )
    with
    | None -> 0L
    | Some sec -> sec.offset
  in
  match e_class with
  | `ELF32 -> (
      match%bitstring bits with
      | {|
        st_name: 32: endian (endian);
        st_value: 32: endian (endian);
        st_size: 32: endian (endian);
        st_info: 8: endian (endian);
        st_other: 8: endian (endian);
        st_shndx: 16: endian (endian)
        |}
        ->
          let sec = Option.try_with (fun () -> sections.(st_shndx)) in
          let sym =
            Symbol.
              { name= ""
              ; value= Int64.of_int32 st_value
              ; size= Int64.of_int32 st_size
              ; bind= parse_st_bind (st_info lsr 4)
              ; typ= parse_st_type (st_info land 0xf)
              ; visibility= parse_st_visibility (st_other land 0x3)
              ; shndx= st_shndx
              ; addr= sym_addr e_class endian data txt sec offset }
          in
          (st_name, sym)
      | {| _ |} -> invalid_arg "bad symbol" )
  | `ELF64 -> (
      match%bitstring bits with
      | {|
        st_name: 32: endian (endian);
        st_info: 8: endian (endian);
        st_other: 8: endian (endian);
        st_shndx: 16: endian (endian);
        st_value: 64: endian (endian);
        st_size: 64: endian (endian)
        |}
        ->
          let sec = Option.try_with (fun () -> sections.(st_shndx)) in
          let sym =
            Symbol.
              { name= ""
              ; value= st_value
              ; size= st_size
              ; bind= parse_st_bind (st_info lsr 4)
              ; typ= parse_st_type (st_info land 0xf)
              ; visibility= parse_st_visibility (st_other land 0x3)
              ; shndx= st_shndx
              ; addr= sym_addr e_class endian data txt sec offset }
          in
          (st_name, sym)
      | {| _ |} -> invalid_arg "bad symbol" )

let parse_elf_rel e_class e_machine endian bits =
  match e_class with
  | `ELF32 -> (
      match%bitstring bits with
      | {|
        r_offset: 32: endian (endian);
        r_info: 32: endian (endian)
        |}
        ->
          let stndx = Int.of_int32_exn Int32.(r_info lsr 8) in
          let r_type = Int.of_int32_exn Int32.(r_info land 0xFFl) in
          Reloc.
            { offset= Int64.of_int32 r_offset
            ; stndx
            ; typ= parse_r_type r_type e_machine
            ; addend= None
            ; symbol= None }
      | {| _ |} -> invalid_arg "bad rel" )
  | `ELF64 -> (
      match%bitstring bits with
      | {|
        r_offset: 64: endian (endian);
        r_info: 64: endian (endian)
        |}
        ->
          let stndx = Int.of_int64_exn Int64.(r_info lsr 32) in
          let r_type = Int.of_int64_exn Int64.(r_info land 0xFFFFFFFFL) in
          Reloc.
            { offset= r_offset
            ; stndx
            ; typ= parse_r_type r_type e_machine
            ; addend= None
            ; symbol= None }
      | {| _ |} -> invalid_arg "bad rel" )

let parse_elf_rela e_class e_machine endian bits =
  match e_class with
  | `ELF32 -> (
      match%bitstring bits with
      | {|
        r_offset: 32: endian (endian);
        r_info: 32: endian (endian);
        r_addend: 32: endian (endian)
        |}
        ->
          let stndx = Int.of_int32_exn Int32.(r_info lsr 8) in
          let r_type = Int.of_int32_exn Int32.(r_info land 0xFFl) in
          Reloc.
            { offset= Int64.of_int32 r_offset
            ; stndx
            ; typ= parse_r_type r_type e_machine
            ; addend= Some (Int64.of_int32 r_addend)
            ; symbol= None }
      | {| _ |} -> invalid_arg "bad rela" )
  | `ELF64 -> (
      match%bitstring bits with
      | {|
        r_offset: 64: endian (endian);
        r_info: 64: endian (endian);
        r_addend: 64: endian (endian)
        |}
        ->
          let stndx = Int.of_int64_exn Int64.(r_info lsr 32) in
          let r_type = Int.of_int64_exn Int64.(r_info land 0xFFFFFFFFL) in
          Reloc.
            { offset= r_offset
            ; stndx
            ; typ= parse_r_type r_type e_machine
            ; addend= Some r_addend
            ; symbol= None }
      | {| _ |} -> invalid_arg "bad rela" )

let parse_dynamic eclass endian segments data =
  let open Option.Let_syntax in
  let%bind pt_dynamic =
    Array.find segments ~f:(fun seg -> Segment.(equal_typ seg.typ T_DYNAMIC))
  in
  let len = Int64.to_int_exn pt_dynamic.filesz in
  let%bind _ = Option.some_if (len > 0) () in
  let pos = Int64.to_int_exn pt_dynamic.offset in
  let read_dyn pos =
    match eclass with
    | `ELF32 ->
        let tag =
          match endian with
          | `LE -> Bigstring.get_int32_t_le data ~pos
          | `BE -> Bigstring.get_int32_t_be data ~pos
        in
        let pos = pos + 4 in
        let value =
          match endian with
          | `LE -> Bigstring.get_int32_t_le data ~pos
          | `BE -> Bigstring.get_int32_t_be data ~pos
        in
        let tag = Int64.(of_int32 tag land 0xFFFFFFFFL) in
        let value = Int64.(of_int32 value land 0xFFFFFFFFL) in
        (tag, value, 8)
    | `ELF64 ->
        let tag =
          match endian with
          | `LE -> Bigstring.get_int64_t_le data ~pos
          | `BE -> Bigstring.get_int64_t_be data ~pos
        in
        let pos = pos + 8 in
        let value =
          match endian with
          | `LE -> Bigstring.get_int64_t_le data ~pos
          | `BE -> Bigstring.get_int64_t_be data ~pos
        in
        (tag, value, 16)
  in
  let rec aux i res =
    if i >= len then res
    else
      let tag, value, inc = read_dyn (pos + i) in
      let tag = parse_dyn_tag tag in
      let dyn = Dyn.{tag; value} in
      let res = dyn :: res in
      match tag with
      | Some Dyn.T_NULL -> res
      | _ -> aux (i + inc) res
  in
  aux 0 [] |> List.rev |> Array.of_list |> return

let parse_dwarf_pointer elf r start enc base addr =
  let is64 =
    match elf.eclass with
    | `ELF64 -> true
    | _ -> false
  in
  let base =
    match enc land 0x70 with
    | 0x10 ->
        let off = Binreader.position r - start in
        Int64.(addr + of_int off)
    | _ -> base
  in
  let open Option.Let_syntax in
  let%bind ptr, is_signed, sz =
    let%bind _ = Option.some_if (enc land lnot 0xFF = 0) () in
    let kind = enc land 0x0F in
    match kind with
    | 0x01 ->
        let%map ptr = Binreader.read_uleb128 r in
        (ptr, false, 8)
    | 0x09 ->
        let%map ptr = Binreader.read_sleb128 r in
        (ptr, true, 8)
    | (0x00 | 0x08) when is64 ->
        let%map ptr = Binreader.read64 r elf.endian in
        (ptr, kind = 0x08, 8)
    | 0x00 | 0x08 ->
        let%map ptr = Binreader.read32 r elf.endian in
        (Int64.(of_int32 ptr land 0xFFFFFFFFL), kind = 0x08, 4)
    | 0x04 | 0x0C ->
        let%map ptr = Binreader.read64 r elf.endian in
        (ptr, kind = 0x0C, 8)
    | 0x03 | 0x0B ->
        let%map ptr = Binreader.read32 r elf.endian in
        (Int64.(of_int32 ptr land 0xFFFFFFFFL), kind = 0x0B, 4)
    | 0x02 | 0x0A ->
        let%map ptr = Binreader.read16 r elf.endian in
        (Int64.(of_int ptr land 0xFFFFL), kind = 0x0A, 2)
    | _ -> None
  in
  let%map ptr =
    match ptr with
    | 0L -> return ptr
    | ptr when enc land 0x70 = 0 || enc land 0x70 = 0x10 ->
        let ptr =
          if is_signed && sz <> 8 then
            let mask = Int64.(1L lsl Int.((sz lsl 3) - 1)) in
            Int64.((ptr lxor mask) - mask)
          else ptr
        in
        Some Int64.(ptr + base)
    | _ -> None
  in
  Dwarf_pointer.{address= ptr; indirect= enc land 0x80 <> 0}

let read_dwarf_pointer elf r (ptr : Dwarf_pointer.t) =
  let open Option.Let_syntax in
  if ptr.indirect then
    let%bind off = file_offset_of_vaddr_segment elf ptr.address in
    let pos = Int64.to_int_exn off in
    let pos' = Binreader.position r in
    let result =
      let%bind _ = Binreader.seek r pos in
      match elf.eclass with
      | `ELF32 ->
          let%map ptr = Binreader.read32 r elf.endian in
          Int64.(of_int32 ptr land 0xFFFFFFFFL)
      | `ELF64 -> Binreader.read64 r elf.endian
    in
    let%bind _ = Binreader.seek r pos' in
    result
  else return ptr.address

let parse_eh_frame elf data =
  let open Option.Let_syntax in
  let is64 =
    match elf.eclass with
    | `ELF64 -> true
    | _ -> false
  in
  let%bind sec = section_of_name elf ".eh_frame" in
  let%bind _ = Option.some_if Int64.(sec.size > 0L) () in
  let r = Binreader.from_bigstring data in
  let%bind seg = Array.find elf.segments ~f:Segment.is_gnu_eh_frame in
  let start = Int64.to_int_exn seg.offset in
  let%bind _ = Binreader.seek r start in
  let%bind version = Binreader.read8 r in
  let%bind _ = Option.some_if (version = 1) () in
  let%bind frame_encoding = Binreader.read8 r in
  let%bind fde_count_encoding = Binreader.read8 r in
  let%bind lookup_table_encoding = Binreader.read8 r in
  let%bind eh_pointer =
    parse_dwarf_pointer elf r start frame_encoding 0L seg.vaddr
  in
  let%bind fde_count_pointer =
    parse_dwarf_pointer elf r start fde_count_encoding 0L seg.vaddr
  in
  let%bind eh_addr = read_dwarf_pointer elf r eh_pointer in
  let%bind fde_count = read_dwarf_pointer elf r fde_count_pointer in
  let%bind seg = segment_of_vaddr elf eh_addr in
  let start = Segment.file_offset_of_vaddr seg eh_addr |> Int64.to_int_exn in
  let%bind _ = Binreader.seek r start in
  let offset () = Int64.of_int (Binreader.position r - start) in
  let cies = ref Int64.Map.empty in
  let cie_augs = Hashtbl.create (module Int64) in
  let fde_index = ref 0L in
  let rec loop () =
    if
      Int64.(offset () < seg.filesz)
      && Int64.(!fde_index < fde_count)
      && Int64.(offset () < sec.size)
    then
      let start_o = offset () in
      let%bind len = Binreader.read32 r elf.endian in
      let%bind len =
        match len with
        | 0xFFFFFFFFl ->
            let%map len = Binreader.read64 r elf.endian in
            len
        | _ -> return Int64.(of_int32 len land 0xFFFFFFFFL)
      in
      let after_len_o = offset () in
      let end_o = Int64.(after_len_o + len) in
      let%bind _ =
        match len with
        | 0L -> return ()
        | _ -> (
            let%bind id = Binreader.read32 r elf.endian in
            let id = Int64.(of_int32 id land 0xFFFFFFFFL) in
            match id with
            | 0L ->
                let%bind version = Binreader.read8 r in
                let%bind _ = Option.some_if (version = 1) () in
                let%bind augmentation_string =
                  Binreader.read_string r ~trunc:true ~size:(Some 8)
                in
                let%bind eh_data =
                  match String.is_substring augmentation_string "eh" with
                  | false -> return None
                  | true ->
                      if is64 then
                        let%map eh_data = Binreader.read64 r elf.endian in
                        return eh_data
                      else
                        let%map eh_data = Binreader.read32 r elf.endian in
                        return Int64.(of_int32_exn eh_data land 0xFFFFFFFFL)
                in
                let%bind code_alignment_factor = Binreader.read_uleb128 r in
                let%bind data_alignment_factor = Binreader.read_uleb128 r in
                let%bind return_address_register = Binreader.read8 r in
                let%bind _ =
                  if
                    (not (String.is_empty augmentation_string))
                    && Char.equal augmentation_string.[0] 'z'
                  then
                    let%bind length = Binreader.read_uleb128 r in
                    let lsda_pointer_encoding = ref None in
                    let fde_pointer_encoding = ref None in
                    let personality = ref None in
                    let i' = String.length augmentation_string in
                    let rec walk i =
                      if i >= i' then return ()
                      else
                        match augmentation_string.[i] with
                        | 'e' when i + 1 = i' -> walk (i + 1)
                        | 'L' when Option.is_none !lsda_pointer_encoding ->
                            let%bind enc = Binreader.read8 r in
                            lsda_pointer_encoding := Some enc;
                            walk (i + 1)
                        | 'P' when Option.is_none !personality ->
                            let%bind encoding = Binreader.read8 r in
                            let%bind pointer =
                              parse_dwarf_pointer elf r start encoding 0L
                                eh_addr
                            in
                            let%bind personality_ptr =
                              read_dwarf_pointer elf r pointer
                            in
                            personality :=
                              Some
                                Cie_augmentation.
                                  {encoding; pointer; personality_ptr};
                            walk (i + 1)
                        | 'R' when Option.is_none !fde_pointer_encoding ->
                            let%bind enc = Binreader.read8 r in
                            fde_pointer_encoding := Some enc;
                            walk (i + 1)
                        | _ -> None
                    in
                    let%map _ = walk 1 in
                    let length = Some length in
                    let lsda_pointer_encoding = !lsda_pointer_encoding in
                    let fde_pointer_encoding = !fde_pointer_encoding in
                    let personality = !personality in
                    Hashtbl.set cie_augs start_o
                      Cie_augmentation.
                        { length
                        ; lsda_pointer_encoding
                        ; personality
                        ; fde_pointer_encoding }
                  else
                    return
                      (Hashtbl.set cie_augs start_o
                         Cie_augmentation.
                           { length= None
                           ; lsda_pointer_encoding= None
                           ; personality= None
                           ; fde_pointer_encoding= None } )
                in
                return
                  (cies :=
                     Map.set !cies start_o
                       Cie.
                         { offset= start_o
                         ; version
                         ; augmentation_string
                         ; eh_data
                         ; code_alignment_factor
                         ; data_alignment_factor
                         ; return_address_register
                         ; fdes= Int64.Map.empty } )
            | _ ->
                let idx = !fde_index in
                fde_index := Int64.succ idx;
                let cie_o = Int64.(after_len_o - id) in
                let%bind cie = Map.find !cies cie_o in
                let%bind augmentation = Hashtbl.find cie_augs cie_o in
                let%bind fde_pointer_encoding =
                  augmentation.fde_pointer_encoding
                in
                let%bind pc_begin_pointer =
                  parse_dwarf_pointer elf r start fde_pointer_encoding 0L
                    eh_addr
                in
                let%bind pc_begin =
                  read_dwarf_pointer elf r pc_begin_pointer
                in
                let%bind pc_range_pointer =
                  parse_dwarf_pointer elf r start fde_pointer_encoding 0L
                    eh_addr
                in
                let%bind pc_range =
                  read_dwarf_pointer elf r pc_range_pointer
                in
                let%bind augmentation_length =
                  match augmentation.length with
                  | None -> return None
                  | Some _ ->
                      let%map len = Binreader.read_uleb128 r in
                      return len
                in
                let%bind lsda_pointer, lsda =
                  match augmentation.lsda_pointer_encoding with
                  | None -> return (None, None)
                  | Some enc ->
                      let%bind lsda_pointer =
                        parse_dwarf_pointer elf r start enc 0L eh_addr
                      in
                      let%bind virtual_address =
                        read_dwarf_pointer elf r lsda_pointer
                      in
                      let pos' = Binreader.position r in
                      let%bind seg = segment_of_vaddr elf virtual_address in
                      let start =
                        Segment.file_offset_of_vaddr seg virtual_address
                        |> Int64.to_int_exn
                      in
                      let%bind _ = Binreader.seek r start in
                      let offset () =
                        Int64.of_int (Binreader.position r - start)
                      in
                      let%bind landing_pad_base_encoding =
                        Binreader.read8 r
                      in
                      let%bind landing_pad_base_pointer, landing_pad_base =
                        if landing_pad_base_encoding <> 0xFF then
                          let%bind ptr =
                            parse_dwarf_pointer elf r start
                              landing_pad_base_encoding 0L virtual_address
                          in
                          let%bind base = read_dwarf_pointer elf r ptr in
                          return (Some ptr, base)
                        else return (None, pc_begin)
                      in
                      let%bind type_table_encoding = Binreader.read8 r in
                      let%bind type_table_offset =
                        if type_table_encoding <> 0xFF then
                          let%map off = Binreader.read_uleb128 r in
                          return off
                        else return None
                      in
                      let%bind call_site_table_encoding =
                        Binreader.read8 r
                      in
                      let%bind call_site_table_length =
                        Binreader.read_uleb128 r
                      in
                      let call_site_table_end =
                        Int64.(call_site_table_length + offset ())
                      in
                      let call_site_table = Vector.create () in
                      let rec loop () =
                        if Int64.(offset () >= call_site_table_end) then
                          return ()
                        else
                          let%bind instruction_start_pointer =
                            parse_dwarf_pointer elf r start
                              call_site_table_encoding 0L virtual_address
                          in
                          let%bind instruction_start =
                            read_dwarf_pointer elf r
                              instruction_start_pointer
                          in
                          let%bind instruction_end_pointer =
                            parse_dwarf_pointer elf r start
                              call_site_table_encoding 0L virtual_address
                          in
                          let%bind instruction_end =
                            read_dwarf_pointer elf r instruction_end_pointer
                          in
                          let%bind landing_pad_pointer =
                            parse_dwarf_pointer elf r start
                              call_site_table_encoding landing_pad_base
                              virtual_address
                          in
                          let%bind landing_pad =
                            read_dwarf_pointer elf r landing_pad_pointer
                          in
                          let%bind action = Binreader.read_uleb128 r in
                          let call_site =
                            Lsda_call_site.
                              { instruction_start_pointer
                              ; instruction_start
                              ; instruction_end_pointer
                              ; instruction_end
                              ; landing_pad_pointer
                              ; landing_pad
                              ; action }
                          in
                          Vector.push_back call_site_table call_site;
                          loop ()
                      in
                      let%bind _ = loop () in
                      let%map _ = Binreader.seek r pos' in
                      let lsda =
                        Lsda.
                          { virtual_address
                          ; landing_pad_base_encoding
                          ; landing_pad_base_pointer
                          ; landing_pad_base
                          ; type_table_encoding
                          ; type_table_offset
                          ; call_site_table_encoding
                          ; call_site_table_length
                          ; call_site_table= Vector.to_array call_site_table
                          }
                      in
                      (Some lsda_pointer, Some lsda)
                in
                let fde =
                  Fde.
                    { augmentation
                    ; pc_begin_pointer
                    ; pc_begin
                    ; pc_range_pointer
                    ; pc_range
                    ; augmentation_length
                    ; lsda_pointer
                    ; lsda }
                in
                let fdes = Map.set cie.fdes idx fde in
                return (cies := Map.set !cies cie_o Cie.{cie with fdes}) )
      in
      let%bind _ = Binreader.seek r (Int64.to_int_exn end_o + start) in
      loop ()
    else return ()
  in
  let%map _ = loop () in
  let cies = !cies in
  Eh_frame.
    { version
    ; frame_encoding
    ; fde_count_encoding
    ; lookup_table_encoding
    ; eh_pointer
    ; fde_count_pointer
    ; cies }

let parse_plt elf data =
  let open Option.Let_syntax in
  let%bind sec = section_of_name elf ".plt" in
  let%bind start =
    match elf.machine with
    | Machine.I386 | Machine.X86_64 -> return Int64.(sec.addr + 0x10L)
    | Machine.ARM -> return Int64.(sec.addr + 0x14L)
    | Machine.AARCH64 -> return Int64.(sec.addr + 0x20L)
    | Machine.RISCV -> return Int64.(sec.addr + 0x20L)
    | _ -> None
  in
  let plt_size addr =
    match elf.machine with
    | Machine.I386 | Machine.X86_64 -> return 0x10L
    | Machine.ARM -> (
        let pos = Int64.(to_int_exn (addr - sec.addr + sec.offset)) in
        match Bigstring.get_int32_t_be data ~pos with
        | 0x7847C046l -> return 0x10L
        | _ -> return 0x0CL )
    | Machine.AARCH64 -> return 0x10L
    | Machine.RISCV -> return 0x10L
    | _ -> None
  in
  Array.fold elf.relocs ~init:(elf.plt, start)
    ~f:(fun (plt, addr) (reloc : Reloc.t) ->
      match reloc.typ with
      | Some Reloc.(I386 I386_JMP_SLOT)
       |Some Reloc.(X86_64 X86_64_JMP_SLOT)
       |Some Reloc.(Arm ARM_JMP_SLOT)
       |Some Reloc.(Armv8 ARMV8_JMP_SLOT)
       |Some Reloc.(Riscv RISCV_JMP_SLOT) -> (
        match plt_size addr with
        | None -> (plt, addr)
        | Some size -> (
          match reloc.symbol with
          | None -> (plt, addr)
          | Some sym ->
              let sym = Symbol.{sym with addr= reloc.offset} in
              let next = Int64.(addr + size) in
              let rec loop a plt =
                if Int64.(a >= next) then plt
                else loop (Int64.succ a) (Map.set plt a sym)
              in
              let plt = loop addr plt in
              (plt, next) ) )
      | _ -> (plt, addr) )
  |> fst |> return

let elf_ident_size = 16

let elf_max_header_size = 64

let is_elf buf =
  try
    ignore @@ parse_elf_ident (bitstring_of_bytes buf);
    true
  with _ -> false

let is_elf_file filename =
  try
    In_channel.with_file filename ~f:(fun file ->
        let buf = Bytes.create elf_ident_size in
        In_channel.really_input_exn file ~buf ~pos:0 ~len:elf_ident_size;
        is_elf buf )
  with _ -> false

let get_chunks tbl data =
  let pos = Int.of_int64_exn tbl.offset in
  let rec aux res n =
    if n >= tbl.num_entries then List.rev res
    else
      let pos = pos + (n * tbl.entry_size) in
      let buf = Bigstring.to_bytes data ~pos ~len:tbl.entry_size in
      aux (buf :: res) (n + 1)
  in
  aux [] 0

let from_bigstring_exn data =
  let elf, shstrndx, seg_tbl, sec_tbl =
    let hdr = Bigstring.to_bytes data ~len:elf_max_header_size in
    let bits = bitstring_of_bytes hdr in
    parse_elf_ehdr bits
  in
  let endian = endian_of elf.endian in
  let segments =
    List.map (get_chunks seg_tbl data) ~f:(fun buf ->
        let bits = bitstring_of_bytes buf in
        parse_elf_phdr elf.eclass endian bits )
    |> Array.of_list
  in
  let get_string pos =
    let open Option.Let_syntax in
    let%bind pos' = Bigstring.find ~pos '\x00' data in
    let len = pos' - pos in
    return (Bigstring.to_string ~pos ~len data)
  in
  let sections =
    let sh_names, sections =
      List.map (get_chunks sec_tbl data) ~f:(fun buf ->
          let bits = bitstring_of_bytes buf in
          parse_elf_shdr elf.eclass endian bits )
      |> List.unzip
    in
    let sections = Array.of_list sections in
    if shstrndx >= sec_tbl.num_entries then sections
    else
      let sh_names = Array.of_list sh_names in
      let pos = Int.of_int64_exn sections.(shstrndx).offset in
      Array.mapi sections ~f:(fun i sec ->
          let pos = pos + Int.of_int32_exn sh_names.(i) in
          match get_string pos with
          | Some name -> {sec with name}
          | None -> sec )
  in
  let sym_size, rel_size, rela_size =
    match elf.eclass with
    | `ELF32 -> (16, 8, 12)
    | `ELF64 -> (24, 16, 24)
  in
  let symtab = Hashtbl.create (module Int) in
  let make_symbols i (sec : Section.t) =
    let link = Int.of_int32_exn sec.link in
    match sec.kind with
    | (Section.T_SYMTAB | Section.T_DYNSYM) when link < sec_tbl.num_entries
      ->
        let st_names, symbols =
          let tbl =
            { offset= sec.offset
            ; entry_size= sym_size
            ; num_entries=
                Int.of_int64_exn Int64.(sec.size / of_int sym_size) }
          in
          List.mapi (get_chunks tbl data) ~f:(fun i buf ->
              let bits = bitstring_of_bytes buf in
              let offset = Int64.to_int_exn sec.offset + (i * sym_size) in
              parse_elf_sym elf.eclass endian bits sections data offset )
          |> List.unzip
        in
        let pos = Int.of_int64_exn sections.(link).offset in
        let syms =
          Option.(
            value ~default:(ref [])
              (Hashtbl.find symtab i >>| fun syms -> ref (List.rev syms)))
        in
        let st_names = Array.of_list st_names in
        let symbols =
          List.mapi symbols ~f:(fun i sym ->
              let pos = pos + Int.of_int32_exn st_names.(i) in
              let sym =
                Option.(
                  value ~default:sym
                    (get_string pos >>| fun name -> {sym with name}))
              in
              syms := sym :: !syms;
              sym )
        in
        if not (List.is_empty !syms) then
          Hashtbl.set symtab i (List.rev !syms);
        Some symbols
    | _ -> None
  in
  let symbols =
    Array.to_list sections
    |> List.filter_mapi ~f:make_symbols
    |> List.concat |> Array.of_list
  in
  let relocs =
    List.filter_mapi (Array.to_list sections) ~f:(fun i sec ->
        let link = Int.of_int32_exn sec.link in
        let open Option.Let_syntax in
        let%bind f, sz =
          match sec.kind with
          | Section.T_REL when link < sec_tbl.num_entries ->
              Some (parse_elf_rel, rel_size)
          | Section.T_RELA when link < sec_tbl.num_entries ->
              Some (parse_elf_rela, rela_size)
          | _ -> None
        in
        if not @@ Hashtbl.mem symtab link then
          ignore @@ make_symbols link sections.(link);
        let tbl =
          let num_entries = Int.of_int64_exn Int64.(sec.size / of_int sz) in
          {offset= sec.offset; entry_size= sz; num_entries}
        in
        let relocs =
          List.mapi (get_chunks tbl data) ~f:(fun i buf ->
              let bits = bitstring_of_bytes buf in
              let rel = f elf.eclass elf.machine endian bits in
              let rel_sym =
                let%bind syms = Hashtbl.find symtab link in
                List.nth syms rel.stndx
              in
              {rel with symbol= rel_sym} )
        in
        return relocs )
    |> List.concat |> Array.of_list
  in
  let dynamic =
    Option.value ~default:[||]
      (parse_dynamic elf.eclass elf.endian segments data)
  in
  let elf = {elf with segments; sections; symbols; relocs; dynamic} in
  let eh_frame = parse_eh_frame elf data in
  let plt = Option.value ~default:elf.plt (parse_plt elf data) in
  {elf with eh_frame; plt}

let from_bigstring data = Option.try_with (fun () -> from_bigstring_exn data)
