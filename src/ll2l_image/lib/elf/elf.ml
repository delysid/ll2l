open Core_kernel
open Ll2l_std

module Osabi = struct
  type t =
    | SYSV
    | HPUX
    | NETBSD
    | LINUX
    | HURD
    | OSABI_86OPEN
    | SOLARIS
    | AIX
    | IRIX
    | FREEBSD
    | TRU64
    | MODESTO
    | OPENBSD
    | OPENVMS
    | NSK
    | AROS
    | ARM
    | STANDALONE
    | EXT of int
end

module Type = struct
  type t = NONE | REL | EXEC | DYN | CORE | EXT of int
end

module Machine = struct
  type t =
    | NONE
    | M32
    | SPARC
    | I386
    | M68K
    | M88K
    | I486
    | I80860
    | MIPS
    | S370
    | MIPS_RS3_LE
    | PARISC
    | VPP500
    | SPARC32PLUS
    | I80960
    | PPC
    | PPC64
    | S390
    | V800
    | FR20
    | RH32
    | RCE
    | ARM
    | ALPHA_STD
    | SH
    | SPARCV9
    | TRICORE
    | ARC
    | H8_300
    | H8_300H
    | H8S
    | H8_500
    | IA_64
    | MIPS_X
    | COLDFIRE
    | M68HC12
    | MMA
    | PCP
    | NCPU
    | NDR1
    | STARCORE
    | ME16
    | ST100
    | TINYJ
    | X86_64
    | PDSP
    | FX66
    | ST9PLUS
    | ST7
    | M68HC16
    | M68HC11
    | M68HC08
    | M68HC05
    | SVX
    | ST19
    | VAX
    | CRIS
    | JAVELIN
    | FIREPATH
    | ZSP
    | MMIX
    | HUANY
    | PRISM
    | AVR
    | FR30
    | D10V
    | D30V
    | V850
    | M32R
    | MN10300
    | MN10200
    | PJ
    | OPENRISC
    | ARC_A5
    | XTENSA
    | VIDEOCORE
    | TM_GPP
    | NS32K
    | TPC
    | SNP1K
    | ST200
    | IP2K
    | MAX
    | CR
    | F2MC16
    | MSP430
    | BLACKFIN
    | SE_C33
    | SEP
    | ARCA
    | UNICORE
    | ALPHA
    | AARCH64
    | RISCV
    | EXT of int
end

module Section = struct
  type kind =
    | T_NULL
    | T_PROGBITS
    | T_SYMTAB
    | T_STRTAB
    | T_RELA
    | T_HASH
    | T_DYNAMIC
    | T_NOTE
    | T_NOBITS
    | T_REL
    | T_SHLIB
    | T_DYNSYM
    | T_INIT_ARRAY
    | T_FINI_ARRAY
    | T_PREINIT_ARRAY
    | T_GROUP
    | T_SYMTAB_SHNDX
    | T_EXT of int32
  [@@deriving equal]

  type flag = F_WRITE | F_ALLOC | F_EXECINSTR | F_EXT of int
  [@@deriving equal]

  type t =
    { name: string
    ; kind: kind
    ; flags: flag list
    ; addr: int64
    ; offset: int64
    ; size: int64
    ; link: int32
    ; info: int32
    ; addralign: int64
    ; entsize: int64 }

  let has_flag sec flg = List.mem sec.flags flg equal_flag [@@inline]

  let is_vaddr_within sec vaddr =
    Int64.(vaddr >= sec.addr && vaddr < sec.addr + sec.size)
    [@@inline]

  let is_file_offset_within sec offset =
    Int64.(offset >= sec.offset && offset < sec.offset + sec.size)
    [@@inline]

  let file_offset_of_vaddr sec vaddr =
    Int64.(sec.offset + (vaddr - sec.addr))
    [@@inline]
end

module Segment = struct
  type typ =
    | T_NULL
    | T_LOAD
    | T_DYNAMIC
    | T_INTERP
    | T_NOTE
    | T_SHLIB
    | T_PHDR
    | T_TLS
    | T_EXT of int32
  [@@deriving equal]

  type flag = F_X | F_W | F_R | F_EXT of int [@@deriving equal]

  type t =
    { typ: typ
    ; flags: flag list
    ; offset: int64
    ; vaddr: int64
    ; paddr: int64
    ; filesz: int64
    ; memsz: int64
    ; align: int64 }

  let has_flag seg flg = List.mem seg.flags flg equal_flag [@@inline]

  let is_vaddr_within seg vaddr =
    Int64.(vaddr >= seg.vaddr && vaddr < seg.vaddr + seg.memsz)
    [@@inline]

  let is_file_offset_within seg offset =
    Int64.(offset >= seg.offset && offset < seg.offset + seg.filesz)
    [@@inline]

  let file_offset_of_vaddr seg vaddr =
    Int64.(seg.offset + (vaddr - seg.vaddr))
    [@@inline]

  let is_gnu_eh_frame seg =
    match seg.typ with
    | T_EXT typ -> Int32.equal typ 0x6474E550l
    | _ -> false
    [@@inline]
end

module Symbol = struct
  type bind = B_LOCAL | B_GLOBAL | B_WEAK | B_EXT of int

  type typ =
    | T_NOTYPE
    | T_OBJECT
    | T_FUNC
    | T_SECTION
    | T_FILE
    | T_COMMON
    | T_TLS
    | T_NUM
    | T_EXT of int

  type visibility =
    | V_DEFAULT
    | V_INTERNAL
    | V_HIDDEN
    | V_PROTECTED
    | V_EXPORTED
    | V_SINGLETON
    | V_ELIMINATE

  type t =
    { name: string
    ; value: int64
    ; size: int64
    ; bind: bind
    ; typ: typ
    ; visibility: visibility
    ; shndx: int
    ; addr: int64 }
end

module Reloc = struct
  type i386 =
    | I386_NONE
    | I386_32
    | I386_PC32
    | I386_GOT32
    | I386_PLT32
    | I386_COPY
    | I386_GLOB_DAT
    | I386_JMP_SLOT
    | I386_RELATIVE
    | I386_GOTOFF
    | I386_GOTPC
    | I386_TLS_TPOFF
    | I386_TLS_IE
    | I386_TLS_GOTIE
    | I386_TLS_LE
    | I386_TLS_GD
    | I386_TLS_LDM
    | I386_TLS_GD_32
    | I386_TLS_GD_PUSH
    | I386_TLS_GD_CALL
    | I386_TLS_GD_POP
    | I386_TLS_LDM_32
    | I386_TLS_LDM_PUSH
    | I386_TLS_LDM_CALL
    | I386_TLS_LDM_POP
    | I386_TLS_LDO_32
    | I386_TLS_IE_32
    | I386_TLS_LE_32
    | I386_TLS_TRPMOD32
    | I386_TLS_DTMOFF32
    | I386_TLS_TPOFF32
    | I386_IRELATIVE

  type arm =
    | ARM_NONE
    | ARM_PC24
    | ARM_ABS32
    | ARM_REL32
    | ARM_PC13
    | ARM_ABS16
    | ARM_ABS12
    | ARM_THM_ABS5
    | ARM_ABS8
    | ARM_SBREL32
    | ARM_THM_PC22
    | ARM_THM_PC8
    | ARM_AMP_VCALL9
    | ARM_SWI24
    | ARM_THM_SWI8
    | ARM_XPC25
    | ARM_THM_XPC22
    | ARM_TLS_DTPMOD32
    | ARM_TLS_DTPOFF32
    | ARM_TLS_TPOFF32
    | ARM_COPY
    | ARM_GLOB_DAT
    | ARM_JMP_SLOT
    | ARM_RELATIVE
    | ARM_GOTOFF
    | ARM_GOTPC
    | ARM_GOT32
    | ARM_PLT32
    | ARM_GNU_VTENTRY
    | ARM_GNU_VTINHERIT
    | ARM_RSBREL32
    | ARM_THM_RPC22
    | ARM_RREL32
    | ARM_RABS32
    | ARM_RPC24
    | ARM_RBASE
    | ARM_IRELATIVE

  type ia64 =
    | IA_64_NONE
    | IA_64_IMM14
    | IA_64_IMM22
    | IA_64_IMM64
    | IA_64_DIR32MSB
    | IA_64_DIR32LSB
    | IA_64_DIR64MSB
    | IA_64_DIR64LSB
    | IA_64_GPREL22
    | IA_64_GPREL64I
    | IA_64_GPREL32MSB
    | IA_64_GPREL32LSB
    | IA_64_GPREL64MSB
    | IA_64_GPREL64LSB
    | IA_64_LTOFF22
    | IA_64_LTOFF64I
    | IA_64_PLTOFF22
    | IA_64_PLTOFF64I
    | IA_64_PLTOFF64MSB
    | IA_64_PLTOFF64LSB
    | IA_64_FPTR64I
    | IA_64_FPTR32MSB
    | IA_64_FPTR32LSB
    | IA_64_FPTR64MSB
    | IA_64_FPTR64LSB
    | IA_64_PCREL60B
    | IA_64_PCREL21B
    | IA_64_PCREL21M
    | IA_64_PCREL21F
    | IA_64_PCREL32MSB
    | IA_64_PCREL32LSB
    | IA_64_PCREL64MSB
    | IA_64_PCREL64LSB
    | IA_64_LTOFF_FPTR22
    | IA_64_LTOFF_FPTR64I
    | IA_64_LTOFF_FPTR32MSB
    | IA_64_LTOFF_FPTR32LSB
    | IA_64_LTOFF_FPTR64MSB
    | IA_64_LTOFF_FPTR64LSB
    | IA_64_SEGREL32MSB
    | IA_64_SEGREL32LSB
    | IA_64_SEGREL64MSB
    | IA_64_SEGREL64LSB
    | IA_64_SECREL32MSB
    | IA_64_SECREL32LSB
    | IA_64_SECREL64MSB
    | IA_64_SECREL64LSB
    | IA_64_REL32MSB
    | IA_64_REL32LSB
    | IA_64_REL64MSB
    | IA_64_REL64LSB
    | IA_64_LTV32MSB
    | IA_64_LTV32LSB
    | IA_64_LTV64MSB
    | IA_64_LTV64LSB
    | IA_64_PCREL21BI
    | IA_64_PCREL22
    | IA_64_PCREL64I
    | IA_64_IPLTMSB
    | IA_64_IPLTLSB
    | IA_64_SUB
    | IA_64_LTOFF22X
    | IA_64_LDXMOV
    | IA_64_TPREL14
    | IA_64_TPREL22
    | IA_64_TPREL64I
    | IA_64_TPREL64MSB
    | IA_64_TPREL64LSB
    | IA_64_LTOFF_TPREL22
    | IA_64_DTPMOD64MSB
    | IA_64_DTPMOD64LSB
    | IA_64_LTOFF_DTPMOD22
    | IA_64_DTPREL14
    | IA_64_DTPREL22
    | IA_64_DTPREL64I
    | IA_64_DTPREL32MSB
    | IA_64_DTPREL32LSB
    | IA_64_DTPREL64MSB
    | IA_64_DTPREL64LSB
    | IA_64_LTOFF_DTPREL22

  type mips =
    | MIPS_NONE
    | MIPS_16
    | MIPS_32
    | MIPS_REL32
    | MIPS_26
    | MIPS_HI16
    | MIPS_LO16
    | MIPS_GPREL16
    | MIPS_LITERAL
    | MIPS_GOT16
    | MIPS_PC16
    | MIPS_CALL16
    | MIPS_GPREL32
    | MIPS_GOTHI16
    | MIPS_GOTLO16
    | MIPS_CALLHI16
    | MIPS_CALLLO16
    | MIPS_GLOB_DAT
    | MIPS_COPY
    | MIPS_JMP_SLOT
    | MIPS_IMPLICIT_RELATIVE

  type ppc =
    | PPC_NONE
    | PPC_ADDR32
    | PPC_ADDR24
    | PPC_ADDR16
    | PPC_ADDR16_LO
    | PPC_ADDR16_HI
    | PPC_ADDR16_HA
    | PPC_ADDR14
    | PPC_ADDR14_BRTAKEN
    | PPC_ADDR14_BRNTAKEN
    | PPC_REL24
    | PPC_REL14
    | PPC_REL14_BRTAKEN
    | PPC_REL14_BRNTAKEN
    | PPC_GOT16
    | PPC_GOT16_LO
    | PPC_GOT16_HI
    | PPC_GOT16_HA
    | PPC_PLTREL24
    | PPC_COPY
    | PPC_GLOB_DAT
    | PPC_JMP_SLOT
    | PPC_RELATIVE
    | PPC_LOCAL24PC
    | PPC_UADDR32
    | PPC_UADDR16
    | PPC_REL32
    | PPC_PLT32
    | PPC_PLTREL32
    | PPC_PLT16_LO
    | PPC_PLT16_HI
    | PPC_PLT16_HA
    | PPC_SDAREL16
    | PPC_SECTOFF
    | PPC_SECTOFF_LO
    | PPC_SECTOFF_HI
    | PPC_SECTOFF_HA
    | PPC_TLS
    | PPC_DTPMOD32
    | PPC_TPREL16
    | PPC_TPREL16_LO
    | PPC_TPREL16_HI
    | PPC_TPREL16_HA
    | PPC_TPREL32
    | PPC_DTPREL16
    | PPC_DTPREL16_LO
    | PPC_DTPREL16_HI
    | PPC_DTPREL16_HA
    | PPC_DTPREL32
    | PPC_GOT_TLSGD16
    | PPC_GOT_TLSGD16_LO
    | PPC_GOT_TLSGD16_HI
    | PPC_GOT_TLSGD16_HA
    | PPC_GOT_TLSLD16
    | PPC_GOT_TLSLD16_LO
    | PPC_GOT_TLSLD16_HI
    | PPC_GOT_TLSLD16_HA
    | PPC_GOT_TPREL16
    | PPC_GOT_TPREL16_LO
    | PPC_GOT_TPREL16_HI
    | PPC_GOT_TPREL16_HA
    | PPC_EMB_NADDR32
    | PPC_EMB_NADDR16
    | PPC_EMB_NADDR16_LO
    | PPC_EMB_NADDR16_HI
    | PPC_EMB_NADDR16_HA
    | PPC_EMB_SDAI16
    | PPC_EMB_SDA2I16
    | PPC_EMB_SDA2REL
    | PPC_EMB_SDA21
    | PPC_EMB_MRKREF
    | PPC_EMB_RELSEC16
    | PPC_EMB_RELST_LO
    | PPC_EMB_RELST_HI
    | PPC_EMB_RELST_HA
    | PPC_EMB_BIT_FLD
    | PPC_EMB_RELSDA

  type ppc64 =
    | PPC64_ADDR64
    | PPC64_ADDR16_HIGHER
    | PPC64_ADDR16_HIGHERA
    | PPC64_ADDR16_HIGHEST
    | PPC64_ADDR16_HIGHESTA
    | PPC64_UADDR64
    | PPC64_REL64
    | PPC64_PLT64
    | PPC64_PLTREL64
    | PPC64_TOC16
    | PPC64_TOC16_LO
    | PPC64_TOC16_HI
    | PPC64_TOC16_HA
    | PPC64_TOC
    | PPC64_DTPMOD64
    | PPC64_TPREL64
    | PPC64_DTPREL64

  type sparc =
    | SPARC_NONE
    | SPARC_8
    | SPARC_16
    | SPARC_32
    | SPARC_DISP8
    | SPARC_DISP16
    | SPARC_DISP32
    | SPARC_WDISP30
    | SPARC_WDISP22
    | SPARC_HI22
    | SPARC_22
    | SPARC_13
    | SPARC_LO10
    | SPARC_GOT10
    | SPARC_GOT13
    | SPARC_GOT22
    | SPARC_PC10
    | SPARC_PC22
    | SPARC_WPLT30
    | SPARC_COPY
    | SPARC_GLOB_DAT
    | SPARC_JMP_SLOT
    | SPARC_RELATIVE
    | SPARC_UA32
    | SPARC_PLT32
    | SPARC_HIPLT22
    | SPARC_LOPLT10
    | SPARC_PCPLT32
    | SPARC_PCPLT22
    | SPARC_PCPLT10
    | SPARC_10
    | SPARC_11
    | SPARC_64
    | SPARC_OLO10
    | SPARC_HH22
    | SPARC_HM10
    | SPARC_LM22
    | SPARC_PC_HH22
    | SPARC_PC_HM10
    | SPARC_PC_LM22
    | SPARC_WDISP16
    | SPARC_WDISP19
    | SPARC_GLOB_JMP
    | SPARC_7
    | SPARC_5
    | SPARC_6
    | SPARC_DISP64
    | SPARC_PLT64
    | SPARC_HIX22
    | SPARC_LOX10
    | SPARC_H44
    | SPARC_M44
    | SPARC_L44
    | SPARC_REGISTER
    | SPARC_UA64
    | SPARC_UA16
    | SPARC_TLS_GD_HI22
    | SPARC_TLS_GD_LO10
    | SPARC_TLS_GD_ADD
    | SPARC_TLS_GD_CALL
    | SPARC_TLS_LDM_HI22
    | SPARC_TLS_LDM_LO10
    | SPARC_TLS_LDM_ADD
    | SPARC_TLS_LDM_CALL
    | SPARC_TLS_LDO_HIX22
    | SPARC_TLS_LDO_LOX10
    | SPARC_TLS_LDO_ADD
    | SPARC_TLS_IE_HI22
    | SPARC_TLS_IE_LO10
    | SPARC_TLS_IE_LD
    | SPARC_TLS_IE_LDX
    | SPARC_TLS_IE_ADD
    | SPARC_TLS_LE_HIX22
    | SPARC_TLS_LE_LOX10
    | SPARC_TLS_DTPMOD32
    | SPARC_TLS_DTPMOD64
    | SPARC_TLS_DTPOFF32
    | SPARC_TLS_DTPOFF64
    | SPARC_TLS_TPOFF32
    | SPARC_TLS_TPOFF64

  type x86_64 =
    | X86_64_NONE
    | X86_64_64
    | X86_64_PC32
    | X86_64_GOT32
    | X86_64_PLT32
    | X86_64_COPY
    | X86_64_GLOB_DAT
    | X86_64_JMP_SLOT
    | X86_64_RELATIVE
    | X86_64_GOTPCREL
    | X86_64_32
    | X86_64_32S
    | X86_64_16
    | X86_64_PC16
    | X86_64_8
    | X86_64_PC8
    | X86_64_DTPMOD64
    | X86_64_DTPOFF64
    | X86_64_TPOFF64
    | X86_64_TLSGD
    | X86_64_TLSLD
    | X86_64_DTPOFF32
    | X86_64_GOTTPOFF
    | X86_64_TPOFF32
    | X86_64_IRELATIVE

  type armv8 =
    | ARMV8_NONE
    | ARMV8_ABS64
    | ARMV8_ABS32
    | ARMV8_ABS16
    | ARMV8_PREL64
    | ARMV8_PREL32
    | ARMV8_PREL16
    | ARMV8_GOTREL64
    | ARMV8_GOTREL32
    | ARMV8_COPY
    | ARMV8_GLOB_DAT
    | ARMV8_JMP_SLOT

  type riscv =
    | RISCV_NONE
    | RISCV_32
    | RISCV_64
    | RISCV_RELATIVE
    | RISCV_COPY
    | RISCV_JMP_SLOT
    | RISCV_TLS_DTPMOD32
    | RISCV_TLS_DTPMOD64
    | RISCV_TLS_DTPREL32
    | RISCV_TLS_DTPREL64
    | RISCV_TLS_TPREL32
    | RISCV_TLS_TPREL64
    | RISCV_BRANCH
    | RISCV_JAL
    | RISCV_CALL
    | RISCV_CALL_PLT
    | RISCV_GOT_HI20
    | RISCV_TLS_GOT_HI20
    | RISCV_TLS_GD_HI20
    | RISCV_PCREL_HI20
    | RISCV_PCREL_LO12_I
    | RISCV_PCREL_LO12_S
    | RISCV_HI20
    | RISCV_LO12_I
    | RISCV_LO12_S
    | RISCV_TPREL_HI20
    | RISCV_TPREL_LO12_I
    | RISCV_TPREL_LO12_S
    | RISCV_TPREL_ADD
    | RISCV_ADD8
    | RISCV_ADD16
    | RISCV_ADD32
    | RISCV_ADD64
    | RISCV_SUB8
    | RISCV_SUB16
    | RISCV_SUB32
    | RISCV_SUB64
    | RISCV_GNU_VTINHERIT
    | RISCV_GNU_VTENTRY
    | RISCV_ALIGN
    | RISCV_RVC_BRANCH
    | RISCV_RVC_JUMP
    | RISCV_RVC_LUI
    | RISCV_GPREL_I
    | RISCV_GPREL_S
    | RISCV_TPREL_I
    | RISCV_TPREL_S
    | RISCV_RELAX
    | RISCV_SUB6
    | RISCV_SET6
    | RISCV_SET8
    | RISCV_SET16
    | RISCV_SET32
    | RISCV_32_PCREL
    | RISCV_IRELATIVE

  type typ =
    | I386 of i386
    | Arm of arm
    | IA64 of ia64
    | Mips of mips
    | Ppc of ppc
    | Ppc64 of ppc64
    | Sparc of sparc
    | X86_64 of x86_64
    | Armv8 of armv8
    | Riscv of riscv

  type t =
    { offset: int64
    ; stndx: int
    ; typ: typ option
    ; addend: int64 option
    ; symbol: Symbol.t option }
end

module Dyn = struct
  type tag =
    | T_NULL
    | T_NEEDED
    | T_PLTRELSZ
    | T_PLTGOT
    | T_HASH
    | T_STRTAB
    | T_SYMTAB
    | T_RELA
    | T_RELASZ
    | T_RELAENT
    | T_STRSZ
    | T_SYMENT
    | T_INIT
    | T_FINI
    | T_SONAME
    | T_RPATH
    | T_SYMBOLIC
    | T_REL
    | T_RELSZ
    | T_RELENT
    | T_PLTREL
    | T_DEBUG
    | T_TEXTREL
    | T_JMPREL
    | T_BIND_NOW
    | T_INIT_ARRAY
    | T_FINI_ARRAY
    | T_INIT_ARRAYSZ
    | T_FINI_ARRAYSZ
    | T_RUNPATH
    | T_FLAGS
    | T_PREINIT_ARRAY
    | T_PREINIT_ARRAYSZ
    | T_OS of int64
    | T_PROC of int64
    | T_GNU_HASH
    | T_TLSDESC_PLT
    | T_TLSDESC_GOT
    | T_GNU_CONFLICT
    | T_GNU_LIBLIST
    | T_CONFIG
    | T_DEPAUDIT
    | T_AUDIT
    | T_PLTPAD
    | T_MOVETAB
    | T_SYMINFO
    | T_VERSYM
    | T_RELACOUNT
    | T_RELCOUNT
    | T_FLAGS_1
    | T_VERDEF
    | T_VERDEFNUM
    | T_VERNEED
    | T_VERNEEDNUM

  type t = {tag: tag option; value: int64}

  let is_df_origin t =
    match t.tag with
    | Some T_FLAGS -> Int64.(t.value land 0x1L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_symbolic t =
    match t.tag with
    | Some T_FLAGS -> Int64.(t.value land 0x2L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_textrel t =
    match t.tag with
    | Some T_FLAGS -> Int64.(t.value land 0x4L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_bind_now t =
    match t.tag with
    | Some T_FLAGS -> Int64.(t.value land 0x8L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_static_tls t =
    match t.tag with
    | Some T_FLAGS -> Int64.(t.value land 0x10L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_now t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x1L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_global t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x2L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_group t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x4L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_nodelete t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x8L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_loadfltr t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x10L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_initfirst t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x20L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_noopen t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x40L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_origin t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x80L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_direct t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x100L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_trans t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x200L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_interpose t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x400L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_nodeflib t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x800L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_nodump t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x1000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_confalt t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x2000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_endfiltee t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x4000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_dispreldne t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x8000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_disprelpnd t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x10000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_nodirect t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x20000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_ignmuldef t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x40000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_noksyms t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x80000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_nohdr t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x100000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_edited t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x200000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_noreloc t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x400000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_symintpose t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x800000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_globaudit t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x1000000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_singleton t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x2000000L <> 0L)
    | _ -> false
    [@@inline]

  let is_df_1_pie t =
    match t.tag with
    | Some T_FLAGS_1 -> Int64.(t.value land 0x8000000L <> 0L)
    | _ -> false
    [@@inline]
end

module Dwarf_pointer = struct
  type t = {address: int64; indirect: bool}
end

module Cie_augmentation = struct
  type personality =
    {encoding: int; pointer: Dwarf_pointer.t; personality_ptr: int64}

  type t =
    { length: int64 option
    ; lsda_pointer_encoding: int option
    ; personality: personality option
    ; fde_pointer_encoding: int option }
end

module Lsda_call_site = struct
  type t =
    { instruction_start_pointer: Dwarf_pointer.t
    ; instruction_start: int64
    ; instruction_end_pointer: Dwarf_pointer.t
    ; instruction_end: int64
    ; landing_pad_pointer: Dwarf_pointer.t
    ; landing_pad: int64
    ; action: int64 }
end

module Lsda = struct
  type t =
    { virtual_address: int64
    ; landing_pad_base_encoding: int
    ; landing_pad_base_pointer: Dwarf_pointer.t option
    ; landing_pad_base: int64
    ; type_table_encoding: int
    ; type_table_offset: int64 option
    ; call_site_table_encoding: int
    ; call_site_table_length: int64
    ; call_site_table: Lsda_call_site.t array }
end

module Fde = struct
  type t =
    { augmentation: Cie_augmentation.t
    ; pc_begin_pointer: Dwarf_pointer.t
    ; pc_begin: int64
    ; pc_range_pointer: Dwarf_pointer.t
    ; pc_range: int64
    ; augmentation_length: int64 option
    ; lsda_pointer: Dwarf_pointer.t option
    ; lsda: Lsda.t option }
end

module Cie = struct
  type t =
    { offset: int64
    ; version: int
    ; augmentation_string: string
    ; eh_data: int64 option
    ; code_alignment_factor: int64
    ; data_alignment_factor: int64
    ; return_address_register: int
    ; fdes: Fde.t Int64.Map.t }
end

module Eh_frame = struct
  type t =
    { version: int
    ; frame_encoding: int
    ; fde_count_encoding: int
    ; lookup_table_encoding: int
    ; eh_pointer: Dwarf_pointer.t
    ; fde_count_pointer: Dwarf_pointer.t
    ; cies: Cie.t Int64.Map.t }
end

type t =
  { eclass: [`ELF32 | `ELF64]
  ; endian: Endian.t
  ; etype: Type.t
  ; machine: Machine.t
  ; version: int32
  ; abiversion: int
  ; osabi: Osabi.t
  ; entry: int64
  ; flags: int32
  ; ehsize: int
  ; phentsize: int
  ; shentsize: int
  ; segments: Segment.t array
  ; sections: Section.t array
  ; symbols: Symbol.t array
  ; relocs: Reloc.t array
  ; dynamic: Dyn.t array
  ; eh_frame: Eh_frame.t option
  ; plt: Symbol.t Int64.Map.t }

let total_header_size elf =
  elf.ehsize
  + (elf.phentsize * Array.length elf.segments)
  + (elf.shentsize * Array.length elf.sections)
  [@@inline]

let virtual_base elf =
  let open Option.Let_syntax in
  let%map phdr =
    Array.find elf.segments ~f:(fun seg ->
        Segment.(equal_typ seg.typ T_PHDR) )
  in
  Int64.(phdr.vaddr - phdr.offset)

let section_of_vaddr elf vaddr =
  Array.find elf.sections (fun sec -> Section.is_vaddr_within sec vaddr)
  [@@inline]

let section_of_name elf name =
  Array.find elf.sections (fun sec -> String.equal name sec.name)
  [@@inline]

let file_offset_of_vaddr_section elf vaddr =
  let open Option.Let_syntax in
  let%map sec = section_of_vaddr elf vaddr in
  Section.file_offset_of_vaddr sec vaddr
  [@@inline]

let segment_of_vaddr elf vaddr =
  Array.find elf.segments (fun seg -> Segment.is_vaddr_within seg vaddr)
  [@@inline]

let file_offset_of_vaddr_segment elf vaddr =
  let open Option.Let_syntax in
  let%map seg = segment_of_vaddr elf vaddr in
  Segment.file_offset_of_vaddr seg vaddr
  [@@inline]

let dyn_syms elf =
  Array.filter elf.symbols ~f:(fun sym ->
      try
        let sec = elf.sections.(sym.shndx) in
        match sec.kind with
        | Section.T_DYNSYM -> true
        | _ -> false
      with Invalid_argument _ -> false )
