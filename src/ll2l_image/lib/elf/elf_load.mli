open Core_kernel

val from_bigstring_exn : Bigstring.t -> Image.t

val from_bigstring : Bigstring.t -> Image.t option
