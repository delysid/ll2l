open Core_kernel
open Stdio
open Ll2l_std
open Mach_o

let parse_header_flag = function
  | 0 -> Flags.NOUNDEFS
  | 1 -> Flags.INCRLINK
  | 2 -> Flags.DYLDLINK
  | 3 -> Flags.BINDATLOAD
  | 4 -> Flags.PREBOUND
  | 5 -> Flags.SPLIT_SEGS
  | 6 -> Flags.LAZY_INIT
  | 7 -> Flags.TWOLEVEL
  | 8 -> Flags.FORCE_FLAT
  | 9 -> Flags.NOMULTIDEFS
  | 10 -> Flags.NOFIXPREBINDING
  | 11 -> Flags.PREBINDABLE
  | 12 -> Flags.ALLMODSBOUND
  | 13 -> Flags.SUBSECTIONS_VIA_SYMBOLS
  | 14 -> Flags.CANONICAL
  | 15 -> Flags.WEAK_DEFINES
  | 16 -> Flags.BINDS_TO_WEAK
  | 17 -> Flags.ALLOW_STACK_EXECUTION
  | 18 -> Flags.ROOT_SAFE
  | 19 -> Flags.SETUID_SAFE
  | 20 -> Flags.NO_REEXPORTED_DYLIBS
  | 21 -> Flags.PIE
  | 22 -> Flags.DEAD_STRIPPABLE_DYLIB
  | 23 -> Flags.HAS_TLV_DESCRIPTORS
  | 24 -> Flags.NO_HEAP_EXECUTION
  | 25 -> Flags.APP_EXTENSION_SAFE
  | n -> Flags.Ext n

let parse_filetype = function
  | 0x1 -> Some Filetype.OBJECT
  | 0x2 -> Some Filetype.EXECUTE
  | 0x3 -> Some Filetype.FVMLIB
  | 0x4 -> Some Filetype.CORE
  | 0x5 -> Some Filetype.PRELOAD
  | 0x6 -> Some Filetype.DYLIB
  | 0x7 -> Some Filetype.DYLINKER
  | 0x8 -> Some Filetype.BUNDLE
  | 0x9 -> Some Filetype.DYLIB_STUB
  | 0xA -> Some Filetype.DSYM
  | 0xB -> Some Filetype.KEXT_BUNDLE
  | _ -> None

let parse_cputype = function
  | 0xFFFFFFFF -> Some Cputype.ANY
  | 1 -> Some Cputype.VAX
  | 6 -> Some Cputype.MC680X0
  | 7 -> Some Cputype.I386
  | 0x01000007 -> Some Cputype.X86_64
  | 8 -> Some Cputype.MIPS
  | 10 -> Some Cputype.MC98000
  | 11 -> Some Cputype.HPPA
  | 12 -> Some Cputype.ARM
  | 0x0100000C -> Some Cputype.ARM64
  | 0x0200000C -> Some Cputype.ARM64_32
  | 13 -> Some Cputype.MC88000
  | 14 -> Some Cputype.SPARC
  | 15 -> Some Cputype.I860
  | 16 -> Some Cputype.ALPHA
  | 18 -> Some Cputype.POWERPC
  | 0x01000012 -> Some Cputype.POWERPC64
  | _ -> None

let parse_cpusubtype_any = function
  | 0xFFFFFFFF -> Some Cpusubtype.MULTIPLE
  | 0 -> Some Cpusubtype.LITTLE_ENDIAN
  | 1 -> Some Cpusubtype.BIG_ENDIAN
  | _ -> None

let parse_cpusubtype_vax = function
  | 0 -> Some Cpusubtype.VAX_ALL
  | 1 -> Some Cpusubtype.VAX780
  | 2 -> Some Cpusubtype.VAX785
  | 3 -> Some Cpusubtype.VAX750
  | 4 -> Some Cpusubtype.VAX730
  | 5 -> Some Cpusubtype.UVAXI
  | 6 -> Some Cpusubtype.UVAXII
  | 7 -> Some Cpusubtype.VAX8200
  | 8 -> Some Cpusubtype.VAX8500
  | 9 -> Some Cpusubtype.VAX8600
  | 10 -> Some Cpusubtype.VAX8650
  | 11 -> Some Cpusubtype.VAX8800
  | 12 -> Some Cpusubtype.UVAXIII
  | _ -> None

let parse_cpusubtype_mc680x0 = function
  | 1 -> Some Cpusubtype.MC680X0_ALL
  | 2 -> Some Cpusubtype.MC68040
  | 3 -> Some Cpusubtype.MC68030_ONLY
  | _ -> None

let parse_cpusubtype_i386 = function
  | 3 -> Some Cpusubtype.I386_ALL
  | 4 -> Some Cpusubtype.I486
  | 132 -> Some Cpusubtype.I486SX
  | 5 -> Some Cpusubtype.I586
  | 22 -> Some Cpusubtype.PENTPRO
  | 54 -> Some Cpusubtype.PENTII_M3
  | 86 -> Some Cpusubtype.PENTII_M5
  | 103 -> Some Cpusubtype.CELERON
  | 119 -> Some Cpusubtype.CELERON_MOBILE
  | 8 -> Some Cpusubtype.PENTIUM_3
  | 24 -> Some Cpusubtype.PENTIUM_3_M
  | 40 -> Some Cpusubtype.PENTIUM_3_XEON
  | 9 -> Some Cpusubtype.PENTIUM_M
  | 10 -> Some Cpusubtype.PENTIUM_4
  | 26 -> Some Cpusubtype.PENTIUM_4_M
  (* | 11 -> Some Cpusubtype.ITANIUM
   * | 27 -> Some Cpusubtype.ITANIUM_2 *)
  | 12 -> Some Cpusubtype.XEON
  | 28 -> Some Cpusubtype.XEON_MP
  | _ -> None

let parse_cpusubtype_x86_64 = function
  | 3 -> Some Cpusubtype.X86_64_ALL
  | 8 -> Some Cpusubtype.X86_64_H
  | _ -> None

let parse_cpusubtype_mips = function
  | 0 -> Some Cpusubtype.MIPS_ALL
  | 1 -> Some Cpusubtype.MIPS_R2300
  | 2 -> Some Cpusubtype.MIPS_R2600
  | 3 -> Some Cpusubtype.MIPS_R2800
  | 4 -> Some Cpusubtype.MIPS_R2000A
  | 5 -> Some Cpusubtype.MIPS_R2000
  | 6 -> Some Cpusubtype.MIPS_R3000A
  | 7 -> Some Cpusubtype.MIPS_R3000
  | _ -> None

let parse_cpusubtype_mc98000 = function
  | 0 -> Some Cpusubtype.MC98000_ALL
  | 1 -> Some Cpusubtype.MC98601
  | _ -> None

let parse_cpusubtype_hppa = function
  | 0 -> Some Cpusubtype.HPPA_ALL
  | 1 -> Some Cpusubtype.HPPA_7100LC
  | _ -> None

let parse_cpusubtype_mc88000 = function
  | 0 -> Some Cpusubtype.MC88000_ALL
  | 1 -> Some Cpusubtype.MC88100
  | 2 -> Some Cpusubtype.MC88110
  | _ -> None

let parse_cpusubtype_sparc = function
  | 0 -> Some Cpusubtype.SPARC_ALL
  | _ -> None

let parse_cpusubtype_i860 = function
  | 0 -> Some Cpusubtype.I860_ALL
  | 1 -> Some Cpusubtype.I860_860
  | _ -> None

let parse_cpusubtype_powerpc = function
  | 0 -> Some Cpusubtype.POWERPC_ALL
  | 1 -> Some Cpusubtype.POWERPC_601
  | 2 -> Some Cpusubtype.POWERPC_602
  | 3 -> Some Cpusubtype.POWERPC_603
  | 4 -> Some Cpusubtype.POWERPC_603E
  | 5 -> Some Cpusubtype.POWERPC_603EV
  | 6 -> Some Cpusubtype.POWERPC_604
  | 7 -> Some Cpusubtype.POWERPC_604E
  | 8 -> Some Cpusubtype.POWERPC_620
  | 9 -> Some Cpusubtype.POWERPC_750
  | 10 -> Some Cpusubtype.POWERPC_7400
  | 11 -> Some Cpusubtype.POWERPC_7450
  | 100 -> Some Cpusubtype.POWERPC_970
  | _ -> None

let parse_cpusubtype_powerpc64 = function
  | 0 -> Some Cpusubtype.POWERPC_ALL
  | _ -> None

let parse_cpusubtype_arm = function
  | 0 -> Some Cpusubtype.ARM_ALL
  | 5 -> Some Cpusubtype.ARM_V4T
  | 6 -> Some Cpusubtype.ARM_V6
  | 7 -> Some Cpusubtype.ARM_V5TEJ
  | 8 -> Some Cpusubtype.ARM_XSCALE
  | 9 -> Some Cpusubtype.ARM_V7
  | 10 -> Some Cpusubtype.ARM_V7F
  | 11 -> Some Cpusubtype.ARM_V7S
  | 12 -> Some Cpusubtype.ARM_V7K
  | 14 -> Some Cpusubtype.ARM_V6M
  | 15 -> Some Cpusubtype.ARM_V7M
  | 16 -> Some Cpusubtype.ARM_V7EM
  | 13 -> Some Cpusubtype.ARM_V8
  | _ -> None

let parse_cpusubtype_arm64 = function
  | 0 -> Some Cpusubtype.ARM64_ALL
  | 1 -> Some Cpusubtype.ARM64_V8
  | 2 -> Some Cpusubtype.ARM64_E
  | _ -> None

let parse_cpusubtype_arm64_32 = function
  | 0 -> Some Cpusubtype.ARM64_32_ALL
  | 1 -> Some Cpusubtype.ARM64_32_V8
  | _ -> None

let parse_cpusubtype = function
  | Cputype.ANY -> parse_cpusubtype_any
  | Cputype.VAX -> parse_cpusubtype_vax
  | Cputype.MC680X0 -> parse_cpusubtype_mc680x0
  | Cputype.I386 -> parse_cpusubtype_i386
  | Cputype.X86_64 -> parse_cpusubtype_x86_64
  | Cputype.MIPS -> parse_cpusubtype_mips
  | Cputype.MC98000 -> parse_cpusubtype_mc98000
  | Cputype.HPPA -> parse_cpusubtype_hppa
  | Cputype.ARM -> parse_cpusubtype_arm
  | Cputype.ARM64 -> parse_cpusubtype_arm64
  | Cputype.ARM64_32 -> parse_cpusubtype_arm64_32
  | Cputype.MC88000 -> parse_cpusubtype_mc88000
  | Cputype.I860 -> parse_cpusubtype_i860
  | Cputype.POWERPC -> parse_cpusubtype_powerpc
  | Cputype.POWERPC64 -> parse_cpusubtype_powerpc64
  | _ -> fun _ -> None

let parse_section_type = function
  | 0x0 -> Some Sectiontype.REGULAR
  | 0x1 -> Some Sectiontype.ZEROFILL
  | 0x2 -> Some Sectiontype.CSTRING_LITERALS
  | 0x3 -> Some Sectiontype.FOURBYTE_LITERALS
  | 0x4 -> Some Sectiontype.EIGHTBYTE_LITERALS
  | 0x5 -> Some Sectiontype.LITERAL_POINTERS
  | 0x6 -> Some Sectiontype.NON_LAZY_SYMBOL_POINTERS
  | 0x7 -> Some Sectiontype.LAZY_SYMBOL_POINTERS
  | 0x8 -> Some Sectiontype.SYMBOL_STUBS
  | 0x9 -> Some Sectiontype.MOD_INIT_FUNC_POINTERS
  | 0xA -> Some Sectiontype.MOD_TERM_FUNC_POINTERS
  | 0xB -> Some Sectiontype.COALESCED
  | 0xC -> Some Sectiontype.GB_ZEROFILL
  | 0xD -> Some Sectiontype.INTERPOSING
  | 0xE -> Some Sectiontype.SIXTEENBYTE_LITERALS
  | 0xF -> Some Sectiontype.DTRACE_DOF
  | 0x10 -> Some Sectiontype.LAZY_DYLIB_SYMBOL_POINTERS
  | 0x11 -> Some Sectiontype.THREAD_LOCAL_REGULAR
  | 0x12 -> Some Sectiontype.THREAD_LOCAL_ZEROFILL
  | 0x13 -> Some Sectiontype.THREAD_LOCAL_VARIABLES
  | 0x14 -> Some Sectiontype.THREAD_LOCAL_VARIABLE_POINTERS
  | 0x15 -> Some Sectiontype.THREAD_LOCAL_INIT_FUNCTION_POINTERS
  | _ -> None

let parse_section_attr = function
  | 31 -> Sectionattr.PURE_INSTRUCTIONS
  | 30 -> Sectionattr.NO_TOC
  | 29 -> Sectionattr.STRIP_STATIC_SYMS
  | 28 -> Sectionattr.NO_DEAD_STRIP
  | 27 -> Sectionattr.LIVE_SUPPORT
  | 26 -> Sectionattr.SELF_MODIFYING_CODE
  | 25 -> Sectionattr.DEBUG
  | 10 -> Sectionattr.SOME_INSTRUCTIONS
  | 9 -> Sectionattr.EXT_RELOC
  | 8 -> Sectionattr.LOC_RELOC
  | n -> Sectionattr.Ext n

let parse_symbol_type = function
  | 0x0 -> Some Symboltype.UNDF
  | 0x2 -> Some Symboltype.ABS
  | 0xE -> Some Symboltype.SECT
  | 0xA -> Some Symboltype.INDR
  | _ -> None

let parse_generic_reloc_typ = function
  | 0 -> Some Reloc.GENERIC_VANILLA
  | 1 -> Some Reloc.GENERIC_PAIR
  | 2 -> Some Reloc.GENERIC_SECTDIFF
  | 3 -> Some Reloc.GENERIC_PB_LA_PTR
  | 4 -> Some Reloc.GENERIC_LOCAL_SECTDIFF
  | 5 -> Some Reloc.GENERIC_TLV
  | _ -> None

let parse_x86_64_reloc_typ = function
  | 0 -> Some Reloc.X86_64_UNSIGNED
  | 1 -> Some Reloc.X86_64_SIGNED
  | 2 -> Some Reloc.X86_64_BRANCH
  | 3 -> Some Reloc.X86_64_GOT_LOAD
  | 4 -> Some Reloc.X86_64_GOT
  | 5 -> Some Reloc.X86_64_SUBTRACTOR
  | 6 -> Some Reloc.X86_64_SIGNED_1
  | 7 -> Some Reloc.X86_64_SIGNED_2
  | 8 -> Some Reloc.X86_64_SIGNED_4
  | 9 -> Some Reloc.X86_64_TLV
  | _ -> None

let parse_ppc_reloc_typ = function
  | 0 -> Some Reloc.PPC_VANILLA
  | 1 -> Some Reloc.PPC_PAIR
  | 2 -> Some Reloc.PPC_BR14
  | 3 -> Some Reloc.PPC_BR24
  | 4 -> Some Reloc.PPC_HI16
  | 5 -> Some Reloc.PPC_LO16
  | 6 -> Some Reloc.PPC_HA16
  | 7 -> Some Reloc.PPC_LO14
  | 8 -> Some Reloc.PPC_SECTDIFF
  | 9 -> Some Reloc.PPC_PB_LA_PTR
  | 10 -> Some Reloc.PPC_HI16_SECTDIFF
  | 11 -> Some Reloc.PPC_LO16_SECTDIFF
  | 12 -> Some Reloc.PPC_HA16_SECTDIFF
  | 13 -> Some Reloc.PPC_JBSR
  | 14 -> Some Reloc.PPC_LO14_SECTDIFF
  | 15 -> Some Reloc.PPC_LOCAL_SECTDIFF
  | _ -> None

let parse_arm_reloc_typ = function
  | 0 -> Some Reloc.ARM_VANILLA
  | 1 -> Some Reloc.ARM_PAIR
  | 2 -> Some Reloc.ARM_SECTDIFF
  | 3 -> Some Reloc.ARM_LOCAL_SECTDIFF
  | 4 -> Some Reloc.ARM_PB_LA_PTR
  | 5 -> Some Reloc.ARM_BR24
  | 6 -> Some Reloc.ARM_THUMB_BR22
  | 7 -> Some Reloc.ARM_THUMB_32BIT_BRANCH
  | 8 -> Some Reloc.ARM_HALF
  | 9 -> Some Reloc.ARM_HALF_SECTDIFF
  | _ -> None

let parse_arm64_reloc_typ = function
  | 0 -> Some Reloc.ARM64_UNSIGNED
  | 1 -> Some Reloc.ARM64_SUBTRACTOR
  | 2 -> Some Reloc.ARM64_BRANCH26
  | 3 -> Some Reloc.ARM64_PAGE21
  | 4 -> Some Reloc.ARM64_PAGEOFF12
  | 5 -> Some Reloc.ARM64_GOT_LOAD_PAGE21
  | 6 -> Some Reloc.ARM64_GOT_LOAD_PAGEOFF12
  | 7 -> Some Reloc.ARM64_POINTER_TO_GOT
  | 8 -> Some Reloc.ARM64_TLVP_LOAD_PAGE21
  | 9 -> Some Reloc.ARM64_TLVP_LOAD_PAGEOFF12
  | 10 -> Some Reloc.ARM64_ADDEND
  | _ -> None

let parse_reloc_typ cputype typ =
  let open Option.Let_syntax in
  match cputype with
  | Some Cputype.I386 ->
      let%map typ = parse_generic_reloc_typ typ in
      Reloc.I386 typ
  | Some Cputype.X86_64 ->
      let%map typ = parse_x86_64_reloc_typ typ in
      Reloc.X86_64 typ
  | Some Cputype.POWERPC | Some Cputype.POWERPC64 ->
      let%map typ = parse_ppc_reloc_typ typ in
      Reloc.Ppc typ
  | Some Cputype.ARM ->
      let%map typ = parse_arm_reloc_typ typ in
      Reloc.Arm typ
  | Some Cputype.ARM64 ->
      let%map typ = parse_arm64_reloc_typ typ in
      Reloc.Arm64 typ
  | _ -> None

let parse_flags f n v =
  let rec aux res b v =
    if b = n then List.rev res
    else
      let res = if Int64.(bit_and v 1L = 1L) then f b :: res else res in
      aux res (b + 1) Int64.(v lsr 1)
  in
  aux [] 0 v

let parse_magic = function
  | 0xFEEDFACEl -> Some (`BE, false)
  | 0xCEFAEDFEl -> Some (`LE, false)
  | 0xFEEDFACFl -> Some (`BE, true)
  | 0xCFFAEDFEl -> Some (`LE, true)
  | _ -> None

let is_mach_o_file filename =
  try
    In_channel.with_file filename ~f:(fun file ->
        let buf = Bytes.create 4 in
        In_channel.really_input_exn file ~buf ~pos:0 ~len:4;
        let magic =
          let b0 = Bytes.get buf 0 |> Char.to_int |> Int.to_int32_exn in
          let b1 = Bytes.get buf 1 |> Char.to_int |> Int.to_int32_exn in
          let b2 = Bytes.get buf 2 |> Char.to_int |> Int.to_int32_exn in
          let b3 = Bytes.get buf 3 |> Char.to_int |> Int.to_int32_exn in
          Int32.((b0 lsl 24) lor (b1 lsl 16) lor (b2 lsl 8) lor b3)
        in
        parse_magic magic |> Option.is_some)
  with _ -> false

let parse_mach_header r =
  let open Option.Let_syntax in
  let%bind magic = Binreader.read32 r `BE in
  let%bind endian, is64 = parse_magic magic in
  let%bind cputype = Binreader.read32 r endian in
  let cputype = parse_cputype (Int32.to_int_exn cputype) in
  let%bind cpusubtype = Binreader.read32 r endian in
  let cpusubtype =
    match cputype with
    | None -> None
    | Some cputype -> parse_cpusubtype cputype (Int32.to_int_exn cpusubtype)
  in
  let%bind filetype = Binreader.read32 r endian in
  let filetype = parse_filetype (Int32.to_int_exn filetype) in
  let%bind ncmds = Binreader.read32 r endian in
  let%bind sizeofcmds = Binreader.read32 r endian in
  let%map flags = Binreader.read32 r endian in
  let flags =
    parse_flags parse_header_flag 32 Int64.(of_int32 flags land 0xFFFFFFFFL)
  in
  (is64, endian, cputype, cpusubtype, filetype, ncmds, sizeofcmds, flags)

type indirect_section =
  {stride: int32; size: int64; addr: int64; index: int32; sec: Section.t}

let trim_string s =
  match String.index s '\x00' with
  | None -> s
  | Some i -> String.subo s ~len:i

let parse_relocs r cputype endian (sections : Section.t array) =
  let open Option.Let_syntax in
  let relocs = Vector.create () in
  let n = Array.length sections in
  let rec loop i =
    if i >= n then return ()
    else
      let sec = sections.(i) in
      let n = Int32.to_int_exn sec.nreloc in
      if n <= 0 then loop (i + 1)
      else
        let%bind _ = Binreader.seek r Int32.(to_int_exn sec.reloff) in
        let rec loop2 i =
          if i >= n then return ()
          else
            let%bind addr = Binreader.read32 r endian in
            let%bind data = Binreader.read32 r endian in
            let reloc =
              (* scatered_relocation_info *)
              if Int32.(addr land 0x80000000l <> zero) then
                let addr = Int32.(addr land 0xFFFFFFl) in
                let typ = Int32.(to_int_exn ((addr lsr 24) land 0xFl)) in
                let typ = parse_reloc_typ cputype typ in
                let len = Int32.(to_int_exn ((addr lsr 28) land 0x3l)) in
                let pcrel = Int32.(addr land 0x40000000l <> zero) in
                Reloc.(Scattered {addr; typ; len; pcrel; value= data; sec})
              else
                let symbolnum = Int32.(to_int_exn (data land 0xFFFFFFl)) in
                let pcrel = Int32.(data land 0x1000000l <> zero) in
                let len = Int32.(to_int_exn ((data lsr 25) land 3l)) in
                let extern = Int32.(data land 0x8000000l <> zero) in
                let typ = Int32.(to_int_exn ((data lsr 28) land 0xFl)) in
                let typ = parse_reloc_typ cputype typ in
                Reloc.(
                  Default {addr; symbolnum; pcrel; len; extern; typ; sec})
            in
            Vector.push_back relocs reloc;
            loop2 (i + 1)
        in
        let%bind _ = loop2 0 in
        loop (i + 1)
  in
  let%map _ = loop 0 in
  Vector.to_array relocs

let parse_exports r trie_offset trie_size =
  let open Option.Let_syntax in
  let exports = Vector.create () in
  let visited = Hash_set.create (module Int64) in
  let rec parse_export_trie offset str =
    if Int64.(offset >= trie_offset + trie_size) then return ()
    else
      let%bind _ = Binreader.seek r Int64.(to_int_exn offset) in
      let%bind b = Binreader.read8 r in
      if b = 0 then
        let%bind num_children = Binreader.read_uleb128 r in
        parse_children num_children str
      else
        let%bind _ = Binreader.read_uleb128 r in
        let%bind flag = Binreader.read8 r in
        let%map symbol_offset = Binreader.read_uleb128 r in
        Vector.push_back exports
          Export.{symbolname= str; addr= symbol_offset}
  and parse_children num_children str =
    if Int64.(num_children <= zero) then return ()
    else
      let%bind suff = Binreader.read_string r in
      let%bind next_node = Binreader.read_uleb128 r in
      if Int64.(next_node = zero) then return ()
      else
        let offset = Int64.(trie_offset + next_node) in
        match Hash_set.strict_add visited offset with
        | Error _ -> return ()
        | Ok () ->
            let pos = Binreader.position r in
            let%bind _ = parse_export_trie offset (str ^ suff) in
            let%bind _ = Binreader.seek r pos in
            parse_children Int64.(num_children - one) str
  in
  let%map _ = parse_export_trie trie_offset "" in
  Vector.to_array exports

let from_bigstring data =
  let open Option.Let_syntax in
  let r = Binreader.from_bigstring data in
  let%bind ( is64
           , endian
           , cputype
           , cpusubtype
           , filetype
           , ncmds
           , sizeofcmds
           , flags ) =
    parse_mach_header r
  in
  (* 64-bit header has a 'reserved' field *)
  let%bind _ = if is64 then Binreader.skip r 4 else return () in
  (* parse the load commands *)
  let ncmds = Int32.to_int_exn ncmds in
  let load_commands = Vector.create ~capacity:ncmds () in
  let r8 () = Binreader.read8 r in
  let r16 () = Binreader.read16 r endian in
  let r32 () = Binreader.read32 r endian in
  let r64 () = Binreader.read64 r endian in
  let sections = Vector.create () in
  let indirect_sections = Vector.create () in
  let symbols = Vector.create () in
  let indirect_symbols = Vector.create () in
  let function_starts = Vector.create () in
  let base = ref 0L in
  let entry = ref None in
  let rec loop i =
    if i >= ncmds then return ()
    else
      let pos = Binreader.position r in
      let%bind cmd = r32 () in
      let%bind cmdsize = r32 () in
      let%bind cmd =
        match cmd with
        | 0x1l | 0x19l ->
            (* LC_SEGMENT/LC_SEGMENT_64 *)
            let is64 = Int32.(cmd = 0x19l) in
            let r3264 () =
              if is64 then r64 ()
              else
                let%map x = r32 () in
                Int64.of_int32 x
            in
            let%bind name = Binreader.read_string r ~size:(Some 16) in
            let name = trim_string name in
            let%bind vmaddr = r3264 () in
            let%bind vmsize = r3264 () in
            let%bind fileoff = r3264 () in
            let%bind filesize = r3264 () in
            let%bind maxprot = r32 () in
            let%bind initprot = r32 () in
            let%bind nsects = r32 () in
            let%bind flags = r32 () in
            if Int64.(fileoff = zero && filesize = zero) then base := vmaddr;
            let n = Int32.to_int_exn nsects in
            let rec loop2 i =
              if i >= n then return ()
              else
                let%bind secname = Binreader.read_string r ~size:(Some 16) in
                let secname = trim_string secname in
                let%bind segname = Binreader.read_string r ~size:(Some 16) in
                let segname = trim_string segname in
                let%bind addr = r3264 () in
                let%bind size = r3264 () in
                let%bind offset = r32 () in
                let%bind align = r32 () in
                let%bind reloff = r32 () in
                let%bind nreloc = r32 () in
                let%bind flags = r32 () in
                let%bind reserved1 = r32 () in
                let%bind reserved2 = r32 () in
                (* 64-bit sections have a third reserved field *)
                let%bind _ =
                  if is64 then Binreader.skip r 4 else return ()
                in
                let typ = Int32.to_int_exn flags land 0xFF in
                let typ = parse_section_type typ in
                let attributes = Int32.to_int_exn flags land 0xFFFFFF00 in
                let attributes =
                  parse_flags parse_section_attr 32 Int64.(of_int attributes)
                in
                let sec =
                  Section.
                    { secname
                    ; segname
                    ; addr
                    ; size
                    ; offset
                    ; align
                    ; reloff
                    ; nreloc
                    ; typ
                    ; attributes }
                in
                Vector.push_back sections sec;
                ( match typ with
                | Some Sectiontype.NON_LAZY_SYMBOL_POINTERS
                 |Some Sectiontype.LAZY_SYMBOL_POINTERS
                 |Some Sectiontype.LAZY_DYLIB_SYMBOL_POINTERS
                 |Some Sectiontype.THREAD_LOCAL_VARIABLE_POINTERS
                 |Some Sectiontype.SYMBOL_STUBS ->
                    let stride =
                      match typ with
                      | Some Sectiontype.SYMBOL_STUBS -> reserved2
                      | _ -> 8l
                    in
                    let index = reserved1 in
                    let indirect_sec = {stride; addr; size; index; sec} in
                    Vector.push_back indirect_sections indirect_sec
                | _ -> () );
                loop2 (i + 1)
            in
            let%map _ = loop2 0 in
            let cmd =
              Segmentcommand.
                { cmd
                ; cmdsize
                ; name
                ; vmaddr
                ; vmsize
                ; fileoff
                ; filesize
                ; maxprot
                ; initprot
                ; flags
                ; nsects }
            in
            if is64 then Loadcommand.Segment64 cmd
            else Loadcommand.Segment32 cmd
        | 0xBl ->
            (* LC_DYSYMTAB *)
            let%bind ilocalsym = r32 () in
            let%bind nlocalsym = r32 () in
            let%bind iextdefsym = r32 () in
            let%bind nextdefsym = r32 () in
            let%bind iundefsym = r32 () in
            let%bind nundefsym = r32 () in
            let%bind tocoff = r32 () in
            let%bind ntoc = r32 () in
            let%bind modtaboff = r32 () in
            let%bind nmodtab = r32 () in
            let%bind extrefsymoff = r32 () in
            let%bind nextrefsyms = r32 () in
            let%bind indirectsymoff = r32 () in
            let%bind nindirectsyms = r32 () in
            let%bind extreloff = r32 () in
            let%bind nextrel = r32 () in
            let%bind locreloff = r32 () in
            let%bind nlocrel = r32 () in
            let rec loop2 i =
              if i >= Vector.length indirect_sections then return ()
              else
                let sec = Vector.get_exn indirect_sections i in
                let%bind _ =
                  Binreader.seek r
                    Int32.(to_int_exn (indirectsymoff + (sec.index * 4l)))
                in
                let rec loop3 i =
                  if Int64.(i >= sec.size) then return ()
                  else
                    let%bind index = r32 () in
                    let%bind _ =
                      if
                        Int32.(
                          index land 0x80000000l = zero
                          && index land 0x40000000l = zero)
                      then
                        let%map sym =
                          Vector.get symbols Int32.(to_int_exn index)
                        in
                        let sym =
                          Symbol.
                            { sym with
                              sec= Some sec.sec
                            ; value= Int64.(sec.addr + i) }
                        in
                        Vector.push_back indirect_symbols sym
                      else return ()
                    in
                    loop3 Int64.(i + of_int32 sec.stride)
                in
                let%bind _ = loop3 0L in
                loop2 (i + 1)
            in
            let%map _ = loop2 0 in
            Loadcommand.(
              Dysymtab
                { cmd
                ; cmdsize
                ; ilocalsym
                ; nlocalsym
                ; iextdefsym
                ; nextdefsym
                ; iundefsym
                ; nundefsym
                ; tocoff
                ; ntoc
                ; modtaboff
                ; nmodtab
                ; extrefsymoff
                ; nextrefsyms
                ; indirectsymoff
                ; nindirectsyms
                ; extreloff
                ; nextrel
                ; locreloff
                ; nlocrel })
        | 0xEl ->
            (* LC_LOAD_DYLINKER *)
            let%map name = r32 () in
            Loadcommand.Loaddylinker Dylinkercommand.{cmd; cmdsize; name}
        | 0xFl ->
            (* LC_ID_DYLINKER *)
            let%map name = r32 () in
            Loadcommand.Iddylinker Dylinkercommand.{cmd; cmdsize; name}
        | 0x1Bl ->
            (* LC_UUID *)
            let%map uuid = Binreader.read_bytes r 16 in
            Loadcommand.(Uuid {cmd; cmdsize; uuid})
        | 0x2l ->
            (* LC_SYMTAB *)
            let%bind symoff = r32 () in
            let%bind nsyms = r32 () in
            let%bind stroff = r32 () in
            let%bind strsize = r32 () in
            let%bind _ = Binreader.seek r Int32.(to_int_exn stroff) in
            let%bind strtab =
              Binreader.read_bytes r Int32.(to_int_exn strsize)
            in
            let strtab = Binreader.from_bytes strtab in
            let%bind _ = Binreader.seek r Int32.(to_int_exn symoff) in
            let r3264 () =
              if is64 then r64 ()
              else
                let%map x = r32 () in
                Int64.of_int32 x
            in
            let n = Int32.to_int_exn nsyms in
            let rec loop2 i =
              if i >= n then return ()
              else
                let%bind strx = r32 () in
                let%bind typ = r8 () in
                let%bind nsect = r8 () in
                (* n_desc *)
                let%bind _ = r16 () in
                let%bind value = r3264 () in
                let%bind _ = Binreader.seek strtab Int32.(to_int_exn strx) in
                let%bind name = Binreader.read_string strtab in
                let sec = Vector.get sections (nsect - 1) in
                let typ = parse_symbol_type (typ land 0xE) in
                let sym = Symbol.{name; typ; sec; value} in
                Vector.push_back symbols sym;
                loop2 (i + 1)
            in
            let%map _ = loop2 0 in
            Loadcommand.(
              Symtab {cmd; cmdsize; symoff; nsyms; stroff; strsize})
        | 0x3l ->
            (* LC_SYMSEG *)
            let%bind offset = r32 () in
            let%map size = r32 () in
            Loadcommand.(Symseg {cmd; cmdsize; offset; size})
        | 0x4l | 0x5l ->
            (* LC_THREAD/LC_UNIXTHREAD *)
            let unix = Int32.(cmd = 0x5l) in
            let%bind flavor = r32 () in
            let%bind count = r32 () in
            let n = Int32.to_int_exn count in
            let rec loop2 i res =
              if i >= n then return (List.rev res)
              else
                let%bind state = r32 () in
                loop2 (i + 1) (state :: res)
            in
            let%bind thread_state = loop2 0 [] in
            let thread_state = Array.of_list thread_state in
            let%map _ =
              (* initial instruction pointer for LC_UNIXTHREAD *)
              if unix then
                let%map _ = Option.(some_if (is_none !entry) ()) in
                match cputype with
                | Some Cputype.I386 ->
                    let eip = Int32.to_int64 thread_state.(10) in
                    entry := Some eip
                | Some Cputype.X86_64 ->
                    let rip_lo = Int32.to_int64 thread_state.(32) in
                    let rip_hi = Int32.to_int64 thread_state.(33) in
                    entry := Some Int64.(rip_lo lor (rip_hi lsl 32))
                | Some Cputype.ARM ->
                    let pc = Int32.to_int64 thread_state.(15) in
                    entry := Some pc
                | Some Cputype.ARM64 | Some Cputype.ARM64_32 ->
                    let pc_lo = Int32.to_int64 thread_state.(64) in
                    let pc_hi = Int32.to_int64 thread_state.(65) in
                    entry := Some Int64.(pc_lo lor (pc_hi lsl 32))
                | Some Cputype.POWERPC ->
                    let pc = Int32.to_int64 thread_state.(0) in
                    entry := Some pc
                | _ -> ()
              else return ()
            in
            let cmd =
              Threadcommand.{cmd; cmdsize; flavor; count; thread_state}
            in
            if unix then Loadcommand.Unixthread cmd
            else Loadcommand.Thread cmd
        | 0x6l | 0x7l ->
            (* LC_LOADFVMLIB/LC_IDFVMLIB *)
            let%bind name = r32 () in
            let%bind minor_version = r32 () in
            let%map header_addr = r32 () in
            let load = Int32.(cmd = 0x6l) in
            let cmd =
              Fvmlibcommand.{cmd; cmdsize; name; minor_version; header_addr}
            in
            if load then Loadcommand.Loadfvmlib cmd
            else Loadcommand.Idfvmlib cmd
        | 0x8l ->
            (* LC_IDENT *)
            return Loadcommand.(Ident {cmd; cmdsize})
        | 0x9l ->
            (* LC_FVMFILE *)
            let%bind name = r32 () in
            let%map header_addr = r32 () in
            Loadcommand.(Fvmfile {cmd; cmdsize; name; header_addr})
        | 0xAl ->
            (* LC_PREPAGE *)
            return Loadcommand.(Prepage {cmd; cmdsize})
        | 0xCl ->
            (* LC_LOAD_DYLIB *)
            let%bind name = r32 () in
            let%bind timestamp = r32 () in
            let%bind current_version = r32 () in
            let%map compatibility_version = r32 () in
            Loadcommand.Loaddylib
              Dylibcommand.
                { cmd
                ; cmdsize
                ; name
                ; timestamp
                ; current_version
                ; compatibility_version }
        | 0xDl ->
            (* LC_ID_DYLIB *)
            let%bind name = r32 () in
            let%bind timestamp = r32 () in
            let%bind current_version = r32 () in
            let%map compatibility_version = r32 () in
            Loadcommand.Iddylib
              Dylibcommand.
                { cmd
                ; cmdsize
                ; name
                ; timestamp
                ; current_version
                ; compatibility_version }
        | 0x10l ->
            (* LC_PREBOUND_DYLIB *)
            let%bind name = r32 () in
            let%bind nmodules = r32 () in
            let%map linked_modules = r32 () in
            Loadcommand.(
              Prebounddylib {cmd; cmdsize; name; nmodules; linked_modules})
        | 0x11l | 0x1Al ->
            let is64 = Int32.(cmd = 0x1Al) in
            let r3264 () =
              if is64 then r64 ()
              else
                let%map x = r32 () in
                Int64.of_int32 x
            in
            let%bind init_address = r3264 () in
            let%bind init_module = r3264 () in
            let%bind reserved1 = r3264 () in
            let%bind reserved2 = r3264 () in
            let%bind reserved3 = r3264 () in
            let%bind reserved4 = r3264 () in
            let%bind reserved5 = r3264 () in
            let%map reserved6 = r3264 () in
            let cmd =
              Routinescommand.
                { cmd
                ; cmdsize
                ; init_address
                ; init_module
                ; reserved1
                ; reserved2
                ; reserved3
                ; reserved4
                ; reserved5
                ; reserved6 }
            in
            if is64 then Loadcommand.Routines64 cmd
            else Loadcommand.Routines32 cmd
        | 0x12l ->
            (* LC_SUB_FRAMEWORK *)
            let%map umbrella = r32 () in
            Loadcommand.(Subframework {cmd; cmdsize; umbrella})
        | 0x13l ->
            (* LC_SUB_UMBRELLA *)
            let%map sub_umbrella = r32 () in
            Loadcommand.(Subumbrella {cmd; cmdsize; sub_umbrella})
        | 0x14l ->
            (* LC_SUB_CLIENT *)
            let%map client = r32 () in
            Loadcommand.(Subclient {cmd; cmdsize; client})
        | 0x15l ->
            (* LC_SUB_LIBRARY *)
            let%map sub_library = r32 () in
            Loadcommand.(Sublibrary {cmd; cmdsize; sub_library})
        | 0x16l ->
            (* LC_TWOLEVEL_HINTS *)
            let%bind offset = r32 () in
            let%map nhints = r32 () in
            Loadcommand.(Twolevelhints {cmd; cmdsize; offset; nhints})
        | 0x17l ->
            (* LC_PREBIND_CKSUM *)
            let%map chksum = r32 () in
            Loadcommand.(Prebindcksum {cmd; cmdsize; chksum})
        | 0x1Dl ->
            (* LC_CODE_SIGNATURE *)
            let%bind dataoff = r32 () in
            let%map datasize = r32 () in
            Loadcommand.Codesignature
              Linkeditdatacommand.{cmd; cmdsize; dataoff; datasize}
        | 0x1El ->
            (* LC_SEGMENT_SPLIT_INFO *)
            let%bind dataoff = r32 () in
            let%map datasize = r32 () in
            Loadcommand.Segmentsplitinfo
              Linkeditdatacommand.{cmd; cmdsize; dataoff; datasize}
        | 0x20l ->
            (* LC_LAZY_LOAD_DYLIB *)
            let%bind name = r32 () in
            let%bind timestamp = r32 () in
            let%bind current_version = r32 () in
            let%map compatibility_version = r32 () in
            Loadcommand.Lazyloaddylib
              Dylibcommand.
                { cmd
                ; cmdsize
                ; name
                ; timestamp
                ; current_version
                ; compatibility_version }
        | 0x24l ->
            (* LC_VERSION_MIN_MACOSX *)
            let%bind version = r32 () in
            let%map sdk = r32 () in
            Loadcommand.Versionminmacosx
              Versionmincommand.{cmd; cmdsize; version; sdk}
        | 0x25l ->
            (* LC_VERSION_MIN_IPHONEOS *)
            let%bind version = r32 () in
            let%map sdk = r32 () in
            Loadcommand.Versionminiphoneos
              Versionmincommand.{cmd; cmdsize; version; sdk}
        | 0x26l ->
            (* LC_FUNCTION_STARTS *)
            let%bind dataoff = r32 () in
            let%bind datasize = r32 () in
            let%bind _ = Binreader.seek r Int32.(to_int_exn dataoff) in
            let addr = ref !base in
            let n = Int32.to_int_exn datasize in
            let rec loop i =
              let%bind b = Binreader.peek8 r in
              if b = 0x00 || i >= n then return ()
              else
                let delta = ref 0L in
                let shift = ref 0 in
                let pos = Binreader.position r in
                let rec loop2 () =
                  let%bind b = Binreader.read8 r in
                  let b = Int.to_int64 b in
                  (delta := Int64.(!delta lor ((b land 0x7FL) lsl !shift)));
                  shift := !shift + 7;
                  if Int64.(b < 0x80L) then (
                    (addr := Int64.(!addr + !delta));
                    return !addr )
                  else loop2 ()
                in
                let%bind a = loop2 () in
                Vector.push_back function_starts a;
                loop (i + (Binreader.position r - pos))
            in
            let%map _ = loop 0 in
            Loadcommand.Functionstarts
              Linkeditdatacommand.{cmd; cmdsize; dataoff; datasize}
        | 0x29l ->
            (* LC_DATA_IN_CODE *)
            let%bind dataoff = r32 () in
            let%map datasize = r32 () in
            Loadcommand.Dataincode
              Linkeditdatacommand.{cmd; cmdsize; dataoff; datasize}
        | 0x2Bl ->
            (* LC_DYLIB_CODE_SIGN_DRS *)
            let%bind dataoff = r32 () in
            let%map datasize = r32 () in
            Loadcommand.Dylibcodesigndrs
              Linkeditdatacommand.{cmd; cmdsize; dataoff; datasize}
        | 0x2Dl ->
            (* LC_LINKER_OPTION *)
            let%map count = r32 () in
            Loadcommand.(Linkeroption {cmd; cmdsize; count})
        | 0x2El ->
            (* LC_LINKER_OPTIMIZATION_HINT *)
            let%bind dataoff = r32 () in
            let%map datasize = r32 () in
            Loadcommand.Linkeroptimizationhint
              Linkeditdatacommand.{cmd; cmdsize; dataoff; datasize}
        | 0x2Fl ->
            (* LC_VERSION_MIN_TVOS *)
            let%bind version = r32 () in
            let%map sdk = r32 () in
            Loadcommand.Versionmintvos
              Versionmincommand.{cmd; cmdsize; version; sdk}
        | 0x30l ->
            (* LC_VERSION_MIN_WATCHOS *)
            let%bind version = r32 () in
            let%map sdk = r32 () in
            Loadcommand.Versionminwatchos
              Versionmincommand.{cmd; cmdsize; version; sdk}
        | 0x80000033l ->
            (* LC_DYLD_EXPORTS_TRIE *)
            let%bind dataoff = r32 () in
            let%map datasize = r32 () in
            Loadcommand.Dyldexportstrie
              Linkeditdatacommand.{cmd; cmdsize; dataoff; datasize}
        | 0x80000034l ->
            (* LC_DYLD_CHAINED_FIXUPS *)
            let%bind dataoff = r32 () in
            let%map datasize = r32 () in
            Loadcommand.Dyldchainedfixups
              Linkeditdatacommand.{cmd; cmdsize; dataoff; datasize}
        | 0x80000018l ->
            (* LC_LOAD_WEAK_DYLIB *)
            let%bind name = r32 () in
            let%bind timestamp = r32 () in
            let%bind current_version = r32 () in
            let%map compatibility_version = r32 () in
            Loadcommand.Loadweakdylib
              Dylibcommand.
                { cmd
                ; cmdsize
                ; name
                ; timestamp
                ; current_version
                ; compatibility_version }
        | 0x8000001Cl ->
            (* LC_RPATH *)
            let%map path = r32 () in
            Loadcommand.(Rpath {cmd; cmdsize; path})
        | 0x22l | 0x80000022l ->
            (* LC_DYLD_INFO/LC_DYLD_INFO_ONLY *)
            let%bind rebase_off = r32 () in
            let%bind rebase_size = r32 () in
            let%bind bind_off = r32 () in
            let%bind bind_size = r32 () in
            let%bind weak_bind_off = r32 () in
            let%bind weak_bind_size = r32 () in
            let%bind lazy_bind_off = r32 () in
            let%bind lazy_bind_size = r32 () in
            let%bind export_off = r32 () in
            let%map export_size = r32 () in
            let only = Int32.(cmd = 0x22l) in
            let cmd =
              Dyldinfocommand.
                { cmd
                ; cmdsize
                ; rebase_off
                ; rebase_size
                ; bind_off
                ; bind_size
                ; weak_bind_off
                ; weak_bind_size
                ; lazy_bind_off
                ; lazy_bind_size
                ; export_off
                ; export_size }
            in
            if only then Loadcommand.Dyldinfoonly cmd
            else Loadcommand.Dyldinfo cmd
        | 0x8000001Fl ->
            (* LC_REEXPORT_DYLIB *)
            let%bind name = r32 () in
            let%bind timestamp = r32 () in
            let%bind current_version = r32 () in
            let%map compatibility_version = r32 () in
            Loadcommand.Reexportdylib
              Dylibcommand.
                { cmd
                ; cmdsize
                ; name
                ; timestamp
                ; current_version
                ; compatibility_version }
        | 0x80000023l ->
            (* LC_LOAD_UPWARD_DYLIB *)
            let%bind name = r32 () in
            let%bind timestamp = r32 () in
            let%bind current_version = r32 () in
            let%map compatibility_version = r32 () in
            Loadcommand.Reexportdylib
              Dylibcommand.
                { cmd
                ; cmdsize
                ; name
                ; timestamp
                ; current_version
                ; compatibility_version }
        | 0x80000028l ->
            (* LC_MAIN *)
            let%bind _ = Option.(some_if (is_none !entry) ()) in
            let%bind entryoff = r64 () in
            let%map stacksize = r64 () in
            entry :=
              Vector.find_map sections ~f:(fun sec ->
                  let offset = Int32.to_int64 sec.offset in
                  Option.some_if
                    Int64.(
                      offset <> zero && offset <= entryoff
                      && entryoff < offset + sec.size)
                    Int64.(entryoff - offset + sec.addr));
            Loadcommand.(Main {cmd; cmdsize; entryoff; stacksize})
        | _ -> return Loadcommand.(Default {cmd; cmdsize})
      in
      let%bind _ = Binreader.seek r (pos + Int32.to_int_exn cmdsize) in
      Vector.push_back load_commands cmd;
      loop (i + 1)
  in
  let%bind _ = loop 0 in
  (* this seems like a hack *)
  let hdrsize = Binreader.position r in
  let sections = Vector.to_array sections in
  let%bind relocs = parse_relocs r cputype endian sections in
  let%bind exports =
    let dyldinfo =
      Vector.find_map load_commands ~f:(function
        | Loadcommand.Dyldinfo info | Loadcommand.Dyldinfoonly info ->
            Some info
        | _ -> None)
    in
    match dyldinfo with
    | None -> return [||]
    | Some info ->
        parse_exports r
          Int64.(of_int32 info.export_off)
          Int64.(of_int32 info.export_size)
  in
  let entry = !entry in
  let symbols = Vector.to_array symbols in
  let indirect_symbols = Vector.to_array indirect_symbols in
  { is64
  ; hdrsize
  ; endian
  ; entry
  ; flags
  ; filetype
  ; cputype
  ; cpusubtype
  ; load_commands= Vector.to_array load_commands
  ; symbols
  ; indirect_symbols
  ; sections
  ; relocs
  ; exports
  ; function_starts= Vector.to_array function_starts }
  |> return

let from_bigstring_exn data =
  match from_bigstring data with
  | None -> invalid_arg "invalid Mach-O file"
  | Some mach_o -> mach_o
