open Core_kernel

val is_mach_o_file : string -> bool

val from_bigstring : Bigstring.t -> Mach_o.t option

val from_bigstring_exn : Bigstring.t -> Mach_o.t
