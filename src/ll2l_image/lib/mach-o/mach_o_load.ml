open Core_kernel
open Ll2l_arch
open Ll2l_std
open Mach_o
module M = Memory

let arch_of macho =
  match macho.cputype with
  | Some Cputype.I386 -> Arch.of_string "ia32"
  | Some Cputype.X86_64 -> Arch.of_string "amd64"
  | Some Cputype.ARM ->
      let estr = Endian.to_string macho.endian in
      let is_thumb =
        match macho.entry with
        | Some entry -> Int64.(entry land one = one)
        | None -> false
      in
      let str = if is_thumb then "thumbv7" else "armv7" in
      Arch.of_string (str ^ estr)
  | Some Cputype.ARM64_32 ->
      let estr = Endian.to_string macho.endian in
      Arch.of_string ("armv8" ^ estr)
  | None -> invalid_arg "no architecture specified"
  | Some _ -> invalid_arg "unsupported architecture"

let load_segments macho data mem =
  Array.iter macho.load_commands ~f:(function
    | Loadcommand.Segment32 seg | Loadcommand.Segment64 seg -> (
        if
          (* __PAGEZERO is used by the OS (in 64-bit mode)
           * to catch null pointer dereferences as well as
           * invalid pointers in software ported from 32-bit
           * to 64-bit, since the size is usually 0x100000000
           * bytes, meaning no 32-bit pointer can be valid. *)
          String.equal seg.name "__PAGEZERO" || Int64.(seg.filesize = zero)
        then ()
        else
          let data =
            let pos, len =
              (Int.of_int64_exn seg.fileoff, Int.of_int64_exn seg.filesize)
            in
            Bigstring.sub data ~pos ~len
          in
          let r = Int32.(seg.initprot land 0x1l <> zero) in
          let w = Int32.(seg.initprot land 0x2l <> zero) in
          let x = Int32.(seg.initprot land 0x4l <> zero) in
          let sections =
            Array.filter macho.sections ~f:(fun sec ->
                String.equal sec.segname seg.name)
          in
          if x && not (Array.is_empty sections) then
            (* same as with ELF: use the section
             * information when we come across 
             * an executable segment. this can lead
             * to better disassembly results. *)
            Array.iter sections ~f:(fun sec ->
                let offset = Int64.of_int32 sec.offset in
                if
                  Int64.(sec.size > zero)
                  && Int64.(offset >= seg.fileoff)
                  && Int64.(offset + sec.size <= seg.fileoff + seg.filesize)
                then
                  let pos = Int64.(to_int_exn (offset - seg.fileoff)) in
                  let len = Int64.to_int_exn sec.size in
                  let addr = Addr.int64 sec.addr in
                  let data = Bigstring.sub data ~pos ~len in
                  let x =
                    (* use this hint to not scan for instructions
                     * in sections that contain only data *)
                    let open Sectionattr in
                    List.mem sec.attributes SOME_INSTRUCTIONS ~equal
                    || List.mem sec.attributes PURE_INSTRUCTIONS ~equal
                  in
                  match
                    M.add_segment_from_bigstring mem addr data ~r ~w ~x
                  with
                  | Error `BadAddr ->
                      invalid_arg
                        (Printf.sprintf
                           "bad segment address 0x%016LX for section %s"
                           (Addr.to_int64 addr) sec.secname)
                  | _ -> ())
          else
            let addr = Addr.int64 seg.vmaddr in
            match M.add_segment_from_bigstring mem addr data ~r ~w ~x with
            | Error `BadAddr ->
                invalid_arg
                  (Printf.sprintf
                     "bad segment address 0x%016LX for segment %s"
                     (Addr.to_int64 addr) seg.name)
            | _ -> () )
    | _ -> ())

let collect_relocs macho =
  Array.fold macho.relocs ~init:Addr.Map.empty ~f:(fun m (reloc : Reloc.t) ->
      let reloc =
        match reloc with
        | Scattered {addr; typ; len; pcrel; value; sec} ->
            let addr = Int64.(sec.addr + of_int32 addr) |> Addr.int64 in
            Image.Reloc.
              { addr
              ; name= None
              ; size= len
              ; value= Some (Int64.of_int32 value)
              ; offset= None }
        | Default {addr; symbolnum; pcrel; len; extern; typ; sec} ->
            let addr = Int64.(sec.addr + of_int32 addr) |> Addr.int64 in
            let name =
              if extern && symbolnum <> 0 then
                Some macho.symbols.(symbolnum).name
              else None
            in
            Image.Reloc.{addr; name; size= len; value= None; offset= None}
      in
      Map.set m reloc.addr reloc)

let from_bigstring_exn data =
  let macho = Mach_o_parse.from_bigstring_exn data in
  let arch = arch_of macho in
  let mem = M.create () in
  load_segments macho data mem;
  let img =
    Image.
      { meta= MachO macho
      ; endian= macho.endian
      ; mem
      ; start= None
      ; relocs= collect_relocs macho
      ; arch
      ; canonical_values= String.Map.empty }
  in
  let validate_addr a =
    let a, arch = Arch.addr_of_arch arch a in
    Option.some_if Image.(is_valid_pc img a) (a, arch)
  in
  let start =
    match macho.entry with
    | None -> None
    | Some entry -> (
      match validate_addr (Addr.int64 entry) with
      | None -> None
      | Some (a, _) -> Some a )
  in
  {img with start}

let from_bigstring data =
  Option.try_with (fun () -> from_bigstring_exn data)
  [@@inline]
