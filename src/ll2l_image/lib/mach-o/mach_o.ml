open Core_kernel
open Ll2l_std

module Flags = struct
  type t =
    | NOUNDEFS
    | INCRLINK
    | DYLDLINK
    | BINDATLOAD
    | PREBOUND
    | SPLIT_SEGS
    | LAZY_INIT
    | TWOLEVEL
    | FORCE_FLAT
    | NOMULTIDEFS
    | NOFIXPREBINDING
    | PREBINDABLE
    | ALLMODSBOUND
    | SUBSECTIONS_VIA_SYMBOLS
    | CANONICAL
    | WEAK_DEFINES
    | BINDS_TO_WEAK
    | ALLOW_STACK_EXECUTION
    | ROOT_SAFE
    | SETUID_SAFE
    | NO_REEXPORTED_DYLIBS
    | PIE
    | DEAD_STRIPPABLE_DYLIB
    | HAS_TLV_DESCRIPTORS
    | NO_HEAP_EXECUTION
    | APP_EXTENSION_SAFE
    | Ext of int
  [@@deriving equal]
end

module Filetype = struct
  type t =
    | OBJECT
    | EXECUTE
    | FVMLIB
    | CORE
    | PRELOAD
    | DYLIB
    | DYLINKER
    | BUNDLE
    | DYLIB_STUB
    | DSYM
    | KEXT_BUNDLE
  [@@deriving equal]
end

module Cputype = struct
  type t =
    | ANY
    | VAX
    | MC680X0
    | I386
    | X86_64
    | MIPS
    | MC98000
    | HPPA
    | ARM
    | ARM64
    | ARM64_32
    | MC88000
    | SPARC
    | I860
    | ALPHA
    | POWERPC
    | POWERPC64
  [@@deriving equal]
end

module Cpusubtype = struct
  type t =
    | MULTIPLE
    | LITTLE_ENDIAN
    | BIG_ENDIAN
    | VAX_ALL
    | VAX780
    | VAX785
    | VAX750
    | VAX730
    | UVAXI
    | UVAXII
    | VAX8200
    | VAX8500
    | VAX8600
    | VAX8650
    | VAX8800
    | UVAXIII
    | MC680X0_ALL
    | MC68030
    | MC68040
    | MC68030_ONLY
    | I386_ALL
    | I386
    | I486
    | I486SX
    | I586
    | PENT
    | PENTPRO
    | PENTII_M3
    | PENTII_M5
    | CELERON
    | CELERON_MOBILE
    | PENTIUM_3
    | PENTIUM_3_M
    | PENTIUM_3_XEON
    | PENTIUM_M
    | PENTIUM_4
    | PENTIUM_4_M
    | ITANIUM
    | ITANIUM_2
    | XEON
    | XEON_MP
    | INTEL_MODEL_ALL
    | X86_ALL
    | X86_64_ALL
    | X86_ARCH1
    | X86_64_H
    | MIPS_ALL
    | MIPS_R2300
    | MIPS_R2600
    | MIPS_R2800
    | MIPS_R2000A
    | MIPS_R2000
    | MIPS_R3000A
    | MIPS_R3000
    | MC98000_ALL
    | MC98601
    | HPPA_ALL
    | HPPA_7100
    | HPPA_7100LC
    | MC88000_ALL
    | MC88100
    | MC88110
    | SPARC_ALL
    | I860_ALL
    | I860_860
    | POWERPC_ALL
    | POWERPC_601
    | POWERPC_602
    | POWERPC_603
    | POWERPC_603E
    | POWERPC_603EV
    | POWERPC_604
    | POWERPC_604E
    | POWERPC_620
    | POWERPC_750
    | POWERPC_7400
    | POWERPC_7450
    | POWERPC_970
    | ARM_ALL
    | ARM_V4T
    | ARM_V6
    | ARM_V5TEJ
    | ARM_XSCALE
    | ARM_V7
    | ARM_V7F
    | ARM_V7S
    | ARM_V7K
    | ARM_V6M
    | ARM_V7M
    | ARM_V7EM
    | ARM_V8
    | ARM64_ALL
    | ARM64_V8
    | ARM64_E
    | ARM64_32_ALL
    | ARM64_32_V8
  [@@deriving equal]
end

module Sectiontype = struct
  type t =
    | REGULAR
    | ZEROFILL
    | CSTRING_LITERALS
    | FOURBYTE_LITERALS
    | EIGHTBYTE_LITERALS
    | LITERAL_POINTERS
    | NON_LAZY_SYMBOL_POINTERS
    | LAZY_SYMBOL_POINTERS
    | SYMBOL_STUBS
    | MOD_INIT_FUNC_POINTERS
    | MOD_TERM_FUNC_POINTERS
    | COALESCED
    | GB_ZEROFILL
    | INTERPOSING
    | SIXTEENBYTE_LITERALS
    | DTRACE_DOF
    | LAZY_DYLIB_SYMBOL_POINTERS
    | THREAD_LOCAL_REGULAR
    | THREAD_LOCAL_ZEROFILL
    | THREAD_LOCAL_VARIABLES
    | THREAD_LOCAL_VARIABLE_POINTERS
    | THREAD_LOCAL_INIT_FUNCTION_POINTERS
  [@@deriving equal]
end

module Sectionattr = struct
  type t =
    | PURE_INSTRUCTIONS
    | NO_TOC
    | STRIP_STATIC_SYMS
    | NO_DEAD_STRIP
    | LIVE_SUPPORT
    | SELF_MODIFYING_CODE
    | DEBUG
    | SOME_INSTRUCTIONS
    | EXT_RELOC
    | LOC_RELOC
    | Ext of int
  [@@deriving equal]
end

module Section = struct
  type t =
    { secname: string
    ; segname: string
    ; addr: int64
    ; size: int64
    ; offset: int32
    ; align: int32
    ; reloff: int32
    ; nreloc: int32
    ; typ: Sectiontype.t option
    ; attributes: Sectionattr.t list }

  let has_attr t attr =
    List.mem t.attributes attr ~equal:Sectionattr.equal
    [@@inline]
end

module Segmentcommand = struct
  type t =
    { cmd: int32
    ; cmdsize: int32
    ; name: string
    ; vmaddr: int64
    ; vmsize: int64
    ; fileoff: int64
    ; filesize: int64
    ; maxprot: int32
    ; initprot: int32
    ; nsects: int32
    ; flags: int32 }
end

module Threadcommand = struct
  type t =
    { cmd: int32
    ; cmdsize: int32
    ; flavor: int32
    ; count: int32
    ; thread_state: int32 array }
end

module Fvmlibcommand = struct
  type t =
    { cmd: int32
    ; cmdsize: int32
    ; name: int32
    ; minor_version: int32
    ; header_addr: int32 }
end

module Dylibcommand = struct
  type t =
    { cmd: int32
    ; cmdsize: int32
    ; name: int32
    ; timestamp: int32
    ; current_version: int32
    ; compatibility_version: int32 }
end

module Dylinkercommand = struct
  type t = {cmd: int32; cmdsize: int32; name: int32}
end

module Routinescommand = struct
  type t =
    { cmd: int32
    ; cmdsize: int32
    ; init_address: int64
    ; init_module: int64
    ; reserved1: int64
    ; reserved2: int64
    ; reserved3: int64
    ; reserved4: int64
    ; reserved5: int64
    ; reserved6: int64 }
end

module Linkeditdatacommand = struct
  type t = {cmd: int32; cmdsize: int32; dataoff: int32; datasize: int32}
end

module Encryptioninfocommand = struct
  type t =
    { cmd: int32
    ; cmdsize: int32
    ; cryptoff: int32
    ; cryptsize: int32
    ; cryptid: int32 }
end

module Dyldinfocommand = struct
  type t =
    { cmd: int32
    ; cmdsize: int32
    ; rebase_off: int32
    ; rebase_size: int32
    ; bind_off: int32
    ; bind_size: int32
    ; weak_bind_off: int32
    ; weak_bind_size: int32
    ; lazy_bind_off: int32
    ; lazy_bind_size: int32
    ; export_off: int32
    ; export_size: int32 }
end

module Versionmincommand = struct
  type t = {cmd: int32; cmdsize: int32; version: int32; sdk: int32}
end

module Loadcommand = struct
  type t =
    | Segment32 of Segmentcommand.t
    | Segment64 of Segmentcommand.t
    | Uuid of {cmd: int32; cmdsize: int32; uuid: bytes}
    | Symtab of
        { cmd: int32
        ; cmdsize: int32
        ; symoff: int32
        ; nsyms: int32
        ; stroff: int32
        ; strsize: int32 }
    | Symseg of {cmd: int32; cmdsize: int32; offset: int32; size: int32}
    | Thread of Threadcommand.t
    | Unixthread of Threadcommand.t
    | Loadfvmlib of Fvmlibcommand.t
    | Idfvmlib of Fvmlibcommand.t
    | Ident of {cmd: int32; cmdsize: int32}
    | Fvmfile of {cmd: int32; cmdsize: int32; name: int32; header_addr: int32}
    | Prepage of {cmd: int32; cmdsize: int32}
    | Dysymtab of
        { cmd: int32
        ; cmdsize: int32
        ; ilocalsym: int32
        ; nlocalsym: int32
        ; iextdefsym: int32
        ; nextdefsym: int32
        ; iundefsym: int32
        ; nundefsym: int32
        ; tocoff: int32
        ; ntoc: int32
        ; modtaboff: int32
        ; nmodtab: int32
        ; extrefsymoff: int32
        ; nextrefsyms: int32
        ; indirectsymoff: int32
        ; nindirectsyms: int32
        ; extreloff: int32
        ; nextrel: int32
        ; locreloff: int32
        ; nlocrel: int32 }
    | Loaddylib of Dylibcommand.t
    | Iddylib of Dylibcommand.t
    | Loaddylinker of Dylinkercommand.t
    | Iddylinker of Dylinkercommand.t
    | Prebounddylib of
        { cmd: int32
        ; cmdsize: int32
        ; name: int32
        ; nmodules: int32
        ; linked_modules: int32 }
    | Routines32 of Routinescommand.t
    | Routines64 of Routinescommand.t
    | Subframework of {cmd: int32; cmdsize: int32; umbrella: int32}
    | Subumbrella of {cmd: int32; cmdsize: int32; sub_umbrella: int32}
    | Subclient of {cmd: int32; cmdsize: int32; client: int32}
    | Sublibrary of {cmd: int32; cmdsize: int32; sub_library: int32}
    | Twolevelhints of
        {cmd: int32; cmdsize: int32; offset: int32; nhints: int32}
    | Prebindcksum of {cmd: int32; cmdsize: int32; chksum: int32}
    | Loadweakdylib of Dylibcommand.t
    | Rpath of {cmd: int32; cmdsize: int32; path: int32}
    | Codesignature of Linkeditdatacommand.t
    | Segmentsplitinfo of Linkeditdatacommand.t
    | Reexportdylib of Dylibcommand.t
    | Lazyloaddylib of Dylibcommand.t
    | Encryptioninfo32 of Encryptioninfocommand.t
    | Encryptioninfo64 of Encryptioninfocommand.t
    | Dyldinfo of Dyldinfocommand.t
    | Dyldinfoonly of Dyldinfocommand.t
    | Loadupwarddylib of Dylibcommand.t
    | Versionminmacosx of Versionmincommand.t
    | Versionminiphoneos of Versionmincommand.t
    | Functionstarts of Linkeditdatacommand.t
    | Dyldenvironment of Dylinkercommand.t
    | Main of {cmd: int32; cmdsize: int32; entryoff: int64; stacksize: int64}
    | Dataincode of Linkeditdatacommand.t
    | Sourceversion of {cmd: int32; cmdsize: int32; version: int64}
    | Dylibcodesigndrs of Linkeditdatacommand.t
    | Linkeroption of {cmd: int32; cmdsize: int32; count: int32}
    | Linkeroptimizationhint of Linkeditdatacommand.t
    | Versionmintvos of Versionmincommand.t
    | Versionminwatchos of Versionmincommand.t
    | Dyldexportstrie of Linkeditdatacommand.t
    | Dyldchainedfixups of Linkeditdatacommand.t
    | Default of {cmd: int32; cmdsize: int32}
end

module Symboltype = struct
  type t = UNDF | ABS | SECT | INDR [@@deriving equal]
end

module Symbol = struct
  type t =
    { name: string
    ; typ: Symboltype.t option
    ; sec: Section.t option
    ; value: int64 }
end

module Reloc = struct
  type generic =
    | GENERIC_VANILLA
    | GENERIC_PAIR
    | GENERIC_SECTDIFF
    | GENERIC_PB_LA_PTR
    | GENERIC_LOCAL_SECTDIFF
    | GENERIC_TLV

  type x86_64 =
    | X86_64_UNSIGNED
    | X86_64_SIGNED
    | X86_64_BRANCH
    | X86_64_GOT_LOAD
    | X86_64_GOT
    | X86_64_SUBTRACTOR
    | X86_64_SIGNED_1
    | X86_64_SIGNED_2
    | X86_64_SIGNED_4
    | X86_64_TLV

  type ppc =
    | PPC_VANILLA
    | PPC_PAIR
    | PPC_BR14
    | PPC_BR24
    | PPC_HI16
    | PPC_LO16
    | PPC_HA16
    | PPC_LO14
    | PPC_SECTDIFF
    | PPC_PB_LA_PTR
    | PPC_HI16_SECTDIFF
    | PPC_LO16_SECTDIFF
    | PPC_HA16_SECTDIFF
    | PPC_JBSR
    | PPC_LO14_SECTDIFF
    | PPC_LOCAL_SECTDIFF

  type arm =
    | ARM_VANILLA
    | ARM_PAIR
    | ARM_SECTDIFF
    | ARM_LOCAL_SECTDIFF
    | ARM_PB_LA_PTR
    | ARM_BR24
    | ARM_THUMB_BR22
    | ARM_THUMB_32BIT_BRANCH
    | ARM_HALF
    | ARM_HALF_SECTDIFF

  type arm64 =
    | ARM64_UNSIGNED
    | ARM64_SUBTRACTOR
    | ARM64_BRANCH26
    | ARM64_PAGE21
    | ARM64_PAGEOFF12
    | ARM64_GOT_LOAD_PAGE21
    | ARM64_GOT_LOAD_PAGEOFF12
    | ARM64_POINTER_TO_GOT
    | ARM64_TLVP_LOAD_PAGE21
    | ARM64_TLVP_LOAD_PAGEOFF12
    | ARM64_ADDEND

  type typ =
    | I386 of generic
    | X86_64 of x86_64
    | Ppc of ppc
    | Arm of arm
    | Arm64 of arm64

  type t =
    | Scattered of
        { addr: int32
        ; typ: typ option
        ; len: int
        ; pcrel: bool
        ; value: int32
        ; sec: Section.t }
    | Default of
        { addr: int32
        ; symbolnum: int
        ; pcrel: bool
        ; len: int
        ; extern: bool
        ; typ: typ option
        ; sec: Section.t }
end

module Export = struct
  type t = {symbolname: string; addr: int64}
end

type t =
  { is64: bool
  ; hdrsize: int
  ; endian: Endian.t
  ; entry: int64 option
  ; flags: Flags.t list
  ; filetype: Filetype.t option
  ; cputype: Cputype.t option
  ; cpusubtype: Cpusubtype.t option
  ; load_commands: Loadcommand.t array
  ; symbols: Symbol.t array
  ; indirect_symbols: Symbol.t array
  ; sections: Section.t array
  ; relocs: Reloc.t array
  ; exports: Export.t array
  ; function_starts: int64 array }

let has_flag t flg = List.mem t.flags flg ~equal:Flags.equal [@@inline]

let base_address t =
  Array.fold t.load_commands ~init:0L ~f:(fun addr cmd ->
      match cmd with
      | (Loadcommand.Segment32 seg | Loadcommand.Segment64 seg)
        when Int64.(seg.fileoff = zero && seg.filesize <> zero) -> seg.vmaddr
      | _ -> addr)
