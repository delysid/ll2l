open Core_kernel

val load_exn : string -> Image.t

val load : string -> Image.t option
