open Core_kernel
open Ll2l_arch
open Ll2l_std

module Meta = struct
  type t = PE of Pe.t | ELF of Elf.t | MachO of Mach_o.t
end

module Reloc = struct
  type t =
    { addr: Addr.t
    ; size: int
    ; value: int64 option
    ; name: string option
    ; offset: int64 option }

  let within rel addr =
    Addr.(addr >= rel.addr && addr < rel.addr + int rel.size)
    [@@inline]

  (* check if the reloc is subsumed by the range (addr, endaddr] *)
  let within2 rel ~addr ~endaddr =
    Addr.(
      rel.addr > addr && rel.addr < endaddr
      && rel.addr + int rel.size <= endaddr)
    [@@inline]
end

type canonical_value = {value: Bitvec.t; size: int}

type t =
  { meta: Meta.t
  ; endian: Endian.t
  ; mem: Memory.t
  ; start: Addr.t option
  ; relocs: Reloc.t Addr.Map.t
  ; arch: Arch.et
  ; canonical_values: canonical_value String.Map.t }

let total_header_size img =
  match img.meta with
  | PE pe -> Pe.total_header_size pe
  | ELF elf -> Elf.total_header_size elf
  | MachO macho -> macho.hdrsize

let virtual_base img =
  match img.meta with
  | PE pe -> Addr.int64 Pe.(pe.image_base) |> Option.return
  | ELF elf -> Elf.virtual_base elf |> Option.map ~f:Addr.int64
  | MachO macho -> Mach_o.base_address macho |> Addr.int64 |> Option.return

let find_intersecting_reloc img addr =
  let open Option.Let_syntax in
  let%bind _, rel = Map.closest_key img.relocs `Less_or_equal_to addr in
  Option.some_if Reloc.(within rel addr) rel
  [@@inline]

let is_valid_pc ?(check_seg = true) ?(check_reloc = true) ?(arch = None) img
    addr =
  let Arch.(T arch) =
    match arch with
    | None -> img.arch
    | Some arch -> arch
  in
  let min_len = Arch.min_instr_length arch in
  Addr.(rem addr (int min_len) = zero)
  && ( (not check_seg)
     ||
     match Memory.find_segment img.mem addr with
     | None -> false
     | Some seg -> Memory.Segment.is_executable seg )
  && ((not check_reloc) || find_intersecting_reloc img addr |> Option.is_none)

let pe_entries (pe : Pe.t) =
  let exports =
    match pe.data_directory.export with
    | Some (_, dir) ->
        Array.to_list dir.exports
        |> List.map ~f:(fun (e : Pe.Export.t) ->
               Pe.va_of_rva pe e.virtual_address )
    | None -> []
  in
  let sym_functions =
    Array.to_list pe.symbols
    |> List.filter_map ~f:(fun (sym : Pe.Symbol.t) ->
           match sym.derived_type with
           | Some Pe.Symbol.DT_FUNCTION -> Some (Pe.va_of_rva pe sym.value)
           | _ -> None )
  in
  let unwind_info =
    match pe.data_directory.exceptions with
    | None -> []
    | Some (_, dir) ->
        List.concat_map (Array.to_list dir.functions) ~f:(fun f ->
            let addr = Pe.va_of_rva pe f.begin_address in
            let unwinds =
              match f.unwind_info.extra with
              | Some (Pe.Unwind_info.Handler h) ->
                  let handler = Pe.va_of_rva pe h.exception_handler in
                  let scope =
                    List.concat_map (Array.to_list h.scope_table)
                      ~f:(fun s ->
                        let beg = Pe.va_of_rva pe s.begin_address in
                        let end' = Pe.va_of_rva pe s.end_address in
                        let handler = Pe.va_of_rva pe s.handler_address in
                        let jmp = Pe.va_of_rva pe s.jump_target in
                        [beg; end'; handler; jmp] )
                  in
                  handler :: scope
              | _ -> []
            in
            addr :: unwinds )
  in
  let tls_callbacks =
    match pe.data_directory.tls with
    | None -> []
    | Some (_, dir) -> dir.callbacks |> Array.to_list
  in
  exports @ sym_functions @ unwind_info @ tls_callbacks

let elf_entries img (elf : Elf.t) =
  let sym_functions =
    Array.to_list elf.symbols
    |> List.filter_map ~f:(fun (sym : Elf.Symbol.t) ->
           match sym.typ with
           | Elf.Symbol.T_FUNC -> Some sym.value
           | _ -> None )
  in
  let landing_pads =
    match elf.eh_frame with
    | None -> []
    | Some eh_frame ->
        Map.fold eh_frame.cies ~init:[] ~f:(fun ~key:_ ~data:cie acc ->
            Map.fold cie.fdes ~init:acc ~f:(fun ~key:_ ~data:fde acc ->
                let acc =
                  match fde.augmentation.personality with
                  | None -> acc
                  | Some per -> per.personality_ptr :: acc
                in
                let acc = fde.pc_begin :: acc in
                match fde.lsda with
                | None -> acc
                | Some lsda ->
                    Array.fold lsda.call_site_table ~init:acc
                      ~f:(fun acc call_site -> call_site.landing_pad :: acc) ) )
  in
  let reloc_values =
    Map.to_alist img.relocs
    |> List.filter_map ~f:(fun (_, rel) -> Reloc.(rel.value))
  in
  sym_functions @ landing_pads @ reloc_values

let macho_entries (macho : Mach_o.t) =
  let sym_functions =
    Array.to_list macho.symbols @ Array.to_list macho.indirect_symbols
    |> List.filter_map ~f:(fun (sym : Mach_o.Symbol.t) ->
           Option.(
             sym.sec
             >>= fun sec ->
             let open Mach_o.Sectionattr in
             (* this is a guess; if the symbol belongs to a
              * section with code, then it may be a function *)
             some_if
               ( List.mem sec.attributes PURE_INSTRUCTIONS ~equal
               || List.mem sec.attributes SOME_INSTRUCTIONS ~equal )
               sym.value) )
  in
  let function_starts = Array.to_list macho.function_starts in
  let exports =
    let base = Mach_o.base_address macho in
    Array.to_list macho.exports
    |> List.map ~f:(fun (exp : Mach_o.Export.t) -> Int64.(exp.addr + base))
  in
  sym_functions @ function_starts @ exports

let entries img =
  let validate_addr a =
    let a, arch = Arch.addr_of_arch img.arch a in
    Option.some_if (is_valid_pc img a) (a, arch)
  in
  let entries =
    match img.meta with
    | PE pe -> pe_entries pe
    | ELF elf -> elf_entries img elf
    | MachO macho -> macho_entries macho
  in
  let entries =
    List.map entries ~f:Addr.int64
    |> List.dedup_and_sort ~compare:Addr.compare
    |> List.filter_map ~f:validate_addr
    |> Addr.Map.of_alist_exn
  in
  match img.start with
  | None -> entries
  | Some start -> Map.set entries start img.arch

let code_pointers img =
  let Arch.(T arch) = img.arch in
  let word_sz = Arch.word_size_of arch in
  (* word size must be a power of 2, between 1 and 8 bytes long *)
  assert (word_sz land pred word_sz = 0);
  assert (word_sz >= 8 && word_sz <= 64);
  let m = Addr.Map.empty in
  let read a =
    match word_sz with
    | 64 -> (
      match Memory.read_int64 img.mem a img.endian with
      | Ok v -> Some v
      | Error _ -> None )
    | 32 -> (
      match Memory.read_int32 img.mem a img.endian with
      | Ok v -> Some Int64.(of_int32 v land 0xFFFFFFFFL)
      | Error _ -> None )
    | 16 -> (
      match Memory.read_int16 img.mem a img.endian with
      | Ok v -> Some Int64.(of_int v land 0xFFFFL)
      | Error _ -> None )
    | 8 -> (
      match Memory.read_byte img.mem a with
      | Ok v -> Some Int64.(of_int v land 0xFFL)
      | Error _ -> None )
    | _ -> None
  in
  List.fold (Memory.address_ranges img.mem) ~init:m ~f:(fun m (a, a') ->
      let end' = Addr.(a' - int word_sz) in
      let rec aux m a =
        if Addr.(a >= end') then m
        else
          let next = Addr.succ a in
          let m =
            match read a with
            | None -> m
            | Some v ->
                let v = Addr.int64 v in
                let v, arch' = Arch.addr_of_arch img.arch v in
                if is_valid_pc img v then Map.set m v arch' else m
          in
          aux m next
      in
      aux m a )

(* the .MIPS.stubs section contains stub functions for the
 * dynamic linker, and the RA register is usually saved in T7.
 * this gets complicated since a JALR instruction is used,
 * which gets interpreted as a call.
 *
 * however, these stubs in and of themselves are not noreturn
 * functions, but we still have to remove the return edge from
 * the call, since the return to caller is non-local.
 *
 * for calls to known noreturn library functions, we can
 * associate the start of the stub with a dynamic symbol *)
let is_sysv_mips_elf_stub img addr =
  match (img.arch, img.meta) with
  | Arch.(T (MIPS _)), Meta.ELF elf -> (
    match elf.osabi with
    | Elf.Osabi.SYSV -> (
        let addr = Addr.to_int64 addr in
        match Elf.section_of_vaddr elf addr with
        | Some sec when String.equal sec.name ".MIPS.stubs" -> true
        | _ -> false )
    | _ -> false )
  | _ -> false

(* similar to .MIPS.stubs *)
let is_sysv_riscv_elf_stub img addr =
  match (img.arch, img.meta) with
  | Arch.(T (RISCV _)), Meta.ELF elf -> (
    match elf.osabi with
    | Elf.Osabi.SYSV ->
        let addr = Addr.to_int64 addr in
        Map.mem elf.plt addr
    | _ -> false )
  | _ -> false

let is_stub_addr img addr =
  is_sysv_mips_elf_stub img addr || is_sysv_riscv_elf_stub img addr

let is_known_noreturn ?(instr = None) ?(indirect = false) img ~dst =
  match img.meta with
  | Meta.PE pe when indirect -> (
      let is_noreturn_name = function
        | "abort"
         |"exit"
         |"_Exit"
         |"_exit"
         |"__fastfail"
         |"ExitProcess"
         |"ExitThread"
         |"RaiseException" -> true
        | _ -> false
      in
      match pe.file_header.machine with
      | Some Pe.File_header.I386 -> (
        (* we need to look at the actual instruction
         * to obtain the corresponding relocation info. *)
        match instr with
        | Some (src, Arch.(I (X86 X86.Mode.IA32, instr))) -> (
          match X86.Instruction.(instr.displacement) with
          | None -> false
          | Some {offset} -> (
            match Map.find img.relocs Addr.(src + int offset) with
            (* the address of the displacement may be a relocation *)
            | Some {offset= Some offset} -> (
                let rel_addr =
                  Int64.(pe.image_base + offset) |> Addr.int64
                in
                match Map.find img.relocs rel_addr with
                | Some {name= Some name} -> is_noreturn_name name
                | _ -> false )
            (* the displacement itself is a pointer to a relocation *)
            | _ -> (
              match Map.find img.relocs dst with
              | Some {name= Some name} -> is_noreturn_name name
              | _ -> false ) ) )
        | _ -> false )
      | Some Pe.File_header.AMD64 -> (
        match Map.find img.relocs dst with
        | Some {name= Some name} -> is_noreturn_name name
        | _ -> false )
      | _ -> false )
  | Meta.PE _ -> false
  | Meta.ELF elf -> (
      let is_noreturn_name = function
        | "abort"
         |"exit"
         |"_Exit"
         |"_exit"
         |"__assert_fail"
         |"__assert_fail_base"
         |"__stack_chk_fail"
         |"_Unwind_resume"
         |"longjmp"
         |"_longjmp"
         |"__libc_fatal"
         |"__fortify_fail"
         |"__fortify_fail_abort"
         |"pthread_exit"
         |"__pthread_unwind_next"
         |"__pthread_unwind"
         |"__libc_start_main"
         |"__chk_fail"
         |"__exit_thread"
         |"obstack_alloc_failed_handler" -> true
        | _ -> false
      in
      if not indirect then
        match Map.find elf.plt (Addr.to_int64 dst) with
        | None -> (
            if
              (* only consider function symbols if the binary is
               * statically linked. if this is the case, then the dynamic
               * interpreter won't be present in the program headers *)
              Array.exists elf.segments ~f:(fun seg ->
                  Elf.Segment.(equal_typ seg.typ T_INTERP) )
            then
              (* MIPS SysV ELF binaries don't use a .plt section *)
              if is_sysv_mips_elf_stub img dst then
                match
                  Array.find elf.symbols ~f:(fun sym ->
                      Addr.(dst = int64 sym.addr) )
                with
                | None -> false
                | Some sym -> is_noreturn_name sym.name
              else false
            else
              match
                Array.find elf.symbols ~f:(fun sym ->
                    match sym.typ with
                    | Elf.Symbol.T_FUNC -> Addr.(dst = int64 sym.value)
                    | _ -> false )
              with
              | None -> false
              | Some sym -> is_noreturn_name sym.name )
        | Some sym -> is_noreturn_name sym.name
      else
        match Map.find img.relocs dst with
        | Some {name= Some name} when not (String.is_empty name) ->
            is_noreturn_name name
        | _ -> (
          (* XXX: avoid doing a linear search *)
          match
            Array.find elf.symbols ~f:(fun Elf.Symbol.{addr} ->
                Addr.(dst = int64 addr) )
          with
          | Some {name} -> is_noreturn_name name
          | _ -> false ) )
  | Meta.MachO macho -> (
      let is_noreturn_name = function
        | "_exit" | "__exit" | "___stack_chk_fail" -> true
        | _ -> false
      in
      let stub_sym (sym : Mach_o.Symbol.t) =
        match sym.sec with
        | Some {typ= Some Mach_o.Sectiontype.SYMBOL_STUBS} ->
            Addr.(dst = int64 sym.value)
        | _ -> false
      in
      match Array.find macho.indirect_symbols ~f:stub_sym with
      | None -> false
      | Some sym -> is_noreturn_name sym.name )
