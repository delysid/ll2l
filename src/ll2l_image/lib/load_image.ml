open Core_kernel
open Stdio

let load_exn filename =
  let f =
    if Sys.file_exists filename then
      if Elf_parse.is_elf_file filename then Elf_load.from_bigstring_exn
      else if Pe_parse.is_pe_file filename then Pe_load.from_bigstring_exn
      else if Mach_o_parse.is_mach_o_file filename then
        Mach_o_load.from_bigstring_exn
      else invalid_arg "unrecognized file type"
    else invalid_arg "file does not exist"
  in
  f @@ Bigstring.of_string @@ In_channel.read_all filename

let load filename = Option.try_with (fun () -> load_exn filename)
