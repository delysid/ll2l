# `ll2l-image`

This library provides front-end support for the following executable formats:

- PE
- ELF
- Mach-O

Each front-end provides a parser for the file format in question, as well as a loader for mapping the key components of the file into a virtual memory space.
The main module `Image` provides a generic data structure which encapsulates the key components of an image file, such as entry points, memory segments, relocations, and so on.

Future support for raw image files is desired, where users must then provide the necessary metadata for loading the file into memory.
