open Core_kernel
open Stdio
open Ll2l_image

let elf_addr_str ~a = function
  | `ELF32 -> Printf.sprintf "0x%08LX" a
  | `ELF64 -> Printf.sprintf "0x%016LX" a

let pe_addr_str ~a = function
  | `PE32 -> Printf.sprintf "0x%08LX" a
  | `PE64 -> Printf.sprintf "0x%016LX" a

let macho_addr_str ~a = function
  | true -> Printf.sprintf "0x%016LX" a
  | false -> Printf.sprintf "0x%08LX" a

let () =
  let filename = Sys.argv.(1) in
  if Elf_parse.is_elf_file filename then (
    printf "elf file '%s':\n\n" filename;
    let data = Bigstring.of_string (In_channel.read_all filename) in
    let elf = Elf_parse.from_bigstring_exn data in
    let elf_addr_str = elf_addr_str elf.eclass in
    ( match Elf.virtual_base elf with
    | Some a -> printf "virtual base = %s\n" (elf_addr_str ~a)
    | None -> () );
    printf "e_entry = %s\ne_ehsize = 0x%X\n\n"
      (elf_addr_str ~a:elf.entry)
      elf.ehsize;
    Array.iteri elf.segments ~f:(fun i seg ->
        printf "segment %d:\np_offset = %s\np_vaddr = %s\np_paddr = %s\n\n" i
          (elf_addr_str ~a:seg.offset)
          (elf_addr_str ~a:seg.vaddr)
          (elf_addr_str ~a:seg.paddr));
    Array.iter elf.sections ~f:(fun sec ->
        printf
          "section '%s':\nsh_addr = %s\nsh_offset = %s\nsh_size = 0x%LX\n\n"
          sec.name
          (elf_addr_str ~a:sec.addr)
          (elf_addr_str ~a:sec.offset)
          sec.size);
    Array.iter elf.symbols ~f:(fun sym ->
        printf
          "symbol '%s':\n\
           st_value = 0x%LX\n\
           st_size = 0x%LX\n\
           st_shndx = %d\n\
           sec name = %s\n\
           addr = %s\n\n"
          sym.name sym.value sym.size sym.shndx
          (try elf.sections.(sym.shndx).name with _ -> "")
          (elf_addr_str ~a:sym.addr));
    Array.iteri elf.relocs ~f:(fun i rel ->
        printf "reloc %d:\nr_offset = 0x%LX\n" i rel.offset;
        ( match rel.addend with
        | Some a -> printf "r_addend = 0x%LX\n" a
        | None -> () );
        match rel.symbol with
        | Some sym -> printf "r_symbol.st_name = '%s'\n\n" sym.name
        | None -> printf "\n");
    Array.iteri elf.dynamic ~f:(fun i dyn ->
        printf "dynamic %d:\nvalue = 0x%LX\n\n" i dyn.value);
    ( match elf.eh_frame with
    | None -> ()
    | Some eh_frame ->
        let dwptr Elf.Dwarf_pointer.{address; indirect} =
          Printf.sprintf "0x%LX%s" address
            (if indirect then " (indirect)" else "")
        in
        printf
          "eh_frame:\n\n\
           version = 0x%X\n\
           frame_encoding = 0x%X\n\
           fde_count_encoding = 0x%X\n\
           lookup_table_encoding = 0x%X\n\
           eh_pointer = %s\n\
           fde_count_pointer = %s\n\n"
          eh_frame.version eh_frame.frame_encoding
          eh_frame.fde_count_encoding eh_frame.lookup_table_encoding
          (dwptr eh_frame.eh_pointer)
          (dwptr eh_frame.fde_count_pointer);
        printf "cies:\n\n";
        Map.iter eh_frame.cies
          ~f:(fun
               Elf.Cie.
                 { offset
                 ; version
                 ; augmentation_string
                 ; eh_data
                 ; code_alignment_factor
                 ; data_alignment_factor
                 ; return_address_register
                 ; fdes }
             ->
            printf
              "offset = 0x%LX\n\
               version = 0x%X\n\
               augmentation_string = %s\n\
               eh_data = %s\n\
               code_alignment_factor = 0x%LX\n\
               data_alignment_factor = 0x%LX\n\
               return_address_register = 0x%X\n"
              offset version augmentation_string
              Option.(
                value ~default:"(none)"
                  (eh_data >>| fun eh_data -> Printf.sprintf "0x%LX" eh_data))
              code_alignment_factor data_alignment_factor
              return_address_register;
            printf "fdes =\n";
            Map.iteri fdes
              ~f:(fun
                   ~key
                   ~data:
                     Elf.Fde.
                       { augmentation
                       ; pc_begin_pointer
                       ; pc_begin
                       ; pc_range_pointer
                       ; pc_range
                       ; augmentation_length
                       ; lsda_pointer
                       ; lsda }
                 ->
                printf "  index = 0x%LX\n" key;
                printf "  augmentation =\n";
                printf "    length = %s\n"
                  Option.(
                    value ~default:"(none)"
                      ( augmentation.length
                      >>| fun length -> Printf.sprintf "0x%LX" length ));
                printf "    lsda_pointer_encoding = %s\n"
                  Option.(
                    value ~default:"(none)"
                      ( augmentation.lsda_pointer_encoding
                      >>| fun enc -> Printf.sprintf "0x%X" enc ));
                printf "    personality = %s\n"
                  Option.(
                    value ~default:"(none)"
                      ( augmentation.personality
                      >>| fun p ->
                      Printf.sprintf
                        "{encoding = 0x%X; pointer = %s; personality_ptr = \
                         0x%LX}"
                        p.encoding (dwptr p.pointer) p.personality_ptr ));
                printf "    fde_pointer_encoding = %s\n"
                  Option.(
                    value ~default:"(none)"
                      ( augmentation.fde_pointer_encoding
                      >>| fun enc -> Printf.sprintf "0x%X" enc ));
                printf "  pc_begin_pointer = %s\n" (dwptr pc_begin_pointer);
                printf "  pc_begin = 0x%LX\n" pc_begin;
                printf "  pc_range_pointer = %s\n" (dwptr pc_range_pointer);
                printf "  pc_range = 0x%LX\n" pc_range;
                printf "  augmentation_length = %s\n"
                  Option.(
                    value ~default:"(none)"
                      ( augmentation_length
                      >>| fun len -> Printf.sprintf "0x%LX" len ));
                printf "  lsda_pointer = %s\n"
                  Option.(
                    value ~default:"(none)"
                      (lsda_pointer >>| fun p -> dwptr p));
                printf "\n");
            printf "\n") );
    printf "\nplt:\n\n";
    Map.iteri elf.plt ~f:(fun ~key ~data ->
        printf "%s:\n" (elf_addr_str ~a:key);
        printf "  name = %s, addr = %s\n" data.name
          (elf_addr_str ~a:data.addr));
    printf "\n" )
  else if Pe_parse.is_pe_file filename then (
    printf "pe file '%s':\n\n" filename;
    let data = Bigstring.of_string (In_channel.read_all filename) in
    let pe = Pe_parse.from_bigstring_exn data in
    let pe_addr_str = pe_addr_str pe.kind in
    printf "entry point = %s\n"
      (pe_addr_str ~a:(Int64.of_int32 pe.address_of_entry_point));
    printf "image base = %s\n" (pe_addr_str ~a:pe.image_base);
    printf "\n";
    Array.iter pe.sections ~f:(fun sec ->
        printf
          "section '%s'\n\
           virtual size = 0x%lX\n\
           virtual address = %s\n\
           readable = %s\n\
           writable = %s\n\
           executable = %s\n\n"
          sec.name sec.virtual_size
          (pe_addr_str ~a:(Int64.of_int32 sec.virtual_address))
          (Bool.to_string
             (Pe.Section.has_characteristic sec Pe.Section.MEM_READ))
          (Bool.to_string
             (Pe.Section.has_characteristic sec Pe.Section.MEM_WRITE))
          (Bool.to_string
             (Pe.Section.has_characteristic sec Pe.Section.MEM_EXECUTE)));
    let dtype_to_string = function
      | Some Pe.Symbol.DT_NULL -> "null"
      | Some Pe.Symbol.DT_POINTER -> "pointer"
      | Some Pe.Symbol.DT_FUNCTION -> "function"
      | Some Pe.Symbol.DT_ARRAY -> "array"
      | None -> "(none)"
    in
    Array.iter pe.symbols ~f:(fun sym ->
        printf "symbol '%s'\nderived type = %s\nnumber of aux syms = %d\n\n"
          sym.name
          (dtype_to_string sym.derived_type)
          sym.number_of_aux_symbols);
    ( match pe.data_directory.import with
    | Some (_, import_dir) ->
        Array.iter import_dir.descriptors ~f:(fun desc ->
            printf "import descriptor '%s'\n" desc.name;
            Array.iter desc.imports ~f:(fun imp ->
                let id_to_string = function
                  | Pe.Import.Name name -> name
                  | Pe.Import.Ordinal ord ->
                      Printf.sprintf "%s.%04X" desc.name ord
                in
                printf
                  "    import '%s'\n    rva = 0x%08lX\n    size = %d\n\n"
                  (id_to_string imp.id) imp.virtual_address imp.size);
            printf "\n")
    | None -> () );
    match pe.data_directory.base_reloc with
    | Some (_, base_reloc_dir) ->
        Array.iter base_reloc_dir.blocks ~f:(fun block ->
            printf "block (rva = %08lX) (size = %08lX)\n"
              block.virtual_address block.size;
            Array.iter block.relocs ~f:(fun reloc ->
                printf "    reloc (rva = %08lX) (size = %d) (value = %LX)\n"
                  reloc.virtual_address
                  (Pe.Base_reloc.get_size reloc.kind)
                  reloc.value);
            printf "\n")
    | None -> () )
  else if Mach_o_parse.is_mach_o_file filename then (
    printf "mach-o file '%s':\n\n" filename;
    let data = Bigstring.of_string (In_channel.read_all filename) in
    let macho = Mach_o_parse.from_bigstring_exn data in
    let macho_addr_str = macho_addr_str macho.is64 in
    printf "entry point = %s\n"
      Option.(
        value ~default:"(none)"
          (macho.entry >>| fun entry -> macho_addr_str entry));
    printf "\nload commands:\n\n";
    Array.iter macho.load_commands ~f:(function
      | Mach_o.Loadcommand.Segment32 seg | Mach_o.Loadcommand.Segment64 seg
        ->
          printf "segment %s\n" seg.name;
          printf
            "vmaddr: %s\nvmsize: 0x%LX\nfileoff: 0x%LX\nfilesize: 0x%LX\n"
            (macho_addr_str seg.vmaddr)
            seg.vmsize seg.fileoff seg.filesize;
          let sections =
            Array.filter macho.sections ~f:(fun sec ->
                String.equal sec.segname seg.name)
          in
          if not (Array.is_empty sections) then (
            printf "\nsections:\n\n";
            Array.iter sections ~f:(fun sec ->
                printf "section %s, seg %s\n" sec.secname sec.segname;
                printf "addr = %s\n" (macho_addr_str sec.addr);
                printf "size = 0x%LX\n" sec.size;
                printf "offset = 0x%lX\n" sec.offset;
                List.iter sec.attributes ~f:(function
                  | Mach_o.Sectionattr.PURE_INSTRUCTIONS ->
                      printf "pure instructions\n"
                  | Mach_o.Sectionattr.NO_TOC ->
                      printf "no table of contents\n"
                  | Mach_o.Sectionattr.STRIP_STATIC_SYMS ->
                      printf "strip static syms\n"
                  | Mach_o.Sectionattr.NO_DEAD_STRIP ->
                      printf "no dead strip\n"
                  | Mach_o.Sectionattr.LIVE_SUPPORT ->
                      printf "live support\n"
                  | Mach_o.Sectionattr.SELF_MODIFYING_CODE ->
                      printf "self modifying code\n"
                  | Mach_o.Sectionattr.DEBUG -> printf "debug\n"
                  | Mach_o.Sectionattr.SOME_INSTRUCTIONS ->
                      printf "some instructions\n"
                  | Mach_o.Sectionattr.EXT_RELOC -> printf "ext reloc\n"
                  | Mach_o.Sectionattr.LOC_RELOC -> printf "loc reloc\n"
                  | _ -> ());
                printf "\n") );
          printf "\n"
      | _ -> ());
    printf "\nsymbols:\n\n";
    Array.iter macho.symbols ~f:(fun sym ->
        printf "symbol %s\nvalue 0x%016LX\n" sym.name sym.value;
        ( match sym.sec with
        | None -> ()
        | Some sec -> printf "sec %s\n" sec.secname );
        printf "\n");
    printf "\nindirect symbols:\n\n";
    Array.iter macho.indirect_symbols ~f:(fun sym ->
        printf "symbol %s\nvalue 0x%016LX\n" sym.name sym.value;
        ( match sym.sec with
        | None -> ()
        | Some sec -> printf "sec %s\n" sec.secname );
        printf "\n");
    printf "\nexports:\n\n";
    Array.iter macho.exports ~f:(fun exp ->
        printf "symbolname %s\naddr 0x%016LX\n\n" exp.symbolname exp.addr) )
  else printf "unknown file type\n"
