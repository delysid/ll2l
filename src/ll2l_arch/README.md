# `ll2l-arch`

This library provides front-end support for the following ISAs (instruction set architectures):

- x86 (IA-32 and AMD64, no support for 16-bit)
- MIPS32 and MIPS64 (including Release 2 and Release 6)
- ARMv7, Thumb 2, and ARMv8 (32-bit, currently incomplete)
- RISC-V (32-bit and 64-bit)

The main module `Arch` provides an API for creating instances of an architecture and interacting with them in a generic way.
In particular, each architecture provides an instruction decoder and an instruction lifter.
The decoders will map a sequence of bytes into an architecture-specific data structure which describes details about the instruction.
The lifters will translate an instruction into an intermediate language which declares its semantics.

Not all instructions are supported yet by the lifters and decoders, and this work is ongoing.
Several of the decoders are adapted from the [B2R2](https://github.com/B2R2-org/B2R2) project.

Future support for the following architectures is desired:

- PowerPC (32-bit and 64-bit)
- AArch64
