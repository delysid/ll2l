open Core_kernel
open Ll2l_std

module Value = struct
  type t =
    | Register of X86_register.t
    | Memory of
        { segment: X86_register.t option
        ; base: X86_register.t option
        ; index: X86_register.t option
        ; scale: int
        ; displacement: (int64 * int) option (* value, size *) }
    | Pointer of {segment: int; offset: int32}
    | Immediate of {value: int64; signed: bool; relative: bool}
end

type t = {value: Value.t; size: int}

let to_string operand ~addr ~len ~wordsz ~opsz ~avx ~is_first =
  let mask_str str =
    let mask_z =
      match avx with
      | Some (X86_avx.Evex evex) -> Some (evex.aaa, evex.z)
      | Some (X86_avx.Mvex mvex) -> Some (mvex.kkk, 0)
      | _ -> None
    in
    match mask_z with
    | Some (mask, z) ->
        let orig = str in
        let str =
          if mask = 0 then ""
          else
            let tbl = X86_register.[|K0; K1; K2; K3; K4; K5; K6; K7|] in
            "{" ^ X86_register.to_string tbl.(mask) ^ "}"
        in
        if z = 0 then orig ^ str else orig ^ str ^ "{z}"
    | None -> str
  in
  match operand.value with
  | Register reg ->
      let str = X86_register.to_string reg in
      if is_first then mask_str str else str
  | Memory mem ->
      let disp = mem.displacement in
      let segment = mem.segment in
      let segment_str =
        Option.(
          value ~default:""
            (segment >>| fun r -> X86_register.to_string r ^ ":"))
      in
      let base = mem.base in
      let base_str =
        Option.(
          value ~default:"" (base >>| fun r -> X86_register.to_string r))
      in
      let index = mem.index in
      let index_str =
        Option.(
          value ~default:"" (index >>| fun r -> X86_register.to_string r))
      in
      let scale = mem.scale in
      let scale_str = if scale > 0 then Int.to_string scale else "" in
      let inside_str =
        if Option.is_some base then
          let base_str =
            match base with
            | (Some X86_register.RIP | Some X86_register.EIP)
              when Option.is_some disp -> ""
            | _ -> base_str
          in
          if Option.is_some index && scale > 0 then
            if scale > 1 then
              base_str ^ " + " ^ index_str ^ " * " ^ scale_str
            else base_str ^ " + " ^ index_str
          else base_str
        else if Option.is_some index && scale > 0 then
          if scale > 1 then index_str ^ " * " ^ scale_str else index_str
        else ""
      in
      let disp_str =
        Option.(
          value ~default:""
            ( disp
            >>| fun (value, size) ->
            match size with
            | 8 ->
                if Int64.(value land 0x80L <> 0L) then
                  let value = Int64.((lnot value + 1L) land 0x7FL) in
                  Printf.sprintf " - 0x%02LX" value
                else Printf.sprintf " + 0x%02LX" value
            | 16 ->
                if Int64.(value land 0x8000L <> 0L) then
                  let value = Int64.((lnot value + 1L) land 0x7FFFL) in
                  Printf.sprintf " - 0x%04LX" value
                else Printf.sprintf " + 0x%04LX" value
            | 32 ->
                if is_none base && is_none index && scale = 0 then
                  Printf.sprintf "0x%08LX" value
                else
                  let is_rip_rel =
                    match base with
                    | Some X86_register.RIP | Some X86_register.EIP -> true
                    | _ -> false
                  in
                  if is_rip_rel then
                    let value =
                      let addr = Addr.to_int64 addr in
                      if Int64.(value land 0x80000000L <> 0L) then
                        Int64.(
                          addr + of_int len
                          - ((lnot value + 1L) land 0x7FFFFFFL))
                      else Int64.(addr + of_int len + value)
                    in
                    if wordsz = 64 then Printf.sprintf "0x%016LX" value
                    else Printf.sprintf "0x%08LX" value
                  else if Int64.(value land 0x80000000L <> 0L) then
                    let value = Int64.((lnot value + 1L) land 0x7FFFFFFFL) in
                    Printf.sprintf " - 0x%08LX" value
                  else Printf.sprintf " + 0x%08LX" value
            | 64 -> Printf.sprintf "0x%016LX" value
            | _ -> invalid_arg "bad displacement size" ))
      in
      let size_str =
        match operand.size with
        | 8 -> "BYTE PTR "
        | 16 -> "WORD PTR "
        | 32 -> "DWORD PTR "
        | 48 -> "FWORD PTR "
        | 64 -> "QWORD PTR "
        | 80 -> "TBYTE PTR "
        | 128 -> "XMMWORD PTR "
        | 256 -> "YMMWORD PTR "
        | 512 -> "ZMMWORD PTR "
        | _ -> ""
      in
      let str = size_str ^ segment_str ^ "[" ^ inside_str ^ disp_str ^ "]" in
      if is_first then mask_str str else str
  | Pointer p -> Printf.sprintf "0x%04X:0x%08lX" p.segment p.offset
  | Immediate imm -> (
      let value, size =
        if
          imm.relative
          && (operand.size = 8 || operand.size = 16 || operand.size = 32)
        then
          let value, is_neg =
            match operand.size with
            | 8 ->
                if Int64.(imm.value land 0x80L <> 0L) then
                  (Int64.((lnot imm.value + 1L) land 0x7FL), true)
                else (imm.value, false)
            | 16 ->
                if Int64.(imm.value land 0x8000L <> 0L) then
                  (Int64.((lnot imm.value + 1L) land 0x7FFFL), true)
                else (imm.value, false)
            | 32 ->
                if Int64.(imm.value land 0x80000000L <> 0L) then
                  (Int64.((lnot imm.value + 1L) land 0x7FFFFFFFL), true)
                else (imm.value, false)
            | _ -> invalid_arg "bad relative size"
          in
          let addr = Addr.to_int64 addr in
          if is_neg then (Int64.(addr + of_int len - value), wordsz)
          else (Int64.(addr + of_int len + value), wordsz)
        else if imm.signed then
          let sign_ext value size =
            let mask = Int64.(1L lsl Int.(size - 1)) in
            Int64.((value lxor mask) - mask)
          in
          (sign_ext imm.value operand.size, opsz)
        else (imm.value, operand.size)
      in
      match size with
      | 8 -> Printf.sprintf "0x%02LX" Int64.(value land 0xFFL)
      | 16 -> Printf.sprintf "0x%04LX" Int64.(value land 0xFFFFL)
      | 32 -> Printf.sprintf "0x%08LX" Int64.(value land 0xFFFFFFFFL)
      | 64 -> Printf.sprintf "0x%016LX" value
      | _ -> Printf.sprintf "%Ld" value )
