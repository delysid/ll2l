open Core_kernel
open Ll2l_std
module Register = X86_register
module Mnemonic = X86_mnemonic
module Operand = X86_operand
module Avx = X86_avx

module Mode = struct
  type t = AMD64 | IA32 [@@deriving equal]

  let word_size_of = function
    | AMD64 -> 64
    | IA32 -> 32

  let of_string = function
    | "amd64" -> AMD64
    | "ia32" -> IA32
    | _ -> invalid_arg "bad x86 mode"

  let to_string = function
    | AMD64 -> "amd64"
    | IA32 -> "ia32"
end

module Prefix = struct
  type kind = Ignored | Effective | Mandatory [@@deriving equal]

  type t = {kind: kind; value: int}

  let lock = 0xF0

  let repne = 0xF2

  let repe = 0xF3

  let seg_cs = 0x2E

  let seg_ss = 0x36

  let seg_ds = 0x3E

  let seg_es = 0x26

  let seg_fs = 0x64

  let seg_gs = 0x65

  let operand_size_override = 0x66

  let address_size_override = 0x67

  let is_lock p = p.value = lock

  let is_repne p = p.value = repne

  let is_repe p = p.value = repe

  let is_seg_cs p = p.value = seg_cs

  let is_seg_ss p = p.value = seg_ss

  let is_seg_ds p = p.value = seg_ds

  let is_seg_es p = p.value = seg_es

  let is_seg_fs p = p.value = seg_fs

  let is_seg_gs p = p.value = seg_gs

  let is_operand_size_override p = p.value = operand_size_override

  let is_address_size_override p = p.value = address_size_override

  let is_rex p = p.value land 0xF0 = 0x40
end

module Rex = struct
  type t = {w: int; r: int; x: int; b: int; offset: int}

  let byte rex =
    (rex.w lsl 3) lor (rex.r lsl 2) lor (rex.x lsr 1) lor rex.b land 0x0F
    lor 0x40
end

module Modrm = struct
  type t = {mod': int; reg: int; rm: int; offset: int}

  let byte modrm =
    (modrm.mod' lsl 6) lor (modrm.reg lsl 3) lor modrm.rm land 0xFF
end

module Sib = struct
  type t = {scale: int; index: int; base: int; offset: int}

  let byte sib =
    (sib.scale lsl 6) lor (sib.index lsl 3) lor sib.base land 0xFF
end

module Displacement = struct
  type t = {value: int64; size: int; offset: int}
end

module Immediate = struct
  type v =
    {value: int64; signed: bool; relative: bool; size: int; offset: int}

  type t = One of v | Two of v * v
end

module Instruction = struct
  type t =
    { mode: Mode.t
    ; mnemonic: Mnemonic.t
    ; bytes: bytes
    ; operands: Operand.t array
    ; prefixes: Prefix.t array
    ; rex: Rex.t option
    ; avx: Avx.t option
    ; modrm: Modrm.t option
    ; sib: Sib.t option
    ; displacement: Displacement.t option
    ; immediate: Immediate.t option
    ; eosz: int
    ; easz: int }

  let min_length = 1

  let max_length = 15

  let length instr = Bytes.length instr.bytes [@@inline]

  let end_addr instr ~addr = Addr.(addr + int (length instr)) [@@inline]

  let no_fallthrough instr =
    Mnemonic.no_fallthrough instr.mnemonic
    [@@inline]

  let may_fallthrough instr =
    Mnemonic.may_fallthrough instr.mnemonic
    [@@inline]

  let must_fallthrough instr =
    Mnemonic.must_fallthrough instr.mnemonic
    [@@inline]

  let is_terminator instr = Mnemonic.is_terminator instr.mnemonic [@@inline]

  let is_conditional_branch instr =
    Mnemonic.is_conditional_branch instr.mnemonic
    [@@inline]

  let branch_address instr ~addr =
    match instr.immediate with
    | Some Immediate.(One imm) when imm.relative ->
        let open Option.Let_syntax in
        let%map v, is_neg =
          match imm.size with
          | 8 ->
              if Int64.(imm.value land 0x80L <> 0L) then
                Some (Int64.((lnot imm.value + 1L) land 0x7FL), true)
              else Some (imm.value, false)
          | 16 ->
              if Int64.(imm.value land 0x8000L <> 0L) then
                Some (Int64.((lnot imm.value + 1L) land 0x7FFFL), true)
              else Some (imm.value, false)
          | 32 ->
              if Int64.(imm.value land 0x80000000L <> 0L) then
                Some (Int64.((lnot imm.value + 1L) land 0x7FFFFFFFL), true)
              else Some (imm.value, false)
          | _ -> None
        in
        let len = length instr in
        if is_neg then Addr.(addr + int len - int64 v)
        else Addr.(addr + int len + int64 v)
    | _ -> None

  let accepts_lock instr =
    if Mnemonic.accepts_lock instr.mnemonic then
      match instr.operands.(0).value with
      | Operand.Value.Memory _ -> true
      | _ -> false
    else false

  let has_xacquire instr =
    let is_repne =
      match Array.find instr.prefixes ~f:Prefix.is_repne with
      | Some p -> Prefix.(equal_kind p.kind Effective)
      | None -> false
    in
    if is_repne then
      ( Array.exists instr.prefixes ~f:Prefix.is_lock
      || Mnemonic.(equal instr.mnemonic XCHG) )
      && accepts_lock instr
    else false

  let has_xrelease instr =
    let is_repe =
      match Array.find instr.prefixes ~f:Prefix.is_repe with
      | Some p -> Prefix.(equal_kind p.kind Effective)
      | None -> false
    in
    if is_repe then
      let module M = Mnemonic in
      let module OV = Operand.Value in
      if
        ( Array.exists instr.prefixes ~f:Prefix.is_lock
        || M.(equal instr.mnemonic XCHG) )
        && accepts_lock instr
      then true
      else if M.(equal instr.mnemonic MOV) then
        let op1 = instr.operands.(0).value in
        let op2 = instr.operands.(1).value in
        match (op1, op2) with
        | OV.Memory _, OV.Register _ | OV.Memory _, OV.Immediate _ -> true
        | _ -> false
      else false
    else false

  let has_rep instr =
    let module M = Mnemonic in
    match instr.mnemonic with
    | M.INSB
     |M.INSW
     |M.INSD
     |M.MONTMUL
     |M.MOVSB
     |M.MOVSW
     |M.MOVSQ
     |M.OUTSB
     |M.OUTSW
     |M.OUTSD
     |M.LODSB
     |M.LODSW
     |M.LODSD
     |M.LODSQ
     |M.STOSB
     |M.STOSW
     |M.STOSD
     |M.STOSQ
     |M.XSTORE -> Array.exists instr.prefixes ~f:Prefix.is_repe
    | M.MOVSD when Array.is_empty instr.operands ->
        Array.exists instr.prefixes ~f:Prefix.is_repe
    | _ -> false

  let has_repe instr =
    let module M = Mnemonic in
    match instr.mnemonic with
    | M.CMPSB
     |M.CMPSW
     |M.CMPSD
     |M.CMPSQ
     |M.SCASB
     |M.SCASW
     |M.SCASD
     |M.SCASQ -> Array.exists instr.prefixes ~f:Prefix.is_repe
    | _ -> false

  let has_repne instr =
    let module M = Mnemonic in
    match instr.mnemonic with
    | M.CMPSB
     |M.CMPSW
     |M.CMPSQ
     |M.SCASB
     |M.SCASW
     |M.SCASD
     |M.SCASQ
    (* even though these operations don't affect the FLAGS register
     * the semantics will be the same as with 0xF3 (REP/REPE) *)
     |M.INSB
     |M.INSW
     |M.INSD
     |M.MOVSB
     |M.MOVSW
     |M.MOVSQ
     |M.OUTSB
     |M.OUTSW
     |M.OUTSD
     |M.LODSB
     |M.LODSW
     |M.LODSD
     |M.LODSQ
     |M.STOSB
     |M.STOSW
     |M.STOSD
     |M.STOSQ -> Array.exists instr.prefixes ~f:Prefix.is_repne
    | (M.CMPSD | M.MOVSD) when Array.is_empty instr.operands ->
        Array.exists instr.prefixes ~f:Prefix.is_repne
    | _ -> false

  let to_string instr ~addr =
    let len = length instr in
    let wordsz = Mode.word_size_of instr.mode in
    let prefix_str =
      let xacquire_release_str =
        if has_xacquire instr then "XACQUIRE "
        else if has_xrelease instr then "XRELEASE "
        else ""
      in
      let lock_str =
        match Array.find instr.prefixes ~f:Prefix.is_lock with
        | Some {kind; _} when Prefix.(equal_kind kind Effective) -> "LOCK "
        | _ -> ""
      in
      let rep_str =
        if has_rep instr then "REP "
        else if has_repe instr then "REPE "
        else if has_repne instr then "REPNE "
        else ""
      in
      xacquire_release_str ^ lock_str ^ rep_str
    in
    let op_str =
      if Array.is_empty instr.operands then ""
      else
        let s =
          String.concat ~sep:", "
          @@ List.mapi (Array.to_list instr.operands) ~f:(fun i op ->
                 Operand.to_string op ~addr ~len ~wordsz ~opsz:instr.eosz
                   ~avx:instr.avx ~is_first:(i = 0))
        in
        " " ^ s
    in
    prefix_str ^ Mnemonic.to_string instr.mnemonic ^ op_str

  let to_string_full ~addr instr =
    let addr_str =
      match Mode.word_size_of instr.mode with
      | 64 -> Printf.sprintf "0x%016LX" (Addr.to_int64 addr)
      (* 32 *)
      | _ -> Printf.sprintf "0x%08lX" (Addr.to_int32 addr)
    in
    let bytes_str, bytes_spc =
      let rec aux res spc i =
        if i < max_length then
          if i >= Bytes.length instr.bytes then aux res ("  " :: spc) (i + 1)
          else
            let b = Bytes.get instr.bytes i in
            let s = Printf.sprintf "%02X" (Char.to_int b land 0xFF) in
            aux (s :: res) spc (i + 1)
        else (List.rev res, " " :: spc)
      in
      let res, spc = aux [] [] 0 in
      (String.concat ~sep:" " res, String.concat ~sep:" " spc)
    in
    Printf.sprintf "%s:  %s %s %s" addr_str bytes_str bytes_spc
      (to_string instr ~addr)
end
