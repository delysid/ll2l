open Base

module Kind = struct
  type kind =
    | GP8
    | GP16
    | GP32
    | GP64
    | X87
    | MMX
    | XMM
    | YMM
    | ZMM
    | Flags
    | IP
    | Segment
    | Table
    | Test
    | Control
    | Debug
    | Mask
    | Bound
    | Uncategorized
end

type t =
  (* General purpose registers 8-bit *)
  | AL
  | CL
  | DL
  | BL
  | AH
  | CH
  | DH
  | BH
  | SPL
  | BPL
  | SIL
  | DIL
  | R8B
  | R9B
  | R10B
  | R11B
  | R12B
  | R13B
  | R14B
  | R15B
  (* General purpose registers 16-bit *)
  | AX
  | CX
  | DX
  | BX
  | SP
  | BP
  | SI
  | DI
  | R8W
  | R9W
  | R10W
  | R11W
  | R12W
  | R13W
  | R14W
  | R15W
  (* General purpose registers 32-bit *)
  | EAX
  | ECX
  | EDX
  | EBX
  | ESP
  | EBP
  | ESI
  | EDI
  | R8D
  | R9D
  | R10D
  | R11D
  | R12D
  | R13D
  | R14D
  | R15D
  (* General purpose registers 64-bit *)
  | RAX
  | RCX
  | RDX
  | RBX
  | RSP
  | RBP
  | RSI
  | RDI
  | R8
  | R9
  | R10
  | R11
  | R12
  | R13
  | R14
  | R15
  (* Floating point legacy registers *)
  | ST0
  | ST1
  | ST2
  | ST3
  | ST4
  | ST5
  | ST6
  | ST7
  | X87CONTROL
  | X87STATUS
  | X87TAG
  (* Floating point multimedia registers *)
  | MM0
  | MM1
  | MM2
  | MM3
  | MM4
  | MM5
  | MM6
  | MM7
  (* Floating point vector registers 128-bit *)
  | XMM0
  | XMM1
  | XMM2
  | XMM3
  | XMM4
  | XMM5
  | XMM6
  | XMM7
  | XMM8
  | XMM9
  | XMM10
  | XMM11
  | XMM12
  | XMM13
  | XMM14
  | XMM15
  | XMM16
  | XMM17
  | XMM18
  | XMM19
  | XMM20
  | XMM21
  | XMM22
  | XMM23
  | XMM24
  | XMM25
  | XMM26
  | XMM27
  | XMM28
  | XMM29
  | XMM30
  | XMM31
  (* Floating point vector registers 256-bit *)
  | YMM0
  | YMM1
  | YMM2
  | YMM3
  | YMM4
  | YMM5
  | YMM6
  | YMM7
  | YMM8
  | YMM9
  | YMM10
  | YMM11
  | YMM12
  | YMM13
  | YMM14
  | YMM15
  | YMM16
  | YMM17
  | YMM18
  | YMM19
  | YMM20
  | YMM21
  | YMM22
  | YMM23
  | YMM24
  | YMM25
  | YMM26
  | YMM27
  | YMM28
  | YMM29
  | YMM30
  | YMM31
  (* Floating point vector registers 512-bit *)
  | ZMM0
  | ZMM1
  | ZMM2
  | ZMM3
  | ZMM4
  | ZMM5
  | ZMM6
  | ZMM7
  | ZMM8
  | ZMM9
  | ZMM10
  | ZMM11
  | ZMM12
  | ZMM13
  | ZMM14
  | ZMM15
  | ZMM16
  | ZMM17
  | ZMM18
  | ZMM19
  | ZMM20
  | ZMM21
  | ZMM22
  | ZMM23
  | ZMM24
  | ZMM25
  | ZMM26
  | ZMM27
  | ZMM28
  | ZMM29
  | ZMM30
  | ZMM31
  (* Flags registers *)
  | FLAGS
  | EFLAGS
  | RFLAGS
  (* Instruction-pointer registers *)
  | IP
  | EIP
  | RIP
  (* Segment registers *)
  | ES
  | CS
  | SS
  | DS
  | FS
  | GS
  (* Table registers *)
  | GDTR
  | LDTR
  | IDTR
  | TR
  (* Test registers *)
  | TR0
  | TR1
  | TR2
  | TR3
  | TR4
  | TR5
  | TR6
  | TR7
  (* Control registers *)
  | CR0
  | CR1
  | CR2
  | CR3
  | CR4
  | CR5
  | CR6
  | CR7
  | CR8
  | CR9
  | CR10
  | CR11
  | CR12
  | CR13
  | CR14
  | CR15
  (* Debug registers *)
  | DR0
  | DR1
  | DR2
  | DR3
  | DR4
  | DR5
  | DR6
  | DR7
  | DR8
  | DR9
  | DR10
  | DR11
  | DR12
  | DR13
  | DR14
  | DR15
  (* Mask registers *)
  | K0
  | K1
  | K2
  | K3
  | K4
  | K5
  | K6
  | K7
  (* Bound registers *)
  | BND0
  | BND1
  | BND2
  | BND3
  | BNDCFG
  | BNDSTATUS
  (* Uncategorized *)
  | MXCSR
  | PKRU
  | XCR0
[@@deriving equal]

let to_string = function
  (* General purpose registers 8-bit *)
  | AL -> "AL"
  | CL -> "CL"
  | DL -> "DL"
  | BL -> "BL"
  | AH -> "AH"
  | CH -> "CH"
  | DH -> "DH"
  | BH -> "BH"
  | SPL -> "SPL"
  | BPL -> "BPL"
  | SIL -> "SIL"
  | DIL -> "DIL"
  | R8B -> "R8B"
  | R9B -> "R9B"
  | R10B -> "R10B"
  | R11B -> "R11B"
  | R12B -> "R12B"
  | R13B -> "R13B"
  | R14B -> "R14B"
  | R15B -> "R15B"
  (* General purpose registers 16-bit *)
  | AX -> "AX"
  | CX -> "CX"
  | DX -> "DX"
  | BX -> "BX"
  | SP -> "SP"
  | BP -> "BP"
  | SI -> "SI"
  | DI -> "DI"
  | R8W -> "R8W"
  | R9W -> "R9W"
  | R10W -> "R10W"
  | R11W -> "R11W"
  | R12W -> "R12W"
  | R13W -> "R13W"
  | R14W -> "R14W"
  | R15W -> "R15W"
  (* General purpose registers 32-bit *)
  | EAX -> "EAX"
  | ECX -> "ECX"
  | EDX -> "EDX"
  | EBX -> "EBX"
  | ESP -> "ESP"
  | EBP -> "EBP"
  | ESI -> "ESI"
  | EDI -> "EDI"
  | R8D -> "R8D"
  | R9D -> "R9D"
  | R10D -> "R10D"
  | R11D -> "R11D"
  | R12D -> "R12D"
  | R13D -> "R13D"
  | R14D -> "R14D"
  | R15D -> "R15D"
  (* General purpose registers 64-bit *)
  | RAX -> "RAX"
  | RCX -> "RCX"
  | RDX -> "RDX"
  | RBX -> "RBX"
  | RSP -> "RSP"
  | RBP -> "RBP"
  | RSI -> "RSI"
  | RDI -> "RDI"
  | R8 -> "R8"
  | R9 -> "R9"
  | R10 -> "R10"
  | R11 -> "R11"
  | R12 -> "R12"
  | R13 -> "R13"
  | R14 -> "R14"
  | R15 -> "R15"
  (* Floating point legacy registers *)
  | ST0 -> "ST0"
  | ST1 -> "ST1"
  | ST2 -> "ST2"
  | ST3 -> "ST3"
  | ST4 -> "ST4"
  | ST5 -> "ST5"
  | ST6 -> "ST6"
  | ST7 -> "ST7"
  | X87CONTROL -> "X87CONTROL"
  | X87STATUS -> "X87STATUS"
  | X87TAG -> "X87TAG"
  (* Floating point multimedia registers *)
  | MM0 -> "MM0"
  | MM1 -> "MM1"
  | MM2 -> "MM2"
  | MM3 -> "MM3"
  | MM4 -> "MM4"
  | MM5 -> "MM5"
  | MM6 -> "MM6"
  | MM7 -> "MM7"
  (* Floating point vector registers 128-bit *)
  | XMM0 -> "XMM0"
  | XMM1 -> "XMM1"
  | XMM2 -> "XMM2"
  | XMM3 -> "XMM3"
  | XMM4 -> "XMM4"
  | XMM5 -> "XMM5"
  | XMM6 -> "XMM6"
  | XMM7 -> "XMM7"
  | XMM8 -> "XMM8"
  | XMM9 -> "XMM9"
  | XMM10 -> "XMM10"
  | XMM11 -> "XMM11"
  | XMM12 -> "XMM12"
  | XMM13 -> "XMM13"
  | XMM14 -> "XMM14"
  | XMM15 -> "XMM15"
  | XMM16 -> "XMM16"
  | XMM17 -> "XMM17"
  | XMM18 -> "XMM18"
  | XMM19 -> "XMM19"
  | XMM20 -> "XMM20"
  | XMM21 -> "XMM21"
  | XMM22 -> "XMM22"
  | XMM23 -> "XMM23"
  | XMM24 -> "XMM24"
  | XMM25 -> "XMM25"
  | XMM26 -> "XMM26"
  | XMM27 -> "XMM27"
  | XMM28 -> "XMM28"
  | XMM29 -> "XMM29"
  | XMM30 -> "XMM30"
  | XMM31 -> "XMM31"
  (* Floating point vector registers 256-bit *)
  | YMM0 -> "YMM0"
  | YMM1 -> "YMM1"
  | YMM2 -> "YMM2"
  | YMM3 -> "YMM3"
  | YMM4 -> "YMM4"
  | YMM5 -> "YMM5"
  | YMM6 -> "YMM6"
  | YMM7 -> "YMM7"
  | YMM8 -> "YMM8"
  | YMM9 -> "YMM9"
  | YMM10 -> "YMM10"
  | YMM11 -> "YMM11"
  | YMM12 -> "YMM12"
  | YMM13 -> "YMM13"
  | YMM14 -> "YMM14"
  | YMM15 -> "YMM15"
  | YMM16 -> "YMM16"
  | YMM17 -> "YMM17"
  | YMM18 -> "YMM18"
  | YMM19 -> "YMM19"
  | YMM20 -> "YMM20"
  | YMM21 -> "YMM21"
  | YMM22 -> "YMM22"
  | YMM23 -> "YMM23"
  | YMM24 -> "YMM24"
  | YMM25 -> "YMM25"
  | YMM26 -> "YMM26"
  | YMM27 -> "YMM27"
  | YMM28 -> "YMM28"
  | YMM29 -> "YMM29"
  | YMM30 -> "YMM30"
  | YMM31 -> "YMM31"
  (* Floating point vector registers 512-bit *)
  | ZMM0 -> "ZMM0"
  | ZMM1 -> "ZMM1"
  | ZMM2 -> "ZMM2"
  | ZMM3 -> "ZMM3"
  | ZMM4 -> "ZMM4"
  | ZMM5 -> "ZMM5"
  | ZMM6 -> "ZMM6"
  | ZMM7 -> "ZMM7"
  | ZMM8 -> "ZMM8"
  | ZMM9 -> "ZMM9"
  | ZMM10 -> "ZMM10"
  | ZMM11 -> "ZMM11"
  | ZMM12 -> "ZMM12"
  | ZMM13 -> "ZMM13"
  | ZMM14 -> "ZMM14"
  | ZMM15 -> "ZMM15"
  | ZMM16 -> "ZMM16"
  | ZMM17 -> "ZMM17"
  | ZMM18 -> "ZMM18"
  | ZMM19 -> "ZMM19"
  | ZMM20 -> "ZMM20"
  | ZMM21 -> "ZMM21"
  | ZMM22 -> "ZMM22"
  | ZMM23 -> "ZMM23"
  | ZMM24 -> "ZMM24"
  | ZMM25 -> "ZMM25"
  | ZMM26 -> "ZMM26"
  | ZMM27 -> "ZMM27"
  | ZMM28 -> "ZMM28"
  | ZMM29 -> "ZMM29"
  | ZMM30 -> "ZMM30"
  | ZMM31 -> "ZMM31"
  (* Flags registers *)
  | FLAGS -> "FLAGS"
  | EFLAGS -> "EFLAGS"
  | RFLAGS -> "RFLAGS"
  (* Instruction-pointer registers *)
  | IP -> "IP"
  | EIP -> "EIP"
  | RIP -> "RIP"
  (* Segment registers *)
  | ES -> "ES"
  | CS -> "CS"
  | SS -> "SS"
  | DS -> "DS"
  | FS -> "FS"
  | GS -> "GS"
  (* Table registers *)
  | GDTR -> "GDTR"
  | LDTR -> "LDTR"
  | IDTR -> "IDTR"
  | TR -> "TR"
  (* Test registers *)
  | TR0 -> "TR0"
  | TR1 -> "TR1"
  | TR2 -> "TR2"
  | TR3 -> "TR3"
  | TR4 -> "TR4"
  | TR5 -> "TR5"
  | TR6 -> "TR6"
  | TR7 -> "TR7"
  (* Control registers *)
  | CR0 -> "CR0"
  | CR1 -> "CR1"
  | CR2 -> "CR2"
  | CR3 -> "CR3"
  | CR4 -> "CR4"
  | CR5 -> "CR5"
  | CR6 -> "CR6"
  | CR7 -> "CR7"
  | CR8 -> "CR8"
  | CR9 -> "CR9"
  | CR10 -> "CR10"
  | CR11 -> "CR11"
  | CR12 -> "CR12"
  | CR13 -> "CR13"
  | CR14 -> "CR14"
  | CR15 -> "CR15"
  (* Debug registers *)
  | DR0 -> "DR0"
  | DR1 -> "DR1"
  | DR2 -> "DR2"
  | DR3 -> "DR3"
  | DR4 -> "DR4"
  | DR5 -> "DR5"
  | DR6 -> "DR6"
  | DR7 -> "DR7"
  | DR8 -> "DR8"
  | DR9 -> "DR9"
  | DR10 -> "DR10"
  | DR11 -> "DR11"
  | DR12 -> "DR12"
  | DR13 -> "DR13"
  | DR14 -> "DR14"
  | DR15 -> "DR15"
  (* Mask registers *)
  | K0 -> "K0"
  | K1 -> "K1"
  | K2 -> "K2"
  | K3 -> "K3"
  | K4 -> "K4"
  | K5 -> "K5"
  | K6 -> "K6"
  | K7 -> "K7"
  (* Bound registers *)
  | BND0 -> "BND0"
  | BND1 -> "BND1"
  | BND2 -> "BND2"
  | BND3 -> "BND3"
  | BNDCFG -> "BNDCFG"
  | BNDSTATUS -> "BNDSTATUS"
  (* Uncategorized *)
  | MXCSR -> "MXCSR"
  | PKRU -> "PKRU"
  | XCR0 -> "XCR0"

let of_string = function
  (* General purpose registers 8-bit *)
  | "AL" -> Some AL
  | "CL" -> Some CL
  | "DL" -> Some DL
  | "BL" -> Some BL
  | "AH" -> Some AH
  | "CH" -> Some CH
  | "DH" -> Some DH
  | "BH" -> Some BH
  | "SPL" -> Some SPL
  | "BPL" -> Some BPL
  | "SIL" -> Some SIL
  | "DIL" -> Some DIL
  | "R8B" -> Some R8B
  | "R9B" -> Some R9B
  | "R10B" -> Some R10B
  | "R11B" -> Some R11B
  | "R12B" -> Some R12B
  | "R13B" -> Some R13B
  | "R14B" -> Some R14B
  | "R15B" -> Some R15B
  (* General purpose registers 16-bit *)
  | "AX" -> Some AX
  | "CX" -> Some CX
  | "DX" -> Some DX
  | "BX" -> Some BX
  | "SP" -> Some SP
  | "BP" -> Some BP
  | "SI" -> Some SI
  | "DI" -> Some DI
  | "R8W" -> Some R8W
  | "R9W" -> Some R9W
  | "R10W" -> Some R10W
  | "R11W" -> Some R11W
  | "R12W" -> Some R12W
  | "R13W" -> Some R13W
  | "R14W" -> Some R14W
  | "R15W" -> Some R15W
  (* General purpose registers 32-bit *)
  | "EAX" -> Some EAX
  | "ECX" -> Some ECX
  | "EDX" -> Some EDX
  | "EBX" -> Some EBX
  | "ESP" -> Some ESP
  | "EBP" -> Some EBP
  | "ESI" -> Some ESI
  | "EDI" -> Some EDI
  | "R8D" -> Some R8D
  | "R9D" -> Some R9D
  | "R10D" -> Some R10D
  | "R11D" -> Some R11D
  | "R12D" -> Some R12D
  | "R13D" -> Some R13D
  | "R14D" -> Some R14D
  | "R15D" -> Some R15D
  (* General purpose registers 64-bit *)
  | "RAX" -> Some RAX
  | "RCX" -> Some RCX
  | "RDX" -> Some RDX
  | "RBX" -> Some RBX
  | "RSP" -> Some RSP
  | "RBP" -> Some RBP
  | "RSI" -> Some RSI
  | "RDI" -> Some RDI
  | "R8" -> Some R8
  | "R9" -> Some R9
  | "R10" -> Some R10
  | "R11" -> Some R11
  | "R12" -> Some R12
  | "R13" -> Some R13
  | "R14" -> Some R14
  | "R15" -> Some R15
  (* Floating point legacy registers *)
  | "ST0" -> Some ST0
  | "ST1" -> Some ST1
  | "ST2" -> Some ST2
  | "ST3" -> Some ST3
  | "ST4" -> Some ST4
  | "ST5" -> Some ST5
  | "ST6" -> Some ST6
  | "ST7" -> Some ST7
  | "X87CONTROL" -> Some X87CONTROL
  | "X87STATUS" -> Some X87STATUS
  | "X87TAG" -> Some X87TAG
  (* Floating point multimedia registers *)
  | "MM0" -> Some MM0
  | "MM1" -> Some MM1
  | "MM2" -> Some MM2
  | "MM3" -> Some MM3
  | "MM4" -> Some MM4
  | "MM5" -> Some MM5
  | "MM6" -> Some MM6
  | "MM7" -> Some MM7
  (* Floating point vector registers 128-bit *)
  | "XMM0" -> Some XMM0
  | "XMM1" -> Some XMM1
  | "XMM2" -> Some XMM2
  | "XMM3" -> Some XMM3
  | "XMM4" -> Some XMM4
  | "XMM5" -> Some XMM5
  | "XMM6" -> Some XMM6
  | "XMM7" -> Some XMM7
  | "XMM8" -> Some XMM8
  | "XMM9" -> Some XMM9
  | "XMM10" -> Some XMM10
  | "XMM11" -> Some XMM11
  | "XMM12" -> Some XMM12
  | "XMM13" -> Some XMM13
  | "XMM14" -> Some XMM14
  | "XMM15" -> Some XMM15
  | "XMM16" -> Some XMM16
  | "XMM17" -> Some XMM17
  | "XMM18" -> Some XMM18
  | "XMM19" -> Some XMM19
  | "XMM20" -> Some XMM20
  | "XMM21" -> Some XMM21
  | "XMM22" -> Some XMM22
  | "XMM23" -> Some XMM23
  | "XMM24" -> Some XMM24
  | "XMM25" -> Some XMM25
  | "XMM26" -> Some XMM26
  | "XMM27" -> Some XMM27
  | "XMM28" -> Some XMM28
  | "XMM29" -> Some XMM29
  | "XMM30" -> Some XMM30
  | "XMM31" -> Some XMM31
  (* Floating point vector registers 256-bit *)
  | "YMM0" -> Some YMM0
  | "YMM1" -> Some YMM1
  | "YMM2" -> Some YMM2
  | "YMM3" -> Some YMM3
  | "YMM4" -> Some YMM4
  | "YMM5" -> Some YMM5
  | "YMM6" -> Some YMM6
  | "YMM7" -> Some YMM7
  | "YMM8" -> Some YMM8
  | "YMM9" -> Some YMM9
  | "YMM10" -> Some YMM10
  | "YMM11" -> Some YMM11
  | "YMM12" -> Some YMM12
  | "YMM13" -> Some YMM13
  | "YMM14" -> Some YMM14
  | "YMM15" -> Some YMM15
  | "YMM16" -> Some YMM16
  | "YMM17" -> Some YMM17
  | "YMM18" -> Some YMM18
  | "YMM19" -> Some YMM19
  | "YMM20" -> Some YMM20
  | "YMM21" -> Some YMM21
  | "YMM22" -> Some YMM22
  | "YMM23" -> Some YMM23
  | "YMM24" -> Some YMM24
  | "YMM25" -> Some YMM25
  | "YMM26" -> Some YMM26
  | "YMM27" -> Some YMM27
  | "YMM28" -> Some YMM28
  | "YMM29" -> Some YMM29
  | "YMM30" -> Some YMM30
  | "YMM31" -> Some YMM31
  (* Floating point vector registers 512-bit *)
  | "ZMM0" -> Some ZMM0
  | "ZMM1" -> Some ZMM1
  | "ZMM2" -> Some ZMM2
  | "ZMM3" -> Some ZMM3
  | "ZMM4" -> Some ZMM4
  | "ZMM5" -> Some ZMM5
  | "ZMM6" -> Some ZMM6
  | "ZMM7" -> Some ZMM7
  | "ZMM8" -> Some ZMM8
  | "ZMM9" -> Some ZMM9
  | "ZMM10" -> Some ZMM10
  | "ZMM11" -> Some ZMM11
  | "ZMM12" -> Some ZMM12
  | "ZMM13" -> Some ZMM13
  | "ZMM14" -> Some ZMM14
  | "ZMM15" -> Some ZMM15
  | "ZMM16" -> Some ZMM16
  | "ZMM17" -> Some ZMM17
  | "ZMM18" -> Some ZMM18
  | "ZMM19" -> Some ZMM19
  | "ZMM20" -> Some ZMM20
  | "ZMM21" -> Some ZMM21
  | "ZMM22" -> Some ZMM22
  | "ZMM23" -> Some ZMM23
  | "ZMM24" -> Some ZMM24
  | "ZMM25" -> Some ZMM25
  | "ZMM26" -> Some ZMM26
  | "ZMM27" -> Some ZMM27
  | "ZMM28" -> Some ZMM28
  | "ZMM29" -> Some ZMM29
  | "ZMM30" -> Some ZMM30
  | "ZMM31" -> Some ZMM31
  (* Flags registers *)
  | "FLAGS" -> Some FLAGS
  | "EFLAGS" -> Some EFLAGS
  | "RFLAGS" -> Some RFLAGS
  (* Instruction-pointer registers *)
  | "IP" -> Some IP
  | "EIP" -> Some EIP
  | "RIP" -> Some RIP
  (* Segment registers *)
  | "ES" -> Some ES
  | "CS" -> Some CS
  | "SS" -> Some SS
  | "DS" -> Some DS
  | "FS" -> Some FS
  | "GS" -> Some GS
  (* Table registers *)
  | "GDTR" -> Some GDTR
  | "LDTR" -> Some LDTR
  | "IDTR" -> Some IDTR
  | "TR" -> Some TR
  (* Test registers *)
  | "TR0" -> Some TR0
  | "TR1" -> Some TR1
  | "TR2" -> Some TR2
  | "TR3" -> Some TR3
  | "TR4" -> Some TR4
  | "TR5" -> Some TR5
  | "TR6" -> Some TR6
  | "TR7" -> Some TR7
  (* Control registers *)
  | "CR0" -> Some CR0
  | "CR1" -> Some CR1
  | "CR2" -> Some CR2
  | "CR3" -> Some CR3
  | "CR4" -> Some CR4
  | "CR5" -> Some CR5
  | "CR6" -> Some CR6
  | "CR7" -> Some CR7
  | "CR8" -> Some CR8
  | "CR9" -> Some CR9
  | "CR10" -> Some CR10
  | "CR11" -> Some CR11
  | "CR12" -> Some CR12
  | "CR13" -> Some CR13
  | "CR14" -> Some CR14
  | "CR15" -> Some CR15
  (* Debug registers *)
  | "DR0" -> Some DR0
  | "DR1" -> Some DR1
  | "DR2" -> Some DR2
  | "DR3" -> Some DR3
  | "DR4" -> Some DR4
  | "DR5" -> Some DR5
  | "DR6" -> Some DR6
  | "DR7" -> Some DR7
  | "DR8" -> Some DR8
  | "DR9" -> Some DR9
  | "DR10" -> Some DR10
  | "DR11" -> Some DR11
  | "DR12" -> Some DR12
  | "DR13" -> Some DR13
  | "DR14" -> Some DR14
  | "DR15" -> Some DR15
  (* Mask registers *)
  | "K0" -> Some K0
  | "K1" -> Some K1
  | "K2" -> Some K2
  | "K3" -> Some K3
  | "K4" -> Some K4
  | "K5" -> Some K5
  | "K6" -> Some K6
  | "K7" -> Some K7
  (* Bound registers *)
  | "BND0" -> Some BND0
  | "BND1" -> Some BND1
  | "BND2" -> Some BND2
  | "BND3" -> Some BND3
  | "BNDCFG" -> Some BNDCFG
  | "BNDSTATUS" -> Some BNDSTATUS
  (* Uncategorized *)
  | "MXCSR" -> Some MXCSR
  | "PKRU" -> Some PKRU
  | "XCR0" -> Some XCR0
  | _ -> None

let kind_of = function
  (* General purpose registers 8-bit *)
  | AL
   |CL
   |DL
   |BL
   |AH
   |CH
   |DH
   |BH
   |SPL
   |BPL
   |SIL
   |DIL
   |R8B
   |R9B
   |R10B
   |R11B
   |R12B
   |R13B
   |R14B
   |R15B -> Kind.GP8
  (* General purpose registers 16-bit *)
  | AX
   |CX
   |DX
   |BX
   |SP
   |BP
   |SI
   |DI
   |R8W
   |R9W
   |R10W
   |R11W
   |R12W
   |R13W
   |R14W
   |R15W -> Kind.GP16
  (* General purpose registers 32-bit *)
  | EAX
   |ECX
   |EDX
   |EBX
   |ESP
   |EBP
   |ESI
   |EDI
   |R8D
   |R9D
   |R10D
   |R11D
   |R12D
   |R13D
   |R14D
   |R15D -> Kind.GP32
  (* General purpose registers 64-bit *)
  | RAX
   |RCX
   |RDX
   |RBX
   |RSP
   |RBP
   |RSI
   |RDI
   |R8
   |R9
   |R10
   |R11
   |R12
   |R13
   |R14
   |R15 -> Kind.GP64
  (* Floating point legacy registers *)
  | ST0
   |ST1
   |ST2
   |ST3
   |ST4
   |ST5
   |ST6
   |ST7
   |X87CONTROL
   |X87STATUS
   |X87TAG -> Kind.X87
  (* Floating point multimedia registers *)
  | MM0 | MM1 | MM2 | MM3 | MM4 | MM5 | MM6 | MM7 -> Kind.MMX
  (* Floating point vector registers 128-bit *)
  | XMM0
   |XMM1
   |XMM2
   |XMM3
   |XMM4
   |XMM5
   |XMM6
   |XMM7
   |XMM8
   |XMM9
   |XMM10
   |XMM11
   |XMM12
   |XMM13
   |XMM14
   |XMM15
   |XMM16
   |XMM17
   |XMM18
   |XMM19
   |XMM20
   |XMM21
   |XMM22
   |XMM23
   |XMM24
   |XMM25
   |XMM26
   |XMM27
   |XMM28
   |XMM29
   |XMM30
   |XMM31 -> Kind.XMM
  (* Floating point vector registers 256-bit *)
  | YMM0
   |YMM1
   |YMM2
   |YMM3
   |YMM4
   |YMM5
   |YMM6
   |YMM7
   |YMM8
   |YMM9
   |YMM10
   |YMM11
   |YMM12
   |YMM13
   |YMM14
   |YMM15
   |YMM16
   |YMM17
   |YMM18
   |YMM19
   |YMM20
   |YMM21
   |YMM22
   |YMM23
   |YMM24
   |YMM25
   |YMM26
   |YMM27
   |YMM28
   |YMM29
   |YMM30
   |YMM31 -> Kind.YMM
  (* Floating point vector registers 512-bit *)
  | ZMM0
   |ZMM1
   |ZMM2
   |ZMM3
   |ZMM4
   |ZMM5
   |ZMM6
   |ZMM7
   |ZMM8
   |ZMM9
   |ZMM10
   |ZMM11
   |ZMM12
   |ZMM13
   |ZMM14
   |ZMM15
   |ZMM16
   |ZMM17
   |ZMM18
   |ZMM19
   |ZMM20
   |ZMM21
   |ZMM22
   |ZMM23
   |ZMM24
   |ZMM25
   |ZMM26
   |ZMM27
   |ZMM28
   |ZMM29
   |ZMM30
   |ZMM31 -> Kind.ZMM
  (* Flags registers *)
  | FLAGS | EFLAGS | RFLAGS -> Kind.Flags
  (* Instruction-pointer registers *)
  | IP | EIP | RIP -> Kind.IP
  (* Segment registers *)
  | ES | CS | SS | DS | FS | GS -> Kind.Segment
  (* Table registers *)
  | GDTR | LDTR | IDTR | TR -> Kind.Table
  (* Test registers *)
  | TR0 | TR1 | TR2 | TR3 | TR4 | TR5 | TR6 | TR7 -> Kind.Test
  (* Control registers *)
  | CR0
   |CR1
   |CR2
   |CR3
   |CR4
   |CR5
   |CR6
   |CR7
   |CR8
   |CR9
   |CR10
   |CR11
   |CR12
   |CR13
   |CR14
   |CR15 -> Kind.Control
  (* Debug registers *)
  | DR0
   |DR1
   |DR2
   |DR3
   |DR4
   |DR5
   |DR6
   |DR7
   |DR8
   |DR9
   |DR10
   |DR11
   |DR12
   |DR13
   |DR14
   |DR15 -> Kind.Debug
  (* Mask registers *)
  | K0 | K1 | K2 | K3 | K4 | K5 | K6 | K7 -> Kind.Mask
  (* Bound registers *)
  | BND0 | BND1 | BND2 | BND3 | BNDCFG | BNDSTATUS -> Kind.Bound
  (* Uncategorized *)
  | MXCSR | PKRU | XCR0 -> Kind.Uncategorized

type width = {width: int; width64: int}

let width_of reg =
  match kind_of reg with
  | Kind.GP8 -> Some {width= 8; width64= 8}
  | Kind.GP16 -> Some {width= 16; width64= 16}
  | Kind.GP32 -> Some {width= 32; width64= 32}
  | Kind.GP64 | Kind.MMX -> Some {width= 64; width64= 64}
  | Kind.X87 -> (
    match reg with
    | ST0 | ST1 | ST2 | ST3 | ST4 | ST5 | ST6 | ST7 ->
        Some {width= 80; width64= 80}
    | X87CONTROL | X87STATUS | X87TAG -> Some {width= 16; width64= 16}
    | _ -> None )
  | Kind.XMM -> Some {width= 128; width64= 128}
  | Kind.YMM -> Some {width= 256; width64= 256}
  | Kind.ZMM -> Some {width= 512; width64= 512}
  | Kind.Flags -> (
    match reg with
    | FLAGS -> Some {width= 16; width64= 16}
    | EFLAGS -> Some {width= 32; width64= 32}
    | RFLAGS -> Some {width= 0; width64= 64}
    | _ -> None )
  | Kind.IP -> (
    match reg with
    | IP -> Some {width= 16; width64= 16}
    | EIP -> Some {width= 32; width64= 32}
    | RIP -> Some {width= 0; width64= 64}
    | _ -> None )
  | Kind.Segment -> Some {width= 16; width64= 16}
  | Kind.Table -> (
    match reg with
    | GDTR | LDTR | IDTR -> Some {width= 48; width64= 48}
    | TR -> Some {width= 16; width64= 16}
    | _ -> None )
  | Kind.Test -> Some {width= 32; width64= 32}
  | Kind.Control | Kind.Debug -> Some {width= 32; width64= 64}
  | Kind.Mask -> Some {width= 0; width64= 0}
  | Kind.Bound -> (
    match reg with
    | BND0 | BND1 | BND2 | BND3 -> Some {width= 128; width64= 128}
    | BNDCFG | BNDSTATUS -> Some {width= 64; width64= 64}
    | _ -> None )
  | Kind.Uncategorized -> (
    match reg with
    | MXCSR | PKRU -> Some {width= 32; width64= 32}
    | XCR0 -> Some {width= 64; width64= 64}
    | _ -> None )
