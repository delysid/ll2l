open Core_kernel
open Ll2l_ir
open Ll2l_std
module I = X86.Instruction
module Imm = X86.Immediate
module M = X86.Mnemonic
module R = X86.Register
module O = X86.Operand
module OV = O.Value
module P = X86.Prefix
module A = X86.Avx

type error = [`BadInstr | `BadOperand of int]

let string_of_error = function
  | `BadInstr -> "bad instruction"
  | `BadOperand n -> Printf.sprintf "bad operand (%d)" n

exception X86_lift_error of error

let cs = Il.reg "CS" 16

let ss = Il.reg "SS" 16

let ds = Il.reg "DS" 16

let es = Il.reg "ES" 16

let fs = Il.reg "FS" 16

let gs = Il.reg "GS" 16

let cs_base wordsz = Il.reg "CS_BASE" wordsz

let ss_base wordsz = Il.reg "SS_BASE" wordsz

let ds_base wordsz = Il.reg "DS_BASE" wordsz

let es_base wordsz = Il.reg "ES_BASE" wordsz

let fs_base wordsz = Il.reg "FS_BASE" wordsz

let gs_base wordsz = Il.reg "GS_BASE" wordsz

let make_reg r is64 =
  let open Option.Let_syntax in
  let%map w = R.width_of r in
  let size = if is64 then w.width64 else w.width in
  (Il.reg (R.to_string r) size, size)

let make_seg_base (instr : I.t) wordsz =
  let open Option.Let_syntax in
  let%bind seg =
    let override =
      Array.find instr.prefixes ~f:(fun p ->
          if P.(equal_kind p.kind Effective) then
            P.is_seg_cs p || P.is_seg_ss p || P.is_seg_ds p || P.is_seg_es p
            || P.is_seg_fs p || P.is_seg_gs p
          else false)
    in
    match override with
    | None -> Some R.DS
    | Some seg -> (
      match seg.value with
      | 0x2E -> Some R.CS
      | 0x36 -> Some R.SS
      | 0x3E -> Some R.DS
      | 0x26 -> Some R.ES
      | 0x64 -> Some R.FS
      | 0x65 -> Some R.GS
      | _ -> None )
  in
  match seg with
  (* | R.CS -> Some Il.(cs_base wordsz)
   * | R.SS -> Some Il.(ss_base wordsz)
   * | R.DS -> Some Il.(ds_base wordsz)
   * | R.ES -> Some Il.(es_base wordsz) *)
  | R.FS -> Some Il.(fs_base wordsz)
  | R.GS -> Some Il.(gs_base wordsz)
  | _ -> None

let neg_disp_8 v = Int64.((lnot v + 1L) land 0x7FL)

let neg_disp_16 v = Int64.((lnot v + 1L) land 0x7FFFL)

let neg_disp_32 v = Int64.((lnot v + 1L) land 0x7FFFFFFFL)

let make_operand_rm' ?(ignore_seg = false) (instr : I.t) addr i =
  let open Result.Let_syntax in
  let op = instr.operands.(i) in
  let is64 = X86.Mode.(equal instr.mode AMD64) in
  let wordsz = X86.Mode.word_size_of instr.mode in
  let addr = Addr.to_int64 addr in
  match op.value with
  | OV.Register r -> (
    match make_reg r is64 with
    | None -> Error (`BadOperand i)
    | Some (r, _) -> Ok (Some r) )
  | OV.Memory m ->
      let%bind sib =
        match m.base with
        | Some rb -> (
          match make_reg rb is64 with
          | None -> Error (`BadOperand i)
          | Some (rb, _) -> (
            match m.index with
            | Some ri when m.scale >= 1 -> (
              match make_reg ri is64 with
              | None -> Error (`BadOperand i)
              | Some (ri, sz) ->
                  if m.scale > 1 then
                    Ok (Some Il.(rb + (ri * num m.scale sz)))
                  else Ok (Some Il.(rb + ri)) )
            | _ -> Ok (Some rb) ) )
        | None -> (
          match m.index with
          | Some ri when m.scale >= 1 -> (
            match make_reg ri is64 with
            | None -> Error (`BadOperand i)
            | Some (ri, sz) ->
                if m.scale > 1 then Ok (Some Il.(ri * num m.scale sz))
                else Ok (Some ri) )
          | _ -> Ok None )
      in
      let disp =
        Option.(
          m.displacement
          >>| fun (value, size) ->
          match size with
          | 8 ->
              if Int64.(value land 0x80L <> 0L) then (neg_disp_8 value, true)
              else (value, false)
          | 16 ->
              if Int64.(value land 0x8000L <> 0L) then
                (neg_disp_16 value, true)
              else (value, false)
          | 32 -> (
              if is_none m.base && is_none m.index && m.scale = 0 then
                (value, false)
              else
                match m.base with
                | Some R.RIP | Some R.EIP ->
                    let len = Bytes.length instr.bytes in
                    if Int64.(value land 0x80000000L <> 0L) then
                      let disp = neg_disp_32 value in
                      (Int64.(addr + of_int len - disp), true)
                    else (Int64.(addr + of_int len + value), false)
                | _ ->
                    if Int64.(value land 0x80000000L <> 0L) then
                      (neg_disp_32 value, true)
                    else (value, false) )
          | _ -> (value, false))
      in
      let%bind e =
        match sib with
        | Some sib -> (
          match disp with
          | Some (disp, is_neg) -> (
              let disp' = Il.num64 disp instr.easz in
              match m.base with
              | Some R.RIP | Some R.EIP -> Ok disp'
              | _ ->
                  if Int64.(disp = 0L) then Ok sib
                  else if is_neg then Ok Il.(sib - disp')
                  else Ok Il.(sib + disp') )
          | None -> Ok sib )
        | None -> (
          match disp with
          | Some (disp, _) -> Ok Il.(num64 disp instr.easz)
          | None -> Error (`BadOperand i) )
      in
      let%map e =
        if ignore_seg then Ok e
        else
          match m.segment with
          (* CS, SS, DS, and ES are effectively neutered in 64-bit mode
           * however, even with 32-bit mode, we can assume flat addressing *)
          | Some R.CS | Some R.SS | Some R.DS | Some R.ES -> Ok e
          (* | Some R.CS ->
           *    if is64 then Ok e
           *    else Ok Il.((cs_base wordsz) + e)
           * | Some R.SS ->
           *    if is64 then Ok e
           *    else Ok Il.((ss_base wordsz) + e)
           * | Some R.DS ->
           *    if is64 then Ok e
           *    else Ok Il.((ds_base wordsz) + e)
           * | Some R.ES ->
           *    if is64 then Ok e
           *    else Ok Il.((es_base wordsz) + e) *)
          | Some R.FS -> Ok Il.(fs_base wordsz + e)
          | Some R.GS -> Ok Il.(gs_base wordsz + e)
          | Some _ -> Error (`BadOperand i)
          | None -> Ok e
      in
      Some Il.(load `LE (op.size lsr 3) mu e)
  | _ -> Ok None

let make_operand_rm ?(ignore_seg = false) (instr : I.t) addr i =
  let open Result.Let_syntax in
  let%bind rm = make_operand_rm' instr addr i ~ignore_seg in
  Option.(value ~default:(Error (`BadOperand i)) (rm >>| fun rm -> Ok rm))

let sign_ext value size =
  let mask = Int64.(1L lsl Int.(size - 1)) in
  Int64.((value lxor mask) - mask)

let make_operand (instr : I.t) addr i =
  let open Result.Let_syntax in
  let%bind rm = make_operand_rm' instr addr i in
  let addr = Addr.to_int64 addr in
  match rm with
  | Some rm -> Ok rm
  | None -> (
      let op = instr.operands.(i) in
      match op.value with
      | OV.Immediate imm ->
          let%bind value, size =
            if imm.signed then
              let wordsz = X86.Mode.(word_size_of instr.mode) in
              if imm.relative then
                let len = Bytes.length instr.bytes in
                let%bind value, is_neg =
                  match op.size with
                  | 8 ->
                      if Int64.(imm.value land 0x80L <> 0L) then
                        Ok (neg_disp_8 imm.value, true)
                      else Ok (imm.value, false)
                  | 16 ->
                      if Int64.(imm.value land 0x8000L <> 0L) then
                        Ok (neg_disp_16 imm.value, true)
                      else Ok (imm.value, false)
                  | 32 ->
                      if Int64.(imm.value land 0x80000000L <> 0L) then
                        Ok (neg_disp_32 imm.value, true)
                      else Ok (imm.value, false)
                  | _ -> Error (`BadOperand i)
                in
                if is_neg then Ok (Int64.(addr + of_int len - value), wordsz)
                else Ok (Int64.(addr + of_int len + value), wordsz)
              else
                let size =
                  if Array.length instr.operands = 1 then instr.eosz
                  else instr.operands.(0).size
                in
                let value = sign_ext imm.value op.size in
                Ok (value, size)
            else if
              (* this is just a hack for hardcoded constants
               * in the source operand since the immediate
               * bytes don't actually exist in the instruction *)
              Int64.(imm.value = 1L) && op.size = 0
            then Ok (imm.value, instr.operands.(0).size)
            else Ok (imm.value, op.size)
          in
          Ok Il.(num64 value size)
      (* Pointer (seg:offset) should be handled manually *)
      | _ -> Error (`BadOperand i) )

let cf = Il.reg "CF" 1

let pf = Il.reg "PF" 1

let af = Il.reg "AF" 1

let zf = Il.reg "ZF" 1

let sf = Il.reg "SF" 1

let tf = Il.reg "TF" 1

let iF = Il.reg "IF" 1

let df = Il.reg "DF" 1

let oF = Il.reg "OF" 1

let iopl = Il.reg "IOPL" 2

let nt = Il.reg "NT" 1

let rf = Il.reg "RF" 1

let vm = Il.reg "VM" 1

let ac = Il.reg "AC" 1

let vif = Il.reg "VIF" 1

let vip = Il.reg "VIP" 1

let id = Il.reg "ID" 1

let fcond = function
  | `B -> cf
  | `BE -> Il.(cf || zf)
  | `L -> Il.(sf <> oF)
  | `LE -> Il.(sf <> oF || zf)
  | `NB -> Il.(~~cf)
  | `NBE -> Il.(~~cf & ~~zf)
  | `NL -> Il.(sf = oF)
  | `NLE -> Il.(sf = oF & ~~zf)
  | `NO -> Il.(~~oF)
  | `NP -> Il.(~~pf)
  | `NS -> Il.(~~sf)
  | `NZ -> Il.(~~zf)
  | `O -> oF
  | `P -> pf
  | `S -> sf
  | `Z -> zf

let fcond_of_mnemonic = function
  | M.CMOVB | M.FCMOVB | M.JB | M.SETB -> Ok (fcond `B)
  | M.CMOVBE | M.FCMOVBE | M.JBE | M.SETBE -> Ok (fcond `BE)
  | M.CMOVL | M.JL | M.SETL -> Ok (fcond `L)
  | M.CMOVLE | M.JLE | M.SETLE -> Ok (fcond `LE)
  | M.CMOVNB | M.FCMOVNB | M.JNB | M.SETNB -> Ok (fcond `NB)
  | M.CMOVNBE | M.FCMOVNBE | M.JNBE | M.SETNBE -> Ok (fcond `NBE)
  | M.CMOVNL | M.JNL | M.SETNL -> Ok (fcond `NL)
  | M.CMOVNLE | M.JNLE | M.SETNLE -> Ok (fcond `NLE)
  | M.CMOVNO | M.JNO | M.SETNO -> Ok (fcond `NO)
  | M.CMOVNP | M.FCMOVNU | M.JNP | M.SETNP -> Ok (fcond `NP)
  | M.CMOVNS | M.JNS | M.SETNS -> Ok (fcond `NS)
  | M.CMOVNZ | M.FCMOVNE | M.JNZ | M.LOOPNE | M.SETNZ -> Ok (fcond `NZ)
  | M.CMOVO | M.JO | M.SETO -> Ok (fcond `O)
  | M.CMOVP | M.FCMOVU | M.JP | M.SETP -> Ok (fcond `P)
  | M.CMOVS | M.JS | M.SETS -> Ok (fcond `S)
  | M.CMOVZ | M.FCMOVE | M.JZ | M.LOOPE | M.SETZ -> Ok (fcond `Z)
  | _ -> Error `BadInstr

let arith_parity e sz ctx =
  let t = Il.Context.tmp ctx sz in
  Il.
    [ t := e
    ; t := t ^ (t >> num 4 sz)
    ; t := t ^ (t >> num 2 sz)
    ; pf := lo 1 (t ^ num 1 sz) ]

let arith_overflow lhs rhs res sz =
  Il.(oF := hi 1 (~~(lhs ^ rhs) & (lhs ^ res)))

let arith_adjust lhs rhs res sz =
  let n = Il.num 16 sz in
  Il.(af := ((lhs ^ res ^ rhs) & n) = n)

let al = Il.reg "AL" 8

let ah = Il.reg "AH" 8

let ax = Il.reg "AX" 16

let eax = Il.reg "EAX" 32

let rax = Il.reg "RAX" 64

let cl = Il.reg "CL" 8

let ch = Il.reg "CH" 8

let cx = Il.reg "CX" 16

let ecx = Il.reg "ECX" 32

let rcx = Il.reg "RCX" 64

let dl = Il.reg "DL" 8

let dh = Il.reg "DH" 8

let dx = Il.reg "DX" 16

let edx = Il.reg "EDX" 32

let rdx = Il.reg "RDX" 64

let bl = Il.reg "BL" 8

let bh = Il.reg "BH" 8

let bx = Il.reg "BX" 16

let ebx = Il.reg "EBX" 32

let rbx = Il.reg "RBX" 64

let spl = Il.reg "SPL" 8

let sp = Il.reg "SP" 16

let esp = Il.reg "ESP" 32

let rsp = Il.reg "RSP" 64

let bpl = Il.reg "BPL" 8

let bp = Il.reg "BP" 16

let ebp = Il.reg "EBP" 32

let rbp = Il.reg "RBP" 64

let sil = Il.reg "SIL" 8

let si = Il.reg "SI" 16

let esi = Il.reg "ESI" 32

let rsi = Il.reg "RSI" 64

let dil = Il.reg "DIL" 8

let di = Il.reg "DI" 16

let edi = Il.reg "EDI" 32

let rdi = Il.reg "RDI" 64

let r8b = Il.reg "R8B" 8

let r8w = Il.reg "R8W" 16

let r8d = Il.reg "R8D" 32

let r8 = Il.reg "R8" 64

let r9b = Il.reg "R9B" 8

let r9w = Il.reg "R9W" 16

let r9d = Il.reg "R9D" 32

let r9 = Il.reg "R9" 64

let r10b = Il.reg "R10B" 8

let r10w = Il.reg "R10W" 16

let r10d = Il.reg "R10D" 32

let r10 = Il.reg "R10" 64

let r11b = Il.reg "R11B" 8

let r11w = Il.reg "R11W" 16

let r11d = Il.reg "R11D" 32

let r11 = Il.reg "R11" 64

let r12b = Il.reg "R12B" 8

let r12w = Il.reg "R12W" 16

let r12d = Il.reg "R12D" 32

let r12 = Il.reg "R12" 64

let r13b = Il.reg "R13B" 8

let r13w = Il.reg "R13W" 16

let r13d = Il.reg "R13D" 32

let r13 = Il.reg "R13" 64

let r14b = Il.reg "R14B" 8

let r14w = Il.reg "R14W" 16

let r14d = Il.reg "R14D" 32

let r14 = Il.reg "R14" 64

let r15b = Il.reg "R15B" 8

let r15w = Il.reg "R15W" 16

let r15d = Il.reg "R15D" 32

let r15 = Il.reg "R15" 64

let cr0 wordsz = Il.reg "CR0" wordsz

let xcr0 = Il.reg "XCR0" 64

let flags_concat ?(clear_vm_rf = false) size =
  let mask =
    match size with
    | 32 -> Some Il.(num 0xFFC00000 size)
    | 64 -> Some Il.(num64 0xFFFFFFFFFFC00000L size)
    | _ -> None
  in
  let src =
    Il.(
      bit false @@ nt @@ iopl @@ oF @@ df @@ iF @@ tf @@ sf @@ zf
      @@ bit false @@ af @@ bit false @@ pf @@ bit true @@ cf)
  in
  match mask with
  | None -> src
  | Some msk ->
      let flg =
        Il.(
          id @@ vip @@ vif @@ ac
          @@ (if clear_vm_rf then num 0 2 else vm @@ rf)
          @@ src)
      in
      Il.(msk || ze size flg)

let flags_extract ?(is_sysret = false) src size =
  let ex_stmts =
    match size with
    | 16 -> []
    | _ ->
        let rfvm =
          if is_sysret then Il.[rf := bit false; vm := bit false]
          else Il.[rf := ex 16 16 src; vm := ex 17 17 src]
        in
        rfvm
        @ Il.
            [ ac := ex 18 18 src
            ; vif := ex 19 19 src
            ; vip := ex 20 20 src
            ; id := ex 21 21 src ]
  in
  Il.
    [ cf := lo 1 src
    ; pf := ex 2 2 src
    ; af := ex 4 4 src
    ; zf := ex 6 6 src
    ; sf := ex 7 7 src
    ; tf := ex 8 8 src
    ; iF := ex 9 9 src
    ; df := ex 10 10 src
    ; oF := ex 11 11 src
    ; iopl := ex 13 12 src
    ; nt := ex 14 14 src ]
  @ ex_stmts

let st0 = Il.reg "ST0" 80

let st1 = Il.reg "ST1" 80

let st2 = Il.reg "ST2" 80

let st3 = Il.reg "ST3" 80

let st4 = Il.reg "ST4" 80

let st5 = Il.reg "ST5" 80

let st6 = Il.reg "ST6" 80

let st7 = Il.reg "ST7" 80

let mm0 = Il.reg "MM0" 64

let mm1 = Il.reg "MM1" 64

let mm2 = Il.reg "MM2" 64

let mm3 = Il.reg "MM3" 64

let mm4 = Il.reg "MM4" 64

let mm5 = Il.reg "MM5" 64

let mm6 = Il.reg "MM6" 64

let mm7 = Il.reg "MM7" 64

(* x87 control word *)

let fpu_im = Il.reg "FPU_IM" 1

let fpu_dm = Il.reg "FPU_DM" 1

let fpu_zm = Il.reg "FPU_ZM" 1

let fpu_om = Il.reg "FPU_OM" 1

let fpu_um = Il.reg "FPU_UM" 1

let fpu_pm = Il.reg "FPU_PM" 1

let fpu_iem = Il.reg "FPU_IEM" 1

let fpu_pc = Il.reg "FPU_PC" 2

let fpu_rc = Il.reg "FPU_RC" 2

let fpu_ic = Il.reg "FPU_IC" 1

(* x87 status word *)

let fpu_i = Il.reg "FPU_I" 1

let fpu_d = Il.reg "FPU_D" 1

let fpu_z = Il.reg "FPU_Z" 1

let fpu_o = Il.reg "FPU_O" 1

let fpu_u = Il.reg "FPU_U" 1

let fpu_p = Il.reg "FPU_P" 1

let fpu_sf = Il.reg "FPU_SF" 1

let fpu_ir = Il.reg "FPU_IR" 1

let fpu_c0 = Il.reg "FPU_C0" 1

let fpu_c1 = Il.reg "FPU_C1" 1

let fpu_c2 = Il.reg "FPU_C2" 1

let fpu_top = Il.reg "FPU_TOP" 3

let fpu_c3 = Il.reg "FPU_C3" 1

let fpu_b = Il.reg "FPU_B" 1

(* x87 tag word *)

let fpu_tag0 = Il.reg "FPU_TAG0" 2

let fpu_tag1 = Il.reg "FPU_TAG1" 2

let fpu_tag2 = Il.reg "FPU_TAG2" 2

let fpu_tag3 = Il.reg "FPU_TAG3" 2

let fpu_tag4 = Il.reg "FPU_TAG4" 2

let fpu_tag5 = Il.reg "FPU_TAG5" 2

let fpu_tag6 = Il.reg "FPU_TAG6" 2

let fpu_tag7 = Il.reg "FPU_TAG7" 2

let fpu_tag_of_operand (instr : I.t) i =
  match instr.operands.(i).value with
  | OV.Register r -> (
    match r with
    | R.ST0 -> Ok fpu_tag0
    | R.ST1 -> Ok fpu_tag1
    | R.ST2 -> Ok fpu_tag2
    | R.ST3 -> Ok fpu_tag3
    | R.ST4 -> Ok fpu_tag4
    | R.ST5 -> Ok fpu_tag5
    | R.ST6 -> Ok fpu_tag6
    | R.ST7 -> Ok fpu_tag7
    | _ -> Error (`BadOperand i) )
  | _ -> Error (`BadOperand i)

let fpu_uflow_oflow b =
  Il.[fpu_c1 := bit b; fpu_i := bit true; fpu_sf := bit true; exn ()]

let fpu_compute_tag tag v l1 l2 l3 =
  (* TODO: check for denormal *)
  Il.
    [ goto_if (is_nan v || is_inf v) l1 l2
    ; ( @ ) l1
    ; tag := num 2 2
    ; goto l3
    ; ( @ ) l2
    ; tag := ze 2 (lo 79 v = num 0 79)
    ; goto l3
    ; ( @ ) l3 ]

let fpu_push ?(tag = None) v l1 l2 l3 ctx =
  let v, s =
    match tag with
    | Some tag ->
        let t1 = Il.Context.tmp ctx 80 in
        let t2 = Il.Context.tmp ctx 2 in
        (t1, Il.[t1 := v; t2 := tag])
    | _ -> (v, [])
  in
  s
  @ Il.
      [ st7 := st6
      ; fpu_tag7 := fpu_tag6
      ; st6 := st5
      ; fpu_tag6 := fpu_tag5
      ; st5 := st4
      ; fpu_tag5 := fpu_tag4
      ; st4 := st3
      ; fpu_tag4 := fpu_tag3
      ; st3 := st2
      ; fpu_tag3 := fpu_tag2
      ; st2 := st1
      ; fpu_tag2 := fpu_tag1
      ; st1 := st0
      ; fpu_tag1 := fpu_tag0
      ; st0 := v ]
  @ (if Option.is_some tag then [] else fpu_compute_tag fpu_tag0 v l1 l2 l3)
  @ Il.[fpu_top := fpu_top - num 1 3]

let fpu_push_const v =
  Il.
    [ st7 := st6
    ; fpu_tag7 := fpu_tag6
    ; st6 := st5
    ; fpu_tag6 := fpu_tag5
    ; st5 := st4
    ; fpu_tag5 := fpu_tag4
    ; st4 := st3
    ; fpu_tag4 := fpu_tag3
    ; st3 := st2
    ; fpu_tag3 := fpu_tag2
    ; st2 := st1
    ; fpu_tag2 := fpu_tag1
    ; st1 := st0
    ; fpu_tag1 := fpu_tag0
    ; st0 := v
    ; fpu_top := fpu_top - num 1 3 ]

let fpu_pop ctx =
  let t = Il.Context.tmp ctx 80 in
  Il.
    [ t := st0
    ; st0 := st1
    ; fpu_tag0 := fpu_tag1
    ; st1 := st2
    ; fpu_tag1 := fpu_tag2
    ; st2 := st3
    ; fpu_tag2 := fpu_tag3
    ; st3 := st4
    ; fpu_tag3 := fpu_tag4
    ; st4 := st5
    ; fpu_tag4 := fpu_tag5
    ; st5 := st6
    ; fpu_tag5 := fpu_tag6
    ; st6 := st7
    ; fpu_tag6 := fpu_tag7
    ; st7 := t
    ; fpu_tag7 := num 3 2
    ; fpu_top := fpu_top + num 1 3 ]

let fpu_pop2 ctx =
  let t1 = Il.Context.tmp ctx 80 in
  let t2 = Il.Context.tmp ctx 80 in
  Il.
    [ t1 := st0
    ; t2 := st1
    ; st0 := st2
    ; fpu_tag0 := fpu_tag2
    ; st1 := st3
    ; fpu_tag1 := fpu_tag3
    ; st2 := st4
    ; fpu_tag2 := fpu_tag4
    ; st3 := st5
    ; fpu_tag3 := fpu_tag5
    ; st4 := st6
    ; fpu_tag4 := fpu_tag6
    ; st5 := st7
    ; fpu_tag5 := fpu_tag7
    ; st6 := t1
    ; fpu_tag6 := num 3 2
    ; st7 := t2
    ; fpu_tag7 := num 3 2
    ; fpu_top := fpu_top + num 2 3 ]

let xmm0 = Il.reg "XMM0" 128

let xmm1 = Il.reg "XMM1" 128

let xmm2 = Il.reg "XMM2" 128

let xmm3 = Il.reg "XMM3" 128

let xmm4 = Il.reg "XMM4" 128

let xmm5 = Il.reg "XMM5" 128

let xmm6 = Il.reg "XMM6" 128

let xmm7 = Il.reg "XMM7" 128

let xmm8 = Il.reg "XMM8" 128

let xmm9 = Il.reg "XMM9" 128

let xmm10 = Il.reg "XMM10" 128

let xmm11 = Il.reg "XMM11" 128

let xmm12 = Il.reg "XMM12" 128

let xmm13 = Il.reg "XMM13" 128

let xmm14 = Il.reg "XMM14" 128

let xmm15 = Il.reg "XMM15" 128

let xmm16 = Il.reg "XMM16" 128

let xmm17 = Il.reg "XMM17" 128

let xmm18 = Il.reg "XMM18" 128

let xmm19 = Il.reg "XMM19" 128

let xmm20 = Il.reg "XMM20" 128

let xmm21 = Il.reg "XMM21" 128

let xmm22 = Il.reg "XMM22" 128

let xmm23 = Il.reg "XMM23" 128

let xmm24 = Il.reg "XMM24" 128

let xmm25 = Il.reg "XMM25" 128

let xmm26 = Il.reg "XMM26" 128

let xmm27 = Il.reg "XMM27" 128

let xmm28 = Il.reg "XMM28" 128

let xmm29 = Il.reg "XMM29" 128

let xmm30 = Il.reg "XMM30" 128

let xmm31 = Il.reg "XMM31" 128

let ymm0 = Il.reg "YMM0" 256

let ymm1 = Il.reg "YMM1" 256

let ymm2 = Il.reg "YMM2" 256

let ymm3 = Il.reg "YMM3" 256

let ymm4 = Il.reg "YMM4" 256

let ymm5 = Il.reg "YMM5" 256

let ymm6 = Il.reg "YMM6" 256

let ymm7 = Il.reg "YMM7" 256

let ymm8 = Il.reg "YMM8" 256

let ymm9 = Il.reg "YMM9" 256

let ymm10 = Il.reg "YMM10" 256

let ymm11 = Il.reg "YMM11" 256

let ymm12 = Il.reg "YMM12" 256

let ymm13 = Il.reg "YMM13" 256

let ymm14 = Il.reg "YMM14" 256

let ymm15 = Il.reg "YMM15" 256

let ymm16 = Il.reg "YMM16" 256

let ymm17 = Il.reg "YMM17" 256

let ymm18 = Il.reg "YMM18" 256

let ymm19 = Il.reg "YMM19" 256

let ymm20 = Il.reg "YMM20" 256

let ymm21 = Il.reg "YMM21" 256

let ymm22 = Il.reg "YMM22" 256

let ymm23 = Il.reg "YMM23" 256

let ymm24 = Il.reg "YMM24" 256

let ymm25 = Il.reg "YMM25" 256

let ymm26 = Il.reg "YMM26" 256

let ymm27 = Il.reg "YMM27" 256

let ymm28 = Il.reg "YMM28" 256

let ymm29 = Il.reg "YMM29" 256

let ymm30 = Il.reg "YMM30" 256

let ymm31 = Il.reg "YMM31" 256

let zmm0 = Il.reg "ZMM0" 512

let zmm1 = Il.reg "ZMM1" 512

let zmm2 = Il.reg "ZMM2" 512

let zmm3 = Il.reg "ZMM3" 512

let zmm4 = Il.reg "ZMM4" 512

let zmm5 = Il.reg "ZMM5" 512

let zmm6 = Il.reg "ZMM6" 512

let zmm7 = Il.reg "ZMM7" 512

let zmm8 = Il.reg "ZMM8" 512

let zmm9 = Il.reg "ZMM9" 512

let zmm10 = Il.reg "ZMM10" 512

let zmm11 = Il.reg "ZMM11" 512

let zmm12 = Il.reg "ZMM12" 512

let zmm13 = Il.reg "ZMM13" 512

let zmm14 = Il.reg "ZMM14" 512

let zmm15 = Il.reg "ZMM15" 512

let zmm16 = Il.reg "ZMM16" 512

let zmm17 = Il.reg "ZMM17" 512

let zmm18 = Il.reg "ZMM18" 512

let zmm19 = Il.reg "ZMM19" 512

let zmm20 = Il.reg "ZMM20" 512

let zmm21 = Il.reg "ZMM21" 512

let zmm22 = Il.reg "ZMM22" 512

let zmm23 = Il.reg "ZMM23" 512

let zmm24 = Il.reg "ZMM24" 512

let zmm25 = Il.reg "ZMM25" 512

let zmm26 = Il.reg "ZMM26" 512

let zmm27 = Il.reg "ZMM27" 512

let zmm28 = Il.reg "ZMM28" 512

let zmm29 = Il.reg "ZMM29" 512

let zmm30 = Il.reg "ZMM30" 512

let zmm31 = Il.reg "ZMM31" 512

let implicit_ze is64 stmts =
  if not is64 then stmts
  else
    let rec aux res = function
      | [] -> List.rev res
      | stmt :: rest ->
          let stmt =
            match stmt with
            | Il.Assign (dst, src) -> (
              match dst with
              | Il.Var v when Il.Var_hint.(equal v.hint Reg) -> (
                  let dst =
                    match R.of_string v.name with
                    | None -> None
                    | Some r -> (
                      match r with
                      | R.EAX -> Some rax
                      | R.ECX -> Some rcx
                      | R.EDX -> Some rdx
                      | R.EBX -> Some rbx
                      | R.ESP -> Some rsp
                      | R.EBP -> Some rbp
                      | R.ESI -> Some rsi
                      | R.EDI -> Some rdi
                      | R.R8D -> Some r8
                      | R.R9D -> Some r9
                      | R.R10D -> Some r10
                      | R.R11D -> Some r11
                      | R.R12D -> Some r12
                      | R.R13D -> Some r13
                      | R.R14D -> Some r14
                      | R.R15D -> Some r15
                      | _ -> None )
                  in
                  match dst with
                  | Some dst ->
                      let src =
                        match src with
                        | Il.Const c -> Il.Const {c with size= 64}
                        | Il.(Cast (ZEXT, _, e)) -> Il.ze 64 e
                        | _ when not (Il.equal_expr src dst) -> Il.ze 64 src
                        | _ -> src
                      in
                      Il.(dst := src)
                  | None -> stmt )
              | _ -> stmt )
            | _ -> stmt
          in
          aux (stmt :: res) rest
    in
    aux [] stmts

let register_overlap is64 stmts =
  let rec aux res = function
    | [] -> List.rev res
    | stmt :: rest ->
        let stmts =
          match stmt with
          | Il.Assign (dst, src) -> (
            match dst with
            | Il.Var v when Il.Var_hint.(equal v.hint Reg) -> (
                let make_gp8 _8l _8h _16 _32 _64 =
                  Il.[stmt; _16 := _8h @@ _8l; _32 := hi 16 _32 @@ _16]
                  @ if is64 then Il.[_64 := hi 32 _64 @@ _32] else []
                in
                let make_gp16 _8l _16 _32 _64 =
                  (stmt :: (if is64 then Il.[_8l := lo 8 _16] else []))
                  @ Il.[_32 := hi 16 _32 @@ _16]
                  @ if is64 then Il.[_64 := hi 32 _64 @@ _32] else []
                in
                let make_gp16h _8l _8h _16 _32 _64 =
                  Il.
                    [ stmt
                    ; _8l := lo 8 _16
                    ; _8h := hi 8 _16
                    ; _32 := hi 16 _32 @@ _16 ]
                  @ if is64 then Il.[_64 := hi 32 _64 @@ _32] else []
                in
                let make_gp32 _8l _16 _32 _64 =
                  Il.[stmt; _16 := lo 16 _32]
                  @
                  if is64 then Il.[_8l := lo 8 _16; _64 := hi 32 _64 @@ _32]
                  else []
                in
                let make_gp32h _8l _8h _16 _32 _64 =
                  Il.
                    [stmt; _16 := lo 16 _32; _8l := lo 8 _16; _8h := hi 8 _16]
                  @ if is64 then Il.[_64 := hi 32 _64 @@ _32] else []
                in
                let make_gp64 _8l _16 _32 _64 =
                  Il.
                    [ stmt
                    ; _32 := lo 32 _64
                    ; _16 := lo 16 _32
                    ; _8l := lo 8 _16 ]
                in
                let make_gp64h _8l _8h _16 _32 _64 =
                  make_gp64 _8l _16 _32 _64 @ Il.[_8h := hi 8 _16]
                in
                let make_x87 st mmx = Il.[stmt; mmx := lo 64 st] in
                let make_mmx mmx st = Il.[stmt; st := hi 16 st @@ mmx] in
                let make_xmm xmm ymm zmm =
                  Il.
                    [stmt; ymm := hi 128 ymm @@ xmm; zmm := hi 256 zmm @@ ymm]
                in
                let make_ymm xmm ymm zmm =
                  Il.[stmt; xmm := lo 128 ymm; zmm := hi 256 zmm @@ ymm]
                in
                let make_zmm xmm ymm zmm =
                  Il.[stmt; ymm := lo 256 zmm; xmm := lo 128 ymm]
                in
                match R.of_string v.name with
                | None -> [stmt]
                | Some r -> (
                  match r with
                  | R.AL | R.AH -> make_gp8 al ah ax eax rax
                  | R.CL | R.CH -> make_gp8 cl ch cx ecx rcx
                  | R.DL | R.DH -> make_gp8 dl dh dx edx rdx
                  | R.BL | R.BH -> make_gp8 bl bh bx ebx rbx
                  | R.SPL -> make_gp8 spl Il.(hi 8 sp) sp esp rsp
                  | R.BPL -> make_gp8 bpl Il.(hi 8 bp) bp ebp rbp
                  | R.SIL -> make_gp8 sil Il.(hi 8 si) si esi rsi
                  | R.DIL -> make_gp8 dil Il.(hi 8 di) di edi rdi
                  | R.R8B -> make_gp8 r8b Il.(hi 8 r8w) r8w r8d r8
                  | R.R9B -> make_gp8 r9b Il.(hi 8 r9w) r9w r9d r9
                  | R.R10B -> make_gp8 r10b Il.(hi 8 r10w) r10w r10d r10
                  | R.R11B -> make_gp8 r11b Il.(hi 8 r11w) r11w r11d r11
                  | R.R12B -> make_gp8 r12b Il.(hi 8 r12w) r12w r12d r12
                  | R.R13B -> make_gp8 r13b Il.(hi 8 r13w) r13w r13d r13
                  | R.R14B -> make_gp8 r14b Il.(hi 8 r14w) r14w r14d r14
                  | R.R15B -> make_gp8 r15b Il.(hi 8 r15w) r15w r15d r15
                  | R.AX -> make_gp16h al ah ax eax rax
                  | R.CX -> make_gp16h cl ch cx ecx rcx
                  | R.DX -> make_gp16h dl dh dx edx rdx
                  | R.BX -> make_gp16h bl bh bx ebx rbx
                  | R.SP -> make_gp16 spl sp esp rsp
                  | R.BP -> make_gp16 bpl bp ebp rbp
                  | R.SI -> make_gp16 sil si esi rsi
                  | R.DI -> make_gp16 dil di edi rdi
                  | R.R8W -> make_gp16 r8b r8w r8d r8
                  | R.R9W -> make_gp16 r9b r9w r9d r9
                  | R.R10W -> make_gp16 r10b r10w r10d r10
                  | R.R11W -> make_gp16 r11b r11w r11d r11
                  | R.R12W -> make_gp16 r12b r12w r12d r12
                  | R.R13W -> make_gp16 r13b r13w r13d r13
                  | R.R14W -> make_gp16 r14b r14w r14d r14
                  | R.R15W -> make_gp16 r15b r15w r15d r15
                  | R.EAX -> make_gp32h al ah ax eax rax
                  | R.ECX -> make_gp32h cl ch cx ecx rcx
                  | R.EDX -> make_gp32h dl dh dx edx rdx
                  | R.EBX -> make_gp32h bl bh bx ebx rbx
                  | R.ESP -> make_gp32 spl sp esp rsp
                  | R.EBP -> make_gp32 bpl bp ebp rbp
                  | R.ESI -> make_gp32 sil si esi rsi
                  | R.EDI -> make_gp32 dil di edi rdi
                  | R.R8D -> make_gp32 r8b r8w r8d r8
                  | R.R9D -> make_gp32 r9b r9w r9d r9
                  | R.R10D -> make_gp32 r10b r10w r10d r10
                  | R.R11D -> make_gp32 r11b r11w r11d r11
                  | R.R12D -> make_gp32 r12b r12w r12d r12
                  | R.R13D -> make_gp32 r13b r13w r13d r13
                  | R.R14D -> make_gp32 r14b r14w r14d r14
                  | R.R15D -> make_gp32 r15b r15w r15d r15
                  | R.RAX -> make_gp64h al ah ax eax rax
                  | R.RCX -> make_gp64h cl ch cx ecx rcx
                  | R.RDX -> make_gp64h dl dh dx edx rdx
                  | R.RBX -> make_gp64h bl bh bx ebx rbx
                  | R.RSP -> make_gp64 spl sp esp rsp
                  | R.RBP -> make_gp64 bpl bp ebp rbp
                  | R.RSI -> make_gp64 sil si esi rsi
                  | R.RDI -> make_gp64 dil di edi rdi
                  | R.R8 -> make_gp64 r8b r8w r8d r8
                  | R.R9 -> make_gp64 r9b r9w r9d r9
                  | R.R10 -> make_gp64 r10b r10w r10d r10
                  | R.R11 -> make_gp64 r11b r11w r11d r11
                  | R.R12 -> make_gp64 r12b r12w r12d r12
                  | R.R13 -> make_gp64 r13b r13w r13d r13
                  | R.R14 -> make_gp64 r14b r14w r14d r14
                  | R.R15 -> make_gp64 r15b r15w r15d r15
                  | R.ST0 -> make_x87 st0 mm7
                  | R.ST1 -> make_x87 st1 mm6
                  | R.ST2 -> make_x87 st2 mm5
                  | R.ST3 -> make_x87 st3 mm4
                  | R.ST4 -> make_x87 st4 mm3
                  | R.ST5 -> make_x87 st5 mm2
                  | R.ST6 -> make_x87 st6 mm1
                  | R.ST7 -> make_x87 st7 mm0
                  | R.MM0 -> make_mmx mm0 st7
                  | R.MM1 -> make_mmx mm1 st6
                  | R.MM2 -> make_mmx mm2 st5
                  | R.MM3 -> make_mmx mm3 st4
                  | R.MM4 -> make_mmx mm4 st3
                  | R.MM5 -> make_mmx mm5 st2
                  | R.MM6 -> make_mmx mm6 st1
                  | R.MM7 -> make_mmx mm7 st0
                  | R.XMM0 -> make_xmm xmm0 ymm0 zmm0
                  | R.XMM1 -> make_xmm xmm1 ymm1 zmm1
                  | R.XMM2 -> make_xmm xmm2 ymm2 zmm2
                  | R.XMM3 -> make_xmm xmm3 ymm3 zmm3
                  | R.XMM4 -> make_xmm xmm4 ymm4 zmm4
                  | R.XMM5 -> make_xmm xmm5 ymm5 zmm5
                  | R.XMM6 -> make_xmm xmm6 ymm6 zmm6
                  | R.XMM7 -> make_xmm xmm7 ymm7 zmm7
                  | R.XMM8 -> make_xmm xmm8 ymm8 zmm8
                  | R.XMM9 -> make_xmm xmm9 ymm9 zmm9
                  | R.XMM10 -> make_xmm xmm10 ymm10 zmm10
                  | R.XMM11 -> make_xmm xmm11 ymm11 zmm11
                  | R.XMM12 -> make_xmm xmm12 ymm12 zmm12
                  | R.XMM13 -> make_xmm xmm13 ymm13 zmm13
                  | R.XMM14 -> make_xmm xmm14 ymm14 zmm14
                  | R.XMM15 -> make_xmm xmm15 ymm15 zmm15
                  | R.XMM16 -> make_xmm xmm16 ymm16 zmm16
                  | R.XMM17 -> make_xmm xmm17 ymm17 zmm17
                  | R.XMM18 -> make_xmm xmm18 ymm19 zmm18
                  | R.XMM19 -> make_xmm xmm19 ymm19 zmm19
                  | R.XMM20 -> make_xmm xmm20 ymm20 zmm20
                  | R.XMM21 -> make_xmm xmm21 ymm21 zmm21
                  | R.XMM22 -> make_xmm xmm22 ymm22 zmm22
                  | R.XMM23 -> make_xmm xmm23 ymm23 zmm23
                  | R.XMM24 -> make_xmm xmm24 ymm24 zmm24
                  | R.XMM25 -> make_xmm xmm25 ymm25 zmm25
                  | R.XMM26 -> make_xmm xmm26 ymm26 zmm26
                  | R.XMM27 -> make_xmm xmm27 ymm27 zmm27
                  | R.XMM28 -> make_xmm xmm28 ymm28 zmm28
                  | R.XMM29 -> make_xmm xmm29 ymm29 zmm29
                  | R.XMM30 -> make_xmm xmm30 ymm30 zmm30
                  | R.XMM31 -> make_xmm xmm31 ymm31 zmm31
                  | R.YMM10 -> make_ymm xmm10 ymm10 zmm10
                  | R.YMM11 -> make_ymm xmm11 ymm11 zmm11
                  | R.YMM12 -> make_ymm xmm12 ymm12 zmm12
                  | R.YMM13 -> make_ymm xmm13 ymm13 zmm13
                  | R.YMM14 -> make_ymm xmm14 ymm14 zmm14
                  | R.YMM15 -> make_ymm xmm15 ymm15 zmm15
                  | R.YMM16 -> make_ymm xmm16 ymm16 zmm16
                  | R.YMM17 -> make_ymm xmm17 ymm17 zmm17
                  | R.YMM18 -> make_ymm xmm18 ymm19 zmm18
                  | R.YMM19 -> make_ymm xmm19 ymm19 zmm19
                  | R.YMM20 -> make_ymm xmm20 ymm20 zmm20
                  | R.YMM21 -> make_ymm xmm21 ymm21 zmm21
                  | R.YMM22 -> make_ymm xmm22 ymm22 zmm22
                  | R.YMM23 -> make_ymm xmm23 ymm23 zmm23
                  | R.YMM24 -> make_ymm xmm24 ymm24 zmm24
                  | R.YMM25 -> make_ymm xmm25 ymm25 zmm25
                  | R.YMM26 -> make_ymm xmm26 ymm26 zmm26
                  | R.YMM27 -> make_ymm xmm27 ymm27 zmm27
                  | R.YMM28 -> make_ymm xmm28 ymm28 zmm28
                  | R.YMM29 -> make_ymm xmm29 ymm29 zmm29
                  | R.YMM30 -> make_ymm xmm30 ymm30 zmm30
                  | R.YMM31 -> make_ymm xmm31 ymm31 zmm31
                  | R.ZMM10 -> make_zmm xmm10 ymm10 zmm10
                  | R.ZMM11 -> make_zmm xmm11 ymm11 zmm11
                  | R.ZMM12 -> make_zmm xmm12 ymm12 zmm12
                  | R.ZMM13 -> make_zmm xmm13 ymm13 zmm13
                  | R.ZMM14 -> make_zmm xmm14 ymm14 zmm14
                  | R.ZMM15 -> make_zmm xmm15 ymm15 zmm15
                  | R.ZMM16 -> make_zmm xmm16 ymm16 zmm16
                  | R.ZMM17 -> make_zmm xmm17 ymm17 zmm17
                  | R.ZMM18 -> make_zmm xmm18 ymm19 zmm18
                  | R.ZMM19 -> make_zmm xmm19 ymm19 zmm19
                  | R.ZMM20 -> make_zmm xmm20 ymm20 zmm20
                  | R.ZMM21 -> make_zmm xmm21 ymm21 zmm21
                  | R.ZMM22 -> make_zmm xmm22 ymm22 zmm22
                  | R.ZMM23 -> make_zmm xmm23 ymm23 zmm23
                  | R.ZMM24 -> make_zmm xmm24 ymm24 zmm24
                  | R.ZMM25 -> make_zmm xmm25 ymm25 zmm25
                  | R.ZMM26 -> make_zmm xmm26 ymm26 zmm26
                  | R.ZMM27 -> make_zmm xmm27 ymm27 zmm27
                  | R.ZMM28 -> make_zmm xmm28 ymm28 zmm28
                  | R.ZMM29 -> make_zmm xmm29 ymm29 zmm29
                  | R.ZMM30 -> make_zmm xmm30 ymm30 zmm30
                  | R.ZMM31 -> make_zmm xmm31 ymm31 zmm31
                  | _ -> [stmt] ) )
            | _ -> [stmt] )
          | _ -> [stmt]
        in
        aux (List.rev stmts @ res) rest
  in
  aux [] stmts

let simd_cmp_op (instr : I.t) i lhs rhs =
  let op = instr.operands.(i) in
  match op.value with
  | OV.Immediate imm when op.size = 8 -> (
      let n = Int64.to_int_exn imm.value in
      let n =
        match instr.avx with
        | None -> n land 0b111
        | Some _ -> n land 0b11111
      in
      match n with
      (* ignore signaling for now *)
      | 0x00 | 0x10 -> Ok Il.(lhs =. rhs)
      | 0x01 | 0x11 -> Ok Il.(lhs <. rhs)
      | 0x02 | 0x12 -> Ok Il.(lhs <=. rhs)
      | 0x03 | 0x13 -> Ok Il.(lhs <=>. rhs)
      | 0x04 | 0x14 -> Ok Il.(lhs <>. rhs || lhs <=>. rhs)
      | 0x05 | 0x15 -> Ok Il.(lhs >=. rhs || lhs <=>. rhs)
      | 0x06 | 0x16 -> Ok Il.(lhs >. rhs || lhs <=>. rhs)
      | 0x07 | 0x17 -> Ok Il.(~~(lhs <=>. rhs))
      | 0x08 | 0x18 -> Ok Il.(lhs =. rhs || lhs <=>. rhs)
      | 0x09 | 0x19 -> Ok Il.(lhs <. rhs || lhs <=>. rhs)
      | 0x0A | 0x1A -> Ok Il.(lhs <=. rhs || lhs <=>. rhs)
      | 0x0B | 0x1B -> Ok Il.(bit false)
      | 0x0C | 0x1C -> Ok Il.(lhs <>. rhs)
      | 0x0D | 0x1D -> Ok Il.(lhs >=. rhs)
      | 0x0E | 0x1E -> Ok Il.(lhs >. rhs)
      | 0x0F | 0x1F -> Ok Il.(bit true)
      | _ -> Error (`BadOperand i) )
  | _ -> Error (`BadOperand i)

let lift (instr : I.t) addr ctx =
  let open Result.Let_syntax in
  let assign_rm dst src =
    match dst with
    | Il.Load r -> Il.(mu := store_r r src)
    | _ -> Il.(dst := src)
  in
  let wordsz = X86.Mode.word_size_of instr.mode in
  let is64 = X86.Mode.(equal instr.mode AMD64) in
  let sp' = if is64 then rsp else esp in
  let bp' = if is64 then rbp else ebp in
  let endaddr = I.end_addr instr ~addr in
  let new_label () = Il.Context.label ctx addr in
  let new_tmp size = Il.Context.tmp ctx size in
  let start_lbl = new_label () in
  let%map stmts =
    match instr.mnemonic with
    | M.AAA | M.AAS ->
        if is64 then Error `BadInstr
        else
          let tl = new_label () in
          let fl = new_label () in
          let cond = Il.((al & num 0x0F 8) > num 9 8 || af) in
          if M.(equal instr.mnemonic AAA) then
            let jl = new_label () in
            Ok
              Il.
                [ goto_if cond tl fl
                ; ( @ ) tl
                ; ax := ax + num 0x106 16
                ; cf := bit true
                ; af := bit true
                ; goto jl
                ; ( @ ) fl
                ; cf := bit false
                ; af := bit false
                ; goto jl
                ; ( @ ) jl
                ; al := al & num 0x0F 8
                ; oF := undefined 1
                ; sf := undefined 1
                ; zf := undefined 1
                ; pf := undefined 1 ]
          else
            Ok
              Il.
                [ goto_if cond tl fl
                ; ( @ ) tl
                ; ax := ax - num 0x106 16
                ; cf := bit true
                ; af := bit true
                ; al := al & num 0x0F 8
                ; jmp (bitv endaddr wordsz)
                ; ( @ ) fl
                ; cf := bit false
                ; af := bit false
                ; al := al & num 0x0F 8 ]
    | M.AAD ->
        if is64 then Error `BadInstr
        else
          let%bind imm8 = make_operand instr addr 0 in
          Ok Il.[al := (al + (ah * imm8)) & num 0xFF 8; ah := num 0 8]
    | M.AAM ->
        if is64 then Error `BadInstr
        else
          let%bind imm8 = make_operand instr addr 0 in
          Ok Il.[ah := al / imm8; al := al % imm8]
    | M.ADC | M.ADD | M.SBB | M.SUB | M.XADD ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand instr addr 1 in
        let is_add =
          match instr.mnemonic with
          | M.ADC | M.ADD | M.XADD -> true
          | _ -> false
        in
        let res =
          if is_add then
            if M.(equal instr.mnemonic ADC) then Il.(lhs + rhs + ze size cf)
            else Il.(lhs + rhs)
          else if M.(equal instr.mnemonic SBB) then
            Il.(lhs - rhs - ze size cf)
          else Il.(lhs - rhs)
        in
        let t = new_tmp size in
        let t_stmt = Il.(t := res) in
        let new_cf =
          if is_add then Il.(t < lhs)
          else if M.(equal instr.mnemonic SBB) then
            Il.(lhs < rhs + ze size cf)
          else Il.(lhs < rhs)
        in
        let cf_stmt = Il.(cf := new_cf) in
        let oF_stmt = arith_overflow lhs rhs t size in
        let af_stmt = arith_adjust lhs rhs t size in
        let res_stmts =
          if M.(equal instr.mnemonic XADD) then
            Il.[rhs := lhs] @ [assign_rm lhs t]
          else [assign_rm lhs t]
        in
        let pf_stmts = arith_parity lhs size ctx in
        let zf_stmt = Il.(zf := lhs = num 0 size) in
        let sf_stmt = Il.(sf := hi 1 lhs) in
        Ok
          ( (t_stmt :: cf_stmt :: oF_stmt :: af_stmt :: res_stmts)
          @ [zf_stmt; sf_stmt] @ pf_stmts )
    | M.ADCX | M.ADOX ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let flg = if M.(equal instr.mnemonic ADCX) then cf else oF in
        let res = Il.(lhs + rhs + ze size flg) in
        let t = new_tmp size in
        Ok Il.[t := res; flg := t < lhs; lhs := t]
    | M.ADDPD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 64 dst +. lo 64 src
            ; t2 := hi 64 dst +. hi 64 src
            ; dst := t2 @@ t1 ]
    | M.ADDPS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 dst +. lo 32 src
            ; t2 := ex 63 32 dst +. ex 63 32 src
            ; t3 := ex 95 64 dst +. ex 95 64 src
            ; t4 := hi 32 dst +. hi 32 src
            ; dst := t4 @@ t3 @@ t2 @@ t1 ]
    | M.ADDSD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 64 src
        in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := lo 64 dst +. src; dst := hi 64 dst @@ t1]
    | M.ADDSS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 32 src
        in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 dst +. src; dst := hi 96 dst @@ t1]
    | M.AND | M.OR ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand instr addr 1 in
        let res =
          if M.(equal instr.mnemonic AND) then Il.(lhs & rhs)
          else Il.(lhs || rhs)
        in
        let res_stmt = assign_rm lhs res in
        let cf_stmt = Il.(cf := num 0 1) in
        let pf_stmts = arith_parity lhs size ctx in
        Ok
          ( res_stmt :: cf_stmt
            :: Il.
                 [ sf := hi 1 lhs
                 ; zf := lhs = num 0 size
                 ; oF := bit false
                 ; af := undefined 1 ]
          @ pf_stmts )
    | M.ANDN ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src1 = make_operand_rm instr addr 1 in
        let%bind src2 = make_operand_rm instr addr 2 in
        Ok
          Il.
            [ dst := ~~src1 & src2
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; oF := bit false
            ; cf := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BEXTR ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src1 = make_operand_rm instr addr 1 in
        let t512 = new_tmp 512 in
        let%bind src2_stmts, dst_stmts =
          match instr.immediate with
          | Some (Imm.One imm) ->
              let s = Int.(of_int64_exn imm.value land 0xFF) in
              let l = Int.((of_int64_exn imm.value land 0xFF00) lsr 8) in
              Ok ([], Il.[dst := ze size (ex Int.(s + l - 1) s t512)])
          | _ ->
              let%bind src2 = make_operand_rm instr addr 2 in
              let ts = new_tmp size in
              let tl = new_tmp size in
              let t = new_tmp size in
              let t2 = new_tmp size in
              Ok
                ( Il.[ts := ze size (lo 8 src2); tl := ze size (ex 15 8 src2)]
                , Il.
                    [ t := lo size (t512 >> ts)
                    ; t2 := (num 1 size << tl) - num 1 size
                    ; dst := t & t2 ] )
        in
        Ok
          ( src2_stmts
          @ Il.[t512 := ze 512 src1]
          @ dst_stmts
          @ Il.[zf := dst = num 0 size] )
    | M.BLCFILL ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        Ok
          Il.
            [ t := src + num 1 size
            ; cf := t < src
            ; dst := t & src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLCI ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        Ok
          Il.
            [ t := src + num 1 size
            ; cf := t < src
            ; dst := ~~t || src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLCIC ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        Ok
          Il.
            [ t := src + num 1 size
            ; cf := t < src
            ; dst := t & ~~src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLCMSK ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        Ok
          Il.
            [ t := src + num 1 size
            ; cf := t < src
            ; dst := t ^ src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLCS ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        Ok
          Il.
            [ t := src + num 1 size
            ; cf := t < src
            ; dst := t || src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLSFILL ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        Ok
          Il.
            [ t := src - num 1 size
            ; cf := hi 1 t <> hi 1 src
            ; dst := t || src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; cf := src = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLSI ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        Ok
          Il.
            [ dst := ~-src & src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; cf := src = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLSIC ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        Ok
          Il.
            [ t := src - num 1 size
            ; cf := hi 1 t <> hi 1 src
            ; dst := t || ~~src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLSMSK ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        Ok
          Il.
            [ dst := (src - num 1 size) ^ src
            ; sf := hi 1 dst
            ; zf := dst = num 0 size
            ; cf := src = num 0 size
            ; oF := bit false
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BLSR ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        Ok
          Il.
            [ dst := (src - num 1 size) & src
            ; sf := hi 1 dst
            ; zf := bit false
            ; oF := bit false
            ; cf := src = num 0 size
            ; pf := undefined 1
            ; af := undefined 1 ]
    | M.BT | M.BTC | M.BTR | M.BTS ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs =
          match instr.immediate with
          (* override the size of the immediate *)
          | Some (Imm.One imm) -> Ok Il.(num64 imm.value size)
          | _ -> make_operand instr addr 1
        in
        let cf_stmt =
          match instr.immediate with
          | Some (Imm.One imm) when Int64.(imm.value = 0L) ->
              Il.(cf := lo 1 lhs)
          | _ -> Il.(cf := lo 1 (lhs >> rhs))
        in
        let res_stmts =
          match instr.mnemonic with
          | M.BTC | M.BTR ->
              let mask =
                match instr.immediate with
                | Some (Imm.One imm) ->
                    let value = Int64.(lnot (1L lsl to_int_exn imm.value)) in
                    Il.(num64 value size)
                | _ -> Il.(~~(num 1 size << rhs))
              in
              if M.(equal instr.mnemonic BTC) then
                [assign_rm lhs Il.((lhs & mask) || ze size ~~cf)]
              else [assign_rm lhs Il.(lhs & mask)]
          | M.BTS ->
              let mask =
                match instr.immediate with
                | Some (Imm.One imm) ->
                    let value = Int64.(1L lsl to_int_exn imm.value) in
                    Il.(num64 value size)
                | _ -> Il.(num 1 size << rhs)
              in
              [assign_rm lhs Il.(lhs || mask)]
          | _ -> []
        in
        Ok
          ( (cf_stmt :: res_stmts)
          @ Il.
              [ oF := undefined 1
              ; sf := undefined 1
              ; pf := undefined 1
              ; af := undefined 1 ] )
    | M.BSF | M.BSR ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let s0l = new_label () in
        let sll = new_label () in
        let stl = new_label () in
        let sbl = new_label () in
        let sfl = new_label () in
        let t = new_tmp size in
        let t2 = new_tmp size in
        let t2_stmt = Il.(t2 := src & num 1 size << t) in
        let cond = Il.(t2 = num 0 size) in
        Ok
          Il.
            [ goto_if (src = num 0 size) s0l sll
            ; ( @ ) s0l
            ; zf := bit true
            ; dst := undefined size
            ; jmp (bitv endaddr wordsz)
            ; ( @ ) sll
            ; zf := bit false
            ; ( if M.(equal instr.mnemonic BSF) then t := num 0 size
              else t := num Int.(size - 1) size )
            ; goto stl
            ; ( @ ) stl
            ; t2_stmt
            ; goto_if cond sbl sfl
            ; ( @ ) sbl
            ; ( if M.(equal instr.mnemonic BSF) then t := t + num 1 size
              else t := t - num 1 size )
            ; goto stl
            ; ( @ ) sfl
            ; dst := t ]
    | M.BSWAP ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        if size = 16 then Ok Il.[dst := undefined 16]
        else
          let t = new_tmp size in
          let%bind src =
            match size with
            | 32 -> Ok Il.(lo 8 t @@ ex 15 8 t @@ ex 23 16 t @@ hi 8 t)
            | 64 ->
                Ok
                  Il.(
                    lo 8 t @@ ex 15 8 t @@ ex 23 16 t @@ ex 31 24 t
                    @@ ex 39 32 t @@ ex 47 40 t @@ ex 55 48 t @@ hi 8 t)
            | _ -> Error (`BadOperand 0)
          in
          Ok Il.[t := dst; dst := src]
    | M.BZHI ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src1 = make_operand_rm instr addr 1 in
        let%bind src2 = make_operand_rm instr addr 2 in
        let t1 = new_tmp 8 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := lo 8 src2
            ; dst := src1
            ; goto_if (t1 < num size 8) l1 l2
            ; ( @ ) l1
            ; dst := dst & ((num 1 size << t1) - num 1 size)
            ; goto l2
            ; ( @ ) l2
            ; cf := t1 > num Int.(size - 1) 8 ]
    | M.CALL ->
        let%bind target = make_operand instr addr 0 in
        let is_sp =
          match instr.operands.(0).value with
          | OV.Memory m -> (
            match m.base with
            | Some R.SP | Some R.ESP | Some R.RSP -> true
            | _ -> false )
          | OV.Register R.SP | OV.Register R.ESP | OV.Register R.RSP -> true
          | _ -> false
        in
        let sp_dec = Il.(num Int.(wordsz lsr 3) wordsz) in
        let loc = Il.bitv endaddr wordsz in
        if is_sp then
          let t = new_tmp wordsz in
          Ok
            Il.
              [ t := target
              ; sp' := sp' - sp_dec
              ; mu := store `LE (wordsz lsr 3) mu sp' loc
              ; call t ]
        else
          Ok
            Il.
              [ sp' := sp' - sp_dec
              ; mu := store `LE (wordsz lsr 3) mu sp' loc
              ; call target ]
    | M.CALLF -> (
        let op = instr.operands.(0) in
        match op.value with
        | OV.Pointer p ->
            if is64 then Error `BadInstr
            else
              let sp_dec = Il.(num Int.((op.size - 16) lsr 3) wordsz) in
              let cs_store = if op.size = 48 then Il.(ze 32 cs) else cs in
              let ip_store =
                if op.size = 48 then Il.(bitv endaddr wordsz)
                else Il.(bitv endaddr 16)
              in
              Ok
                Il.
                  [ sp' := sp' - sp_dec
                  ; mu := store `LE (wordsz lsr 3) mu sp' cs_store
                  ; sp' := sp' - sp_dec
                  ; mu := store `LE (wordsz lsr 3) mu sp' ip_store
                  ; cs := num p.segment 16
                  ; call (num32 p.offset wordsz) ]
        | OV.Memory _ ->
            let%bind target = make_operand_rm instr addr 0 in
            let sp_dec = Il.(num Int.((op.size - 16) lsr 3) wordsz) in
            let cs_store, ip_store =
              match op.size with
              | 48 -> (Il.(ze 32 cs), Il.(bitv endaddr 32))
              | 80 -> (Il.(ze 64 cs), Il.(bitv endaddr 64))
              | _ -> (cs, Il.(bitv endaddr 16))
            in
            let t = new_tmp op.size in
            let t' = new_tmp wordsz in
            let dst =
              let offset = op.size - 16 in
              if offset < wordsz then Il.(ze wordsz (lo offset t))
              else Il.(lo offset t)
            in
            Ok
              Il.
                [ sp' := sp' - sp_dec
                ; mu := store `LE (wordsz lsr 3) mu sp' cs_store
                ; sp' := sp' - sp_dec
                ; mu := store `LE (wordsz lsr 3) mu sp' ip_store
                ; t := target
                ; cs := hi 16 t
                ; t' := dst
                ; call t' ]
        | _ -> Error (`BadOperand 0) )
    | M.CBW -> Ok Il.[ax := se 16 al]
    | M.CDQ -> Ok Il.[edx := se 32 (hi 1 eax)]
    | M.CDQE -> Ok Il.[rax := se 64 eax]
    | M.CQO -> Ok Il.[rdx := se 64 (hi 1 rax)]
    | M.CWD -> Ok Il.[dx := se 16 (hi 1 ax)]
    | M.CWDE -> Ok Il.[eax := se 32 ax]
    | M.CLAC -> Ok Il.[ac := bit false]
    | M.CLC -> Ok Il.[cf := bit false]
    | M.CLD -> Ok Il.[df := bit false]
    | M.CLI -> Ok Il.[iF := bit false]
    | M.CLTS ->
        let mask = Int64.(lnot (1L lsl 3) land 0xFFFFFFFFL) in
        let cr0 = cr0 wordsz in
        Ok Il.[cr0 := cr0 & num64 mask wordsz]
    | M.CMC -> Ok Il.[cf := ~~cf]
    | M.CMOVB
     |M.CMOVBE
     |M.CMOVL
     |M.CMOVLE
     |M.CMOVNB
     |M.CMOVNBE
     |M.CMOVNL
     |M.CMOVNLE
     |M.CMOVNO
     |M.CMOVNP
     |M.CMOVNS
     |M.CMOVNZ
     |M.CMOVO
     |M.CMOVP
     |M.CMOVS
     |M.CMOVZ ->
        let%bind cond = fcond_of_mnemonic instr.mnemonic in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let tl = new_label () in
        let fl = new_label () in
        let res_stmt = assign_rm lhs rhs in
        Ok Il.[goto_if cond tl fl; ( @ ) tl; res_stmt; goto fl; ( @ ) fl]
    | M.CMP ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand instr addr 1 in
        let cf_stmt = Il.(cf := lhs < rhs) in
        let zf_stmt = Il.(zf := lhs = rhs) in
        let sf_stmt = Il.(sf := lhs <$ rhs) in
        let sub = Il.(lhs - rhs) in
        let t = new_tmp size in
        let t_stmt = Il.(t := sub) in
        let oF_stmt = arith_overflow lhs rhs t size in
        let af_stmt = arith_adjust lhs rhs t size in
        let pf_stmts = arith_parity t size ctx in
        Ok
          ( cf_stmt :: zf_stmt :: sf_stmt :: t_stmt :: oF_stmt :: af_stmt
          :: pf_stmts )
    | M.CMPPD ->
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let%bind cmp1 = simd_cmp_op instr 2 Il.(lo 64 lhs) Il.(lo 64 rhs) in
        let%bind cmp2 = simd_cmp_op instr 2 Il.(hi 64 lhs) Il.(hi 64 rhs) in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        Ok Il.[t1 := se 64 cmp1; t2 := se 64 cmp2; lhs := t2 @@ t1]
    | M.CMPPS ->
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let%bind cmp1 = simd_cmp_op instr 2 Il.(lo 32 lhs) Il.(lo 32 rhs) in
        let%bind cmp2 =
          simd_cmp_op instr 2 Il.(ex 63 32 lhs) Il.(ex 63 32 rhs)
        in
        let%bind cmp3 =
          simd_cmp_op instr 2 Il.(ex 95 64 lhs) Il.(ex 95 64 rhs)
        in
        let%bind cmp4 = simd_cmp_op instr 2 Il.(hi 32 lhs) Il.(hi 32 rhs) in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        Ok
          Il.
            [ t1 := se 32 cmp1
            ; t2 := se 32 cmp2
            ; t3 := se 32 cmp3
            ; t4 := se 32 cmp4
            ; lhs := t4 @@ t3 @@ t2 @@ t1 ]
    | M.CMPSB
     |M.LODSB
     |M.MOVSB
     |M.SCASB
     |M.STOSB
     |M.CMPSW
     |M.LODSW
     |M.MOVSW
     |M.SCASW
     |M.STOSW
     |M.CMPSD
     |M.LODSD
     |M.MOVSD
     |M.SCASD
     |M.STOSD
     |M.CMPSQ
     |M.LODSQ
     |M.MOVSQ
     |M.SCASQ
     |M.STOSQ
    (* this is for differentiating MOVSD (string) from MOVSD (SSE) *)
      when Array.is_empty instr.operands ->
        let size =
          match instr.mnemonic with
          | M.CMPSB | M.LODSB | M.MOVSB | M.SCASB | M.STOSB -> 8
          | M.CMPSW | M.LODSW | M.MOVSW | M.SCASW | M.STOSW -> 16
          | M.CMPSD | M.LODSD | M.MOVSD | M.SCASD | M.STOSD -> 32
          | _ -> 64
        in
        let si', di' =
          match instr.easz with
          | 16 -> (si, di)
          | 32 -> (esi, edi)
          | _ -> (rsi, rdi)
        in
        let df_inc_dec =
          let sz = Il.num (size lsr 3) instr.easz in
          let sz2 = Il.num ((size lsr 3) * 2) instr.easz in
          Il.(sz - (ze instr.easz df * sz2))
        in
        let cx' =
          match instr.easz with
          | 16 -> cx
          | 32 -> ecx
          | _ -> rcx
        in
        let lhs, rhs =
          match instr.mnemonic with
          | M.CMPSB
           |M.MOVSB
           |M.CMPSW
           |M.MOVSW
           |M.CMPSD
           |M.MOVSD
           |M.CMPSQ
           |M.MOVSQ ->
              ( Il.(load `LE (size lsr 3) mu di')
              , Il.(load `LE (size lsr 3) mu si') )
          (* if is64 then
           *   (Il.(load `LE (size lsr 3) mu di'),
           *    Il.(load `LE (size lsr 3) mu si'))
           * else
           *   (Il.(load `LE (size lsr 3) mu (di' + es_base wordsz)),
           *    Il.(load `LE (size lsr 3) mu (si' + ds_base wordsz))) *)
          | M.LODSB | M.LODSW | M.LODSD | M.LODSQ ->
              let ax' =
                match size with
                | 8 -> al
                | 16 -> ax
                | 32 -> eax
                | _ -> rax
              in
              (ax', Il.(load `LE (size lsr 3) mu si'))
          (* if is64
           * then (ax', Il.(load `LE (size lsr 3) mu si'))
           * else (ax', Il.(load `LE (size lsr 3) mu (si' + ds_base wordsz))) *)
          (* SCAS/STOS *)
          | _ ->
              let is_stos =
                match instr.mnemonic with
                | M.STOSB | M.STOSW | M.STOSD | M.STOSQ -> true
                | _ -> false
              in
              let ax' =
                match size with
                | 8 -> al
                | 16 -> ax
                | 32 -> eax
                | _ -> rax
              in
              let ld = Il.(load `LE (size lsr 3) mu di') in
              if is_stos then (ld, ax') else (ax', ld)
          (* if is64 then
           *   let ld = Il.(load `LE (size lsr 3) mu di') in
           *   if is_stos then (ld, ax') else (ax', ld)
           * else
           *   let ld = Il.(load `LE (size lsr 3) mu (di' + es_base wordsz)) in
           *   if is_stos then (ld, ax') else (ax', ld) *)
        in
        let t2 = new_tmp instr.easz in
        let t2_stmt = Il.(t2 := df_inc_dec) in
        let string_stmts =
          match instr.mnemonic with
          | M.CMPSB
           |M.CMPSW
           |M.CMPSD
           |M.CMPSQ
           |M.SCASB
           |M.SCASW
           |M.SCASD
           |M.SCASQ ->
              let t = new_tmp size in
              let t_stmt = Il.(t := lhs - rhs) in
              let oF_stmt = arith_overflow lhs rhs t size in
              let af_stmt = arith_adjust lhs rhs t size in
              let cf_stmt = Il.(cf := lhs < rhs) in
              let zf_stmt = Il.(zf := lhs = rhs) in
              let sf_stmt = Il.(sf := lhs <$ rhs) in
              let pf_stmts = arith_parity t size ctx in
              let inc_stmts =
                match instr.mnemonic with
                | M.CMPSB | M.CMPSW | M.CMPSD | M.CMPSQ ->
                    Il.[di' := di' + t2; si' := si' + t2]
                | _ -> Il.[di' := di' + t2]
              in
              t_stmt :: oF_stmt :: af_stmt :: cf_stmt :: zf_stmt :: sf_stmt
              :: (pf_stmts @ inc_stmts)
          | M.LODSB | M.LODSW | M.LODSD | M.LODSQ ->
              Il.[lhs := rhs; si' := si' + df_inc_dec]
          | M.MOVSB | M.MOVSW | M.MOVSD | M.MOVSQ ->
              Il.[assign_rm lhs rhs; di' := di' + t2; si' := si' + t2]
          (* STOS *)
          | _ -> Il.[assign_rm lhs rhs; di' := di' + t2]
        in
        let string_stmts = t2_stmt :: string_stmts in
        let is_repe, is_repne =
          ( Array.exists instr.prefixes P.is_repe
          , Array.exists instr.prefixes P.is_repne )
        in
        if is_repe || is_repne then
          let cx_cond = Il.(cx' = num 0 instr.easz) in
          let string_lbl = new_label () in
          let exit_lbl = new_label () in
          Ok
            ( Il.[goto_if cx_cond exit_lbl string_lbl; ( @ ) string_lbl]
            @ string_stmts
            @ Il.[cx' := cx' - num 1 instr.easz]
            @ ( if is_repe then Il.[goto_if ~~zf exit_lbl start_lbl]
              else Il.[goto_if zf exit_lbl start_lbl] )
            @ Il.[( @ ) exit_lbl] )
        else Ok string_stmts
    | M.CMPSD ->
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let%bind cmp = simd_cmp_op instr 2 Il.(lo 64 lhs) Il.(lo 64 rhs) in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := se 64 cmp; lhs := hi 64 lhs @@ t1]
    | M.CMPSS ->
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let%bind cmp = simd_cmp_op instr 2 Il.(lo 32 lhs) Il.(lo 32 rhs) in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := se 32 cmp; lhs := hi 96 lhs @@ t1]
    | M.CMPXCHG ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let ax' =
          match size with
          | 8 -> al
          | 16 -> ax
          | 32 -> eax
          | _ -> rax
        in
        let t1 = new_tmp size in
        let cf_stmt = Il.(cf := ax' < t1) in
        let zf_stmt = Il.(zf := ax' = t1) in
        let sf_stmt = Il.(sf := ax' <$ t1) in
        let sub = Il.(ax' - t1) in
        let t = new_tmp size in
        let t_stmt = Il.(t := sub) in
        let oF_stmt = arith_overflow ax' t1 t size in
        let af_stmt = arith_adjust ax' t1 t size in
        let pf_stmts = arith_parity t size ctx in
        let te = new_label () in
        let fe = new_label () in
        let res_stmt = assign_rm lhs rhs in
        Ok
          ( Il.[t1 := lhs]
          @ cf_stmt :: zf_stmt :: sf_stmt :: t_stmt :: oF_stmt :: af_stmt
            :: pf_stmts
          @ Il.
              [ goto_if zf te fe
              ; ( @ ) te
              ; res_stmt
              ; jmp (bitv endaddr wordsz)
              ; ( @ ) fe
              ; ax' := t1 ] )
    | M.CMPXCHG8B | M.CMPXCHG16B ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let dx', ax', cx', bx' =
          if M.(equal instr.mnemonic CMPXCHG8B) then (edx, eax, ecx, ebx)
          else (rdx, rax, rcx, rbx)
        in
        let t1 = new_tmp size in
        let dxax = Il.(dx' @@ ax') in
        let cf_stmt = Il.(cf := dxax < t1) in
        let zf_stmt = Il.(zf := dxax = t1) in
        let sf_stmt = Il.(sf := dxax <$ t1) in
        let sub = Il.(dxax - t1) in
        let t = new_tmp size in
        let t_stmt = Il.(t := sub) in
        let oF_stmt = arith_overflow dxax t1 t size in
        let af_stmt = arith_adjust dxax t1 t size in
        let pf_stmts = arith_parity t size ctx in
        let te = new_label () in
        let fe = new_label () in
        let res_stmt = assign_rm dst Il.(cx' @@ bx') in
        let hsz = size lsr 1 in
        Ok
          ( Il.[t1 := dst]
          @ cf_stmt :: zf_stmt :: sf_stmt :: t_stmt :: oF_stmt :: af_stmt
            :: pf_stmts
          @ Il.
              [ goto_if zf te fe
              ; ( @ ) te
              ; res_stmt
              ; jmp (bitv endaddr wordsz)
              ; ( @ ) fe
              ; dx' := hi hsz t1
              ; ax' := lo hsz t1 ] )
    | M.COMISD | M.COMISS ->
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let sz = if M.(equal instr.mnemonic COMISD) then 64 else 32 in
        let t1 = new_tmp sz in
        let t2 = new_tmp sz in
        Ok
          Il.
            [ t1 := lo sz lhs
            ; t2 := lo sz rhs
            ; oF := bit false
            ; af := bit false
            ; sf := bit false
            ; pf := t1 <=>. t2
            ; zf := pf || t1 =. t2
            ; cf := pf || t1 <. t2 ]
    | M.CVTDQ2PD | M.CVTPI2PD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        Ok
          Il.
            [ t1 := itof 64 (lo 32 src)
            ; t2 := itof 64 (ex 63 32 src)
            ; dst := t2 @@ t1 ]
    | M.CVTDQ2PS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        Ok
          Il.
            [ t1 := itof 32 (lo 32 src)
            ; t2 := itof 32 (ex 63 32 src)
            ; t3 := itof 32 (ex 95 64 src)
            ; t4 := itof 32 (hi 32 src)
            ; dst := t4 @@ t3 @@ t2 @@ t1 ]
    | M.CVTPD2DQ ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := ftoi 32 (lo 64 src)
            ; t2 := ftoi 32 (hi 64 src)
            ; dst := num 0 64 @@ t2 @@ t1 ]
    | M.CVTPD2PI ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := ftoi 32 (lo 64 src)
            ; t2 := ftoi 32 (hi 64 src)
            ; dst := t2 @@ t1 ]
    | M.CVTPD2PS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := ftof 32 (lo 64 src)
            ; t2 := ftof 32 (hi 64 src)
            ; dst := num 0 64 @@ t2 @@ t1 ]
    | M.CVTPI2PS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := itof 32 (lo 32 src)
            ; t2 := itof 32 (hi 32 src)
            ; dst := hi 64 dst @@ t2 @@ t1 ]
    | M.CVTPS2DQ ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        Ok
          Il.
            [ t1 := ftoi 32 (lo 32 src)
            ; t2 := ftoi 32 (ex 63 32 src)
            ; t3 := ftoi 32 (ex 95 64 src)
            ; t4 := ftoi 32 (hi 32 src)
            ; dst := t4 @@ t3 @@ t2 @@ t1 ]
    | M.CVTPS2PD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        Ok
          Il.
            [ t1 := ftof 64 (lo 32 src)
            ; t2 := ftof 64 (ex 63 32 src)
            ; dst := t2 @@ t1 ]
    | M.CVTPS2PI ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := ftoi 32 (lo 32 src)
            ; t2 := ftoi 32 (ex 63 32 src)
            ; dst := t2 @@ t1 ]
    | M.CVTSD2SI ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 64 src
        in
        Ok Il.[dst := ftoi size src]
    | M.CVTSD2SS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 64 src
        in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := ftof 32 src; dst := hi 96 dst @@ t1]
    | M.CVTSI2SD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := itof 64 src; dst := hi 64 dst @@ t1]
    | M.CVTSI2SS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := itof 32 src; dst := hi 96 dst @@ t1]
    | M.CVTSS2SD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 32 src
        in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := ftof 64 src; dst := hi 64 dst @@ t1]
    | M.CVTSS2SI ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 32 src
        in
        Ok Il.[dst := ftoi size src]
    | M.CVTTPD2DQ | M.CVTTPD2PI ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t1' = new_tmp 32 in
        let t2' = new_tmp 32 in
        Ok
          Il.
            [ t1 := ftrunc (lo 64 src)
            ; t2 := ftrunc (ex 63 32 src)
            ; t1' := ftoi 32 t1
            ; t2' := ftoi 32 t2
            ; ( if Int.equal size 128 then dst := num 0 64 @@ t2' @@ t1'
              else dst := t2' @@ t1' ) ]
    | M.CVTTPS2DQ ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t1' = new_tmp 32 in
        let t2' = new_tmp 32 in
        let t3' = new_tmp 32 in
        let t4' = new_tmp 32 in
        Ok
          Il.
            [ t1 := ftrunc (lo 32 src)
            ; t2 := ftrunc (ex 63 32 src)
            ; t3 := ftrunc (ex 95 64 src)
            ; t4 := ftrunc (hi 32 src)
            ; t1' := ftoi 32 t1
            ; t2' := ftoi 32 t2
            ; t3' := ftoi 32 t3
            ; t4' := ftoi 32 t4
            ; dst := t4' @@ t3' @@ t2' @@ t1' ]
    | M.CVTTPS2PI ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t1' = new_tmp 32 in
        let t2' = new_tmp 32 in
        Ok
          Il.
            [ t1 := ftrunc (lo 32 src)
            ; t2 := ftrunc (ex 63 32 src)
            ; t1' := ftoi 32 t1
            ; t2' := ftoi 32 t2
            ; dst := t2' @@ t1' ]
    | M.CVTTSD2SI ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 64 src
        in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := ftrunc src; dst := ftoi size t1]
    | M.CVTTSS2SI ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 32 src
        in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := ftrunc src; dst := ftoi size t1]
    | M.DEC | M.INC ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let rhs = Il.num 1 size in
        let res =
          if M.(equal instr.mnemonic DEC) then Il.(lhs - rhs)
          else Il.(lhs + rhs)
        in
        let t = new_tmp size in
        let t_stmt = Il.(t := res) in
        let oF_stmt = arith_overflow lhs rhs t size in
        let af_stmt = arith_adjust lhs rhs t size in
        let res_stmt = assign_rm lhs t in
        let zf_stmt = Il.(zf := lhs = num 0 size) in
        let sf_stmt = Il.(sf := hi 1 lhs) in
        let pf_stmts = arith_parity lhs size ctx in
        Ok
          ( t_stmt :: oF_stmt :: af_stmt :: res_stmt :: zf_stmt :: sf_stmt
          :: pf_stmts )
    | M.DAA | M.DAS ->
        if is64 then Error `BadInstr
        else
          let tal = new_tmp 8 in
          let tcf = new_tmp 1 in
          let l1 = new_label () in
          let l2 = new_label () in
          let l3 = new_label () in
          let l4 = new_label () in
          let l5 = new_label () in
          let c1 = Il.((al & num 0x0F 8) > num 9 8 || af) in
          let c2 = Il.(tal > num 0x99 8 || tcf) in
          let is_add = M.(equal instr.mnemonic DAA) in
          Ok
            Il.
              [ tal := al
              ; tcf := cf
              ; cf := bit false
              ; goto_if c1 l1 l2
              ; ( @ ) l1
              ; (if is_add then al := al + num 6 8 else al := al - num 6 8)
              ; cf := tcf || hi 1 tal <> hi 1 al
              ; af := bit true
              ; goto l3
              ; ( @ ) l2
              ; af := bit false
              ; goto l3
              ; ( @ ) l3
              ; goto_if c2 l4 l5
              ; ( @ ) l4
              ; ( if is_add then al := al + num 0x60 8
                else al := al - num 0x60 8 )
              ; cf := bit true
              ; jmp (bitv endaddr wordsz)
              ; ( @ ) l5
              ; cf := bit false ]
    | M.DIV | M.IDIV ->
        let size = instr.operands.(0).size in
        let%bind src = make_operand_rm instr addr 0 in
        let signed = M.(equal instr.mnemonic IDIV) in
        let tsz = size * 2 in
        let t = new_tmp tsz in
        let l1 = new_label () in
        let l2 = new_label () in
        let res_stmts =
          if size = 8 then
            if signed then
              Il.
                [ t := ax /$ se 16 src
                ; ah := lo 8 (ax %$ se 16 src)
                ; al := lo 8 t ]
            else
              Il.
                [ t := ax / ze 16 src
                ; ah := lo 8 (ax % se 16 src)
                ; al := lo 8 t ]
          else
            let dx', ax' =
              match size with
              | 16 -> (dx, ax)
              | 32 -> (edx, eax)
              | _ -> (rdx, rax)
            in
            if signed then
              Il.
                [ t := (dx' @@ ax') /$ se tsz src
                ; dx' := lo size ((dx' @@ ax') %$ se tsz src)
                ; ax' := lo size t ]
            else
              Il.
                [ t := (dx' @@ ax') / ze tsz src
                ; dx' := lo size ((dx' @@ ax') % ze tsz src)
                ; ax' := lo size t ]
        in
        Ok
          ( Il.[goto_if (src = num 0 size) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
          @ res_stmts
          @ Il.
              [ oF := undefined 1
              ; cf := undefined 1
              ; pf := undefined 1
              ; zf := undefined 1
              ; sf := undefined 1
              ; af := undefined 1 ] )
    | M.DIVPD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 64 dst /. lo 64 src
            ; t2 := hi 64 dst /. hi 64 src
            ; dst := t2 @@ t1 ]
    | M.DIVPS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 dst /. lo 32 src
            ; t2 := ex 63 32 dst /. ex 63 32 src
            ; t3 := ex 95 64 dst /. ex 95 64 src
            ; t4 := hi 32 dst /. hi 32 src
            ; dst := t4 @@ t3 @@ t2 @@ t1 ]
    | M.DIVSD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 64 src
        in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := lo 64 dst /. src; dst := hi 64 dst @@ t1]
    | M.DIVSS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let src =
          match instr.operands.(1).value with
          | OV.Memory _ -> src
          | _ -> Il.lo 32 src
        in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 dst /. src; dst := hi 96 dst @@ t1]
    | M.EXTRACTPS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let op2 = instr.operands.(2) in
        let%bind sh =
          match op2.value with
          | OV.Immediate imm when op2.size = 8 ->
              let n = Int64.to_int_exn imm.value in
              Ok (n land 1 * 32)
          | _ -> Error (`BadOperand 2)
        in
        let src = if sh = 0 then src else Il.(src >> num sh 128) in
        let src = Il.lo 32 src in
        Ok Il.[assign_rm dst src]
    | M.F2XM1 ->
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let n1 = Il.num_big "0xBFFF8000000000000000" 80 in
        let p1 = Il.num_big "0x3FFF8000000000000000" 80 in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.
              [ ( @ ) l2
              ; goto_if (st0 <. n1 || st0 >. p1) l3 l4
              ; ( @ ) l3
              ; st0 := undefined 80
              ; jmp (bitv endaddr wordsz)
              ; ( @ ) l4
              ; st0 := exp2 st0 -. p1 ] )
    | M.FABS ->
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c1 := bit false
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1
            ; goto_if (fpu_tag0 = num 3 2) l1 l2
            ; ( @ ) l1
            ; fpu_i := bit true
            ; fpu_sf := bit true
            ; exn ()
            ; ( @ ) l2
            ; st0 := fabs st0 ]
    | M.FADD
     |M.FADDP
     |M.FIADD
     |M.FSUB
     |M.FSUBR
     |M.FSUBP
     |M.FSUBRP
     |M.FISUB
     |M.FISUBR ->
        (* TODO: how do we check if the result is rounded? *)
        let op =
          match instr.mnemonic with
          | M.FADD | M.FADDP | M.FIADD -> Il.( +. )
          | _ -> Il.( -. )
        in
        let is_int =
          match instr.mnemonic with
          | M.FIADD | M.FISUB | M.FISUBR -> true
          | _ -> false
        in
        let is_rev =
          match instr.mnemonic with
          | M.FSUBR | M.FSUBRP | M.FISUBR -> true
          | _ -> false
        in
        let%bind stmts =
          match Array.length instr.operands with
          | 1 ->
              let%bind src = make_operand_rm instr addr 0 in
              let l1 = new_label () in
              let l2 = new_label () in
              let l3 = new_label () in
              let l4 = new_label () in
              let l5 = new_label () in
              let src =
                if is_int then Il.(itof 80 src) else Il.(ftof 80 src)
              in
              let res =
                if is_rev then Il.(st0 := op src st0)
                else Il.(st0 := op st0 src)
              in
              Ok
                ( Il.[goto_if (fpu_tag0 = num 3 2) l1 l2; ( @ ) l1]
                @ fpu_uflow_oflow false
                @ Il.[( @ ) l2; res; fpu_c1 := bit false]
                @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5 )
          | 2 ->
              let%bind dst = make_operand_rm instr addr 0 in
              let%bind src = make_operand_rm instr addr 1 in
              let%bind tag_dst = fpu_tag_of_operand instr 0 in
              let%bind tag_src = fpu_tag_of_operand instr 1 in
              let l1 = new_label () in
              let l2 = new_label () in
              let l3 = new_label () in
              let l4 = new_label () in
              let l5 = new_label () in
              Ok
                ( Il.
                    [ goto_if (tag_dst = num 3 2 || tag_src = num 3 2) l1 l2
                    ; ( @ ) l1 ]
                @ fpu_uflow_oflow false
                @ Il.
                    [ ( @ ) l2
                    ; ( if is_rev then dst := op src dst
                      else dst := op dst src )
                    ; fpu_c1 := bit false ]
                @ fpu_compute_tag tag_dst dst l3 l4 l5 )
          | _ -> Error `BadInstr
        in
        let undef_stmts =
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1 ]
        in
        let pop_stmts =
          match instr.mnemonic with
          | M.FADDP | M.FSUBP -> fpu_pop ctx
          | _ -> []
        in
        Ok (undef_stmts @ stmts @ pop_stmts)
    | M.FCHS ->
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c1 := bit false
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1
            ; goto_if (fpu_tag0 = num 3 2) l1 l2
            ; ( @ ) l1
            ; fpu_i := bit true
            ; fpu_sf := bit true
            ; exn ()
            ; ( @ ) l2
            ; st0 := ~~(hi 1 st0) @@ lo 79 st0 ]
    | M.FCMOVB
     |M.FCMOVBE
     |M.FCMOVE
     |M.FCMOVNB
     |M.FCMOVNBE
     |M.FCMOVNE
     |M.FCMOVNU
     |M.FCMOVU ->
        let%bind fcond = fcond_of_mnemonic instr.mnemonic in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let%bind tag_dst = fpu_tag_of_operand instr 0 in
        let%bind tag_src = fpu_tag_of_operand instr 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let l6 = new_label () in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (tag_dst = num 3 2 || tag_src = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.[( @ ) l2; goto_if fcond l3 l6; ( @ ) l3; dst := src]
          @ fpu_compute_tag tag_dst dst l4 l5 l6 )
    | M.FCOM | M.FCOMP | M.FCOMPP ->
        let%bind src, is_mem =
          let is_mem = Array.length instr.operands = 1 in
          let%bind src =
            if is_mem then make_operand_rm instr addr 0
            else if Array.is_empty instr.operands then Ok st1
            else make_operand_rm instr addr 1
          in
          Ok (src, is_mem)
        in
        let%bind oflow_cond =
          if is_mem then Ok Il.(fpu_tag0 = num 3 2)
          else if Array.is_empty instr.operands then Ok fpu_tag1
          else
            let%bind tag_src = fpu_tag_of_operand instr 1 in
            Ok Il.(fpu_tag0 = num 3 2 || tag_src = num 3 2)
        in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let l6 = new_label () in
        let l7 = new_label () in
        let l8 = new_label () in
        let l9 = new_label () in
        let l10 = new_label () in
        let pop_stmts =
          match instr.mnemonic with
          | M.FCOMP -> fpu_pop ctx
          | M.FCOMPP -> fpu_pop2 ctx
          | _ -> []
        in
        Ok
          ( Il.
              [ fpu_c1 := bit false
              ; goto_if oflow_cond l1 l2
              ; ( @ ) l1
              ; fpu_i := bit true
              ; fpu_sf := bit true
              ; exn ()
              ; ( @ ) l2
              ; goto_if (st0 >. src) l3 l4
              ; ( @ ) l3
              ; fpu_c0 := bit false
              ; fpu_c2 := bit false
              ; fpu_c3 := bit false
              ; goto l8
              ; ( @ ) l4
              ; goto_if (st0 <. src) l5 l6
              ; ( @ ) l5
              ; fpu_c0 := bit true
              ; fpu_c2 := bit false
              ; fpu_c3 := bit false
              ; goto l8
              ; ( @ ) l6
              ; goto_if (st0 =. src) l7 l8
              ; ( @ ) l7
              ; fpu_c0 := bit false
              ; fpu_c2 := bit false
              ; fpu_c3 := bit true
              ; goto l8
              ; ( @ ) l8
              ; goto_if (is_nan st0 || is_nan src) l9 l10
              ; ( @ ) l9
              ; fpu_i := bit true
              ; fpu_c0 := bit true
              ; fpu_c2 := bit true
              ; fpu_c3 := bit true
              ; exn ()
              ; ( @ ) l10 ]
          @ pop_stmts )
    | M.FCOMI | M.FCOMIP | M.FUCOMI | M.FUCOMIP ->
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        let%bind lhs_tag = fpu_tag_of_operand instr 0 in
        let%bind rhs_tag = fpu_tag_of_operand instr 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let l6 = new_label () in
        let l7 = new_label () in
        let l8 = new_label () in
        let pop_stmts =
          match instr.mnemonic with
          | M.FCOMIP | M.FUCOMIP -> fpu_pop ctx
          | _ -> []
        in
        let nan_stmts =
          match instr.mnemonic with
          | M.FCOMI | M.FCOMIP ->
              let l9 = new_label () in
              let l10 = new_label () in
              Il.
                [ goto_if (is_nan lhs || is_nan rhs) l9 l10
                ; ( @ ) l9
                ; fpu_i := bit true
                ; cf := bit true
                ; pf := bit true
                ; zf := bit true
                ; exn ()
                ; ( @ ) l10 ]
              @ pop_stmts
          | _ ->
              let l9 = new_label () in
              let l10 = new_label () in
              let l11 = new_label () in
              Il.
                [ goto_if (is_nan lhs || is_nan rhs) l9 l11
                ; ( @ ) l9
                ; cf := bit true
                ; pf := bit true
                ; zf := bit true
                ; goto_if (ex 64 63 lhs || ex 64 63 rhs) l10 l11
                ; ( @ ) l10
                ; fpu_i := bit true
                ; exn ()
                ; ( @ ) l11 ]
              @ pop_stmts
        in
        Ok
          ( Il.
              [ fpu_c1 := bit false
              ; goto_if (lhs_tag = num 3 2 || rhs_tag = num 3 2) l1 l2
              ; ( @ ) l1
              ; fpu_i := bit true
              ; fpu_sf := bit true
              ; exn ()
              ; ( @ ) l2
              ; goto_if (lhs >. rhs) l3 l4
              ; ( @ ) l3
              ; cf := bit false
              ; pf := bit false
              ; zf := bit false
              ; goto l8
              ; ( @ ) l4
              ; goto_if (lhs <. rhs) l5 l6
              ; ( @ ) l5
              ; cf := bit true
              ; pf := bit false
              ; zf := bit false
              ; goto l8
              ; ( @ ) l6
              ; goto_if (lhs =. rhs) l7 l8
              ; ( @ ) l7
              ; cf := bit false
              ; pf := bit false
              ; zf := bit true
              ; goto l8
              ; ( @ ) l8 ]
          @ nan_stmts )
    | M.FCOS | M.FSIN ->
        (* this code assumes abs(st0) < 2^63 *)
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := bit false
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.
              [ ( @ ) l2
              ; ( if M.(equal instr.mnemonic FCOS) then st0 := cos st0
                else st0 := sin st0 )
              ; fpu_c1 := bit false ]
          @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5 )
    | M.FDECSTP ->
        let t1 = new_tmp 80 in
        let t2 = new_tmp 3 in
        Ok
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c1 := bit false
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1
            ; t1 := st7
            ; t2 := fpu_tag7
            ; st7 := st6
            ; fpu_tag7 := fpu_tag6
            ; st6 := st5
            ; fpu_tag6 := fpu_tag5
            ; st5 := st4
            ; fpu_tag5 := fpu_tag4
            ; st4 := st3
            ; fpu_tag4 := fpu_tag3
            ; st3 := st2
            ; fpu_tag3 := fpu_tag2
            ; st2 := st1
            ; fpu_tag2 := fpu_tag1
            ; st1 := st0
            ; fpu_tag1 := fpu_tag0
            ; st0 := t1
            ; fpu_tag0 := t2
            ; fpu_top := fpu_top - num 1 3 ]
    | M.FDIV | M.FDIVP | M.FIDIV ->
        let neg0 = Il.num_big "0x80000000000000000000" 80 in
        let%bind stmts =
          match Array.length instr.operands with
          | 1 ->
              let%bind src = make_operand_rm instr addr 0 in
              let l1 = new_label () in
              let l2 = new_label () in
              let l3 = new_label () in
              let l4 = new_label () in
              let l5 = new_label () in
              let l6 = new_label () in
              let t = new_tmp 80 in
              Ok
                ( Il.[goto_if (fpu_tag0 = num 3 2) l1 l2; ( @ ) l1]
                @ fpu_uflow_oflow false
                @ Il.
                    [ ( @ ) l2
                    ; fpu_c1 := bit false
                    ; ( if M.(equal instr.mnemonic FDIV) then t := ftof 80 src
                      else t := itof 80 src )
                    ; goto_if (t =. num 0 80 || t =. neg0) l3 l4
                    ; ( @ ) l3
                    ; fpu_z := bit true
                    ; exn ()
                    ; ( @ ) l4
                    ; st0 := st0 /. t ]
                @ fpu_compute_tag fpu_tag0 st0 l4 l5 l6 )
          | 2 ->
              let%bind dst = make_operand_rm instr addr 0 in
              let%bind src = make_operand_rm instr addr 1 in
              let%bind tag_dst = fpu_tag_of_operand instr 0 in
              let%bind tag_src = fpu_tag_of_operand instr 1 in
              let l1 = new_label () in
              let l2 = new_label () in
              let l3 = new_label () in
              let l4 = new_label () in
              let l5 = new_label () in
              let l6 = new_label () in
              Ok
                ( Il.
                    [ goto_if (tag_dst = num 3 2 || tag_src = num 3 2) l1 l2
                    ; ( @ ) l1 ]
                @ fpu_uflow_oflow false
                @ Il.
                    [ ( @ ) l2
                    ; fpu_c1 := bit false
                    ; goto_if (src =. num 0 80 || src =. neg0) l3 l4
                    ; ( @ ) l3
                    ; fpu_z := bit true
                    ; exn ()
                    ; ( @ ) l4
                    ; dst := dst /. src ]
                @ fpu_compute_tag tag_dst dst l4 l5 l6 )
          | _ -> Error `BadInstr
        in
        let undef_stmts =
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1 ]
        in
        let pop_stmts =
          match instr.mnemonic with
          | M.FDIVP -> fpu_pop ctx
          | _ -> []
        in
        Ok (undef_stmts @ stmts @ pop_stmts)
    | M.FDIVR | M.FDIVRP | M.FIDIVR ->
        let neg0 = Il.num_big "0x80000000000000000000" 80 in
        let%bind stmts =
          match Array.length instr.operands with
          | 1 ->
              let%bind src = make_operand_rm instr addr 0 in
              let l1 = new_label () in
              let l2 = new_label () in
              let l3 = new_label () in
              let l4 = new_label () in
              let l5 = new_label () in
              let l6 = new_label () in
              Ok
                ( Il.[goto_if (fpu_tag0 = num 3 2) l1 l2; ( @ ) l1]
                @ fpu_uflow_oflow false
                @ Il.
                    [ ( @ ) l2
                    ; fpu_c1 := bit false
                    ; goto_if (st0 =. num 0 80 || st0 =. neg0) l3 l4
                    ; ( @ ) l3
                    ; fpu_z := bit true
                    ; exn ()
                    ; ( @ ) l4
                    ; ( if M.(equal instr.mnemonic FDIVR) then
                        st0 := ftof 80 src /. st0
                      else st0 := itof 80 src /. st0 ) ]
                @ fpu_compute_tag fpu_tag0 st0 l4 l5 l6 )
          | 2 ->
              let%bind dst = make_operand_rm instr addr 0 in
              let%bind src = make_operand_rm instr addr 1 in
              let%bind tag_dst = fpu_tag_of_operand instr 0 in
              let%bind tag_src = fpu_tag_of_operand instr 1 in
              let l1 = new_label () in
              let l2 = new_label () in
              let l3 = new_label () in
              let l4 = new_label () in
              let l5 = new_label () in
              let l6 = new_label () in
              Ok
                ( Il.
                    [ goto_if (tag_dst = num 3 2 || tag_src = num 3 2) l1 l2
                    ; ( @ ) l1 ]
                @ fpu_uflow_oflow false
                @ Il.
                    [ ( @ ) l2
                    ; fpu_c1 := bit false
                    ; goto_if (dst =. num 0 80 || dst =. neg0) l3 l4
                    ; ( @ ) l3
                    ; fpu_z := bit true
                    ; exn ()
                    ; ( @ ) l4
                    ; dst := src /. dst ]
                @ fpu_compute_tag tag_dst dst l4 l5 l6 )
          | _ -> Error `BadInstr
        in
        let undef_stmts =
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1 ]
        in
        let pop_stmts =
          match instr.mnemonic with
          | M.FDIVRP -> fpu_pop ctx
          | _ -> []
        in
        Ok (undef_stmts @ stmts @ pop_stmts)
    | M.FFREE ->
        let%bind tag = fpu_tag_of_operand instr 0 in
        Ok
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c1 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1
            ; tag := num 3 2 ]
    | M.FILD ->
        let%bind src = make_operand_rm instr addr 0 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let t = new_tmp 80 in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag7 <> num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow true
          @ Il.[( @ ) l2; fpu_c1 := bit false; t := itof 80 src]
          @ fpu_push t l3 l4 l5 ctx )
    | M.FIMUL | M.FMUL | M.FMULP ->
        let%bind stmts =
          match Array.length instr.operands with
          | 1 ->
              let%bind src = make_operand_rm instr addr 0 in
              let l1 = new_label () in
              let l2 = new_label () in
              let l3 = new_label () in
              let l4 = new_label () in
              let l5 = new_label () in
              Ok
                ( Il.[goto_if (fpu_tag0 = num 3 2) l1 l2; ( @ ) l1]
                @ fpu_uflow_oflow false
                @ Il.
                    [ ( @ ) l2
                    ; fpu_c1 := bit false
                    ; ( if M.(equal instr.mnemonic FMUL) then
                        st0 := st0 *. ftof 80 src
                      else st0 := st0 *. itof 80 src ) ]
                @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5 )
          | 2 ->
              let%bind dst = make_operand_rm instr addr 0 in
              let%bind src = make_operand_rm instr addr 1 in
              let%bind tag_dst = fpu_tag_of_operand instr 0 in
              let%bind tag_src = fpu_tag_of_operand instr 1 in
              let l1 = new_label () in
              let l2 = new_label () in
              let l3 = new_label () in
              let l4 = new_label () in
              let l5 = new_label () in
              Ok
                ( Il.
                    [ goto_if (tag_dst = num 3 2 || tag_src = num 3 2) l1 l2
                    ; ( @ ) l1 ]
                @ fpu_uflow_oflow false
                @ Il.[( @ ) l2; fpu_c1 := bit false; dst := dst *. src]
                @ fpu_compute_tag tag_dst dst l3 l4 l5 )
          | _ -> Error `BadInstr
        in
        let undef_stmts =
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1 ]
        in
        let pop_stmts =
          match instr.mnemonic with
          | M.FDIVP -> fpu_pop ctx
          | _ -> []
        in
        Ok (undef_stmts @ stmts @ pop_stmts)
    | M.FINCSTP ->
        let t1 = new_tmp 80 in
        let t2 = new_tmp 3 in
        Ok
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c1 := bit false
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1
            ; t1 := st0
            ; t2 := fpu_tag0
            ; st0 := st1
            ; fpu_tag0 := fpu_tag1
            ; st1 := st2
            ; fpu_tag1 := fpu_tag2
            ; st2 := st3
            ; fpu_tag2 := fpu_tag3
            ; st3 := st4
            ; fpu_tag3 := fpu_tag4
            ; st4 := st5
            ; fpu_tag4 := fpu_tag5
            ; st5 := st6
            ; fpu_tag5 := fpu_tag6
            ; st6 := st7
            ; fpu_tag6 := fpu_tag7
            ; st7 := t1
            ; fpu_tag7 := t2
            ; fpu_top := fpu_top + num 1 3 ]
    | M.FIST | M.FISTP | M.FISTTP ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let is_trunc = M.(equal instr.mnemonic FISTTP) in
        let l1 = new_label () in
        let l2 = new_label () in
        let pop_stmts =
          if M.(equal instr.mnemonic FIST) then [] else fpu_pop ctx
        in
        let src =
          if is_trunc then Il.(ftoi size (ftrunc st0))
          else Il.(ftoi size st0)
        in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.[( @ ) l2; fpu_c1 := bit false; assign_rm dst src]
          @ pop_stmts )
    | M.FLD ->
        let%bind src = make_operand_rm instr addr 0 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let op = instr.operands.(0) in
        let%bind v, tag =
          match op.value with
          | OV.Memory _ ->
              if op.size = 80 then Ok (src, None)
              else Ok (Il.ftof 80 src, None)
          | _ ->
              let%bind tag = fpu_tag_of_operand instr 0 in
              Ok (src, Some tag)
        in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag7 <> num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow true
          @ Il.[( @ ) l2; fpu_c1 := bit false]
          @ fpu_push v l3 l4 l5 ctx ~tag )
    | M.FLD1 | M.FLDL2T | M.FLDL2E | M.FLDPI | M.FLDLG2 | M.FLDLN2 | M.FLDZ
      ->
        let l1 = new_label () in
        let l2 = new_label () in
        let v =
          match instr.mnemonic with
          | M.FLD1 -> Il.num_big "0x3FFF8000000000000000" 80
          | M.FLDL2T -> Il.num_big "0x4000D49A784BCD1B8AFE" 80
          | M.FLDL2E -> Il.num_big "0x3FFFB8AA3B295C17F0BC" 80
          | M.FLDPI -> Il.num_big "0x4000C90FDAA22168C235" 80
          | M.FLDLG2 -> Il.num_big "0x3FFD9A209A84FBCFF799" 80
          | M.FLDLN2 -> Il.num_big "0x3FFEB17217F7D1CF79AC" 80
          (* FLDZ *)
          | _ -> Il.num 0 80
        in
        let tag_v =
          match instr.mnemonic with
          | M.FLDZ -> Il.num 1 2
          | _ -> Il.num 0 2
        in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag7 <> num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow true
          @ Il.[( @ ) l2; fpu_c1 := bit false]
          @ fpu_push_const v
          @ Il.[fpu_tag0 := tag_v] )
    | M.FLDCW ->
        let%bind src = make_operand_rm instr addr 0 in
        Ok
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c1 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1
            ; fpu_im := lo 1 src
            ; fpu_dm := ex 2 1 src
            ; fpu_zm := ex 3 2 src
            ; fpu_om := ex 4 3 src
            ; fpu_um := ex 5 4 src
            ; fpu_pm := ex 6 5 src
            ; fpu_iem := ex 8 7 src
            ; fpu_pc := ex 10 8 src
            ; fpu_rc := ex 12 10 src
            ; fpu_ic := ex 13 12 src ]
    | M.FLDENV ->
        let size = instr.operands.(0).size in
        let%bind src = make_operand_rm instr addr 0 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        Ok
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c1 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1
            ; t1 := lo 16 src
            ; fpu_im := lo 1 t1
            ; fpu_dm := ex 2 1 t1
            ; fpu_zm := ex 3 2 t1
            ; fpu_om := ex 4 3 t1
            ; fpu_um := ex 5 4 t1
            ; fpu_pm := ex 6 5 t1
            ; fpu_iem := ex 8 7 t1
            ; fpu_pc := ex 10 8 t1
            ; fpu_rc := ex 12 10 t1
            ; fpu_ic := ex 13 12 t1
            ; ( if Int.(size = 112) then t2 := ex 32 16 src
              else t2 := ex 48 32 src )
            ; fpu_i := lo 1 t2
            ; fpu_d := ex 2 1 t2
            ; fpu_z := ex 3 2 t2
            ; fpu_o := ex 4 3 t2
            ; fpu_u := ex 5 4 t2
            ; fpu_p := ex 6 5 t2
            ; fpu_sf := ex 7 6 t2
            ; fpu_ir := ex 8 7 t2
            ; fpu_c0 := ex 9 8 t2
            ; fpu_c1 := ex 10 9 t2
            ; fpu_c2 := ex 11 10 t2
            ; fpu_top := ex 14 11 t2
            ; fpu_c3 := ex 15 14 t2
            ; fpu_b := ex 16 15 t2
            ; ( if Int.(size = 112) then t3 := ex 48 32 src
              else t3 := ex 80 64 src )
            ; fpu_tag0 := lo 2 t3
            ; fpu_tag1 := ex 4 2 t3
            ; fpu_tag2 := ex 6 4 t3
            ; fpu_tag3 := ex 8 6 t3
            ; fpu_tag4 := ex 10 8 t3
            ; fpu_tag5 := ex 12 10 t3
            ; fpu_tag6 := ex 14 12 t3
            ; fpu_tag7 := ex 16 14 t3 ]
    | M.FNCLEX ->
        Ok
          Il.
            [ fpu_i := bit false
            ; fpu_d := bit false
            ; fpu_z := bit false
            ; fpu_o := bit false
            ; fpu_u := bit false
            ; fpu_p := bit false
            ; fpu_sf := bit false
            ; fpu_ir := bit false
            ; fpu_c0 := undefined 1
            ; fpu_c1 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1
            ; fpu_b := bit false ]
    | M.FNINIT ->
        Ok
          Il.
            [ fpu_im := bit true
            ; fpu_dm := bit true
            ; fpu_zm := bit true
            ; fpu_om := bit true
            ; fpu_um := bit true
            ; fpu_pm := bit true
            ; fpu_iem := bit false
            ; fpu_pc := num 3 2
            ; fpu_rc := num 0 2
            ; fpu_ic := bit false
            ; fpu_i := bit false
            ; fpu_d := bit false
            ; fpu_z := bit false
            ; fpu_o := bit false
            ; fpu_u := bit false
            ; fpu_p := bit false
            ; fpu_sf := bit false
            ; fpu_ir := bit false
            ; fpu_c0 := bit false
            ; fpu_c1 := bit false
            ; fpu_c2 := bit false
            ; fpu_top := num 0 3
            ; fpu_c3 := bit false
            ; fpu_b := bit false
            ; fpu_tag0 := num 3 2
            ; fpu_tag1 := num 3 2
            ; fpu_tag2 := num 3 2
            ; fpu_tag3 := num 3 2
            ; fpu_tag4 := num 3 2
            ; fpu_tag5 := num 3 2
            ; fpu_tag6 := num 3 2
            ; fpu_tag7 := num 3 2 ]
    | M.FNOP ->
        Ok
          Il.
            [ fpu_c0 := undefined 1
            ; fpu_c1 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1 ]
    | M.FNSTCW ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let t = new_tmp size in
        Ok
          Il.
            [ t :=
                num 7 3 @@ fpu_ic @@ fpu_rc @@ fpu_pc @@ fpu_iem @@ bit true
                @@ fpu_pm @@ fpu_um @@ fpu_om @@ fpu_zm @@ fpu_dm @@ fpu_im
            ; assign_rm dst t
            ; fpu_c0 := undefined 1
            ; fpu_c1 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1 ]
    | M.FNSTSW ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let t = new_tmp size in
        Ok
          Il.
            [ t :=
                fpu_b @@ fpu_c3 @@ fpu_top @@ fpu_c2 @@ fpu_c1 @@ fpu_c0
                @@ fpu_ir @@ fpu_sf @@ fpu_p @@ fpu_u @@ fpu_o @@ fpu_z
                @@ fpu_d @@ fpu_i
            ; assign_rm dst t
            ; fpu_c0 := undefined 1
            ; fpu_c1 := undefined 1
            ; fpu_c2 := undefined 1
            ; fpu_c3 := undefined 1 ]
    | M.FPATAN ->
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2 || fpu_tag1 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.[( @ ) l2; fpu_c1 := bit false; st1 := atan (st1 /. st0)]
          @ fpu_compute_tag fpu_tag1 st1 l3 l4 l5
          @ fpu_pop ctx )
    | M.FPREM | M.FPREM1 ->
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let t1 = new_tmp 64 in
        Ok
          ( Il.
              [ goto_if (fpu_tag0 = num 3 2 || fpu_tag1 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.[( @ ) l2; t1 := ftoi 64 (st0 /. st1); st0 := st0 %. st1]
          @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5
          @ Il.
              [ fpu_c0 := ex 3 2 t1
              ; fpu_c1 := lo 1 t1
              ; fpu_c2 := ex 79 64 st0 - ex 79 64 st1 >=$ num 64 15
              ; fpu_c3 := ex 2 1 t1 ] )
    | M.FPTAN ->
        let p1 = Il.num_big "0x3FFF8000000000000000" 80 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let l6 = new_label () in
        let l7 = new_label () in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.[( @ ) l2; st0 := tan st0]
          @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5
          @ Il.[goto_if (fpu_tag7 <> num 3 2) l6 l7; ( @ ) l6]
          @ fpu_uflow_oflow true
          @ Il.[( @ ) l7; fpu_c1 := bit false]
          @ fpu_push_const p1
          @ Il.[fpu_tag0 := num 0 2] )
    | M.FRNDINT ->
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.
              [ ( @ ) l2
              ; fpu_c1 := bit false
              ; (* this should depend on fpu_rc *)
                st0 := round st0 ]
          @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5 )
    | M.FSCALE ->
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2 || fpu_tag1 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.
              [( @ ) l2; fpu_c1 := bit false; st0 := st0 *. exp2 (ftrunc st1)]
          @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5 )
    | M.FSINCOS ->
        (* this code assumes abs(st0) < 2^63 *)
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let l6 = new_label () in
        let l7 = new_label () in
        let l8 = new_label () in
        let l9 = new_label () in
        let l10 = new_label () in
        let t = new_tmp 80 in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := bit false
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.[( @ ) l2; t := cos st0; st0 := sin st0]
          @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5
          @ Il.[goto_if (fpu_tag7 <> num 3 2) l6 l7; ( @ ) l6]
          @ fpu_uflow_oflow true
          @ Il.[( @ ) l7; fpu_c1 := bit false]
          @ fpu_push t l8 l9 l10 ctx )
    | M.FST | M.FSTP -> (
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let l1 = new_label () in
        let l2 = new_label () in
        let pop_stmts =
          if M.(equal instr.mnemonic FSTP) then fpu_pop ctx else []
        in
        match dst with
        | Var _ ->
            let%bind tag = fpu_tag_of_operand instr 0 in
            Ok
              ( Il.
                  [ goto_if (fpu_tag0 = num 3 2 || tag = num 3 2) l1 l2
                  ; ( @ ) l1 ]
              @ fpu_uflow_oflow false
              @ Il.[( @ ) l2; assign_rm dst st0; tag := fpu_tag0]
              @ pop_stmts )
        | _ ->
            Ok
              ( Il.[goto_if (fpu_tag0 = num 3 2) l1 l2; ( @ ) l1]
              @ fpu_uflow_oflow false
              @ Il.[( @ ) l2; assign_rm dst (ftof size st0)]
              @ pop_stmts ) )
    | M.FSQRT ->
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2 || fpu_tag1 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.[( @ ) l2; fpu_c1 := bit false; st0 := sqrt st0]
          @ fpu_compute_tag fpu_tag0 st0 l3 l4 l5 )
    | M.FTST ->
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let l6 = new_label () in
        let l7 = new_label () in
        let l8 = new_label () in
        let l9 = new_label () in
        let neg0 = Il.num_big "0x80000000000000000000" 80 in
        Ok
          ( Il.
              [ fpu_c1 := bit false
              ; goto_if (fpu_tag0 = num 3 2) l1 l2
              ; ( @ ) l1 ]
          @ fpu_uflow_oflow false
          @ Il.
              [ ( @ ) l2
              ; goto_if (st0 >. num 0 80) l3 l4
              ; ( @ ) l3
              ; fpu_c0 := bit false
              ; fpu_c2 := bit false
              ; fpu_c3 := bit false
              ; goto l9
              ; ( @ ) l4
              ; goto_if (st0 <. neg0) l5 l6
              ; ( @ ) l5
              ; fpu_c0 := bit true
              ; fpu_c2 := bit false
              ; fpu_c3 := bit false
              ; goto l9
              ; ( @ ) l6
              ; goto_if (st0 =. num 0 80 || st0 =. neg0) l7 l8
              ; ( @ ) l7
              ; fpu_c0 := bit false
              ; fpu_c2 := bit false
              ; fpu_c3 := bit true
              ; goto l9
              ; ( @ ) l8
              ; fpu_c0 := bit true
              ; fpu_c2 := bit true
              ; fpu_c3 := bit true
              ; goto l9
              ; ( @ ) l9 ] )
    | M.FXCH ->
        let%bind src = make_operand_rm instr addr 0 in
        let%bind tag = fpu_tag_of_operand instr 0 in
        let l1 = new_label () in
        let l2 = new_label () in
        let t1 = new_tmp 80 in
        let t2 = new_tmp 2 in
        if Il.equal_expr tag fpu_tag0 then
          Ok
            Il.
              [ fpu_c0 := undefined 1
              ; fpu_c1 := bit false
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2) l1 l2
              ; ( @ ) l1
              ; fpu_i := bit true
              ; fpu_sf := bit true
              ; exn ()
              ; ( @ ) l2 ]
        else
          Ok
            Il.
              [ fpu_c0 := undefined 1
              ; fpu_c1 := bit false
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2 || tag = num 3 2) l1 l2
              ; ( @ ) l1
              ; fpu_i := bit true
              ; fpu_sf := bit true
              ; exn ()
              ; ( @ ) l2
              ; t1 := st0
              ; t2 := fpu_tag0
              ; st0 := src
              ; fpu_tag0 := tag
              ; src := t1
              ; tag := t2 ]
    | M.FYL2X | M.FYL2XP1 ->
        let p1 = Il.num_big "0x3FFF8000000000000000" 80 in
        let l1 = new_label () in
        let l2 = new_label () in
        let src =
          if M.(equal instr.mnemonic FYL2X) then st0 else Il.(st0 +. p1)
        in
        Ok
          ( Il.
              [ fpu_c0 := undefined 1
              ; fpu_c1 := bit false
              ; fpu_c2 := undefined 1
              ; fpu_c3 := undefined 1
              ; goto_if (fpu_tag0 = num 3 2 || fpu_tag1 = num 3 2) l1 l2
              ; ( @ ) l1
              ; fpu_i := bit true
              ; exn ()
              ; ( @ ) l2
              ; st1 := st1 *. flog2 src ]
          @ fpu_pop ctx )
    | M.HLT -> Ok Il.[halt ()]
    | M.IMUL | M.MUL ->
        let%bind res_stmts =
          match Array.length instr.operands with
          | 1 ->
              let size = instr.operands.(0).size in
              let%bind src = make_operand_rm instr addr 0 in
              let signed = M.(equal instr.mnemonic IMUL) in
              let tsz = size * 2 in
              let t = new_tmp tsz in
              if size = 8 then
                if signed then
                  Ok
                    Il.
                      [ t := se 16 al * se 16 src
                      ; ax := t
                      ; cf := se 16 (lo 8 t) <> t
                      ; oF := cf ]
                else
                  Ok
                    Il.
                      [ t := ze 16 al * ze 16 src
                      ; ax := t
                      ; cf := hi 8 t = num 0 8
                      ; oF := cf ]
              else
                let dx', ax' =
                  match size with
                  | 16 -> (dx, ax)
                  | 32 -> (edx, eax)
                  | _ -> (rdx, rax)
                in
                if signed then
                  Ok
                    Il.
                      [ t := se tsz ax' * se tsz src
                      ; dx' := hi size t
                      ; ax' := lo size t
                      ; cf := se tsz ax' <> t
                      ; oF := cf ]
                else
                  Ok
                    Il.
                      [ t := ze tsz ax' * ze tsz src
                      ; dx' := hi size t
                      ; ax' := lo size t
                      ; cf := dx' = num 0 size
                      ; oF := cf ]
          | 2 ->
              let size = instr.operands.(0).size in
              let%bind dst = make_operand_rm instr addr 0 in
              let%bind src = make_operand_rm instr addr 1 in
              let tsz = size * 2 in
              let t = new_tmp tsz in
              Ok
                Il.
                  [ t := se tsz dst * se tsz src
                  ; dst := lo size t
                  ; cf := se tsz dst <> t
                  ; oF := cf ]
          | 3 ->
              let size = instr.operands.(0).size in
              let%bind dst = make_operand_rm instr addr 0 in
              let%bind src1 = make_operand_rm instr addr 1 in
              let%bind src2 = make_operand instr addr 2 in
              let tsz = size * 2 in
              let t = new_tmp tsz in
              Ok
                Il.
                  [ t := se tsz src1 * se tsz src2
                  ; dst := lo size t
                  ; cf := se tsz dst <> t
                  ; oF := cf ]
          | _ -> Error `BadInstr
        in
        Ok
          ( res_stmts
          @ Il.
              [ pf := undefined 1
              ; zf := undefined 1
              ; sf := undefined 1
              ; af := undefined 1 ] )
    | M.INT ->
        let%bind imm =
          match instr.immediate with
          | Some (Imm.One imm) -> Ok imm.value
          | _ -> Error (`BadOperand 0)
        in
        let flg = flags_concat wordsz in
        let t = new_tmp wordsz in
        let sp_dec = wordsz lsr 3 in
        Ok
          Il.
            [ t := flg
            ; sp' := sp' - num sp_dec wordsz
            ; assign_rm (load `LE sp_dec mu sp') t
            ; sp' := sp' - num 2 wordsz
            ; assign_rm (load `LE 2 mu sp') cs
            ; sp' := sp' - num sp_dec wordsz
            ; assign_rm (load `LE sp_dec mu sp') (bitv endaddr wordsz)
            ; interrupt Int.(of_int64_exn imm) ]
    | M.INT1 | M.INT3 ->
        let flg = flags_concat wordsz in
        let t = new_tmp wordsz in
        let sp_dec = wordsz lsr 3 in
        Ok
          Il.
            [ t := flg
            ; sp' := sp' - num 2 wordsz
            ; assign_rm (load `LE 2 mu sp') cs
            ; sp' := sp' - num sp_dec wordsz
            ; assign_rm (load `LE sp_dec mu sp') (bitv endaddr wordsz)
            ; sp' := sp' - num sp_dec wordsz
            ; assign_rm (load `LE sp_dec mu sp') t
            ; breakpoint () ]
    | M.INTO ->
        let l1 = new_label () in
        let l2 = new_label () in
        let flg = flags_concat wordsz in
        let t = new_tmp wordsz in
        let sp_dec = wordsz lsr 3 in
        Ok
          Il.
            [ goto_if (oF = bit true) l1 l2
            ; ( @ ) l1
            ; t := flg
            ; sp' := sp' - num 2 wordsz
            ; assign_rm (load `LE 2 mu sp') cs
            ; sp' := sp' - num sp_dec wordsz
            ; assign_rm (load `LE sp_dec mu sp') (bitv endaddr wordsz)
            ; sp' := sp' - num sp_dec wordsz
            ; assign_rm (load `LE sp_dec mu sp') t
            ; breakpoint ()
            ; ( @ ) l2 ]
    | M.IRETD | M.IRETQ | M.IRETW ->
        let size =
          match instr.mnemonic with
          | M.IRETD -> 32
          | M.IRETQ -> 64
          | _ -> 16
        in
        let t_ip = new_tmp wordsz in
        let t_flg = new_tmp size in
        let ld_sp sz = Il.(load `LE (sz lsr 3) mu sp') in
        let t_ip_stmt =
          if size <> wordsz then Il.(t_ip := ze wordsz (ld_sp size))
          else Il.(t_ip := ld_sp size)
        in
        let flg_stmts = flags_extract t_flg size in
        let szinc = size lsr 3 in
        Ok
          ( t_ip_stmt
            :: Il.
                 [ sp' := sp' + num szinc wordsz
                 ; cs := ld_sp 16
                 ; sp' := sp' + num 2 wordsz
                 ; t_flg := ld_sp size
                 ; sp' := sp' + num szinc wordsz ]
          @ flg_stmts
          @ Il.[jmp t_ip] )
    | M.JB
     |M.JBE
     |M.JL
     |M.JLE
     |M.JNB
     |M.JNBE
     |M.JNL
     |M.JNLE
     |M.JNO
     |M.JNP
     |M.JNS
     |M.JNZ
     |M.JO
     |M.JP
     |M.JS
     |M.JZ ->
        let%bind cond = fcond_of_mnemonic instr.mnemonic in
        let%bind target = make_operand instr addr 0 in
        Ok Il.[jmp_if cond target (bitv endaddr wordsz)]
    | M.JCXZ ->
        let%bind target = make_operand instr addr 0 in
        Ok Il.[jmp_if (cx = num 0 16) target (bitv endaddr wordsz)]
    | M.JECXZ ->
        let%bind target = make_operand instr addr 0 in
        Ok Il.[jmp_if (ecx = num 0 32) target (bitv endaddr wordsz)]
    | M.JMP ->
        let%bind target = make_operand instr addr 0 in
        Ok Il.[jmp target]
    | M.JMPF -> (
        let op = instr.operands.(0) in
        match op.value with
        | OV.Pointer p ->
            if is64 then Error `BadInstr
            else Ok Il.[cs := num p.segment 16; jmp (num32 p.offset wordsz)]
        | OV.Memory _ ->
            let%bind target = make_operand_rm instr addr 0 in
            let t = new_tmp op.size in
            let t' = new_tmp wordsz in
            let dst =
              let offset = op.size - 16 in
              if offset < wordsz then Il.(ze wordsz (lo offset t))
              else Il.(lo offset t)
            in
            Ok Il.[t := target; cs := hi 16 t; t' := dst; jmp t']
        | _ -> Error (`BadOperand 0) )
    | M.JRCXZ ->
        let%bind target = make_operand instr addr 0 in
        Ok Il.[jmp_if (rcx = num 0 64) target (bitv endaddr wordsz)]
    | M.LAHF ->
        let src =
          Il.(
            sf @@ zf @@ bit false @@ af @@ bit false @@ pf @@ bit true @@ cf)
        in
        Ok Il.[ah := src]
    | M.LEA -> (
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 ~ignore_seg:true in
        match rhs with
        | Il.Load r ->
            let loc =
              (* account for implicit zero-extend *)
              if is64 && size = 32 && instr.easz = 64 then r.loc
              else if size < instr.easz then Il.(lo size r.loc)
              else r.loc
            in
            Ok [assign_rm lhs loc]
        | _ -> Error `BadInstr )
    | M.LEAVE ->
        Ok
          Il.
            [ sp' := bp'
            ; bp := load `LE (wordsz lsr 3) mu sp'
            ; sp' := sp' + num Int.(wordsz lsr 3) wordsz ]
    | M.LMSW ->
        let%bind src = make_operand_rm instr addr 0 in
        let cr0 = cr0 wordsz in
        let s1 = wordsz - 1 in
        Ok Il.[cr0 := ex s1 4 cr0 @@ lo 4 src]
    | M.LOOP | M.LOOPE | M.LOOPNE ->
        let%bind target = make_operand instr addr 0 in
        let cx' =
          match instr.easz with
          | 16 -> cx
          | 32 -> ecx
          | _ -> rcx
        in
        let cond = Il.(cx' = num 0 instr.easz) in
        let%bind fcond = fcond_of_mnemonic instr.mnemonic in
        let cond =
          match instr.mnemonic with
          | M.LOOPE | M.LOOPNE -> Il.(cond & fcond)
          | _ -> cond
        in
        let dec = assign_rm cx' Il.(cx' - num 1 instr.easz) in
        Ok Il.[dec; jmp_if cond target (bitv endaddr wordsz)]
    | M.LZCNT ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let mask = Il.(num 1 size << t) in
        let s1 = size - 1 in
        Ok
          Il.
            [ t := num s1 size
            ; goto l1
            ; ( @ ) l1
            ; goto_if (t >=$ num 0 size & (src & mask) = num 0 size) l2 l3
            ; ( @ ) l2
            ; t := t - num 1 size
            ; dst := dst + num 1 size
            ; goto l1
            ; ( @ ) l3
            ; cf := dst = num size size
            ; zf := dst = num 0 size
            ; oF := undefined size
            ; sf := undefined size
            ; pf := undefined size
            ; af := undefined size ]
    | M.MOV | M.MOVNTI | M.MOVSX | M.MOVSXD | M.MOVZX ->
        let size_lhs = instr.operands.(0).size in
        let size_rhs = instr.operands.(1).size in
        if size_lhs < size_rhs then Error (`BadOperand 0)
        else
          let%bind lhs = make_operand_rm instr addr 0 in
          let%bind rhs = make_operand instr addr 1 in
          let rhs =
            match instr.mnemonic with
            | M.MOVZX -> Il.(ze size_lhs rhs)
            | M.MOVSX -> Il.(se size_lhs rhs)
            | M.MOVSXD when size_lhs > size_rhs -> Il.(se size_lhs rhs)
            | _ -> rhs
          in
          Ok Il.[assign_rm lhs rhs]
    | M.MOVAPD | M.MOVAPS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        Ok [assign_rm dst src]
    | M.MOVD | M.MOVQ | M.VMOVD | M.VMOVQ -> (
        let op0 = instr.operands.(0) in
        let op1 = instr.operands.(1) in
        let size_dst = op0.size in
        let size_src = op1.size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        match (op0.value, op1.value) with
        | OV.Memory _, _ ->
            if size_dst < size_src then
              Ok [assign_rm dst Il.(lo size_dst src)]
            else Ok [assign_rm dst src]
        | OV.Register _, _ ->
            if size_dst > size_src then Ok Il.[dst := ze size_dst src]
            else Ok Il.[dst := src]
        | _ -> Error (`BadOperand 0) )
    | M.MOVDQA | M.MOVDQU ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        Ok [assign_rm dst src]
    | (M.MOVSD | M.MOVSS (* avoid convusion with MOVSD string op *))
      when not (Array.is_empty instr.operands) -> (
        let op0 = instr.operands.(0) in
        let op1 = instr.operands.(1) in
        let size_dst = op0.size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let size =
          match instr.mnemonic with
          | M.MOVSD -> 64
          (* MOVSS *)
          | _ -> 32
        in
        let size_diff = size_dst - size in
        match (op0.value, op1.value) with
        | OV.Memory _, _ -> Ok [assign_rm dst Il.(lo size src)]
        | OV.Register _, OV.Register _ ->
            Ok Il.[dst := hi size_diff dst @@ lo size src]
        | OV.Register _, OV.Memory _ -> Ok Il.[dst := ze size_dst src]
        | _ -> Error (`BadOperand 0) )
    | M.MULPD ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 64 dst *. lo 64 src
            ; t2 := hi 64 dst *. hi 64 src
            ; assign_rm dst (t2 @@ t1) ]
    | M.MULPS ->
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 dst *. lo 32 src
            ; t2 := ex 63 32 dst *. ex 63 32 src
            ; t3 := ex 95 64 dst *. ex 95 64 src
            ; t4 := hi 32 dst *. hi 32 src
            ; assign_rm dst (t4 @@ t3 @@ t2 @@ t1) ]
    | M.MULSD | M.MULSS ->
        let size_dst = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let is_mem =
          match instr.operands.(1).value with
          | OV.Memory _ -> true
          | _ -> false
        in
        let size =
          match instr.mnemonic with
          | M.MULSD -> 64
          (* MULSS *)
          | _ -> 32
        in
        let size_diff = size_dst - size in
        let t = new_tmp size in
        let src = if is_mem then src else Il.(lo size src) in
        Ok
          Il.[t := lo size dst *. src; assign_rm dst (hi size_diff dst @@ t)]
    | M.MULX ->
        let size = instr.operands.(0).size in
        let%bind dst_hi = make_operand_rm instr addr 0 in
        let%bind dst_lo = make_operand_rm instr addr 1 in
        let%bind src = make_operand_rm instr addr 2 in
        let dx' = if size = 32 then edx else rdx in
        let s2 = size lsl 1 in
        let t = new_tmp s2 in
        Ok
          Il.
            [ t := ze s2 src * ze s2 dx'
            ; dst_hi := hi size t
            ; dst_lo := lo size t ]
    | M.NEG ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let cf_stmt = Il.(cf := dst <> num 0 size) in
        let res_stmt = assign_rm dst Il.(~-dst) in
        let pf_stmts = arith_parity dst size ctx in
        let sf_stmt = Il.(sf := dst <$ num 0 size) in
        let oF_stmt = Il.(oF := bit false) in
        let af_stmt = Il.(af := bit false) in
        let zf_stmt = Il.(zf := dst = num 0 size) in
        Ok
          ([cf_stmt; res_stmt; sf_stmt; oF_stmt; af_stmt; zf_stmt] @ pf_stmts)
    | M.NOP -> Ok Il.[]
    | M.NOT ->
        let%bind dst = make_operand_rm instr addr 0 in
        Ok [assign_rm dst Il.(~~dst)]
    | M.POP ->
        let is_sp, is_mem =
          match instr.operands.(0).value with
          | OV.Register R.SP | OV.Register R.ESP | OV.Register R.RSP ->
              (true, false)
          | OV.Memory m -> (
            match m.base with
            | Some R.SP | Some R.ESP | Some R.RSP -> (true, true)
            | _ -> (false, true) )
          | _ -> (false, true)
        in
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let sp_inc = Il.(num Int.(size lsr 3) wordsz) in
        if is_sp then
          if not is_mem then
            let src = Il.(load `LE (size lsr 3) mu sp') in
            Ok [assign_rm dst src]
          else
            let t = new_tmp size in
            Ok
              Il.
                [ t := load `LE (size lsr 3) mu sp'
                ; sp' := sp' + sp_inc
                ; assign_rm dst t ]
        else
          let src = Il.(load `LE (size lsr 3) mu sp') in
          Ok Il.[assign_rm dst src; sp' := sp' + sp_inc]
    | M.POPA ->
        if is64 then Error `BadInstr
        else
          let sp_inc = Il.(num 2 wordsz) in
          let sp_inc2 = Il.(num 4 wordsz) in
          let ld = Il.(load `LE 2 mu sp') in
          Ok
            Il.
              [ di := ld
              ; sp' := sp' + sp_inc
              ; si := ld
              ; sp' := sp' + sp_inc
              ; bp := ld
              ; sp' := sp' + sp_inc2
              ; bx := ld
              ; sp' := sp' + sp_inc
              ; dx := ld
              ; sp' := sp' + sp_inc
              ; cx := ld
              ; sp' := sp' + sp_inc
              ; ax := ld
              ; sp' := sp' + sp_inc ]
    | M.POPAD ->
        if is64 then Error `BadInstr
        else
          let sp_inc = Il.(num Int.(wordsz lsr 3) wordsz) in
          let sp_inc2 = Il.(num Int.((wordsz * 2) lsr 3) wordsz) in
          let ld = Il.(load `LE (wordsz lsr 3) mu sp') in
          Ok
            Il.
              [ edi := ld
              ; sp' := sp' + sp_inc
              ; esi := ld
              ; sp' := sp' + sp_inc
              ; ebp := ld
              ; sp' := sp' + sp_inc2
              ; ebx := ld
              ; sp' := sp' + sp_inc
              ; edx := ld
              ; sp' := sp' + sp_inc
              ; ecx := ld
              ; sp' := sp' + sp_inc
              ; eax := ld
              ; sp' := sp' + sp_inc ]
    | M.POPF | M.POPFD | M.POPFQ ->
        let size =
          match instr.mnemonic with
          | M.POPF -> 16
          | M.POPFD -> 32
          | _ -> 64
        in
        let sp_inc = Il.(num Int.(size lsr 3) wordsz) in
        let t1 = new_tmp size in
        let t2 = new_tmp 2 in
        let pop_stmts =
          Il.
            [ t1 := load `LE (size lsr 3) mu sp'
            ; sp' := sp' + sp_inc
            ; t2 := lo 2 cs ]
        in
        let default =
          Il.
            [ cf := lo 1 t1
            ; pf := ex 2 2 t1
            ; af := ex 4 4 t1
            ; zf := ex 6 6 t1
            ; sf := ex 7 7 t1
            ; tf := ex 8 8 t1
            ; df := ex 10 10 t1
            ; oF := ex 11 11 t1
            ; nt := ex 14 14 t1 ]
        in
        let l1 = new_label () in
        let l2 = new_label () in
        let priv_stmts =
          if size = 16 then Il.[iF := ex 9 9 t1]
          else
            let l3 = new_label () in
            let l4 = new_label () in
            Il.
              [ rf := bit false
              ; ac := ex 18 18 t1
              ; id := ex 21 21 t1
              ; goto_if (t2 > iopl) l3 l4
              ; ( @ ) l3
              ; jmp (bitv endaddr wordsz)
              ; ( @ ) l4
              ; iF := ex 9 9 t1 ]
        in
        Ok
          ( pop_stmts @ default
          @ Il.
              [ goto_if (t2 = num 0 2 || lo 1 (cr0 wordsz)) l1 l2
              ; ( @ ) l1
              ; iF := ex 9 9 t1
              ; iopl := ex 13 12 t1 ]
          @ ( if size = 16 then []
            else Il.[rf := bit false; id := ex 21 21 t1] )
          @ Il.[jmp (bitv endaddr wordsz); ( @ ) l2]
          @ priv_stmts )
    | M.PUSH ->
        let is_sp =
          match instr.operands.(0).value with
          | OV.Register R.SP | OV.Register R.ESP | OV.Register R.RSP -> true
          | OV.Memory m -> (
            match m.base with
            | Some R.SP | Some R.ESP | Some R.RSP -> true
            | _ -> false )
          | _ -> false
        in
        let size =
          match instr.operands.(0).value with
          | OV.Immediate _ -> instr.eosz
          | _ -> instr.operands.(0).size
        in
        let%bind src = make_operand instr addr 0 in
        let sp_dec = Il.(num Int.(size lsr 3) wordsz) in
        if is_sp then
          let t = new_tmp size in
          Ok
            Il.
              [ t := src
              ; sp' := sp' - sp_dec
              ; assign_rm (load `LE (size lsr 3) mu sp') t ]
        else
          Ok
            Il.
              [ sp' := sp' - sp_dec
              ; assign_rm (load `LE (size lsr 3) mu sp') src ]
    | M.PUSHF | M.PUSHFD | M.PUSHFQ ->
        let size =
          match instr.mnemonic with
          | M.PUSHF -> 16
          | M.PUSHFD -> 32
          | _ -> 64
        in
        let sp_dec = Il.(num Int.(size lsr 3) wordsz) in
        let t = new_tmp size in
        let clear_vm_rf = size <> 16 in
        let src = flags_concat size ~clear_vm_rf in
        Ok
          Il.
            [ t := src
            ; sp' := sp' - sp_dec
            ; assign_rm (load `LE (size lsr 3) mu sp') t ]
    | M.SAHF ->
        Ok
          Il.
            [ sf := hi 1 ah
            ; zf := ex 6 6 ah
            ; af := ex 4 4 ah
            ; pf := ex 2 2 ah
            ; cf := lo 1 ah ]
    | M.RETF ->
        let pop_sz =
          match instr.immediate with
          | Some (Imm.One imm) ->
              ((wordsz + 16) lsr 3) + Int.(of_int64_exn imm.value)
          | _ -> (wordsz + 16) lsr 3
        in
        let sp_inc = Il.(num pop_sz wordsz) in
        let t = new_tmp wordsz in
        Ok
          Il.
            [ t := load `LE Int.((wordsz + 16) lsr 3) mu sp'
            ; sp' := sp' + sp_inc
            ; cs := hi 16 t
            ; jmp (lo wordsz t) ]
    | M.RETN ->
        let pop_sz =
          match instr.immediate with
          | Some (Imm.One imm) ->
              (wordsz lsr 3) + Int.(of_int64_exn imm.value)
          | _ -> wordsz lsr 3
        in
        let sp_inc = Il.(num pop_sz wordsz) in
        let t = new_tmp wordsz in
        Ok
          Il.
            [t := load `LE (wordsz lsr 3) mu sp'; sp' := sp' + sp_inc; jmp t]
    | M.SALC -> Ok Il.[al := se 8 cf]
    | M.SAR | M.SHL | M.SHR ->
        let size = instr.operands.(0).size in
        let cntmask =
          match instr.rex with
          | Some rex when rex.w <> 0 -> 0x3F
          | _ -> 0x1F
        in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs, imm =
          match instr.operands.(1).value with
          | OV.Register r -> (
            match make_reg r is64 with
            | None -> Error (`BadOperand 1)
            | Some (r, _) -> Ok (Il.(ze size r & num cntmask size), None) )
          | OV.Immediate imm ->
              let v = Int64.(imm.value land of_int cntmask) in
              Ok (Il.(num64 v size), Some v)
          | _ -> Error `BadInstr
        in
        let rhs, rhs_stmts =
          match imm with
          | None ->
              let t = new_tmp size in
              (t, Il.[t := rhs])
          | _ -> (rhs, [])
        in
        let res =
          match instr.mnemonic with
          | M.SAR -> Il.(lhs >>> rhs)
          | M.SHL -> Il.(lhs << rhs)
          | _ -> Il.(lhs >> rhs)
        in
        let res_stmt = assign_rm lhs res in
        let sf_stmt = Il.(sf := lhs <$ num 0 size) in
        let zf_stmt = Il.(zf := lhs = num 0 size) in
        let pf_stmts = arith_parity lhs size ctx in
        let stmts =
          match imm with
          | Some imm -> (
            match imm with
            | 0L -> Il.[]
            | 1L -> (
                let t = new_tmp size in
                let t_stmt = Il.(t := lhs) in
                (t_stmt :: res_stmt :: sf_stmt :: zf_stmt :: pf_stmts)
                @ Il.[af := undefined 1]
                @
                match instr.mnemonic with
                | M.SAR -> Il.[cf := lo 1 t; oF := bit false]
                | M.SHL -> Il.[cf := hi 1 t; oF := hi 1 lhs ^ cf]
                | _ -> Il.[cf := lo 1 t; oF := hi 1 t] )
            | imm ->
                let t = new_tmp size in
                let t_stmt = Il.(t := lhs) in
                let pos = Int.(of_int64_exn imm) - 1 in
                t_stmt :: res_stmt :: sf_stmt :: zf_stmt
                ::
                ( match instr.mnemonic with
                | M.SAR | M.SHR -> Il.[cf := ex pos pos t]
                | _ ->
                    let pos = size - 1 - pos in
                    Il.[cf := ex pos pos t] )
                @ pf_stmts
                @ Il.[oF := undefined 1; af := undefined 1] )
          | None ->
              let t = new_tmp size in
              let t_stmt = Il.(t := lhs) in
              let c1l = new_label () in
              let ctl = new_label () in
              let c0l = new_label () in
              let cul = new_label () in
              let cfl = new_label () in
              let c1l_stmts =
                match instr.mnemonic with
                | M.SAR -> Il.[cf := lo 1 t; oF := bit false]
                | M.SHL -> Il.[cf := hi 1 t; oF := hi 1 lhs ^ cf]
                | _ -> Il.[cf := lo 1 t; oF := hi 1 t]
              in
              let cf_ul_stmt =
                match instr.mnemonic with
                | M.SAR | M.SHR -> Il.(cf := lo 1 (t >> rhs - num 1 size))
                | _ -> Il.(cf := hi 1 (t << rhs - num 1 size))
              in
              rhs_stmts @ [t_stmt; res_stmt]
              @ Il.[goto_if (rhs = num 1 size) c1l ctl; ( @ ) c1l]
              @ c1l_stmts
              @ Il.
                  [ goto cfl
                  ; ( @ ) ctl
                  ; goto_if (rhs = num 0 size) c0l cul
                  ; ( @ ) c0l
                  ; jmp (bitv endaddr wordsz)
                  ; ( @ ) cul
                  ; cf_ul_stmt
                  ; oF := undefined 1
                  ; goto cfl
                  ; ( @ ) cfl
                  ; sf_stmt
                  ; zf_stmt ]
              @ pf_stmts
              @ Il.[af := undefined 1]
        in
        Ok stmts
    | M.SARX | M.SHLX | M.SHRX ->
        let size = instr.operands.(0).size in
        let%bind cntmask =
          match instr.avx with
          | Some (A.Vex vex) when vex.l = 0 ->
              Ok (if is64 && vex.w <> 0 then 0x3F else 0x1F)
          | _ -> Error `BadInstr
        in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src1 = make_operand_rm instr addr 1 in
        let%bind src2 = make_operand_rm instr addr 2 in
        let src2, src2_stmt =
          let t = new_tmp size in
          (t, Il.(t := src2 & num cntmask size))
        in
        let res_stmt =
          match instr.mnemonic with
          | M.SARX -> Il.(dst := src1 >>> src2)
          | M.SHLX -> Il.(dst := src1 << src2)
          | _ -> Il.(dst := src1 >> src2)
        in
        Ok [src2_stmt; res_stmt]
    | M.SETB
     |M.SETBE
     |M.SETL
     |M.SETLE
     |M.SETNB
     |M.SETNBE
     |M.SETNL
     |M.SETNLE
     |M.SETNO
     |M.SETNP
     |M.SETNS
     |M.SETNZ
     |M.SETO
     |M.SETP
     |M.SETS
     |M.SETZ ->
        let%bind cond = fcond_of_mnemonic instr.mnemonic in
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        Ok [assign_rm dst Il.(ze size cond)]
    | M.SHLD | M.SHRD ->
        let size = instr.operands.(0).size in
        let cntmask =
          match instr.rex with
          | Some rex when rex.w <> 0 -> 0x3F
          | _ -> 0x1F
        in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let%bind cnt, imm =
          match instr.operands.(2).value with
          | OV.Register r -> (
            match make_reg r is64 with
            | None -> Error (`BadOperand 2)
            | Some (r, _) -> Ok (Il.(ze size r & num cntmask size), None) )
          | OV.Immediate imm ->
              let v = Int64.(imm.value land of_int cntmask) in
              Ok (Il.(num64 v size), Some v)
          | _ -> Error `BadInstr
        in
        let cnt, cnt_stmts =
          match imm with
          | None ->
              let t = new_tmp size in
              (t, Il.[t := cnt])
          | _ -> (cnt, [])
        in
        let res_stmts =
          match imm with
          | None -> (
              let cnt_opp = Il.(num Int.(size - 1) size - cnt) in
              match instr.mnemonic with
              | M.SHLD ->
                  let t1 = new_tmp size in
                  let t2 = new_tmp size in
                  Il.
                    [ t1 := dst << cnt
                    ; t2 := src >> cnt_opp
                    ; assign_rm dst (t1 || t2) ]
              | _ ->
                  let t1 = new_tmp size in
                  let t2 = new_tmp size in
                  Il.
                    [ t1 := dst >> cnt
                    ; t2 := src << cnt_opp
                    ; assign_rm dst (t1 || t2) ] )
          | Some imm when Int64.(imm <> 0L) -> (
              let v = Int64.(of_int size - 1L - imm) in
              let v = Int.of_int64_exn v in
              let s1 = size - 1 in
              match instr.mnemonic with
              | M.SHLD ->
                  [assign_rm dst Il.(ex v 0 dst @@ ex s1 Int.(v + 1) src)]
              | _ ->
                  let h = s1 - v in
                  [assign_rm dst Il.(ex Int.(h - 1) 0 src @@ ex s1 h dst)] )
          | _ -> []
        in
        let sf_stmt = Il.(sf := dst <$ num 0 size) in
        let zf_stmt = Il.(zf := dst = num 0 size) in
        let pf_stmts = arith_parity dst size ctx in
        let stmts =
          match imm with
          | Some imm -> (
            match imm with
            | 0L -> Il.[]
            | imm ->
                let t = new_tmp size in
                let t_stmt = Il.(t := dst) in
                let pos = Int.(of_int64_exn imm) - 1 in
                let oF_stmt =
                  if Int64.(imm = 1L) then Il.(oF := hi 1 dst <> hi 1 t)
                  else Il.(oF := undefined 1)
                in
                (t_stmt :: res_stmts)
                @ Il.[sf_stmt; zf_stmt; oF_stmt; af := undefined 1]
                @ ( match instr.mnemonic with
                  | M.SHRD -> Il.[cf := ex pos pos t]
                  | _ ->
                      let pos = size - 1 - pos in
                      Il.[cf := ex pos pos t] )
                @ pf_stmts )
          | None ->
              let t = new_tmp size in
              let t_stmt = Il.(t := dst) in
              let c0l = new_label () in
              let ctl = new_label () in
              let c1l = new_label () in
              let cgl = new_label () in
              let cfl = new_label () in
              let cf_ul_stmt =
                match instr.mnemonic with
                | M.SHLD -> Il.(cf := hi 1 (t << cnt - num 1 size))
                | _ -> Il.(cf := lo 1 (t >> cnt - num 1 size))
              in
              cnt_stmts @ (t_stmt :: res_stmts)
              @ Il.
                  [ goto_if (cnt = num 0 size) c0l ctl
                  ; ( @ ) c0l
                  ; jmp (bitv endaddr wordsz)
                  ; ( @ ) ctl
                  ; sf_stmt
                  ; zf_stmt
                  ; af := undefined 1
                  ; cf_ul_stmt
                  ; goto_if (cnt = num 1 size) c1l cgl
                  ; ( @ ) cgl
                  ; oF := undefined 1
                  ; goto cfl
                  ; ( @ ) c1l
                  ; oF := hi 1 dst <> hi 1 t
                  ; goto cfl
                  ; ( @ ) cfl ]
              @ pf_stmts
        in
        Ok stmts
    | M.STC -> Ok Il.[cf := bit true]
    | M.STD -> Ok Il.[df := bit true]
    | M.STI -> Ok Il.[iF := bit true]
    | M.RCL | M.RCR | M.ROL | M.ROR ->
        let size = instr.operands.(0).size in
        let cntmask =
          match instr.rex with
          | Some rex when rex.w <> 0 -> 0x3F
          | _ -> 0x1F
        in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind cnt, imm =
          match instr.operands.(1).value with
          | OV.Register r -> (
            match make_reg r is64 with
            | None -> Error (`BadOperand 1)
            | Some (r, sz) ->
                let cnt =
                  if sz = size then Il.(r & num cntmask size)
                  else Il.(ze size r & num cntmask size)
                in
                let cnt =
                  match size with
                  | 8 -> Il.(cnt % num 9 size)
                  | 16 -> Il.(cnt % num 17 size)
                  | _ -> cnt
                in
                Ok (cnt, None) )
          | OV.Immediate imm ->
              let v = Int.(of_int64_exn imm.value land cntmask) in
              let v =
                match size with
                | 8 -> v mod 9
                | 16 -> v mod 17
                | _ -> v
              in
              Ok (Il.(num v size), Some v)
          | _ -> Error `BadInstr
        in
        let cnt, cnt_stmts =
          match imm with
          | None ->
              let t = new_tmp size in
              (t, Il.[t := cnt])
          | _ -> (cnt, [])
        in
        let res_stmts cntone =
          match imm with
          | None -> (
              let nsz = Il.(num size size) in
              let nsz1 = Il.(num Int.(size + 1) size) in
              match instr.mnemonic with
              | M.RCL ->
                  let l = Il.(dst >> nsz1 - cnt) in
                  let c = Il.(ze size cf << cnt - num 1 size) in
                  let t1 = new_tmp size in
                  let t2 = new_tmp size in
                  if cntone then
                    Il.[t1 := c; t2 := dst << cnt || t1; assign_rm dst t2]
                  else
                    let t3 = new_tmp size in
                    Il.
                      [ t1 := l
                      ; t2 := c || t1
                      ; t3 := dst << cnt || t2
                      ; assign_rm dst t3 ]
              | M.RCR ->
                  let l = Il.(dst << nsz1 - cnt) in
                  let c = Il.(ze size cf << nsz - cnt) in
                  let t1 = new_tmp size in
                  let t2 = new_tmp size in
                  if cntone then
                    Il.[t1 := c; t2 := dst >> cnt || t1; assign_rm dst t2]
                  else
                    let t3 = new_tmp size in
                    Il.
                      [ t1 := l
                      ; t2 := c || t1
                      ; t3 := dst >> cnt || t2
                      ; assign_rm dst t3 ]
              | M.ROL ->
                  let t = new_tmp size in
                  Il.[t := dst << cnt || dst >> nsz - cnt; assign_rm dst t]
              (* ROR *)
              | _ ->
                  let t = new_tmp size in
                  Il.[t := dst >> cnt || dst << nsz - cnt; assign_rm dst t] )
          | Some imm when imm <> 0 -> (
            match instr.mnemonic with
            | M.RCL ->
                let s1 = size - 2 in
                let h1 = s1 - imm + 1 in
                let src =
                  if imm > 1 then Il.(ex h1 0 dst @@ cf @@ ex s1 h1 dst)
                  else Il.(ex h1 0 dst @@ cf)
                in
                [assign_rm dst src]
            | M.RCR ->
                let s1 = size - 1 in
                let i1 = imm - 1 in
                let src =
                  if imm > 1 then Il.(ex i1 1 dst @@ cf @@ ex s1 imm dst)
                  else Il.(cf @@ ex s1 imm dst)
                in
                [assign_rm dst src]
            | M.ROL ->
                let s1 = size - 1 in
                let h1 = s1 - imm in
                [assign_rm dst Il.(ex h1 0 dst @@ ex s1 Int.(h1 + 1) dst)]
            (* ROR *)
            | _ ->
                let s1 = size - 1 in
                let i1 = imm - 1 in
                [assign_rm dst Il.(ex i1 0 dst @@ ex s1 imm dst)] )
          | _ -> []
        in
        let stmts =
          match imm with
          | Some imm when imm = 0 -> Il.[]
          | Some imm ->
              let t = new_tmp size in
              let t_stmt = Il.(t := dst) in
              let cf_stmt =
                match instr.mnemonic with
                | M.RCL ->
                    let s = size - imm in
                    Il.(cf := ex s s t)
                | M.RCR ->
                    let s = imm - 1 in
                    Il.(cf := ex s s t)
                | M.ROL -> Il.(cf := lo 1 t)
                (* ROR *)
                | _ -> Il.(cf := hi 1 t)
              in
              let oF_stmt =
                match imm with
                | 1 -> (
                  match instr.mnemonic with
                  | M.RCR | M.ROR -> Il.(oF := cf ^ hi 1 dst)
                  (* RCL/ROL *)
                  | _ ->
                      let s2 = size - 2 in
                      Il.(oF := hi 1 dst ^ ex s2 s2 dst) )
                | _ -> Il.(oF := undefined 1)
              in
              if M.(equal instr.mnemonic RCR) then
                (t_stmt :: cnt_stmts) @ [oF_stmt] @ res_stmts false
                @ [cf_stmt]
              else
                (t_stmt :: cf_stmt :: cnt_stmts) @ res_stmts false @ [oF_stmt]
          | None ->
              let t = new_tmp size in
              let t_stmt = Il.(t := dst) in
              let cf_stmt =
                match instr.mnemonic with
                | M.RCL -> Il.(cf := lo 1 (t >> num size size - cnt))
                | M.RCR -> Il.(cf := lo 1 (t >> cnt - num 1 size))
                | M.ROL -> Il.(cf := lo 1 t)
                (* ROR *)
                | _ -> Il.(cf := hi 1 t)
              in
              let oF_stmt =
                match instr.mnemonic with
                | M.RCR | M.ROR -> Il.(oF := cf ^ hi 1 dst)
                (* RCL/ROL *)
                | _ ->
                    let s2 = size - 2 in
                    Il.(oF := hi 1 dst ^ ex s2 s2 dst)
              in
              let c0l = new_label () in
              let ctl = new_label () in
              let c1l = new_label () in
              let cgl = new_label () in
              cnt_stmts
              @ Il.
                  [ goto_if (cnt = num 0 size) c0l ctl
                  ; ( @ ) c0l
                  ; jmp (bitv endaddr wordsz)
                  ; ( @ ) ctl
                  ; t_stmt
                  ; goto_if (cnt = num 1 size) c1l cgl
                  ; ( @ ) c1l ]
              @ ( if M.(equal instr.mnemonic RCR) then
                  (oF_stmt :: res_stmts true) @ [cf_stmt]
                else res_stmts true @ [oF_stmt; cf_stmt] )
              @ Il.[jmp (bitv endaddr wordsz); ( @ ) cgl; oF := undefined 1]
              @ res_stmts false @ [cf_stmt]
        in
        Ok stmts
    | M.RORX ->
        let size = instr.operands.(0).size in
        let%bind cntmask =
          match instr.avx with
          | Some (A.Vex vex) when vex.l = 0 ->
              Ok (if is64 && vex.w <> 0 then 0x3F else 0x1F)
          | _ -> Error `BadInstr
        in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let%bind cnt, imm =
          match instr.immediate with
          | Some (Imm.One imm) ->
              let v = Int.(of_int64_exn imm.value land cntmask) in
              Ok (Il.(num v size), v)
          | _ -> Error (`BadOperand 2)
        in
        let stmts =
          match imm with
          | 0 -> Il.[]
          | imm ->
              let s1 = size - 1 in
              let i1 = imm - 1 in
              Il.[dst := ex i1 0 src @@ ex s1 imm src]
        in
        Ok stmts
    | M.SYSCALL ->
        if not is64 then Error `BadInstr
        else
          Ok
            Il.
              [ rcx := bitv endaddr wordsz
              ; r11 := flags_concat wordsz
              ; cs := hi 14 cs @@ num 0 2
              ; syscall () ]
    | M.SYSENTER ->
        Ok
          Il.
            [ iF := bit false
            ; vm := bit false
            ; rf := bit false
            ; cs := hi 14 cs @@ num 0 2
            ; syscall () ]
    | M.SYSEXIT ->
        let size =
          match instr.rex with
          | Some rex when rex.w <> 0 -> 64
          | _ -> 32
        in
        let cx', dx' =
          if size = 64 then (rcx, rdx)
          else if is64 then (Il.ze 64 ecx, Il.ze 64 edx)
          else (ecx, edx)
        in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ goto_if (lo 2 cs <> num 0 2) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; sp' := cx'
            ; cs := hi 14 cs @@ num 3 2
            ; jmp dx' ]
    | M.SYSRET ->
        if not is64 then Error `BadInstr
        else
          let l1 = new_label () in
          let l2 = new_label () in
          let ex_stmts = flags_extract r11 wordsz ~is_sysret:true in
          Ok
            ( Il.
                [ goto_if (lo 2 cs <> num 0 2) l1 l2
                ; ( @ ) l1
                ; exn ()
                ; ( @ ) l2 ]
            @ ex_stmts
            @ Il.[cs := hi 14 cs @@ num 3 2; jmp rcx] )
    | M.TEST ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand instr addr 1 in
        let and' = Il.(lhs & rhs) in
        let t = new_tmp size in
        let t_stmt = Il.(t := and') in
        let pf_stmts = arith_parity t size ctx in
        let cf_stmt = Il.(cf := bit false) in
        let oF_stmt = Il.(oF := bit false) in
        let zf_stmt = Il.(zf := t = num 0 size) in
        let sf_stmt = Il.(sf := t <$ num 0 size) in
        let af_stmt = Il.(af := undefined 1) in
        Ok ([t_stmt; cf_stmt; oF_stmt; zf_stmt; sf_stmt; af_stmt] @ pf_stmts)
    | M.TZCNT ->
        let size = instr.operands.(0).size in
        let%bind dst = make_operand_rm instr addr 0 in
        let%bind src = make_operand_rm instr addr 1 in
        let t = new_tmp size in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let mask = Il.(num 1 size << t) in
        Ok
          Il.
            [ t := num 0 size
            ; goto l1
            ; ( @ ) l1
            ; goto_if (t < num size size & (src & mask) = num 0 size) l2 l3
            ; ( @ ) l2
            ; t := t + num 1 size
            ; dst := dst + num 1 size
            ; goto l1
            ; ( @ ) l3
            ; cf := dst = num size size
            ; zf := dst = num 0 size
            ; oF := undefined size
            ; sf := undefined size
            ; pf := undefined size
            ; af := undefined size ]
    | M.UD0 | M.UD1 | M.UD2 -> Ok Il.[exn ()]
    | M.XCHG ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand_rm instr addr 1 in
        if Il.(equal_expr lhs rhs) then Ok Il.[]
        else
          let t = new_tmp size in
          Ok Il.[t := lhs; lhs := rhs; rhs := t]
    | M.XGETBV ->
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        Ok
          Il.
            [ goto_if (ecx = num 0 32) l1 l2
            ; ( @ ) l1
            ; eax := lo 32 xcr0
            ; edx := hi 32 xcr0
            ; jmp (bitv endaddr wordsz)
            ; ( @ ) l2
            ; goto_if (ecx = num 1 32) l3 l4
            ; ( @ ) l3
            ; unk ()
            ; jmp (bitv endaddr wordsz)
            ; ( @ ) l4
            ; exn () ]
    | M.XLAT ->
        let bx' =
          match instr.easz with
          | 16 -> bx
          | 32 -> ebx
          | _ -> rbx
        in
        let ld =
          if is64 then Il.(load `LE 1 mu (bx' + ze instr.easz al))
          else
            match make_seg_base instr wordsz with
            | Some base ->
                let base =
                  if instr.easz = wordsz then base
                  else Il.(lo instr.easz base)
                in
                Il.(load `LE 1 mu (base + bx' + ze instr.easz al))
            | None -> Il.(load `LE 1 mu (bx' + ze instr.easz al))
        in
        Ok Il.[al := ld]
    | M.XOR ->
        let size = instr.operands.(0).size in
        let%bind lhs = make_operand_rm instr addr 0 in
        let%bind rhs = make_operand instr addr 1 in
        let res_stmt, zf_stmt, sf_stmt =
          if Il.(equal_expr lhs rhs) then
            ( assign_rm lhs Il.(num 0 size)
            , Il.(zf := bit true)
            , Il.(sf := bit false) )
          else
            ( assign_rm lhs Il.(lhs ^ rhs)
            , Il.(zf := lhs = num 0 size)
            , Il.(sf := lhs <$ num 0 size) )
        in
        let cf_stmt = Il.(cf := bit false) in
        let oF_stmt = Il.(oF := bit false) in
        let af_stmt = Il.(af := undefined 1) in
        let pf_stmts = arith_parity lhs size ctx in
        Ok
          ( res_stmt :: zf_stmt :: sf_stmt :: cf_stmt :: oF_stmt :: af_stmt
          :: pf_stmts )
    | M.XSETBV ->
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ goto_if (ecx = num 0 32) l1 l2
            ; ( @ ) l1
            ; xcr0 := edx @@ eax
            ; jmp (bitv endaddr wordsz)
            ; ( @ ) l2
            ; exn () ]
    | _ -> Ok Il.[unk ()]
  in
  let stmts = Il.(( @ ) start_lbl) :: stmts in
  Il.fold_constants_in_stmts stmts
  |> Il.normalize_end endaddr wordsz ctx
  |> Il.normalize_conditions ctx
  |> implicit_ze is64 |> Il.remove_identity_assigns |> register_overlap is64
  |> (fun stmts -> Il.Context.reset_tmp ctx; stmts)
  |> Il.make_blocks

let lift_exn instr addr ctx =
  match lift instr addr ctx with
  | Ok il -> il
  | Error err -> raise (X86_lift_error err)
