open Base

type error =
  [ `OOB
  | `Invalid
  | `TooLong
  | `BadReg
  | `BadLock
  | `BadLegacyPrefix
  | `BadREX
  | `BadMap
  | `BadEVEX
  | `BadMVEX
  | `BadMask
  | `Unimpl ]

val string_of_error : error -> string

exception X86_decode_error of error

val decode : X86.Mode.t -> bytes -> (X86.Instruction.t, error) Result.t

val decode_exn : X86.Mode.t -> bytes -> X86.Instruction.t
