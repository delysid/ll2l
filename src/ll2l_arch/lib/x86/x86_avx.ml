open Base

type vex =
  { r: int
  ; x: int
  ; b: int
  ; map_select: int
  ; w: int
  ; vvvv: int
  ; l: int
  ; pp: int
  ; offset: int
  ; size: int }

type xop =
  { r: int
  ; x: int
  ; b: int
  ; map_select: int
  ; w: int
  ; vvvv: int
  ; l: int
  ; pp: int
  ; offset: int }

module Evex_tupletype = struct
  type t =
    | FV
    | HV
    | FVM
    | T1S
    | T1F
    | T2
    | T4
    | T8
    | HVM
    | QVM
    | OVM
    | M128
    | DUP
  [@@deriving equal]
end

type evex =
  { r: int
  ; x: int
  ; b: int
  ; r': int
  ; map_select: int
  ; w: int
  ; vvv: int
  ; pp: int
  ; z: int
  ; l': int
  ; l: int
  ; ll: int
  ; b': int
  ; v': int
  ; aaa: int
  ; offset: int
  ; tuple_type: Evex_tupletype.t option
  ; input_size: int option }

type mvex =
  { r: int
  ; x: int
  ; b: int
  ; r': int
  ; map_select: int
  ; w: int
  ; vvv: int
  ; pp: int
  ; e: int
  ; sss: int
  ; v': int
  ; kkk: int
  ; offset: int }

type t = Vex of vex | Xop of xop | Mvex of mvex | Evex of evex

let vector_length = function
  | Vex vex -> if vex.l = 0x00 then 128 else 256
  | Xop xop -> if xop.l = 0x00 then 128 else 256
  | Mvex _ -> 512
  | Evex evex -> (
    match evex.ll with
    | 0x00 -> 128
    | 0x01 -> 256
    | 0x03 -> 512
    | _ -> invalid_arg "bad evex.ll value" )
