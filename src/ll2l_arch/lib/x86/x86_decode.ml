(* this code is borrowed (and extended/improved) from the B2R2 project *)

open Core_kernel
module D = X86_descriptors
module DA = D.A
module DS = D.S
module M = X86_mnemonic
module R = X86_register
module O = X86_operand
module P = X86.Prefix
module A = X86.Avx
module Mode = X86.Mode

type error =
  [ `OOB
  | `Invalid
  | `TooLong
  | `BadReg
  | `BadLock
  | `BadLegacyPrefix
  | `BadREX
  | `BadMap
  | `BadEVEX
  | `BadMVEX
  | `BadMask
  | `Unimpl ]

let string_of_error = function
  | `OOB -> "out of bounds"
  | `Invalid -> "invalid"
  | `TooLong -> "too long"
  | `BadReg -> "bad register"
  | `BadLock -> "bad lock prefix"
  | `BadLegacyPrefix -> "bad legacy prefix"
  | `BadREX -> "bad REX prefix"
  | `BadMap -> "invalid map"
  | `BadEVEX -> "bad EVEX prefix"
  | `BadMVEX -> "bad MVEX prefix"
  | `BadMask -> "bad AVX mask"
  | `Unimpl -> "not implemented"

exception X86_decode_error of error

type size_cond = SzDef32 | SzDef64 | Sz64 | SzOnly64 | SzInv64

type ins_size =
  { mem_op_size: int
  ; mem_addr_size: int
  ; mem_reg_size: int
  ; reg_size: int
  ; operation_size: int
  ; size_cond: size_cond }

module Context = struct
  type prefix =
    { mutable has_lock: bool
    ; mutable group1: int
    ; mutable group2: int
    ; mutable has_operand_size_override: bool
    ; mutable has_address_size_override: bool
    ; mutable effective_segment: int
    ; mutable mandatory_candidate: int
    ; mutable offset_lock: int
    ; mutable offset_group1: int
    ; mutable offset_group2: int
    ; mutable offset_operand_size_override: int
    ; mutable offset_address_size_override: int
    ; mutable offset_segment: int
    ; mutable offset_mandatory: int }

  type t =
    { mode: Mode.t
    ; bytes: bytes
    ; mutable mnemonic: M.t
    ; mutable pos: int
    ; mutable prefixes: P.t array
    ; mutable rex: X86.Rex.t option
    ; mutable vrex: (int * int * int * int) option
    ; mutable avx: A.t option
    ; mutable vector_length: int option
    ; mutable reg_ops: R.t list
    ; prefix: prefix
    ; mutable effective_operand_size: int
    ; mutable effective_address_size: int
    ; mutable modrm: X86.Modrm.t option
    ; mutable sib: X86.Sib.t option
    ; mutable disp: X86.Displacement.t option
    ; mutable imm: X86.Immediate.t option
    ; mutable no_modrm: bool
    ; mutable is_3dnow: bool }

  let consumed ctx = Bytes.subo ctx.bytes ~len:ctx.pos

  let peek_o ctx o =
    let open Result.Let_syntax in
    let ok_len = ctx.pos + o <= X86.Instruction.max_length in
    let%bind _ = Result.ok_if_true ~error:`TooLong ok_len in
    let ok_bytes = Bytes.length ctx.bytes - ctx.pos > o in
    let%map _ = Result.ok_if_true ~error:`OOB ok_bytes in
    Char.to_int (Bytes.get ctx.bytes (ctx.pos + o)) land 0xFF

  let peek ctx = peek_o ctx 0

  let skip_o ctx o =
    let open Result.Let_syntax in
    let ok_len = ctx.pos + o - 1 < X86.Instruction.max_length in
    let%map _ = Result.ok_if_true ~error:`TooLong ok_len in
    ctx.pos <- ctx.pos + o

  let skip ctx = skip_o ctx 1

  let next ctx =
    let open Result.Let_syntax in
    let%map b = peek ctx in
    ctx.pos <- ctx.pos + 1;
    b

  let next_n ctx n =
    let open Result.Let_syntax in
    let ok_len = ctx.pos + n <= X86.Instruction.max_length in
    let%bind _ = Result.ok_if_true ~error:`TooLong ok_len in
    let ok_bytes = Bytes.length ctx.bytes - ctx.pos >= n in
    let%map _ = Result.ok_if_true ~error:`OOB ok_bytes in
    let bs = Bytes.sub ctx.bytes ~len:n ~pos:ctx.pos in
    ctx.pos <- ctx.pos + n;
    bs

  let next_int16 ctx =
    let open Result.Let_syntax in
    let%map bs = next_n ctx 2 in
    let b1 = Char.to_int (Bytes.get bs 0) land 0xFF in
    let b2 = Char.to_int (Bytes.get bs 1) land 0xFF in
    (b2 lsl 8) lor b1 land 0xFFFF

  let next_int32 ctx =
    let open Result.Let_syntax in
    let%map bs = next_n ctx 4 in
    let b1 = Int32.(of_int_exn (Char.to_int (Bytes.get bs 0)) land 0xFFl) in
    let b2 = Int32.(of_int_exn (Char.to_int (Bytes.get bs 1)) land 0xFFl) in
    let b3 = Int32.(of_int_exn (Char.to_int (Bytes.get bs 2)) land 0xFFl) in
    let b4 = Int32.(of_int_exn (Char.to_int (Bytes.get bs 3)) land 0xFFl) in
    Int32.(
      (b4 lsl 24) lor (b3 lsl 16) lor (b2 lsl 8) lor b1 land 0xFFFFFFFFl)

  let next_int64 ctx =
    let open Result.Let_syntax in
    let%map bs = next_n ctx 8 in
    let b1 = Int64.(of_int (Char.to_int (Bytes.get bs 0)) land 0xFFL) in
    let b2 = Int64.(of_int (Char.to_int (Bytes.get bs 1)) land 0xFFL) in
    let b3 = Int64.(of_int (Char.to_int (Bytes.get bs 2)) land 0xFFL) in
    let b4 = Int64.(of_int (Char.to_int (Bytes.get bs 3)) land 0xFFL) in
    let b5 = Int64.(of_int (Char.to_int (Bytes.get bs 4)) land 0xFFL) in
    let b6 = Int64.(of_int (Char.to_int (Bytes.get bs 5)) land 0xFFL) in
    let b7 = Int64.(of_int (Char.to_int (Bytes.get bs 6)) land 0xFFL) in
    let b8 = Int64.(of_int (Char.to_int (Bytes.get bs 7)) land 0xFFL) in
    Int64.(
      (b8 lsl 56) lor (b7 lsl 48) lor (b6 lsl 40) lor (b5 lsl 32)
      lor (b4 lsl 24) lor (b3 lsl 16) lor (b2 lsl 8) lor b1
      land 0xFFFFFFFFFFFFFFFFL)

  let parse_rex ctx b offset =
    let open Result.Let_syntax in
    let%map _ = Result.ok_if_true ~error:`Invalid (b land 0xF0 = 0x40) in
    let rex =
      X86.Rex.
        { w= (b lsr 3) land 0x01
        ; r= (b lsr 2) land 0x01
        ; x= (b lsr 1) land 0x01
        ; b= b land 0x01
        ; offset }
    in
    ctx.rex <- Some rex

  let vrex_three_byte w r x b =
    let r = lnot r land 0x01 in
    let x = lnot x land 0x01 in
    let b = lnot b land 0x01 in
    (w, r, x, b)

  let parse_xop ctx b1 b2 b3 offset =
    let open Result.Let_syntax in
    let%bind _ =
      Result.ok_if_true ~error:`Invalid
        (not (b1 <> 0x8F || b2 land 0x1F < 8 || offset <> ctx.pos - 3))
    in
    let map_select = b2 land 0x1F in
    let ok_map = map_select >= 0x08 && map_select <= 0x0A in
    let%map _ = Result.ok_if_true ~error:`BadMap ok_map in
    let r = (b2 lsr 7) land 0x01 in
    let x = (b2 lsr 6) land 0x01 in
    let b = (b2 lsr 5) land 0x01 in
    let w = (b3 lsr 7) land 0x01 in
    let vvvv = (b3 lsr 3) land 0x0F in
    let l = (b3 lsr 2) land 0x01 in
    let pp = b3 land 0x03 in
    let vlen = if l = 0x00 then 128 else 256 in
    let xop = A.{r; x; b; map_select; w; vvvv; l; pp; offset} in
    ctx.avx <- Some (A.Xop xop);
    ctx.vector_length <- Some vlen;
    ctx.vrex <- Some (vrex_three_byte w r x b)

  let parse_vex ctx b1 b2 ?(b3 = None) offset =
    let open Result.Let_syntax in
    let%map vex =
      match b1 with
      | 0xC4 when offset = ctx.pos - 3 -> (
          let map_select = b2 land 0x1F in
          if map_select > 0x03 then Error `BadMap
          else
            match b3 with
            | Some b3 ->
                let vex =
                  A.
                    { r= (b2 lsr 7) land 0x01
                    ; x= (b2 lsr 6) land 0x01
                    ; b= (b2 lsr 5) land 0x01
                    ; map_select
                    ; w= (b3 lsr 7) land 0x01
                    ; vvvv= (b3 lsr 3) land 0x0F
                    ; l= (b3 lsr 2) land 0x01
                    ; pp= b3 land 0x03
                    ; offset
                    ; size= 3 }
                in
                Ok vex
            | None -> Error `Invalid )
      | 0xC5 when offset = ctx.pos - 2 ->
          let vex =
            A.
              { r= (b2 lsr 7) land 0x01
              ; x= 1
              ; b= 1
              ; map_select= 1
              ; w= 0
              ; vvvv= (b2 lsr 3) land 0x0F
              ; l= (b2 lsr 2) land 0x01
              ; pp= b2 land 0x03
              ; offset
              ; size= 2 }
          in
          Ok vex
      | _ -> Error `Invalid
    in
    let vlen = if vex.l = 0x00 then 128 else 256 in
    let vrex =
      if vex.size = 2 then if vex.r = 0 then Some (0, 1, 0, 0) else None
      else Some (vrex_three_byte vex.w vex.r vex.x vex.b)
    in
    ctx.avx <- Some (A.Vex vex);
    ctx.vector_length <- Some vlen;
    ctx.vrex <- vrex;
    ()

  let parse_evex ctx b1 b2 b3 b4 offset =
    if b1 <> 0x62 || (b3 lsr 2) land 0x01 <> 0x01 then Error `Invalid
    else if (b2 lsr 2) land 0x03 <> 0x00 then Error `BadEVEX
    else
      let map_select = b2 land 0x03 in
      if map_select = 0x00 then Error `BadMap
      else
        let v' = (b4 lsr 3) land 0x01 in
        if v' = 0x00 && not Mode.(equal ctx.mode AMD64) then Error `BadEVEX
        else
          let z = (b4 lsr 7) land 0x01 in
          let aaa = b4 land 0x07 in
          if z <> 0x00 && aaa = 0x00 then Error `BadMask
          else
            let b' = (b4 lsr 4) land 0x01 in
            let ll = (b4 lsr 5) land 0x03 in
            if b' = 0x00 && ll = 3 then Error `BadEVEX
            else
              let r = (b2 lsr 7) land 0x01 in
              let x = (b2 lsr 6) land 0x01 in
              let b = (b2 lsr 5) land 0x01 in
              let r' = (b2 lsr 4) land 0x01 in
              let w = (b3 lsr 7) land 0x01 in
              let vvv = (b3 lsr 3) land 0x0F in
              let pp = b3 land 0x03 in
              let l' = (b4 lsr 7) land 0x01 in
              let l = (b4 lsr 5) land 0x01 in
              let evex =
                A.
                  { r
                  ; x
                  ; b
                  ; r'
                  ; map_select
                  ; w
                  ; vvv
                  ; pp
                  ; z
                  ; l'
                  ; l
                  ; ll
                  ; b'
                  ; v'
                  ; aaa
                  ; offset
                  ; tuple_type= None
                  ; input_size= None }
              in
              let vlen =
                if ll = 0x00 then 128 else if ll = 0x01 then 256 else 512
                (* 0x03 *)
              in
              ctx.avx <- Some (A.Evex evex);
              ctx.vector_length <- Some vlen;
              ctx.vrex <- Some (vrex_three_byte w r x b);
              Ok ()

  let parse_mvex ctx b1 b2 b3 b4 offset =
    if
      b1 <> 0x62
      || offset <> ctx.pos - 4
      || (b3 lsr 2) land 0x01 <> 0x00
      || not Mode.(equal ctx.mode AMD64)
    then Error `Invalid
    else
      let map_select = b2 land 0x0F in
      if map_select > 0x03 then Error `BadMap
      else
        let r = (b2 lsr 7) land 0x01 in
        let x = (b2 lsr 6) land 0x01 in
        let b = (b2 lsr 5) land 0x01 in
        let r' = (b2 lsr 4) land 0x01 in
        let w = (b3 lsr 7) land 0x01 in
        let vvv = (b3 lsr 3) land 0x0F in
        let pp = b3 land 0x03 in
        let e = (b4 lsr 7) land 0x01 in
        let sss = (b4 lsr 4) land 0x07 in
        let v' = (b4 lsr 3) land 0x01 in
        let kkk = b4 land 0x07 in
        let mvex =
          A.{r; x; b; r'; map_select; w; vvv; pp; e; sss; v'; kkk; offset}
        in
        ctx.avx <- Some (A.Mvex mvex);
        ctx.vector_length <- Some 512;
        ctx.vrex <- Some (vrex_three_byte w r x b);
        Ok ()

  let modrm_mod b = (b lsr 6) land 0x03

  let modrm_reg b = (b lsr 3) land 0x07

  let modrm_rm b = b land 0x07

  let parse_modrm ctx b offset =
    if Option.is_some ctx.modrm || offset <> ctx.pos - 1 then Error `Invalid
    else
      let modrm =
        X86.Modrm.
          {mod'= modrm_mod b; reg= modrm_reg b; rm= modrm_rm b; offset}
      in
      ctx.modrm <- Some modrm;
      Ok ()

  let parse_sib ctx b offset =
    match ctx.modrm with
    | Some modrm
      when modrm.rm = 4 && Option.is_none ctx.sib && offset = ctx.pos - 1 ->
        let sib =
          X86.Sib.
            {scale= modrm_mod b; index= modrm_reg b; base= modrm_rm b; offset}
        in
        ctx.sib <- Some sib;
        Ok ()
    | _ -> Error `Invalid

  let parse_displacement ctx size =
    if Option.is_some ctx.disp then Error `Invalid
    else
      let open Result.Let_syntax in
      let offset = ctx.pos in
      let%map value =
        match size with
        | 8 ->
            let%map value = next ctx in
            Int64.(of_int value land 0xFFL)
        | 16 ->
            let%map value = next_int16 ctx in
            Int64.(of_int value land 0xFFFFL)
        | 32 ->
            let%map value = next_int32 ctx in
            Int64.(of_int32 value land 0xFFFFFFFFL)
        | 64 -> next_int64 ctx
        | _ -> Error `Invalid
      in
      ctx.disp <- Some X86.Displacement.{value; size; offset}

  let parse_immediate ctx size signed relative =
    match ctx.imm with
    | None | Some (X86.Immediate.One _) -> (
        if (not signed) && relative then Error `Invalid
        else
          let open Result.Let_syntax in
          let offset = ctx.pos in
          let%bind value =
            match size with
            | 8 ->
                let%map value = next ctx in
                Int64.(of_int value land 0xFFL)
            | 16 ->
                let%map value = next_int16 ctx in
                Int64.(of_int value land 0xFFFFL)
            | 32 ->
                let%map value = next_int32 ctx in
                Int64.(of_int32 value land 0xFFFFFFFFL)
            | 64 -> next_int64 ctx
            | _ -> Error `Invalid
          in
          let imm = X86.Immediate.{value; signed; relative; size; offset} in
          match ctx.imm with
          | Some (X86.Immediate.One imm1) ->
              return (ctx.imm <- Some (X86.Immediate.Two (imm1, imm)))
          | None -> return (ctx.imm <- Some (X86.Immediate.One imm))
          | _ -> Error `Invalid (* unreachable *) )
    | Some (X86.Immediate.Two _) -> Error `Invalid

  let parse_optional_prefixes ctx =
    if not (Array.is_empty ctx.prefixes) then Error `Invalid
    else
      let open Result.Let_syntax in
      let rex_byte = ref 0x00 in
      let rex_offset = ref 0 in
      let rec aux res offset =
        let%bind b = peek ctx in
        let finished =
          match b with
          | 0xF0 ->
              ctx.prefix.has_lock <- true;
              ctx.prefix.offset_lock <- offset;
              false
          | 0xF2 | 0xF3 ->
              ctx.prefix.group1 <- b;
              ctx.prefix.mandatory_candidate <- b;
              ctx.prefix.offset_group1 <- offset;
              false
          | 0x2E | 0x36 | 0x3E | 0x26 ->
              ctx.prefix.group2 <- b;
              ctx.prefix.offset_group2 <- offset;
              if
                (not Mode.(equal ctx.mode AMD64))
                || ctx.prefix.effective_segment <> 0x64
                   && ctx.prefix.effective_segment <> 0x65
              then (
                ctx.prefix.effective_segment <- b;
                ctx.prefix.offset_segment <- offset );
              false
          | 0x64 | 0x65 ->
              ctx.prefix.group2 <- b;
              ctx.prefix.offset_group2 <- offset;
              ctx.prefix.effective_segment <- b;
              ctx.prefix.offset_segment <- offset;
              false
          | 0x66 ->
              ctx.prefix.has_operand_size_override <- true;
              ctx.prefix.offset_operand_size_override <- offset;
              if ctx.prefix.mandatory_candidate = 0 then (
                ctx.prefix.mandatory_candidate <- 0x66;
                ctx.prefix.offset_mandatory <- offset );
              false
          | 0x67 ->
              ctx.prefix.has_address_size_override <- true;
              ctx.prefix.offset_address_size_override <- offset;
              false
          | _ ->
              if Mode.(equal ctx.mode AMD64) && b land 0xF0 = 0x40 then (
                rex_byte := b;
                rex_offset := offset;
                false )
              else true
        in
        if finished then return (List.rev res)
        else (
          if !rex_byte <> 0x00 && !rex_byte <> b then (
            rex_byte := 0x00;
            rex_offset := 0 );
          let%bind _ = skip ctx in
          aux (b :: res) (offset + 1) )
      in
      let%bind prefixes = aux [] 0 in
      let prefixes = Array.of_list prefixes in
      let prefixes =
        Array.mapi prefixes ~f:(fun i b ->
            let kind =
              if
                (b = 0x66 && i = ctx.prefix.offset_operand_size_override)
                || (b = 0x67 && i = ctx.prefix.offset_address_size_override)
                || (b = !rex_byte && i = !rex_offset)
              then P.Effective
              else P.Ignored
            in
            P.{kind; value= b} )
      in
      ctx.prefixes <- prefixes;
      if !rex_byte = 0x00 then return ()
      else parse_rex ctx !rex_byte !rex_offset

  let parse_vex_info ctx =
    let open Result.Let_syntax in
    let is_vex b =
      if b land 0xF0 >= 0xC0 then true else Mode.(equal ctx.mode AMD64)
    in
    let check () =
      if Option.is_some ctx.rex then Error `BadREX
      else if ctx.prefix.has_lock then Error `BadLock
      else if ctx.prefix.mandatory_candidate <> 0 then Error `BadLegacyPrefix
      else Ok ()
    in
    let%bind b1 = peek ctx in
    if Bytes.length ctx.bytes - ctx.pos <= 1 then return ()
    else
      let%bind b2 = peek_o ctx 1 in
      match b1 with
      | 0xC5 when is_vex b2 ->
          let%bind _ = check () in
          let offset = ctx.pos in
          let%bind _ = skip_o ctx 2 in
          parse_vex ctx b1 b2 offset
      | 0xC4 when is_vex b2 ->
          let%bind _ = check () in
          let offset = ctx.pos in
          let%bind b3 = peek_o ctx 2 in
          let%bind _ = skip_o ctx 3 in
          parse_vex ctx b1 b2 offset ~b3:(Some b3)
      | 0x62 when is_vex b2 ->
          let%bind _ = check () in
          let offset = ctx.pos in
          let%bind b3 = peek_o ctx 2 in
          let%bind b4 = peek_o ctx 3 in
          let%bind _ = skip_o ctx 4 in
          if (b3 lsr 2) land 0x01 = 0 then parse_mvex ctx b1 b2 b3 b4 offset
          else parse_evex ctx b1 b2 b3 b4 offset
      | 0x8F when b2 land 0x1F >= 8 ->
          let%bind _ = check () in
          let offset = ctx.pos in
          let%bind b3 = peek_o ctx 2 in
          let%bind _ = skip_o ctx 3 in
          parse_xop ctx b1 b2 b3 offset
      | _ -> return ()

  let vex_pp ctx =
    match ctx.avx with
    | Some (A.Vex vex) -> Ok vex.pp
    | Some (A.Xop xop) -> Ok xop.pp
    | Some (A.Evex evex) -> Ok evex.pp
    | Some (A.Mvex mvex) -> Ok mvex.pp
    | None -> Error `Invalid

  let is_reg_operand = function
    | D.Reg _ | D.RegGrp _
     |D.ModeSize (DA.D, _)
     |D.ModeSize (DA.G, _)
     |D.ModeSize (DA.KM, _)
     |D.ModeSize (DA.KR, _)
     |D.ModeSize (DA.N, _)
     |D.ModeSize (DA.P, _)
     |D.ModeSize (DA.R, _)
     |D.ModeSize (DA.S, _)
     |D.ModeSize (DA.T, _)
     |D.ModeSize (DA.U, _)
     |D.ModeSize (DA.V, _)
     |D.ModeSize (DA.VZ, _) -> true
    | _ -> false

  let operation_size reg_size opr_size m descs =
    match m with
    | M.PUSH | M.POP -> opr_size
    | M.MOVSB | M.INSB | M.STOSB | M.LODSB | M.OUTSB | M.SCASB -> 8
    | M.OUTSW -> 16
    | M.OUTSD -> 32
    | _ ->
        if Array.is_empty descs then opr_size
        else if is_reg_operand descs.(0) then reg_size
        else opr_size

  let select_rex ctx =
    match ctx.vrex with
    | Some vrex -> Some vrex
    | None -> (
      match ctx.rex with
      | Some rex -> Some (rex.w, rex.r, rex.x, rex.b)
      | None -> None )

  let size_by_size_desc ctx desc =
    match desc with
    | DS.B -> (8, 8)
    | DS.Bnd ->
        let eff = if Mode.(equal ctx.mode AMD64) then 128 else 64 in
        (ctx.effective_operand_size, eff)
    | DS.W -> (16, 16)
    | DS.D -> (32, 32)
    | DS.DB -> (32, 8)
    | DS.DW -> (32, 16)
    | DS.P -> (
      match ctx.effective_operand_size with
      | 16 -> (16, 32)
      | 32 -> (32, 48)
      | _ -> (64, 80) )
    | DS.PI | DS.Q -> (64, 64)
    | DS.DQA -> (ctx.effective_address_size, ctx.effective_address_size)
    | DS.DQB -> (128, 8)
    | DS.DQW -> (128, 16)
    | DS.DQD | DS.SSD -> (128, 32)
    | DS.DQQDQ -> (
      match ctx.vector_length with
      | Some 128 -> (128, 64)
      | _ -> (128, 128) )
    | DS.DQDQ -> (
      match ctx.vector_length with
      | Some 128 -> (128, 32)
      | _ -> (128, 64) )
    | DS.DQWD -> (
      match ctx.vector_length with
      | Some 128 -> (128, 16)
      | _ -> (128, 32) )
    | DS.DQQ | DS.SDQ | DS.SSQ -> (128, 64)
    | DS.DQ | DS.SD | DS.SS -> (128, 128)
    | DS.Hsae -> (
      match ctx.vector_length with
      | Some 128 -> (128, 64)
      | Some 256 -> (128, 128)
      | Some 512 -> (256, 256)
      (* fixme *)
      | _ -> (128, 64) )
    | DS.PSQ -> (Option.value_exn ctx.vector_length, 64)
    | DS.PD | DS.PS | DS.X | DS.XZ | DS.Msk ->
        let vlen =
          match ctx.vector_length with
          | Some vlen -> vlen
          (* fixme *)
          | None -> 128
        in
        (vlen, vlen)
    | DS.QQ -> (256, 256)
    | DS.QQQ -> (512, 512)
    | DS.QQQA -> (ctx.effective_address_size, 512)
    | DS.Y ->
        let has_rex_w =
          match select_rex ctx with
          | Some (w, _, _, _) when w <> 0 -> true
          | _ -> false
        in
        if Mode.(equal ctx.mode AMD64) && has_rex_w then (64, 64)
        else (32, 32)
    | DS.YB | DS.YD | DS.YW ->
        let osz =
          match desc with
          | DS.YB -> 8
          | DS.YD -> 32
          (* YW *)
          | _ -> 16
        in
        let has_rex_w =
          match select_rex ctx with
          | Some (w, _, _, _) when w <> 0 -> true
          | _ -> false
        in
        if Mode.(equal ctx.mode AMD64) && has_rex_w then (64, osz)
        else (32, osz)
    | DS.Z -> (
      match ctx.effective_operand_size with
      | 32 | 64 -> (32, ctx.effective_operand_size)
      | _ -> (ctx.effective_operand_size, ctx.effective_operand_size) )
    | _ -> (ctx.effective_operand_size, ctx.effective_operand_size)

  let reg_size ctx descs =
    match Array.find descs ~f:is_reg_operand with
    | Some (D.Reg reg) ->
        Result.(
          of_option (R.width_of reg) ~error:`BadReg
          >>| fun w ->
          if Mode.(equal ctx.mode AMD64) then w.width64 else w.width)
    | Some (D.RegGrp (_, s, _)) | Some (D.ModeSize (_, s)) ->
        Ok (size_by_size_desc ctx s |> fst)
    | _ -> Ok ctx.effective_operand_size

  let conv_mem_size = function
    | D.ModeSize (DA.BndM, s)
     |D.ModeSize (DA.E, s)
     |D.ModeSize (DA.M, s)
     |D.ModeSize (DA.MZ, s)
     |D.ModeSize (DA.O, s)
     |D.ModeSize (DA.Q, s)
     |D.ModeSize (DA.U, s)
     |D.ModeSize (DA.W, s)
     |D.ModeSize (DA.WZ, s)
     |D.ModeSize (DA.X, s)
     |D.ModeSize (DA.Y, s) -> Some s
    | _ -> None

  let mem_size ctx descs =
    match Array.find_map descs ~f:conv_mem_size with
    | None ->
        ( ctx.effective_operand_size
        , ctx.effective_address_size
        , ctx.effective_operand_size )
    | Some s ->
        let r, o = size_by_size_desc ctx s in
        (o, ctx.effective_address_size, r)

  let opr_size size = function
    | Sz64 -> 64
    | SzDef64 when size = 32 -> 64
    | _ -> size

  let has_effective ctx v =
    Array.exists ctx.prefixes ~f:(fun p ->
        match p with
        | {kind; value} when P.(equal_kind kind Effective) && value = v ->
            true
        | _ -> false )

  let make_mandatory ctx =
    let rec aux i =
      if i < 0 then ()
      else
        match ctx.prefixes.(i) with
        | p when p.value = ctx.prefix.mandatory_candidate ->
            ctx.prefixes.(i) <- {p with kind= P.Mandatory}
        | _ -> aux (i - 1)
    in
    aux (Array.length ctx.prefixes - 1)

  let ignore_all_prefix ctx v =
    Array.iteri ctx.prefixes ~f:(fun i p ->
        if p.value = v then ctx.prefixes.(i) <- {p with kind= P.Ignored} )

  let size32 ctx =
    let has_opsize = has_effective ctx 0x66 in
    let has_addrsize = has_effective ctx 0x67 in
    if has_opsize then if has_addrsize then (16, 16) else (16, 32)
    else if has_addrsize then (32, 16)
    else (32, 32)

  let size64 ctx size_cond =
    let has_opsize = has_effective ctx 0x66 in
    let has_addrsize = has_effective ctx 0x67 in
    match select_rex ctx with
    | Some (w, _, _, _) when w <> 0 ->
        if has_addrsize then (64, 32) else (64, 64)
    | _ ->
        if has_opsize then
          if has_addrsize then (16, 32) else (opr_size 16 size_cond, 64)
        else if has_addrsize then (32, 32)
        else (opr_size 32 size_cond, 64)

  let set_effective_size ctx size_cond =
    let opsz, addrsz =
      if Mode.(equal ctx.mode AMD64) then size64 ctx size_cond
      else size32 ctx
    in
    ctx.effective_operand_size <- opsz;
    ctx.effective_address_size <- addrsz

  let new_ins_size ctx size_cond op descs =
    let open Result.Let_syntax in
    set_effective_size ctx size_cond;
    let%map r = reg_size ctx descs in
    let m_op, m_adr, m_reg = mem_size ctx descs in
    { mem_op_size= m_op
    ; mem_addr_size= m_adr
    ; mem_reg_size= m_reg
    ; reg_size= r
    ; operation_size= operation_size r m_op op descs
    ; size_cond }

  let process_op_descs descs = function
    | M.CMPSD when not (Array.is_empty descs) -> descs
    | M.CMPSB | M.CMPSW | M.CMPSD | M.CMPSQ -> [||]
    | _ -> descs

  let parse_op ctx m size_cond descs =
    let open Result.Let_syntax in
    let%map ins_size = new_ins_size ctx size_cond m descs in
    (m, ins_size, process_op_descs descs m)

  let st_reg_tbl = R.[|ST0; ST1; ST2; ST3; ST4; ST5; ST6; ST7|]

  let d8_op_within_00_to_BF ctx b =
    match modrm_reg b with
    | 0 -> Ok M.FADD
    | 1 -> Ok M.FMUL
    | 2 -> Ok M.FCOM
    | 3 -> Ok M.FCOMP
    | 4 -> Ok M.FSUB
    | 5 -> Ok M.FSUBR
    | 6 -> Ok M.FDIV
    | 7 -> Ok M.FDIVR
    | _ -> Error `Invalid

  let dc_op_within_00_to_BF ctx = d8_op_within_00_to_BF ctx

  let d8_op_outside_00_to_BF ctx = function
    | b when b >= 0xC0 && b <= 0xC7 -> Ok M.FADD
    | b when b >= 0xC8 && b <= 0xCF -> Ok M.FMUL
    | b when b >= 0xD0 && b <= 0xD7 -> Ok M.FCOM
    | b when b >= 0xD8 && b <= 0xDF -> Ok M.FCOMP
    | b when b >= 0xE0 && b <= 0xE7 -> Ok M.FSUB
    | b when b >= 0xE8 && b <= 0xEF -> Ok M.FSUBR
    | b when b >= 0xF0 && b <= 0xF7 -> Ok M.FDIV
    | b when b >= 0xF8 && b <= 0xFF -> Ok M.FDIVR
    | _ -> Error `Invalid

  let d9_op_within_00_to_BF ctx b =
    match modrm_reg b with
    | 0 -> Ok M.FLD
    | 2 -> Ok M.FST
    | 3 -> Ok M.FSTP
    | 4 -> Ok M.FLDENV
    | 5 -> Ok M.FLDCW
    | 6 -> Ok M.FSTENV
    | 7 -> Ok M.FNSTCW
    | _ -> Error `Invalid

  let d9_op_outside_00_to_BF ctx = function
    | b when b >= 0xC0 && b <= 0xC7 -> Ok M.FLD
    | b when b >= 0xC8 && b <= 0xCF -> Ok M.FXCH
    | 0xD0 -> Ok M.FNOP
    | 0xE0 -> Ok M.FCHS
    | 0xE1 -> Ok M.FABS
    | 0xE4 -> Ok M.FTST
    | 0xE5 -> Ok M.FXAM
    | 0xE8 -> Ok M.FLD1
    | 0xE9 -> Ok M.FLDL2T
    | 0xEA -> Ok M.FLDL2E
    | 0xEB -> Ok M.FLDPI
    | 0xEC -> Ok M.FLDLG2
    | 0xED -> Ok M.FLDLN2
    | 0xEE -> Ok M.FLDZ
    | 0xF0 -> Ok M.F2XM1
    | 0xF1 -> Ok M.FYL2X
    | 0xF2 -> Ok M.FPTAN
    | 0xF3 -> Ok M.FPATAN
    | 0xF4 -> Ok M.FXTRACT
    | 0xF5 -> Ok M.FPREM1
    | 0xF6 -> Ok M.FDECSTP
    | 0xF7 -> Ok M.FINCSTP
    | 0xF8 -> Ok M.FPREM
    | 0xF9 -> Ok M.FYL2XP1
    | 0xFA -> Ok M.FSQRT
    | 0xFB -> Ok M.FSINCOS
    | 0xFC -> Ok M.FRNDINT
    | 0xFD -> Ok M.FSCALE
    | 0xFE -> Ok M.FSIN
    | 0xFF -> Ok M.FCOS
    | _ -> Error `Invalid

  let da_op_within_00_to_BF ctx b =
    match modrm_reg b with
    | 0 -> Ok M.FIADD
    | 1 -> Ok M.FIMUL
    | 2 -> Ok M.FICOM
    | 3 -> Ok M.FICOMP
    | 4 -> Ok M.FISUB
    | 5 -> Ok M.FISUBR
    | 6 -> Ok M.FIDIV
    | 7 -> Ok M.FIDIVR
    | _ -> Error `Invalid

  let de_op_within_00_to_BF ctx = da_op_within_00_to_BF ctx

  let da_op_outside_00_to_BF ctx = function
    | b when b >= 0xC0 && b <= 0xC7 -> Ok M.FCMOVB
    | b when b >= 0xC8 && b <= 0xCF -> Ok M.FCMOVE
    | b when b >= 0xD0 && b <= 0xD7 -> Ok M.FCMOVBE
    | b when b >= 0xD8 && b <= 0xDF -> Ok M.FCMOVU
    | 0xE9 -> Ok M.FUCOMPP
    | _ -> Error `Invalid

  let db_op_within_00_to_BF ctx b =
    match modrm_reg b with
    | 0 -> Ok M.FILD
    | 1 -> Ok M.FISTTP
    | 2 -> Ok M.FIST
    | 3 -> Ok M.FISTP
    | 5 -> Ok M.FLD
    | 7 -> Ok M.FSTP
    | _ -> Error `Invalid

  let db_op_outside_00_to_BF ctx = function
    | b when b >= 0xC0 && b <= 0xC7 -> Ok M.FCMOVNB
    | b when b >= 0xC8 && b <= 0xCF -> Ok M.FCMOVNE
    | b when b >= 0xD0 && b <= 0xD7 -> Ok M.FCMOVNBE
    | b when b >= 0xD8 && b <= 0xDF -> Ok M.FCMOVNU
    | b when b >= 0xE8 && b <= 0xEF -> Ok M.FUCOMI
    | b when b >= 0xF0 && b <= 0xF7 -> Ok M.FCOMI
    | 0xE2 -> Ok M.FNCLEX
    | 0xE3 -> Ok M.FNINIT
    | _ -> Error `Invalid

  let dc_op_outside_00_to_BF ctx = function
    | b when b >= 0xC0 && b <= 0xC7 -> Ok M.FADD
    | b when b >= 0xC8 && b <= 0xCF -> Ok M.FMUL
    | b when b >= 0xE0 && b <= 0xE7 -> Ok M.FSUBR
    | b when b >= 0xE8 && b <= 0xEF -> Ok M.FSUB
    | b when b >= 0xF0 && b <= 0xF7 -> Ok M.FDIVR
    | b when b >= 0xF8 && b <= 0xFF -> Ok M.FDIV
    | _ -> Error `Invalid

  let dd_op_within_00_to_BF ctx b =
    match modrm_reg b with
    | 0 -> Ok M.FLD
    | 1 -> Ok M.FISTTP
    | 2 -> Ok M.FST
    | 3 -> Ok M.FSTP
    | 4 -> Ok M.FRSTOR
    | 6 -> Ok M.FNSAVE
    | 7 -> Ok M.FNSTSW
    | _ -> Error `Invalid

  let dd_op_outside_00_to_BF ctx = function
    | b when b >= 0xC0 && b <= 0xC7 -> Ok M.FFREE
    | b when b >= 0xC8 && b <= 0xCF -> Ok M.FXCH4
    | b when b >= 0xD0 && b <= 0xD7 -> Ok M.FST
    | b when b >= 0xD8 && b <= 0xDF -> Ok M.FSTP
    | b when b >= 0xE0 && b <= 0xE7 -> Ok M.FUCOM
    | b when b >= 0xE8 && b <= 0xEF -> Ok M.FUCOMP
    | _ -> Error `Invalid

  let de_op_outside_00_to_BF ctx = function
    | b when b >= 0xC0 && b <= 0xC7 -> Ok M.FADDP
    | b when b >= 0xC8 && b <= 0xCF -> Ok M.FMULP
    | 0xD9 -> Ok M.FCOMPP
    | b when b >= 0xE0 && b <= 0xE7 -> Ok M.FSUBRP
    | b when b >= 0xE8 && b <= 0xEF -> Ok M.FSUBP
    | b when b >= 0xF0 && b <= 0xF7 -> Ok M.FDIVRP
    | b when b >= 0xF8 && b <= 0xFF -> Ok M.FDIVP
    | _ -> Error `Invalid

  let df_op_within_00_to_BF ctx b =
    match modrm_reg b with
    | 0 -> Ok M.FILD
    | 1 -> Ok M.FISTTP
    | 2 -> Ok M.FIST
    | 3 -> Ok M.FISTP
    | 4 -> Ok M.FBLD
    | 5 -> Ok M.FILD
    | 6 -> Ok M.FBSTP
    | 7 -> Ok M.FISTP
    | _ -> Error `Invalid

  let df_op_outside_00_to_BF ctx = function
    | 0xE0 -> Ok M.FNSTSW
    | b when b >= 0xE8 && b <= 0xEF -> Ok M.FUCOMIP
    | b when b >= 0xF0 && b <= 0xF7 -> Ok M.FCOMIP
    | _ -> Error `Invalid

  let d8_over_BF ctx b =
    let open Result.Let_syntax in
    let r2 = st_reg_tbl.(modrm_rm b) in
    let%map m = d8_op_outside_00_to_BF ctx b in
    (m, [R.ST0; r2])

  let d9_over_BF ctx b =
    let open Result.Let_syntax in
    let ops =
      if b < 0xC0 || b >= 0xD0 then [] else [st_reg_tbl.(modrm_rm b)]
    in
    let%map m = d9_op_outside_00_to_BF ctx b in
    (m, ops)

  let da_over_BF ctx b =
    let open Result.Let_syntax in
    let ops = if b = 0xE9 then [] else [R.ST0; st_reg_tbl.(modrm_rm b)] in
    let%map m = da_op_outside_00_to_BF ctx b in
    (m, ops)

  let db_over_BF ctx b =
    let open Result.Let_syntax in
    let ops =
      if b = 0xE2 || b = 0xE3 then [] else [R.ST0; st_reg_tbl.(modrm_rm b)]
    in
    let%map m = db_op_outside_00_to_BF ctx b in
    (m, ops)

  let dc_over_BF ctx b =
    let open Result.Let_syntax in
    let%map m = dc_op_outside_00_to_BF ctx b in
    (m, [st_reg_tbl.(modrm_rm b); R.ST0])

  let dd_over_BF ctx b =
    let open Result.Let_syntax in
    let ops =
      if (b >= 0xC0 && b <= 0xCF) || (b >= 0xD0 && b <= 0xDF) then
        [st_reg_tbl.(modrm_rm b)]
      else if b < 0xE0 || b >= 0xE8 then []
      else [st_reg_tbl.(modrm_rm b); R.ST0]
    in
    let%map m = dd_op_outside_00_to_BF ctx b in
    (m, ops)

  let de_over_BF ctx b =
    let open Result.Let_syntax in
    let ops = if b = 0xD9 then [] else [st_reg_tbl.(modrm_rm b); R.ST0] in
    let%map m = de_op_outside_00_to_BF ctx b in
    (m, ops)

  let df_over_BF ctx b =
    let open Result.Let_syntax in
    let ops =
      if b = 0xE0 then [R.AX] else [R.ST0; st_reg_tbl.(modrm_rm b)]
    in
    let%map m = df_op_outside_00_to_BF ctx b in
    (m, ops)

  let d9_esc_effective_opr_size_by_modrm ctx = function
    | 1 -> Ok 80
    | 0 | 2 | 3 -> Ok 32
    | 4 | 6 -> if has_effective ctx 0x66 then Ok 112 else Ok 224
    | 5 | 7 -> Ok 16
    | _ -> Error `Invalid

  let db_esc_effective_opr_size_by_modrm ctx = function
    | 5 | 6 | 7 -> Ok 80
    | 0 | 1 | 2 | 3 -> Ok 32
    | _ -> Error `Invalid

  let dd_esc_effective_opr_size_by_modrm ctx = function
    | 0 | 1 | 2 | 3 -> Ok 64
    | 4 | 5 -> Ok 80
    | 6 -> if has_effective ctx 0x66 then Ok 752 else Ok 864
    | 7 -> Ok 16
    | _ -> Error `Invalid

  let df_esc_effective_opr_size_by_modrm ctx = function
    | 0 | 1 | 2 | 3 -> Ok 16
    | 4 | 6 -> Ok 80
    | 5 | 7 -> Ok 64
    | _ -> Error `Invalid

  let parse_esc_op ctx esc_flag op_in_f op_out_f =
    let open Result.Let_syntax in
    let%bind b = peek_o ctx 1 in
    if b <= 0xBF then (
      let%bind m = op_in_f ctx b in
      let%bind ins_size = new_ins_size ctx SzDef32 m D._Mz in
      let%bind eff_opr_size =
        match esc_flag with
        | 0xD8 -> Ok 32
        | 0xD9 -> d9_esc_effective_opr_size_by_modrm ctx (modrm_reg b)
        | 0xDA -> Ok 32
        | 0xDB -> db_esc_effective_opr_size_by_modrm ctx (modrm_reg b)
        | 0xDC -> Ok 64
        | 0xDD -> dd_esc_effective_opr_size_by_modrm ctx (modrm_reg b)
        | 0xDE -> Ok 16
        | 0xDF -> df_esc_effective_opr_size_by_modrm ctx (modrm_reg b)
        | _ -> Error `Invalid
      in
      let ins_size = {ins_size with mem_op_size= eff_opr_size} in
      ctx.effective_operand_size <- eff_opr_size;
      return (m, ins_size, D._Mz) )
    else
      let%bind m, reg_ops = op_out_f ctx b in
      (* returning this data structure isn't really necessary since
       * we've basically completed the instruction at this point,
       * so the fields in this record should be ignored
       * (i.e. just use X86_register.width_of for the operand sizes) *)
      let%bind ins_size = new_ins_size ctx SzDef32 m [||] in
      let%bind _ = skip_o ctx 2 in
      let%map _ = parse_modrm ctx b (ctx.pos - 1) in
      ctx.reg_ops <- reg_ops;
      (m, ins_size, [||])

  let grp1op = M.[|ADD; OR; ADC; SBB; AND; SUB; XOR; CMP|]

  let grp1aop = M.[|POP|]

  let grp2op = M.[|ROL; ROR; RCL; RCR; SHL; SHR; Invalid; SAR|]

  let grp4op = M.[|INC; DEC|]

  let grp5op = M.[|INC; DEC; CALL; CALLF; JMP; JMPF; PUSH|]

  let grp5desc = D.[|_Ev; _Ev; _Ev; _Mp; _Ev; _Mp; _Ev|]

  let grp5scnd = [|SzDef32; SzDef32; Sz64; SzDef32; Sz64; SzDef32; SzDef64|]

  let grp7op = M.[|SGDT; SIDT; LGDT; LIDT; SMSW; RSTORSSP; LMSW; INVLPG|]

  let grp7desc = D.[|_Ms; _Ms; _Ms; _Ms; _Mw; _Mq; _Ew; _Ew|]

  let grp8op = M.[|Invalid; Invalid; Invalid; Invalid; BT; BTS; BTR; BTC|]

  let grp16op = M.[|PREFETCHNTA; PREFETCHT0; PREFETCHT1; PREFETCHT2|]

  let op_and_opr_kind_by_op_grp3 ctx kind_flag reg_bits desc =
    match (reg_bits, desc) with
    | 0, [|k|] -> Ok (M.TEST, [|k; kind_flag|], SzDef32)
    | 2, _ -> Ok (M.NOT, desc, SzDef32)
    | 3, _ -> Ok (M.NEG, desc, SzDef32)
    | 4, _ -> Ok (M.MUL, desc, SzDef32)
    | 5, _ -> Ok (M.IMUL, desc, SzDef32)
    | 6, _ -> Ok (M.DIV, desc, SzDef32)
    | 7, _ -> Ok (M.IDIV, desc, SzDef32)
    | _ -> Error `Invalid

  let mod_is_memory b = modrm_mod b <> 3

  let op_and_opr_kind_by_op_grp6 ctx b reg_bits =
    match (mod_is_memory b, reg_bits) with
    | true, 0 -> Ok (M.SLDT, D._Mv, SzDef32)
    | false, 0 -> Ok (M.SLDT, D._Rv, SzDef32)
    | true, 1 -> Ok (M.STR, D._Mv, SzDef32)
    | false, 1 -> Ok (M.STR, D._Rv, SzDef32)
    | _, 2 -> Ok (M.LLDT, D._Ew, SzDef32)
    | _, 3 -> Ok (M.LTR, D._Ew, SzDef32)
    | _, 4 -> Ok (M.VERR, D._Ew, SzDef32)
    | _, 5 -> Ok (M.VERW, D._Ew, SzDef32)
    | _, 6 -> Ok (M.JMPE, [||], SzDef32)
    | _ -> Error `Invalid

  let parse_op_and_opr_kind_by_op_grp7 ctx b reg_bits =
    let open Result.Let_syntax in
    let not_memory = function
      | 0, 1 ->
          let%map _ = skip_o ctx 2 in
          (M.VMCALL, [||], SzDef32)
      | 0, 2 ->
          let%map _ = skip_o ctx 2 in
          (M.VMLAUNCH, [||], SzDef32)
      | 0, 3 ->
          let%map _ = skip_o ctx 2 in
          (M.VMRESUME, [||], SzDef32)
      | 0, 4 ->
          let%map _ = skip_o ctx 2 in
          (M.VMXOFF, [||], SzDef32)
      | 0, 5 ->
          let%map _ = skip_o ctx 2 in
          (M.PCONFIG, [||], SzDef32)
      | 1, 0 ->
          let%map _ = skip_o ctx 2 in
          (M.MONITOR, [||], SzDef32)
      | 1, 1 ->
          let%map _ = skip_o ctx 2 in
          (M.MWAIT, [||], SzDef32)
      | 1, 2 ->
          let%map _ = skip_o ctx 2 in
          (M.CLAC, [||], SzDef32)
      | 1, 3 ->
          let%map _ = skip_o ctx 2 in
          (M.STAC, [||], SzDef32)
      | 2, 0 ->
          let%map _ = skip_o ctx 2 in
          (M.XGETBV, [||], SzDef32)
      | 2, 1 ->
          let%map _ = skip_o ctx 2 in
          (M.XSETBV, [||], SzDef32)
      | 2, 4 ->
          let%map _ = skip_o ctx 2 in
          (M.VMFUNC, [||], SzDef32)
      | 2, 5 ->
          let%map _ = skip_o ctx 2 in
          (M.XEND, [||], SzDef32)
      | 2, 6 ->
          let%map _ = skip_o ctx 2 in
          (M.XTEST, [||], SzDef32)
      | 2, 7 ->
          let%map _ = skip_o ctx 2 in
          (M.ENCLU, [||], SzDef32)
      | 3, 0 ->
          let%bind _ = skip_o ctx 1 in
          let%map reg =
            set_effective_size ctx SzDef32;
            match ctx.effective_address_size with
            | 64 -> Ok R.RAX
            | 32 -> Ok R.EAX
            | 16 -> Ok R.AX
            | _ -> Error `BadReg
          in
          (M.VMRUN, D.[|Reg reg|], SzDef32)
      | 3, 1 ->
          let%map _ = skip_o ctx 2 in
          (M.VMMCALL, [||], SzDef32)
      | 3, 2 ->
          let%bind _ = skip_o ctx 1 in
          let%map reg =
            set_effective_size ctx SzDef32;
            match ctx.effective_address_size with
            | 64 -> Ok R.RAX
            | 32 -> Ok R.EAX
            | 16 -> Ok R.AX
            | _ -> Error `BadReg
          in
          (M.VMLOAD, D.[|Reg reg|], SzDef32)
      | 3, 3 ->
          let%map _ = skip_o ctx 2 in
          (M.VMSAVE, [||], SzDef32)
      | 3, 4 ->
          let%map _ = skip_o ctx 2 in
          (M.STGI, [||], SzDef32)
      | 3, 5 ->
          let%map _ = skip_o ctx 2 in
          (M.CLGI, [||], SzDef32)
      | 3, 6 ->
          let%map _ = skip_o ctx 1 in
          (M.SKINIT, D.[|Reg R.EAX|], SzDef32)
      | 3, 7 ->
          let%bind _ = skip_o ctx 1 in
          let%map reg =
            set_effective_size ctx SzDef32;
            match ctx.effective_address_size with
            | 64 -> Ok R.RAX
            | 32 -> Ok R.EAX
            | 16 -> Ok R.AX
            | _ -> Error `BadReg
          in
          (M.INVLPGA, D.[|Reg reg; Reg R.ECX|], SzDef32)
      | 4, _ -> Ok (M.SMSW, D._Rv, SzDef32)
      | 5, 6 ->
          let%map _ = skip_o ctx 2 in
          (M.RDPKRU, [||], SzDef32)
      | 5, 7 ->
          let%map _ = skip_o ctx 2 in
          (M.WRPKRU, [||], SzDef32)
      | 6, _ -> Ok (M.LMSW, D._Ew, SzDef32)
      | 7, 0 ->
          if Mode.(equal ctx.mode AMD64) then
            let%map _ = skip_o ctx 2 in
            (M.SWAPGS, [||], SzOnly64)
          else Error `Invalid
      | 7, 1 ->
          let%map _ = skip_o ctx 2 in
          (M.RDTSCP, [||], SzDef32)
      | _ -> Error `Invalid
    in
    if mod_is_memory b then
      Ok (grp7op.(reg_bits), grp7desc.(reg_bits), SzDef32)
    else not_memory (reg_bits, modrm_rm b)

  let op_and_opr_kind_by_op_grp9 ctx b reg_bits =
    let has_rex_w =
      match ctx.rex with
      | Some rex when rex.w <> 0 -> true
      | _ -> false
    in
    match
      ( mod_is_memory b
      , reg_bits
      , ctx.prefix.has_operand_size_override
      , ctx.prefix.group1 = 0xF3
      , has_rex_w )
    with
    | true, 1, false, false, false -> Ok (M.CMPXCHG8B, D._Mq, SzDef32)
    | true, 1, false, false, true -> Ok (M.CMPXCHG16B, D._Mdq, SzDef32)
    | true, 3, false, false, false -> Ok (M.XRSTORS, D._Mq, SzDef32)
    | true, 3, false, false, true -> Ok (M.XRSTORS64, D._Mq, SzDef32)
    | true, 4, false, false, false -> Ok (M.XSAVEC, D._Mq, SzDef32)
    | true, 4, false, false, true -> Ok (M.XSAVEC64, D._Mq, SzDef32)
    | true, 5, false, false, false -> Ok (M.VMPTRLD, D._Mq, SzDef32)
    | true, 5, false, false, true -> Ok (M.VMPTRST, D._Mq, SzDef32)
    | true, 6, false, false, _ -> Ok (M.VMPTRLD, D._Mq, SzDef32)
    | true, 7, false, false, _ -> Ok (M.VMPTRST, D._Mq, SzDef32)
    | true, 6, true, false, _ -> Ok (M.VMCLEAR, D._Mq, SzDef32)
    | true, 6, false, true, _ -> Ok (M.VMXON, D._Mq, SzDef32)
    | true, 7, false, true, _ -> Ok (M.VMPTRST, D._Mq, SzDef32)
    | false, 6, false, false, _ -> Ok (M.RDRAND, D._Rv, SzDef32)
    | false, 7, false, false, _ -> Ok (M.RDSEED, D._Rv, SzDef32)
    | false, 7, _, true, _ ->
        ignore_all_prefix ctx 0x66;
        Ok (M.RDPID, D._Rv, SzDef64)
    | _ -> Error `Invalid

  let parse_op_and_opr_kind_by_op_grp11 ctx op_flag k_flag b reg descs =
    match reg with
    | 0 -> Ok (M.MOV, descs, SzDef32)
    | 3 when mod_is_memory b -> Error `Invalid
    | 3 ->
        let open Result.Let_syntax in
        let%bind b2 = next ctx in
        if b2 <> 0xF8 then Error `Invalid
        else return (op_flag, k_flag, SzDef32)
    | _ -> Error `Invalid

  let grp_has_prefix ctx vex legacy =
    let open Result.Let_syntax in
    if Option.is_some ctx.avx then
      let%map pp = vex_pp ctx in
      pp = vex
    else (
      make_mandatory ctx;
      return (ctx.prefix.mandatory_candidate = legacy) )

  let op_and_opr_kind_by_op_grp12 ctx b reg_bits =
    let open Result.Let_syntax in
    let%bind opsz = grp_has_prefix ctx 1 0x66 in
    match (mod_is_memory b, reg_bits, opsz) with
    | false, 2, false -> Ok (M.PSRLW, D._NqIb, SzDef32)
    | false, 2, true ->
        if Option.is_none ctx.avx then Ok (M.PSRLW, D._UdqIb, SzDef32)
        else Ok (M.VPSRLW, D._HxUxIb, SzDef32)
    | false, 4, false -> Ok (M.PSRAW, D._NqIb, SzDef32)
    | false, 4, true ->
        if Option.is_none ctx.avx then Ok (M.PSRAW, D._UdqIb, SzDef32)
        else Ok (M.VPSRAW, D._HxUxIb, SzDef32)
    | false, 6, false -> Ok (M.PSLLW, D._NqIb, SzDef32)
    | false, 6, true ->
        if Option.is_none ctx.avx then Ok (M.PSLLW, D._UdqIb, SzDef32)
        else Ok (M.VPSLLW, D._HxUxIb, SzDef32)
    | _ -> Error `Invalid

  let op_and_opr_kind_by_op_grp13 ctx b reg_bits =
    let open Result.Let_syntax in
    let%bind opsz = grp_has_prefix ctx 1 0x66 in
    match (mod_is_memory b, reg_bits, opsz) with
    | false, 2, false -> Ok (M.PSRLD, D._NqIb, SzDef32)
    | false, 2, true ->
        if Option.is_none ctx.avx then Ok (M.PSRLD, D._UdqIb, SzDef32)
        else Ok (M.VPSRLD, D._HxUxIb, SzDef32)
    | false, 4, false -> Ok (M.PSRAD, D._NqIb, SzDef32)
    | false, 4, true ->
        if Option.is_none ctx.avx then Ok (M.PSRAD, D._UdqIb, SzDef32)
        else Ok (M.VPSRAD, D._HxUxIb, SzDef32)
    | false, 6, false -> Ok (M.PSLLD, D._NqIb, SzDef32)
    | false, 6, true ->
        if Option.is_none ctx.avx then Ok (M.PSLLD, D._UdqIb, SzDef32)
        else Ok (M.VPSLLD, D._HxUxIb, SzDef32)
    | _ -> Error `Invalid

  let op_and_opr_kind_by_op_grp14 ctx b reg_bits =
    let open Result.Let_syntax in
    let%bind opsz = grp_has_prefix ctx 1 0x66 in
    match (mod_is_memory b, reg_bits, opsz) with
    | false, 2, false -> Ok (M.PSRLQ, D._NqIb, SzDef32)
    | false, 2, true ->
        if Option.is_none ctx.avx then Ok (M.PSRLQ, D._UdqIb, SzDef32)
        else Ok (M.VPSRLQ, D._HxUxIb, SzDef32)
    | false, 3, true ->
        if Option.is_none ctx.avx then Ok (M.PSRLDQ, D._UdqIb, SzDef32)
        else Ok (M.VPSRLDQ, D._HxUxIb, SzDef32)
    | false, 6, false -> Ok (M.PSLLQ, D._NqIb, SzDef32)
    | false, 6, true ->
        if Option.is_none ctx.avx then Ok (M.PSLLQ, D._UdqIb, SzDef32)
        else Ok (M.VPSLLQ, D._HxUxIb, SzDef32)
    | false, 7, true ->
        if Option.is_none ctx.avx then Ok (M.PSLLDQ, D._UdqIb, SzDef32)
        else Ok (M.VPSLLDQ, D._HxUxIb, SzDef32)
    | _ -> Error `Invalid

  let parse_op_and_opr_kind_by_op_grp15 ctx b reg_bits =
    let open Result.Let_syntax in
    let%bind repz = grp_has_prefix ctx 2 0xF3 in
    let has_rex_w =
      match ctx.rex with
      | Some rex when rex.w <> 0 -> true
      | _ -> false
    in
    match (mod_is_memory b, reg_bits, repz) with
    | true, 0, false ->
        let m = if has_rex_w then M.FXSAVE64 else M.FXSAVE in
        Ok (m, D._Ev, SzDef32)
    | true, 1, false ->
        let m = if has_rex_w then M.FXRSTOR64 else M.FXRSTOR in
        Ok (m, D._Ev, SzDef32)
    | true, 2, false -> Ok (M.LDMXCSR, D._Ev, SzDef32)
    | true, 3, false -> Ok (M.STMXCSR, D._Ev, SzDef32)
    | true, 4, false -> Ok (M.XSAVE, D._Ev, SzDef32)
    | true, 5, false -> Ok (M.XRSTOR, D._Ev, SzDef32)
    | true, 6, false ->
        let%bind osz = grp_has_prefix ctx 1 0x66 in
        if not osz then Ok (M.XSAVEOPT, D._Ev, SzDef32)
        else Ok (M.CLWB, D._Mqqqa, SzDef32)
    | true, 6, true -> Ok (M.CLRSSBSY, D._Mq, SzDef32)
    | true, 7, false -> Ok (M.CLFLUSH, D._Ev, SzDef32)
    | false, 5, false ->
        let%map _ = skip_o ctx 2 in
        (M.LFENCE, [||], SzDef32)
    | false, 6, false ->
        let%map _ = skip_o ctx 2 in
        (M.MFENCE, [||], SzDef32)
    | false, 7, false ->
        let%map _ = skip_o ctx 2 in
        (M.SFENCE, [||], SzDef32)
    | false, 0, true -> Ok (M.RDFSBASE, D._Ry, SzDef32)
    | false, 1, true -> Ok (M.RDGSBASE, D._Ry, SzDef32)
    | false, 2, true -> Ok (M.WRFSBASE, D._Ry, SzDef32)
    | false, 3, true -> Ok (M.WRGSBASE, D._Ry, SzDef32)
    | _ -> Error `Invalid

  let parse_op_grp17 ctx reg_bits =
    match reg_bits with
    | 1 -> Ok M.BLSR
    | 2 -> Ok M.BLSMSK
    | 3 -> Ok M.BLSI
    | _ -> Error `Invalid

  let parse_op_and_opr_kind_by_grp ctx b descs grp =
    let open Result.Let_syntax in
    let r = modrm_reg b in
    match grp with
    | `G1 -> Ok (grp1op.(r), descs, SzDef32)
    | `G1Inv64 ->
        if not Mode.(equal ctx.mode IA32) then Error `Invalid
        else Ok (grp1op.(r), descs, SzDef64)
    | `G1A when r < 0 || r >= Array.length grp1aop -> Error `Invalid
    | `G1A -> Ok (grp1aop.(r), descs, SzDef64)
    | `G2 when r = 6 || r >= Array.length grp2op -> Error `Invalid
    | `G2 -> Ok (grp2op.(r), descs, SzDef32)
    | `G3A -> op_and_opr_kind_by_op_grp3 ctx D.__SIb r descs
    | `G3B -> op_and_opr_kind_by_op_grp3 ctx D.__SIz r descs
    | `G4 when r < 0 || r >= Array.length grp4op -> Error `Invalid
    | `G4 -> Ok (grp4op.(r), D._Eb, SzDef32)
    | `G5
      when r < 0
           || r >= Array.length grp5op
           || r >= Array.length grp5desc
           || r >= Array.length grp5scnd -> Error `Invalid
    | `G5 -> Ok (grp5op.(r), grp5desc.(r), grp5scnd.(r))
    | `G6 -> op_and_opr_kind_by_op_grp6 ctx b r
    | `G7 -> parse_op_and_opr_kind_by_op_grp7 ctx b r
    | `G8 when r < 0 || r >= Array.length grp8op -> Error `Invalid
    | `G8 -> Ok (grp8op.(r), descs, SzDef32)
    | `G9 -> op_and_opr_kind_by_op_grp9 ctx b r
    | `G10 -> Ok (M.UD1, D._GdEv, SzDef32)
    | `G11A -> parse_op_and_opr_kind_by_op_grp11 ctx M.XABORT D._Ib b r descs
    | `G11B -> parse_op_and_opr_kind_by_op_grp11 ctx M.XBEGIN D._Jz b r descs
    | `G12 -> op_and_opr_kind_by_op_grp12 ctx b r
    | `G13 -> op_and_opr_kind_by_op_grp13 ctx b r
    | `G14 -> op_and_opr_kind_by_op_grp14 ctx b r
    | `G15 -> parse_op_and_opr_kind_by_op_grp15 ctx b r
    | `G16 when r < 0 || r >= Array.length grp16op -> Error `Invalid
    | `G16 -> Ok (grp16op.(r), descs, SzDef32)
    | `G17 -> (
      match ctx.avx with
      | Some (A.Vex _) ->
          let%map m = parse_op_grp17 ctx r in
          (m, descs, SzDef32)
      | _ -> Error `Invalid )

  let parse_grp_opcode ctx grp descs =
    let open Result.Let_syntax in
    let%bind b = peek_o ctx 1 in
    let%bind m, descs, sc = parse_op_and_opr_kind_by_grp ctx b descs grp in
    parse_op ctx m sc descs

  let pick_reg ctx sz (grp : X86_register.t array) =
    match sz with
    | 512 -> Ok grp.(6)
    | 256 -> Ok grp.(5)
    | 128 -> Ok grp.(4)
    | 64 -> Ok grp.(3)
    | 32 -> Ok grp.(2)
    | 16 -> Ok grp.(1)
    | 8 -> Ok grp.(0)
    | _ -> Error `Invalid

  let select_rgrp ctx attr g1 g2 rex =
    match rex with
    | 0, 0, 0, 1 | 1, 0, 0, 1 -> (
      match attr with
      | D.BaseRM | D.OpREX | D.Mod11 | D.SIBBase -> Ok g1
      | _ -> Ok g2 )
    | 0, 0, 1, 0 | 1, 0, 1, 0 -> (
      match attr with
      | D.SIBIdx -> Ok g1
      | _ -> Ok g2 )
    | 0, 0, 1, 1 | 1, 0, 1, 1 -> (
      match attr with
      | D.OpREX | D.BaseRM | D.Mod11 | D.SIBIdx | D.SIBBase -> Ok g1
      | _ -> Ok g2 )
    | 0, 1, 0, 0 | 1, 1, 0, 0 -> (
      match attr with
      | D.Bits -> Ok g1
      | _ -> Ok g2 )
    | 0, 1, 0, 1 | 1, 1, 0, 1 -> (
      match attr with
      | D.OpREX | D.Mod11 | D.Bits | D.SIBBase -> Ok g1
      | _ -> Ok g2 )
    | 0, 1, 1, 0 | 1, 1, 1, 0 -> (
      match attr with
      | D.Bits | D.SIBIdx -> Ok g1
      | _ -> Ok g2 )
    | 0, 1, 1, 1 | 1, 1, 1, 1 -> (
      match attr with
      | D.OpREX | D.BaseRM | D.Mod11 | D.Bits | D.SIBBase | D.SIBIdx -> Ok g1
      | _ -> Ok g2 )
    | 0, 0, 0, 0 | 1, 0, 0, 0 -> Ok g2
    | _ -> Error `Invalid

  let grp_EAX = R.[|AL; AX; EAX; RAX; XMM0; YMM0; ZMM0|]

  let grp_ECX = R.[|CL; CX; ECX; RCX; XMM1; YMM1; ZMM1|]

  let grp_EDX = R.[|DL; DX; EDX; RDX; XMM2; YMM2; ZMM2|]

  let grp_EBX = R.[|BL; BX; EBX; RBX; XMM3; YMM3; ZMM3|]

  let grp_AH = R.[|AH; SP; ESP; RSP; XMM4; YMM4; ZMM4|]

  let grp_CH = R.[|CH; BP; EBP; RBP; XMM5; YMM5; ZMM5|]

  let grp_DH = R.[|DH; SI; ESI; RSI; XMM6; YMM6; ZMM6|]

  let grp_BH = R.[|BH; DI; EDI; RDI; XMM7; YMM7; ZMM7|]

  let grp_ESP = R.[|SPL; SP; ESP; RSP; XMM4; YMM4; ZMM4|]

  let grp_EBP = R.[|BPL; BP; EBP; RBP; XMM5; YMM5; ZMM5|]

  let grp_ESI = R.[|SIL; SI; ESI; RSI; XMM6; YMM6; ZMM6|]

  let grp_EDI = R.[|DIL; DI; EDI; RDI; XMM7; YMM7; ZMM7|]

  let grp_R8 = R.[|R8B; R8W; R8D; R8; XMM8; YMM8; ZMM8|]

  let grp_R9 = R.[|R9B; R9W; R9D; R9; XMM9; YMM9; ZMM9|]

  let grp_R10 = R.[|R10B; R10W; R10D; R10; XMM10; YMM10; ZMM10|]

  let grp_R11 = R.[|R11B; R11W; R11D; R11; XMM11; YMM11; ZMM11|]

  let grp_R12 = R.[|R12B; R12W; R12D; R12; XMM12; YMM12; ZMM12|]

  let grp_R13 = R.[|R13B; R13W; R13D; R13; XMM13; YMM13; ZMM13|]

  let grp_R14 = R.[|R14B; R14W; R14D; R14; XMM14; YMM14; ZMM14|]

  let grp_R15 = R.[|R15B; R15W; R15D; R15; XMM15; YMM15; ZMM15|]

  let tbl_grp_NOREX =
    [|grp_EAX; grp_ECX; grp_EDX; grp_EBX; grp_AH; grp_CH; grp_DH; grp_BH|]

  let tbl_grp1 =
    [|grp_R8; grp_R9; grp_R10; grp_R11; grp_R12; grp_R13; grp_R14; grp_R15|]

  let tbl_grp2 =
    [|grp_EAX; grp_ECX; grp_EDX; grp_EBX; grp_ESP; grp_EBP; grp_ESI; grp_EDI|]

  let xmm_ext1 = R.[|XMM16; XMM17; XMM18; XMM19; XMM20; XMM21; XMM22; XMM23|]

  let xmm_ext2 = R.[|XMM24; XMM25; XMM26; XMM27; XMM28; XMM29; XMM30; XMM31|]

  let ymm_ext1 = R.[|YMM16; YMM17; YMM18; YMM19; YMM20; YMM21; YMM22; YMM23|]

  let ymm_ext2 = R.[|YMM24; YMM25; YMM26; YMM27; YMM28; YMM29; YMM30; YMM31|]

  let zmm_ext1 = R.[|ZMM16; ZMM17; ZMM18; ZMM19; ZMM20; ZMM21; ZMM22; ZMM23|]

  let zmm_ext2 = R.[|ZMM24; ZMM25; ZMM26; ZMM27; ZMM28; ZMM29; ZMM30; ZMM31|]

  let find_reg ctx sz attr grp rex =
    let open Result.Let_syntax in
    match rex with
    | Some rex -> (
        let%bind rgrp =
          select_rgrp ctx attr tbl_grp1.(grp) tbl_grp2.(grp) rex
        in
        let%bind reg = pick_reg ctx sz rgrp in
        let handle_enhanced r' =
          let _, r, x, b = rex in
          match attr with
          | D.Mod11 ->
              if x <> 0 then
                if b = 0 then
                  match sz with
                  | 128 -> Ok xmm_ext1.(grp)
                  | 256 -> Ok ymm_ext1.(grp)
                  | 512 -> Ok zmm_ext1.(grp)
                  | _ -> Error `Invalid
                else
                  match sz with
                  | 128 -> Ok xmm_ext2.(grp)
                  | 256 -> Ok ymm_ext2.(grp)
                  | 512 -> Ok zmm_ext2.(grp)
                  | _ -> Error `Invalid
              else Ok reg
          | D.Bits when sz = 512 ->
              if lnot r' land 0x01 = 0 then Ok reg
              else if r <> 0 then Ok zmm_ext2.(grp)
              else Ok zmm_ext1.(grp)
          | _ -> Ok reg
        in
        match ctx.avx with
        | Some (A.Evex evex) -> handle_enhanced evex.r'
        | Some (A.Mvex mvex) -> handle_enhanced mvex.r'
        | _ -> Ok reg )
    | None -> pick_reg ctx sz tbl_grp_NOREX.(grp)

  let desc_for_reg_grp ctx grp =
    let open Result.Let_syntax in
    let rex =
      match ctx.rex with
      | Some rex -> Some (rex.w, rex.r, rex.x, rex.b)
      | None -> None
    in
    let%map r = find_reg ctx 8 D.Mod11 grp rex in
    D._RegIb r

  let select_op_info ctx =
    match ctx.prefix.mandatory_candidate with
    | 0x66 -> 1
    | 0xF3 -> 2
    | 0xF2 -> 3
    | _ -> 0

  let parse_vex_op ctx sc op_nor op_vex ds_nor ds_vex =
    let open Result.Let_syntax in
    let%bind m, descs =
      if Option.is_some ctx.avx then
        let%bind idx = vex_pp ctx in
        Ok (op_vex.(idx), ds_vex.(idx))
      else (
        make_mandatory ctx;
        let idx = select_op_info ctx in
        Ok (op_nor.(idx), ds_nor.(idx)) )
    in
    parse_op ctx m sc descs

  let select_op_info_by_mem ctx op_nor_mem op_nor_reg op_vex_mem op_vex_reg
      ds_nor_mem ds_nor_reg ds_vex_mem ds_vex_reg =
    let open Result.Let_syntax in
    let%map b = peek_o ctx 1 in
    if mod_is_memory b then (op_nor_mem, op_vex_mem, ds_nor_mem, ds_vex_mem)
    else (op_nor_reg, op_vex_reg, ds_nor_reg, ds_vex_reg)

  let select_op_info_by_rex ctx op_nor_64 op_nor_32 op_vex_64 op_vex_32
      ds_nor_64 ds_nor_32 ds_vex_64 ds_vex_32 =
    match select_rex ctx with
    | Some (w, _, _, _) when w <> 0 ->
        (op_nor_64, op_vex_64, ds_nor_64, ds_vex_64)
    | _ -> (op_nor_32, op_vex_32, ds_nor_32, ds_vex_32)

  let select_op_info_by_rex_evex ctx op_64 op_32 ds_64 ds_32 =
    match select_rex ctx with
    | Some (w, _, _, _) when w <> 0 -> (op_64, ds_64)
    | _ -> (op_32, ds_32)

  let opcode_0F0D ctx =
    let open Result.Let_syntax in
    let%bind b = peek_o ctx 1 in
    match (mod_is_memory b, modrm_reg b) with
    | true, 1 -> return (M.PREFETCHW, D._Ev)
    | true, 2 -> return (M.PREFETCHWT1, D._Ev)
    | false, _ -> return (M.NOP, D._EvGv)
    | _ -> Error `Invalid

  let parse_evex_op ctx sc op_nor op_vex op_evex ds_nor ds_vex ds_evex =
    let op_vex, ds_vex =
      match ctx.avx with
      | Some (A.Evex _) -> (op_evex, ds_evex)
      | Some (A.Vex _) -> (op_vex, ds_vex)
      | _ -> (D.op_empty, D.ds_empty)
    in
    parse_vex_op ctx sc op_nor op_vex ds_nor ds_vex

  let parse_evex_by_rex ctx op_nor op_vex op_64 op_32 ds_nor ds_vex ds_64
      ds_32 =
    let op_evex, ds_evex =
      select_op_info_by_rex_evex ctx op_64 op_32 ds_64 ds_32
    in
    parse_evex_op ctx SzDef32 op_nor op_vex op_evex ds_nor ds_vex ds_evex

  let parse_vex_by_mem ctx op_nor_mem op_nor_reg op_vex_mem op_vex_reg
      ds_nor_mem ds_nor_reg ds_vex_mem ds_vex_reg =
    let open Result.Let_syntax in
    let%bind op_nor, op_vex, ds_nor, ds_vex =
      select_op_info_by_mem ctx op_nor_mem op_nor_reg op_vex_mem op_vex_reg
        ds_nor_mem ds_nor_reg ds_vex_mem ds_vex_reg
    in
    parse_vex_op ctx SzDef32 op_nor op_vex ds_nor ds_vex

  let parse_vex_by_rex ctx op_nor_64 op_nor_32 op_vex_64 op_vex_32 ds_nor_64
      ds_nor_32 ds_vex_64 ds_vex_32 =
    let op_nor, op_vex, ds_nor, ds_vex =
      select_op_info_by_rex ctx op_nor_64 op_nor_32 op_vex_64 op_vex_32
        ds_nor_64 ds_nor_32 ds_vex_64 ds_vex_32
    in
    parse_vex_op ctx SzDef32 op_nor op_vex ds_nor ds_vex

  let parse_vex_and_evex_by_rex ctx op_nor_64 op_nor_32 op_vex_64 op_vex_32
      op_evex_64 op_evex_32 ds_nor_64 ds_nor_32 ds_vex_64 ds_vex_32
      ds_evex_64 ds_evex_32 =
    let op_evex, ds_evex =
      select_op_info_by_rex_evex ctx op_evex_64 op_evex_32 ds_evex_64
        ds_evex_32
    in
    let op_nor, op_vex, ds_nor, ds_vex =
      select_op_info_by_rex ctx op_nor_64 op_nor_32 op_vex_64 op_vex_32
        ds_nor_64 ds_nor_32 ds_vex_64 ds_vex_32
    in
    parse_evex_op ctx SzDef32 op_nor op_vex op_evex ds_nor ds_vex ds_evex

  let parse_bnd_op ctx sc op ds =
    ignore_all_prefix ctx 0x67;
    let idx = select_op_info ctx in
    make_mandatory ctx;
    parse_op ctx op.(idx) sc ds.(idx)

  let parse_non_vex_op ctx sc op ds =
    let m, descs =
      if ctx.prefix.has_operand_size_override && ctx.prefix.group1 = 0xF2
      then (op.(4), ds.(4))
      else
        let idx = select_op_info ctx in
        (op.(idx), ds.(idx))
    in
    parse_op ctx m sc descs

  (* 0F 38 *)
  let parse_three_byte_op1 ctx =
    let open Result.Let_syntax in
    let pos = ctx.pos in
    let%bind b = peek ctx in
    let%bind op =
      match b with
      | 0x00 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3800 D.op_vex_0F3800
            D.ds_nor_0F3800 D.ds_vex_0F3800
      | 0x01 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3801 D.op_vex_0F3801
            D.op_empty D.ds_nor_0F3801 D.ds_vex_0F3801 D.ds_empty
      | 0x02 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3802 D.op_vex_0F3802
            D.op_empty D.ds_nor_0F3802 D.ds_vex_0F3802 D.ds_empty
      | 0x03 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3803 D.op_vex_0F3803
            D.op_empty D.ds_nor_0F3803 D.ds_vex_0F3803 D.ds_empty
      | 0x04 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3804 D.op_vex_0F3804
            D.ds_nor_0F3804 D.ds_vex_0F3804
      | 0x05 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3805 D.op_vex_0F3805
            D.op_empty D.ds_nor_0F3805 D.ds_vex_0F3805 D.ds_empty
      | 0x06 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3806 D.op_vex_0F3806
            D.op_empty D.ds_nor_0F3806 D.ds_vex_0F3806 D.ds_empty
      | 0x07 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3807 D.op_vex_0F3807
            D.op_empty D.ds_nor_0F3807 D.ds_vex_0F3807 D.ds_empty
      | 0x08 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3808 D.op_vex_0F3808
            D.op_empty D.ds_nor_0F3808 D.ds_vex_0F3808 D.ds_empty
      | 0x09 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3809 D.op_vex_0F3809
            D.op_empty D.ds_nor_0F3809 D.ds_vex_0F3809 D.ds_empty
      | 0x0A ->
          parse_evex_op ctx SzDef32 D.op_nor_0F380A D.op_vex_0F380A
            D.op_empty D.ds_nor_0F380A D.ds_vex_0F380A D.ds_empty
      | 0x0B ->
          parse_vex_op ctx SzDef32 D.op_nor_0F380B D.op_vex_0F380B
            D.ds_nor_0F380B D.ds_vex_0F380B
      | 0x0C ->
          parse_evex_by_rex ctx D.op_nor_0F380C D.op_vex_0F380C D.op_empty
            D.op_evex_0F380C D.ds_nor_0F380C D.ds_vex_0F380C D.ds_empty
            D.ds_evex_0F380C
      | 0x0D ->
          parse_evex_by_rex ctx D.op_nor_0F380D D.op_vex_0F380D
            D.op_evex_0F380D D.op_empty D.ds_nor_0F380D D.ds_vex_0F380D
            D.ds_evex_0F380D D.ds_empty
      | 0x0E ->
          parse_evex_op ctx SzDef32 D.op_nor_0F380E D.op_vex_0F380E
            D.op_empty D.ds_nor_0F380E D.ds_vex_0F380E D.ds_empty
      | 0x0F ->
          parse_evex_op ctx SzDef32 D.op_nor_0F380F D.op_vex_0F380F
            D.op_empty D.ds_nor_0F380F D.ds_vex_0F380F D.ds_empty
      | 0x10 ->
          parse_evex_by_rex ctx D.op_nor_0F3810 D.op_vex_0F3810
            D.op_evex_0F3810B64 D.op_evex_0F3810B32 D.ds_nor_0F3810
            D.ds_vex_0F3810 D.ds_evex_0F3810B64 D.ds_evex_0F3810B32
      | 0x11 ->
          parse_evex_by_rex ctx D.op_nor_0F3811 D.op_vex_0F3811
            D.op_evex_0F3811B64 D.op_evex_0F3811B32 D.ds_nor_0F3811
            D.ds_vex_0F3811 D.ds_evex_0F3811B64 D.ds_evex_0F3811B32
      | 0x12 ->
          parse_evex_by_rex ctx D.op_nor_0F3812 D.op_vex_0F3812
            D.op_evex_0F3812B64 D.op_evex_0F3812B32 D.ds_nor_0F3812
            D.ds_vex_0F3812 D.ds_evex_0F3812B64 D.ds_evex_0F3812B32
      | 0x13 ->
          parse_evex_by_rex ctx D.op_nor_0F3813 D.op_vex_0F3813
            D.op_evex_0F3813B64 D.op_evex_0F3813B32 D.ds_nor_0F3813
            D.ds_vex_0F3813 D.ds_evex_0F3813B64 D.ds_evex_0F3813B32
      | 0x14 ->
          parse_evex_by_rex ctx D.op_nor_0F3814 D.op_vex_0F3814
            D.op_evex_0F3814B64 D.op_evex_0F3814B32 D.ds_nor_0F3814
            D.ds_vex_0F3814 D.ds_evex_0F3814B64 D.ds_evex_0F3814B32
      | 0x15 ->
          parse_evex_by_rex ctx D.op_nor_0F3815 D.op_vex_0F3815
            D.op_evex_0F3815B64 D.op_evex_0F3815B32 D.ds_nor_0F3815
            D.ds_vex_0F3815 D.ds_evex_0F3815B64 D.ds_evex_0F3815B32
      | 0x16 ->
          parse_evex_by_rex ctx D.op_nor_0F3816 D.op_vex_0F3816
            D.op_evex_0F3816B64 D.op_evex_0F3816B32 D.ds_nor_0F3816
            D.ds_vex_0F3816 D.ds_evex_0F3816B64 D.ds_evex_0F3816B32
      | 0x17 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3817 D.op_vex_0F3817
            D.op_empty D.ds_nor_0F3817 D.ds_vex_0F3817 D.ds_empty
      | 0x18 ->
          (* let%bind _ = skip ctx in *)
          parse_evex_by_rex ctx D.op_nor_0F3818 D.op_vex_0F3818 D.op_empty
            D.op_evex_0F3818 D.ds_nor_0F3818 D.ds_vex_0F3818 D.ds_empty
            D.ds_evex_0F3818
      | 0x19 ->
          parse_evex_by_rex ctx D.op_nor_0F3819 D.op_vex_0F3819
            D.op_evex_0F3819B64 D.op_evex_0F3819B32 D.ds_nor_0F3819
            D.ds_vex_0F3819 D.ds_evex_0F3819B64 D.ds_evex_0F3819B32
      | 0x1A ->
          parse_evex_by_rex ctx D.op_nor_0F381A D.op_vex_0F381A
            D.op_evex_0F381AB64 D.op_evex_0F381AB32 D.ds_nor_0F381A
            D.ds_vex_0F381A D.ds_evex_0F381AB64 D.ds_evex_0F381AB32
      | 0x1B ->
          parse_evex_by_rex ctx D.op_nor_0F381B D.op_vex_0F381B
            D.op_evex_0F381BB64 D.op_evex_0F381BB32 D.ds_nor_0F381B
            D.ds_vex_0F381B D.ds_evex_0F381BB64 D.ds_evex_0F381BB32
      | 0x1C ->
          parse_vex_op ctx SzDef32 D.op_nor_0F381C D.op_vex_0F381C
            D.ds_nor_0F381C D.ds_vex_0F381C
      | 0x1D ->
          parse_vex_op ctx SzDef32 D.op_nor_0F381D D.op_vex_0F381D
            D.ds_nor_0F381D D.ds_vex_0F381D
      | 0x1E ->
          parse_vex_op ctx SzDef32 D.op_nor_0F381E D.op_vex_0F381E
            D.ds_nor_0F381E D.ds_vex_0F381E
      | 0x1F ->
          parse_evex_by_rex ctx D.op_nor_0F381F D.op_vex_0F381F
            D.op_evex_0F381FB64 D.op_evex_0F381FB32 D.ds_nor_0F381F
            D.ds_vex_0F381F D.ds_evex_0F381FB64 D.ds_evex_0F381FB32
      | 0x20 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3820 D.op_vex_0F3820
            D.ds_nor_0F3820 D.ds_vex_0F3820
      | 0x21 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3821 D.op_vex_0F3821
            D.ds_nor_0F3821 D.ds_vex_0F3821
      | 0x22 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3822 D.op_vex_0F3822
            D.ds_nor_0F3822 D.ds_vex_0F3822
      | 0x23 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3823 D.op_vex_0F3823
            D.ds_nor_0F3823 D.ds_vex_0F3823
      | 0x24 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3824 D.op_vex_0F3824
            D.ds_nor_0F3824 D.ds_vex_0F3824
      | 0x25 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3825 D.op_vex_0F3825
            D.ds_nor_0F3825 D.ds_vex_0F3825
      | 0x26 ->
          parse_evex_by_rex ctx D.op_nor_0F3826 D.op_vex_0F3826
            D.op_evex_0F3826B64 D.op_evex_0F3826B32 D.ds_nor_0F3826
            D.ds_vex_0F3826 D.ds_evex_0F3826B64 D.ds_evex_0F3826B32
      | 0x27 ->
          parse_evex_by_rex ctx D.op_nor_0F3827 D.op_vex_0F3827
            D.op_evex_0F3827B64 D.op_evex_0F3827B32 D.ds_nor_0F3827
            D.ds_vex_0F3827 D.ds_evex_0F3827B64 D.ds_evex_0F3827B32
      | 0x28 ->
          parse_evex_by_rex ctx D.op_nor_0F3828 D.op_vex_0F3828
            D.op_evex_0F3828B64 D.op_evex_0F3828B32 D.ds_nor_0F3828
            D.ds_vex_0F3828 D.ds_evex_0F3828B64 D.ds_evex_0F3828B32
      | 0x29 ->
          parse_evex_by_rex ctx D.op_nor_0F3829 D.op_vex_0F3829
            D.op_evex_0F3829B64 D.op_evex_0F3829B32 D.ds_nor_0F3829
            D.ds_vex_0F3829 D.ds_evex_0F3829B64 D.ds_evex_0F3829B32
      | 0x2A ->
          parse_evex_by_rex ctx D.op_nor_0F382A D.op_vex_0F382A
            D.op_evex_0F382AB64 D.op_evex_0F382AB32 D.ds_nor_0F382A
            D.ds_vex_0F382A D.ds_evex_0F382AB64 D.ds_evex_0F382AB32
      | 0x2B ->
          parse_evex_by_rex ctx D.op_nor_0F382B D.op_vex_0F382B
            D.op_evex_0F382BB64 D.op_evex_0F382BB32 D.ds_nor_0F382B
            D.ds_vex_0F382B D.ds_evex_0F382BB64 D.ds_evex_0F382BB32
      | 0x2C ->
          parse_evex_by_rex ctx D.op_nor_0F382C D.op_vex_0F382C
            D.op_evex_0F382CB64 D.op_evex_0F382CB32 D.ds_nor_0F382C
            D.ds_vex_0F382C D.ds_evex_0F382CB64 D.ds_evex_0F382CB32
      | 0x2D ->
          parse_evex_by_rex ctx D.op_nor_0F382D D.op_vex_0F382D
            D.op_evex_0F382DB64 D.op_evex_0F382DB32 D.ds_nor_0F382D
            D.ds_vex_0F382D D.ds_evex_0F382DB64 D.ds_evex_0F382DB32
      | 0x2E ->
          parse_evex_op ctx SzDef32 D.op_nor_0F382E D.op_vex_0F382E
            D.op_empty D.ds_nor_0F382E D.ds_vex_0F382E D.ds_empty
      | 0x2F ->
          parse_evex_op ctx SzDef32 D.op_nor_0F382F D.op_vex_0F382F
            D.op_empty D.ds_nor_0F382F D.ds_vex_0F382F D.ds_empty
      | 0x30 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3830 D.op_vex_0F3830
            D.ds_nor_0F3830 D.ds_vex_0F3830
      | 0x31 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3831 D.op_vex_0F3831
            D.ds_nor_0F3831 D.ds_vex_0F3831
      | 0x32 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3832 D.op_vex_0F3832
            D.ds_nor_0F3832 D.ds_vex_0F3832
      | 0x33 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3833 D.op_vex_0F3833
            D.ds_nor_0F3833 D.ds_vex_0F3833
      | 0x34 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3834 D.op_vex_0F3834
            D.ds_nor_0F3834 D.ds_vex_0F3834
      | 0x35 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3835 D.op_vex_0F3835
            D.ds_nor_0F3835 D.ds_vex_0F3835
      | 0x36 ->
          parse_evex_by_rex ctx D.op_nor_0F3836 D.op_vex_0F3836
            D.op_evex_0F3836B64 D.op_evex_0F3836B32 D.ds_nor_0F3836
            D.ds_vex_0F3836 D.ds_evex_0F3836B64 D.ds_evex_0F3836B32
      | 0x37 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3837 D.op_vex_0F3837
            D.ds_nor_0F3837 D.ds_vex_0F3837
      | 0x38 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3838 D.op_vex_0F3838
            D.ds_nor_0F3838 D.ds_vex_0F3838
      | 0x39 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3839 D.op_vex_0F3839
            D.ds_nor_0F3839 D.ds_vex_0F3839
      | 0x3A ->
          parse_evex_by_rex ctx D.op_nor_0F383A D.op_vex_0F383A
            D.op_evex_0F383AB64 D.op_evex_0F383AB32 D.ds_nor_0F383A
            D.ds_vex_0F383A D.ds_evex_0F383AB64 D.ds_evex_0F383AB32
      | 0x3B ->
          parse_evex_by_rex ctx D.op_nor_0F383B D.op_vex_0F383B
            D.op_evex_0F383BB64 D.op_evex_0F383BB32 D.ds_nor_0F383B
            D.ds_vex_0F383B D.ds_evex_0F383BB64 D.ds_evex_0F383BB32
      | 0x3C ->
          parse_vex_op ctx SzDef32 D.op_nor_0F383C D.op_vex_0F383C
            D.ds_nor_0F383C D.ds_vex_0F383C
      | 0x3D ->
          parse_evex_by_rex ctx D.op_nor_0F383D D.op_vex_0F383D
            D.op_evex_0F383DB64 D.op_evex_0F383DB32 D.ds_nor_0F383D
            D.ds_vex_0F383D D.ds_evex_0F383DB64 D.ds_evex_0F383DB32
      | 0x3E ->
          parse_vex_op ctx SzDef32 D.op_nor_0F383E D.op_vex_0F383E
            D.ds_nor_0F383E D.ds_vex_0F383E
      | 0x3F ->
          parse_evex_by_rex ctx D.op_nor_0F383F D.op_vex_0F383F
            D.op_evex_0F383FB64 D.op_evex_0F383FB32 D.ds_nor_0F383F
            D.ds_vex_0F383F D.ds_evex_0F383FB64 D.ds_evex_0F383FB32
      | 0x40 ->
          parse_evex_by_rex ctx D.op_nor_0F3840 D.op_vex_0F3840
            D.op_evex_0F3840B64 D.op_evex_0F3840B32 D.ds_nor_0F3840
            D.ds_vex_0F3840 D.ds_evex_0F3840B64 D.ds_evex_0F3840B32
      | 0x41 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3841 D.op_vex_0F3841
            D.op_empty D.ds_nor_0F3841 D.ds_vex_0F3841 D.ds_empty
      | 0x46 ->
          parse_vex_and_evex_by_rex ctx D.op_nor_0F3846 D.op_nor_0F3846
            D.op_vex_0F3846B64 D.op_vex_0F3846B32 D.op_evex_0F3846B64
            D.op_evex_0F3846B32 D.ds_nor_0F3846 D.ds_nor_0F3846
            D.ds_vex_0F3846B64 D.ds_vex_0F3846B32 D.ds_evex_0F3846B64
            D.ds_evex_0F3846B32
      | 0x4C ->
          parse_evex_by_rex ctx D.op_nor_0F384C D.op_vex_0F384C
            D.op_evex_0F384CB64 D.op_evex_0F384CB32 D.ds_nor_0F384C
            D.ds_vex_0F384C D.ds_evex_0F384CB64 D.ds_evex_0F384CB32
      | 0x4D ->
          parse_evex_by_rex ctx D.op_nor_0F384D D.op_vex_0F384D
            D.op_evex_0F384DB64 D.op_evex_0F384DB32 D.ds_nor_0F384D
            D.ds_vex_0F384D D.ds_evex_0F384DB64 D.ds_evex_0F384DB32
      | 0x4E ->
          parse_evex_by_rex ctx D.op_nor_0F384E D.op_vex_0F384E
            D.op_evex_0F384EB64 D.op_evex_0F384EB32 D.ds_nor_0F384E
            D.ds_vex_0F384E D.ds_evex_0F384EB64 D.ds_evex_0F384EB32
      | 0x4F ->
          parse_evex_by_rex ctx D.op_nor_0F384F D.op_vex_0F384F
            D.op_evex_0F384FB64 D.op_evex_0F384FB32 D.ds_nor_0F384F
            D.ds_vex_0F384F D.ds_evex_0F384FB64 D.ds_evex_0F384FB32
      | 0x5A ->
          parse_vex_op ctx SzDef32 D.op_nor_0F385A D.op_vex_0F385A
            D.ds_nor_0F385A D.ds_vex_0F385A
      | 0x78 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3878 D.op_vex_0F3878
            D.ds_nor_0F3878 D.ds_vex_0F3878
      | 0xF0 -> parse_non_vex_op ctx SzDef32 D.op_nor_0F38F0 D.ds_nor_0F38F0
      | 0xF1 -> parse_non_vex_op ctx SzDef32 D.op_nor_0F38F1 D.ds_nor_0F38F1
      | 0xF2 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F38F2 D.op_vex_0F38F2
            D.ds_nor_0F38F2 D.ds_vex_0F38F2
      | 0xF3 -> parse_grp_opcode ctx `G17 D._ByEy
      | 0xF5 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F38F5 D.op_vex_0F38F5
            D.ds_nor_0F38F5 D.ds_vex_0F38F5
      | 0xF6 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F38F6 D.op_vex_0F38F6
            D.ds_nor_0F38F6 D.ds_vex_0F38F6
      | 0xF7 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F38F7 D.op_vex_0F38F7
            D.ds_nor_0F38F7 D.ds_vex_0F38F7
      | 0xF8 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F38F8 D.op_vex_0F38F8
            D.ds_nor_0F38F8 D.ds_vex_0F38F8
      | _ -> Error `Invalid
    in
    if ctx.pos = pos then
      let%map _ = skip ctx in
      op
    else return op

  (* 0F 3A *)
  let parse_three_byte_op2 ctx =
    let open Result.Let_syntax in
    let pos = ctx.pos in
    let%bind b = peek ctx in
    let%bind op =
      match b with
      | 0x00 ->
          parse_evex_by_rex ctx D.op_nor_0F3A00 D.op_vex_0F3A00
            D.op_evex_0F3A00B64 D.op_evex_0F3A00B32 D.ds_nor_0F3A00
            D.ds_vex_0F3A00 D.ds_evex_0F3A00B64 D.ds_evex_0F3A00B32
      | 0x01 ->
          parse_evex_by_rex ctx D.op_nor_0F3A01 D.op_vex_0F3A01
            D.op_evex_0F3A01B64 D.op_evex_0F3A01B32 D.ds_nor_0F3A01
            D.ds_vex_0F3A01 D.ds_evex_0F3A01B64 D.ds_evex_0F3A01B32
      | 0x02 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A02 D.op_vex_0F3A02
            D.op_empty D.ds_nor_0F3A02 D.ds_vex_0F3A02 D.ds_empty
      | 0x03 ->
          parse_evex_by_rex ctx D.op_nor_0F3A03 D.op_vex_0F3A03
            D.op_evex_0F3A03B64 D.op_evex_0F3A03B32 D.ds_nor_0F3A03
            D.ds_vex_0F3A03 D.ds_evex_0F3A03B64 D.ds_evex_0F3A03B32
      | 0x04 ->
          parse_evex_by_rex ctx D.op_nor_0F3A04 D.op_vex_0F3A04
            D.op_evex_0F3A04B64 D.op_evex_0F3A04B32 D.ds_nor_0F3A04
            D.ds_vex_0F3A04 D.ds_evex_0F3A04B64 D.ds_evex_0F3A04B32
      | 0x05 ->
          parse_evex_by_rex ctx D.op_nor_0F3A05 D.op_vex_0F3A05
            D.op_evex_0F3A05B64 D.op_evex_0F3A05B32 D.ds_nor_0F3A05
            D.ds_vex_0F3A05 D.ds_evex_0F3A05B64 D.ds_evex_0F3A05B32
      | 0x06 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A06 D.op_vex_0F3A06
            D.op_empty D.ds_nor_0F3A06 D.ds_vex_0F3A06 D.ds_empty
      | 0x08 ->
          parse_evex_by_rex ctx D.op_nor_0F3A08 D.op_vex_0F3A08
            D.op_evex_0F3A08B64 D.op_evex_0F3A08B32 D.ds_nor_0F3A08
            D.ds_vex_0F3A08 D.ds_evex_0F3A08B64 D.ds_evex_0F3A08B32
      | 0x09 ->
          parse_evex_by_rex ctx D.op_nor_0F3A09 D.op_vex_0F3A09
            D.op_evex_0F3A09B64 D.op_evex_0F3A09B32 D.ds_nor_0F3A09
            D.ds_vex_0F3A09 D.ds_evex_0F3A09B64 D.ds_evex_0F3A09B32
      | 0x0A ->
          parse_evex_by_rex ctx D.op_nor_0F3A0A D.op_vex_0F3A0A
            D.op_evex_0F3A0AB64 D.op_evex_0F3A0AB32 D.ds_nor_0F3A0A
            D.ds_vex_0F3A0A D.ds_evex_0F3A0AB64 D.ds_evex_0F3A0AB32
      | 0x0B ->
          parse_evex_by_rex ctx D.op_nor_0F3A0B D.op_vex_0F3A0B
            D.op_evex_0F3A0BB64 D.op_evex_0F3A0BB32 D.ds_nor_0F3A0B
            D.ds_vex_0F3A0B D.ds_evex_0F3A0BB64 D.ds_evex_0F3A0BB32
      | 0x0C ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A0C D.op_vex_0F3A0C
            D.op_empty D.ds_nor_0F3A0C D.ds_vex_0F3A0C D.ds_empty
      | 0x0D ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A0D D.op_vex_0F3A0D
            D.op_empty D.ds_nor_0F3A0D D.ds_vex_0F3A0D D.ds_empty
      | 0x0E ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A0E D.op_vex_0F3A0E
            D.op_empty D.ds_nor_0F3A0E D.ds_vex_0F3A0E D.ds_empty
      | 0x0F ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A0F D.op_vex_0F3A0F
            D.ds_nor_0F3A0F D.ds_vex_0F3A0F
      | 0x14 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A14 D.op_vex_0F3A14
            D.ds_nor_0F3A14 D.ds_vex_0F3A14
      | 0x15 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A15 D.op_vex_0F3A15
            D.ds_nor_0F3A15 D.ds_vex_0F3A15
      | 0x16 ->
          parse_vex_and_evex_by_rex ctx D.op_nor_0F3A16B64 D.op_nor_0F3A16B32
            D.op_vex_0F3A16B64 D.op_vex_0F3A16B32 D.op_evex_0F3A16B64
            D.op_evex_0F3A16B32 D.ds_nor_0F3A16B64 D.ds_nor_0F3A16B32
            D.ds_vex_0F3A16B64 D.ds_vex_0F3A16B32 D.ds_evex_0F3A16B64
            D.ds_evex_0F3A16B32
      | 0x17 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A17 D.op_vex_0F3A17
            D.ds_nor_0F3A17 D.ds_vex_0F3A17
      | 0x18 ->
          parse_evex_by_rex ctx D.op_nor_0F3A18 D.op_vex_0F3A18
            D.op_evex_0F3A18B64 D.op_evex_0F3A18B32 D.ds_nor_0F3A18
            D.ds_vex_0F3A18 D.ds_evex_0F3A18B64 D.ds_evex_0F3A18B32
      | 0x19 ->
          parse_evex_by_rex ctx D.op_nor_0F3A19 D.op_vex_0F3A19
            D.op_evex_0F3A19B64 D.op_evex_0F3A19B32 D.ds_nor_0F3A19
            D.ds_vex_0F3A19 D.ds_evex_0F3A19B64 D.ds_evex_0F3A19B32
      | 0x1A ->
          parse_evex_by_rex ctx D.op_nor_0F3A1A D.op_vex_0F3A1A
            D.op_evex_0F3A1AB64 D.op_evex_0F3A1AB32 D.ds_nor_0F3A1A
            D.ds_vex_0F3A1A D.ds_evex_0F3A1AB64 D.ds_evex_0F3A1AB32
      | 0x1B ->
          parse_evex_by_rex ctx D.op_nor_0F3A1B D.op_vex_0F3A1B
            D.op_evex_0F3A1BB64 D.op_evex_0F3A1BB32 D.ds_nor_0F3A1B
            D.ds_vex_0F3A1B D.ds_evex_0F3A1BB64 D.ds_evex_0F3A1BB32
      | 0x1D ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A1D D.op_vex_0F3A1D
            D.ds_nor_0F3A1D D.ds_vex_0F3A1D
      | 0x1E ->
          parse_evex_by_rex ctx D.op_nor_0F3A1E D.op_vex_0F3A1E
            D.op_evex_0F3A1EB64 D.op_evex_0F3A1EB32 D.ds_nor_0F3A1E
            D.ds_vex_0F3A1E D.ds_evex_0F3A1EB64 D.ds_evex_0F3A1EB32
      | 0x1F ->
          parse_evex_by_rex ctx D.op_nor_0F3A1F D.op_vex_0F3A1F
            D.op_evex_0F3A1FB64 D.op_evex_0F3A1FB32 D.ds_nor_0F3A1F
            D.ds_vex_0F3A1F D.ds_evex_0F3A1FB64 D.ds_evex_0F3A1FB32
      | 0x20 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A20 D.op_vex_0F3A20
            D.ds_nor_0F3A20 D.ds_vex_0F3A20
      | 0x21 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A21 D.op_vex_0F3A21
            D.ds_nor_0F3A21 D.ds_vex_0F3A21
      | 0x22 ->
          parse_vex_and_evex_by_rex ctx D.op_nor_0F3A22B64 D.op_nor_0F3A22B32
            D.op_vex_0F3A22B64 D.op_vex_0F3A22B32 D.op_evex_0F3A22B64
            D.op_evex_0F3A22B32 D.ds_nor_0F3A22B64 D.ds_nor_0F3A22B32
            D.ds_vex_0F3A22B64 D.ds_vex_0F3A22B32 D.ds_evex_0F3A22B64
            D.ds_evex_0F3A22B32
      | 0x38 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A38 D.op_vex_0F3A38
            D.ds_nor_0F3A38 D.ds_vex_0F3A38
      | 0x39 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F3A39 D.op_vex_0F3A39
            D.ds_nor_0F3A39 D.ds_vex_0F3A39
      | 0x60 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A60 D.op_vex_0F3A60
            D.op_empty D.ds_nor_0F3A60 D.ds_vex_0F3A60 D.ds_empty
      | 0x61 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A61 D.op_vex_0F3A61
            D.op_empty D.ds_nor_0F3A61 D.ds_vex_0F3A61 D.ds_empty
      | 0x62 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A62 D.op_vex_0F3A62
            D.op_empty D.ds_nor_0F3A62 D.ds_vex_0F3A62 D.ds_empty
      | 0x63 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3A63 D.op_vex_0F3A63
            D.op_empty D.ds_nor_0F3A63 D.ds_vex_0F3A63 D.ds_empty
      | 0xF0 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F3AF0 D.op_vex_0F3AF0
            D.op_empty D.ds_nor_0F3AF0 D.ds_vex_0F3AF0 D.ds_empty
      | _ -> Error `Invalid
    in
    if ctx.pos = pos then
      let%map _ = skip ctx in
      op
    else return op

  let parse_3dnow ctx =
    let open Result.Let_syntax in
    (* from sandpile.org:
     * "The 3DNow! instruction encoding differs from the regular
     *  instruction encoding, in that the mod R/M (and SIB) byte
     *  as well as an optional displacement follow directly after
     *  the two 0Fh prefix bytes. The byte selecting the instruction
     *  itself is appended as the last byte." *)
    let%bind b = next ctx in
    let%bind _ = parse_modrm ctx b (ctx.pos - 1) in
    ctx.no_modrm <- true;
    ctx.is_3dnow <- true;
    let _, addrsz =
      if Mode.(equal ctx.mode AMD64) then size64 ctx SzDef32 else size32 ctx
    in
    let%bind o =
      if addrsz = 16 then
        match modrm_mod b with
        | 0 when modrm_rm b = 6 -> Ok 2
        | 1 -> Ok 1
        | 2 -> Ok 2
        | _ -> Ok 0
      else
        match modrm_mod b with
        | 0 -> (
          match modrm_rm b with
          | 4 ->
              let%bind b2 = peek ctx in
              Ok (if modrm_rm b2 = 5 then 5 else 1)
          | 5 -> Ok 4
          | _ -> Ok 0 )
        | 1 -> (
          match modrm_rm b with
          | 4 -> Ok 2
          | 5 -> Ok 4
          | _ -> Ok 1 )
        | 2 -> (
          match modrm_rm b with
          | 4 -> Ok 5
          | _ -> Ok 4 )
        | _ -> Ok 0
    in
    let%bind b2 = peek_o ctx o in
    match b2 with
    | 0x0C -> parse_op ctx M.PI2FW SzDef32 D._PqQq
    | 0x0D -> parse_op ctx M.PI2FD SzDef32 D._PqQq
    | 0x1C -> parse_op ctx M.PF2IW SzDef32 D._PqQq
    | 0x1D -> parse_op ctx M.PF2ID SzDef32 D._PqQq
    | 0x86 -> parse_op ctx M.PFRCP SzDef32 D._PqQq
    | 0x87 -> parse_op ctx M.PFRSQRTV SzDef32 D._PqQq
    | 0x8A -> parse_op ctx M.PFNACC SzDef32 D._PqQq
    | 0x8E -> parse_op ctx M.PFPNACC SzDef32 D._PqQq
    | 0x90 -> parse_op ctx M.PFCMPGE SzDef32 D._PqQq
    | 0x94 -> parse_op ctx M.PFMIN SzDef32 D._PqQq
    | 0x96 -> parse_op ctx M.PFRCP SzDef32 D._PqQq
    | 0x97 -> parse_op ctx M.PFRSQRT SzDef32 D._PqQq
    | 0x9A -> parse_op ctx M.PFSUB SzDef32 D._PqQq
    | 0x9E -> parse_op ctx M.PFADD SzDef32 D._PqQq
    | 0xA0 -> parse_op ctx M.PFCMPGT SzDef32 D._PqQq
    | 0xA4 -> parse_op ctx M.PFMAX SzDef32 D._PqQq
    | 0xA6 -> parse_op ctx M.PFRCPIT1 SzDef32 D._PqQq
    | 0xA7 -> parse_op ctx M.PFRSQIT1 SzDef32 D._PqQq
    | 0xAA -> parse_op ctx M.PFSUBR SzDef32 D._PqQq
    | 0xAE -> parse_op ctx M.PFACC SzDef32 D._PqQq
    | 0xB0 -> parse_op ctx M.PFCMPEQ SzDef32 D._PqQq
    | 0xB4 -> parse_op ctx M.PFMUL SzDef32 D._PqQq
    | 0xB6 -> parse_op ctx M.PFRCPIT2 SzDef32 D._PqQq
    | 0xB7 -> parse_op ctx M.PMULHRW SzDef32 D._PqQq
    | 0xBB -> parse_op ctx M.PSWAPD SzDef32 D._PqQq
    | 0xBF -> parse_op ctx M.PAVGUSB SzDef32 D._PqQq
    | _ -> Error `Invalid

  let parse_two_byte ctx =
    let open Result.Let_syntax in
    let pos = ctx.pos in
    let%bind b = peek ctx in
    let%bind op =
      match b with
      | 0x02 -> parse_op ctx M.LAR SzDef32 D._GvEw
      | 0x03 -> parse_op ctx M.LSL SzDef32 D._GvEw
      | 0x05 ->
          if not Mode.(equal ctx.mode AMD64) then
            parse_op ctx M.SYSCALL SzDef32 [||]
          else parse_op ctx M.SYSCALL SzDef64 [||]
      | 0x06 -> parse_op ctx M.CLTS SzDef32 [||]
      | 0x07 ->
          if not Mode.(equal ctx.mode AMD64) then
            parse_op ctx M.SYSRET SzDef32 [||]
          else parse_op ctx M.SYSRET SzDef64 [||]
      | 0x08 -> parse_op ctx M.INVD SzDef32 [||]
      | 0x09 -> parse_op ctx M.WBINVD SzDef32 [||]
      | 0x0B -> parse_op ctx M.UD2 SzDef32 [||]
      | 0x0D ->
          let%bind op, desc = opcode_0F0D ctx in
          parse_op ctx op SzDef32 desc
      | 0x0E -> parse_op ctx M.FEMMS SzDef32 [||]
      | 0x0F ->
          let%bind _ =
            if ctx.prefix.has_lock then Error `BadLock else Ok ()
          in
          let%bind _ = skip ctx in
          parse_3dnow ctx
      | 0x10 ->
          parse_vex_by_mem ctx D.op_nor_0F10 D.op_nor_0F10 D.op_vex_0F10_mem
            D.op_vex_0F10_reg D.ds_nor_0F10 D.ds_nor_0F10 D.ds_vex_0F10_mem
            D.ds_vex_0F10_reg
      | 0x11 ->
          parse_vex_by_mem ctx D.op_nor_0F11 D.op_nor_0F11 D.op_vex_0F11_mem
            D.op_vex_0F11_reg D.ds_nor_0F11 D.ds_nor_0F11 D.ds_vex_0F11_mem
            D.ds_vex_0F11_reg
      | 0x12 ->
          parse_vex_by_mem ctx D.op_nor_0F12_mem D.op_nor_0F12_reg
            D.op_vex_0F12_mem D.op_vex_0F12_reg D.ds_nor_0F12_mem
            D.ds_nor_0F12_reg D.ds_vex_0F12_mem D.ds_vex_0F12_reg
      | 0x13 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F13 D.op_vex_0F13 D.ds_nor_0F13
            D.ds_vex_0F13
      | 0x14 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F14 D.op_vex_0F14 D.ds_nor_0F14
            D.ds_vex_0F14
      | 0x15 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F15 D.op_vex_0F15 D.ds_nor_0F15
            D.ds_vex_0F15
      | 0x16 ->
          parse_vex_by_mem ctx D.op_nor_0F16_mem D.op_nor_0F16_reg
            D.op_vex_0F16_mem D.op_vex_0F16_reg D.ds_nor_0F16_mem
            D.ds_nor_0F16_reg D.ds_vex_0F16_mem D.ds_vex_0F16_reg
      | 0x17 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F17 D.op_vex_0F17 D.ds_nor_0F17
            D.ds_vex_0F17
      | 0x19 -> parse_op ctx M.NOP SzDef32 D._E0v
      | 0x1A -> parse_bnd_op ctx SzDef32 D.op_nor_0F1A D.ds_nor_0F1A
      | 0x1B -> parse_bnd_op ctx SzDef32 D.op_nor_0F1B D.ds_nor_0F1B
      | 0x1C ->
          let%bind b' = peek_o ctx 1 in
          if modrm_reg b' = 0b000 then parse_op ctx M.CLDEMOTE SzDef32 D._Mb
          else parse_op ctx M.NOP SzDef32 D._E0v
      | 0x1D -> parse_op ctx M.NOP SzDef32 D._E0v
      | 0x1E -> (
          let%bind b' = peek_o ctx 1 in
          match modrm_reg b' with
          | 0b001 ->
              make_mandatory ctx;
              if ctx.prefix.mandatory_candidate = 0xF3 then
                match select_rex ctx with
                | Some (w, _, _, _) when w <> 0 ->
                    parse_op ctx M.RDSSPQ SzDef32 D._Rq
                | _ -> parse_op ctx M.RDSSPD SzDef32 D._Rd
              else Error `Invalid
          | 0b111 ->
              make_mandatory ctx;
              if ctx.prefix.mandatory_candidate = 0xF3 then
                match b' with
                | 0xFA ->
                    let%bind _ = skip_o ctx 2 in
                    parse_op ctx M.ENDBR64 SzDef32 [||]
                | 0xFB ->
                    let%bind _ = skip_o ctx 2 in
                    parse_op ctx M.ENDBR32 SzDef32 [||]
                | _ -> Error `Invalid
              else Error `Invalid
          | _ -> parse_op ctx M.NOP SzDef32 D._E0v )
      | 0x1F -> parse_op ctx M.NOP SzDef32 D._E0v
      | 0x20 -> parse_op ctx M.MOV Sz64 D._RdCd
      | 0x21 -> parse_op ctx M.MOV SzDef32 D._RdDd
      | 0x22 -> parse_op ctx M.MOV SzDef32 D._CdRd
      | 0x23 -> parse_op ctx M.MOV SzDef32 D._DdRd
      | 0x24 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.MOV SzDef32 D._RdTd
      | 0x26 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.MOV SzDef32 D._TdRd
      | 0x28 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F28 D.op_vex_0F28 D.ds_nor_0F28
            D.ds_vex_0F28
      | 0x29 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F29 D.op_vex_0F29 D.ds_nor_0F29
            D.ds_vex_0F29
      | 0x2A ->
          parse_vex_op ctx SzDef32 D.op_nor_0F2A D.op_vex_0F2A D.ds_nor_0F2A
            D.ds_vex_0F2A
      | 0x2B ->
          parse_vex_op ctx SzDef32 D.op_nor_0F2B D.op_vex_0F2B D.ds_nor_0F2B
            D.ds_vex_0F2B
      | 0x2C ->
          parse_vex_op ctx SzDef32 D.op_nor_0F2C D.op_vex_0F2C D.ds_nor_0F2C
            D.ds_vex_0F2C
      | 0x2D ->
          parse_vex_op ctx SzDef32 D.op_nor_0F2D D.op_vex_0F2D D.ds_nor_0F2D
            D.ds_vex_0F2D
      | 0x2E ->
          parse_vex_op ctx SzDef32 D.op_nor_0F2E D.op_vex_0F2E D.ds_nor_0F2E
            D.ds_vex_0F2E
      | 0x2F ->
          parse_vex_op ctx SzDef32 D.op_nor_0F2F D.op_vex_0F2F D.ds_nor_0F2F
            D.ds_vex_0F2F
      | 0x30 -> parse_op ctx M.WRMSR SzDef32 [||]
      | 0x31 -> parse_op ctx M.RDTSC SzDef32 [||]
      | 0x32 -> parse_op ctx M.RDMSR SzDef32 [||]
      | 0x33 -> parse_op ctx M.RDPMC SzDef32 [||]
      | 0x34 -> parse_op ctx M.SYSENTER SzDef32 [||]
      | 0x35 -> parse_op ctx M.SYSEXIT SzDef32 [||]
      | 0x37 -> parse_op ctx M.GETSEC SzDef32 [||]
      | 0x40 -> parse_op ctx M.CMOVO SzDef32 D._GvEv
      | 0x41 -> parse_op ctx M.CMOVNO SzDef32 D._GvEv
      | 0x42 -> parse_op ctx M.CMOVB SzDef32 D._GvEv
      | 0x43 -> parse_op ctx M.CMOVNB SzDef32 D._GvEv
      | 0x44 -> parse_op ctx M.CMOVZ SzDef32 D._GvEv
      | 0x45 -> parse_op ctx M.CMOVNZ SzDef32 D._GvEv
      | 0x46 -> parse_op ctx M.CMOVBE SzDef32 D._GvEv
      | 0x47 -> parse_op ctx M.CMOVNBE SzDef32 D._GvEv
      | 0x48 -> parse_op ctx M.CMOVS SzDef32 D._GvEv
      | 0x49 -> parse_op ctx M.CMOVNS SzDef32 D._GvEv
      | 0x4A -> parse_op ctx M.CMOVP SzDef32 D._GvEv
      | 0x4B -> parse_op ctx M.CMOVNP SzDef32 D._GvEv
      | 0x4C -> parse_op ctx M.CMOVL SzDef32 D._GvEv
      | 0x4D -> parse_op ctx M.CMOVNL SzDef32 D._GvEv
      | 0x4E -> parse_op ctx M.CMOVLE SzDef32 D._GvEv
      | 0x4F -> parse_op ctx M.CMOVNLE SzDef32 D._GvEv
      | 0x50 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F50 D.op_vex_0F50 D.op_empty
            D.ds_nor_0F50 D.ds_vex_0F50 D.ds_empty
      | 0x51 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F51 D.op_vex_0F51 D.ds_nor_0F51
            D.ds_vex_0F51
      | 0x52 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F52 D.op_vex_0F52 D.op_empty
            D.ds_nor_0F52 D.ds_vex_0F52 D.ds_empty
      | 0x53 ->
          parse_evex_op ctx SzDef32 D.op_nor_0F53 D.op_vex_0F53 D.op_empty
            D.ds_nor_0F53 D.ds_vex_0F53 D.ds_empty
      | 0x54 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F54 D.op_vex_0F54 D.ds_nor_0F54
            D.ds_vex_0F54
      | 0x55 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F55 D.op_vex_0F55 D.ds_nor_0F55
            D.ds_vex_0F55
      | 0x56 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F56 D.op_vex_0F56 D.ds_nor_0F56
            D.ds_vex_0F56
      | 0x57 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F57 D.op_vex_0F57 D.ds_nor_0F57
            D.ds_vex_0F57
      | 0x58 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F58 D.op_vex_0F58 D.ds_nor_0F58
            D.ds_vex_0F58
      | 0x59 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F59 D.op_vex_0F59 D.ds_nor_0F59
            D.ds_vex_0F59
      | 0x5A ->
          parse_vex_op ctx SzDef32 D.op_nor_0F5A D.op_vex_0F5A D.ds_nor_0F5A
            D.ds_vex_0F5A
      | 0x5B ->
          parse_vex_op ctx SzDef32 D.op_nor_0F5B D.op_vex_0F5B D.ds_nor_0F5B
            D.ds_vex_0F5B
      | 0x5C ->
          parse_vex_op ctx SzDef32 D.op_nor_0F5C D.op_vex_0F5C D.ds_nor_0F5C
            D.ds_vex_0F5C
      | 0x5D ->
          parse_vex_op ctx SzDef32 D.op_nor_0F5D D.op_vex_0F5D D.ds_nor_0F5D
            D.ds_vex_0F5D
      | 0x5E ->
          parse_vex_op ctx SzDef32 D.op_nor_0F5E D.op_vex_0F5E D.ds_nor_0F5E
            D.ds_vex_0F5E
      | 0x5F ->
          parse_vex_op ctx SzDef32 D.op_nor_0F5F D.op_vex_0F5F D.ds_nor_0F5F
            D.ds_vex_0F5F
      | 0x60 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F60 D.op_vex_0F60 D.ds_nor_0F60
            D.ds_vex_0F60
      | 0x61 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F61 D.op_vex_0F61 D.ds_nor_0F61
            D.ds_vex_0F61
      | 0x62 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F62 D.op_vex_0F62 D.ds_nor_0F62
            D.ds_vex_0F62
      | 0x63 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F63 D.op_vex_0F63 D.ds_nor_0F63
            D.ds_vex_0F63
      | 0x64 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F64 D.op_vex_0F64 D.ds_nor_0F64
            D.ds_vex_0F64
      | 0x65 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F65 D.op_vex_0F65 D.ds_nor_0F65
            D.ds_vex_0F65
      | 0x66 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F66 D.op_vex_0F66 D.ds_nor_0F66
            D.ds_vex_0F66
      | 0x67 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F67 D.op_vex_0F67 D.ds_nor_0F67
            D.ds_vex_0F67
      | 0x68 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F68 D.op_vex_0F68 D.ds_nor_0F68
            D.ds_vex_0F68
      | 0x69 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F69 D.op_vex_0F69 D.ds_nor_0F69
            D.ds_vex_0F69
      | 0x6A ->
          parse_vex_op ctx SzDef32 D.op_nor_0F6A D.op_vex_0F6A D.ds_nor_0F6A
            D.ds_vex_0F6A
      | 0x6B ->
          parse_vex_op ctx SzDef32 D.op_nor_0F6B D.op_vex_0F6B D.ds_nor_0F6B
            D.ds_vex_0F6B
      | 0x6C ->
          parse_vex_op ctx SzDef32 D.op_nor_0F6C D.op_vex_0F6C D.ds_nor_0F6C
            D.ds_vex_0F6C
      | 0x6D ->
          parse_vex_op ctx SzDef32 D.op_nor_0F6D D.op_vex_0F6D D.ds_nor_0F6D
            D.ds_vex_0F6D
      | 0x6E ->
          parse_vex_by_rex ctx D.op_nor_0F6EB64 D.op_nor_0F6EB32
            D.op_vex_0F6EB64 D.op_vex_0F6EB32 D.ds_nor_0F6EB64
            D.ds_nor_0F6EB32 D.ds_vex_0F6EB64 D.ds_vex_0F6EB32
      | 0x6F ->
          parse_evex_by_rex ctx D.op_nor_0F6F D.op_vex_0F6F D.op_evex_0F6FB64
            D.op_evex_0F6FB32 D.ds_nor_0F6F D.ds_vex_0F6F D.ds_evex_0F6FB64
            D.ds_evex_0F6FB32
      | 0x70 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F70 D.op_vex_0F70 D.ds_nor_0F70
            D.ds_vex_0F70
      | 0x74 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F74 D.op_vex_0F74 D.ds_nor_0F74
            D.ds_vex_0F74
      | 0x75 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F75 D.op_vex_0F75 D.ds_nor_0F75
            D.ds_vex_0F75
      | 0x76 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F76 D.op_vex_0F76 D.ds_nor_0F76
            D.ds_vex_0F76
      | 0x77 ->
          parse_vex_op ctx SzDef32 D.op_nor_0F77 D.op_vex_0F77 D.ds_nor_0F77
            D.ds_vex_0F77
      | 0x7E ->
          parse_vex_by_rex ctx D.op_nor_0F7EB64 D.op_nor_0F7EB32
            D.op_vex_0F7EB64 D.op_vex_0F7EB32 D.ds_nor_0F7EB64
            D.ds_nor_0F7EB32 D.ds_vex_0F7EB64 D.ds_vex_0F7EB32
      | 0x7F ->
          parse_evex_by_rex ctx D.op_nor_0F7F D.op_vex_0F7F D.op_evex_0F7FB64
            D.op_evex_0F7FB32 D.ds_nor_0F7F D.ds_vex_0F7F D.ds_evex_0F7FB64
            D.ds_evex_0F7FB32
      | 0x80 -> parse_op ctx M.JO Sz64 D._Jz
      | 0x81 -> parse_op ctx M.JNO Sz64 D._Jz
      | 0x82 -> parse_op ctx M.JB Sz64 D._Jz
      | 0x83 -> parse_op ctx M.JNB Sz64 D._Jz
      | 0x84 -> parse_op ctx M.JZ Sz64 D._Jz
      | 0x85 -> parse_op ctx M.JNZ Sz64 D._Jz
      | 0x86 -> parse_op ctx M.JBE Sz64 D._Jz
      | 0x87 -> parse_op ctx M.JNBE Sz64 D._Jz
      | 0x88 -> parse_op ctx M.JS Sz64 D._Jz
      | 0x89 -> parse_op ctx M.JNS Sz64 D._Jz
      | 0x8A -> parse_op ctx M.JP Sz64 D._Jz
      | 0x8B -> parse_op ctx M.JNP Sz64 D._Jz
      | 0x8C -> parse_op ctx M.JL Sz64 D._Jz
      | 0x8D -> parse_op ctx M.JNL Sz64 D._Jz
      | 0x8E -> parse_op ctx M.JLE Sz64 D._Jz
      | 0x8F -> parse_op ctx M.JNLE Sz64 D._Jz
      | 0x90 -> parse_op ctx M.SETO SzDef32 D._Eb
      | 0x91 -> parse_op ctx M.SETNO SzDef32 D._Eb
      | 0x92 -> parse_op ctx M.SETB SzDef32 D._Eb
      | 0x93 -> parse_op ctx M.SETNB SzDef32 D._Eb
      | 0x94 -> parse_op ctx M.SETZ SzDef32 D._Eb
      | 0x95 -> parse_op ctx M.SETNZ SzDef32 D._Eb
      | 0x96 -> parse_op ctx M.SETBE SzDef32 D._Eb
      | 0x97 -> parse_op ctx M.SETNBE SzDef32 D._Eb
      | 0x98 -> parse_op ctx M.SETS SzDef32 D._Eb
      | 0x99 -> parse_op ctx M.SETNS SzDef32 D._Eb
      | 0x9A -> parse_op ctx M.SETP SzDef32 D._Eb
      | 0x9B -> parse_op ctx M.SETNP SzDef32 D._Eb
      | 0x9C -> parse_op ctx M.SETL SzDef32 D._Eb
      | 0x9D -> parse_op ctx M.SETNL SzDef32 D._Eb
      | 0x9E -> parse_op ctx M.SETLE SzDef32 D._Eb
      | 0x9F -> parse_op ctx M.SETNLE SzDef32 D._Eb
      | 0xA0 -> parse_op ctx M.PUSH SzDef64 (D._ORSR R.FS)
      | 0xA1 -> parse_op ctx M.POP SzDef64 (D._ORSR R.FS)
      | 0xA2 -> parse_op ctx M.CPUID SzDef32 [||]
      | 0xA3 -> parse_op ctx M.BT SzDef32 D._EvGv
      | 0xA4 -> parse_op ctx M.SHLD SzDef32 D._EvGvIb
      | 0xA5 -> parse_op ctx M.SHLD SzDef32 D._EvGvCL
      | 0xA6 ->
          if ctx.prefix.mandatory_candidate <> 0xF3 then Error `Invalid
          else
            let%bind b2 = peek_o ctx 1 in
            if b2 <> 0xC0 then Error `Invalid
            else
              let%bind _ = skip_o ctx 2 in
              parse_op ctx M.MONTMUL SzDef32 [||]
      | 0xA7 ->
          if ctx.prefix.mandatory_candidate = 0xF2 then Error `Invalid
          else
            let%bind b2 = peek_o ctx 1 in
            if b2 <> 0xC0 then Error `Invalid
            else
              let%bind _ = skip_o ctx 2 in
              parse_op ctx M.XSTORE SzDef32 [||]
      | 0xA8 -> parse_op ctx M.PUSH SzDef64 (D._ORSR R.GS)
      | 0xA9 -> parse_op ctx M.POP SzDef64 (D._ORSR R.GS)
      | 0xAA -> parse_op ctx M.RSM SzDef32 [||]
      | 0xAB -> parse_op ctx M.BTS SzDef32 D._EvGv
      | 0xAC -> parse_op ctx M.SHRD SzDef32 D._EvGvIb
      | 0xAD -> parse_op ctx M.SHRD SzDef32 D._EvGvCL
      | 0xAF -> parse_op ctx M.IMUL SzDef32 D._GvEv
      | 0xB0 -> parse_op ctx M.CMPXCHG SzDef32 D._EbGb
      | 0xB1 -> parse_op ctx M.CMPXCHG SzDef32 D._EvGv
      | 0xB2 -> parse_op ctx M.LSS SzDef32 D._GvMp
      | 0xB3 -> parse_op ctx M.BTR SzDef32 D._EvGv
      | 0xB4 -> parse_op ctx M.LFS SzDef32 D._GvMp
      | 0xB5 -> parse_op ctx M.LGS SzDef32 D._GvMp
      | 0xB6 -> parse_op ctx M.MOVZX SzDef32 D._GvEb
      | 0xB7 -> parse_op ctx M.MOVZX SzDef32 D._GvEw
      | 0xB8 ->
          if ctx.prefix.group1 <> 0xF3 then Error `Invalid
          else parse_op ctx M.POPCNT SzDef32 D._GvEv
      | 0xBB ->
          if ctx.prefix.group1 = 0xF3 then Error `Invalid
          else parse_op ctx M.BTC SzDef32 D._EvGv
      | 0xBC when ctx.prefix.group1 = 0xF3 ->
          parse_op ctx M.TZCNT SzDef32 D._GvEv
      | 0xBC -> parse_op ctx M.BSF SzDef32 D._GvEv
      | 0xBD when ctx.prefix.group1 = 0xF3 ->
          parse_op ctx M.LZCNT SzDef32 D._GvEv
      | 0xBD -> parse_op ctx M.BSR SzDef32 D._GvEv
      | 0xBE -> parse_op ctx M.MOVSX SzDef32 D._GvEb
      | 0xBF -> parse_op ctx M.MOVSX SzDef32 D._GvEw
      | 0xC0 -> parse_op ctx M.XADD SzDef32 D._EbGb
      | 0xC1 -> parse_op ctx M.XADD SzDef32 D._EvGv
      | 0xC2 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FC2 D.op_vex_0FC2 D.ds_nor_0FC2
            D.ds_vex_0FC2
      | 0xC3 -> parse_op ctx M.MOVNTI SzDef32 D._MyGy
      | 0xC4 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FC4 D.op_vex_0FC4 D.ds_nor_0FC4
            D.ds_vex_0FC4
      | 0xC5 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FC5 D.op_vex_0FC5 D.ds_nor_0FC5
            D.ds_vex_0FC5
      | 0xC6 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FC6 D.op_vex_0FC6 D.ds_nor_0FC6
            D.ds_vex_0FC6
      | 0xC8 | 0xC9 | 0xCA | 0xCB | 0xCC | 0xCD | 0xCE | 0xCF ->
          (* ignore_all_prefix ctx 0x66; *)
          ctx.no_modrm <- true;
          parse_op ctx M.BSWAP SzDef32 (D._RGv (b land 7))
      | 0xD1 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FD1 D.op_vex_0FD1 D.ds_nor_0FD1
            D.ds_vex_0FD1
      | 0xD2 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FD2 D.op_vex_0FD2 D.ds_nor_0FD2
            D.ds_vex_0FD2
      | 0xD3 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FD3 D.op_vex_0FD3 D.ds_nor_0FD3
            D.ds_vex_0FD3
      | 0xD4 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FD4 D.op_vex_0FD4 D.ds_nor_0FD4
            D.ds_vex_0FD4
      | 0xD5 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FD5 D.op_vex_0FD5 D.ds_nor_0FD5
            D.ds_vex_0FD5
      | 0xD6 ->
          let%bind _ =
            match ctx.vector_length with
            | Some len when len = 256 -> Error `Invalid
            | _ -> Ok ()
          in
          parse_vex_op ctx SzDef32 D.op_nor_0FD6 D.op_vex_0FD6 D.ds_nor_0FD6
            D.ds_vex_0FD6
      | 0xD7 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FD7 D.op_vex_0FD7 D.ds_nor_0FD7
            D.ds_vex_0FD7
      | 0xD8 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FD8 D.op_vex_0FD8 D.ds_nor_0FD8
            D.ds_vex_0FD8
      | 0xD9 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FD9 D.op_vex_0FD9 D.ds_nor_0FD9
            D.ds_vex_0FD9
      | 0xDA ->
          parse_vex_op ctx SzDef32 D.op_nor_0FDA D.op_vex_0FDA D.ds_nor_0FDA
            D.ds_vex_0FDA
      | 0xDB ->
          parse_vex_op ctx SzDef32 D.op_nor_0FDB D.op_vex_0FDB D.ds_nor_0FDB
            D.ds_vex_0FDB
      | 0xDC ->
          parse_vex_op ctx SzDef32 D.op_nor_0FDC D.op_vex_0FDC D.ds_nor_0FDC
            D.ds_vex_0FDC
      | 0xDD ->
          parse_vex_op ctx SzDef32 D.op_nor_0FDD D.op_vex_0FDD D.ds_nor_0FDD
            D.ds_vex_0FDD
      | 0xDE ->
          parse_vex_op ctx SzDef32 D.op_nor_0FDE D.op_vex_0FDE D.ds_nor_0FDE
            D.ds_vex_0FDE
      | 0xDF ->
          parse_vex_op ctx SzDef32 D.op_nor_0FDF D.op_vex_0FDF D.ds_nor_0FDF
            D.ds_vex_0FDF
      | 0xE0 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE0 D.op_vex_0FE0 D.ds_nor_0FE0
            D.ds_vex_0FE0
      | 0xE1 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE1 D.op_vex_0FE1 D.ds_nor_0FE1
            D.ds_vex_0FE1
      | 0xE2 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE2 D.op_vex_0FE2 D.ds_nor_0FE2
            D.ds_vex_0FE2
      | 0xE3 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE3 D.op_vex_0FE3 D.ds_nor_0FE3
            D.ds_vex_0FE3
      | 0xE4 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE4 D.op_vex_0FE4 D.ds_nor_0FE4
            D.ds_vex_0FE4
      | 0xE5 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE5 D.op_vex_0FE5 D.ds_nor_0FE5
            D.ds_vex_0FE5
      | 0xE6 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE6 D.op_vex_0FE6 D.ds_nor_0FE6
            D.ds_vex_0FE6
      | 0xE7 ->
          parse_evex_by_rex ctx D.op_nor_0FE7 D.op_vex_0FE7 D.op_evex_0FE7B64
            D.op_evex_0FE7B32 D.ds_nor_0FE7 D.ds_vex_0FE7 D.ds_evex_0FE7B64
            D.ds_evex_0FE7B32
      | 0xE8 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE8 D.op_vex_0FE8 D.ds_nor_0FE8
            D.ds_vex_0FE8
      | 0xE9 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FE9 D.op_vex_0FE9 D.ds_nor_0FE9
            D.ds_vex_0FE9
      | 0xEA ->
          parse_vex_op ctx SzDef32 D.op_nor_0FEA D.op_vex_0FEA D.ds_nor_0FEA
            D.ds_vex_0FEA
      | 0xEB ->
          parse_vex_op ctx SzDef32 D.op_nor_0FEB D.op_vex_0FEB D.ds_nor_0FEB
            D.ds_vex_0FEB
      | 0xEC ->
          parse_vex_op ctx SzDef32 D.op_nor_0FEC D.op_vex_0FEC D.ds_nor_0FEC
            D.ds_vex_0FEC
      | 0xED ->
          parse_vex_op ctx SzDef32 D.op_nor_0FED D.op_vex_0FED D.ds_nor_0FED
            D.ds_vex_0FED
      | 0xEE ->
          parse_vex_op ctx SzDef32 D.op_nor_0FEE D.op_vex_0FEE D.ds_nor_0FEE
            D.ds_vex_0FEE
      | 0xEF ->
          parse_vex_op ctx SzDef32 D.op_nor_0FEF D.op_vex_0FEF D.ds_nor_0FEF
            D.ds_vex_0FEF
      | 0xF0 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF0 D.op_vex_0FF0 D.ds_nor_0FF0
            D.ds_vex_0FF0
      | 0xF1 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF1 D.op_vex_0FF1 D.ds_nor_0FF1
            D.ds_vex_0FF1
      | 0xF2 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF2 D.op_vex_0FF2 D.ds_nor_0FF2
            D.ds_vex_0FF2
      | 0xF3 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF3 D.op_vex_0FF3 D.ds_nor_0FF3
            D.ds_vex_0FF3
      | 0xF4 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF4 D.op_vex_0FF4 D.ds_nor_0FF4
            D.ds_vex_0FF4
      | 0xF5 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF5 D.op_vex_0FF5 D.ds_nor_0FF5
            D.ds_vex_0FF5
      | 0xF6 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF6 D.op_vex_0FF6 D.ds_nor_0FF6
            D.ds_vex_0FF6
      | 0xF8 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF8 D.op_vex_0FF8 D.ds_nor_0FF8
            D.ds_vex_0FF8
      | 0xF9 ->
          parse_vex_op ctx SzDef32 D.op_nor_0FF9 D.op_vex_0FF9 D.ds_nor_0FF9
            D.ds_vex_0FF9
      | 0xFA ->
          parse_vex_op ctx SzDef32 D.op_nor_0FFA D.op_vex_0FFA D.ds_nor_0FFA
            D.ds_vex_0FFA
      | 0xFB ->
          parse_vex_op ctx SzDef32 D.op_nor_0FFB D.op_vex_0FFB D.ds_nor_0FFB
            D.ds_vex_0FFB
      | 0xFC ->
          parse_vex_op ctx SzDef32 D.op_nor_0FFC D.op_vex_0FFC D.ds_nor_0FFC
            D.ds_vex_0FFC
      | 0xFD ->
          parse_vex_op ctx SzDef32 D.op_nor_0FFD D.op_vex_0FFD D.ds_nor_0FFD
            D.ds_vex_0FFD
      | 0xFE ->
          parse_vex_op ctx SzDef32 D.op_nor_0FFE D.op_vex_0FFE D.ds_nor_0FFE
            D.ds_vex_0FFE
      | 0xFF -> parse_op ctx M.UD0 SzDef32 D._GdEv
      (* group *)
      | 0x00 -> parse_grp_opcode ctx `G6 [||]
      | 0x01 -> parse_grp_opcode ctx `G7 [||]
      | 0xB9 -> parse_grp_opcode ctx `G10 [||]
      | 0xBA -> parse_grp_opcode ctx `G8 D._EvIb
      | 0xC7 -> parse_grp_opcode ctx `G9 [||]
      | 0x71 -> parse_grp_opcode ctx `G12 [||]
      | 0x72 -> parse_grp_opcode ctx `G13 [||]
      | 0x73 -> parse_grp_opcode ctx `G14 [||]
      | 0xAE -> parse_grp_opcode ctx `G15 [||]
      | 0x18 -> parse_grp_opcode ctx `G16 D._Mv
      (* escape *)
      | 0x38 ->
          let%bind _ = skip ctx in
          parse_three_byte_op1 ctx
      | 0x3A ->
          let%bind _ = skip ctx in
          parse_three_byte_op2 ctx
      | _ -> Error `Invalid
    in
    if ctx.pos = pos then
      let%map _ = skip ctx in
      op
    else return op

  let parse_one_byte ctx =
    let open Result.Let_syntax in
    let pos = ctx.pos in
    let%bind b = peek ctx in
    let%bind op =
      match b with
      | 0x00 -> parse_op ctx M.ADD SzDef32 D._EbGb
      | 0x01 -> parse_op ctx M.ADD SzDef32 D._EvGv
      | 0x02 -> parse_op ctx M.ADD SzDef32 D._GbEb
      | 0x03 -> parse_op ctx M.ADD SzDef32 D._GvEv
      | 0x04 ->
          ctx.no_modrm <- true;
          parse_op ctx M.ADD SzDef32 D._ALIb
      | 0x05 ->
          ctx.no_modrm <- true;
          parse_op ctx M.ADD SzDef32 D._RGvSIz
      | 0x06 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.PUSH SzInv64 (D._ORSR R.ES)
      | 0x07 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.POP SzInv64 (D._ORSR R.ES)
      | 0x08 -> parse_op ctx M.OR SzDef32 D._EbGb
      | 0x09 -> parse_op ctx M.OR SzDef32 D._EvGv
      | 0x0A -> parse_op ctx M.OR SzDef32 D._GbEb
      | 0x0B -> parse_op ctx M.OR SzDef32 D._GvEv
      | 0x0C ->
          ctx.no_modrm <- true;
          parse_op ctx M.OR SzDef32 D._ALIb
      | 0x0D ->
          ctx.no_modrm <- true;
          parse_op ctx M.OR SzDef32 D._RGvSIz
      | 0x0E ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.PUSH SzInv64 (D._ORSR R.CS)
      | 0x10 -> parse_op ctx M.ADC SzDef32 D._EbGb
      | 0x11 -> parse_op ctx M.ADC SzDef32 D._EvGv
      | 0x12 -> parse_op ctx M.ADC SzDef32 D._GbEb
      | 0x13 -> parse_op ctx M.ADC SzDef32 D._GvEv
      | 0x14 ->
          ctx.no_modrm <- true;
          parse_op ctx M.ADC SzDef32 D._ALIb
      | 0x15 ->
          ctx.no_modrm <- true;
          parse_op ctx M.ADC SzDef32 D._RGvSIz
      | 0x16 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.PUSH SzInv64 (D._ORSR R.SS)
      | 0x17 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.POP SzInv64 (D._ORSR R.SS)
      | 0x18 -> parse_op ctx M.SBB SzDef32 D._EbGb
      | 0x19 -> parse_op ctx M.SBB SzDef32 D._EvGv
      | 0x1A -> parse_op ctx M.SBB SzDef32 D._GbEb
      | 0x1B -> parse_op ctx M.SBB SzDef32 D._GvEv
      | 0x1C ->
          ctx.no_modrm <- true;
          parse_op ctx M.SBB SzDef32 D._ALIb
      | 0x1D ->
          ctx.no_modrm <- true;
          parse_op ctx M.SBB SzDef32 D._RGvSIz
      | 0x1E ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.PUSH SzInv64 (D._ORSR R.DS)
      | 0x1F ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.POP SzInv64 (D._ORSR R.DS)
      | 0x20 -> parse_op ctx M.AND SzDef32 D._EbGb
      | 0x21 -> parse_op ctx M.AND SzDef32 D._EvGv
      | 0x22 -> parse_op ctx M.AND SzDef32 D._GbEb
      | 0x23 -> parse_op ctx M.AND SzDef32 D._GvEv
      | 0x24 ->
          ctx.no_modrm <- true;
          parse_op ctx M.AND SzDef32 D._ALIb
      | 0x25 ->
          ctx.no_modrm <- true;
          parse_op ctx M.AND SzDef32 D._RGvSIz
      | 0x27 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.DAA SzInv64 [||]
      | 0x28 -> parse_op ctx M.SUB SzDef32 D._EbGb
      | 0x29 -> parse_op ctx M.SUB SzDef32 D._EvGv
      | 0x2A -> parse_op ctx M.SUB SzDef32 D._GbEb
      | 0x2B -> parse_op ctx M.SUB SzDef32 D._GvEv
      | 0x2C ->
          ctx.no_modrm <- true;
          parse_op ctx M.SUB SzDef32 D._ALIb
      | 0x2D ->
          ctx.no_modrm <- true;
          parse_op ctx M.SUB SzDef32 D._RGvSIz
      | 0x2F ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.DAS SzInv64 [||]
      | 0x30 -> parse_op ctx M.XOR SzDef32 D._EbGb
      | 0x31 -> parse_op ctx M.XOR SzDef32 D._EvGv
      | 0x32 -> parse_op ctx M.XOR SzDef32 D._GbEb
      | 0x33 -> parse_op ctx M.XOR SzDef32 D._GvEv
      | 0x34 ->
          ctx.no_modrm <- true;
          parse_op ctx M.XOR SzDef32 D._ALIb
      | 0x35 ->
          ctx.no_modrm <- true;
          parse_op ctx M.XOR SzDef32 D._RGvSIz
      | 0x37 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.AAA SzInv64 [||]
      | 0x38 -> parse_op ctx M.CMP SzDef32 D._EbGb
      | 0x39 -> parse_op ctx M.CMP SzDef32 D._EvGv
      | 0x3A -> parse_op ctx M.CMP SzDef32 D._GbEb
      | 0x3B -> parse_op ctx M.CMP SzDef32 D._GvEv
      | 0x3C ->
          ctx.no_modrm <- true;
          parse_op ctx M.CMP SzDef32 D._ALIb
      | 0x3D ->
          ctx.no_modrm <- true;
          parse_op ctx M.CMP SzDef32 D._RGvSIz
      | 0x3F ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.AAS SzInv64 [||]
      | 0x40 | 0x41 | 0x42 | 0x43 | 0x44 | 0x45 | 0x46 | 0x47 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else (
            ctx.no_modrm <- true;
            parse_op ctx M.INC SzInv64 (D._RGz (b land 7) false) )
      | 0x48 | 0x49 | 0x4A | 0x4B | 0x4C | 0x4D | 0x4E | 0x4F ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else (
            ctx.no_modrm <- true;
            parse_op ctx M.DEC SzInv64 (D._RGz (b land 7) false) )
      | 0x50 | 0x51 | 0x52 | 0x53 | 0x54 | 0x55 | 0x56 | 0x57 ->
          ctx.no_modrm <- true;
          parse_op ctx M.PUSH SzDef64 (D._RGv (b land 7))
      | 0x58 | 0x59 | 0x5A | 0x5B | 0x5C | 0x5D | 0x5E | 0x5F ->
          ctx.no_modrm <- true;
          parse_op ctx M.POP SzDef64 (D._RGv (b land 7))
      | 0x60 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else if ctx.prefix.has_operand_size_override then
            parse_op ctx M.PUSHA SzInv64 [||]
          else parse_op ctx M.PUSHAD SzInv64 [||]
      | 0x61 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else if ctx.prefix.has_operand_size_override then
            parse_op ctx M.POPA SzInv64 [||]
          else parse_op ctx M.POPAD SzInv64 [||]
      | 0x62 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.BOUND SzInv64 D._GvMa
      | 0x63 ->
          if Mode.(equal ctx.mode AMD64) then
            let desc =
              match ctx.rex with
              | Some _ -> D._GvEd
              | None -> D._GvEv
            in
            parse_op ctx M.MOVSXD SzDef32 desc
          else parse_op ctx M.ARPL SzInv64 D._EwGw
      | 0x68 -> parse_op ctx M.PUSH SzDef64 D._SIz
      | 0x69 -> parse_op ctx M.IMUL SzDef32 D._GvEvSIz
      | 0x6A -> parse_op ctx M.PUSH SzDef64 D._SIb
      | 0x6B -> parse_op ctx M.IMUL SzDef32 D._GvEvSIb
      | 0x6C -> parse_op ctx M.INSB SzDef32 [||]
      | 0x6D ->
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.INSW SzDef32 [||]
          else parse_op ctx M.INSD SzDef32 [||]
      | 0x6E -> parse_op ctx M.OUTSB SzDef32 [||]
      | 0x6F ->
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.OUTSW SzDef32 [||]
          else parse_op ctx M.OUTSD SzDef32 [||]
      | 0x70 -> parse_op ctx M.JO Sz64 D._Jb
      | 0x71 -> parse_op ctx M.JNO Sz64 D._Jb
      | 0x72 -> parse_op ctx M.JB Sz64 D._Jb
      | 0x73 -> parse_op ctx M.JNB Sz64 D._Jb
      | 0x74 -> parse_op ctx M.JZ Sz64 D._Jb
      | 0x75 -> parse_op ctx M.JNZ Sz64 D._Jb
      | 0x76 -> parse_op ctx M.JBE Sz64 D._Jb
      | 0x77 -> parse_op ctx M.JNBE Sz64 D._Jb
      | 0x78 -> parse_op ctx M.JS Sz64 D._Jb
      | 0x79 -> parse_op ctx M.JNS Sz64 D._Jb
      | 0x7A -> parse_op ctx M.JP Sz64 D._Jb
      | 0x7B -> parse_op ctx M.JNP Sz64 D._Jb
      | 0x7C -> parse_op ctx M.JL Sz64 D._Jb
      | 0x7D -> parse_op ctx M.JNL Sz64 D._Jb
      | 0x7E -> parse_op ctx M.JLE Sz64 D._Jb
      | 0x7F -> parse_op ctx M.JNLE Sz64 D._Jb
      | 0x84 -> parse_op ctx M.TEST SzDef32 D._EbGb
      | 0x85 -> parse_op ctx M.TEST SzDef32 D._EvGv
      | 0x86 -> parse_op ctx M.XCHG SzDef32 D._EbGb
      | 0x87 -> parse_op ctx M.XCHG SzDef32 D._EvGv
      | 0x88 -> parse_op ctx M.MOV SzDef32 D._EbGb
      | 0x89 -> parse_op ctx M.MOV SzDef32 D._EvGv
      | 0x8A -> parse_op ctx M.MOV SzDef32 D._GbEb
      | 0x8B -> parse_op ctx M.MOV SzDef32 D._GvEv
      | 0x8C ->
          let%bind b2 = peek_o ctx 1 in
          let reg = modrm_reg b2 in
          if reg > 5 then Error `BadReg
          else parse_op ctx M.MOV SzDef32 D._EvSw
      | 0x8D -> parse_op ctx M.LEA SzDef32 D._GvMv
      | 0x8E ->
          let%bind b2 = peek_o ctx 1 in
          let reg = modrm_reg b2 in
          if reg = 1 || reg > 5 then Error `BadReg
          else parse_op ctx M.MOV SzDef32 D._SwEw
      | 0x90 ->
          if Array.is_empty ctx.prefixes then parse_op ctx M.NOP SzDef32 [||]
          else if ctx.prefix.group1 = 0xF3 then
            parse_op ctx M.PAUSE SzDef32 [||]
          else (
            ctx.no_modrm <- true;
            parse_op ctx M.XCHG SzDef32 D._RGzRGz )
      | 0x91 | 0x92 | 0x93 | 0x94 | 0x95 | 0x96 | 0x97 ->
          ctx.no_modrm <- true;
          parse_op ctx M.XCHG SzDef32 (D._RGvRGv (b land 7))
      | 0x98 -> (
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.CBW SzDef32 [||]
          else
            match ctx.rex with
            | Some rex when rex.w <> 0 -> parse_op ctx M.CDQE SzDef32 [||]
            | _ -> parse_op ctx M.CWDE SzDef32 [||] )
      | 0x99 -> (
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.CWD SzDef32 [||]
          else
            match ctx.rex with
            | Some rex when rex.w <> 0 -> parse_op ctx M.CQO SzDef32 [||]
            | _ -> parse_op ctx M.CDQ SzDef32 [||] )
      | 0x9A ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.CALLF SzInv64 D._Ap
      | 0x9B -> parse_op ctx M.FWAIT SzDef32 [||]
      | 0x9C ->
          if Mode.(equal ctx.mode AMD64) then
            if ctx.prefix.has_operand_size_override then
              parse_op ctx M.PUSHF SzDef64 [||]
            else parse_op ctx M.PUSHFQ SzDef64 [||]
          else if ctx.prefix.has_operand_size_override then
            parse_op ctx M.PUSHF SzDef32 [||]
          else parse_op ctx M.PUSHFD SzDef32 [||]
      | 0x9D ->
          if Mode.(equal ctx.mode AMD64) then
            if ctx.prefix.has_operand_size_override then
              parse_op ctx M.POPF SzDef64 [||]
            else parse_op ctx M.POPFQ SzDef64 [||]
          else if ctx.prefix.has_operand_size_override then
            parse_op ctx M.POPF SzDef32 [||]
          else parse_op ctx M.POPFD SzDef32 [||]
      | 0x9E -> parse_op ctx M.SAHF SzDef32 [||]
      | 0x9F -> parse_op ctx M.LAHF SzDef32 [||]
      | 0xA0 ->
          ctx.no_modrm <- true;
          parse_op ctx M.MOV SzDef32 D._ALOb
      | 0xA1 ->
          ctx.no_modrm <- true;
          parse_op ctx M.MOV SzDef32 (D._RGvOv 0 false)
      | 0xA2 -> parse_op ctx M.MOV SzDef32 D._ObAL
      | 0xA3 -> parse_op ctx M.MOV SzDef32 (D._OvRGv 0 false)
      | 0xA4 -> parse_op ctx M.MOVSB SzDef32 [||]
      | 0xA5 -> (
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.MOVSW SzDef32 [||]
          else
            match ctx.rex with
            | Some rex when rex.w <> 0 -> parse_op ctx M.MOVSQ SzDef32 [||]
            | _ -> parse_op ctx M.MOVSD SzDef32 [||] )
      | 0xA6 -> parse_op ctx M.CMPSB SzDef32 D._XbYb
      | 0xA7 -> (
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.CMPSW SzDef32 [||]
          else
            match ctx.rex with
            | Some rex when rex.w <> 0 -> parse_op ctx M.CMPSQ SzDef32 [||]
            | _ -> parse_op ctx M.CMPSD SzDef32 [||] )
      | 0xA8 ->
          ctx.no_modrm <- true;
          parse_op ctx M.TEST SzDef32 D._ALIb
      | 0xA9 ->
          ctx.no_modrm <- true;
          parse_op ctx M.TEST SzDef32 D._RGvSIz
      | 0xAA -> parse_op ctx M.STOSB SzDef32 [||]
      | 0xAB -> (
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.STOSW SzDef32 [||]
          else
            match ctx.rex with
            | Some rex when rex.w <> 0 -> parse_op ctx M.STOSQ SzDef32 [||]
            | _ -> parse_op ctx M.STOSD SzDef32 [||] )
      | 0xAC -> parse_op ctx M.LODSB SzDef32 [||]
      | 0xAD -> (
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.LODSW SzDef32 [||]
          else
            match ctx.rex with
            | Some rex when rex.w <> 0 -> parse_op ctx M.LODSQ SzDef32 [||]
            | _ -> parse_op ctx M.LODSD SzDef32 [||] )
      | 0xAE -> parse_op ctx M.SCASB SzDef32 [||]
      | 0xAF -> (
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.SCASW SzDef32 [||]
          else
            match ctx.rex with
            | Some rex when rex.w <> 0 -> parse_op ctx M.SCASQ SzDef32 [||]
            | _ -> parse_op ctx M.SCASD SzDef32 [||] )
      | 0xB0 | 0xB1 | 0xB2 | 0xB3 | 0xB4 | 0xB5 | 0xB6 | 0xB7 ->
          ctx.no_modrm <- true;
          let%bind desc = desc_for_reg_grp ctx (b land 7) in
          parse_op ctx M.MOV SzDef32 desc
      | 0xB8 | 0xB9 | 0xBA | 0xBB | 0xBC | 0xBD | 0xBE | 0xBF ->
          ctx.no_modrm <- true;
          parse_op ctx M.MOV SzDef32 (D._RGvIv (b land 7))
      | 0xC2 -> parse_op ctx M.RETN Sz64 D._Iw
      | 0xC3 -> parse_op ctx M.RETN Sz64 [||]
      | 0xC4 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.LES SzInv64 D._GzMp
      | 0xC5 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.LDS SzInv64 D._GzMp
      | 0xC8 -> parse_op ctx M.ENTER SzDef32 D._IwIb
      | 0xC9 -> parse_op ctx M.LEAVE SzDef64 [||]
      | 0xCA -> parse_op ctx M.RETF SzDef32 D._Iw
      | 0xCB -> parse_op ctx M.RETF SzDef32 [||]
      | 0xCC -> parse_op ctx M.INT3 SzDef32 [||]
      | 0xCD -> parse_op ctx M.INT SzDef32 D._Ib
      | 0xCE ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.INTO SzInv64 [||]
      | 0xCF -> (
          if ctx.prefix.has_operand_size_override then
            parse_op ctx M.IRETW SzDef32 [||]
          else
            match ctx.rex with
            | Some rex when rex.w <> 0 -> parse_op ctx M.IRETQ SzDef32 [||]
            | _ -> parse_op ctx M.IRETD SzDef32 [||] )
      | 0xD4 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.AAM SzInv64 D._Ib
      | 0xD5 ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.AAD SzInv64 D._Ib
      | 0xD6 -> parse_op ctx M.SALC SzDef32 [||]
      | 0xD7 -> parse_op ctx M.XLAT SzDef32 [||]
      | 0xD8 -> parse_esc_op ctx 0xD8 d8_op_within_00_to_BF d8_over_BF
      | 0xD9 -> parse_esc_op ctx 0xD9 d9_op_within_00_to_BF d9_over_BF
      | 0xDA -> parse_esc_op ctx 0xDA da_op_within_00_to_BF da_over_BF
      | 0xDB -> parse_esc_op ctx 0xDB db_op_within_00_to_BF db_over_BF
      | 0xDC -> parse_esc_op ctx 0xDC dc_op_within_00_to_BF dc_over_BF
      | 0xDD -> parse_esc_op ctx 0xDD dd_op_within_00_to_BF dd_over_BF
      | 0xDE -> parse_esc_op ctx 0xDE de_op_within_00_to_BF de_over_BF
      | 0xDF -> parse_esc_op ctx 0xDF df_op_within_00_to_BF df_over_BF
      | 0xE0 -> parse_op ctx M.LOOPNE Sz64 D._Jb
      | 0xE1 -> parse_op ctx M.LOOPE Sz64 D._Jb
      | 0xE2 -> parse_op ctx M.LOOP Sz64 D._Jb
      | 0xE3 ->
          if Mode.(equal ctx.mode AMD64) then
            if ctx.prefix.has_operand_size_override then
              parse_op ctx M.JECXZ Sz64 D._Jb
            else parse_op ctx M.JRCXZ Sz64 D._Jb
          else if ctx.prefix.has_operand_size_override then
            parse_op ctx M.JCXZ Sz64 D._Jb
          else parse_op ctx M.JECXZ Sz64 D._Jb
      | 0xE4 ->
          ctx.no_modrm <- true;
          parse_op ctx M.IN SzDef32 D._ALIb
      | 0xE5 ->
          ctx.no_modrm <- true;
          parse_op ctx M.IN SzDef32 (D._RGvIb 0 false)
      | 0xE6 ->
          ctx.no_modrm <- true;
          parse_op ctx M.OUT SzDef32 D._IbAL
      | 0xE7 ->
          ctx.no_modrm <- true;
          parse_op ctx M.OUT SzDef32 (D._IbRGv 0 false)
      | 0xE8 -> parse_op ctx M.CALL Sz64 D._Jz
      | 0xE9 -> parse_op ctx M.JMP Sz64 D._Jz
      | 0xEA ->
          if not Mode.(equal ctx.mode IA32) then Error `Invalid
          else parse_op ctx M.JMPF SzInv64 D._Ap
      | 0xEB -> parse_op ctx M.JMP Sz64 D._Jb
      | 0xEC ->
          ctx.no_modrm <- true;
          parse_op ctx M.IN SzDef32 D._ALDX
      | 0xED ->
          ctx.no_modrm <- true;
          parse_op ctx M.IN SzDef32 D._RGzDX
      | 0xEE ->
          ctx.no_modrm <- true;
          parse_op ctx M.OUT SzDef32 D._DXAL
      | 0xEF ->
          ctx.no_modrm <- true;
          parse_op ctx M.OUT SzDef32 D._DXRGz
      | 0xF1 -> parse_op ctx M.INT1 Sz64 [||]
      | 0xF4 -> parse_op ctx M.HLT Sz64 [||]
      | 0xF5 -> parse_op ctx M.CMC Sz64 [||]
      | 0xF8 -> parse_op ctx M.CLC Sz64 [||]
      | 0xF9 -> parse_op ctx M.STC Sz64 [||]
      | 0xFA -> parse_op ctx M.CLI Sz64 [||]
      | 0xFB -> parse_op ctx M.STI Sz64 [||]
      | 0xFC -> parse_op ctx M.CLD Sz64 [||]
      | 0xFD -> parse_op ctx M.STD Sz64 [||]
      (* group *)
      | 0x80 -> parse_grp_opcode ctx `G1 D._EbIb
      | 0x81 -> parse_grp_opcode ctx `G1 D._EvSIz
      | 0x82 -> parse_grp_opcode ctx `G1Inv64 D._EbIb
      | 0x83 -> parse_grp_opcode ctx `G1 D._EvSIb
      | 0x8F -> parse_grp_opcode ctx `G1A D._Ev
      | 0xC0 -> parse_grp_opcode ctx `G2 D._EbIb
      | 0xC1 -> parse_grp_opcode ctx `G2 D._EvIb
      | 0xD0 -> parse_grp_opcode ctx `G2 D._Eb1L
      | 0xD1 -> parse_grp_opcode ctx `G2 D._Ev1L
      | 0xD2 -> parse_grp_opcode ctx `G2 D._EbCL
      | 0xD3 -> parse_grp_opcode ctx `G2 D._EvCL
      | 0xF6 -> parse_grp_opcode ctx `G3A D._Eb
      | 0xF7 -> parse_grp_opcode ctx `G3B D._Ev
      | 0xFE -> parse_grp_opcode ctx `G4 [||]
      | 0xFF -> parse_grp_opcode ctx `G5 [||]
      | 0xC6 -> parse_grp_opcode ctx `G11A D._EbIb
      | 0xC7 -> parse_grp_opcode ctx `G11B D._EvSIz
      (* escape *)
      | 0x0F ->
          let%bind _ = skip ctx in
          parse_two_byte ctx
      | _ -> Error `Invalid
    in
    if ctx.pos = pos then
      let%map _ = skip ctx in
      op
    else return op

  let parse_xop8 ctx =
    let open Result.Let_syntax in
    let pos = ctx.pos in
    let%bind b = peek ctx in
    let%bind op =
      match b with
      | 0x85 -> parse_op ctx M.VPMACSSWW SzDef32 D._VdqHdqWdqLdq
      | 0x86 -> parse_op ctx M.VPMACSSWD SzDef32 D._VdqHdqWdqLdq
      | 0x87 -> parse_op ctx M.VPMACSSDQL SzDef32 D._VdqHdqWdqLdq
      | 0x8E -> parse_op ctx M.VPMACSSDD SzDef32 D._VdqHdqWdqLdq
      | 0x8F -> parse_op ctx M.VPMACSSDQH SzDef32 D._VdqHdqWdqLdq
      | 0x95 -> parse_op ctx M.VPMACSWW SzDef32 D._VdqHdqWdqLdq
      | 0x96 -> parse_op ctx M.VPMACSWD SzDef32 D._VdqHdqWdqLdq
      | 0x97 -> parse_op ctx M.VPMACSDQL SzDef32 D._VdqHdqWdqLdq
      | 0x9E -> parse_op ctx M.VPMACSDD SzDef32 D._VdqHdqWdqLdq
      | 0x9F -> parse_op ctx M.VPMACSDQH SzDef32 D._VdqHdqWdqLdq
      | 0xA2 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPCMOV SzDef32 D._VxHxWxLx
            else parse_op ctx M.VPCMOV SzDef32 D._VxHxLxWx
        | None -> Error `Invalid )
      | 0xA3 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPPERM SzDef32 D._VdqHdqWdqLdq
            else parse_op ctx M.VPPERM SzDef32 D._VdqHdqLdqWdq
        | None -> Error `Invalid )
      | 0xA6 -> parse_op ctx M.VPMADCSSWD SzDef32 D._VdqHdqWdqLdq
      | 0xB6 -> parse_op ctx M.VPMADCSWD SzDef32 D._VdqHdqWdqLdq
      | 0xC0 -> parse_op ctx M.VPROTB SzDef32 D._VdqWdqIb
      | 0xC1 -> parse_op ctx M.VPROTW SzDef32 D._VdqWdqIb
      | 0xC2 -> parse_op ctx M.VPROTD SzDef32 D._VdqWdqIb
      | 0xC3 -> parse_op ctx M.VPROTQ SzDef32 D._VdqWdqIb
      | 0xCC -> parse_op ctx M.VPCOMB SzDef32 D._VdqHdqWdqIb
      | 0xCD -> parse_op ctx M.VPCOMW SzDef32 D._VdqHdqWdqIb
      | 0xCE -> parse_op ctx M.VPCOMD SzDef32 D._VdqHdqWdqIb
      | 0xCF -> parse_op ctx M.VPCOMQ SzDef32 D._VdqHdqWdqIb
      | 0xEC -> parse_op ctx M.VPCOMUB SzDef32 D._VdqHdqWdqIb
      | 0xED -> parse_op ctx M.VPCOMUW SzDef32 D._VdqHdqWdqIb
      | 0xEE -> parse_op ctx M.VPCOMUD SzDef32 D._VdqHdqWdqIb
      | 0xEF -> parse_op ctx M.VPCOMUQ SzDef32 D._VdqHdqWdqIb
      | _ -> Error `Invalid
    in
    if ctx.pos = pos then
      let%map _ = skip ctx in
      op
    else return op

  let parse_xop9 ctx =
    let open Result.Let_syntax in
    let pos = ctx.pos in
    let%bind b = peek ctx in
    let%bind op =
      match b with
      | 0x01 -> (
          let%bind _ = skip ctx in
          let%bind b2 = peek ctx in
          let r = modrm_reg b2 in
          match r with
          | 1 -> parse_op ctx M.BLCFILL SzDef32 D._ByEy
          | 2 -> parse_op ctx M.BLSFILL SzDef32 D._ByEy
          | 3 -> parse_op ctx M.BLCS SzDef32 D._ByEy
          | 4 -> parse_op ctx M.TZMSK SzDef32 D._ByEy
          | 5 -> parse_op ctx M.BLCIC SzDef32 D._ByEy
          | 6 -> parse_op ctx M.BLSIC SzDef32 D._ByEy
          | 7 -> parse_op ctx M.T1MSKC SzDef32 D._ByEy
          | _ -> Error `Invalid )
      | 0x02 -> (
          let%bind _ = skip ctx in
          let%bind b2 = peek ctx in
          let r = modrm_reg b2 in
          match r with
          | 1 -> parse_op ctx M.BLCMSK SzDef32 D._ByEy
          | 6 -> parse_op ctx M.BLCI SzDef32 D._ByEy
          | _ -> Error `Invalid )
      | 0x12 -> (
          let%bind _ = skip ctx in
          let%bind b2 = peek ctx in
          let r = modrm_reg b2 in
          match r with
          | 0 -> parse_op ctx M.LLWPCB SzDef32 D._Ry
          | 1 -> parse_op ctx M.SLWPCB SzDef32 D._Ry
          | _ -> Error `Invalid )
      | 0x80 -> parse_op ctx M.VFRCZPS SzDef32 D._VxWx
      | 0x81 -> parse_op ctx M.VFRCZPD SzDef32 D._VxWx
      | 0x82 -> parse_op ctx M.VFRCZSS SzDef32 D._VdqWdqq
      | 0x83 -> parse_op ctx M.VFRCZSD SzDef32 D._VssWss
      | 0x84 -> parse_op ctx M.VFRCZSD SzDef32 D._VsdWsd
      | 0x90 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPROTB SzDef32 D._VssWssHss
            else parse_op ctx M.VPROTB SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x91 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPROTW SzDef32 D._VssWssHss
            else parse_op ctx M.VPROTW SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x92 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPROTD SzDef32 D._VssWssHss
            else parse_op ctx M.VPROTD SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x93 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPROTQ SzDef32 D._VssWssHss
            else parse_op ctx M.VPROTQ SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x94 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPSHLB SzDef32 D._VssWssHss
            else parse_op ctx M.VPSHLB SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x95 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPSHLW SzDef32 D._VssWssHss
            else parse_op ctx M.VPSHLW SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x96 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPSHLD SzDef32 D._VssWssHss
            else parse_op ctx M.VPSHLD SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x97 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPSHLQ SzDef32 D._VssWssHss
            else parse_op ctx M.VPSHLQ SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x98 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPSHAB SzDef32 D._VssWssHss
            else parse_op ctx M.VPSHAB SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x99 -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPSHAW SzDef32 D._VssWssHss
            else parse_op ctx M.VPSHAW SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x9A -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPSHAD SzDef32 D._VssWssHss
            else parse_op ctx M.VPSHAD SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0x9B -> (
        match ctx.vrex with
        | Some (w, _, _, _) ->
            if w = 0 then parse_op ctx M.VPSHAQ SzDef32 D._VssWssHss
            else parse_op ctx M.VPSHAQ SzDef32 D._VssHssWss
        | None -> Error `Invalid )
      | 0xC1 -> parse_op ctx M.VPHADDBW SzDef32 D._VdqWdq
      | 0xC2 -> parse_op ctx M.VPHADDBD SzDef32 D._VdqWdq
      | 0xC3 -> parse_op ctx M.VPHADDBQ SzDef32 D._VdqWdq
      | 0xC6 -> parse_op ctx M.VPHADDWD SzDef32 D._VdqWdq
      | 0xC7 -> parse_op ctx M.VPHADDWQ SzDef32 D._VdqWdq
      | 0xCB -> parse_op ctx M.VPHADDDQ SzDef32 D._VdqWdq
      | 0xD1 -> parse_op ctx M.VPHADDUBW SzDef32 D._VdqWdq
      | 0xD2 -> parse_op ctx M.VPHADDUBD SzDef32 D._VdqWdq
      | 0xD3 -> parse_op ctx M.VPHADDUBQ SzDef32 D._VdqWdq
      | 0xD6 -> parse_op ctx M.VPHADDUWD SzDef32 D._VdqWdq
      | 0xD7 -> parse_op ctx M.VPHADDUWQ SzDef32 D._VdqWdq
      | 0xDB -> parse_op ctx M.VPHADDUDQ SzDef32 D._VdqWdq
      | 0xE1 -> parse_op ctx M.VPHSUBBW SzDef32 D._VdqWdq
      | 0xE2 -> parse_op ctx M.VPHSUBWD SzDef32 D._VdqWdq
      | 0xE3 -> parse_op ctx M.VPHSUBDQ SzDef32 D._VdqWdq
      | _ -> Error `Invalid
    in
    if ctx.pos = pos then
      let%map _ = skip ctx in
      op
    else return op

  let parse_xopA ctx =
    let open Result.Let_syntax in
    let pos = ctx.pos in
    let%bind b = peek ctx in
    let%bind op =
      match b with
      | 0x10 -> parse_op ctx M.BEXTR SzDef32 D._GyEyId
      | 0x12 -> (
          let%bind _ = skip ctx in
          let%bind b2 = peek ctx in
          let r = modrm_reg b2 in
          match r with
          | 0 -> parse_op ctx M.LWPINS SzDef32 D._ByEdId
          | 1 -> parse_op ctx M.LWPVAL SzDef32 D._ByEdId
          | _ -> Error `Invalid )
      | _ -> Error `Invalid
    in
    if ctx.pos = pos then
      let%map _ = skip ctx in
      op
    else return op

  let parse_opcode ctx =
    let open Result.Let_syntax in
    let%bind m, sz, descs =
      match ctx.avx with
      | Some (A.Vex vex) -> (
        match vex.map_select with
        | 1 -> parse_two_byte ctx
        | 2 -> parse_three_byte_op1 ctx
        | 3 -> parse_three_byte_op2 ctx
        | _ -> Error `Invalid )
      | Some (A.Xop xop) -> (
        match xop.map_select with
        | 0x8 -> parse_xop8 ctx
        | 0x9 -> parse_xop9 ctx
        | 0xA -> parse_xopA ctx
        | _ -> Error `Invalid )
      | Some (A.Evex evex) -> (
        match evex.map_select with
        | 1 -> parse_two_byte ctx
        | 2 -> parse_three_byte_op1 ctx
        | 3 -> parse_three_byte_op2 ctx
        | _ -> Error `Invalid )
      | Some (A.Mvex mvex) -> Error `Unimpl
      | None -> parse_one_byte ctx
    in
    match m with
    | M.Invalid -> Error `Invalid
    | _ ->
        ctx.mnemonic <- m;
        return (sz, descs)

  let parse_opr_for_direct_jmp ctx ins_size =
    let open Result.Let_syntax in
    let size = ins_size.mem_addr_size in
    match size with
    | 16 | 32 -> (
        let%bind _ = parse_immediate ctx size false false in
        let%bind _ = parse_immediate ctx 16 false false in
        match ctx.imm with
        | Some (X86.Immediate.Two (offset, segment)) ->
            let segment = Int64.(segment.value land 0xFFFFL) in
            let offset = Int64.(offset.value land 0xFFFFFFFFL) in
            let value =
              O.Value.(
                Pointer
                  { segment= Int.of_int64_trunc segment
                  ; offset= Int32.of_int64_trunc offset })
            in
            Ok O.{value; size= size + 16}
        | _ -> Error `Invalid )
    | _ -> Error `Invalid

  let imm_size ctx eff_sz = function
    | DS.B -> Ok 8
    | DS.D -> Ok 32
    | DS.V -> Ok eff_sz
    | DS.W -> Ok 16
    | DS.Z -> if eff_sz = 64 || eff_sz = 32 then Ok 32 else Ok eff_sz
    | _ -> Error `Invalid

  let parse_opr_for_rel_jmp ctx ins_size kind =
    let open Result.Let_syntax in
    let%bind size = imm_size ctx ins_size.mem_op_size kind in
    let signed = true in
    let relative = true in
    let%bind _ = parse_immediate ctx size signed relative in
    match ctx.imm with
    | Some (X86.Immediate.One imm) | Some (X86.Immediate.Two (_, imm)) ->
        let value =
          O.Value.(Immediate {value= imm.value; signed; relative})
        in
        Ok O.{value; size}
    | _ -> Error `Invalid

  (* EVEX and MVEX use a compressed 8-bit displacement *)
  let compress_disp ctx disp =
    let scale =
      match ctx.avx with
      | Some (A.Evex evex) -> (
          let vlen = Option.value_exn ctx.vector_length in
          let module T = A.Evex_tupletype in
          let w = A.(evex.w) in
          let bcst = A.(evex.b' = 1) in
          match A.(evex.tuple_type) with
          | Some T.FV when not bcst -> (
            match A.(evex.input_size) with
            | Some 32 when w = 0 -> (
              match vlen with
              | 128 -> Some 16L
              | 256 -> Some 32L
              | 512 -> Some 64L
              | _ -> None )
            | Some 64 when w = 1 -> (
              match vlen with
              | 128 -> Some 8L
              | 256 -> Some 16L
              | 512 -> Some 32L
              | _ -> None )
            | _ -> None )
          | Some T.FV -> (
            match A.(evex.input_size) with
            | Some 32 when w = 0 -> Some 4L
            | Some 64 when w = 1 -> Some 8L
            | _ -> None )
          | Some T.HV when not bcst -> (
            match A.(evex.input_size) with
            | Some 32 when w = 0 -> (
              match vlen with
              | 128 -> Some 8L
              | 256 -> Some 16L
              | 512 -> Some 32L
              | _ -> None )
            | _ -> None )
          | Some T.HV -> (
            match A.(evex.input_size) with
            | Some 32 when w = 0 -> Some 4L
            | _ -> None )
          | Some T.FVM -> (
            match vlen with
            | 128 -> Some 16L
            | 256 -> Some 32L
            | 512 -> Some 64L
            | _ -> None )
          | Some T.T1S -> (
            match A.(evex.input_size) with
            | Some 8 -> Some 1L
            | Some 16 -> Some 2L
            | Some 32 when w = 0 -> Some 4L
            | Some 64 when w = 1 -> Some 8L
            | _ -> None )
          | Some T.T1F -> (
            match A.(evex.input_size) with
            | Some 32 -> Some 4L
            | Some 64 -> Some 8L
            | _ -> None )
          | Some T.T2 -> (
            match A.(evex.input_size) with
            | Some 32 when w = 0 -> Some 8L
            | Some 64 when w = 1 && vlen <> 128 -> Some 16L
            | _ -> None )
          | Some T.T4 -> (
            match A.(evex.input_size) with
            | Some 32 when w = 0 && vlen <> 128 -> Some 16L
            | Some 64 when w = 1 && vlen = 512 -> Some 32L
            | _ -> None )
          | Some T.T8 -> (
            match A.(evex.input_size) with
            | Some 32 when w = 0 && vlen = 512 -> Some 32L
            | _ -> None )
          | Some T.HVM -> (
            match vlen with
            | 128 -> Some 8L
            | 256 -> Some 16L
            | 512 -> Some 32L
            | _ -> None )
          | Some T.QVM -> (
            match vlen with
            | 128 -> Some 4L
            | 256 -> Some 8L
            | 512 -> Some 16L
            | _ -> None )
          | Some T.OVM -> (
            match vlen with
            | 128 -> Some 2L
            | 256 -> Some 4L
            | 512 -> Some 8L
            | _ -> None )
          | Some T.M128 -> Some 16L
          | Some T.DUP -> (
            match vlen with
            | 128 -> Some 8L
            | 256 -> Some 32L
            | 512 -> Some 64L
            | _ -> None )
          | _ -> None )
      | Some (A.Mvex _) -> None (* not implemented *)
      | _ -> None
    in
    Option.(
      value ~default:disp (scale >>| fun scale -> Int64.(disp * scale)))

  let effective_seg_reg ctx =
    match ctx.prefix.effective_segment with
    | 0x2E -> Some R.CS
    | 0x36 -> Some R.SS
    | 0x3E -> Some R.DS
    | 0x26 -> Some R.ES
    | 0x64 -> Some R.FS
    | 0x65 -> Some R.GS
    | _ -> None

  let make_seg segment base =
    if Option.is_none segment then
      match base with
      | Some reg -> (
        match reg with
        | R.RBP | R.EBP | R.BP | R.RSP | R.ESP | R.SP -> Some R.SS
        | _ -> Some R.DS )
      | None -> Some R.DS
    else segment

  let parse_opr_mem ctx ins_size segment base index scale disp_size =
    let open Result.Let_syntax in
    let size = ins_size.mem_op_size in
    let segment = make_seg segment base in
    match disp_size with
    | Some disp_size -> (
        let%bind _ = parse_displacement ctx disp_size in
        match ctx.disp with
        | Some disp ->
            let value =
              match disp_size with
              | 8 -> compress_disp ctx disp.value
              | _ -> disp.value
            in
            let displacement = Some (value, disp_size) in
            let value =
              O.Value.(Memory {segment; base; index; scale; displacement})
            in
            Ok O.{value; size}
        | None -> Error `Invalid )
    | None ->
        let value =
          O.Value.(Memory {segment; base; index; scale; displacement= None})
        in
        Ok O.{value; size}

  let parse_opr_imm ctx ins_size kind signed =
    let open Result.Let_syntax in
    let%bind size = imm_size ctx ins_size.mem_op_size kind in
    let relative = false in
    let%bind _ = parse_immediate ctx size signed relative in
    match ctx.imm with
    | Some (X86.Immediate.One imm) | Some (X86.Immediate.Two (_, imm)) ->
        let value = imm.value in
        let value = O.Value.(Immediate {value; signed; relative}) in
        Ok O.{value; size}
    | None -> Error `Invalid

  let tbl_mem16 =
    [| (* Mod 00b *)
       (Some R.BX, Some (R.SI, 1), None)
     ; (Some R.BX, Some (R.DI, 1), None)
     ; (Some R.BP, Some (R.SI, 1), None)
     ; (Some R.BP, Some (R.DI, 1), None)
     ; (Some R.SI, None, None)
     ; (Some R.DI, None, None)
     ; (None, None, Some 16)
     ; (Some R.BX, None, None)
     ; (* Mod 01b *)
       (Some R.BX, Some (R.SI, 1), Some 8)
     ; (Some R.BX, Some (R.DI, 1), Some 8)
     ; (Some R.BP, Some (R.SI, 1), Some 8)
     ; (Some R.BP, Some (R.DI, 1), Some 8)
     ; (Some R.SI, None, Some 8)
     ; (Some R.DI, None, Some 8)
     ; (Some R.BP, None, Some 8)
     ; (Some R.BX, None, Some 8)
     ; (* Mod 10b *)
       (Some R.BX, Some (R.SI, 1), Some 16)
     ; (Some R.BX, Some (R.DI, 1), Some 16)
     ; (Some R.BP, Some (R.SI, 1), Some 16)
     ; (Some R.BP, Some (R.DI, 1), Some 16)
     ; (Some R.SI, None, Some 16)
     ; (Some R.DI, None, Some 16)
     ; (Some R.BP, None, Some 16)
     ; (Some R.BX, None, Some 16) |]

  let tbl_mem32 =
    [| (* Mod 00b *)
       (Some (Some 0), None)
     ; (Some (Some 1), None)
     ; (Some (Some 2), None)
     ; (Some (Some 3), None)
     ; (None, None)
     ; (Some None, Some 32)
     ; (Some (Some 6), None)
     ; (Some (Some 7), None)
     ; (* Mod 01b *)
       (Some (Some 0), Some 8)
     ; (Some (Some 1), Some 8)
     ; (Some (Some 2), Some 8)
     ; (Some (Some 3), Some 8)
     ; (None, Some 8)
     ; (Some (Some 5), Some 8)
     ; (Some (Some 6), Some 8)
     ; (Some (Some 7), Some 8)
     ; (* Mod 10b *)
       (Some (Some 0), Some 32)
     ; (Some (Some 1), Some 32)
     ; (Some (Some 2), Some 32)
     ; (Some (Some 3), Some 32)
     ; (None, Some 32)
     ; (Some (Some 5), Some 32)
     ; (Some (Some 6), Some 32)
     ; (Some (Some 7), Some 32) |]

  let tbl_scale = [|1; 2; 4; 8|]

  let parse_mem16 ctx ins_size segment =
    match ctx.modrm with
    | None -> Error `Invalid
    | Some modrm ->
        let mrm = (modrm.mod' lsl 3) lor modrm.rm in
        let base, si, disp_size = tbl_mem16.(mrm) in
        let scale, index =
          match si with
          | Some (index, scale) -> (scale, Some index)
          | None -> (0, None)
        in
        parse_opr_mem ctx ins_size segment base index scale disp_size

  let scaled_index ctx ins_size s i rex =
    let has_rex_x =
      match ctx.rex with
      | Some rex when rex.x <> 0 -> true
      | _ -> false
    in
    if i = 4 && not has_rex_x then Ok None
    else
      let open Result.Let_syntax in
      let%bind r = find_reg ctx ins_size.mem_addr_size D.SIBIdx i rex in
      Ok (Some (r, tbl_scale.(s)))

  let base_reg ctx ins_size b rex =
    match ctx.modrm with
    | None -> Error `Invalid
    | Some modrm ->
        if b = 5 && modrm.mod' = 0 then Ok None
        else
          let open Result.Let_syntax in
          let%bind r = find_reg ctx ins_size.mem_addr_size D.SIBBase b rex in
          Ok (Some r)

  let sib_displacement_size ctx disp_size b =
    match ctx.modrm with
    | None -> Error `Invalid
    | Some modrm -> (
      match disp_size with
      | Some disp_size -> Ok disp_size
      | None when modrm.mod' = 0 && b = 5 -> Ok 32
      | None when modrm.mod' = 1 && b = 5 -> Ok 8
      | None when modrm.mod' = 2 && b = 5 -> Ok 32
      | _ -> Ok 0 )

  let parse_opr_mem_with_sib ctx ins_size segment opr_size disp_size =
    let open Result.Let_syntax in
    let%bind b = next ctx in
    let%bind _ = parse_sib ctx b (ctx.pos - 1) in
    match ctx.sib with
    | None -> Error `Invalid
    | Some sib ->
        let%bind disp_size = sib_displacement_size ctx disp_size sib.base in
        let rex =
          match ctx.rex with
          | Some rex -> Some (rex.w, rex.r, rex.x, rex.b)
          | None -> None
        in
        let%bind base = base_reg ctx ins_size sib.base rex in
        let%bind si = scaled_index ctx ins_size sib.scale sib.index rex in
        let index, scale =
          match si with
          | Some (index, scale) -> (Some index, scale)
          | None -> (None, 0)
        in
        let%bind displacement =
          match disp_size with
          | 0 -> Ok None
          | disp_size -> (
              let%bind _ = parse_displacement ctx disp_size in
              match ctx.disp with
              | Some disp ->
                  let value =
                    match disp_size with
                    | 8 -> compress_disp ctx disp.value
                    | _ -> disp.value
                  in
                  Ok (Some (value, disp_size))
              | None -> Error `Invalid )
        in
        let segment = make_seg segment base in
        let value =
          O.Value.(Memory {segment; base; index; scale; displacement})
        in
        Ok O.{value; size= opr_size}

  let parse_opr_rip_relative_mem ctx ins_size segment disp_size =
    match ctx.mode with
    | IA32 -> parse_opr_mem ctx ins_size segment None None 0 disp_size
    | AMD64 ->
        if ctx.prefix.has_address_size_override then
          parse_opr_mem ctx ins_size segment (Some R.EIP) None 0 disp_size
        else parse_opr_mem ctx ins_size segment (Some R.RIP) None 0 disp_size

  let base_rm_reg ctx ins_size grp =
    find_reg ctx ins_size.mem_addr_size D.BaseRM grp (select_rex ctx)

  let parse_mem32 ctx ins_size segment opr_size =
    match ctx.modrm with
    | None -> Error `Invalid
    | Some modrm -> (
        let open Result.Let_syntax in
        let mrm = (modrm.mod' lsl 3) lor modrm.rm in
        match tbl_mem32.(mrm) with
        | Some None, disp_size ->
            parse_opr_rip_relative_mem ctx ins_size segment disp_size
        | Some (Some b), disp_size ->
            let%bind r = base_rm_reg ctx ins_size b in
            parse_opr_mem ctx ins_size segment (Some r) None 0 disp_size
        | None, disp_size ->
            parse_opr_mem_with_sib ctx ins_size segment opr_size disp_size )

  let parse_memory ctx ins_size segment =
    if ins_size.mem_addr_size = 16 then parse_mem16 ctx ins_size segment
    else parse_mem32 ctx ins_size segment ins_size.mem_op_size

  let parse_reg ctx ins_size grp sz attr =
    let open Result.Let_syntax in
    let%map r = find_reg ctx sz attr grp (select_rex ctx) in
    let value = O.Value.Register r in
    O.{value; size= sz}

  let parse_mem_or_reg ctx ins_size segment =
    match ctx.modrm with
    | None -> Error `Invalid
    | Some modrm ->
        if modrm.mod' = 0b11 then
          parse_reg ctx ins_size modrm.rm ins_size.mem_reg_size D.Mod11
        else parse_memory ctx ins_size segment

  let xmm_reg_tbl =
    R.
      [| XMM0
       ; XMM1
       ; XMM2
       ; XMM3
       ; XMM4
       ; XMM5
       ; XMM6
       ; XMM7
       ; XMM8
       ; XMM9
       ; XMM10
       ; XMM11
       ; XMM12
       ; XMM13
       ; XMM14
       ; XMM15
       ; XMM16
       ; XMM17
       ; XMM18
       ; XMM19
       ; XMM20
       ; XMM21
       ; XMM22
       ; XMM23
       ; XMM24
       ; XMM25
       ; XMM26
       ; XMM27
       ; XMM28
       ; XMM29
       ; XMM30
       ; XMM31 |]

  let ymm_reg_tbl =
    R.
      [| YMM0
       ; YMM1
       ; YMM2
       ; YMM3
       ; YMM4
       ; YMM5
       ; YMM6
       ; YMM7
       ; YMM8
       ; YMM9
       ; YMM10
       ; YMM11
       ; YMM12
       ; YMM13
       ; YMM14
       ; YMM15
       ; YMM16
       ; YMM17
       ; YMM18
       ; YMM19
       ; YMM20
       ; YMM21
       ; YMM22
       ; YMM23
       ; YMM24
       ; YMM25
       ; YMM26
       ; YMM27
       ; YMM28
       ; YMM29
       ; YMM30
       ; YMM31 |]

  let zmm_reg_tbl =
    R.
      [| ZMM0
       ; ZMM1
       ; ZMM2
       ; ZMM3
       ; ZMM4
       ; ZMM5
       ; ZMM6
       ; ZMM7
       ; ZMM8
       ; ZMM9
       ; ZMM10
       ; ZMM11
       ; ZMM12
       ; ZMM13
       ; ZMM14
       ; ZMM15
       ; ZMM16
       ; ZMM17
       ; ZMM18
       ; ZMM19
       ; ZMM20
       ; ZMM21
       ; ZMM22
       ; ZMM23
       ; ZMM24
       ; ZMM25
       ; ZMM26
       ; ZMM27
       ; ZMM28
       ; ZMM29
       ; ZMM30
       ; ZMM31 |]

  let select_vvvv ctx =
    match ctx.avx with
    | Some (A.Vex vex) -> Ok (lnot vex.vvvv land 0x0F)
    | Some (A.Xop xop) -> Ok (lnot xop.vvvv land 0x0F)
    | Some (A.Evex evex) ->
        Ok (lnot evex.vvv land 0x0F lor ((lnot evex.v' land 0x01) lsl 4))
    | Some (A.Mvex mvex) ->
        Ok (lnot mvex.vvv land 0x0F lor ((lnot mvex.v' land 0x01) lsl 4))
    | None -> Error `Invalid

  let parse_xmm_reg ctx =
    let open Result.Let_syntax in
    match ctx.vector_length with
    | None -> Error `Invalid
    | Some vlen ->
        let%bind vvvv = select_vvvv ctx in
        let%map r =
          match vlen with
          | 128 -> Ok xmm_reg_tbl.(vvvv)
          | 256 -> Ok ymm_reg_tbl.(vvvv)
          (* Both the Intel and AMD manuals specify that the 'H' operand mode
           * selects a 128/256-bit register based on the 'vvvv' field, but
           * AVX-512 opcodes also make use of this mode, so perhaps
           * the documentation is not up to date *)
          | 512 -> Ok zmm_reg_tbl.(vvvv)
          | _ -> Error `Invalid
        in
        let value = O.Value.(Register r) in
        O.{value; size= vlen}

  let parse_vex_to_gpr ctx ins_size =
    let open Result.Let_syntax in
    let%bind vvvv = select_vvvv ctx in
    let tbl = if vvvv > 7 then tbl_grp1.(vvvv land 7) else tbl_grp2.(vvvv) in
    let%map r = pick_reg ctx ins_size.reg_size tbl in
    let value = O.Value.(Register r) in
    O.{value; size= ins_size.reg_size}

  let mmx_reg_tbl = R.[|MM0; MM1; MM2; MM3; MM4; MM5; MM7; MM7|]

  let parse_mmx_reg n =
    let r = mmx_reg_tbl.(n) in
    let value = O.Value.(Register r) in
    O.{value; size= 64}

  let seg_reg_tbl = R.[|ES; CS; SS; DS; FS; GS|]

  let parse_seg_reg ctx n =
    if n >= Array.length seg_reg_tbl then Error `BadReg
    else
      let r = seg_reg_tbl.(n) in
      let value = O.Value.(Register r) in
      Ok O.{value; size= 16}

  let bnd_reg_tbl = R.[|BND0; BND1; BND2; BND3|]

  let parse_bnd_reg ctx n =
    match ctx.rex with
    | Some rex when rex.b <> 0 -> Error `BadReg
    | _ when n < 0 || n >= Array.length bnd_reg_tbl -> Error `BadReg
    | _ ->
        let r = bnd_reg_tbl.(n) in
        let value = O.Value.(Register r) in
        Ok O.{value; size= 64}

  let test_reg_tbl = R.[|TR0; TR1; TR2; TR3; TR4; TR5; TR6; TR7|]

  let parse_test_reg n =
    let r = test_reg_tbl.(n) in
    let value = O.Value.(Register r) in
    O.{value; size= 32}

  let control_reg_tbl =
    R.
      [| CR0
       ; CR1
       ; CR2
       ; CR3
       ; CR4
       ; CR5
       ; CR6
       ; CR7
       ; CR8
       ; CR9
       ; CR10
       ; CR11
       ; CR12
       ; CR13
       ; CR14
       ; CR15 |]

  let parse_control_reg ctx n =
    let n =
      match ctx.rex with
      | Some rex when rex.r <> 0 -> n + 8
      | _ -> n
    in
    match n with
    | 1 | 5 | 6 | 7 -> Error `BadReg
    | n when n >= 9 && n <= 15 -> Error `BadReg
    | _ -> (
        let r = control_reg_tbl.(n) in
        let value = O.Value.(Register r) in
        match R.width_of r with
        | None -> Error `BadReg
        | Some w ->
            let size =
              if Mode.(equal ctx.mode AMD64) then w.width64 else w.width
            in
            Ok O.{value; size} )

  let debug_reg_tbl =
    R.
      [| DR0
       ; DR1
       ; DR2
       ; DR3
       ; DR4
       ; DR5
       ; DR6
       ; DR7
       ; DR8
       ; DR9
       ; DR10
       ; DR11
       ; DR12
       ; DR13
       ; DR14
       ; DR15 |]

  let parse_debug_reg ctx n =
    match ctx.rex with
    | Some rex when rex.r <> 0 -> Error `BadReg
    | _ -> (
        let r = debug_reg_tbl.(n) in
        let value = O.Value.(Register r) in
        match R.width_of r with
        | None -> Error `BadReg
        | Some w ->
            let size =
              if Mode.(equal ctx.mode AMD64) then w.width64 else w.width
            in
            Ok O.{value; size} )

  let parse_xmm_from_imm ctx ins_size =
    let open Result.Let_syntax in
    let%bind _ = parse_immediate ctx 8 false false in
    (* From the AMD Architecture Programmer's Manual (Appendix A):
     * "YMM or XMM register specified using the most-significant 4 bits
     *  of an 8-bit immediate value. In legacy or compatibility mode
     *  the most significant bit is ignored." *)
    match ctx.imm with
    | None -> Error `Invalid
    | Some (X86.Immediate.One imm) | Some (X86.Immediate.Two (_, imm)) ->
        let idx =
          if Mode.(equal ctx.mode IA32) then
            Int64.((imm.value lsr 4) land 0x07L)
          else Int64.((imm.value lsr 4) land 0x0FL)
        in
        let idx = Int.of_int64_trunc idx in
        let size = ins_size.reg_size in
        let%bind r =
          match size with
          | 128 -> Ok xmm_reg_tbl.(idx)
          | 256 -> Ok ymm_reg_tbl.(idx)
          | _ -> Error `Invalid
        in
        Ok O.{value= Value.(Register r); size}

  let opmask_reg_tbl = R.[|K0; K1; K2; K3; K4; K5; K6; K7|]

  let parse_opmask_reg ctx n =
    let r = opmask_reg_tbl.(n) in
    let value = O.Value.(Register r) in
    match R.width_of r with
    | None -> Error `BadReg
    | Some w ->
        let size =
          if Mode.(equal ctx.mode AMD64) then w.width64 else w.width
        in
        Ok O.{value; size}

  let parse_with_modrm ctx ins_size segment mode =
    match ctx.modrm with
    | None -> Error `Invalid
    | Some modrm -> (
        let is_mem = modrm.mod' <> 3 in
        match mode with
        | (DA.M | DA.MZ) when is_mem -> parse_memory ctx ins_size segment
        | DA.R | DA.U ->
            parse_reg ctx ins_size modrm.rm ins_size.reg_size D.Mod11
        | DA.E | DA.W | DA.WZ -> parse_mem_or_reg ctx ins_size segment
        | DA.E0 when modrm.reg = 0 -> parse_mem_or_reg ctx ins_size segment
        | DA.G | DA.V | DA.VZ ->
            parse_reg ctx ins_size modrm.reg ins_size.reg_size D.Bits
        | DA.B -> parse_vex_to_gpr ctx ins_size
        | DA.C -> parse_control_reg ctx modrm.reg
        | DA.D -> parse_debug_reg ctx modrm.reg
        | DA.H -> parse_xmm_reg ctx
        | DA.KM -> parse_opmask_reg ctx modrm.rm
        | DA.KR -> parse_opmask_reg ctx modrm.reg
        | DA.P -> Ok (parse_mmx_reg modrm.reg)
        | DA.S -> parse_seg_reg ctx modrm.reg
        | DA.T -> Ok (parse_test_reg modrm.reg)
        | DA.N -> Ok (parse_mmx_reg modrm.rm)
        | DA.Q when is_mem -> parse_memory ctx ins_size segment
        | DA.Q -> Ok (parse_mmx_reg modrm.rm)
        | DA.BndR -> parse_bnd_reg ctx modrm.rm
        | DA.BndM when is_mem -> parse_memory ctx ins_size segment
        | _ -> Error `Invalid )

  let parse_opr_only_disp ctx ins_size segment =
    let size = Some ins_size.mem_addr_size in
    parse_opr_mem ctx ins_size segment None None 0 size

  let parse_operand ctx ins_size segment desc =
    let open Result.Let_syntax in
    match desc with
    | D.RegGrp (grp, _, attr) ->
        let rex =
          match ctx.rex with
          | Some rex -> Some (rex.w, rex.r, rex.x, rex.b)
          | None -> None
        in
        let size = ins_size.reg_size in
        let%map r = find_reg ctx size attr grp rex in
        let value = O.Value.Register r in
        O.{value; size}
    | D.Reg r -> (
        let value = O.Value.Register r in
        match R.width_of r with
        | None -> Error `BadReg
        | Some w ->
            let size =
              if Mode.(equal ctx.mode AMD64) then w.width64 else w.width
            in
            Ok O.{value; size} )
    | D.ImmOne ->
        let value =
          O.Value.(Immediate {value= 1L; signed= false; relative= false})
        in
        Ok O.{value; size= 0}
        (* is it really 0 though? *)
    | D.ModeSize (DA.A, _) -> parse_opr_for_direct_jmp ctx ins_size
    | D.ModeSize (DA.J, kind) -> parse_opr_for_rel_jmp ctx ins_size kind
    | D.ModeSize (DA.L, _) -> parse_xmm_from_imm ctx ins_size
    | D.ModeSize (DA.O, _) -> parse_opr_only_disp ctx ins_size segment
    | D.ModeSize (DA.I, kind) -> parse_opr_imm ctx ins_size kind false
    | D.ModeSize (DA.SI, kind) -> parse_opr_imm ctx ins_size kind true
    | D.ModeSize (mode, _) -> parse_with_modrm ctx ins_size segment mode

  let parse_operands ctx ins_size descs =
    let open Result.Let_syntax in
    let segment =
      match effective_seg_reg ctx with
      | Some seg -> Some seg
      | None -> (
        match ctx.mnemonic with
        | M.LDS -> Some R.DS
        | M.LES -> Some R.ES
        | M.LFS -> Some R.FS
        | M.LGS -> Some R.GS
        | M.LSS -> Some R.SS
        | _ -> None )
    in
    match descs with
    | [||] -> Ok [||]
    | [|o|] ->
        let%map o = parse_operand ctx ins_size segment o in
        [|o|]
    | [|o1; o2|] ->
        let%bind o1 = parse_operand ctx ins_size segment o1 in
        let%map o2 = parse_operand ctx ins_size segment o2 in
        [|o1; o2|]
    | [|o1; o2; o3|] ->
        let%bind o1 = parse_operand ctx ins_size segment o1 in
        let%bind o2 = parse_operand ctx ins_size segment o2 in
        let%map o3 = parse_operand ctx ins_size segment o3 in
        [|o1; o2; o3|]
    | [|o1; o2; o3; o4|] ->
        let%bind o1 = parse_operand ctx ins_size segment o1 in
        let%bind o2 = parse_operand ctx ins_size segment o2 in
        let%bind o3 = parse_operand ctx ins_size segment o3 in
        let%map o4 = parse_operand ctx ins_size segment o4 in
        [|o1; o2; o3; o4|]
    | _ -> Error `Invalid

  let set_additional_avx_info ctx =
    match ctx.avx with
    | Some (A.Evex evex) ->
        let module T = A.Evex_tupletype in
        let vlen = Option.value_exn ctx.vector_length in
        let tuple_type, input_size =
          match ctx.mnemonic with
          | M.VALIGND -> (Some T.FV, Some 32)
          | M.VALIGNQ -> (Some T.FV, Some 64)
          | M.VBLENDMPD -> (Some T.FV, Some 64)
          | M.VBLENDMPS -> (Some T.FV, Some 32)
          | M.VBROADCASTSD -> (Some T.T1S, Some 64)
          | M.VBROADCASTF32X2 -> (Some T.T2, Some 32)
          | M.VBROADCASTF32X4 -> (Some T.T4, Some 32)
          | M.VBROADCASTF32X8 -> (Some T.T8, Some 32)
          | M.VBROADCASTF64X2 -> (Some T.T2, Some 64)
          | M.VBROADCASTF64X4 -> (Some T.T4, Some 64)
          | M.VBROADCASTSS -> (Some T.T1S, Some 32)
          | M.VCOMPRESSPD -> (Some T.T1S, Some 64)
          | M.VCOMPRESSPS -> (Some T.T1S, Some 32)
          | M.VCVTPD2QQ -> (Some T.FV, Some 64)
          | M.VCVTPD2UDQ -> (Some T.FV, Some 64)
          | M.VCVTPD2UQQ -> (Some T.FV, Some 64)
          | M.VCVTPH2PS -> (Some T.HVM, Some 16)
          | M.VCVTPS2PH -> (Some T.HVM, Some 32)
          | M.VCVTPS2QQ -> (Some T.HV, Some 32)
          | M.VCVTPS2UDQ -> (Some T.FV, Some 32)
          | M.VCVTPS2UQQ -> (Some T.HV, Some 32)
          | M.VCVTQQ2PD -> (Some T.FV, Some 64)
          | M.VCVTQQ2PS -> (Some T.FV, Some 64)
          | M.VCVTSD2USI -> (Some T.T1F, Some 64)
          | M.VCVTSS2USI -> (Some T.T1F, Some 32)
          | M.VCVTTPD2QQ -> (Some T.FV, Some 64)
          | M.VCVTTPD2UDQ -> (Some T.FV, Some 64)
          | M.VCVTTPD2UQQ -> (Some T.FV, Some 64)
          | M.VCVTTPS2QQ -> (Some T.HV, Some 32)
          | M.VCVTTPS2UDQ -> (Some T.FV, Some 32)
          | M.VCVTTPS2UQQ -> (Some T.HV, Some 32)
          | M.VCVTTSD2USI -> (Some T.T1F, Some 64)
          | M.VCVTTSS2USI -> (Some T.T1F, Some 32)
          | M.VCVTUDQ2PD -> (Some T.HV, Some 32)
          | M.VCVTUDQ2PS -> (Some T.FV, Some 32)
          | M.VCVTUQQ2PD -> (Some T.FV, Some 64)
          | M.VCVTUQQ2PS -> (Some T.FV, Some 64)
          | M.VCVTUSI2SD | M.VCVTUSI2SS ->
              let size = if A.(evex.w <> 0) then 64 else 32 in
              (Some T.T1S, Some size)
          | M.VDBPSADBW -> (Some T.FVM, Some 8)
          | M.VEXPANDPD -> (Some T.T1S, Some 64)
          | M.VEXPANDPS -> (Some T.T1S, Some 32)
          | M.VEXTRACTF32X4 | M.VEXTRACTI32X4 -> (Some T.T4, Some 32)
          | M.VEXTRACTF32X8 | M.VEXTRACTI32X8 -> (Some T.T8, Some 32)
          | M.VEXTRACTF64X2 | M.VEXTRACTI64X2 -> (Some T.T2, Some 64)
          | M.VEXTRACTF64X4 | M.VEXTRACTI64X4 -> (Some T.T4, Some 64)
          | M.VFIXUPIMMPD -> (Some T.FV, Some 64)
          | M.VFIXUPIMMPS -> (Some T.FV, Some 32)
          | M.VFIXUPIMMSD -> (Some T.T1S, Some 64)
          | M.VFIXUPIMMSS -> (Some T.T1S, Some 32)
          | M.VFMADD132PD when vlen = 128 -> (None, Some 64)
          | M.VFMADD132PD
           |M.VFMADD213PD
           |M.VFMADD231PD
           |M.VFMADDSUB132PD
           |M.VFMADDSUB213PD
           |M.VFMADDSUB231PD
           |M.VFMSUB132PD
           |M.VFMSUB213PD
           |M.VFMSUB231PD
           |M.VFMSUBADD132PD
           |M.VFMSUBADD213PD
           |M.VFMSUBADD231PD
           |M.VFNMADD132PD
           |M.VFNMADD213PD
           |M.VFNMADD231PD
           |M.VFNMSUB132PD
           |M.VFNMSUB213PD
           |M.VFNMSUB231PD -> (Some T.FV, Some 64)
          | M.VFMADD132PS
           |M.VFMADD213PS
           |M.VFMADD231PS
           |M.VFMADDSUB132PS
           |M.VFMADDSUB213PS
           |M.VFMADDSUB231PS
           |M.VFMSUB132PS
           |M.VFMSUB213PS
           |M.VFMSUB231PS
           |M.VFMSUBADD132PS
           |M.VFMSUBADD213PS
           |M.VFMSUBADD231PS
           |M.VFNMADD132PS
           |M.VFNMADD213PS
           |M.VFNMADD231PS
           |M.VFNMSUB132PS
           |M.VFNMSUB213PS
           |M.VFNMSUB231PS -> (Some T.FV, Some 32)
          | M.VFMADD132SD
           |M.VFMADD213SD
           |M.VFMADD231SD
           |M.VFMSUB132SD
           |M.VFMSUB213SD
           |M.VFMSUB231SD
           |M.VFNMADD132SD
           |M.VFNMADD213SD
           |M.VFNMADD231SD
           |M.VFNMSUB132SD
           |M.VFNMSUB213SD
           |M.VFNMSUB231SD -> (Some T.T1S, Some 64)
          | M.VFMADD132SS
           |M.VFMADD213SS
           |M.VFMADD231SS
           |M.VFMSUB132SS
           |M.VFMSUB213SS
           |M.VFMSUB231SS
           |M.VFNMADD132SS
           |M.VFNMADD213SS
           |M.VFNMADD231SS
           |M.VFNMSUB132SS
           |M.VFNMSUB213SS
           |M.VFNMSUB231SS -> (Some T.T1S, Some 32)
          | M.VFPCLASSPD | M.VGETEXPPD | M.VGETMANTPD -> (Some T.FV, Some 64)
          | M.VFPCLASSPS | M.VGETEXPPS | M.VGETMANTPS -> (Some T.FV, Some 32)
          | M.VFPCLASSSD
           |M.VGATHERDPD
           |M.VGATHERQPD
           |M.VGETEXPSD
           |M.VGETMANTSD -> (Some T.T1S, Some 64)
          | M.VFPCLASSSS
           |M.VGATHERDPS
           |M.VGATHERQPS
           |M.VGETEXPSS
           |M.VGETMANTSS -> (Some T.T1S, Some 32)
          | M.VINSERTF32X4 | M.VINSERTI32X4 -> (Some T.T4, Some 32)
          | M.VINSERTF32X8 | M.VINSERTI32X8 -> (Some T.T8, Some 32)
          | M.VINSERTF64X2 | M.VINSERTI64X2 -> (Some T.T2, Some 64)
          | M.VINSERTF64X4 | M.VINSERTI64X4 -> (Some T.T4, Some 64)
          | M.VMOVDQA32 -> (Some T.FVM, Some 32)
          | M.VMOVDQA64 -> (Some T.FVM, Some 64)
          | M.VMOVDQU8 -> (Some T.FVM, Some 8)
          | M.VMOVDQU16 -> (Some T.FVM, Some 16)
          | M.VMOVDQU32 -> (Some T.FVM, Some 32)
          | M.VMOVDQU64 -> (Some T.FVM, Some 64)
          | M.VPBLENDMB -> (Some T.FVM, Some 8)
          | M.VPBLENDMD -> (Some T.FVM, Some 32)
          | M.VPBLENDMQ -> (Some T.FVM, Some 64)
          | M.VPBLENDMW -> (Some T.FVM, Some 16)
          | M.VPBROADCASTB -> (Some T.T1S, Some 8)
          | M.VPBROADCASTD -> (Some T.T1S, Some 32)
          | M.VPBROADCASTQ -> (Some T.T1S, Some 32)
          | M.VPBROADCASTW -> (Some T.T1S, Some 16)
          | M.VPCMPB | M.VPCMPUB | M.VPERMB | M.VPERMI2B | M.VPERMT2B ->
              (Some T.FVM, Some 8)
          | M.VPCMPD
           |M.VPCMPUD
           |M.VPERMD
           |M.VPERMI2D
           |M.VPERMI2PS
           |M.VPERMILPS
           |M.VPERMPS
           |M.VPERMT2D
           |M.VPERMT2PS
           |M.VPLZCNTD -> (Some T.FV, Some 32)
          | M.VPCMPQ
           |M.VPCMPUQ
           |M.VPERMQ
           |M.VPERMI2Q
           |M.VPERMI2PD
           |M.VPERMILPD
           |M.VPERMPD
           |M.VPERMT2Q
           |M.VPERMT2PD
           |M.VPLZCNTQ
           |M.VPMADD52HUQ
           |M.VPMADD52LUQ -> (Some T.FV, Some 64)
          | M.VPCMPW | M.VPCMPUW | M.VPERMW | M.VPERMI2W | M.VPERMT2W ->
              (Some T.FVM, Some 16)
          | M.VPCOMPRESSD | M.VPEXPANDD | M.VPGATHERDD | M.VPGATHERQD ->
              (Some T.T1S, Some 32)
          | M.VPCOMPRESSQ | M.VPEXPANDQ | M.VPGATHERDQ | M.VPGATHERQQ ->
              (Some T.T1S, Some 64)
          | M.VPCONFLICTD -> (Some T.FV, Some 32)
          | M.VPCONFLICTQ -> (Some T.FV, Some 64)
          | M.VPMOVB2M -> (None, Some 8)
          | M.VPMOVD2M -> (None, Some 32)
          | M.VPMOVQ2M -> (None, Some 64)
          | M.VPMOVW2M -> (None, Some 16)
          | M.VPMOVDB | M.VPMOVSDB | M.VPMOVUSDB -> (Some T.QVM, Some 32)
          | M.VPMOVDW | M.VPMOVSDW | M.VPMOVUSDW -> (Some T.HVM, Some 32)
          | M.VPMOVQB | M.VPMOVSQB | M.VPMOVUSQB -> (Some T.OVM, Some 64)
          | M.VPMOVQD | M.VPMOVSQD | M.VPMOVUSQD -> (Some T.HVM, Some 64)
          | M.VPMOVQW | M.VPMOVSQW | M.VPMOVUSQW -> (Some T.QVM, Some 64)
          | M.VPMOVWB | M.VPMOVSWB | M.VPMOVUSWB -> (Some T.HVM, Some 16)
          | M.VPMULTISHIFTQB -> (Some T.FV, Some 64)
          | M.VPROLD | M.VPROLVD | M.VPRORD | M.VPRORVD ->
              (Some T.FV, Some 32)
          | M.VPROLQ | M.VPROLVQ | M.VPRORQ | M.VPRORVQ ->
              (Some T.FV, Some 64)
          | M.VPSCATTERDD | M.VPSCATTERDQ -> (Some T.T1S, Some 32)
          | M.VPSCATTERQD | M.VPSCATTERQQ -> (Some T.T1S, Some 64)
          | M.VPSLLVD | M.VPSRAVD | M.VPSRLVD -> (Some T.FV, Some 32)
          | M.VPSLLVQ | M.VPSRAVQ | M.VPSRLVQ -> (Some T.FV, Some 64)
          | M.VPSLLVW | M.VPSRAVW | M.VPSRLVW -> (Some T.FVM, Some 16)
          | M.VPTERNLOGD -> (Some T.FV, Some 32)
          | M.VPTERNLOGQ -> (Some T.FV, Some 64)
          | M.VPTESTMB | M.VPTESTNMB -> (Some T.FVM, Some 8)
          | M.VPTESTMD | M.VPTESTNMD -> (Some T.FV, Some 32)
          | M.VPTESTMQ | M.VPTESTNMQ -> (Some T.FV, Some 64)
          | M.VPTESTMW | M.VPTESTNMW -> (Some T.FVM, Some 16)
          | M.VRANGEPD
           |M.VRCP14PD
           |M.VREDUCEPD
           |M.VRNDSCALEPD
           |M.VRSQRT14PD
           |M.VSCALEFPD
           |M.VSHUFF64X2
           |M.VSHUFI64X2 -> (Some T.FV, Some 64)
          | M.VRANGEPS
           |M.VRCP14PS
           |M.VREDUCEPS
           |M.VRNDSCALEPS
           |M.VRSQRT14PS
           |M.VSCALEFPS
           |M.VSHUFF32X4
           |M.VSHUFI32X4 -> (Some T.FV, Some 32)
          | M.VRANGESD
           |M.VRCP14SD
           |M.VREDUCESD
           |M.VRNDSCALESD
           |M.VRSQRT14SD
           |M.VSCALEFSD -> (Some T.T1S, Some 64)
          | M.VRANGESS
           |M.VRCP14SS
           |M.VREDUCESS
           |M.VRNDSCALESS
           |M.VRSQRT14SS
           |M.VSCALEFSS -> (Some T.T1S, Some 32)
          | _ -> (None, None)
        in
        let evex = A.{evex with tuple_type; input_size} in
        ctx.avx <- Some A.(Evex evex)
    | _ -> ()
end

let decode mode bytes =
  let ctx =
    Context.
      { mode
      ; bytes
      ; mnemonic= M.Invalid
      ; pos= 0
      ; prefixes= [||]
      ; rex= None
      ; vrex= None
      ; avx= None
      ; vector_length= None
      ; reg_ops= []
      ; prefix=
          { has_lock= false
          ; group1= 0
          ; group2= 0
          ; has_operand_size_override= false
          ; has_address_size_override= false
          ; effective_segment= 0
          ; mandatory_candidate= 0
          ; offset_lock= 0
          ; offset_group1= 0
          ; offset_group2= 0
          ; offset_operand_size_override= 0
          ; offset_address_size_override= 0
          ; offset_segment= 0
          ; offset_mandatory= 0 }
      ; effective_operand_size= 0
      ; effective_address_size= 0
      ; modrm= None
      ; sib= None
      ; disp= None
      ; imm= None
      ; no_modrm= false
      ; is_3dnow= false }
  in
  let open Result.Let_syntax in
  let%bind _ = Context.parse_optional_prefixes ctx in
  let%bind _ = Context.parse_vex_info ctx in
  let%bind sz, descs = Context.parse_opcode ctx in
  Context.set_additional_avx_info ctx;
  let%bind operands, modrm, sib, displacement, immediate =
    if not (List.is_empty ctx.reg_ops) then
      let%map operands =
        let is64 = Mode.(equal mode AMD64) in
        let rec aux res = function
          | [] -> return (List.rev res |> Array.of_list)
          | r :: rs -> (
            match R.width_of r with
            | None -> Error `BadReg
            | Some w ->
                let size = if is64 then w.width64 else w.width in
                let o = O.{value= Value.Register r; size} in
                aux (o :: res) rs )
        in
        aux [] ctx.reg_ops
      in
      (operands, ctx.modrm, None, None, None)
    else
      let%map operands =
        if Array.is_empty descs then Ok [||]
        else
          let%bind _ =
            if ctx.no_modrm then Ok ()
            else
              match descs.(0) with
              | D.ModeSize (DA.A, _)
               |D.ModeSize (DA.I, _)
               |D.ModeSize (DA.SI, _)
               |D.ModeSize (DA.J, _)
               |D.ModeSize (DA.O, _) -> Ok ()
              | _ ->
                  let%bind b = Context.next ctx in
                  Context.parse_modrm ctx b (ctx.pos - 1)
          in
          Context.parse_operands ctx sz descs
      in
      (operands, ctx.modrm, ctx.sib, ctx.disp, ctx.imm)
  in
  let%bind _ = if ctx.is_3dnow then Context.skip ctx else return () in
  let instr =
    X86.Instruction.
      { mode
      ; mnemonic= ctx.mnemonic
      ; bytes= Bytes.subo bytes ~len:ctx.pos
      ; operands
      ; prefixes= ctx.prefixes
      ; rex= ctx.rex
      ; avx= ctx.avx
      ; modrm
      ; sib
      ; displacement
      ; immediate
      ; eosz= ctx.effective_operand_size
      ; easz= ctx.effective_address_size }
  in
  let%bind _ =
    Result.ok_if_true ~error:`BadLock
      ((not ctx.prefix.has_lock) || X86.Instruction.accepts_lock instr)
  in
  (* effective F0 (LOCK) prefix *)
  ( if ctx.prefix.has_lock then
    let i = ctx.prefix.offset_lock in
    instr.prefixes.(i) <- {kind= P.Effective; value= P.lock} );
  (* effective F3/F2 (REP/REPE/REPNE) prefix *)
  let is_mov_xrelease =
    if M.(equal instr.mnemonic MOV) then
      let op1 = instr.operands.(0).value in
      let op2 = instr.operands.(1).value in
      match (op1, op2) with
      | O.Value.Memory _, O.Value.Register _
       |O.Value.Memory _, O.Value.Immediate _ -> true
      | _ -> false
    else false
  in
  ( if
    (not (Array.is_empty instr.prefixes))
    && ( X86.Instruction.has_rep instr
       || X86.Instruction.has_repe instr
       || X86.Instruction.has_repne instr
       || ctx.prefix.has_lock || is_mov_xrelease )
  then
    let i = ctx.prefix.offset_group1 in
    let value = instr.prefixes.(i).value in
    instr.prefixes.(i) <- {kind= P.Effective; value} );
  (* effective segment override (is this correct?) *)
  ( if ctx.prefix.effective_segment <> 0 then
    let has_mem (operand : O.t) =
      match operand.value with
      | O.Value.Memory _ -> true
      | _ -> false
    in
    if Array.exists instr.operands has_mem || M.(equal instr.mnemonic XLAT)
    then
      let i = ctx.prefix.offset_segment in
      let value = instr.prefixes.(i).value in
      instr.prefixes.(i) <- {kind= P.Effective; value} );
  (* branch hint prefix, only valid for Jcc opcodes
   * from the Intel Developer's Manual (Volume 2, Chapter 2):
   * "Some earlier microarchitectures used these as branch hints,
   *  but recent generations have not and they are reserved
   *  for future hint usage." *)
  ( if M.is_jcc ctx.mnemonic then
    let rec aux i =
      if i >= 0 then
        let value = instr.prefixes.(i).value in
        match value with
        | 0x2E | 0x3E -> instr.prefixes.(i) <- {kind= P.Effective; value}
        | _ -> aux (pred i)
    in
    aux (Array.length instr.prefixes |> pred) );
  return instr

let decode_exn mode bytes =
  match decode mode bytes with
  | Ok instr -> instr
  | Error err -> raise (X86_decode_error err)
