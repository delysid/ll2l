(* this code is borrowed (and extended/improved) from the B2R2 project *)

open Base
module M = X86_mnemonic
module R = X86_register

module A = struct
  type t =
    | A
    | B
    | BndR
    | BndM
    | C
    | D
    | E
    | G
    | H
    | I
    | SI
    | J
    | KM
    | KR
    | L
    | M
    | MZ
    | N
    | O
    | P
    | Q
    | R
    | S
    | T
    | U
    | V
    | VZ
    | W
    | WZ
    | X
    | Y
    | E0
end

module S = struct
  type t =
    | A
    | B
    | Bnd
    | D
    | DB
    | DQA
    | DQ
    | DQB
    | DQD
    | DQDQ
    | DQQ
    | DQQDQ
    | DQW
    | DW
    | DQWD
    | Hsae
    | P
    | PD
    | PI
    | PS
    | PSQ
    | Q
    | QQ
    | QQQ
    | QQQA
    | S
    | SD
    | SDQ
    | SS
    | SSD
    | SSQ
    | V
    | W
    | X
    | XZ
    | Y
    | YB
    | YD
    | YW
    | Z
    | Msk
end

type reg_grp_attr =
  | NoReg
  | Mod11
  | OpREX
  | OpNoREX
  | Bits
  | BaseRM
  | SIBIdx
  | SIBBase

type operand_enc =
  | ModeSize of A.t * S.t
  | Reg of R.t
  | RegGrp of int * S.t * reg_grp_attr
  | ImmOne

let __Ap = ModeSize (A.A, S.P)

let __BNDRbnd = ModeSize (A.BndR, S.Bnd)

let __BNDMbnd = ModeSize (A.BndM, S.Bnd)

let __By = ModeSize (A.B, S.Y)

let __Cd = ModeSize (A.C, S.Y)

let __Dd = ModeSize (A.D, S.D)

let __E0v = ModeSize (A.E0, S.V)

let __Eb = ModeSize (A.E, S.B)

let __Ed = ModeSize (A.E, S.D)

let __Edb = ModeSize (A.E, S.DB)

let __Edw = ModeSize (A.E, S.DW)

let __Edqb = ModeSize (A.E, S.DQB)

let __Edqd = ModeSize (A.E, S.DQD)

let __Edqa = ModeSize (A.E, S.DQA)

let __Ep = ModeSize (A.E, S.P)

let __Eq = ModeSize (A.E, S.Q)

let __Ev = ModeSize (A.E, S.V)

let __Ew = ModeSize (A.E, S.W)

let __Ey = ModeSize (A.E, S.Y)

let __Eyb = ModeSize (A.E, S.YB)

let __Eyd = ModeSize (A.E, S.YD)

let __Eyw = ModeSize (A.E, S.YW)

let __Gb = ModeSize (A.G, S.B)

let __Gd = ModeSize (A.G, S.D)

let __Gdqa = ModeSize (A.G, S.DQA)

let __Gv = ModeSize (A.G, S.V)

let __Gw = ModeSize (A.G, S.W)

let __Gy = ModeSize (A.G, S.Y)

let __Gz = ModeSize (A.G, S.Z)

let __Hdq = ModeSize (A.H, S.DQ)

let __Hpd = ModeSize (A.H, S.PD)

let __Hps = ModeSize (A.H, S.PS)

let __Hqq = ModeSize (A.H, S.QQ)

let __Hqqq = ModeSize (A.H, S.QQQ)

let __Hsd = ModeSize (A.H, S.SD)

let __Hss = ModeSize (A.H, S.SS)

let __Hx = ModeSize (A.H, S.X)

let __SIb = ModeSize (A.SI, S.B)

let __SIv = ModeSize (A.SI, S.V)

let __SIw = ModeSize (A.SI, S.W)

let __SIz = ModeSize (A.SI, S.Z)

let __Jb = ModeSize (A.J, S.B)

let __Jz = ModeSize (A.J, S.Z)

let __KM = ModeSize (A.KM, S.Msk)

let __KR = ModeSize (A.KR, S.Msk)

let __Ldq = ModeSize (A.L, S.DQ)

let __Lx = ModeSize (A.L, S.X)

let __Ma = ModeSize (A.M, S.A)

let __Mb = ModeSize (A.M, S.B)

let __Md = ModeSize (A.M, S.D)

let __Mdq = ModeSize (A.M, S.DQ)

let __Mdqa = ModeSize (A.M, S.DQA)

let __Mdqd = ModeSize (A.M, S.DQD)

let __Mp = ModeSize (A.M, S.P)

let __Mpd = ModeSize (A.M, S.PD)

let __Mps = ModeSize (A.M, S.PS)

let __Mq = ModeSize (A.M, S.Q)

let __Mqq = ModeSize (A.M, S.QQ)

let __Mqqqa = ModeSize (A.M, S.QQQA)

let __Ms = ModeSize (A.M, S.S)

let __Mv = ModeSize (A.M, S.V)

let __Mw = ModeSize (A.M, S.W)

let __Mx = ModeSize (A.M, S.X)

let __My = ModeSize (A.M, S.Y)

let __Mz = ModeSize (A.M, S.Z)

let __MZqqq = ModeSize (A.MZ, S.QQQ)

let __MZxz = ModeSize (A.MZ, S.XZ)

let __Nq = ModeSize (A.N, S.Q)

let __Ob = ModeSize (A.O, S.B)

let __Ov = ModeSize (A.O, S.V)

let __Pd = ModeSize (A.P, S.D)

let __Ppi = ModeSize (A.P, S.PI)

let __Pq = ModeSize (A.P, S.Q)

let __Qd = ModeSize (A.Q, S.D)

let __Qpi = ModeSize (A.Q, S.PI)

let __Qq = ModeSize (A.Q, S.Q)

let __Rd = ModeSize (A.R, S.D)

let __Rq = ModeSize (A.R, S.Q)

let __Rv = ModeSize (A.R, S.V)

let __Ry = ModeSize (A.R, S.Y)

let __Ib = ModeSize (A.I, S.B)

let __Id = ModeSize (A.I, S.D)

let __Iv = ModeSize (A.I, S.V)

let __Iw = ModeSize (A.I, S.W)

let __Iz = ModeSize (A.I, S.Z)

let __Sw = ModeSize (A.S, S.W)

let __Td = ModeSize (A.T, S.D)

let __Udq = ModeSize (A.U, S.DQ)

let __Upd = ModeSize (A.U, S.PD)

let __Ups = ModeSize (A.U, S.PS)

let __Uq = ModeSize (A.U, S.Q)

let __Ux = ModeSize (A.U, S.X)

let __Vdq = ModeSize (A.V, S.DQ)

let __Vpd = ModeSize (A.V, S.PD)

let __Vps = ModeSize (A.V, S.PS)

let __Vq = ModeSize (A.V, S.Q)

let __Vqq = ModeSize (A.V, S.QQ)

let __Vqqq = ModeSize (A.V, S.QQQ)

let __Vsd = ModeSize (A.V, S.SD)

let __Vss = ModeSize (A.V, S.SS)

let __Vx = ModeSize (A.V, S.X)

let __Vy = ModeSize (A.V, S.Y)

let __VZxz = ModeSize (A.VZ, S.XZ)

let __Wd = ModeSize (A.W, S.D)

let __Wdq = ModeSize (A.W, S.DQ)

let __Wdqd = ModeSize (A.W, S.DQD)

let __Wdqdq = ModeSize (A.W, S.DQDQ)

let __Wdqq = ModeSize (A.W, S.DQQ)

let __Wdqqdq = ModeSize (A.W, S.DQQDQ)

let __Wdqw = ModeSize (A.W, S.DQW)

let __Wdqwd = ModeSize (A.W, S.DQWD)

let __Whsae = ModeSize (A.W, S.Hsae)

let __Wpd = ModeSize (A.W, S.PD)

let __Wps = ModeSize (A.W, S.PS)

let __Wpsq = ModeSize (A.W, S.PSQ)

let __Wqq = ModeSize (A.W, S.QQ)

let __Wsd = ModeSize (A.W, S.SD)

let __Wsdq = ModeSize (A.W, S.SDQ)

let __Wss = ModeSize (A.W, S.SS)

let __Wssd = ModeSize (A.W, S.SSD)

let __Wssq = ModeSize (A.W, S.SSQ)

let __Wx = ModeSize (A.W, S.X)

let __Wy = ModeSize (A.W, S.Y)

let __WZxz = ModeSize (A.WZ, S.XZ)

let __Xb = ModeSize (A.X, S.B)

let __Xv = ModeSize (A.X, S.V)

let __Yb = ModeSize (A.Y, S.B)

let __Yv = ModeSize (A.Y, S.V)

let _Ap = [|__Ap|]

let _Dd = [|__Dd|]

let _E0v = [|__E0v|]

let _Eb = [|__Eb|]

let _Ep = [|__Ep|]

let _Ev = [|__Ev|]

let _Ew = [|__Ew|]

let _Ey = [|__Ey|]

let _Gb = [|__Gb|]

let _Gd = [|__Gd|]

let _Gv = [|__Gv|]

let _Gw = [|__Gw|]

let _Gy = [|__Gy|]

let _Gz = [|__Gz|]

let _Ib = [|__Ib|]

let _Iv = [|__Iv|]

let _Iw = [|__Iw|]

let _Iz = [|__Iz|]

let _Jb = [|__Jb|]

let _Jz = [|__Jz|]

let _Ma = [|__Ma|]

let _Mb = [|__Mb|]

let _Mdq = [|__Mdq|]

let _Mp = [|__Mp|]

let _Mq = [|__Mq|]

let _Mqqqa = [|__Mqqqa|]

let _Ms = [|__Ms|]

let _Mv = [|__Mv|]

let _Mw = [|__Mw|]

let _My = [|__My|]

let _Mz = [|__Mz|]

let _Pd = [|__Pd|]

let _Pq = [|__Pq|]

let _Qq = [|__Qq|]

let _Rd = [|__Rd|]

let _Rq = [|__Rq|]

let _Rv = [|__Rv|]

let _Ry = [|__Ry|]

let _SIb = [|__SIb|]

let _SIv = [|__SIv|]

let _SIw = [|__SIw|]

let _SIz = [|__SIz|]

let _Sw = [|__Sw|]

let _Vdq = [|__Vdq|]

let _Vx = [|__Vx|]

let _Wdq = [|__Wdq|]

let _Wdqd = [|__Wdqd|]

let _Wdqq = [|__Wdqq|]

let _Wx = [|__Wx|]

let _ORSR sg = [|Reg sg|]

let _ALDX = [|Reg R.AL; Reg R.DX|]

let _ALIb = [|Reg R.AL; __Ib|]

let _ALOb = [|Reg R.AL; __Ob|]

let _ByEy = [|__By; __Ey|]

let _ByEdId = [|__By; __Ed; __Id|]

let _BNDRbndBNDMbnd = [|__BNDRbnd; __BNDMbnd|]

let _BNDMbndBNDRbnd = [|__BNDMbnd; __BNDRbnd|]

let _BNDRbndEdqa = [|__BNDRbnd; __Edqa|]

let _BNDRbndMdqa = [|__BNDRbnd; __Mdqa|]

let _CdRd = [|__Cd; __Rd|]

let _DdRd = [|__Dd; __Rd|]

let _DXAL = [|Reg R.DX; Reg R.AL|]

let _Eb1L = [|__Eb; ImmOne|]

let _EbCL = [|__Eb; Reg R.CL|]

let _EbGb = [|__Eb; __Gb|]

let _EbIb = [|__Eb; __Ib|]

let _EbVdqIb = [|__Eb; __Vdq; __Ib|]

let _EdVdqIb = [|__Ed; __Vdq; __Ib|]

let _EqVdqIb = [|__Eq; __Vdq; __Ib|]

let _EwVdqIb = [|__Ew; __Vdq; __Ib|]

let _Ev1L = [|__Ev; ImmOne|]

let _EvCL = [|__Ev; Reg R.CL|]

let _EvGv = [|__Ev; __Gv|]

let _EvIb = [|__Ev; __Ib|]

let _EvIz = [|__Ev; __Iz|]

let _EvSIb = [|__Ev; __SIb|]

let _EvSIz = [|__Ev; __SIz|]

let _EvSw = [|__Ev; __Sw|]

let _EwGw = [|__Ew; __Gw|]

let _EyPd = [|__Ey; __Pd|]

let _EyPq = [|__Ey; __Pq|]

let _EyVdq = [|__Ey; __Vdq|]

let _EybVdqIb = [|__Eyb; __Vdq; __Ib|]

let _EydVdqIb = [|__Eyd; __Vdq; __Ib|]

let _EywVdqIb = [|__Eyw; __Vdq; __Ib|]

let _GbEb = [|__Gb; __Eb|]

let _GdEb = [|__Gd; __Eb|]

let _GdEv = [|__Gd; __Ed|]

let _GdEw = [|__Gd; __Ew|]

let _GdEy = [|__Gd; __Ey|]

let _GdNq = [|__Gd; __Nq|]

let _GdUdq = [|__Gd; __Udq|]

let _GdUx = [|__Gd; __Ux|]

let _GdqaMZqqq = [|__Gdqa; __MZqqq|]

let _GvEb = [|__Gv; __Eb|]

let _GvEd = [|__Gv; __Ed|]

let _GvEv = [|__Gv; __Ev|]

let _GvEw = [|__Gv; __Ew|]

let _GvEy = [|__Gv; __Ey|]

let _GvMa = [|__Gv; __Ma|]

let _GvMp = [|__Gv; __Mp|]

let _GvMv = [|__Gv; __Mv|]

let _GwMw = [|__Gw; __Mw|]

let _GyMy = [|__Gy; __My|]

let _GyUdq = [|__Gy; __Udq|]

let _GyUpd = [|__Gy; __Upd|]

let _GyUps = [|__Gy; __Ups|]

let _GyUx = [|__Gy; __Ux|]

let _GyWdq = [|__Gy; __Wdq|]

let _GyWsd = [|__Gy; __Wsd|]

let _GyWsdq = [|__Gy; __Wsdq|]

let _GyWss = [|__Gy; __Wss|]

let _GyWssd = [|__Gy; __Wssd|]

let _GzMp = [|__Gz; __Mp|]

let _IbAL = [|__Ib; Reg R.AL|]

let _IwIb = [|__Iw; __Ib|]

let _KRUx = [|__KR; __Ux|]

let _MbVdqIb = [|__Mb; __Vdq; __Ib|]

let _MdVdqIb = [|__Md; __Vdq; __Ib|]

let _MdqVdq = [|__Mdq; __Vdq|]

let _MdqaBNDRbnd = [|__Mdqa; __BNDRbnd|]

let _MpdVpd = [|__Mpd; __Vpd|]

let _MpsVps = [|__Mps; __Vps|]

let _MqPq = [|__Mq; __Pq|]

let _MqVdq = [|__Mq; __Vdq|]

let _MwGw = [|__Gw; __Mw|]

let _MwVdqIb = [|__Mw; __Vdq; __Ib|]

let _MxVx = [|__Mx; __Vx|]

let _MyGy = [|__My; __Gy|]

let _MZxzVZxz = [|__MZxz; __VZxz|]

let _NqIb = [|__Nq; __Ib|]

let _ObAL = [|__Ob; Reg R.AL|]

let _PdEy = [|__Pd; __Ey|]

let _PpiWdq = [|__Ppi; __Wdq|]

let _PpiWdqq = [|__Ppi; __Wdqq|]

let _PpiWpd = [|__Ppi; __Wpd|]

let _PpiWps = [|__Ppi; __Wps|]

let _PpiWpsq = [|__Ppi; __Wpsq|]

let _PqEy = [|__Pq; __Ey|]

let _PqQd = [|__Pq; __Qd|]

let _PqQq = [|__Pq; __Qq|]

let _PqUdq = [|__Pq; __Udq|]

let _PqWdq = [|__Pq; __Wdq|]

let _QpiWpd = [|__Qpi; __Wpd|]

let _QqPq = [|__Qq; __Pq|]

let _RdCd = [|__Rd; __Cd|]

let _RdDd = [|__Rd; __Dd|]

let _RdTd = [|__Rd; __Td|]

let _SwEw = [|__Sw; __Ew|]

let _TdRd = [|__Td; __Rd|]

let _UdqIb = [|__Udq; __Ib|]

let _VdqEdIb = [|__Vdq; __Ed; __Ib|]

let _VdqEdbIb = [|__Vdq; __Edb; __Ib|]

let _VdqEdqbIb = [|__Vdq; __Edqb; __Ib|]

let _VdqEdqdIb = [|__Vdq; __Edqd; __Ib|]

let _VdqEqIb = [|__Vdq; __Eq; __Ib|]

let _VdqEy = [|__Vdq; __Ey|]

let _VdqMdq = [|__Vdq; __Mdq|]

let _VdqMq = [|__Vdq; __Mq|]

let _VdqNq = [|__Vdq; __Nq|]

let _VdqQq = [|__Vdq; __Qq|]

let _VdqUdq = [|__Vdq; __Udq|]

let _VdqWdq = [|__Vdq; __Wdq|]

let _VdqWdqd = [|__Vdq; __Wdqd|]

let _VdqWdqq = [|__Vdq; __Wdqq|]

let _VdqWdqw = [|__Vdq; __Wdqw|]

let _VpdWpd = [|__Vpd; __Wpd|]

let _VpsWps = [|__Vps; __Wps|]

let _VqqMdq = [|__Vqq; __Mdq|]

let _VsdWsd = [|__Vsd; __Wsd|]

let _VsdWsdq = [|__Vsd; __Wsdq|]

let _VssWss = [|__Vss; __Wss|]

let _VssWssd = [|__Vss; __Wssd|]

let _VxKM = [|__Vx; __KM|]

let _VxMd = [|__Vx; __Md|]

let _VxMdq = [|__Vx; __Mdq|]

let _VxMq = [|__Vx; __Mq|]

let _VxMqq = [|__Vx; __Mqq|]

let _VxMx = [|__Vx; __Mx|]

let _VxWdqdq = [|__Vx; __Wdqdq|]

let _VxWdqq = [|__Vx; __Wdqq|]

let _VxWdqqdq = [|__Vx; __Wdqqdq|]

let _VxWdqwd = [|__Vx; __Wdqwd|]

let _VxWhsae = [|__Vx; __Whsae|]

let _VxWss = [|__Vx; __Wss|]

let _VxWssd = [|__Vx; __Wssd|]

let _VxWssq = [|__Vx; __Wssq|]

let _VxWx = [|__Vx; __Wx|]

let _VxKM = [|__Vx; __KM|]

let _VyEy = [|__Vy; __Ey|]

let _VyWyIb = [|__Vy; __Wy; __Ib|]

let _VZxzWdqd = [|__VZxz; __Wdqd|]

let _VZxzWZxz = [|__VZxz; __WZxz|]

let _WdqdVdq = [|__Wdqd; __Vdq|]

let _WdqqVdq = [|__Wdqq; __Vdq|]

let _WdqwVdq = [|__Wdqw; __Vdq|]

let _WdqVdq = [|__Wdq; __Vdq|]

let _WqqVqqqIb = [|__Wqq; __Vqqq; __Ib|]

let _WpdVpd = [|__Wpd; __Vpd|]

let _WpsVps = [|__Wps; __Vps|]

let _WssdVx = [|__Wssd; __Vx|]

let _WssqVx = [|__Wssq; __Vx|]

let _WssVx = [|__Wss; __Vx|]

let _WxVx = [|__Wx; __Vx|]

let _WZxzVZxz = [|__WZxz; __VZxz|]

let _XbYb = [|__Xb; __Yb|]

let _XvYv = [|__Xv; __Yv|]

let _YbXb = [|__Yb; __Xb|]

let _YvXv = [|__Yv; __Xv|]

let _RegIb r = [|Reg r; __Ib|]

let __RGz rg changeable =
  let attr = if changeable then OpREX else OpNoREX in
  RegGrp (rg, S.Z, attr)

let __RGv rg changeable =
  let attr = if changeable then OpREX else OpNoREX in
  RegGrp (rg, S.V, attr)

let _RGv rg = [|__RGv rg true|]

let _RGz rg rc = [|__RGz rg rc|]

let _RGvOv rg rc = [|__RGv rg rc; __Ov|]

let _OvRGv rg rc = [|__Ov; __RGv rg rc|]

let _RGzRGz = [|__RGz 0 false; __RGz 0 true|]

let _RGvRGv rg2 = [|__RGv 0 false; __RGv rg2 true|]

let _RGvIb rg rc = [|__RGv rg rc; __Ib|]

let _IbRGv rg rc = [|__Ib; __RGv rg rc|]

let _RGvSIz = [|__RGv 0 false; __SIz|]

let _RGvIv rg = [|__RGv rg true; __Iv|]

let _RGvDX = [|__RGv 0 false; Reg R.DX|]

let _DXRGv = [|Reg R.DX; __RGv 0 false|]

let _RGzDX = [|__RGz 0 false; Reg R.DX|]

let _DXRGz = [|Reg R.DX; __RGz 0 false|]

let _EvGvCL = [|__Ev; __Gv; Reg R.CL|]

let _EvGvIb = [|__Ev; __Gv; __Ib|]

let _GdNqIb = [|__Gd; __Nq; __Ib|]

let _GdUdqIb = [|__Gd; __Udq; __Ib|]

let _GvEvIb = [|__Gv; __Ev; __Ib|]

let _GvEvIz = [|__Gv; __Ev; __Iz|]

let _GvEvSIb = [|__Gv; __Ev; __SIb|]

let _GvEvSIz = [|__Gv; __Ev; __SIz|]

let _GyByEy = [|__Gy; __By; __Ey|]

let _GyEyBy = [|__Gy; __Ey; __By|]

let _GyEyIb = [|__Gy; __Ey; __Ib|]

let _GyEyId = [|__Gy; __Ey; __Id|]

let _HxUxIb = [|__Hx; __Ux; __Ib|]

let _KRHxWx = [|__KR; __Hx; __Wx|]

let _MxHxVx = [|__Mx; __Hx; __Vx|]

let _PqEdwIb = [|__Pq; __Edw; __Ib|]

let _PqQqIb = [|__Pq; __Qq; __Ib|]

let _VdqEdwIb = [|__Vdq; __Edw; __Ib|]

let _VdqHdqMdq = [|__Vdq; __Hdq; __Mdq|]

let _VdqHdqMdqd = [|__Vdq; __Hdq; __Mdqd|]

let _VdqHdqMq = [|__Vdq; __Hdq; __Mq|]

let _VdqHdqUdq = [|__Vdq; __Hdq; __Udq|]

let _VdqWdqIb = [|__Vdq; __Wdq; __Ib|]

let _VpdHpdWpd = [|__Vpd; __Hpd; __Wpd|]

let _VpsHpsWps = [|__Vps; __Hps; __Wps|]

let _VqqHqqWqq = [|__Vqq; __Hqq; __Wqq|]

let _VsdHsdEy = [|__Vsd; __Hsd; __Ey|]

let _VsdHsdWsd = [|__Vsd; __Hsd; __Wsd|]

let _VsdHsdWsdIb = [|__Vsd; __Hsd; __Wsd; __Ib|]

let _VsdHsdWsdq = [|__Vsd; __Hsd; __Wsdq|]

let _VsdWsdIb = [|__Vsd; __Wsd; __Ib|]

let _VsdWsdqIb = [|__Vsd; __Wsdq; __Ib|]

let _VssHssEy = [|__Vss; __Hss; __Ey|]

let _VssHssWss = [|__Vss; __Hss; __Wss|]

let _VssHssWssd = [|__Vss; __Hss; __Wssd|]

let _VssWssdIb = [|__Vss; __Wssd; __Ib|]

let _VssWssHss = [|__Vss; __Wss; __Hss|]

let _VxHxMx = [|__Vx; __Hx; __Mx|]

let _VxHxWdq = [|__Vx; __Hx; __Wdq|]

let _VxHxWsd = [|__Vx; __Hx; __Wsd|]

let _VxHxWss = [|__Vx; __Hx; __Wss|]

let _VxHxWx = [|__Vx; __Hx; __Wx|]

let _VxWxIb = [|__Vx; __Wx; __Ib|]

let _WdqVqqIb = [|__Wdq; __Vqq; __Ib|]

let _WdqVxIb = [|__Wdq; __Vx; __Ib|]

let _WpsqVxIb = [|__Wpsq; __Vx; __Ib|]

let _WsdHxVsd = [|__Wsd; __Hx; __Vsd|]

let _WssHxVss = [|__Wss; __Hx; __Vss|]

let _KRHxWxIb = [|__KR; __Hx; __Wx; __Ib|]

let _VdqHdqEdIb = [|__Vdq; __Hdq; __Ed; __Ib|]

let _VdqHdqEdqbIb = [|__Vdq; __Hdq; __Edqb; __Ib|]

let _VdqHdqEdqdIb = [|__Vdq; __Hdq; __Edqd; __Ib|]

let _VdqHdqEdwIb = [|__Vdq; __Hdq; __Edw; __Ib|]

let _VdqHdqEqIb = [|__Vdq; __Hdq; __Eq; __Ib|]

let _VdqHdqLdqWdq = [|__Vdq; __Hdq; __Ldq; __Wdq|]

let _VdqHdqWdqIb = [|__Vdq; __Hdq; __Wdq; __Ib|]

let _VdqHdqWdqLdq = [|__Vdq; __Hdq; __Wdq; __Ldq|]

let _VpsHpsWpsIb = [|__Vps; __Hps; __Wps; __Ib|]

let _VqqHqqWdqIb = [|__Vqq; __Hqq; __Wdq; __Ib|]

let _VqqqHqqqWqqIb = [|__Vqqq; __Hqqq; __Wqq; __Ib|]

let _VxHxWxIb = [|__Vx; __Hx; __Wx; __Ib|]

let _VxHxWdqIb = [|__Vx; __Hx; __Wdq; __Ib|]

let _VxHxLxWx = [|__Vx; __Hx; __Lx; __Wx|]

let _VxHxWxLx = [|__Vx; __Hx; __Wx; __Lx|]

let op_nor_0F10 = M.[|MOVUPS; MOVUPD; MOVSS; MOVSD|]

let op_vex_0F10_mem = M.[|VMOVUPS; VMOVUPD; VMOVSS; VMOVSD|]

let op_vex_0F10_reg = M.[|VMOVUPS; VMOVUPD; VMOVSS; VMOVSD|]

let op_nor_0F11 = M.[|MOVUPS; MOVUPD; MOVSS; MOVSD|]

let op_vex_0F11_mem = M.[|VMOVUPS; VMOVUPD; VMOVSS; VMOVSD|]

let op_vex_0F11_reg = M.[|VMOVUPS; VMOVUPD; VMOVSS; VMOVSD|]

let op_nor_0F12_mem = M.[|MOVLPS; MOVLPD; MOVSLDUP; MOVDDUP|]

let op_nor_0F12_reg = M.[|MOVHLPS; MOVLPD; MOVSLDUP; MOVDDUP|]

let op_vex_0F12_mem = M.[|VMOVLPS; VMOVLPD; VMOVSLDUP; VMOVDDUP|]

let op_vex_0F12_reg = M.[|VMOVHLPS; VMOVLPD; VMOVSLDUP; VMOVDDUP|]

let op_nor_0F13 = M.[|MOVLPS; MOVLPD; Invalid; Invalid|]

let op_vex_0F13 = M.[|VMOVLPS; VMOVLPD; Invalid; Invalid|]

let op_nor_0F14 = M.[|UNPCKLPS; UNPCKLPD; Invalid; Invalid|]

let op_vex_0F14 = M.[|VUNPCKLPS; VUNPCKLPD; Invalid; Invalid|]

let op_nor_0F15 = M.[|UNPCKHPS; UNPCKHPD; Invalid; Invalid|]

let op_vex_0F15 = M.[|VUNPCKHPS; VUNPCKHPD; Invalid; Invalid|]

let op_nor_0F16_mem = M.[|MOVHPS; MOVHPD; MOVSHDUP; Invalid|]

let op_nor_0F16_reg = M.[|MOVLHPS; MOVHPD; MOVSHDUP; Invalid|]

let op_vex_0F16_mem = M.[|VMOVHPS; VMOVHPD; VMOVSHDUP; Invalid|]

let op_vex_0F16_reg = M.[|VMOVLHPS; VMOVHPD; VMOVSHDUP; Invalid|]

let op_nor_0F17 = M.[|MOVHPS; MOVHPD; Invalid; Invalid|]

let op_vex_0F17 = M.[|VMOVHPS; VMOVHPD; Invalid; Invalid|]

let op_nor_0F1A = M.[|BNDLDX; BNDMOV; BNDCL; BNDCU|]

let op_nor_0F1B = M.[|BNDSTX; BNDMOV; BNDMK; BNDCN|]

let op_nor_0F28 = M.[|MOVAPS; MOVAPD; Invalid; Invalid|]

let op_vex_0F28 = M.[|VMOVAPS; VMOVAPD; Invalid; Invalid|]

let op_nor_0F29 = M.[|MOVAPS; MOVAPD; Invalid; Invalid|]

let op_vex_0F29 = M.[|VMOVAPS; VMOVAPS; Invalid; Invalid|]

let op_nor_0F2A = M.[|CVTPI2PS; CVTPI2PD; CVTSI2SS; CVTSI2SD|]

let op_vex_0F2A = M.[|Invalid; Invalid; VCVTSI2SS; VCVTSI2SD|]

let op_nor_0F2B = M.[|MOVNTPS; MOVNTPD; Invalid; Invalid|]

let op_vex_0F2B = M.[|VMOVNTPS; VMOVNTPD; Invalid; Invalid|]

let op_nor_0F2C = M.[|CVTTPS2PI; CVTTPD2PI; CVTTSS2SI; CVTTSD2SI|]

let op_vex_0F2C = M.[|Invalid; Invalid; VCVTTSS2SI; VCVTTSD2SI|]

let op_nor_0F2D = M.[|CVTPS2PI; CVTPD2PI; CVTSS2SI; CVTSD2SI|]

let op_vex_0F2D = M.[|Invalid; Invalid; VCVTSS2SI; VCVTSD2SI|]

let op_nor_0F2E = M.[|UCOMISS; UCOMISD; Invalid; Invalid|]

let op_vex_0F2E = M.[|VUCOMISS; VUCOMISD; Invalid; Invalid|]

let op_nor_0F2F = M.[|COMISS; COMISD; Invalid; Invalid|]

let op_vex_0F2F = M.[|VCOMISS; VCOMISD; Invalid; Invalid|]

let op_nor_0F50 = M.[|MOVMSKPS; MOVMSKPD; Invalid; Invalid|]

let op_vex_0F50 = M.[|VMOVMSKPS; VMOVMSKPD; Invalid; Invalid|]

let op_nor_0F51 = M.[|SQRTPS; SQRTPD; SQRTSS; SQRTSD|]

let op_vex_0F51 = M.[|VSQRTPS; VSQRTPD; VSQRTSS; VSQRTSD|]

let op_nor_0F52 = M.[|RSQRTPS; Invalid; RSQRTSS; Invalid|]

let op_vex_0F52 = M.[|VRSQRTPS; Invalid; VRSQRTSS; Invalid|]

let op_nor_0F53 = M.[|RCPPS; Invalid; RCPSS; Invalid|]

let op_vex_0F53 = M.[|VRCPPS; Invalid; VRCPSS; Invalid|]

let op_nor_0F54 = M.[|ANDPS; ANDPD; Invalid; Invalid|]

let op_vex_0F54 = M.[|VANDPS; VANDPD; Invalid; Invalid|]

let op_nor_0F55 = M.[|ANDNPS; ANDNPD; Invalid; Invalid|]

let op_vex_0F55 = M.[|VANDNPS; VANDNPD; Invalid; Invalid|]

let op_nor_0F56 = M.[|ORPS; ORPD; Invalid; Invalid|]

let op_vex_0F56 = M.[|VORPS; VORPD; Invalid; Invalid|]

let op_nor_0F57 = M.[|XORPS; XORPD; Invalid; Invalid|]

let op_vex_0F57 = M.[|VXORPS; VXORPD; Invalid; Invalid|]

let op_nor_0F58 = M.[|ADDPS; ADDPD; ADDSS; ADDSD|]

let op_vex_0F58 = M.[|VADDPS; VADDPD; VADDSS; VADDSD|]

let op_nor_0F59 = M.[|MULPS; MULPD; MULSS; MULSD|]

let op_vex_0F59 = M.[|VMULPS; VMULPD; VMULSS; VMULSD|]

let op_nor_0F5A = M.[|CVTPS2PD; CVTPD2PS; CVTSS2SD; CVTSD2SS|]

let op_vex_0F5A = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F5B = M.[|CVTDQ2PS; CVTPS2DQ; CVTTPS2DQ; Invalid|]

let op_vex_0F5B = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F5C = M.[|SUBPS; SUBPD; SUBSS; SUBSD|]

let op_vex_0F5C = M.[|VSUBPS; VSUBPD; VSUBSS; VSUBSD|]

let op_nor_0F5D = M.[|MINPS; MINPD; MINSS; MINSD|]

let op_vex_0F5D = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F5E = M.[|DIVPS; DIVPD; DIVSS; DIVSD|]

let op_vex_0F5E = M.[|VDIVPS; VDIVPD; VDIVSS; VDIVSD|]

let op_nor_0F5F = M.[|MAXPS; MAXPD; MAXSS; MAXSD|]

let op_vex_0F5F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F60 = M.[|PUNPCKLBW; PUNPCKLBW; Invalid; Invalid|]

let op_vex_0F60 = M.[|Invalid; VPUNPCKLBW; Invalid; Invalid|]

let op_nor_0F61 = M.[|PUNPCKLWD; PUNPCKLWD; Invalid; Invalid|]

let op_vex_0F61 = M.[|Invalid; VPUNPCKLWD; Invalid; Invalid|]

let op_nor_0F62 = M.[|PUNPCKLDQ; PUNPCKLDQ; Invalid; Invalid|]

let op_vex_0F62 = M.[|Invalid; VPUNPCKLDQ; Invalid; Invalid|]

let op_nor_0F63 = M.[|PACKSSWB; PACKSSWB; Invalid; Invalid|]

let op_vex_0F63 = M.[|Invalid; VPACKSSWB; Invalid; Invalid|]

let op_nor_0F64 = M.[|PCMPGTB; PCMPGTB; Invalid; Invalid|]

let op_vex_0F64 = M.[|Invalid; VPCMPGTB; Invalid; Invalid|]

let op_nor_0F65 = M.[|PCMPGTW; PCMPGTW; Invalid; Invalid|]

let op_vex_0F65 = M.[|Invalid; VPCMPGTW; Invalid; Invalid|]

let op_nor_0F66 = M.[|PCMPGTD; PCMPGTD; Invalid; Invalid|]

let op_vex_0F66 = M.[|Invalid; VPCMPGTD; Invalid; Invalid|]

let op_nor_0F67 = M.[|PACKUSWB; PACKUSWB; Invalid; Invalid|]

let op_vex_0F67 = M.[|Invalid; VPACKUSWB; Invalid; Invalid|]

let op_nor_0F68 = M.[|PUNPCKHBW; PUNPCKHBW; Invalid; Invalid|]

let op_vex_0F68 = M.[|Invalid; VPUNPCKHBW; Invalid; Invalid|]

let op_nor_0F69 = M.[|PUNPCKHWD; PUNPCKHWD; Invalid; Invalid|]

let op_vex_0F69 = M.[|Invalid; VPUNPCKHWD; Invalid; Invalid|]

let op_nor_0F6A = M.[|PUNPCKHDQ; PUNPCKHDQ; Invalid; Invalid|]

let op_vex_0F6A = M.[|Invalid; VPUNPCKHDQ; Invalid; Invalid|]

let op_nor_0F6B = M.[|PACKSSDW; PACKSSDW; Invalid; Invalid|]

let op_vex_0F6B = M.[|Invalid; VPACKSSDW; Invalid; Invalid|]

let op_nor_0F6C = M.[|Invalid; PUNPCKLQDQ; Invalid; Invalid|]

let op_vex_0F6C = M.[|Invalid; VPUNPCKLQDQ; Invalid; Invalid|]

let op_nor_0F6D = M.[|Invalid; PUNPCKHQDQ; Invalid; Invalid|]

let op_vex_0F6D = M.[|Invalid; VPUNPCKHQDQ; Invalid; Invalid|]

let op_nor_0F6EB64 = M.[|MOVQ; MOVQ; Invalid; Invalid|]

let op_nor_0F6EB32 = M.[|MOVD; MOVD; Invalid; Invalid|]

let op_vex_0F6EB64 = M.[|Invalid; VMOVQ; Invalid; Invalid|]

let op_vex_0F6EB32 = M.[|Invalid; VMOVD; Invalid; Invalid|]

let op_nor_0F6F = M.[|MOVQ; MOVDQA; MOVDQU; Invalid|]

let op_vex_0F6F = M.[|Invalid; VMOVDQA; VMOVDQU; Invalid|]

let op_evex_0F6FB64 = M.[|Invalid; VMOVDQA64; VMOVDQU64; Invalid|]

let op_evex_0F6FB32 = M.[|Invalid; VMOVDQA32; VMOVDQU32; Invalid|]

let op_nor_0F70 = M.[|PSHUFW; PSHUFD; PSHUFHW; PSHUFLW|]

let op_vex_0F70 = M.[|Invalid; VPSHUFD; VPSHUFHW; VPSHUFLW|]

let op_nor_0F74 = M.[|PCMPEQB; PCMPEQB; Invalid; Invalid|]

let op_vex_0F74 = M.[|Invalid; VPCMPEQB; Invalid; Invalid|]

let op_nor_0F75 = M.[|PCMPEQW; PCMPEQW; Invalid; Invalid|]

let op_vex_0F75 = M.[|Invalid; VPCMPEQW; Invalid; Invalid|]

let op_nor_0F76 = M.[|PCMPEQD; PCMPEQD; Invalid; Invalid|]

let op_vex_0F76 = M.[|Invalid; VPCMPEQD; Invalid; Invalid|]

let op_nor_0F77 = M.[|EMMS; Invalid; Invalid; Invalid|]

let op_vex_0F77 = M.[|VZEROUPPER; Invalid; Invalid; Invalid|]

let op_nor_0F7EB64 = M.[|MOVQ; MOVQ; MOVQ; Invalid|]

let op_nor_0F7EB32 = M.[|MOVD; MOVD; MOVQ; Invalid|]

let op_vex_0F7EB64 = M.[|Invalid; VMOVQ; VMOVQ; Invalid|]

let op_vex_0F7EB32 = M.[|Invalid; VMOVD; VMOVQ; Invalid|]

let op_nor_0F7F = M.[|MOVQ; MOVDQA; MOVDQU; Invalid|]

let op_vex_0F7F = M.[|Invalid; VMOVDQA; VMOVDQU; Invalid|]

let op_evex_0F7FB64 = M.[|Invalid; VMOVDQA64; Invalid; Invalid|]

let op_evex_0F7FB32 = M.[|Invalid; VMOVDQA32; Invalid; Invalid|]

let op_nor_0FC2 = M.[|CMPPS; CMPPD; CMPSS; CMPSD|]

let op_vex_0FC2 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0FC4 = M.[|PINSRW; PINSRW; Invalid; Invalid|]

let op_vex_0FC4 = M.[|Invalid; VPINSRW; Invalid; Invalid|]

let op_nor_0FC5 = M.[|PEXTRW; PEXTRW; Invalid; Invalid|]

let op_vex_0FC5 = M.[|Invalid; VPEXTRW; Invalid; Invalid|]

let op_nor_0FC6 = M.[|SHUFPS; SHUFPD; Invalid; Invalid|]

let op_vex_0FC6 = M.[|VSHUFPS; VSHUFPD; Invalid; Invalid|]

let op_nor_0FD1 = M.[|PSRLW; PSRLW; Invalid; Invalid|]

let op_vex_0FD1 = M.[|Invalid; VPSRLW; Invalid; Invalid|]

let op_nor_0FD2 = M.[|PSRLD; PSRLD; Invalid; Invalid|]

let op_vex_0FD2 = M.[|Invalid; VPSRLD; Invalid; Invalid|]

let op_nor_0FD3 = M.[|PSRLQ; PSRLQ; Invalid; Invalid|]

let op_vex_0FD3 = M.[|Invalid; VPSRLQ; Invalid; Invalid|]

let op_nor_0FD4 = M.[|PADDQ; PADDQ; Invalid; Invalid|]

let op_vex_0FD4 = M.[|Invalid; VPADDQ; Invalid; Invalid|]

let op_nor_0FD5 = M.[|PMULLW; PMULLW; Invalid; Invalid|]

let op_vex_0FD5 = M.[|Invalid; VPMULLW; Invalid; Invalid|]

let op_nor_0FD6 = M.[|Invalid; MOVQ; MOVQ2DQ; MOVDQ2Q|]

let op_vex_0FD6 = M.[|Invalid; VMOVQ; Invalid; Invalid|]

let op_nor_0FD7 = M.[|PMOVMSKB; PMOVMSKB; Invalid; Invalid|]

let op_vex_0FD7 = M.[|Invalid; VPMOVMSKB; Invalid; Invalid|]

let op_nor_0FD8 = M.[|PSUBUSB; PSUBUSB; Invalid; Invalid|]

let op_vex_0FD8 = M.[|Invalid; VPSUBUSB; Invalid; Invalid|]

let op_nor_0FD9 = M.[|PSUBUSW; PSUBUSW; Invalid; Invalid|]

let op_vex_0FD9 = M.[|Invalid; VPSUBUSW; Invalid; Invalid|]

let op_nor_0FDA = M.[|PMINUB; PMINUB; Invalid; Invalid|]

let op_vex_0FDA = M.[|Invalid; VPMINUB; Invalid; Invalid|]

let op_nor_0FDB = M.[|PAND; PAND; Invalid; Invalid|]

let op_vex_0FDB = M.[|Invalid; VPAND; Invalid; Invalid|]

let op_nor_0FDC = M.[|PADDUSB; PADDUSB; Invalid; Invalid|]

let op_vex_0FDC = M.[|Invalid; VPADDUSB; Invalid; Invalid|]

let op_nor_0FDD = M.[|PADDUSW; PADDUSW; Invalid; Invalid|]

let op_vex_0FDD = M.[|Invalid; VPADDUSW; Invalid; Invalid|]

let op_nor_0FDE = M.[|PMAXUB; PMAXUB; Invalid; Invalid|]

let op_vex_0FDE = M.[|Invalid; VPMAXUB; Invalid; Invalid|]

let op_nor_0FDF = M.[|PANDN; PANDN; Invalid; Invalid|]

let op_vex_0FDF = M.[|Invalid; VPANDN; Invalid; Invalid|]

let op_nor_0FE0 = M.[|PAVGB; PAVGB; Invalid; Invalid|]

let op_vex_0FE0 = M.[|Invalid; VPAVGB; Invalid; Invalid|]

let op_nor_0FE1 = M.[|PSRAW; PSRAW; Invalid; Invalid|]

let op_vex_0FE1 = M.[|Invalid; VPSRAW; Invalid; Invalid|]

let op_nor_0FE2 = M.[|PSRAD; PSRAD; Invalid; Invalid|]

let op_vex_0FE2 = M.[|Invalid; VPSRAD; Invalid; Invalid|]

let op_nor_0FE3 = M.[|PAVGW; PAVGW; Invalid; Invalid|]

let op_vex_0FE3 = M.[|Invalid; VPAVGW; Invalid; Invalid|]

let op_nor_0FE4 = M.[|PMULHUW; PMULHUW; Invalid; Invalid|]

let op_vex_0FE4 = M.[|Invalid; VPMULHUW; Invalid; Invalid|]

let op_nor_0FE5 = M.[|PMULHW; PMULHW; Invalid; Invalid|]

let op_vex_0FE5 = M.[|Invalid; VPMULHW; Invalid; Invalid|]

let op_nor_0FE6 = M.[|Invalid; CVTTPD2DQ; CVTDQ2PD; CVTPD2DQ|]

let op_vex_0FE6 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0FE7 = M.[|MOVNTQ; MOVNTDQ; Invalid; Invalid|]

let op_vex_0FE7 = M.[|Invalid; VMOVNTDQ; Invalid; Invalid|]

let op_evex_0FE7B64 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0FE7B32 = M.[|Invalid; VMOVNTDQ; Invalid; Invalid|]

let op_nor_0FE8 = M.[|PSUBSB; PSUBSB; Invalid; Invalid|]

let op_vex_0FE8 = M.[|Invalid; VPSUBSB; Invalid; Invalid|]

let op_nor_0FE9 = M.[|PSUBSW; PSUBSW; Invalid; Invalid|]

let op_vex_0FE9 = M.[|Invalid; VPSUBSW; Invalid; Invalid|]

let op_nor_0FEA = M.[|PMINSW; PMINSW; Invalid; Invalid|]

let op_vex_0FEA = M.[|Invalid; VPMINSW; Invalid; Invalid|]

let op_nor_0FEB = M.[|POR; POR; Invalid; Invalid|]

let op_vex_0FEB = M.[|Invalid; VPOR; Invalid; Invalid|]

let op_nor_0FEC = M.[|PADDSB; PADDSB; Invalid; Invalid|]

let op_vex_0FEC = M.[|Invalid; VPADDSB; Invalid; Invalid|]

let op_nor_0FED = M.[|PADDSW; PADDSW; Invalid; Invalid|]

let op_vex_0FED = M.[|Invalid; VPADDSW; Invalid; Invalid|]

let op_nor_0FEE = M.[|PMAXSW; PMAXSW; Invalid; Invalid|]

let op_vex_0FEE = M.[|Invalid; VPMAXSW; Invalid; Invalid|]

let op_nor_0FEF = M.[|PXOR; PXOR; Invalid; Invalid|]

let op_vex_0FEF = M.[|Invalid; VPXOR; Invalid; Invalid|]

let op_nor_0FF0 = M.[|Invalid; Invalid; Invalid; LDDQU|]

let op_vex_0FF0 = M.[|Invalid; Invalid; Invalid; VLDDQU|]

let op_nor_0FF1 = M.[|PSLLW; PSLLW; Invalid; Invalid|]

let op_vex_0FF1 = M.[|Invalid; VPSLLW; Invalid; Invalid|]

let op_nor_0FF2 = M.[|PSLLD; PSLLD; Invalid; Invalid|]

let op_vex_0FF2 = M.[|Invalid; VPSLLD; Invalid; Invalid|]

let op_nor_0FF3 = M.[|PSLLQ; PSLLQ; Invalid; Invalid|]

let op_vex_0FF3 = M.[|Invalid; VPSLLQ; Invalid; Invalid|]

let op_nor_0FF4 = M.[|PMULUDQ; PMULUDQ; Invalid; Invalid|]

let op_vex_0FF4 = M.[|Invalid; VPMULUDQ; Invalid; Invalid|]

let op_nor_0FF5 = M.[|PMADDWD; PMADDWD; Invalid; Invalid|]

let op_vex_0FF5 = M.[|Invalid; VPMADDWD; Invalid; Invalid|]

let op_nor_0FF6 = M.[|PSADBW; PSADBW; Invalid; Invalid|]

let op_vex_0FF6 = M.[|Invalid; VPSADBW; Invalid; Invalid|]

let op_nor_0FF8 = M.[|PSUBB; PSUBB; Invalid; Invalid|]

let op_vex_0FF8 = M.[|Invalid; VPSUBB; Invalid; Invalid|]

let op_nor_0FF9 = M.[|PSUBW; PSUBW; Invalid; Invalid|]

let op_vex_0FF9 = M.[|Invalid; VPSUBW; Invalid; Invalid|]

let op_nor_0FFA = M.[|PSUBD; PSUBD; Invalid; Invalid|]

let op_vex_0FFA = M.[|Invalid; VPSUBD; Invalid; Invalid|]

let op_nor_0FFB = M.[|PSUBQ; PSUBQ; Invalid; Invalid|]

let op_vex_0FFB = M.[|Invalid; VPSUBQ; Invalid; Invalid|]

let op_nor_0FFC = M.[|PADDB; PADDB; Invalid; Invalid|]

let op_vex_0FFC = M.[|Invalid; VPADDB; Invalid; Invalid|]

let op_nor_0FFD = M.[|PADDW; PADDW; Invalid; Invalid|]

let op_vex_0FFD = M.[|Invalid; VPADDW; Invalid; Invalid|]

let op_nor_0FFE = M.[|PADDD; PADDD; Invalid; Invalid|]

let op_vex_0FFE = M.[|Invalid; VPADDD; Invalid; Invalid|]

let op_nor_0F3800 = M.[|PSHUFB; PSHUFB; Invalid; Invalid|]

let op_vex_0F3800 = M.[|Invalid; VPSHUFB; Invalid; Invalid|]

let op_nor_0F3801 = M.[|PHADDW; PHADDW; Invalid; Invalid|]

let op_vex_0F3801 = M.[|Invalid; VPHADDW; Invalid; Invalid|]

let op_nor_0F3802 = M.[|PHADDD; PHADDD; Invalid; Invalid|]

let op_vex_0F3802 = M.[|Invalid; VPHADDD; Invalid; Invalid|]

let op_nor_0F3803 = M.[|PHADDSW; PHADDSW; Invalid; Invalid|]

let op_vex_0F3803 = M.[|Invalid; VPHADDSW; Invalid; Invalid|]

let op_nor_0F3804 = M.[|PMADDUBSW; PMADDUBSW; Invalid; Invalid|]

let op_vex_0F3804 = M.[|Invalid; VPMADDUBSW; Invalid; Invalid|]

let op_nor_0F3805 = M.[|PHSUBW; PHSUBW; Invalid; Invalid|]

let op_vex_0F3805 = M.[|Invalid; VPHSUBW; Invalid; Invalid|]

let op_nor_0F3806 = M.[|PHSUBD; PHSUBD; Invalid; Invalid|]

let op_vex_0F3806 = M.[|Invalid; VPHSUBD; Invalid; Invalid|]

let op_nor_0F3807 = M.[|PHSUBSW; PHSUBSW; Invalid; Invalid|]

let op_vex_0F3807 = M.[|Invalid; VPHSUBSW; Invalid; Invalid|]

let op_nor_0F3808 = M.[|PSIGNB; PSIGNB; Invalid; Invalid|]

let op_vex_0F3808 = M.[|Invalid; VPSIGNB; Invalid; Invalid|]

let op_nor_0F3809 = M.[|PSIGNW; PSIGNW; Invalid; Invalid|]

let op_vex_0F3809 = M.[|Invalid; VPSIGNW; Invalid; Invalid|]

let op_nor_0F380A = M.[|PSIGND; PSIGND; Invalid; Invalid|]

let op_vex_0F380A = M.[|Invalid; VPSIGND; Invalid; Invalid|]

let op_nor_0F380B = M.[|PMULHRSW; PMULHRSW; Invalid; Invalid|]

let op_vex_0F380B = M.[|Invalid; VPMULHRSW; Invalid; Invalid|]

let op_nor_0F380C = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F380C = M.[|Invalid; VPERMILPS; Invalid; Invalid|]

let op_evex_0F380C = M.[|Invalid; VPERMILPS; Invalid; Invalid|]

let op_nor_0F380D = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F380D = M.[|Invalid; VPERMILPD; Invalid; Invalid|]

let op_evex_0F380D = M.[|Invalid; VPERMILPD; Invalid; Invalid|]

let op_nor_0F380E = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F380E = M.[|Invalid; VTESTPS; Invalid; Invalid|]

let op_nor_0F380F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F380F = M.[|Invalid; VTESTPD; Invalid; Invalid|]

let op_nor_0F3810 = M.[|Invalid; PBLENDVB; Invalid; Invalid|]

let op_vex_0F3810 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3810B64 = M.[|Invalid; VPSRLVW; Invalid; Invalid|]

let op_evex_0F3810B32 = M.[|Invalid; Invalid; VPMOVUSWB; Invalid|]

let op_nor_0F3811 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3811 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3811B64 = M.[|Invalid; VPSRAVW; Invalid; Invalid|]

let op_evex_0F3811B32 = M.[|Invalid; Invalid; VPMOVUSDB; Invalid|]

let op_nor_0F3812 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3812 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3812B64 = M.[|Invalid; VPSLLVW; Invalid; Invalid|]

let op_evex_0F3812B32 = M.[|Invalid; Invalid; VPMOVUSQB; Invalid|]

let op_nor_0F3813 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3813 = M.[|Invalid; VCVTPH2PS; Invalid; Invalid|]

let op_evex_0F3813B64 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3813B32 = M.[|Invalid; VCVTPH2PS; VPMOVUSDW; Invalid|]

let op_nor_0F3814 = M.[|Invalid; BLENDVPS; Invalid; Invalid|]

let op_vex_0F3814 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3814B64 = M.[|Invalid; VPRORVQ; Invalid; Invalid|]

let op_evex_0F3814B32 = M.[|Invalid; VPRORVD; VPMOVUSQW; Invalid|]

let op_nor_0F3815 = M.[|Invalid; BLENDVPD; Invalid; Invalid|]

let op_vex_0F3815 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3815B64 = M.[|Invalid; VPROLVQ; Invalid; Invalid|]

let op_evex_0F3815B32 = M.[|Invalid; VPROLVD; VPMOVUSQD; Invalid|]

let op_nor_0F3816 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3816 = M.[|Invalid; VPERMPS; Invalid; Invalid|]

let op_evex_0F3816B64 = M.[|Invalid; VPERMPD; Invalid; Invalid|]

let op_evex_0F3816B32 = M.[|Invalid; VPERMPS; Invalid; Invalid|]

let op_nor_0F3817 = M.[|Invalid; PTEST; Invalid; Invalid|]

let op_vex_0F3817 = M.[|Invalid; VPTEST; Invalid; Invalid|]

let op_nor_0F3818 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3818 = M.[|Invalid; VBROADCASTSS; Invalid; Invalid|]

let op_evex_0F3818 = M.[|Invalid; VBROADCASTSS; Invalid; Invalid|]

let op_nor_0F3819 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3819 = M.[|Invalid; VBROADCASTSS; Invalid; Invalid|]

let op_evex_0F3819B64 = M.[|Invalid; VBROADCASTSD; Invalid; Invalid|]

let op_evex_0F3819B32 = M.[|Invalid; VBROADCASTF32X2; Invalid; Invalid|]

let op_nor_0F381A = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F381A = M.[|Invalid; VBROADCASTF128; Invalid; Invalid|]

let op_evex_0F381AB64 = M.[|Invalid; VBROADCASTF64X2; Invalid; Invalid|]

let op_evex_0F381AB32 = M.[|Invalid; VBROADCASTF32X4; Invalid; Invalid|]

let op_nor_0F381B = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F381B = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F381BB64 = M.[|Invalid; VBROADCASTF64X4; Invalid; Invalid|]

let op_evex_0F381BB32 = M.[|Invalid; VBROADCASTF32X8; Invalid; Invalid|]

let op_nor_0F381C = M.[|PABSB; PABSB; Invalid; Invalid|]

let op_vex_0F381C = M.[|Invalid; VPABSB; Invalid; Invalid|]

let op_nor_0F381D = M.[|PABSW; PABSW; Invalid; Invalid|]

let op_vex_0F381D = M.[|Invalid; VPABSW; Invalid; Invalid|]

let op_nor_0F381E = M.[|PABSD; PABSD; Invalid; Invalid|]

let op_vex_0F381E = M.[|Invalid; VPABSD; Invalid; Invalid|]

let op_nor_0F381F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F381F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F381FB64 = M.[|Invalid; VPABSQ; Invalid; Invalid|]

let op_evex_0F381FB32 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F3820 = M.[|Invalid; PMOVSXBW; Invalid; Invalid|]

let op_vex_0F3820 = M.[|Invalid; VPMOVSXBW; Invalid; Invalid|]

let op_nor_0F3821 = M.[|Invalid; PMOVSXBD; Invalid; Invalid|]

let op_vex_0F3821 = M.[|Invalid; VPMOVSXBD; Invalid; Invalid|]

let op_nor_0F3822 = M.[|Invalid; PMOVSXBQ; Invalid; Invalid|]

let op_vex_0F3822 = M.[|Invalid; VPMOVSXBQ; Invalid; Invalid|]

let op_nor_0F3823 = M.[|Invalid; PMOVSXWD; Invalid; Invalid|]

let op_vex_0F3823 = M.[|Invalid; VPMOVSXWD; Invalid; Invalid|]

let op_nor_0F3824 = M.[|Invalid; PMOVSXWQ; Invalid; Invalid|]

let op_vex_0F3824 = M.[|Invalid; VPMOVSXWQ; Invalid; Invalid|]

let op_nor_0F3825 = M.[|Invalid; PMOVSXDQ; Invalid; Invalid|]

let op_vex_0F3825 = M.[|Invalid; VPMOVSXDQ; Invalid; Invalid|]

let op_nor_0F3826 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3826 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3826B64 = M.[|Invalid; VPTESTMW; VPTESTNMW; Invalid|]

let op_evex_0F3826B32 = M.[|Invalid; VPTESTMB; VPTESTNMB; Invalid|]

let op_nor_0F3827 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3827 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3827B64 = M.[|Invalid; VPTESTMQ; VPTESTNMQ; Invalid|]

let op_evex_0F3827B32 = M.[|Invalid; VPTESTMD; VPTESTNMD; Invalid|]

let op_nor_0F3828 = M.[|Invalid; PMULDQ; Invalid; Invalid|]

let op_vex_0F3828 = M.[|Invalid; VPMULDQ; Invalid; Invalid|]

let op_evex_0F3828B64 = M.[|Invalid; VPMULDQ; VPMOVM2W; Invalid|]

let op_evex_0F3828B32 = M.[|Invalid; Invalid; VPMOVM2B; Invalid|]

let op_nor_0F3829 = M.[|Invalid; PCMPEQQ; Invalid; Invalid|]

let op_vex_0F3829 = M.[|Invalid; VPCMPEQQ; Invalid; Invalid|]

let op_evex_0F3829B64 = M.[|Invalid; VPCMPEQQ; VPMOVW2M; Invalid|]

let op_evex_0F3829B32 = M.[|Invalid; Invalid; VPMOVB2M; Invalid|]

let op_nor_0F382A = M.[|Invalid; MOVNTDQA; Invalid; Invalid|]

let op_vex_0F382A = M.[|Invalid; VMOVNTDQA; Invalid; Invalid|]

let op_evex_0F382AB64 = M.[|Invalid; Invalid; VPBROADCASTMB2Q; Invalid|]

let op_evex_0F382AB32 = M.[|Invalid; VMOVNTDQA; Invalid; Invalid|]

let op_nor_0F382B = M.[|Invalid; PACKUSDW; Invalid; Invalid|]

let op_vex_0F382B = M.[|Invalid; VPACKUSDW; Invalid; Invalid|]

let op_evex_0F382BB64 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F382BB32 = M.[|Invalid; VPACKUSDW; Invalid; Invalid|]

let op_nor_0F382C = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F382C = M.[|Invalid; VMASKMOVPS; Invalid; Invalid|]

let op_evex_0F382CB64 = M.[|Invalid; VSCALEFPD; Invalid; Invalid|]

let op_evex_0F382CB32 = M.[|Invalid; VSCALEFPS; Invalid; Invalid|]

let op_nor_0F382D = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F382D = M.[|Invalid; VMASKMOVPD; Invalid; Invalid|]

let op_evex_0F382DB64 = M.[|Invalid; VSCALEFSD; Invalid; Invalid|]

let op_evex_0F382DB32 = M.[|Invalid; VSCALEFSS; Invalid; Invalid|]

let op_nor_0F382E = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F382E = M.[|Invalid; VMASKMOVPS; Invalid; Invalid|]

let op_nor_0F382F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F382F = M.[|Invalid; VMASKMOVPS; Invalid; Invalid|]

let op_nor_0F3830 = M.[|Invalid; PMOVZXBW; Invalid; Invalid|]

let op_vex_0F3830 = M.[|Invalid; VPMOVZXBW; Invalid; Invalid|]

let op_nor_0F3831 = M.[|Invalid; PMOVZXBD; Invalid; Invalid|]

let op_vex_0F3831 = M.[|Invalid; VPMOVZXBD; Invalid; Invalid|]

let op_nor_0F3832 = M.[|Invalid; PMOVZXBQ; Invalid; Invalid|]

let op_vex_0F3832 = M.[|Invalid; VPMOVZXBQ; Invalid; Invalid|]

let op_nor_0F3833 = M.[|Invalid; PMOVZXWD; Invalid; Invalid|]

let op_vex_0F3833 = M.[|Invalid; VPMOVZXWD; Invalid; Invalid|]

let op_nor_0F3834 = M.[|Invalid; PMOVZXWQ; Invalid; Invalid|]

let op_vex_0F3834 = M.[|Invalid; VPMOVZXWQ; Invalid; Invalid|]

let op_nor_0F3835 = M.[|Invalid; PMOVZXDQ; Invalid; Invalid|]

let op_vex_0F3835 = M.[|Invalid; VPMOVZXDQ; Invalid; Invalid|]

let op_nor_0F3836 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3836 = M.[|Invalid; VPERMD; Invalid; Invalid|]

let op_evex_0F3836B64 = M.[|Invalid; VPERMQ; Invalid; Invalid|]

let op_evex_0F3836B32 = M.[|Invalid; VPERMD; Invalid; Invalid|]

let op_nor_0F3837 = M.[|Invalid; PCMPGTQ; Invalid; Invalid|]

let op_vex_0F3837 = M.[|Invalid; VPCMPGTQ; Invalid; Invalid|]

let op_nor_0F3838 = M.[|Invalid; PMINSB; Invalid; Invalid|]

let op_vex_0F3838 = M.[|Invalid; VPMINSB; Invalid; Invalid|]

let op_nor_0F3839 = M.[|Invalid; PMINSD; Invalid; Invalid|]

let op_vex_0F3839 = M.[|Invalid; VPMINSD; Invalid; Invalid|]

let op_nor_0F383A = M.[|Invalid; PMINUW; Invalid; Invalid|]

let op_vex_0F383A = M.[|Invalid; VPMINUW; Invalid; Invalid|]

let op_evex_0F383AB64 = M.[|Invalid; VPMINUW; Invalid; Invalid|]

let op_evex_0F383AB32 = M.[|Invalid; VPMINUW; VPBROADCASTMW2D; Invalid|]

let op_nor_0F383B = M.[|Invalid; PMINUD; Invalid; Invalid|]

let op_vex_0F383B = M.[|Invalid; VPMINUD; Invalid; Invalid|]

let op_evex_0F383BB64 = M.[|Invalid; VPMINUQ; Invalid; Invalid|]

let op_evex_0F383BB32 = M.[|Invalid; VPMINUD; Invalid; Invalid|]

let op_nor_0F383C = M.[|Invalid; PMAXSB; Invalid; Invalid|]

let op_vex_0F383C = M.[|Invalid; VPMAXSB; Invalid; Invalid|]

let op_nor_0F383D = M.[|Invalid; PMAXSD; Invalid; Invalid|]

let op_vex_0F383D = M.[|Invalid; VPMAXSD; Invalid; Invalid|]

let op_evex_0F383DB64 = M.[|Invalid; VPMAXSQ; Invalid; Invalid|]

let op_evex_0F383DB32 = M.[|Invalid; VPMAXSD; Invalid; Invalid|]

let op_nor_0F383E = M.[|Invalid; PMAXUW; Invalid; Invalid|]

let op_vex_0F383E = M.[|Invalid; VPMAXUW; Invalid; Invalid|]

let op_nor_0F383F = M.[|Invalid; PMAXUD; Invalid; Invalid|]

let op_vex_0F383F = M.[|Invalid; VPMAXUD; Invalid; Invalid|]

let op_evex_0F383FB64 = M.[|Invalid; VPMAXUQ; Invalid; Invalid|]

let op_evex_0F383FB32 = M.[|Invalid; VPMAXUD; Invalid; Invalid|]

let op_nor_0F3840 = M.[|Invalid; PMULLD; Invalid; Invalid|]

let op_vex_0F3840 = M.[|Invalid; VPMULLD; Invalid; Invalid|]

let op_evex_0F3840B64 = M.[|Invalid; VPMULLQ; Invalid; Invalid|]

let op_evex_0F3840B32 = M.[|Invalid; VPMULLD; Invalid; Invalid|]

let op_nor_0F3841 = M.[|Invalid; PHMINPOSUW; Invalid; Invalid|]

let op_vex_0F3841 = M.[|Invalid; VPHMINPOSUW; Invalid; Invalid|]

let op_nor_0F3846 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3846B64 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3846B32 = M.[|Invalid; VPSRAVD; Invalid; Invalid|]

let op_evex_0F3846B64 = M.[|Invalid; VPSRAVQ; Invalid; Invalid|]

let op_evex_0F3846B32 = M.[|Invalid; VPSRAVD; Invalid; Invalid|]

let op_nor_0F384C = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F384C = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F384CB64 = M.[|Invalid; VRCP14PD; Invalid; Invalid|]

let op_evex_0F384CB32 = M.[|Invalid; VRCP14PS; Invalid; Invalid|]

let op_nor_0F384D = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F384D = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F384DB64 = M.[|Invalid; VRCP14SD; Invalid; Invalid|]

let op_evex_0F384DB32 = M.[|Invalid; VRCP14SS; Invalid; Invalid|]

let op_nor_0F384E = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F384E = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F384EB64 = M.[|Invalid; VRSQRT14PD; Invalid; Invalid|]

let op_evex_0F384EB32 = M.[|Invalid; VRSQRT14PS; Invalid; Invalid|]

let op_nor_0F384F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F384F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F384FB64 = M.[|Invalid; VRSQRT14SD; Invalid; Invalid|]

let op_evex_0F384FB32 = M.[|Invalid; VRSQRT14SS; Invalid; Invalid|]

let op_nor_0F385A = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F385A = M.[|Invalid; VBROADCASTI128; Invalid; Invalid|]

let op_nor_0F3878 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3878 = M.[|Invalid; VPBROADCASTB; Invalid; Invalid|]

let op_nor_0F38F0 = M.[|MOVBE; MOVBE; Invalid; CRC32; CRC32|]

let op_nor_0F38F1 = M.[|MOVBE; MOVBE; Invalid; CRC32; CRC32|]

let op_nor_0F38F2 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F38F2 = M.[|ANDN; Invalid; Invalid; Invalid|]

let op_nor_0F38F5 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F38F5 = M.[|BZHI; Invalid; Invalid; Invalid|]

let op_nor_0F38F6 = M.[|Invalid; ADCX; ADOX; Invalid|]

let op_vex_0F38F6 = M.[|Invalid; Invalid; Invalid; MULX|]

let op_nor_0F38F7 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F38F7 = M.[|BEXTR; SHLX; SARX; SHRX|]

let op_nor_0F38F8 = M.[|Invalid; MOVDIR64B; ENQCMDS; ENQCMD|]

let op_vex_0F38F8 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F3A00 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A00 = M.[|Invalid; VPERMQ; Invalid; Invalid|]

let op_evex_0F3A00B64 = M.[|Invalid; VPERMQ; Invalid; Invalid|]

let op_evex_0F3A00B32 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F3A01 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A01 = M.[|Invalid; VPERMPD; Invalid; Invalid|]

let op_evex_0F3A01B64 = M.[|Invalid; VPERMPD; Invalid; Invalid|]

let op_evex_0F3A01B32 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F3A02 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A02 = M.[|Invalid; VPBLENDD; Invalid; Invalid|]

let op_nor_0F3A03 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A03 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3A03B64 = M.[|Invalid; VALIGNQ; Invalid; Invalid|]

let op_evex_0F3A03B32 = M.[|Invalid; VALIGND; Invalid; Invalid|]

let op_nor_0F3A04 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A04 = M.[|Invalid; VPERMILPS; Invalid; Invalid|]

let op_evex_0F3A04B64 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3A04B32 = M.[|Invalid; VPERMILPS; Invalid; Invalid|]

let op_nor_0F3A05 = M.[|Invalid; VPERMILPD; Invalid; Invalid|]

let op_vex_0F3A05 = M.[|Invalid; VPERMILPD; Invalid; Invalid|]

let op_evex_0F3A05B64 = M.[|Invalid; VPERMILPD; Invalid; Invalid|]

let op_evex_0F3A05B32 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F3A06 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A06 = M.[|Invalid; VPERM2F128; Invalid; Invalid|]

let op_nor_0F3A08 = M.[|Invalid; ROUNDPS; Invalid; Invalid|]

let op_vex_0F3A08 = M.[|Invalid; VROUNDPS; Invalid; Invalid|]

let op_evex_0F3A08B64 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3A08B32 = M.[|Invalid; VRNDSCALEPS; Invalid; Invalid|]

let op_nor_0F3A09 = M.[|Invalid; ROUNDPD; Invalid; Invalid|]

let op_vex_0F3A09 = M.[|Invalid; VROUNDPD; Invalid; Invalid|]

let op_evex_0F3A09B64 = M.[|Invalid; VRNDSCALEPD; Invalid; Invalid|]

let op_evex_0F3A09B32 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F3A0A = M.[|Invalid; ROUNDSS; Invalid; Invalid|]

let op_vex_0F3A0A = M.[|Invalid; VROUNDSS; Invalid; Invalid|]

let op_evex_0F3A0AB64 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3A0AB32 = M.[|Invalid; VRNDSCALESS; Invalid; Invalid|]

let op_nor_0F3A0B = M.[|Invalid; ROUNDSD; Invalid; Invalid|]

let op_vex_0F3A0B = M.[|Invalid; VROUNDSD; Invalid; Invalid|]

let op_evex_0F3A0BB64 = M.[|Invalid; VRNDSCALESD; Invalid; Invalid|]

let op_evex_0F3A0BB32 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_nor_0F3A0C = M.[|Invalid; BLENDPS; Invalid; Invalid|]

let op_vex_0F3A0C = M.[|Invalid; VBLENDPS; Invalid; Invalid|]

let op_nor_0F3A0D = M.[|Invalid; BLENDPD; Invalid; Invalid|]

let op_vex_0F3A0D = M.[|Invalid; VBLENDPD; Invalid; Invalid|]

let op_nor_0F3A0E = M.[|Invalid; PBLENDW; Invalid; Invalid|]

let op_vex_0F3A0E = M.[|Invalid; VPBLENDW; Invalid; Invalid|]

let op_nor_0F3A0F = M.[|PALIGNR; PALIGNR; Invalid; Invalid|]

let op_vex_0F3A0F = M.[|Invalid; VPALIGNR; Invalid; Invalid|]

let op_nor_0F3A14 = M.[|Invalid; PEXTRB; Invalid; Invalid|]

let op_vex_0F3A14 = M.[|Invalid; VPEXTRB; Invalid; Invalid|]

let op_nor_0F3A15 = M.[|Invalid; PEXTRW; Invalid; Invalid|]

let op_vex_0F3A15 = M.[|Invalid; VPEXTRW; Invalid; Invalid|]

let op_nor_0F3A16B64 = M.[|Invalid; PEXTRQ; Invalid; Invalid|]

let op_nor_0F3A16B32 = M.[|Invalid; PEXTRD; Invalid; Invalid|]

let op_vex_0F3A16B64 = M.[|Invalid; VPEXTRQ; Invalid; Invalid|]

let op_vex_0F3A16B32 = M.[|Invalid; VPEXTRD; Invalid; Invalid|]

let op_evex_0F3A16B64 = M.[|Invalid; VPEXTRQ; Invalid; Invalid|]

let op_evex_0F3A16B32 = M.[|Invalid; VPEXTRD; Invalid; Invalid|]

let op_nor_0F3A17 = M.[|Invalid; EXTRACTPS; Invalid; Invalid|]

let op_vex_0F3A17 = M.[|Invalid; VEXTRACTPS; Invalid; Invalid|]

let op_nor_0F3A18 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A18 = M.[|Invalid; VINSERTF128; Invalid; Invalid|]

let op_evex_0F3A18B64 = M.[|Invalid; VINSERTF64X2; Invalid; Invalid|]

let op_evex_0F3A18B32 = M.[|Invalid; VINSERTF32X4; Invalid; Invalid|]

let op_nor_0F3A19 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A19 = M.[|Invalid; VEXTRACTF128; Invalid; Invalid|]

let op_evex_0F3A19B64 = M.[|Invalid; VEXTRACTF64X2; Invalid; Invalid|]

let op_evex_0F3A19B32 = M.[|Invalid; VEXTRACTF32X4; Invalid; Invalid|]

let op_nor_0F3A1A = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A1A = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3A1AB64 = M.[|Invalid; VINSERTF64X4; Invalid; Invalid|]

let op_evex_0F3A1AB32 = M.[|Invalid; VINSERTF32X8; Invalid; Invalid|]

let op_nor_0F3A1B = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A1B = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3A1BB64 = M.[|Invalid; VEXTRACTF64X4; Invalid; Invalid|]

let op_evex_0F3A1BB32 = M.[|Invalid; VEXTRACTF32X8; Invalid; Invalid|]

let op_nor_0F3A1D = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A1D = M.[|Invalid; VCVTPS2PH; Invalid; Invalid|]

let op_nor_0F3A1E = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A1E = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3A1EB64 = M.[|Invalid; VPCMPUQ; Invalid; Invalid|]

let op_evex_0F3A1EB32 = M.[|Invalid; VPCMPUD; Invalid; Invalid|]

let op_nor_0F3A1F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A1F = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_evex_0F3A1FB64 = M.[|Invalid; VPCMPQ; Invalid; Invalid|]

let op_evex_0F3A1FB32 = M.[|Invalid; VPCMPD; Invalid; Invalid|]

let op_nor_0F3A20 = M.[|Invalid; PINSRB; Invalid; Invalid|]

let op_vex_0F3A20 = M.[|Invalid; VPINSRB; Invalid; Invalid|]

let op_nor_0F3A21 = M.[|Invalid; INSERTPS; Invalid; Invalid|]

let op_vex_0F3A21 = M.[|Invalid; VINSERTPS; Invalid; Invalid|]

let op_nor_0F3A22B64 = M.[|Invalid; PINSRQ; Invalid; Invalid|]

let op_nor_0F3A22B32 = M.[|Invalid; PINSRD; Invalid; Invalid|]

let op_vex_0F3A22B64 = M.[|Invalid; VPINSRQ; Invalid; Invalid|]

let op_vex_0F3A22B32 = M.[|Invalid; VPINSRD; Invalid; Invalid|]

let op_evex_0F3A22B64 = M.[|Invalid; VPINSRQ; Invalid; Invalid|]

let op_evex_0F3A22B32 = M.[|Invalid; VPINSRD; Invalid; Invalid|]

let op_nor_0F3A38 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A38 = M.[|Invalid; VINSERTI128; Invalid; Invalid|]

let op_nor_0F3A39 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3A39 = M.[|Invalid; VEXTRACTI128; Invalid; Invalid|]

let op_nor_0F3A60 = M.[|Invalid; PCMPESTRM; Invalid; Invalid|]

let op_vex_0F3A60 = M.[|Invalid; VPCMPESTRM; Invalid; Invalid|]

let op_nor_0F3A61 = M.[|Invalid; PCMPESTRI; Invalid; Invalid|]

let op_vex_0F3A61 = M.[|Invalid; VPCMPESTRI; Invalid; Invalid|]

let op_nor_0F3A62 = M.[|Invalid; PCMPISTRM; Invalid; Invalid|]

let op_vex_0F3A62 = M.[|Invalid; VPCMPISTRM; Invalid; Invalid|]

let op_nor_0F3A63 = M.[|Invalid; PCMPISTRI; Invalid; Invalid|]

let op_vex_0F3A63 = M.[|Invalid; VPCMPISTRI; Invalid; Invalid|]

let op_nor_0F3A0B = M.[|Invalid; ROUNDSD; Invalid; Invalid|]

let op_vex_0F3A0B = M.[|Invalid; VROUNDSD; Invalid; Invalid|]

let op_nor_0F3AF0 = M.[|Invalid; Invalid; Invalid; Invalid|]

let op_vex_0F3AF0 = M.[|Invalid; Invalid; Invalid; RORX|]

let op_empty = M.[|Invalid; Invalid; Invalid; Invalid|]

let e : operand_enc array = [||]

let ds_nor_0F1A =
  [|_BNDRbndMdqa; _BNDRbndBNDMbnd; _BNDRbndEdqa; _BNDRbndEdqa|]

let ds_nor_0F1B =
  [|_MdqaBNDRbnd; _BNDMbndBNDRbnd; _BNDRbndMdqa; _BNDRbndEdqa|]

let ds_nor_0F10 = [|_VdqWdq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F10_mem = [|_VpsWps; _VpdWpd; _VxWssd; _VxWssq|]

let ds_vex_0F10_reg = [|_VpsWps; _VpdWpd; _VxHxWss; _VxHxWsd|]

let ds_nor_0F11 = [|_WdqVdq; _WdqVdq; _WdqdVdq; _WdqqVdq|]

let ds_vex_0F11_mem = [|_WpsVps; _WpdVpd; _WssdVx; _WssqVx|]

let ds_vex_0F11_reg = [|_WpsVps; _WpdVpd; _WssHxVss; _WsdHxVsd|]

let ds_nor_0F12_mem = [|_VdqMq; _VdqMq; _VdqWdq; _VdqWdqq|]

let ds_nor_0F12_reg = [|_VdqUdq; _VdqMq; _VdqWdq; _VdqWdqq|]

let ds_vex_0F12_mem = [|_VdqHdqMq; _VdqHdqMdqd; _VxWx; _VxWx|]

let ds_vex_0F12_reg = [|_VdqHdqUdq; _VdqHdqMdqd; _VxWx; _VxWx|]

let ds_nor_0F13 = [|_MqVdq; _MqVdq; e; e|]

let ds_vex_0F13 = [|_MqVdq; _MqVdq; e; e|]

let ds_nor_0F14 = [|_VdqWdq; _VdqWdq; e; e|]

let ds_vex_0F14 = [|_VxHxWx; _VxHxWx; e; e|]

let ds_nor_0F15 = [|_VdqWdq; _VdqWdq; e; e|]

let ds_vex_0F15 = [|_VxHxWx; _VxHxWx; e; e|]

let ds_nor_0F16_mem = [|_VdqMq; _VdqMq; _VdqWdq; e|]

let ds_nor_0F16_reg = [|_VdqUdq; _VdqMq; _VdqWdq; e|]

let ds_vex_0F16_mem = [|_VdqHdqMq; _VdqHdqMq; _VxWx; e|]

let ds_vex_0F16_reg = [|_VdqHdqUdq; _VdqHdqMq; _VxWx; e|]

let ds_nor_0F17 = [|_MqVdq; _MqVdq; e; e|]

let ds_vex_0F17 = [|_MqVdq; _MqVdq; e; e|]

let ds_nor_0F28 = [|_VdqWdq; _VdqWdq; e; e|]

let ds_vex_0F28 = [|_VpsWps; _VpdWpd; e; e|]

let ds_nor_0F29 = [|_WdqVdq; _WdqVdq; e; e|]

let ds_vex_0F29 = [|_WpsVps; _WpdVpd; e; e|]

let ds_nor_0F2A = [|_VdqQq; _VdqQq; _VdqEy; _VdqEy|]

let ds_vex_0F2A = [|e; e; _VssHssEy; _VsdHsdEy|]

let ds_nor_0F2B = [|_MdqVdq; _MdqVdq; e; e|]

let ds_vex_0F2B = [|_MpsVps; _MpdVpd; e; e|]

let ds_nor_0F2C = [|_PpiWdqq; _PpiWdq; _GyWssd; _GyWsdq|]

let ds_vex_0F2C = [|e; e; _GyWssd; _GyWsdq|]

let ds_nor_0F2D = [|_PpiWdqq; _PpiWdq; _GyWssd; _GyWsdq|]

let ds_vex_0F2D = [|e; e; _GyWssd; _GyWsdq|]

let ds_nor_0F2E = [|_VssWssd; _VsdWsdq; e; e|]

let ds_vex_0F2E = [|_VssWssd; _VsdWsdq; e; e|]

let ds_nor_0F2F = [|_VssWssd; _VsdWsdq; e; e|]

let ds_vex_0F2F = [|_VssWssd; _VsdWsdq; e; e|]

let ds_nor_0F50 = [|_GyUdq; _GyUdq; e; e|]

let ds_vex_0F50 = [|_GyUps; _GyUpd; e; e|]

let ds_nor_0F51 = [|_VdqWdq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F51 = [|_VpsWps; _VpdWpd; _VssHssWss; _VsdHsdWsd|]

let ds_nor_0F52 = [|_VdqWdq; e; _VdqWdqd; e|]

let ds_vex_0F52 = [|_VpsWps; e; _VssHssWss; e|]

let ds_nor_0F53 = [|_VdqWdq; e; _VdqWdqd; e|]

let ds_vex_0F53 = [|_VpsWps; e; _VssHssWss; e|]

let ds_nor_0F54 = [|_VdqWdq; _VdqWdq; e; e|]

let ds_vex_0F54 = [|_VpsHpsWps; _VpdHpdWpd; e; e|]

let ds_nor_0F55 = [|_VdqWdq; _VdqWdq; e; e|]

let ds_vex_0F55 = [|_VpsHpsWps; _VpdHpdWpd; e; e|]

let ds_nor_0F56 = [|_VdqWdq; _VdqWdq; e; e|]

let ds_vex_0F56 = [|_VpsHpsWps; _VpdHpdWpd; e; e|]

let ds_nor_0F57 = [|_VdqWdq; _VdqWdq; e; e|]

let ds_vex_0F57 = [|_VpsHpsWps; _VpdHpdWpd; e; e|]

let ds_nor_0F58 = [|_VdqWdq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F58 = [|_VpsHpsWps; _VpdHpdWpd; _VssHssWssd; _VsdHsdWsdq|]

let ds_nor_0F59 = [|_VdqWdq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F59 = [|_VpsHpsWps; _VpdHpdWpd; _VssHssWssd; _VsdHsdWsdq|]

let ds_nor_0F5A = [|_VdqWdqq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F5A = [|e; e; e; e|]

let ds_nor_0F5B = [|_VdqWdq; _VdqWdq; _VdqWdq; e|]

let ds_vex_0F5B = [|e; e; e; e|]

let ds_nor_0F5C = [|_VdqWdq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F5C = [|_VpsHpsWps; _VpdHpdWpd; _VssHssWssd; _VsdHsdWsdq|]

let ds_nor_0F5D = [|_VdqWdq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F5D = [|e; e; e; e|]

let ds_nor_0F5E = [|_VdqWdq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F5E = [|_VpsHpsWps; _VpdHpdWpd; _VssHssWssd; _VsdHsdWsdq|]

let ds_nor_0F5F = [|_VdqWdq; _VdqWdq; _VdqWdqd; _VdqWdqq|]

let ds_vex_0F5F = [|e; e; e; e|]

let ds_nor_0F60 = [|_PqQd; _VdqWdq; e; e|]

let ds_vex_0F60 = [|e; _VxHxWx; e; e|]

let ds_nor_0F61 = [|_PqQd; _VdqWdq; e; e|]

let ds_vex_0F61 = [|e; _VxHxWx; e; e|]

let ds_nor_0F62 = [|_PqQd; _VdqWdq; e; e|]

let ds_vex_0F62 = [|e; _VxHxWx; e; e|]

let ds_nor_0F63 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F63 = [|e; _VxHxWx; e; e|]

let ds_nor_0F64 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F64 = [|e; _VxHxWx; e; e|]

let ds_nor_0F65 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F65 = [|e; _VxHxWx; e; e|]

let ds_nor_0F66 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F66 = [|e; _VxHxWx; e; e|]

let ds_nor_0F67 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F67 = [|e; _VxHxWx; e; e|]

let ds_nor_0F68 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F68 = [|e; _VxHxWx; e; e|]

let ds_nor_0F69 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F69 = [|e; _VxHxWx; e; e|]

let ds_nor_0F6A = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F6A = [|e; _VxHxWx; e; e|]

let ds_nor_0F6B = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F6B = [|e; _VxHxWx; e; e|]

let ds_nor_0F6C = [|e; _VdqWdq; e; e|]

let ds_vex_0F6C = [|e; _VxHxWx; e; e|]

let ds_nor_0F6D = [|e; _VdqWdq; e; e|]

let ds_vex_0F6D = [|e; _VxHxWx; e; e|]

let ds_nor_0F6EB64 = [|_PdEy; _VdqEy; e; e|]

let ds_nor_0F6EB32 = [|_PdEy; _VdqEy; e; e|]

let ds_vex_0F6EB64 = [|e; _VdqEy; e; e|]

let ds_vex_0F6EB32 = [|e; _VdqEy; e; e|]

let ds_nor_0F6F = [|_PqQq; _VdqWdq; _VdqWdq; e|]

let ds_vex_0F6F = [|e; _VxWx; _VxWx; e|]

let ds_evex_0F6FB64 = [|e; _VZxzWZxz; _VZxzWZxz; e|]

let ds_evex_0F6FB32 = [|e; _VZxzWZxz; _VZxzWZxz; e|]

let ds_nor_0F70 = [|_PqQqIb; _VdqWdqIb; _VdqWdqIb; _VdqWdqIb|]

let ds_vex_0F70 = [|e; _VxWxIb; _VxWxIb; _VxWxIb|]

let ds_nor_0F74 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F74 = [|e; _VxHxWx; e; e|]

let ds_nor_0F75 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F75 = [|e; _VxHxWx; e; e|]

let ds_nor_0F76 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F76 = [|e; _VxHxWx; e; e|]

let ds_nor_0F77 = [|e; e; e; e|]

let ds_vex_0F77 = [|e; e; e; e|]

let ds_nor_0F7EB64 = [|_EyPq; _EyVdq; _VdqWdqq; e|]

let ds_nor_0F7EB32 = [|_EyPq; _EyVdq; _VdqWdqq; e|]

let ds_vex_0F7EB64 = [|e; _EyVdq; _VdqWdqq; e|]

let ds_vex_0F7EB32 = [|e; _EyVdq; _VdqWdqq; e|]

let ds_nor_0F7F = [|_QqPq; _WdqVdq; _WdqVdq; e|]

let ds_vex_0F7F = [|e; _WxVx; _WxVx; e|]

let ds_evex_0F7FB64 = [|e; _WZxzVZxz; e; e|]

let ds_evex_0F7FB32 = [|e; _WZxzVZxz; e; e|]

let ds_nor_0FC2 = [|_VdqWdqIb; _VdqWdqIb; _VssWssdIb; _VsdWsdqIb|]

let ds_vex_0FC2 = [|e; e; e; e|]

let ds_nor_0FC4 = [|_PqEdwIb; _VdqEdwIb; e; e|]

let ds_vex_0FC4 = [|e; _VdqHdqEdwIb; e; e|]

let ds_nor_0FC5 = [|_GdNqIb; _GdUdqIb; e; e|]

let ds_vex_0FC5 = [|e; _GdUdqIb; e; e|]

let ds_nor_0FC6 = [|_VdqWdqIb; _VdqWdqIb; e; e|]

let ds_vex_0FC6 = [|_VpsHpsWpsIb; _VpsHpsWpsIb; e; e|]

let ds_nor_0FD1 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FD1 = [|e; _VxHxWdq; e; e|]

let ds_nor_0FD2 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FD2 = [|e; _VxHxWdq; e; e|]

let ds_nor_0FD3 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FD3 = [|e; _VxHxWdq; e; e|]

let ds_nor_0FD4 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FD4 = [|e; _VxHxWx; e; e|]

let ds_nor_0FD5 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FD5 = [|e; _VxHxWx; e; e|]

let ds_nor_0FD6 = [|e; _WdqqVdq; _VdqNq; _PqUdq|]

let ds_vex_0FD6 = [|e; _WdqqVdq; e; e|]

let ds_nor_0FD7 = [|_GdNq; _GdUdq; e; e|]

let ds_vex_0FD7 = [|e; _GyUx; e; e|]

let ds_nor_0FD8 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FD8 = [|e; _VxHxWx; e; e|]

let ds_nor_0FD9 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FD9 = [|e; _VxHxWx; e; e|]

let ds_nor_0FDA = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FDA = [|e; _VxHxWx; e; e|]

let ds_nor_0FDB = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FDB = [|e; _VxHxWx; e; e|]

let ds_nor_0FDC = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FDC = [|e; _VxHxWx; e; e|]

let ds_nor_0FDD = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FDD = [|e; _VxHxWx; e; e|]

let ds_nor_0FDE = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FDE = [|e; _VxHxWx; e; e|]

let ds_nor_0FDF = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FDF = [|e; _VxHxWx; e; e|]

let ds_nor_0FE0 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FE0 = [|e; _VxHxWx; e; e|]

let ds_nor_0FE1 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FE1 = [|e; _VxHxWdq; e; e|]

let ds_nor_0FE2 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FE2 = [|e; _VxHxWdq; e; e|]

let ds_nor_0FE3 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FE3 = [|e; _VxHxWx; e; e|]

let ds_nor_0FE4 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FE4 = [|e; _VxHxWx; e; e|]

let ds_nor_0FE5 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FE5 = [|e; _VxHxWx; e; e|]

let ds_nor_0FE6 = [|e; _VdqWdq; _VdqWdqq; _VdqWdq|]

let ds_vex_0FE6 = [|e; e; e; e|]

let ds_nor_0FE7 = [|_MqPq; _MdqVdq; e; e|]

let ds_vex_0FE7 = [|e; _MxVx; e; e|]

let ds_evex_0FE7B64 = [|e; e; e; e|]

let ds_evex_0FE7B32 = [|e; _MZxzVZxz; e; e|]

let ds_nor_0FE8 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FE8 = [|e; _VxHxWx; e; e|]

let ds_nor_0FE9 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FE9 = [|e; _VxHxWx; e; e|]

let ds_nor_0FEA = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FEA = [|e; _VxHxWx; e; e|]

let ds_nor_0FEB = [|_PqQq; _VdqWdq; e; e; e|]

let ds_vex_0FEB = [|e; _VxHxWx; e; e|]

let ds_nor_0FEC = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FEC = [|e; _VxHxWx; e; e|]

let ds_nor_0FED = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FED = [|e; _VxHxWx; e; e|]

let ds_nor_0FEE = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FEE = [|e; _VxHxWx; e; e|]

let ds_nor_0FEF = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FEF = [|e; _VxHxWx; e; e|]

let ds_nor_0FF0 = [|e; e; e; _VdqMdq|]

let ds_vex_0FF0 = [|e; e; e; _VxMx|]

let ds_nor_0FF1 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FF1 = [|e; _VxHxWdq; e; e|]

let ds_nor_0FF2 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FF2 = [|e; _VxHxWdq; e; e|]

let ds_nor_0FF3 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FF3 = [|e; _VxHxWdq; e; e|]

let ds_nor_0FF4 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FF4 = [|e; _VxHxWx; e; e|]

let ds_nor_0FF5 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FF5 = [|e; _VxHxWx; e; e|]

let ds_nor_0FF6 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FF6 = [|e; _VxHxWx; e; e|]

let ds_nor_0FF8 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FF8 = [|e; _VxHxWx; e; e|]

let ds_nor_0FF9 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FF9 = [|e; _VxHxWx; e; e|]

let ds_nor_0FFA = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FFA = [|e; _VxHxWx; e; e|]

let ds_nor_0FFB = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FFB = [|e; _VxHxWx; e; e|]

let ds_nor_0FFC = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FFC = [|e; _VxHxWx; e; e|]

let ds_nor_0FFD = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FFD = [|e; _VxHxWx; e; e|]

let ds_nor_0FFE = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0FFE = [|e; _VxHxWx; e; e|]

let ds_nor_0F3800 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3800 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3801 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3801 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3802 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3802 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3803 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3803 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3804 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3804 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3805 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3805 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3806 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3806 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3807 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3807 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3808 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3808 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3809 = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F3809 = [|e; _VxHxWx; e; e|]

let ds_nor_0F380A = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F380A = [|e; _VxHxWx; e; e|]

let ds_nor_0F380B = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F380B = [|e; _VxHxWx; e; e|]

let ds_nor_0F380C = [|e; e; e; e|]

let ds_vex_0F380C = [|e; _VxHxWx; e; e|]

let ds_evex_0F380C = [|e; _VxHxWx; e; e|]

let ds_nor_0F380D = [|e; e; e; e|]

let ds_vex_0F380D = [|e; _VxHxWx; e; e|]

let ds_evex_0F380D = [|e; _VxHxWx; e; e|]

let ds_nor_0F380E = [|e; e; e; e|]

let ds_vex_0F380E = [|e; _VxWx; e; e|]

let ds_nor_0F380F = [|e; e; e; e|]

let ds_vex_0F380F = [|e; _VxWx; e; e|]

let ds_nor_0F3810 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3810 = [|e; e; e; e|]

let ds_evex_0F3810B64 = [|e; _VpsHpsWps; e; e|]

let ds_evex_0F3810B32 = [|e; e; _WdqqVdq; e|]

let ds_nor_0F3811 = [|e; e; e; e|]

let ds_vex_0F3811 = [|e; e; e; e|]

let ds_evex_0F3811B64 = [|e; _VpsHpsWps; e; e|]

let ds_evex_0F3811B32 = [|e; e; _WdqdVdq; e|]

let ds_nor_0F3812 = [|e; e; e; e|]

let ds_vex_0F3812 = [|e; e; e; e|]

let ds_evex_0F3812B64 = [|e; _VpsHpsWps; e; e|]

let ds_evex_0F3812B32 = [|e; e; _WdqwVdq; e|]

let ds_nor_0F3813 = [|e; e; e; e|]

let ds_vex_0F3813 = [|e; _VxWdqqdq; e; e|]

let ds_evex_0F3813B64 = [|e; e; e; e|]

let ds_evex_0F3813B32 = [|e; _VxWhsae; _WdqqVdq; e|]

let ds_nor_0F3814 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3814 = [|e; e; e; e|]

let ds_evex_0F3814B64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3814B32 = [|e; _VxHxWx; _WdqdVdq; e|]

let ds_nor_0F3815 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3815 = [|e; e; e; e|]

let ds_evex_0F3815B64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3815B32 = [|e; _VxHxWx; _WdqqVdq; e|]

let ds_nor_0F3816 = [|e; e; e; e|]

let ds_vex_0F3816 = [|e; _VqqHqqWqq; e; e|]

let ds_evex_0F3816B64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3816B32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3817 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3817 = [|e; _VxWx; e; e|]

let ds_nor_0F3818 = [|e; e; e; e|]

let ds_vex_0F3818 = [|e; _VxMd; e; e|]

let ds_evex_0F3818 = [|e; _VZxzWdqd; e; e|]

let ds_nor_0F3819 = [|e; e; e; e|]

let ds_vex_0F3819 = [|e; _VxMq; e; e|]

let ds_evex_0F3819B64 = [|e; _VxWdqq; e; e|]

let ds_evex_0F3819B32 = [|e; _VxWdqq; e; e|]

let ds_nor_0F381A = [|e; e; e; e|]

let ds_vex_0F381A = [|e; _VxMdq; e; e|]

let ds_evex_0F381AB64 = [|e; _VxMdq; e; e|]

let ds_evex_0F381AB32 = [|e; _VxMdq; e; e|]

let ds_nor_0F381B = [|e; e; e; e|]

let ds_vex_0F381B = [|e; e; e; e|]

let ds_evex_0F381BB64 = [|e; _VxMqq; e; e|]

let ds_evex_0F381BB32 = [|e; _VxMqq; e; e|]

let ds_nor_0F381C = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F381C = [|e; _VxWx; e; e|]

let ds_nor_0F381D = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F381D = [|e; _VxWx; e; e|]

let ds_nor_0F381E = [|_PqQq; _VdqWdq; e; e|]

let ds_vex_0F381E = [|e; _VxWx; e; e|]

let ds_nor_0F381F = [|e; e; e; e|]

let ds_vex_0F381F = [|e; e; e; e|]

let ds_evex_0F381FB64 = [|e; _VxWx; e; e|]

let ds_evex_0F381FB32 = [|e; e; e; e|]

let ds_nor_0F3820 = [|e; _VdqWdqq; e; e|]

let ds_vex_0F3820 = [|e; _VxWdqqdq; e; e|]

let ds_nor_0F3821 = [|e; _VdqWdqd; e; e|]

let ds_vex_0F3821 = [|e; _VxWdqdq; e; e|]

let ds_nor_0F3822 = [|e; _VdqWdqw; e; e|]

let ds_vex_0F3822 = [|e; _VxWdqwd; e; e|]

let ds_nor_0F3823 = [|e; _VdqWdqq; e; e|]

let ds_vex_0F3823 = [|e; _VxWdqqdq; e; e|]

let ds_nor_0F3824 = [|e; _VdqWdqd; e; e|]

let ds_vex_0F3824 = [|e; _VxWdqdq; e; e|]

let ds_nor_0F3825 = [|e; _VdqWdqq; e; e|]

let ds_vex_0F3825 = [|e; _VxWdqqdq; e; e|]

let ds_nor_0F3826 = [|e; e; e; e|]

let ds_vex_0F3826 = [|e; e; e; e|]

let ds_evex_0F3826B64 = [|e; _KRHxWx; _KRHxWx; e|]

let ds_evex_0F3826B32 = [|e; _KRHxWx; _KRHxWx; e|]

let ds_nor_0F3827 = [|e; e; e; e|]

let ds_vex_0F3827 = [|e; e; e; e|]

let ds_evex_0F3827B64 = [|e; _KRHxWx; _KRHxWx; e|]

let ds_evex_0F3827B32 = [|e; _KRHxWx; _KRHxWx; e|]

let ds_nor_0F3828 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3828 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3828B64 = [|e; _VxHxWx; _VxKM; e|]

let ds_evex_0F3828B32 = [|e; e; _VxKM; e|]

let ds_nor_0F3829 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3829 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3829B64 = [|e; _KRHxWx; _KRUx; e|]

let ds_evex_0F3829B32 = [|e; e; _KRUx; e|]

let ds_nor_0F382A = [|e; _VdqMdq; e; e|]

let ds_vex_0F382A = [|e; _VxMx; e; e|]

let ds_evex_0F382AB64 = [|e; e; _VxKM; e|]

let ds_evex_0F382AB32 = [|e; _VxMx; e; e|]

let ds_nor_0F382B = [|e; _VdqWdq; e; e|]

let ds_vex_0F382B = [|e; _VxHxWx; e; e|]

let ds_evex_0F382BB64 = [|e; e; e; e|]

let ds_evex_0F382BB32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F382C = [|e; e; e; e|]

let ds_vex_0F382C = [|e; _VxHxMx; e; e|]

let ds_evex_0F382CB64 = [|e; e; e; e|]

let ds_evex_0F382CB32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F382D = [|e; e; e; e|]

let ds_vex_0F382D = [|e; _VxHxMx; e; e|]

let ds_evex_0F382DB64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F382DB32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F382E = [|e; e; e; e|]

let ds_vex_0F382E = [|e; _MxHxVx; e; e|]

let ds_nor_0F382F = [|e; e; e; e|]

let ds_vex_0F382F = [|e; _MxHxVx; e; e|]

let ds_nor_0F3830 = [|e; _VdqWdqq; e; e|]

let ds_vex_0F3830 = [|e; _VxWdqqdq; e; e|]

let ds_nor_0F3831 = [|e; _VdqWdqd; e; e|]

let ds_vex_0F3831 = [|e; _VxWdqdq; e; e|]

let ds_nor_0F3832 = [|e; _VdqWdqw; e; e|]

let ds_vex_0F3832 = [|e; _VxWdqwd; e; e|]

let ds_nor_0F3833 = [|e; _VdqWdqq; e; e|]

let ds_vex_0F3833 = [|e; _VxWdqqdq; e; e|]

let ds_nor_0F3834 = [|e; _VdqWdqd; e; e|]

let ds_vex_0F3834 = [|e; _VxWdqdq; e; e|]

let ds_nor_0F3835 = [|e; _VdqWdqq; e; e|]

let ds_vex_0F3835 = [|e; _VxWdqqdq; e; e|]

let ds_nor_0F3836 = [|e; e; e; e|]

let ds_vex_0F3836 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3836B64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3836B32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3837 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3837 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3838 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3838 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3839 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3839 = [|e; _VxHxWx; e; e|]

let ds_nor_0F383A = [|e; _VdqWdq; e; e|]

let ds_vex_0F383A = [|e; _VxHxWx; e; e|]

let ds_evex_0F383AB64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F383AB32 = [|e; _VxHxWx; _VxKM; e|]

let ds_nor_0F383B = [|e; _VdqWdq; e; e|]

let ds_vex_0F383B = [|e; _VxHxWx; e; e|]

let ds_evex_0F383BB64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F383BB32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F383C = [|e; _VdqWdq; e; e|]

let ds_vex_0F383C = [|e; _VxHxWx; e; e|]

let ds_nor_0F383D = [|e; _VdqWdq; e; e|]

let ds_vex_0F383D = [|e; _VxHxWx; e; e|]

let ds_evex_0F383DB64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F383DB32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F383E = [|e; _VdqWdq; e; e|]

let ds_vex_0F383E = [|e; _VxHxWx; e; e|]

let ds_nor_0F383F = [|e; _VdqWdq; e; e|]

let ds_vex_0F383F = [|e; _VxHxWx; e; e|]

let ds_evex_0F383FB64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F383FB32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3840 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3840 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3840B64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3840B32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F3841 = [|e; _VdqWdq; e; e|]

let ds_vex_0F3841 = [|e; _VdqWdq; e; e|]

let ds_nor_0F3846 = [|e; e; e; e|]

let ds_vex_0F3846B64 = [|e; e; e; e|]

let ds_vex_0F3846B32 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3846B64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F3846B32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F384C = [|e; e; e; e|]

let ds_vex_0F384C = [|e; e; e; e|]

let ds_evex_0F384CB64 = [|e; _VxWx; e; e|]

let ds_evex_0F384CB32 = [|e; _VxWx; e; e|]

let ds_nor_0F384D = [|e; e; e; e|]

let ds_vex_0F384D = [|e; e; e; e|]

let ds_evex_0F384DB64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F384DB32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F384E = [|e; e; e; e|]

let ds_vex_0F384E = [|e; e; e; e|]

let ds_evex_0F384EB64 = [|e; _VxWx; e; e|]

let ds_evex_0F384EB32 = [|e; _VxWx; e; e|]

let ds_nor_0F384F = [|e; e; e; e|]

let ds_vex_0F384F = [|e; e; e; e|]

let ds_evex_0F384FB64 = [|e; _VxHxWx; e; e|]

let ds_evex_0F384FB32 = [|e; _VxHxWx; e; e|]

let ds_nor_0F385A = [|e; e; e; e|]

let ds_vex_0F385A = [|e; _VqqMdq; e; e|]

let ds_nor_0F3878 = [|e; e; e; e|]

let ds_vex_0F3878 = [|e; _VxWx; e; e|]

let ds_nor_0F38F0 = [|_GyMy; _GwMw; e; _GvEb; _GdEb|]

let ds_nor_0F38F1 = [|_MyGy; _MwGw; e; _GvEy; _GdEw|]

let ds_nor_0F38F2 = [|e; e; e; e|]

let ds_vex_0F38F2 = [|_GyByEy; e; e; e|]

let ds_nor_0F38F5 = [|e; e; e; e|]

let ds_vex_0F38F5 = [|_GyEyBy; e; e; e|]

let ds_nor_0F38F6 = [|e; _GvEv; _GvEv; e|]

let ds_vex_0F38F6 = [|e; e; e; _GyByEy|]

let ds_nor_0F38F7 = [|e; e; e; e|]

let ds_vex_0F38F7 = [|_GyEyBy; _GyEyBy; _GyEyBy; _GyEyBy|]

let ds_nor_0F38F8 = [|e; _GdqaMZqqq; _GdqaMZqqq; _GdqaMZqqq|]

let ds_vex_0F38F8 = [|e; e; e; e|]

let ds_nor_0F3A00 = [|e; e; e; e|]

let ds_vex_0F3A00 = [|e; _VyWyIb; e; e|]

let ds_evex_0F3A00B64 = [|e; _VyWyIb; e; e|]

let ds_evex_0F3A00B32 = [|e; e; e; e|]

let ds_nor_0F3A01 = [|e; e; e; e|]

let ds_vex_0F3A01 = [|e; _VyWyIb; e; e|]

let ds_evex_0F3A01B64 = [|e; _VyWyIb; e; e|]

let ds_evex_0F3A01B32 = [|e; e; e; e|]

let ds_nor_0F3A02 = [|e; e; e; e|]

let ds_vex_0F3A02 = [|e; _VxHxWxIb; e; e|]

let ds_nor_0F3A03 = [|e; e; e; e|]

let ds_vex_0F3A03 = [|e; e; e; e|]

let ds_evex_0F3A03B64 = [|e; _VxHxWxIb; e; e|]

let ds_evex_0F3A03B32 = [|e; _VxHxWxIb; e; e|]

let ds_nor_0F3A04 = [|e; e; e; e|]

let ds_vex_0F3A04 = [|e; e; e; e|]

let ds_evex_0F3A04B64 = [|e; e; e; e|]

let ds_evex_0F3A04B32 = [|e; _VxWxIb; e; e|]

let ds_nor_0F3A05 = [|e; e; e; e|]

let ds_vex_0F3A05 = [|e; e; e; e|]

let ds_evex_0F3A05B64 = [|e; _VxWxIb; e; e|]

let ds_evex_0F3A05B32 = [|e; e; e; e|]

let ds_nor_0F3A06 = [|e; e; e; e|]

let ds_vex_0F3A06 = [|e; _VxHxWxIb; e; e|]

let ds_nor_0F3A08 = [|e; _VxWxIb; e; e|]

let ds_vex_0F3A08 = [|e; _VxWxIb; e; e|]

let ds_evex_0F3A08B64 = [|e; e; e; e|]

let ds_evex_0F3A08B32 = [|e; _VxWxIb; e; e|]

let ds_nor_0F3A09 = [|e; _VxWxIb; e; e|]

let ds_vex_0F3A09 = [|e; _VxWxIb; e; e|]

let ds_evex_0F3A09B64 = [|e; _VxWxIb; e; e|]

let ds_evex_0F3A09B32 = [|e; e; e; e|]

let ds_nor_0F3A0A = [|e; _VdqWdqIb; e; e|]

let ds_vex_0F3A0A = [|e; _VdqHdqWdqIb; e; e|]

let ds_evex_0F3A0AB64 = [|e; e; e; e|]

let ds_evex_0F3A0AB32 = [|e; _VdqHdqWdqIb; e; e|]

let ds_nor_0F3A0B = [|e; _VdqWdqIb; e; e|]

let ds_vex_0F3A0B = [|e; _VdqHdqWdqIb; e; e|]

let ds_evex_0F3A0BB64 = [|e; _VdqHdqWdqIb; e; e|]

let ds_evex_0F3A0BB32 = [|e; e; e; e|]

let ds_nor_0F3A0C = [|e; _VxWxIb; e; e|]

let ds_vex_0F3A0C = [|e; _VxHxWxIb; e; e|]

let ds_nor_0F3A0D = [|e; _VxWxIb; e; e|]

let ds_vex_0F3A0D = [|e; _VxHxWxIb; e; e|]

let ds_nor_0F3A0E = [|e; _VxWxIb; e; e|]

let ds_vex_0F3A0E = [|e; _VxHxWxIb; e; e|]

let ds_nor_0F3A0F = [|_PqQqIb; _VdqWdqIb; e; e|]

let ds_vex_0F3A0F = [|e; _VxHxWxIb; e; e|]

let ds_nor_0F3A14 = [|e; _EybVdqIb; e; e|]

let ds_vex_0F3A14 = [|e; _EybVdqIb; e; e|]

let ds_nor_0F3A15 = [|e; _EywVdqIb; e; e|]

let ds_vex_0F3A15 = [|e; _EywVdqIb; e; e|]

let ds_nor_0F3A16B64 = [|e; _EqVdqIb; e; e|]

let ds_nor_0F3A16B32 = [|e; _EdVdqIb; e; e|]

let ds_vex_0F3A16B64 = [|e; _EqVdqIb; e; e|]

let ds_vex_0F3A16B32 = [|e; _EdVdqIb; e; e|]

let ds_evex_0F3A16B64 = [|e; _EqVdqIb; e; e|]

let ds_evex_0F3A16B32 = [|e; _EdVdqIb; e; e|]

let ds_nor_0F3A17 = [|e; _EydVdqIb; e; e|]

let ds_vex_0F3A17 = [|e; _EydVdqIb; e; e|]

let ds_nor_0F3A18 = [|e; e; e; e|]

let ds_vex_0F3A18 = [|e; _VxHxWdqIb; e; e|]

let ds_evex_0F3A18B64 = [|e; _VxHxWdqIb; e; e|]

let ds_evex_0F3A18B32 = [|e; _VxHxWdqIb; e; e|]

let ds_nor_0F3A19 = [|e; e; e; e|]

let ds_vex_0F3A19 = [|e; _WdqVqqIb; e; e|]

let ds_evex_0F3A19B64 = [|e; _WdqVxIb; e; e|]

let ds_evex_0F3A19B32 = [|e; _WdqVxIb; e; e|]

let ds_nor_0F3A1A = [|e; e; e; e|]

let ds_vex_0F3A1A = [|e; e; e; e|]

let ds_evex_0F3A1AB64 = [|e; _VqqqHqqqWqqIb; e; e|]

let ds_evex_0F3A1AB32 = [|e; _VqqqHqqqWqqIb; e; e|]

let ds_nor_0F3A1B = [|e; e; e; e|]

let ds_vex_0F3A1B = [|e; e; e; e|]

let ds_evex_0F3A1BB64 = [|e; _WqqVqqqIb; e; e|]

let ds_evex_0F3A1BB32 = [|e; _WqqVqqqIb; e; e|]

let ds_nor_0F3A1D = [|e; e; e; e|]

let ds_vex_0F3A1D = [|e; _WpsqVxIb; e; e|]

let ds_evex_0F3A1DB64 = [|e; e; e; e|]

let ds_evex_0F3A1DB32 = [|e; _WpsqVxIb; e; e|]

let ds_nor_0F3A1E = [|e; e; e; e|]

let ds_vex_0F3A1E = [|e; e; e; e|]

let ds_evex_0F3A1EB64 = [|e; _KRHxWxIb; e; e|]

let ds_evex_0F3A1EB32 = [|e; _KRHxWxIb; e; e|]

let ds_nor_0F3A1F = [|e; e; e; e|]

let ds_vex_0F3A1F = [|e; e; e; e|]

let ds_evex_0F3A1FB64 = [|e; _KRHxWxIb; e; e|]

let ds_evex_0F3A1FB32 = [|e; _KRHxWxIb; e; e|]

let ds_nor_0F3A20 = [|e; _VdqEdbIb; e; e|]

let ds_vex_0F3A20 = [|e; _VdqHdqEdqbIb; e; e|]

let ds_nor_0F3A21 = [|e; _VdqEdqdIb; e; e|]

let ds_vex_0F3A21 = [|e; _VdqHdqEdqdIb; e; e|]

let ds_nor_0F3A22B64 = [|e; _VdqEqIb; e; e|]

let ds_nor_0F3A22B32 = [|e; _VdqEdIb; e; e|]

let ds_vex_0F3A22B64 = [|e; _VdqHdqEqIb; e; e|]

let ds_vex_0F3A22B32 = [|e; _VdqHdqEdIb; e; e|]

let ds_evex_0F3A22B64 = [|e; _VdqHdqEqIb; e; e|]

let ds_evex_0F3A22B32 = [|e; _VdqHdqEdIb; e; e|]

let ds_nor_0F3A38 = [|e; e; e; e|]

let ds_vex_0F3A38 = [|e; _VqqHqqWdqIb; e; e|]

let ds_nor_0F3A39 = [|e; e; e; e|]

let ds_vex_0F3A39 = [|e; _WdqVqqIb; e; e|]

let ds_nor_0F3A60 = [|e; _VdqWdqIb; e; e|]

let ds_vex_0F3A60 = [|e; _VdqWdqIb; e; e|]

let ds_nor_0F3A61 = [|e; _VdqWdqIb; e; e|]

let ds_vex_0F3A61 = [|e; _VdqWdqIb; e; e|]

let ds_nor_0F3A62 = [|e; _VdqWdqIb; e; e|]

let ds_vex_0F3A62 = [|e; _VdqWdqIb; e; e|]

let ds_nor_0F3A63 = [|e; _VdqWdqIb; e; e|]

let ds_vex_0F3A63 = [|e; _VdqWdqIb; e; e|]

let ds_nor_0F3A0B = [|e; _VsdWsdIb; e; e|]

let ds_vex_0F3A0B = [|e; _VsdHsdWsdIb; e; e|]

let ds_nor_0F3AF0 = [|e; e; e; e|]

let ds_vex_0F3AF0 = [|e; e; e; _GyEyIb|]

let ds_empty = [|e; e; e; e|]
