open Base
open Ll2l_ir
open Ll2l_std

type (_, _, _, _) t =
  | X86 :
      X86.Mode.t
      -> (X86.Instruction.t, X86_decode.error, X86_lift.error, unit) t
  | MIPS :
      Mips.Mode.t * Endian.t
      -> (Mips.Instruction.t, Mips_decode.error, Mips_lift.error, unit) t
  | ARM :
      Arm.Mode.t * Endian.t
      -> ( Arm.Instruction.t
         , Arm_decode.error
         , Arm_lift.error
         , Arm_decode.Context.t )
         t
  | RISCV :
      Riscv.Mode.t
      -> (Riscv.Instruction.t, Riscv_decode.error, Riscv_lift.error, unit) t

(* existential type for arch *)
type et = T : ('a, 'b, 'c, 'd) t -> et [@@unboxed]

(* existential type for arch + instruction *)
type ei = I : ('a, 'b, 'c, 'd) t * 'a -> ei

(* existential type for arch + decode context *)
type ec = C : ('a, 'b, 'c, 'd) t * 'd -> ec

(* existential type for arch + instruction + decode context *)
type eic = IC : ('a, 'b, 'c, 'd) t * 'a * 'd -> eic

let of_string = function
  | "ia32" -> T (X86 IA32)
  | "amd64" -> T (X86 AMD64)
  | "mips32le" -> T (MIPS (Mips32, `LE))
  | "mips32r2le" -> T (MIPS (Mips32r2, `LE))
  | "mips32r6le" -> T (MIPS (Mips32r6, `LE))
  | "mips64le" -> T (MIPS (Mips64, `LE))
  | "mips64r2le" -> T (MIPS (Mips64r2, `LE))
  | "mips64r6le" -> T (MIPS (Mips64r6, `LE))
  | "mips32be" -> T (MIPS (Mips32, `BE))
  | "mips32r2be" -> T (MIPS (Mips32r2, `BE))
  | "mips32r6be" -> T (MIPS (Mips32r6, `BE))
  | "mips64be" -> T (MIPS (Mips64, `BE))
  | "mips64r2be" -> T (MIPS (Mips64r2, `BE))
  | "mips64r6be" -> T (MIPS (Mips64r6, `BE))
  | "armv7le" -> T (ARM (Armv7, `LE))
  | "armv7be" -> T (ARM (Armv7, `BE))
  | "thumbv7le" -> T (ARM (Thumbv7, `LE))
  | "thumbv7be" -> T (ARM (Thumbv7, `BE))
  | "armv8le" -> T (ARM (Armv8, `LE))
  | "armv8be" -> T (ARM (Armv8, `BE))
  | "riscv32" -> T (RISCV (RV32 {c= false; flen= None}))
  | "riscv32.fs" -> T (RISCV (RV32 {c= false; flen= Some Riscv.Flen.S}))
  | "riscv32.fd" -> T (RISCV (RV32 {c= false; flen= Some Riscv.Flen.D}))
  | "riscv32.fq" -> T (RISCV (RV32 {c= false; flen= Some Riscv.Flen.Q}))
  | "riscv64" -> T (RISCV (RV64 {c= false; flen= None}))
  | "riscv64.fs" -> T (RISCV (RV64 {c= false; flen= Some Riscv.Flen.S}))
  | "riscv64.fd" -> T (RISCV (RV64 {c= false; flen= Some Riscv.Flen.D}))
  | "riscv64.fq" -> T (RISCV (RV64 {c= false; flen= Some Riscv.Flen.Q}))
  | "riscv32.c" -> T (RISCV (RV32 {c= true; flen= None}))
  | "riscv32.c.fs" -> T (RISCV (RV32 {c= true; flen= Some Riscv.Flen.S}))
  | "riscv32.c.fd" -> T (RISCV (RV32 {c= true; flen= Some Riscv.Flen.D}))
  | "riscv32.c.fq" -> T (RISCV (RV32 {c= true; flen= Some Riscv.Flen.Q}))
  | "riscv64.c" -> T (RISCV (RV64 {c= true; flen= None}))
  | "riscv64.c.fs" -> T (RISCV (RV64 {c= true; flen= Some Riscv.Flen.S}))
  | "riscv64.c.fd" -> T (RISCV (RV64 {c= true; flen= Some Riscv.Flen.D}))
  | "riscv64.c.fq" -> T (RISCV (RV64 {c= true; flen= Some Riscv.Flen.Q}))
  | _ -> invalid_arg "bad arch name"

let to_string : type a b c d. (a, b, c, d) t -> string = function
  | X86 mode -> X86.Mode.to_string mode
  | MIPS (mode, endian) -> Mips.Mode.to_string mode ^ Endian.to_string endian
  | ARM (mode, endian) -> Arm.Mode.to_string mode ^ Endian.to_string endian
  | RISCV mode -> Riscv.Mode.to_string mode

let compare : type a b c d e f g h. (a, b, c, d) t -> (e, f, g, h) t -> int =
 fun arch arch' -> String.compare (to_string arch) (to_string arch')

let equal : type a b c d e f g h. (a, b, c, d) t -> (e, f, g, h) t -> bool =
 fun arch arch' -> compare arch arch' = 0

let word_size_of : type a b c d. (a, b, c, d) t -> int = function
  | X86 mode -> X86.Mode.word_size_of mode
  | MIPS (mode, _) -> Mips.Mode.word_size_of mode
  | ARM _ -> Arm.Mode.word_size
  | RISCV mode -> Riscv.Mode.word_size_of mode

let endian_of : type a b c d. (a, b, c, d) t -> Endian.t = function
  | X86 _ -> `LE
  | MIPS (_, endian) -> endian
  | ARM (_, endian) -> endian
  | RISCV _ -> `LE

let create_decode_ctx : type a b c d. (a, b, c, d) t -> d = function
  | X86 _ -> ()
  | MIPS _ -> ()
  | ARM _ -> Arm_decode.Context.create ()
  | RISCV _ -> ()

let clone_decode_ctx : type a b c d. (a, b, c, d) t -> d -> d =
 fun arch ctx ->
  match arch with
  | X86 _ -> ()
  | MIPS _ -> ()
  | ARM _ -> Arm_decode.Context.clone ctx
  | RISCV _ -> ()

let switch_decode_ctx :
    type a b c d e f g h. (a, b, c, d) t -> (e, f, g, h) t -> d -> h =
 fun arch_from arch_to ctx ->
  match (arch_from, arch_to) with
  | X86 _, X86 _ -> clone_decode_ctx arch_from ctx
  | X86 _, _ -> create_decode_ctx arch_to
  | MIPS _, MIPS _ -> clone_decode_ctx arch_from ctx
  | MIPS _, _ -> create_decode_ctx arch_to
  | ARM _, ARM _ -> clone_decode_ctx arch_from ctx
  | ARM _, _ -> create_decode_ctx arch_to
  | RISCV _, RISCV _ -> clone_decode_ctx arch_from ctx
  | RISCV _, _ -> create_decode_ctx arch_to

(* this is to handle switching the ISA depending
 * on the contents of the address (e.g. ARM/Thumb) *)
let addr_of_arch arch addr =
  match arch with
  | T (ARM (mode, endian)) ->
      let new_arch thumb =
        match mode with
        | _ when thumb -> ARM (Arm.Mode.Thumbv7, endian)
        | Arm.Mode.Thumbv7 when not thumb -> ARM (Arm.Mode.Armv7, endian)
        | _ -> ARM (mode, endian)
      in
      let new_addr = Addr.(addr land int32 0xFFFFFFFEl) in
      (new_addr, T (new_arch Addr.(addr land one = one)))
  | _ -> (addr, arch)

let decode : type a b c d. (a, b, c, d) t -> d -> bytes -> (a, b) Result.t =
 fun arch ctx bytes ->
  match arch with
  | X86 mode -> X86_decode.decode mode bytes
  | MIPS (mode, endian) -> Mips_decode.decode mode endian bytes
  | ARM (mode, endian) -> Arm_decode.decode ctx mode endian bytes
  | RISCV mode -> Riscv_decode.decode mode bytes

let decode_exn : type a b c d. (a, b, c, d) t -> d -> bytes -> a =
 fun arch ctx bytes ->
  match arch with
  | X86 mode -> X86_decode.decode_exn mode bytes
  | MIPS (mode, endian) -> Mips_decode.decode_exn mode endian bytes
  | ARM (mode, endian) -> Arm_decode.decode_exn ctx mode endian bytes
  | RISCV mode -> Riscv_decode.decode_exn mode bytes

let lift :
    type a b c d.
       ?delay:a list
    -> (a, b, c, d) t
    -> a
    -> Addr.t
    -> Il.Context.t
    -> (Il.t, c) Result.t =
 fun ?(delay = []) arch instr addr ctx ->
  match arch with
  | X86 _ -> X86_lift.lift instr addr ctx
  | MIPS _ -> Mips_lift.lift instr addr ctx ~delay:(List.hd delay)
  | ARM _ -> Arm_lift.lift instr addr ctx
  | RISCV _ -> Riscv_lift.lift instr addr ctx

let lift_exn :
    type a b c d.
    ?delay:a list -> (a, b, c, d) t -> a -> Addr.t -> Il.Context.t -> Il.t =
 fun ?(delay = []) arch instr addr ctx ->
  match arch with
  | X86 _ -> X86_lift.lift_exn instr addr ctx
  | MIPS _ -> Mips_lift.lift_exn instr addr ctx ~delay:(List.hd delay)
  | ARM _ -> Arm_lift.lift_exn instr addr ctx
  | RISCV _ -> Riscv_lift.lift_exn instr addr ctx

let min_instr_length : type a b c d. (a, b, c, d) t -> int = function
  | X86 _ -> 1
  | MIPS _ -> Mips.Instruction.min_length
  | ARM (mode, _) -> Arm.Mode.min_instr_length mode
  | RISCV mode -> Riscv.Mode.min_instr_length mode

let max_instr_length : type a b c d. (a, b, c, d) t -> int = function
  | X86 _ -> X86.Instruction.max_length
  | MIPS _ -> Mips.Instruction.max_length
  | ARM _ -> Arm.Instruction.max_length
  | RISCV _ -> Riscv.Instruction.max_length

let instr_length : type a b c d. (a, b, c, d) t -> a -> int =
 fun arch instr ->
  match arch with
  | X86 _ -> X86.Instruction.length instr
  | MIPS _ -> Mips.Instruction.length
  | ARM _ -> Arm.Instruction.length instr
  | RISCV _ -> Riscv.Instruction.length instr

let instr_end_addr :
    type a b c d. (a, b, c, d) t -> a -> addr:Addr.t -> Addr.t =
 fun arch instr ~addr ->
  match arch with
  | X86 _ -> X86.Instruction.end_addr instr ~addr
  | MIPS _ -> Mips.Instruction.end_addr ~addr
  | ARM _ -> Arm.Instruction.end_addr instr ~addr
  | RISCV _ -> Riscv.Instruction.end_addr instr ~addr

let no_fallthrough_instr : type a b c d. (a, b, c, d) t -> a -> bool =
 fun arch instr ->
  match arch with
  | X86 _ -> X86.Instruction.no_fallthrough instr
  | MIPS _ -> Mips.Instruction.no_fallthrough instr
  | ARM _ -> Arm.Instruction.no_fallthrough instr
  | RISCV _ -> Riscv.Instruction.no_fallthrough instr

let may_fallthrough_instr : type a b c d. (a, b, c, d) t -> a -> bool =
 fun arch instr ->
  match arch with
  | X86 _ -> X86.Instruction.may_fallthrough instr
  | MIPS _ -> Mips.Instruction.may_fallthrough instr
  | ARM _ -> Arm.Instruction.may_fallthrough instr
  | RISCV _ -> Riscv.Instruction.may_fallthrough instr

let must_fallthrough_instr : type a b c d. (a, b, c, d) t -> a -> bool =
 fun arch instr ->
  match arch with
  | X86 _ -> X86.Instruction.must_fallthrough instr
  | MIPS _ -> Mips.Instruction.must_fallthrough instr
  | ARM _ -> Arm.Instruction.must_fallthrough instr
  | RISCV _ -> Riscv.Instruction.must_fallthrough instr

let is_terminator_instr : type a b c d. (a, b, c, d) t -> a -> bool =
 fun arch instr ->
  match arch with
  | X86 _ -> X86.Instruction.is_terminator instr
  | MIPS _ -> Mips.Instruction.is_terminator instr
  | ARM _ -> Arm.Instruction.is_terminator instr
  | RISCV _ -> Riscv.Instruction.is_terminator instr

let delay_slot_size : type a b c d. (a, b, c, d) t -> a -> int =
 fun arch instr ->
  match arch with
  | X86 _ -> 0
  | MIPS _ -> Mips.Instruction.delay_slot_size instr
  | ARM _ -> 0
  | RISCV _ -> 0

let is_conditional_branch : type a b c d. (a, b, c, d) t -> a -> bool =
 fun arch instr ->
  match arch with
  | X86 _ -> X86.Instruction.is_conditional_branch instr
  | MIPS _ -> Mips.Instruction.is_conditional_branch instr
  | ARM _ -> Arm.Instruction.is_conditional_branch instr
  | RISCV _ -> Riscv.Instruction.is_conditional_branch instr

let has_conditional_delay : type a b c d. (a, b, c, d) t -> a -> bool =
 fun arch instr ->
  match arch with
  | X86 _ -> false
  | MIPS _ -> Mips.Instruction.has_conditional_delay instr
  | ARM _ -> false
  | RISCV _ -> false

let branch_address_of_instr :
    type a b c d. (a, b, c, d) t -> a -> addr:Addr.t -> Addr.t option =
 fun arch instr ~addr ->
  match arch with
  | X86 _ -> X86.Instruction.branch_address instr ~addr
  | MIPS _ -> Mips.Instruction.branch_address instr ~addr
  | ARM _ -> Arm.Instruction.branch_address instr ~addr
  | RISCV _ -> Riscv.Instruction.branch_address instr ~addr

let string_of_instr :
    type a b c d. (a, b, c, d) t -> a -> addr:Addr.t -> string =
 fun arch instr ~addr ->
  match arch with
  | X86 _ -> X86.Instruction.to_string instr ~addr
  | MIPS _ -> Mips.Instruction.to_string instr ~addr
  | ARM _ -> Arm.Instruction.to_string instr ~addr
  | RISCV _ -> Riscv.Instruction.to_string instr ~addr

let string_of_instr_full :
    type a b c d. (a, b, c, d) t -> a -> addr:Addr.t -> string =
 fun arch instr ~addr ->
  match arch with
  | X86 _ -> X86.Instruction.to_string_full instr ~addr
  | MIPS _ -> Mips.Instruction.to_string_full instr ~addr
  | ARM _ -> Arm.Instruction.to_string_full instr ~addr
  | RISCV _ -> Riscv.Instruction.to_string_full instr ~addr

let string_of_decode_err : type a b c d. (a, b, c, d) t -> b -> string =
 fun arch err ->
  match arch with
  | X86 _ -> X86_decode.string_of_error err
  | MIPS _ -> Mips_decode.string_of_error err
  | ARM _ -> Arm_decode.string_of_error err
  | RISCV _ -> Riscv_decode.string_of_error err

let string_of_lift_err : type a b c d. (a, b, c, d) t -> c -> string =
 fun arch err ->
  match arch with
  | X86 _ -> X86_lift.string_of_error err
  | MIPS _ -> Mips_lift.string_of_error err
  | ARM _ -> Arm_lift.string_of_error err
  | RISCV _ -> Riscv_lift.string_of_error err

let is_stack_pointer_var : type a b c d. (a, b, c, d) t -> Il.var -> bool =
 fun arch var ->
  match (var.size, var.hint) with
  | Some _, Il.Var_hint.Reg -> (
    match arch with
    | X86 _ -> (
      match X86.Register.of_string var.name with
      | Some SPL | Some SP | Some ESP | Some RSP -> true
      | _ -> false )
    | MIPS _ -> (
      match Mips.Register.of_string var.name with
      | Some SP -> true
      | _ -> false )
    | ARM _ -> (
      match Arm.Register.of_string var.name with
      | Some SP -> true
      | _ -> false )
    | RISCV _ -> (
      match Riscv.Register.of_string var.name with
      | Some SP -> true
      | _ -> false ) )
  | _ -> false
