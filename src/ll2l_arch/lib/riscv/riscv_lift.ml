open Core_kernel
open Ll2l_ir
open Ll2l_std
module I = Riscv.Instruction
module Imm = Riscv.Immediate
module M = Riscv.Mnemonic
module Mode = Riscv.Mode
module O = Riscv.Operand
module R = Riscv.Register

type error = [`BadInstr | `BadOperand of int]

let string_of_error = function
  | `BadInstr -> "bad instruction"
  | `BadOperand i -> Printf.sprintf "bad operand (%d)" i

exception Riscv_lift_error of error

let make_reg r is64 =
  let w = R.width_of r in
  let s = if is64 then w.width64 else w.width32 in
  Il.reg (R.to_string r) s

let make_operand (instr : I.t) addr i =
  let word_sz = Mode.word_size_of instr.mode in
  let is64 = word_sz = 64 in
  match instr.operands.(i) with
  | O.Reg r -> Ok (make_reg r is64)
  | O.(Imm {value; kind= Literal; size}) -> Ok (Il.num64 value size)
  | O.(Imm {value; kind= Relative; size}) ->
      Ok (Il.bitv Addr.(addr + int64 value) word_sz)
  | O.(Mem {base; disp; size}) ->
      let b = make_reg base is64 in
      let loc =
        if Int64.is_negative disp then
          let disp = Int64.(lnot disp + 1L) in
          Il.(b - num64 disp word_sz)
        else Il.(b + num64 disp word_sz)
      in
      Ok Il.(load `LE Int.(size lsr 3) mu loc)
  | O.(Memaddr {base; disp= None}) -> Ok (make_reg base is64)
  | O.(Memaddr {base; disp= Some disp}) ->
      let b = make_reg base is64 in
      if Int64.is_negative disp then
        let disp = Int64.(lnot disp + 1L) in
        Ok Il.(b - num64 disp word_sz)
      else Ok Il.(b + num64 disp word_sz)

let zero sz = Il.reg "ZERO" sz

let ra sz = Il.reg "RA" sz

let replace_zero word_sz stmts =
  let zero = zero word_sz in
  let zeroi = Il.num 0 word_sz in
  let sub = (zero, zeroi) in
  let rec aux res = function
    | [] -> List.rev res
    | stmt :: rest -> (
      match stmt with
      | Il.Assign (dst, _) when Il.equal_expr dst zero ->
          (* Chapter 2.1: "Register x0 is hardwired with all bits equal to
             0." *)
          aux res rest
      | _ ->
          let stmt = Il.substitute_expr_in_stmt stmt ~sub in
          aux (stmt :: res) rest )
  in
  aux [] stmts

let lift (instr : I.t) addr ctx =
  let open Result.Let_syntax in
  let endaddr = I.end_addr ~addr instr in
  let new_label () = Il.Context.label ctx addr in
  let new_tmp size = Il.Context.tmp ctx size in
  let start_lbl = new_label () in
  let word_sz = Mode.word_size_of instr.mode in
  let is64 = word_sz = 64 in
  let o = make_operand instr addr in
  let assign_mem dst src =
    match dst with
    | Il.Load r -> Il.(r.mem := store_r r src)
    | _ -> Il.(dst := src)
  in
  let%map stmts =
    match instr.mnemonic with
    | M.ADD ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := rs1 + rs2]
    | M.ADDI ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rd := rs1 + imm]
    | M.ADDIW ->
        let%bind rd = o 1 in
        let%bind rs1 = o 1 in
        let%bind imm = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs1; t1 := t1 + lo 32 imm; rd := se 64 t1]
    | M.ADDW ->
        let%bind rd = o 1 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok
          Il.[t1 := lo 32 rs1; t2 := lo 32 rs2; t3 := t1 + t2; rd := se 64 t3]
    | M.AND ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := rs1 & rs2]
    | M.ANDI ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rd := rs1 & imm]
    | M.AUIPC ->
        let%bind rd = o 0 in
        let%bind imm = o 1 in
        Ok Il.[rd := (imm << num 12 word_sz) + bitv addr word_sz]
    | M.BEQ ->
        let%bind rs1 = o 0 in
        let%bind rs2 = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs1 = rs2) rel (bitv endaddr word_sz)]
    | M.BGE ->
        let%bind rs1 = o 0 in
        let%bind rs2 = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs1 >=$ rs2) rel (bitv endaddr word_sz)]
    | M.BGEU ->
        let%bind rs1 = o 0 in
        let%bind rs2 = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs1 >= rs2) rel (bitv endaddr word_sz)]
    | M.BLT ->
        let%bind rs1 = o 0 in
        let%bind rs2 = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs1 <$ rs2) rel (bitv endaddr word_sz)]
    | M.BLTU ->
        let%bind rs1 = o 0 in
        let%bind rs2 = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs1 < rs2) rel (bitv endaddr word_sz)]
    | M.BNE ->
        let%bind rs1 = o 0 in
        let%bind rs2 = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs1 <> rs2) rel (bitv endaddr word_sz)]
    | M.C_ADD ->
        let%bind rd = o 0 in
        let%bind rs2 = o 1 in
        Ok Il.[rd := rd + rs2]
    | M.C_ADDI ->
        let%bind rd = o 0 in
        let%bind imm = o 1 in
        Ok Il.[rd := rd + imm]
    | M.C_ADDIW ->
        let%bind rd = o 0 in
        let%bind imm = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rd; t1 := t1 + lo 32 imm; rd := se 64 t1]
    | M.C_ADDW ->
        let%bind rd = o 0 in
        let%bind rs2 = o 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rd; t2 := lo 32 rs2; t3 := t1 + t2; rd := se 64 t3]
    | M.C_ADDI16SP ->
        let%bind sp = o 0 in
        let%bind imm = o 1 in
        Ok Il.[sp := sp + imm]
    | M.C_ADDI4SPN ->
        let%bind rd = o 0 in
        let%bind sp = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rd := sp + imm]
    | M.C_AND ->
        let%bind rd = o 0 in
        let%bind rs2 = o 1 in
        Ok Il.[rd := rd & rs2]
    | M.C_ANDI ->
        let%bind rd = o 0 in
        let%bind imm = o 1 in
        Ok Il.[rd := rd & imm]
    | M.C_BEQZ ->
        let%bind rs1 = o 0 in
        let%bind rel = o 1 in
        Ok Il.[jmp_if (rs1 = num 0 word_sz) rel (bitv endaddr word_sz)]
    | M.C_BNEZ ->
        let%bind rs1 = o 0 in
        let%bind rel = o 1 in
        Ok Il.[jmp_if (rs1 <> num 0 word_sz) rel (bitv endaddr word_sz)]
    | M.C_EBREAK -> Ok Il.[breakpoint ()]
    | M.C_J ->
        let%bind rel = o 0 in
        Ok Il.[jmp rel]
    | M.C_JAL ->
        let%bind rel = o 0 in
        Ok Il.[ra word_sz := bitv endaddr word_sz; call rel]
    | M.C_JALR ->
        let%bind rs1 = o 0 in
        Ok Il.[ra word_sz := bitv endaddr word_sz; call rs1]
    | M.C_JR ->
        let%bind rs1 = o 0 in
        Ok Il.[jmp rs1]
    | M.C_LD | M.C_LDSP ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        Ok Il.[rd := m]
    | M.C_LI ->
        let%bind rd = o 0 in
        let%bind imm = o 1 in
        Ok Il.[rd := imm]
    | M.C_LUI ->
        let%bind rd = o 0 in
        let%bind imm = o 1 in
        Ok Il.[rd := imm << num 12 word_sz]
    | (M.C_LW | M.C_LWSP) when is64 ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := m; rd := se 64 t1]
    | M.C_LW | M.C_LWSP ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        Ok Il.[rd := m]
    | M.C_MV ->
        let%bind rd = o 0 in
        let%bind rs2 = o 1 in
        Ok Il.[rd := rs2]
    | M.C_NOP -> Ok []
    | M.C_OR ->
        let%bind rd = o 0 in
        let%bind rs2 = o 1 in
        Ok Il.[rd := rd || rs2]
    | M.C_SD | M.C_SDSP ->
        let%bind rs2 = o 0 in
        let%bind m = o 1 in
        Ok [assign_mem m rs2]
    | M.C_SLLI ->
        let%bind rd = o 0 in
        let%bind shamt = o 1 in
        Ok Il.[rd := rd << shamt]
    | M.C_SRAI ->
        let%bind rd = o 0 in
        let%bind shamt = o 1 in
        Ok Il.[rd := rd >>> shamt]
    | M.C_SRLI ->
        let%bind rd = o 0 in
        let%bind shamt = o 1 in
        Ok Il.[rd := rd >> shamt]
    | M.C_SUB ->
        let%bind rd = o 0 in
        let%bind rs2 = o 1 in
        Ok Il.[rd := rd - rs2]
    | M.C_SUBW ->
        let%bind rd = o 0 in
        let%bind rs2 = o 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rd; t2 := lo 32 rs2; t3 := t1 - t2; rd := se 64 t3]
    | (M.C_SW | M.C_SWSP) when is64 ->
        let%bind rs2 = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs2; assign_mem m t1]
    | M.C_SW | M.C_SWSP ->
        let%bind rs2 = o 0 in
        let%bind m = o 1 in
        Ok [assign_mem m rs2]
    | M.C_XOR ->
        let%bind rd = o 0 in
        let%bind rs2 = o 1 in
        Ok Il.[rd := rd ^ rs2]
    | M.DIV ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let ones = Il.bitv Bitvec.(ones mod modulus word_sz) word_sz in
        let mins = Il.bitv Bitvec.(min_signed_value word_sz) word_sz in
        Ok
          Il.
            [ goto_if (rs2 = num 0 word_sz) l1 l2
            ; ( @ ) l1
            ; rd := ones
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; goto_if (rs1 = mins & rs2 = ones) l3 l4
            ; ( @ ) l3
            ; rd := mins
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l4
            ; rd := rs1 /$ rs2 ]
    | M.DIVU ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ goto_if (rs2 = num 0 word_sz) l1 l2
            ; ( @ ) l1
            ; rd := bitv Bitvec.(ones mod modulus word_sz) word_sz
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; rd := rs1 / rs2 ]
    | M.DIVUW ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := lo 32 rs1
            ; t2 := lo 32 rs2
            ; goto_if (t2 = num 0 32) l1 l2
            ; ( @ ) l1
            ; rd := bitv Bitvec.(ones mod modulus word_sz) word_sz
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; t3 := t1 / t2
            ; rd := se 64 t3 ]
    | M.DIVW ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let ones = Il.bitv Bitvec.(ones mod modulus 32) 32 in
        let mins = Il.bitv Bitvec.(min_signed_value 32) 32 in
        Ok
          Il.
            [ t1 := lo 32 rs1
            ; t2 := lo 32 rs2
            ; goto_if (t2 = num 0 32) l1 l2
            ; ( @ ) l1
            ; rd := se 64 ones
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; goto_if (t1 = mins & t2 = ones) l3 l4
            ; ( @ ) l3
            ; rd := se 64 mins
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l4
            ; t3 := t1 /$ t2
            ; rd := se 64 t3 ]
    | M.EBREAK -> Ok Il.[breakpoint ()]
    | M.ECALL -> Ok Il.[syscall ()]
    | M.J ->
        let%bind rel = o 0 in
        Ok Il.[jmp rel]
    | M.JAL ->
        let%bind rd = o 0 in
        let%bind rel = o 1 in
        Ok Il.[rd := bitv endaddr word_sz; call rel]
    | M.JALR ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        Ok Il.[rd := bitv endaddr word_sz; call rs1]
    | M.JR ->
        let%bind rs1 = o 0 in
        Ok Il.[jmp rs1]
    | M.LB ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 8 in
        Ok Il.[t1 := m; rd := se word_sz t1]
    | M.LBU ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 8 in
        Ok Il.[t1 := m; rd := ze word_sz t1]
    | M.LD ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        Ok Il.[rd := m]
    | M.LH ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 16 in
        Ok Il.[t1 := m; rd := se word_sz t1]
    | M.LHU ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 16 in
        Ok Il.[t1 := m; rd := ze word_sz t1]
    | M.LW when is64 ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := m; rd := se word_sz t1]
    | M.LW ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        Ok Il.[rd := m]
    | M.LWU ->
        let%bind rd = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := m; rd := ze word_sz t1]
    | M.MUL ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let sz = word_sz lsl 1 in
        let t1 = new_tmp sz in
        let t2 = new_tmp sz in
        let t3 = new_tmp sz in
        Ok
          Il.
            [ t1 := se sz rs1
            ; t2 := se sz rs2
            ; t3 := t1 * t3
            ; rd := lo word_sz t3 ]
    | M.MULH ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let sz = word_sz lsl 1 in
        let t1 = new_tmp sz in
        let t2 = new_tmp sz in
        let t3 = new_tmp sz in
        Ok
          Il.
            [ t1 := se sz rs1
            ; t2 := se sz rs2
            ; t3 := t1 * t3
            ; rd := hi word_sz t3 ]
    | M.MULHU ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let sz = word_sz lsl 1 in
        let t1 = new_tmp sz in
        let t2 = new_tmp sz in
        let t3 = new_tmp sz in
        Ok
          Il.
            [ t1 := ze sz rs1
            ; t2 := ze sz rs2
            ; t3 := t1 * t3
            ; rd := hi word_sz t3 ]
    | M.MULHSU ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let sz = word_sz lsl 1 in
        let t1 = new_tmp sz in
        let t2 = new_tmp sz in
        let t3 = new_tmp sz in
        Ok
          Il.
            [ t1 := se sz rs1
            ; t2 := ze sz rs2
            ; t3 := t1 * t3
            ; rd := hi word_sz t3 ]
    | M.OR ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := rs1 || rs2]
    | M.ORI ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rd := rs1 || imm]
    | M.LUI ->
        let%bind rd = o 0 in
        let%bind imm = o 1 in
        let half = word_sz lsr 1 in
        Ok Il.[rd := lo half (imm << num 12 word_sz) @@ lo half rd]
    | M.REM ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let ones = Il.bitv Bitvec.(ones mod modulus word_sz) word_sz in
        let mins = Il.bitv Bitvec.(min_signed_value word_sz) word_sz in
        Ok
          Il.
            [ goto_if (rs2 = num 0 word_sz) l1 l2
            ; ( @ ) l1
            ; rd := rs1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; goto_if (rs1 = mins & rs2 = ones) l3 l4
            ; ( @ ) l3
            ; rd := num 0 word_sz
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l4
            ; rd := rs1 %$ rs2 ]
    | M.REMU ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ goto_if (rs2 = num 0 word_sz) l1 l2
            ; ( @ ) l1
            ; rd := rs1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; rd := rs1 % rs2 ]
    | M.REMUW ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := lo 32 rs1
            ; t2 := lo 32 rs2
            ; goto_if (t2 = num 0 32) l1 l2
            ; ( @ ) l1
            ; rd := se 64 t1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; t3 := t1 % t2
            ; rd := se 64 t3 ]
    | M.REMW ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let ones = Il.bitv Bitvec.(ones mod modulus 32) 32 in
        let mins = Il.bitv Bitvec.(min_signed_value 32) 32 in
        Ok
          Il.
            [ t1 := lo 32 rs1
            ; t2 := lo 32 rs2
            ; goto_if (t2 = num 0 32) l1 l2
            ; ( @ ) l1
            ; rd := se 64 t1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; goto_if (t1 = mins & t2 = ones) l3 l4
            ; ( @ ) l3
            ; rd := num 0 word_sz
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l4
            ; t3 := t1 %$ t2
            ; rd := se 64 t3 ]
    | M.SB ->
        let%bind rs2 = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 8 in
        Ok Il.[t1 := lo 8 rs2; assign_mem m t1]
    | M.SD ->
        let%bind rs2 = o 0 in
        let%bind m = o 1 in
        Ok [assign_mem m rs2]
    | M.SH ->
        let%bind rs2 = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 16 in
        Ok Il.[t1 := lo 16 rs2; assign_mem m t1]
    | M.SW when is64 ->
        let%bind rs2 = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs2; assign_mem m t1]
    | M.SW ->
        let%bind rs2 = o 0 in
        let%bind m = o 1 in
        Ok [assign_mem m rs2]
    | M.SLL ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := rs1 << rs2]
    | M.SLLI ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind shamt = o 2 in
        Ok Il.[rd := rs1 << shamt]
    | M.SLLIW ->
        let%bind rd = o 1 in
        let%bind rs1 = o 1 in
        let%bind shamt = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs1; t1 := t1 << lo 32 shamt; rd := se 64 t1]
    | M.SLLW ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok
          Il.
            [t1 := lo 32 rs1; t2 := lo 32 rs2; t3 := t1 << t2; rd := se 64 t3]
    | M.SLT ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := ze word_sz (rs1 <$ rs2)]
    | M.SLTI ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rd := ze word_sz (rs1 <$ imm)]
    | M.SLTIU ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rd := ze word_sz (rs1 < imm)]
    | M.SLTU ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := ze word_sz (rs1 < rs2)]
    | M.SRA ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := rs1 >>> rs2]
    | M.SRAI ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind shamt = o 2 in
        Ok Il.[rd := rs1 >>> shamt]
    | M.SRAIW ->
        let%bind rd = o 1 in
        let%bind rs1 = o 1 in
        let%bind shamt = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs1; t1 := t1 >>> lo 32 shamt; rd := se 64 t1]
    | M.SRAW ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 rs1
            ; t2 := lo 32 rs2
            ; t3 := t1 >>> t2
            ; rd := se 64 t3 ]
    | M.SRL ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := rs1 >> rs2]
    | M.SRLI ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind shamt = o 2 in
        Ok Il.[rd := rs1 >> shamt]
    | M.SRLIW ->
        let%bind rd = o 1 in
        let%bind rs1 = o 1 in
        let%bind shamt = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs1; t1 := t1 >> lo 32 shamt; rd := se 64 t1]
    | M.SRLW ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok
          Il.
            [t1 := lo 32 rs1; t2 := lo 32 rs2; t3 := t1 >> t2; rd := se 64 t3]
    | M.SUB ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := rs1 - rs2]
    | M.SUBW ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok
          Il.[t1 := lo 32 rs1; t2 := lo 32 rs2; t3 := t1 - t2; rd := se 64 t3]
    | M.WFI -> Ok Il.[wait ()]
    | M.XOR ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind rs2 = o 2 in
        Ok Il.[rd := rs1 ^ rs2]
    | M.XORI ->
        let%bind rd = o 0 in
        let%bind rs1 = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rd := rs1 ^ imm]
    | _ -> Ok Il.[unk ()]
  in
  let stmts = Il.(( @ ) start_lbl) :: stmts in
  replace_zero word_sz stmts
  |> Il.fold_constants_in_stmts
  |> Il.normalize_end endaddr word_sz ctx
  |> Il.normalize_conditions ctx
  |> Il.remove_identity_assigns
  |> (fun stmts -> Il.Context.reset_tmp ctx; stmts)
  |> Il.make_blocks

let lift_exn instr addr ctx =
  match lift instr addr ctx with
  | Ok il -> il
  | Error err -> raise (Riscv_lift_error err)
