open Core_kernel
open Ll2l_std
module Register = Riscv_register
module Mnemonic = Riscv_mnemonic

module Flen = struct
  type t = S | D | Q [@@deriving equal]

  let to_int = function
    | S -> 32
    | D -> 64
    | Q -> 128
end

module Mode = struct
  type opt = {c: bool; flen: Flen.t option} [@@deriving equal]

  type t = RV32 of opt | RV64 of opt [@@deriving equal]

  let word_size_of = function
    | RV32 _ -> 32
    | RV64 _ -> 64

  let flen_of = function
    | RV32 {flen= Some flen} | RV64 {flen= Some flen} ->
        Some (Flen.to_int flen)
    | _ -> None

  let flen_at_least mode flen =
    match flen_of mode with
    | None -> false
    | Some flen' -> flen >= flen'

  let has_compact = function
    | RV32 {c} | RV64 {c} -> c

  let min_instr_length = function
    | (RV32 {c} | RV64 {c}) when c -> 2
    | _ -> 4

  let of_string = function
    | "riscv32" -> RV32 {c= false; flen= None}
    | "riscv32.fs" -> RV32 {c= false; flen= Some Flen.S}
    | "riscv32.fd" -> RV32 {c= false; flen= Some Flen.D}
    | "riscv32.fq" -> RV32 {c= false; flen= Some Flen.Q}
    | "riscv64" -> RV64 {c= false; flen= None}
    | "riscv64.fs" -> RV64 {c= false; flen= Some Flen.S}
    | "riscv64.fd" -> RV64 {c= false; flen= Some Flen.D}
    | "riscv64.fq" -> RV64 {c= false; flen= Some Flen.Q}
    | "riscv32.c" -> RV32 {c= true; flen= None}
    | "riscv32.c.fs" -> RV32 {c= true; flen= Some Flen.S}
    | "riscv32.c.fd" -> RV32 {c= true; flen= Some Flen.D}
    | "riscv32.c.fq" -> RV32 {c= true; flen= Some Flen.Q}
    | "riscv64.c" -> RV64 {c= true; flen= None}
    | "riscv64.c.fs" -> RV64 {c= true; flen= Some Flen.S}
    | "riscv64.c.fd" -> RV64 {c= true; flen= Some Flen.D}
    | "riscv64.c.fq" -> RV64 {c= true; flen= Some Flen.Q}
    | _ -> invalid_arg "bad RISC-V mode"

  let to_string = function
    | RV32 {c= false; flen= None} -> "riscv32"
    | RV32 {c= false; flen= Some Flen.S} -> "riscv32.fs"
    | RV32 {c= false; flen= Some Flen.D} -> "riscv32.fd"
    | RV32 {c= false; flen= Some Flen.Q} -> "riscv32.fq"
    | RV64 {c= false; flen= None} -> "riscv64"
    | RV64 {c= false; flen= Some Flen.S} -> "riscv64.fs"
    | RV64 {c= false; flen= Some Flen.D} -> "riscv64.fd"
    | RV64 {c= false; flen= Some Flen.Q} -> "riscv64.fq"
    | RV32 {c= true; flen= None} -> "riscv32.c"
    | RV32 {c= true; flen= Some Flen.S} -> "riscv32.c.fs"
    | RV32 {c= true; flen= Some Flen.D} -> "riscv32.c.fd"
    | RV32 {c= true; flen= Some Flen.Q} -> "riscv32.c.fq"
    | RV64 {c= true; flen= None} -> "riscv64.c"
    | RV64 {c= true; flen= Some Flen.S} -> "riscv64.c.fs"
    | RV64 {c= true; flen= Some Flen.D} -> "riscv64.c.fd"
    | RV64 {c= true; flen= Some Flen.Q} -> "riscv64.c.fq"
end

module Immediate = struct
  type kind = Relative | Literal [@@deriving equal]

  type t = {value: int64; kind: kind; size: int} [@@deriving equal]

  let is_relative imm = equal_kind imm.kind Relative

  let is_literal imm = equal_kind imm.kind Literal
end

module Operand = struct
  type t =
    | Reg of Register.t
    | Imm of Immediate.t
    | Mem of {base: Register.t; disp: int64; size: int}
    | Memaddr of {base: Register.t; disp: int64 option}
  [@@deriving equal]

  let to_string ~addr ~opsz = function
    | Reg r -> Register.to_string r
    | Imm {value; kind} -> (
      match kind with
      | Immediate.Relative ->
          let value = Addr.(addr + int64 value) in
          if opsz = 64 then Printf.sprintf "0x%016LX" (Addr.to_int64 value)
          else Printf.sprintf "0x%08lX" (Addr.to_int32 value)
      | Immediate.Literal -> Printf.sprintf "0x%LX" value )
    | Mem {base; disp} ->
        let b = Register.to_string base in
        if Int64.is_negative disp then
          let disp = Int64.(lnot disp + 1L) in
          Printf.sprintf "-0x%LX(%s)" disp b
        else Printf.sprintf "0x%LX(%s)" disp b
    | Memaddr {base; disp} -> (
        let b = Register.to_string base in
        match disp with
        | None -> Printf.sprintf "(%s)" b
        | Some disp ->
            if Int64.is_negative disp then
              let disp = Int64.(lnot disp + 1L) in
              Printf.sprintf "-0x%LX(%s)" disp b
            else Printf.sprintf "0x%LX(%s)" disp b )
end

module Instruction = struct
  type t =
    { mode: Mode.t
    ; mnemonic: Mnemonic.t
    ; bytes: bytes
    ; operands: Operand.t array
    ; opsz: int }

  let max_length = 4

  let length instr = Bytes.length instr.bytes [@@inline]

  let end_addr ~addr instr = Addr.(addr + int (length instr)) [@@inline]

  let opcode instr =
    let b1 = Char.to_int @@ Bytes.get instr.bytes 0 in
    let b2 = Char.to_int @@ Bytes.get instr.bytes 1 in
    if Bytes.length instr.bytes = 2 then b1 lor (b2 lsl 8) land 0xFFFF
    else
      let b3 = Char.to_int @@ Bytes.get instr.bytes 2 in
      let b4 = Char.to_int @@ Bytes.get instr.bytes 3 in
      b1 lor (b2 lsl 8) lor (b3 lsl 16) lor (b4 lsl 24) land 0xFFFFFFFF

  let no_fallthrough instr =
    let module M = Mnemonic in
    match instr.mnemonic with
    | M.J | M.JR -> true
    | M.C_J | M.C_JR -> true
    | _ -> false

  let may_fallthrough instr =
    let module M = Mnemonic in
    match instr.mnemonic with
    | M.JAL
     |M.JALR
     |M.BEQ
     |M.BGE
     |M.BLT
     |M.BGEU
     |M.BLTU
     |M.BNE
     |M.C_JAL
     |M.C_JALR
     |M.C_BEQZ
     |M.C_BNEZ
     |M.C_EBREAK
     |M.EBREAK
     |M.ECALL
     |M.MRET
     |M.SRET
     |M.URET
     |M.WFI -> true
    | _ -> false

  let must_fallthrough instr =
    not (no_fallthrough instr || may_fallthrough instr)
    [@@inline]

  let is_terminator instr = not (must_fallthrough instr) [@@inline]

  let is_conditional_branch instr =
    let module M = Mnemonic in
    match instr.mnemonic with
    | M.BEQ | M.BGE | M.BLT | M.BGEU | M.BLTU | M.BNE | M.C_BEQZ | M.C_BNEZ
      -> true
    | _ -> false

  let branch_address instr ~addr =
    if not (is_terminator instr) then None
    else
      Array.find_map instr.operands ~f:(function
        | Operand.(Imm {value; kind= Relative}) ->
            Some Addr.(int64 value + addr)
        | _ -> None)

  let to_string ~addr instr =
    let m_str = Mnemonic.to_string instr.mnemonic in
    let op_str =
      if Array.is_empty instr.operands then ""
      else
        List.mapi (Array.to_list instr.operands) ~f:(fun _ op ->
            Operand.to_string op ~addr ~opsz:instr.opsz)
        |> String.concat ~sep:", " |> Printf.sprintf " %s"
    in
    m_str ^ op_str

  let to_string_full ~addr instr =
    let addr_str =
      match Mode.word_size_of instr.mode with
      | 64 -> Printf.sprintf "0x%016LX" (Addr.to_int64 addr)
      (* 32 *)
      | _ -> Printf.sprintf "0x%08lX" (Addr.to_int32 addr)
    in
    let bytes_str =
      Bytes.to_list instr.bytes |> List.map ~f:Char.to_int
      |> List.map ~f:(Printf.sprintf "%02X")
      |> String.concat ~sep:" "
    in
    let bytes_str =
      let n = 4 - length instr in
      if n > 0 then bytes_str ^ String.init (n * 3) ~f:(fun _ -> ' ')
      else bytes_str
    in
    Printf.sprintf "%s:  %s  %s" addr_str bytes_str (to_string instr ~addr)
end
