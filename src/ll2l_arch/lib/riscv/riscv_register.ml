open Core_kernel

module Kind = struct
  type t = GP | FP [@@deriving equal]
end

type t =
  (* general purpose *)
  | ZERO
  | RA
  | SP
  | GP
  | TP
  | T0
  | T1
  | T2
  | FP
  | S1
  | A0
  | A1
  | A2
  | A3
  | A4
  | A5
  | A6
  | A7
  | S2
  | S3
  | S4
  | S5
  | S6
  | S7
  | S8
  | S9
  | S10
  | S11
  | T3
  | T4
  | T5
  | T6
  (* floating point *)
  | FT0
  | FT1
  | FT2
  | FT3
  | FT4
  | FT5
  | FT6
  | FT7
  | FS0
  | FS1
  | FA0
  | FA1
  | FA2
  | FA3
  | FA4
  | FA5
  | FA6
  | FA7
  | FS2
  | FS3
  | FS4
  | FS5
  | FS6
  | FS7
  | FS8
  | FS9
  | FS10
  | FS11
  | FT8
  | FT9
  | FT10
  | FT11
[@@deriving equal]

let to_string = function
  | ZERO -> "ZERO"
  | RA -> "RA"
  | SP -> "SP"
  | GP -> "GP"
  | TP -> "TP"
  | T0 -> "T0"
  | T1 -> "T1"
  | T2 -> "T2"
  | FP -> "FP"
  | S1 -> "S1"
  | A0 -> "A0"
  | A1 -> "A1"
  | A2 -> "A2"
  | A3 -> "A3"
  | A4 -> "A4"
  | A5 -> "A5"
  | A6 -> "A6"
  | A7 -> "A7"
  | S2 -> "S2"
  | S3 -> "S3"
  | S4 -> "S4"
  | S5 -> "S5"
  | S6 -> "S6"
  | S7 -> "S7"
  | S8 -> "S8"
  | S9 -> "S9"
  | S10 -> "S10"
  | S11 -> "S11"
  | T3 -> "T3"
  | T4 -> "T4"
  | T5 -> "T5"
  | T6 -> "T6"
  | FT0 -> "FT0"
  | FT1 -> "FT1"
  | FT2 -> "FT2"
  | FT3 -> "FT3"
  | FT4 -> "FT4"
  | FT5 -> "FT5"
  | FT6 -> "FT6"
  | FT7 -> "FT7"
  | FS0 -> "FS0"
  | FS1 -> "FS1"
  | FA0 -> "FA0"
  | FA1 -> "FA1"
  | FA2 -> "FA2"
  | FA3 -> "FA3"
  | FA4 -> "FA4"
  | FA5 -> "FA5"
  | FA6 -> "FA6"
  | FA7 -> "FA7"
  | FS2 -> "FS2"
  | FS3 -> "FS3"
  | FS4 -> "FS4"
  | FS5 -> "FS5"
  | FS6 -> "FS6"
  | FS7 -> "FS7"
  | FS8 -> "FS8"
  | FS9 -> "FS9"
  | FS10 -> "FS10"
  | FS11 -> "FS11"
  | FT8 -> "FT8"
  | FT9 -> "FT9"
  | FT10 -> "FT10"
  | FT11 -> "FT11"

let of_string = function
  | "ZERO" -> Some ZERO
  | "RA" -> Some RA
  | "SP" -> Some SP
  | "GP" -> Some GP
  | "TP" -> Some TP
  | "T0" -> Some T0
  | "T1" -> Some T1
  | "T2" -> Some T2
  | "FP" -> Some FP
  | "S1" -> Some S1
  | "A0" -> Some A0
  | "A1" -> Some A1
  | "A2" -> Some A2
  | "A3" -> Some A3
  | "A4" -> Some A4
  | "A5" -> Some A5
  | "A6" -> Some A6
  | "A7" -> Some A7
  | "S2" -> Some S2
  | "S3" -> Some S3
  | "S4" -> Some S4
  | "S5" -> Some S5
  | "S6" -> Some S6
  | "S7" -> Some S7
  | "S8" -> Some S8
  | "S9" -> Some S9
  | "S10" -> Some S10
  | "S11" -> Some S11
  | "T3" -> Some T3
  | "T4" -> Some T4
  | "T5" -> Some T5
  | "T6" -> Some T6
  | "FT0" -> Some FT0
  | "FT1" -> Some FT1
  | "FT2" -> Some FT2
  | "FT3" -> Some FT3
  | "FT4" -> Some FT4
  | "FT5" -> Some FT5
  | "FT6" -> Some FT6
  | "FT7" -> Some FT7
  | "FS0" -> Some FS0
  | "FS1" -> Some FS1
  | "FA0" -> Some FA0
  | "FA1" -> Some FA1
  | "FA2" -> Some FA2
  | "FA3" -> Some FA3
  | "FA4" -> Some FA4
  | "FA5" -> Some FA5
  | "FA6" -> Some FA6
  | "FA7" -> Some FA7
  | "FS2" -> Some FS2
  | "FS3" -> Some FS3
  | "FS4" -> Some FS4
  | "FS5" -> Some FS5
  | "FS6" -> Some FS6
  | "FS7" -> Some FS7
  | "FS8" -> Some FS8
  | "FS9" -> Some FS9
  | "FS10" -> Some FS10
  | "FS11" -> Some FS11
  | "FT8" -> Some FT8
  | "FT9" -> Some FT9
  | "FT10" -> Some FT10
  | "FT11" -> Some FT11
  | _ -> None

let gp_reg_of_int = function
  | 0 -> Some ZERO
  | 1 -> Some RA
  | 2 -> Some SP
  | 3 -> Some GP
  | 4 -> Some TP
  | 5 -> Some T0
  | 6 -> Some T1
  | 7 -> Some T2
  | 8 -> Some FP
  | 9 -> Some S1
  | 10 -> Some A0
  | 11 -> Some A1
  | 12 -> Some A2
  | 13 -> Some A3
  | 14 -> Some A4
  | 15 -> Some A5
  | 16 -> Some A6
  | 17 -> Some A7
  | 18 -> Some S2
  | 19 -> Some S3
  | 20 -> Some S4
  | 21 -> Some S5
  | 22 -> Some S6
  | 23 -> Some S7
  | 24 -> Some S8
  | 25 -> Some S9
  | 26 -> Some S10
  | 27 -> Some S11
  | 28 -> Some T3
  | 29 -> Some T4
  | 30 -> Some T5
  | 31 -> Some T6
  | _ -> None

let int_of_gp_reg = function
  | ZERO -> Some 0
  | RA -> Some 1
  | SP -> Some 2
  | GP -> Some 3
  | TP -> Some 4
  | T0 -> Some 5
  | T1 -> Some 6
  | T2 -> Some 7
  | FP -> Some 8
  | S1 -> Some 9
  | A0 -> Some 10
  | A1 -> Some 11
  | A2 -> Some 12
  | A3 -> Some 13
  | A4 -> Some 14
  | A5 -> Some 15
  | A6 -> Some 16
  | A7 -> Some 17
  | S2 -> Some 18
  | S3 -> Some 19
  | S4 -> Some 20
  | S5 -> Some 21
  | S6 -> Some 22
  | S7 -> Some 23
  | S8 -> Some 24
  | S9 -> Some 25
  | S10 -> Some 26
  | S11 -> Some 27
  | T3 -> Some 28
  | T4 -> Some 29
  | T5 -> Some 30
  | T6 -> Some 31
  | _ -> None

let fp_reg_of_int = function
  | 0 -> Some FT0
  | 1 -> Some FT1
  | 2 -> Some FT2
  | 3 -> Some FT3
  | 4 -> Some FT4
  | 5 -> Some FT5
  | 6 -> Some FT6
  | 7 -> Some FT7
  | 8 -> Some FS0
  | 9 -> Some FS1
  | 10 -> Some FA0
  | 11 -> Some FA1
  | 12 -> Some FA2
  | 13 -> Some FA3
  | 14 -> Some FA4
  | 15 -> Some FA5
  | 16 -> Some FA6
  | 17 -> Some FA7
  | 18 -> Some FS2
  | 19 -> Some FS3
  | 20 -> Some FS4
  | 21 -> Some FS5
  | 22 -> Some FS6
  | 23 -> Some FS7
  | 24 -> Some FS8
  | 25 -> Some FS9
  | 26 -> Some FS10
  | 27 -> Some FS11
  | 28 -> Some FT8
  | 29 -> Some FT9
  | 30 -> Some FT10
  | 31 -> Some FT11
  | _ -> None

let int_of_fp_reg = function
  | FT0 -> Some 0
  | FT1 -> Some 1
  | FT2 -> Some 2
  | FT3 -> Some 3
  | FT4 -> Some 4
  | FT5 -> Some 5
  | FT6 -> Some 6
  | FT7 -> Some 7
  | FS0 -> Some 8
  | FS1 -> Some 9
  | FA0 -> Some 10
  | FA1 -> Some 11
  | FA2 -> Some 12
  | FA3 -> Some 13
  | FA4 -> Some 14
  | FA5 -> Some 15
  | FA6 -> Some 16
  | FA7 -> Some 17
  | FS2 -> Some 18
  | FS3 -> Some 19
  | FS4 -> Some 20
  | FS5 -> Some 21
  | FS6 -> Some 22
  | FS7 -> Some 23
  | FS8 -> Some 24
  | FS9 -> Some 25
  | FS10 -> Some 26
  | FS11 -> Some 27
  | FT8 -> Some 28
  | FT9 -> Some 29
  | FT10 -> Some 30
  | FT11 -> Some 31
  | _ -> None

let kind_of = function
  (* general purpose *)
  | ZERO
   |RA
   |SP
   |GP
   |TP
   |T0
   |T1
   |T2
   |FP
   |S1
   |A0
   |A1
   |A2
   |A3
   |A4
   |A5
   |A6
   |A7
   |S2
   |S3
   |S4
   |S5
   |S6
   |S7
   |S8
   |S9
   |S10
   |S11
   |T3
   |T4
   |T5
   |T6 -> Kind.GP
  | FT0
   |FT1
   |FT2
   |FT3
   |FT4
   |FT5
   |FT6
   |FT7
   |FS0
   |FS1
   |FA0
   |FA1
   |FA2
   |FA3
   |FA4
   |FA5
   |FA6
   |FA7
   |FS2
   |FS3
   |FS4
   |FS5
   |FS6
   |FS7
   |FS8
   |FS9
   |FS10
   |FS11
   |FT8
   |FT9
   |FT10
   |FT11 -> Kind.FP

type width = {width32: int; width64: int}

let width_of ?flen r =
  match kind_of r with
  | Kind.GP -> {width32= 32; width64= 64}
  | Kind.FP -> (
    match flen with
    | None -> invalid_arg (Printf.sprintf "invalid FLEN")
    | Some 32 -> {width32= 32; width64= 32}
    | Some 64 -> {width32= 64; width64= 64}
    | Some 128 -> {width32= 128; width64= 128}
    | Some flen -> invalid_arg (Printf.sprintf "invalid FLEN %d" flen) )
