open Core_kernel
module I = Riscv.Instruction
module Imm = Riscv.Immediate
module Mode = Riscv.Mode
module M = Riscv.Mnemonic
module O = Riscv.Operand
module R = Riscv.Register

type error = [`OOB | `Invalid | `BadOperand | `Unimpl]

let string_of_error = function
  | `OOB -> "out of bounds"
  | `Invalid -> "invalid"
  | `BadOperand -> "bad operand"
  | `Unimpl -> "unimplemented"

exception Riscv_decode_error of error

let sign_ext value size =
  let mask = Int64.(1L lsl Int.(size - 1)) in
  Int64.((value lxor mask) - mask)

let ex b hi lo =
  let hi, lo = if max hi lo = hi then (hi, lo) else (lo, hi) in
  let off = hi - lo + 1 in
  if off > 31 then failwith "invalid range"
  else (b lsr lo) land (Int.pow 2 off - 1)

let tst_i b pos = (b lsr pos) land 1 [@@inline]

let tst b pos =
  match tst_i b pos with
  | 0 -> false
  | _ -> true

let concat n1 n2 shift = (n1 lsl shift) lor n2 [@@inline]

let rs3_gp b ~mode =
  let n = ex b 31 27 in
  (* if Mode.has_compact mode && n > 15 then None else *) R.gp_reg_of_int n

let rs3_fp b ~mode =
  let n = ex b 31 27 in
  (* if Mode.has_compact mode && n > 15 then None else *) R.fp_reg_of_int n

let rs2_gp b ~mode =
  let n = ex b 24 20 in
  (* if Mode.has_compact mode && n > 15 then None else *) R.gp_reg_of_int n

let rs2_fp b ~mode =
  let n = ex b 24 20 in
  (* if Mode.has_compact mode && n > 15 then None else *) R.fp_reg_of_int n

let rs1_gp b ~mode =
  let n = ex b 19 15 in
  (* if Mode.has_compact mode && n > 15 then None else *) R.gp_reg_of_int n

let rs1_fp b ~mode =
  let n = ex b 19 15 in
  (* if Mode.has_compact mode && n > 15 then None else *) R.fp_reg_of_int n

let rd_gp b ~mode =
  let n = ex b 11 7 in
  (* if Mode.has_compact mode && n > 15 then None else *) R.gp_reg_of_int n

let rd_fp b ~mode =
  let n = ex b 11 7 in
  (* if Mode.has_compact mode && n > 15 then None else *) R.fp_reg_of_int n

let rs3_gp_reg b ~mode = Option.(rs3_gp b ~mode >>| fun r -> O.Reg r)

let rs3_fp_reg b ~mode = Option.(rs3_fp b ~mode >>| fun r -> O.Reg r)

let rs2_gp_reg b ~mode = Option.(rs2_gp b ~mode >>| fun r -> O.Reg r)

let rs2_fp_reg b ~mode = Option.(rs2_fp b ~mode >>| fun r -> O.Reg r)

let rs1_gp_reg b ~mode = Option.(rs1_gp b ~mode >>| fun r -> O.Reg r)

let rs1_fp_reg b ~mode = Option.(rs1_fp b ~mode >>| fun r -> O.Reg r)

let rd_gp_reg b ~mode = Option.(rd_gp b ~mode >>| fun r -> O.Reg r)

let rd_fp_reg b ~mode = Option.(rd_fp b ~mode >>| fun r -> O.Reg r)

let imm_i b = Int64.of_int (ex b 31 20) [@@inline]

let imm_i_shamt b = Int64.of_int (ex b 24 20) [@@inline]

let imm_i_shamt64 b = Int64.of_int (ex b 25 20) [@@inline]

let imm_s b = Int64.of_int (concat (ex b 31 25) (ex b 4 0) 5) [@@inline]

let imm_b b =
  let b_12 = tst_i b 31 in
  let b_10_5 = ex b 30 25 in
  let b_4_1 = ex b 11 8 in
  let b_11 = tst_i b 7 in
  Int64.of_int
    (concat (concat (concat (concat b_12 b_11 1) b_10_5 6) b_4_1 4) 0 1)

let imm_u b = Int64.of_int (ex b 31 12) [@@inline]

let imm_j b =
  let b_20 = tst_i b 31 in
  let b_10_1 = ex b 30 21 in
  let b_11 = tst_i b 20 in
  let b_19_12 = ex b 19 12 in
  Int64.of_int
    (concat (concat (concat (concat b_20 b_19_12 8) b_11 1) b_10_1 10) 0 1)

let membaseoff_i b ~size ~mode =
  Option.(
    rs1_gp b ~mode
    >>| fun rs1 ->
    let disp = sign_ext (imm_i b) 12 in
    O.(Mem {base= rs1; disp; size}))

let membaseoff_s b ~size ~mode =
  Option.(
    rs1_gp b ~mode
    >>| fun rs1 ->
    let disp = sign_ext (imm_s b) 12 in
    O.(Mem {base= rs1; disp; size}))

let make_rdmembaseoff b ~size ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>= fun rd -> membaseoff_i b ~size ~mode >>| fun mem -> [|rd; mem|])

let make_rdmembaseoff_fp b ~size ~mode =
  Option.(
    rd_fp_reg b ~mode
    >>= fun rd -> membaseoff_i b ~size ~mode >>| fun mem -> [|rd; mem|])

let make_rdrs1imm b ~opsz ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>= fun rd ->
    rs1_gp_reg b ~mode
    >>| fun rs1 ->
    let value = sign_ext (imm_i b) 12 in
    let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
    [|rd; rs1; imm|])

let make_rduimmimm b ~opsz ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>| fun rd ->
    let value = Int64.of_int (ex b 19 15) in
    let uimm = O.Imm Imm.{value; kind= Literal; size= opsz} in
    let value = sign_ext (imm_i b) 12 in
    let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
    [|rd; uimm; imm|])

let make_rdrs1shamt b ~opsz ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>= fun rd ->
    rs1_gp_reg b ~mode
    >>| fun rs1 ->
    let value = imm_i_shamt b in
    let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
    [|rd; rs1; imm|])

let make_rdrs1shamt64 b ~opsz ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>= fun rd ->
    rs1_gp_reg b ~mode
    >>| fun rs1 ->
    let value = imm_i_shamt64 b in
    let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
    [|rd; rs1; imm|])

let make_rdrs1rs2 b ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>= fun rd ->
    rs1_gp_reg b ~mode
    >>= fun rs1 -> rs2_gp_reg b ~mode >>| fun rs2 -> [|rd; rs1; rs2|])

let make_rdrs1rs2_fp b ~mode =
  Option.(
    rd_fp_reg b ~mode
    >>= fun rd ->
    rs1_fp_reg b ~mode
    >>= fun rs1 -> rs2_fp_reg b ~mode >>| fun rs2 -> [|rd; rs1; rs2|])

let make_rdrs1_fp b ~mode =
  Option.(
    rd_fp_reg b ~mode
    >>= fun rd -> rs1_fp_reg b ~mode >>| fun rs1 -> [|rd; rs1|])

let make_rdimmu b ~opsz ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>| fun rd ->
    let value = imm_u b in
    let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
    [|rd; imm|])

let make_rs2membaseoff b ~size ~mode =
  Option.(
    rs2_gp_reg b ~mode
    >>= fun rs2 -> membaseoff_s b ~mode ~size >>| fun mem -> [|rs2; mem|])

let make_rs2membaseoff_fp b ~size ~mode =
  Option.(
    rs2_fp_reg b ~mode
    >>= fun rs2 -> membaseoff_s b ~mode ~size >>| fun mem -> [|rs2; mem|])

let make_rdaddr b ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>= fun rd ->
    rs1_gp b ~mode >>| fun rs1 -> [|rd; O.Memaddr {base= rs1; disp= None}|])

let make_rdrs2addr b ~mode =
  Option.(
    rd_gp_reg b ~mode
    >>= fun rd ->
    rs2_gp_reg b ~mode
    >>= fun rs2 ->
    rs1_gp b ~mode
    >>| fun rs1 -> [|rd; rs2; O.Memaddr {base= rs1; disp= None}|])

let make_rdrs1rs2rs3_fp b ~mode =
  Option.(
    rd_fp_reg b ~mode
    >>= fun rd ->
    rs1_fp_reg b ~mode
    >>= fun rs1 ->
    rs2_fp_reg b ~mode
    >>= fun rs2 -> rs3_fp_reg b ~mode >>| fun rs3 -> [|rd; rs1; rs2; rs3|])

let make_rel_j b ~mode ~opsz =
  let value = sign_ext (imm_j b) 21 in
  Some [|O.Imm Imm.{value; kind= Relative; size= opsz}|]

let make_rdrel_j b ~mode ~opsz =
  Option.(
    rd_gp_reg b ~mode
    >>| fun rd ->
    let value = sign_ext (imm_j b) 21 in
    let rel = O.Imm Imm.{value; kind= Relative; size= opsz} in
    [|rd; rel|])

let make_rs1off_j b ~mode ~opsz =
  Option.(
    rs1_gp b ~mode
    >>| fun rs1 ->
    let disp = Some (sign_ext (imm_j b) 21) in
    let addr = O.(Memaddr {base= rs1; disp}) in
    [|addr|])

let make_rdrs1off_j b ~mode ~opsz =
  Option.(
    rd_gp_reg b ~mode
    >>= fun rd ->
    rs1_gp b ~mode
    >>| fun rs1 ->
    let disp = Some (sign_ext (imm_j b) 21) in
    let addr = O.(Memaddr {base= rs1; disp}) in
    [|rd; addr|])

let make_rs1rs2rel_b b ~mode ~opsz =
  Option.(
    rs1_gp_reg b ~mode
    >>= fun rs1 ->
    rs2_gp_reg b ~mode
    >>| fun rs2 ->
    let value = sign_ext (imm_b b) 13 in
    let rel = O.Imm Imm.{value; kind= Relative; size= opsz} in
    [|rs1; rs2; rel|])

let make_rs1rs2 b ~mode =
  Option.(
    rs1_gp_reg b ~mode
    >>= fun rs1 -> rs2_gp_reg b ~mode >>| fun rs2 -> [|rs1; rs2|])

let ops_or_error mode b f =
  Result.of_option (f b ~mode) ~error:`BadOperand
  [@@inline]

let parse_load mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b000 ->
      let%map ops = o (make_rdmembaseoff ~size:8) in
      (M.LB, ops, opsz)
  | 0b001 ->
      let%map ops = o (make_rdmembaseoff ~size:16) in
      (M.LH, ops, opsz)
  | 0b010 ->
      let%map ops = o (make_rdmembaseoff ~size:32) in
      (M.LW, ops, opsz)
  | 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o (make_rdmembaseoff ~size:64) in
      (M.LD, ops, opsz)
  | 0b100 ->
      let%map ops = o (make_rdmembaseoff ~size:8) in
      (M.LBU, ops, opsz)
  | 0b101 ->
      let%map ops = o (make_rdmembaseoff ~size:16) in
      (M.LHU, ops, opsz)
  | 0b110 when Mode.word_size_of mode = 64 ->
      let%map ops = o (make_rdmembaseoff ~size:32) in
      (M.LWU, ops, opsz)
  | _ -> Error `Invalid

let parse_load_fp mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b010 when Mode.flen_at_least mode 32 ->
      let%map ops = o (make_rdmembaseoff_fp ~size:32) in
      (M.FLW, ops, opsz)
  | 0b011 when Mode.flen_at_least mode 64 ->
      let%map ops = o (make_rdmembaseoff_fp ~size:64) in
      (M.FLD, ops, opsz)
  | 0b100 when Mode.flen_at_least mode 128 ->
      let%map ops = o (make_rdmembaseoff_fp ~size:128) in
      (M.FLQ, ops, opsz)
  | _ -> Error `Invalid

let parse_misc_mem mode b opsz =
  match ex b 14 12 with
  | 0b000 when ex b 31 28 = 0b0000 ->
      let ops = [||] in
      Ok (M.FENCE, ops, opsz)
  | 0b000 when ex b 31 28 = 0b1000 ->
      let ops = [||] in
      Ok (M.FENCE_TSO, ops, opsz)
  | 0b001 ->
      let ops = [||] in
      Ok (M.FENCE_I, ops, opsz)
  | _ -> Error `Invalid

let parse_op_imm mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b000 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.ADDI, ops, opsz)
  | 0b001 when Mode.word_size_of mode = 64 && ex b 31 26 = 0b000000 ->
      let%map ops = o (make_rdrs1shamt64 ~opsz) in
      (M.SLLI, ops, opsz)
  | 0b001 when ex b 31 25 = 0b0000000 ->
      let%map ops = o (make_rdrs1shamt ~opsz) in
      (M.SLLI, ops, opsz)
  | 0b010 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.SLTI, ops, opsz)
  | 0b011 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.SLTIU, ops, opsz)
  | 0b100 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.XORI, ops, opsz)
  | 0b101 when Mode.word_size_of mode = 64 && ex b 31 26 = 0b000000 ->
      let%map ops = o (make_rdrs1shamt64 ~opsz) in
      (M.SRLI, ops, opsz)
  | 0b101 when ex b 31 25 = 0b0000000 ->
      let%map ops = o (make_rdrs1shamt ~opsz) in
      (M.SRLI, ops, opsz)
  | 0b101 when Mode.word_size_of mode = 64 && ex b 31 26 = 0b010000 ->
      let%map ops = o (make_rdrs1shamt64 ~opsz) in
      (M.SRAI, ops, opsz)
  | 0b101 when ex b 31 25 = 0b0100000 ->
      let%map ops = o (make_rdrs1shamt ~opsz) in
      (M.SRAI, ops, opsz)
  | 0b110 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.ORI, ops, opsz)
  | 0b111 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.ANDI, ops, opsz)
  | _ -> Error `Invalid

let parse_op_imm_32 mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b000 when Mode.word_size_of mode = 64 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.ADDIW, ops, opsz)
  | 0b001 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000000 ->
      let%map ops = o (make_rdrs1shamt ~opsz) in
      (M.SLLIW, ops, opsz)
  | 0b101 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000000 ->
      let%map ops = o (make_rdrs1shamt ~opsz) in
      (M.SRLIW, ops, opsz)
  | 0b101 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0100000 ->
      let%map ops = o (make_rdrs1shamt ~opsz) in
      (M.SRAIW, ops, opsz)
  | _ -> Error `Invalid

let parse_op mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b000 when ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.ADD, ops, opsz)
  | 0b000 when ex b 31 25 = 0b0100000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SUB, ops, opsz)
  | 0b000 when ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.MUL, ops, opsz)
  | 0b001 when ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SLL, ops, opsz)
  | 0b001 when ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.MULH, ops, opsz)
  | 0b010 when ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SLT, ops, opsz)
  | 0b010 when ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.MULHSU, ops, opsz)
  | 0b011 when ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SLTU, ops, opsz)
  | 0b011 when ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.MULHU, ops, opsz)
  | 0b100 when ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.XOR, ops, opsz)
  | 0b100 when ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.DIV, ops, opsz)
  | 0b101 when ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SRL, ops, opsz)
  | 0b101 when ex b 31 25 = 0b0100000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SRA, ops, opsz)
  | 0b101 when ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.DIVU, ops, opsz)
  | 0b110 when ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.OR, ops, opsz)
  | 0b110 when ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.REM, ops, opsz)
  | 0b111 when ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.AND, ops, opsz)
  | 0b111 when ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.REMU, ops, opsz)
  | _ -> Error `Invalid

let parse_op_32 mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b000 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.ADDW, ops, opsz)
  | 0b000 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0100000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SUBW, ops, opsz)
  | 0b000 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.MULW, ops, opsz)
  | 0b001 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SLLW, ops, opsz)
  | 0b100 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.DIVW, ops, opsz)
  | 0b101 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SRLW, ops, opsz)
  | 0b101 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0100000 ->
      let%map ops = o make_rdrs1rs2 in
      (M.SRAW, ops, opsz)
  | 0b101 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.DIVUW, ops, opsz)
  | 0b110 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.REMW, ops, opsz)
  | 0b111 when Mode.word_size_of mode = 64 && ex b 31 25 = 0b0000001 ->
      let%map ops = o make_rdrs1rs2 in
      (M.REMUW, ops, opsz)
  | _ -> Error `Invalid

let parse_store mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b000 ->
      let%map ops = o (make_rs2membaseoff ~size:8) in
      let opsz = 8 in
      (M.SB, ops, opsz)
  | 0b001 ->
      let%map ops = o (make_rs2membaseoff ~size:16) in
      let opsz = 16 in
      (M.SH, ops, opsz)
  | 0b010 ->
      let%map ops = o (make_rs2membaseoff ~size:32) in
      let opsz = 32 in
      (M.SW, ops, opsz)
  | 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o (make_rs2membaseoff ~size:64) in
      let opsz = 64 in
      (M.SD, ops, opsz)
  | _ -> Error `Invalid

let parse_store_fp mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b010 when Mode.flen_at_least mode 32 ->
      let%map ops = o (make_rs2membaseoff_fp ~size:32) in
      let opsz = 32 in
      (M.FSW, ops, opsz)
  | 0b011 when Mode.flen_at_least mode 64 ->
      let%map ops = o (make_rs2membaseoff_fp ~size:64) in
      let opsz = 64 in
      (M.FSD, ops, opsz)
  | 0b100 when Mode.flen_at_least mode 128 ->
      let%map ops = o (make_rs2membaseoff_fp ~size:128) in
      let opsz = 128 in
      (M.FSQ, ops, opsz)
  | _ -> Error `Invalid

let parse_amo mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match (ex b 31 27, ex b 14 12) with
  | 0b00010, 0b010 when ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdaddr in
      (M.LR_W, ops, opsz)
  | 0b00010, 0b010 ->
      let%map ops = o make_rdaddr in
      let opsz = 32 in
      (M.SC_W, ops, opsz)
  | 0b00001, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOSWAP_W, ops, opsz)
  | 0b00000, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOADD_W, ops, opsz)
  | 0b00100, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOXOR_W, ops, opsz)
  | 0b01100, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOAND_W, ops, opsz)
  | 0b01000, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOOR_W, ops, opsz)
  | 0b10000, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOMIN_W, ops, opsz)
  | 0b10100, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOMAX_W, ops, opsz)
  | 0b11000, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOMINU_W, ops, opsz)
  | 0b11100, 0b010 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOMAXU_W, ops, opsz)
  | 0b00010, 0b011 when Mode.word_size_of mode = 64 && ex b 24 20 = 0b00000
    ->
      let%map ops = o make_rdaddr in
      (M.LR_D, ops, opsz)
  | 0b00010, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdaddr in
      let opsz = 32 in
      (M.SC_D, ops, opsz)
  | 0b00001, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOSWAP_D, ops, opsz)
  | 0b00000, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOADD_D, ops, opsz)
  | 0b00100, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOXOR_D, ops, opsz)
  | 0b01100, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOAND_D, ops, opsz)
  | 0b01000, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOOR_D, ops, opsz)
  | 0b10000, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOMIN_D, ops, opsz)
  | 0b10100, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOMAX_D, ops, opsz)
  | 0b11000, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOMINU_D, ops, opsz)
  | 0b11100, 0b011 when Mode.word_size_of mode = 64 ->
      let%map ops = o make_rdrs2addr in
      (M.AMOMAXU_D, ops, opsz)
  | _ -> Error `Invalid

let parse_madd mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 26 25 with
  | 0b00 when Mode.flen_at_least mode 32 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FMADD_S, ops, opsz)
  | 0b01 when Mode.flen_at_least mode 64 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FMADD_D, ops, opsz)
  | 0b11 when Mode.flen_at_least mode 128 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FMADD_Q, ops, opsz)
  | _ -> Error `Invalid

let parse_msub mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 26 25 with
  | 0b00 when Mode.flen_at_least mode 32 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FMSUB_S, ops, opsz)
  | 0b01 when Mode.flen_at_least mode 64 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FMSUB_D, ops, opsz)
  | 0b11 when Mode.flen_at_least mode 128 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FMSUB_Q, ops, opsz)
  | _ -> Error `Invalid

let parse_nmadd mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 26 25 with
  | 0b00 when Mode.flen_at_least mode 32 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FNMADD_S, ops, opsz)
  | 0b01 when Mode.flen_at_least mode 64 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FNMADD_D, ops, opsz)
  | 0b11 when Mode.flen_at_least mode 128 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FNMADD_Q, ops, opsz)
  | _ -> Error `Invalid

let parse_nmsub mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 26 25 with
  | 0b00 when Mode.flen_at_least mode 32 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FNMSUB_S, ops, opsz)
  | 0b01 when Mode.flen_at_least mode 64 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FNMSUB_D, ops, opsz)
  | 0b11 when Mode.flen_at_least mode 128 ->
      let%map ops = o make_rdrs1rs2rs3_fp in
      (M.FNMSUB_Q, ops, opsz)
  | _ -> Error `Invalid

let parse_op_fp mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 31 25 with
  | 0b0000000 when Mode.flen_at_least mode 32 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FADD_S, ops, opsz)
  | 0b0000001 when Mode.flen_at_least mode 64 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FADD_D, ops, opsz)
  | 0b0000011 when Mode.flen_at_least mode 128 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FADD_Q, ops, opsz)
  | 0b0000100 when Mode.flen_at_least mode 32 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSUB_S, ops, opsz)
  | 0b0000101 when Mode.flen_at_least mode 64 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSUB_D, ops, opsz)
  | 0b0000111 when Mode.flen_at_least mode 128 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSUB_Q, ops, opsz)
  | 0b0001000 when Mode.flen_at_least mode 32 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMUL_S, ops, opsz)
  | 0b0001001 when Mode.flen_at_least mode 64 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMUL_D, ops, opsz)
  | 0b0001011 when Mode.flen_at_least mode 128 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMUL_Q, ops, opsz)
  | 0b0001100 when Mode.flen_at_least mode 32 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FDIV_S, ops, opsz)
  | 0b0001101 when Mode.flen_at_least mode 64 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FDIV_D, ops, opsz)
  | 0b0001111 when Mode.flen_at_least mode 128 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FDIV_Q, ops, opsz)
  | 0b0101100 when Mode.flen_at_least mode 32 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSQRT_S, ops, opsz)
  | 0b0101101 when Mode.flen_at_least mode 64 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSQRT_D, ops, opsz)
  | 0b0101111 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSQRT_Q, ops, opsz)
  | 0b0010000 when Mode.flen_at_least mode 32 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJ_S, ops, opsz)
  | 0b0010001 when Mode.flen_at_least mode 64 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJ_D, ops, opsz)
  | 0b0010011 when Mode.flen_at_least mode 128 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJ_Q, ops, opsz)
  | 0b0010000 when Mode.flen_at_least mode 32 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJN_S, ops, opsz)
  | 0b0010001 when Mode.flen_at_least mode 64 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJN_D, ops, opsz)
  | 0b0010011 when Mode.flen_at_least mode 128 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJN_Q, ops, opsz)
  | 0b0010000 when Mode.flen_at_least mode 32 && ex b 14 12 = 0b010 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJX_S, ops, opsz)
  | 0b0010001 when Mode.flen_at_least mode 64 && ex b 14 12 = 0b010 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJX_D, ops, opsz)
  | 0b0010011 when Mode.flen_at_least mode 128 && ex b 14 12 = 0b010 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FSGNJX_Q, ops, opsz)
  | 0b0010100 when Mode.flen_at_least mode 32 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMIN_S, ops, opsz)
  | 0b0010101 when Mode.flen_at_least mode 64 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMIN_D, ops, opsz)
  | 0b0010111 when Mode.flen_at_least mode 128 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMIN_Q, ops, opsz)
  | 0b0010100 when Mode.flen_at_least mode 32 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMAX_S, ops, opsz)
  | 0b0010101 when Mode.flen_at_least mode 64 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMAX_D, ops, opsz)
  | 0b0010111 when Mode.flen_at_least mode 128 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FMAX_Q, ops, opsz)
  | 0b1100000 when Mode.flen_at_least mode 32 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_W_S, ops, opsz)
  | 0b0100000 when Mode.flen_at_least mode 64 && ex b 24 20 = 0b00001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_S_D, ops, opsz)
  | 0b0100000 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00011 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_S_Q, ops, opsz)
  | 0b1100000 when Mode.flen_at_least mode 32 && ex b 24 20 = 0b00001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_WU_S, ops, opsz)
  | 0b0100001 when Mode.flen_at_least mode 64 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_D_S, ops, opsz)
  | 0b0100001 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00011 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_D_Q, ops, opsz)
  | 0b0100011 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_Q_S, ops, opsz)
  | 0b0100011 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_Q_D, ops, opsz)
  | 0b1100000
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 32
         && ex b 24 20 = 0b00010 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_L_S, ops, opsz)
  | 0b1100001
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 64
         && ex b 24 20 = 0b00010 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_L_D, ops, opsz)
  | 0b1100011
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 128
         && ex b 24 20 = 0b00010 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_L_Q, ops, opsz)
  | 0b1100000
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 32
         && ex b 24 20 = 0b00011 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_LU_S, ops, opsz)
  | 0b1100001
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 64
         && ex b 24 20 = 0b00011 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_LU_D, ops, opsz)
  | 0b1100011
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 128
         && ex b 24 20 = 0b00011 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_LU_Q, ops, opsz)
  | 0b1110000
    when Mode.flen_at_least mode 32
         && ex b 24 20 = 0b00000
         && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FMV_X_W, ops, opsz)
  | 0b1110001
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 64
         && ex b 24 20 = 0b00000
         && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FMV_X_D, ops, opsz)
  | 0b1010000 when Mode.flen_at_least mode 32 && ex b 14 12 = 0b010 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FEQ_S, ops, opsz)
  | 0b1010001 when Mode.flen_at_least mode 64 && ex b 14 12 = 0b010 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FEQ_D, ops, opsz)
  | 0b1010011 when Mode.flen_at_least mode 128 && ex b 14 12 = 0b010 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FEQ_Q, ops, opsz)
  | 0b1010000 when Mode.flen_at_least mode 32 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FLT_S, ops, opsz)
  | 0b1010001 when Mode.flen_at_least mode 64 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FLT_D, ops, opsz)
  | 0b1010011 when Mode.flen_at_least mode 128 && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FLT_Q, ops, opsz)
  | 0b1010000 when Mode.flen_at_least mode 32 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FLE_S, ops, opsz)
  | 0b1010001 when Mode.flen_at_least mode 64 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FLE_D, ops, opsz)
  | 0b1010011 when Mode.flen_at_least mode 128 && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1rs2_fp in
      (M.FLE_Q, ops, opsz)
  | 0b1110000
    when Mode.flen_at_least mode 32
         && ex b 24 20 = 0b00000
         && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCLASS_S, ops, opsz)
  | 0b1110001
    when Mode.flen_at_least mode 64
         && ex b 24 20 = 0b00000
         && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCLASS_D, ops, opsz)
  | 0b1110011
    when Mode.flen_at_least mode 128
         && ex b 24 20 = 0b00000
         && ex b 14 12 = 0b001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCLASS_Q, ops, opsz)
  | 0b1100001 when Mode.flen_at_least mode 64 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_W_D, ops, opsz)
  | 0b1100011 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_W_Q, ops, opsz)
  | 0b1100001 when Mode.flen_at_least mode 64 && ex b 24 20 = 0b00001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_WU_D, ops, opsz)
  | 0b1100011 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_WU_Q, ops, opsz)
  | 0b1101000 when Mode.flen_at_least mode 32 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_S_W, ops, opsz)
  | 0b1101001 when Mode.flen_at_least mode 64 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_D_W, ops, opsz)
  | 0b1101011 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_Q_W, ops, opsz)
  | 0b1101000 when Mode.flen_at_least mode 32 && ex b 24 20 = 0b00001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_S_WU, ops, opsz)
  | 0b1101001 when Mode.flen_at_least mode 64 && ex b 24 20 = 0b00001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_D_WU, ops, opsz)
  | 0b1101011 when Mode.flen_at_least mode 128 && ex b 24 20 = 0b00001 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_Q_WU, ops, opsz)
  | 0b1101000
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 32
         && ex b 24 20 = 0b00010 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_S_L, ops, opsz)
  | 0b1101001
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 64
         && ex b 24 20 = 0b00010 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_D_L, ops, opsz)
  | 0b1101011
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 128
         && ex b 24 20 = 0b00010 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_Q_L, ops, opsz)
  | 0b1101000
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 32
         && ex b 24 20 = 0b00011 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_S_LU, ops, opsz)
  | 0b1101001
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 64
         && ex b 24 20 = 0b00011 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_D_LU, ops, opsz)
  | 0b1101011
    when Mode.word_size_of mode = 64
         && Mode.flen_at_least mode 128
         && ex b 24 20 = 0b00011 ->
      let%map ops = o make_rdrs1_fp in
      (M.FCVT_Q_LU, ops, opsz)
  | 0b1111000
    when Mode.flen_at_least mode 32
         && ex b 24 20 = 0b00000
         && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FMV_W_X, ops, opsz)
  | 0b1111001
    when Mode.flen_at_least mode 64
         && ex b 24 20 = 0b00000
         && ex b 14 12 = 0b000 ->
      let%map ops = o make_rdrs1_fp in
      (M.FMV_D_X, ops, opsz)
  | _ -> Error `Invalid

let parse_branch mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b000 ->
      let%map ops = o (make_rs1rs2rel_b ~opsz) in
      (M.BEQ, ops, opsz)
  | 0b001 ->
      let%map ops = o (make_rs1rs2rel_b ~opsz) in
      (M.BNE, ops, opsz)
  | 0b100 ->
      let%map ops = o (make_rs1rs2rel_b ~opsz) in
      (M.BLT, ops, opsz)
  | 0b101 ->
      let%map ops = o (make_rs1rs2rel_b ~opsz) in
      (M.BGE, ops, opsz)
  | 0b110 ->
      let%map ops = o (make_rs1rs2rel_b ~opsz) in
      (M.BLTU, ops, opsz)
  | 0b111 ->
      let%map ops = o (make_rs1rs2rel_b ~opsz) in
      (M.BGEU, ops, opsz)
  | _ -> Error `Invalid

let parse_system_priv mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match (ex b 31 25, ex b 24 20, ex b 19 15, ex b 11 7) with
  | 0b0000000, 0b00000, 0b00000, 0b00000 -> Ok (M.ECALL, [||], opsz)
  | 0b0000001, 0b00000, 0b00000, 0b00000 -> Ok (M.EBREAK, [||], opsz)
  | 0b0000000, 0b00010, 0b00000, 0b00000 -> Ok (M.URET, [||], opsz)
  | 0b0001000, 0b00010, 0b00000, 0b00000 -> Ok (M.SRET, [||], opsz)
  | 0b0011000, 0b00010, 0b00000, 0b00000 -> Ok (M.MRET, [||], opsz)
  | 0b0001000, 0b00101, 0b00000, 0b00000 -> Ok (M.WFI, [||], opsz)
  | 0b0001001, _, _, 0b00000 ->
      let%map ops = o make_rs1rs2 in
      (M.SFENCE_VMA, ops, opsz)
  | 0b0010001, _, _, 0b00000 ->
      let%map ops = o make_rs1rs2 in
      (M.HFENCE_BVMA, ops, opsz)
  | 0b1010001, _, _, 0b00000 ->
      let%map ops = o make_rs1rs2 in
      (M.HFENCE_GVMA, ops, opsz)
  | _ -> Error `Invalid

let parse_system mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match ex b 14 12 with
  | 0b000 -> parse_system_priv mode b opsz
  | 0b001 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.CSRRW, ops, opsz)
  | 0b010 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.CSRRS, ops, opsz)
  | 0b011 ->
      let%map ops = o (make_rdrs1imm ~opsz) in
      (M.CSRRC, ops, opsz)
  | 0b101 ->
      let%map ops = o (make_rduimmimm ~opsz) in
      (M.CSRRWI, ops, opsz)
  | 0b110 ->
      let%map ops = o (make_rduimmimm ~opsz) in
      (M.CSRRSI, ops, opsz)
  | 0b111 ->
      let%map ops = o (make_rduimmimm ~opsz) in
      (M.CSRRCI, ops, opsz)
  | _ -> Error `Invalid

let parse_base_opcode mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match (ex b 6 5, ex b 4 2) with
  | 0b00, 0b000 -> parse_load mode b opsz
  | 0b00, 0b001 -> parse_load_fp mode b opsz
  (* custom-0 *)
  | 0b00, 0b010 -> Error `Unimpl
  | 0b00, 0b011 -> parse_misc_mem mode b opsz
  | 0b00, 0b100 -> parse_op_imm mode b opsz
  | 0b00, 0b101 ->
      let%map ops = o (make_rdimmu ~opsz) in
      (M.AUIPC, ops, opsz)
  | 0b00, 0b110 -> parse_op_imm_32 mode b opsz
  (* 48b *)
  | 0b00, 0b111 -> Error `Unimpl
  | 0b01, 0b000 -> parse_store mode b opsz
  | 0b01, 0b001 -> parse_store_fp mode b opsz
  (* custom-1 *)
  | 0b01, 0b010 -> Error `Unimpl
  | 0b01, 0b011 -> parse_amo mode b opsz
  | 0b01, 0b100 -> parse_op mode b opsz
  | 0b01, 0b101 ->
      let%map ops = o (make_rdimmu ~opsz) in
      (M.LUI, ops, opsz)
  | 0b01, 0b110 -> parse_op_32 mode b opsz
  (* 64b *)
  | 0b01, 0b111 -> Error `Unimpl
  | 0b10, 0b000 -> parse_madd mode b opsz
  | 0b10, 0b001 -> parse_msub mode b opsz
  | 0b10, 0b010 -> parse_nmsub mode b opsz
  | 0b10, 0b011 -> parse_nmadd mode b opsz
  | 0b10, 0b100 -> parse_op_fp mode b opsz
  (* reserved *)
  | 0b10, 0b101 -> Error `Unimpl
  (* custom-2/rv128 *)
  | 0b10, 0b110 -> Error `Unimpl
  (* 48b *)
  | 0b10, 0b111 -> Error `Unimpl
  | 0b11, 0b000 -> parse_branch mode b opsz
  | 0b11, 0b001 when ex b 14 12 = 0b000 && ex b 11 7 = 0b00000 ->
      let%map ops = o (make_rs1off_j ~opsz) in
      (M.JR, ops, opsz)
  | 0b11, 0b001 when ex b 14 12 = 0b000 ->
      let%map ops = o (make_rdrs1off_j ~opsz) in
      (M.JALR, ops, opsz)
  (* reserved *)
  | 0b11, 0b010 -> Error `Unimpl
  | 0b11, 0b011 when ex b 11 7 = 0b00000 ->
      let%map ops = o (make_rel_j ~opsz) in
      (M.J, ops, opsz)
  | 0b11, 0b011 ->
      let%map ops = o (make_rdrel_j ~opsz) in
      (M.JAL, ops, opsz)
  | 0b11, 0b100 -> parse_system mode b opsz
  (* reserved *)
  | 0b11, 0b101 -> Error `Unimpl
  (* custom-3/rv128 *)
  | 0b11, 0b110 -> Error `Unimpl
  (* >=80b *)
  | 0b11, 0b111 -> Error `Unimpl
  | _ -> Error `Invalid

let parse_opcode mode b opsz =
  match ex b 1 0 with
  | 0b11 -> parse_base_opcode mode b opsz
  | _ -> Error `Invalid

module C = struct
  let rdrs1_gp b =
    let n = ex b 11 7 in
    (* if n > 15 then None else *) R.gp_reg_of_int n

  let rdrs1_fp b =
    let n = ex b 11 7 in
    (* if n > 15 then None else *) R.fp_reg_of_int n

  let rs2_gp b =
    let n = ex b 6 2 in
    (* if n > 15 then None else *) R.gp_reg_of_int n

  let rs2_fp b =
    let n = ex b 6 2 in
    (* if n > 15 then None else *) R.fp_reg_of_int n

  let rs1'_gp b =
    let n = ex b 9 7 lor 0b1000 in
    R.gp_reg_of_int n

  let rs1'_fp b =
    let n = ex b 9 7 lor 0b1000 in
    R.fp_reg_of_int n

  let rd'rs1'_gp b =
    let n = ex b 9 7 lor 0b1000 in
    R.gp_reg_of_int n

  let rd'rs1'_fp b =
    let n = ex b 9 7 lor 0b1000 in
    R.fp_reg_of_int n

  let rd'_gp b =
    let n = ex b 4 2 lor 0b1000 in
    R.gp_reg_of_int n

  let rd'_fp b =
    let n = ex b 4 2 lor 0b1000 in
    R.fp_reg_of_int n

  let rs2'_gp b =
    let n = ex b 4 2 lor 0b1000 in
    R.gp_reg_of_int n

  let rs2'_fp b =
    let n = ex b 4 2 lor 0b1000 in
    R.fp_reg_of_int n

  let rdrs1_gp_reg b = Option.(rdrs1_gp b >>| fun r -> O.Reg r)

  let rdrs1_fp_reg b = Option.(rdrs1_fp b >>| fun r -> O.Reg r)

  let rs2_gp_reg b = Option.(rs2_gp b >>| fun r -> O.Reg r)

  let rs2_fp_reg b = Option.(rs2_fp b >>| fun r -> O.Reg r)

  let rs1'_gp_reg b = Option.(rs1'_gp b >>| fun r -> O.Reg r)

  let rs1'_fp_reg b = Option.(rs1'_fp b >>| fun r -> O.Reg r)

  let rd'rs1'_gp_reg b = Option.(rd'rs1'_gp b >>| fun r -> O.Reg r)

  let rd'rs1'_fp_reg b = Option.(rd'rs1'_fp b >>| fun r -> O.Reg r)

  let rd'_gp_reg b = Option.(rd'_gp b >>| fun r -> O.Reg r)

  let rd'_fp_reg b = Option.(rd'_fp b >>| fun r -> O.Reg r)

  let rs2'_gp_reg b = Option.(rs2'_gp b >>| fun r -> O.Reg r)

  let rs2'_fp_reg b = Option.(rs2'_fp b >>| fun r -> O.Reg r)

  let imm_5_4_2_7_6 b =
    let b_5 = tst_i b 12 in
    let b_4_2 = ex b 6 4 in
    let b_7_6 = ex b 3 2 in
    Int64.of_int (concat (concat (concat b_7_6 b_5 1) b_4_2 3) 0 2)

  let imm_5_4_3_8_6 b =
    let b_5 = tst_i b 12 in
    let b_4_3 = ex b 6 5 in
    let b_8_6 = ex b 4 2 in
    Int64.of_int (concat (concat (concat b_8_6 b_5 1) b_4_3 2) 0 3)

  let imm_5_4_9_6 b =
    let b_5 = tst_i b 12 in
    let b_4 = tst_i b 6 in
    let b_9_6 = ex b 5 2 in
    Int64.of_int (concat (concat (concat b_9_6 b_5 1) b_4 1) 0 4)

  let imm_5_2_7_6 b =
    let b_5_2 = ex b 12 9 in
    let b_7_6 = ex b 8 7 in
    Int64.of_int (concat (concat b_7_6 b_5_2 4) 0 2)

  let imm_5_3_8_6 b =
    let b_5_3 = ex b 12 10 in
    let b_8_6 = ex b 9 7 in
    Int64.of_int (concat (concat b_8_6 b_5_3 3) 0 3)

  let imm_5_4_9_6 b =
    let b_5_4 = ex b 12 11 in
    let b_9_6 = ex b 10 7 in
    Int64.of_int (concat (concat b_9_6 b_5_4 2) 0 4)

  let imm_5_3_2_6 b =
    let b_5_3 = ex b 12 10 in
    let b_2 = tst_i b 6 in
    let b_6 = tst_i b 5 in
    Int64.of_int (concat (concat (concat b_6 b_5_3 3) b_2 1) 0 2)

  let imm_5_3_7_6 b =
    let b_5_3 = ex b 12 10 in
    let b_7_6 = ex b 6 5 in
    Int64.of_int (concat (concat b_7_6 b_5_3 3) 0 3)

  let imm_5_4_8_7_6 b =
    let b_5 = tst_i b 12 in
    let b_4 = tst_i b 11 in
    let b_8 = tst_i b 10 in
    let b_7_6 = ex b 6 5 in
    Int64.of_int
      (concat (concat (concat (concat b_8 b_7_6 2) b_5 1) b_4 1) 0 4)

  let imm_11_4_9_8_10_6_7_3_1_5 b =
    let b_11 = tst_i b 12 in
    let b_4 = tst_i b 11 in
    let b_9_8 = ex b 10 9 in
    let b_10 = tst_i b 8 in
    let b_6 = tst_i b 7 in
    let b_7 = tst_i b 6 in
    let b_3_1 = ex b 5 3 in
    let b_5 = tst_i b 2 in
    Int64.of_int
      (concat
         (concat
            (concat
               (concat
                  (concat
                     (concat (concat (concat b_11 b_10 1) b_9_8 2) b_7 1)
                     b_6 1)
                  b_5 1)
               b_4 1)
            b_3_1 3)
         0 1)

  let imm_8_4_3_7_6_2_1_5 b =
    let b_8 = tst_i b 12 in
    let b_4_3 = ex b 11 10 in
    let b_7_6 = ex b 6 5 in
    let b_2_1 = ex b 4 3 in
    let b_5 = tst_i b 2 in
    Int64.of_int
      (concat
         (concat
            (concat (concat (concat b_8 b_7_6 2) b_5 1) b_4_3 2)
            b_2_1 2)
         0 1)

  let imm_5_4_0 b =
    let b_5 = tst_i b 12 in
    let b_4_0 = ex b 6 2 in
    Int64.of_int (concat b_5 b_4_0 5)

  let nzimm_17_16_12 b =
    let b_17 = tst_i b 12 in
    let b_16_12 = ex b 6 2 in
    Int64.of_int (concat b_17 b_16_12 5)

  let nzimm_5_4_0 b =
    let b_5 = tst_i b 12 in
    let b_4_0 = ex b 6 2 in
    Int64.of_int (concat b_5 b_4_0 5)

  let nzimm_9_4_6_8_7_5 b =
    let b_9 = tst_i b 12 in
    let b_4 = tst_i b 6 in
    let b_6 = tst_i b 5 in
    let b_8_7 = ex b 4 3 in
    let b_5 = tst_i b 2 in
    Int64.of_int
      (concat
         (concat (concat (concat (concat b_9 b_8_7 2) b_6 1) b_5 1) b_4 1)
         0 3)

  let nzimm_5_4_9_6_2_3 b =
    let b_5_4 = ex b 12 11 in
    let b_9_6 = ex b 10 7 in
    let b_2 = tst_i b 6 in
    let b_3 = tst_i b 5 in
    Int64.of_int
      (concat (concat (concat (concat b_9_6 b_5_4 2) b_3 1) b_2 1) 0 2)

  let shamt_5_4_0 b =
    let b_5 = tst_i b 12 in
    let b_4_0 = ex b 6 2 in
    Int64.of_int (concat b_5 b_4_0 5)

  let make_lwsp b ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let disp = imm_5_4_2_7_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 32}) in
      [|rd; mem|])

  let make_ldsp b ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let disp = imm_5_4_3_8_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 64}) in
      [|rd; mem|])

  let make_lqsp b ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let disp = imm_5_4_9_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 128}) in
      [|rd; mem|])

  let make_flwsp b ~mode =
    Option.(
      rdrs1_fp_reg b
      >>| fun rd ->
      let disp = imm_5_4_2_7_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 32}) in
      [|rd; mem|])

  let make_fldsp b ~mode =
    Option.(
      rdrs1_fp_reg b
      >>| fun rd ->
      let disp = imm_5_4_3_8_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 64}) in
      [|rd; mem|])

  let make_swsp b ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let disp = imm_5_2_7_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 32}) in
      [|rd; mem|])

  let make_sdsp b ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let disp = imm_5_3_8_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 64}) in
      [|rd; mem|])

  let make_sqsp b ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let disp = imm_5_4_9_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 128}) in
      [|rd; mem|])

  let make_fswsp b ~mode =
    Option.(
      rdrs1_fp_reg b
      >>| fun rd ->
      let disp = imm_5_2_7_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 32}) in
      [|rd; mem|])

  let make_fsdsp b ~mode =
    Option.(
      rdrs1_fp_reg b
      >>| fun rd ->
      let disp = imm_5_3_8_6 b in
      let mem = O.(Mem {base= R.SP; disp; size= 64}) in
      [|rd; mem|])

  let make_lw b ~mode =
    Option.(
      rd'_gp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_3_2_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 32}) in
      [|rd'; mem|])

  let make_ld b ~mode =
    Option.(
      rd'_gp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_3_7_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 64}) in
      [|rd'; mem|])

  let make_lq b ~mode =
    Option.(
      rd'_gp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_4_8_7_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 128}) in
      [|rd'; mem|])

  let make_flw b ~mode =
    Option.(
      rd'_fp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_3_2_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 32}) in
      [|rd'; mem|])

  let make_fld b ~mode =
    Option.(
      rd'_fp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_3_7_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 64}) in
      [|rd'; mem|])

  let make_sw b ~mode =
    Option.(
      rd'_gp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_3_2_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 32}) in
      [|rd'; mem|])

  let make_sd b ~mode =
    Option.(
      rd'_gp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_3_7_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 64}) in
      [|rd'; mem|])

  let make_sq b ~mode =
    Option.(
      rd'_gp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_4_8_7_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 128}) in
      [|rd'; mem|])

  let make_fsw b ~mode =
    Option.(
      rd'_fp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_3_2_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 32}) in
      [|rd'; mem|])

  let make_fsd b ~mode =
    Option.(
      rd'_fp_reg b
      >>= fun rd' ->
      rs1'_gp b
      >>| fun rs1' ->
      let disp = imm_5_3_7_6 b in
      let mem = O.(Mem {base= rs1'; disp; size= 64}) in
      [|rd'; mem|])

  let make_j b ~opsz ~mode =
    let value = sign_ext (imm_11_4_9_8_10_6_7_3_1_5 b) 12 in
    let rel = O.Imm Imm.{value; kind= Relative; size= opsz} in
    Some [|rel|]

  let make_jal = make_j

  let make_jr b ~mode = Option.(rdrs1_gp_reg b >>| fun rs1 -> [|rs1|])

  let make_jalr = make_jr

  let make_beqz b ~opsz ~mode =
    Option.(
      rs1'_gp_reg b
      >>| fun rs1' ->
      let value = sign_ext (imm_8_4_3_7_6_2_1_5 b) 9 in
      let rel = O.Imm Imm.{value; kind= Relative; size= opsz} in
      [|rs1'; rel|])

  let make_bnez b ~opsz ~mode =
    Option.(
      rs1'_gp_reg b
      >>| fun rs1' ->
      let value = sign_ext (imm_8_4_3_7_6_2_1_5 b) 9 in
      let rel = O.Imm Imm.{value; kind= Relative; size= opsz} in
      [|rs1'; rel|])

  let make_li b ~opsz ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let value = sign_ext (imm_5_4_0 b) 6 in
      let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
      [|rd; imm|])

  let make_lui b ~opsz ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let value = sign_ext (nzimm_17_16_12 b) 6 in
      let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
      [|rd; imm|])

  let make_addi b ~opsz ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let value = sign_ext (nzimm_5_4_0 b) 6 in
      let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
      [|rd; imm|])

  let make_addiw = make_li

  let make_addi16sp b ~opsz ~mode =
    let value = sign_ext (nzimm_9_4_6_8_7_5 b) 9 in
    let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
    Some [|O.Reg R.SP; imm|]

  let make_addi4spn b ~opsz ~mode =
    Option.(
      rd'_gp_reg b
      >>| fun rd' ->
      let value = nzimm_5_4_9_6_2_3 b in
      let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
      [|rd'; O.Reg R.SP; imm|])

  let make_slli b ~opsz ~mode =
    Option.(
      rdrs1_gp_reg b
      >>| fun rd ->
      let value = shamt_5_4_0 b in
      let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
      [|rd; imm|])

  let make_srli b ~opsz ~mode =
    Option.(
      rd'rs1'_gp_reg b
      >>| fun rd' ->
      let value = shamt_5_4_0 b in
      let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
      [|rd'; imm|])

  let make_srai = make_srli

  let make_andi b ~opsz ~mode =
    Option.(
      rd'rs1'_gp_reg b
      >>| fun rd' ->
      let value = sign_ext (imm_5_4_0 b) 6 in
      let imm = O.Imm Imm.{value; kind= Literal; size= opsz} in
      [|rd'; imm|])

  let make_mv b ~mode =
    Option.(
      rd'rs1'_gp_reg b
      >>= fun rd' -> rs2'_gp_reg b >>| fun rs2' -> [|rd'; rs2'|])

  let make_add = make_mv

  let make_and = make_mv

  let make_or = make_mv

  let make_xor = make_mv

  let make_sub = make_mv

  let make_addw = make_mv

  let make_subw = make_mv
end

let parse_rvc_opcode mode b opsz =
  let open Result.Let_syntax in
  let o = ops_or_error mode b in
  match (ex b 15 13, ex b 1 0) with
  (* Illegal instruction *)
  | 0b000, 0b00 when ex b 12 2 = 0b00000000000 -> Error `Invalid
  | 0b000, 0b00 ->
      let%map ops = o C.(make_addi4spn ~opsz) in
      (M.C_ADDI4SPN, ops, opsz)
  | 0b001, 0b00 when Mode.word_size_of mode <= 64 ->
      let%map ops = o C.make_fld in
      (M.C_FLD, ops, opsz)
  (* C.LQ (RV128) *)
  | 0b001, 0b00 -> Error `Unimpl
  | 0b010, 0b00 ->
      let%map ops = o C.make_lw in
      (M.C_LW, ops, opsz)
  | 0b011, 0b00
    when Mode.word_size_of mode = 32 && Mode.flen_at_least mode 32 ->
      let%map ops = o C.make_flw in
      (M.C_FLW, ops, opsz)
  | 0b011, 0b00 when Mode.word_size_of mode = 64 ->
      let%map ops = o C.make_ld in
      (M.C_LD, ops, opsz)
  (* Reserved *)
  | 0b100, 0b00 -> Error `Unimpl
  | 0b101, 0b00
    when Mode.word_size_of mode <= 64 && Mode.flen_at_least mode 64 ->
      let%map ops = o C.make_fsd in
      let opsz = 64 in
      (M.C_FSD, ops, opsz)
  (* C.SQ *)
  | 0b101, 0b00 -> Error `Unimpl
  | 0b110, 0b00 ->
      let%map ops = o C.make_sw in
      let opsz = 32 in
      (M.C_SW, ops, opsz)
  | 0b111, 0b00
    when Mode.word_size_of mode = 32 && Mode.flen_at_least mode 32 ->
      let%map ops = o C.make_fsw in
      (M.C_FSW, ops, opsz)
  | 0b111, 0b00 ->
      let%map ops = o C.make_sd in
      let opsz = 64 in
      (M.C_SD, ops, opsz)
  | 0b000, 0b01 when ex b 11 7 = 0b00000 -> Ok (M.C_NOP, [||], opsz)
  | 0b000, 0b01 ->
      let%map ops = o C.(make_addi ~opsz) in
      (M.C_ADDI, ops, opsz)
  | 0b001, 0b01 when Mode.word_size_of mode = 32 ->
      let%map ops = o C.(make_jal ~opsz) in
      (M.C_JAL, ops, opsz)
  | 0b001, 0b01 when Mode.word_size_of mode = 64 ->
      let%map ops = o C.(make_addiw ~opsz) in
      (M.C_ADDIW, ops, opsz)
  | 0b010, 0b01 ->
      let%map ops = o C.(make_li ~opsz) in
      (M.C_LI, ops, opsz)
  | 0b011, 0b01 when ex b 11 7 = 2 ->
      let%map ops = o C.(make_addi16sp ~opsz) in
      (M.C_ADDI16SP, ops, opsz)
  | 0b011, 0b01 ->
      let%map ops = o C.(make_lui ~opsz) in
      (M.C_LUI, ops, opsz)
  | 0b100, 0b01 when ex b 11 10 = 0b00 ->
      let%map ops = o C.(make_srli ~opsz) in
      (M.C_SRLI, ops, opsz)
  (* C.SRLI64 *)
  | 0b100, 0b01 when ex b 12 10 = 0b000 && ex b 6 2 = 0b00000 ->
      Error `Unimpl
  | 0b100, 0b01 when ex b 11 10 = 0b01 ->
      let%map ops = o C.(make_srai ~opsz) in
      (M.C_SRAI, ops, opsz)
  (* C.SRAI64 *)
  | 0b100, 0b01 when ex b 12 10 = 0b001 && ex b 6 2 = 0b00000 ->
      Error `Unimpl
  | 0b100, 0b01 when ex b 11 10 = 0b10 ->
      let%map ops = o C.(make_andi ~opsz) in
      (M.C_ANDI, ops, opsz)
  | 0b100, 0b01 when ex b 12 10 = 0b011 && ex b 6 5 = 0b00 ->
      let%map ops = o C.make_sub in
      (M.C_SUB, ops, opsz)
  | 0b100, 0b01 when ex b 12 10 = 0b011 && ex b 6 5 = 0b01 ->
      let%map ops = o C.make_xor in
      (M.C_XOR, ops, opsz)
  | 0b100, 0b01 when ex b 12 11 = 0b011 && ex b 6 5 = 0b10 ->
      let%map ops = o C.make_or in
      (M.C_OR, ops, opsz)
  | 0b100, 0b01 when ex b 12 11 = 0b011 && ex b 6 5 = 0b11 ->
      let%map ops = o C.make_and in
      (M.C_AND, ops, opsz)
  | 0b100, 0b01
    when Mode.word_size_of mode = 64 && ex b 12 10 = 0b111 && ex b 6 5 = 0b00
    ->
      let%map ops = o C.make_subw in
      (M.C_SUBW, ops, opsz)
  | 0b100, 0b01
    when Mode.word_size_of mode = 64 && ex b 12 10 = 0b111 && ex b 6 5 = 0b01
    ->
      let%map ops = o C.make_addw in
      (M.C_ADDW, ops, opsz)
  (* Reserved *)
  | 0b100, 0b01 when ex b 12 10 = 0b111 && ex b 6 5 = 0b10 -> Error `Unimpl
  (* Reserved *)
  | 0b100, 0b01 when ex b 12 10 = 0b111 && ex b 6 5 = 0b11 -> Error `Unimpl
  | 0b101, 0b01 ->
      let%map ops = o C.(make_j ~opsz) in
      (M.C_J, ops, opsz)
  | 0b110, 0b01 ->
      let%map ops = o C.(make_beqz ~opsz) in
      (M.C_BEQZ, ops, opsz)
  | 0b111, 0b01 ->
      let%map ops = o C.(make_bnez ~opsz) in
      (M.C_BNEZ, ops, opsz)
  (* C.SLLI64 *)
  | 0b000, 0b10 when (not (tst b 12)) && ex b 6 2 = 0b00000 -> Error `Unimpl
  | 0b000, 0b10 ->
      let%map ops = o C.(make_slli ~opsz) in
      (M.C_SLLI, ops, opsz)
  | 0b001, 0b10 when Mode.word_size_of mode <= 64 ->
      let%map ops = o C.make_fldsp in
      (M.C_FLDSP, ops, opsz)
  (* C.LQSP *)
  | 0b001, 0b10 -> Error `Unimpl
  | 0b010, 0b10 ->
      let%map ops = o C.make_lwsp in
      (M.C_LWSP, ops, opsz)
  | 0b011, 0b10 when Mode.word_size_of mode = 32 ->
      let%map ops = o C.make_flwsp in
      (M.C_FLWSP, ops, opsz)
  | 0b011, 0b10 ->
      let%map ops = o C.make_ldsp in
      (M.C_LDSP, ops, opsz)
  | 0b100, 0b10
    when (not (tst b 12)) && ex b 11 7 <> 0b00000 && ex b 6 2 = 0b00000 ->
      let%map ops = o C.make_jr in
      (M.C_JR, ops, opsz)
  | 0b100, 0b10 when not (tst b 12) ->
      let%map ops = o C.make_mv in
      (M.C_MV, ops, opsz)
  | 0b100, 0b10 when tst b 12 && ex b 11 2 = 0b0000000000 ->
      Ok (M.C_EBREAK, [||], opsz)
  | 0b100, 0b10 when tst b 12 && ex b 11 7 <> 0b00000 && ex b 6 2 = 0b00000
    ->
      let%map ops = o C.make_jalr in
      (M.C_JALR, ops, opsz)
  | 0b100, 0b10 when tst b 12 ->
      let%map ops = o C.make_add in
      (M.C_ADD, ops, opsz)
  | 0b101, 0b10 when Mode.word_size_of mode <= 64 ->
      let%map ops = o C.make_fsdsp in
      let opsz = 64 in
      (M.C_FSDSP, ops, opsz)
  | 0b101, 0b10 -> Error `Unimpl
  | 0b110, 0b10 ->
      let%map ops = o C.make_swsp in
      let opsz = 32 in
      (M.C_SWSP, ops, opsz)
  | 0b111, 0b10 when Mode.word_size_of mode = 32 ->
      let%map ops = o C.make_fswsp in
      let opsz = 32 in
      (M.C_FSWSP, ops, opsz)
  | 0b111, 0b10 ->
      let%map ops = o C.make_sdsp in
      let opsz = 64 in
      (M.C_SDSP, ops, opsz)
  | _ -> Error `Invalid

let decode mode bytes =
  let open Result.Let_syntax in
  assert (Int.num_bits > 31);
  let opsz = Mode.word_size_of mode in
  let%map bytes, mnemonic, operands, opsz =
    if Mode.has_compact mode then
      let len = Mode.min_instr_length mode in
      let blen = Bytes.length bytes in
      let%bind _ = Result.ok_if_true ~error:`OOB (blen >= len) in
      let b1 = Char.to_int @@ Bytes.get bytes 0 in
      let b2 = Char.to_int @@ Bytes.get bytes 1 in
      if b1 land 0b11 <> 0b11 then
        let b = b1 lor (b2 lsl 8) land 0xFFFF in
        let%map mnemonic, operands, opsz = parse_rvc_opcode mode b opsz in
        (Bytes.subo bytes ~len, mnemonic, operands, opsz)
      else
        let len = I.max_length in
        let%bind _ = Result.ok_if_true ~error:`OOB (blen >= len) in
        let b3 = Char.to_int @@ Bytes.get bytes 2 in
        let b4 = Char.to_int @@ Bytes.get bytes 3 in
        let b =
          b1 lor (b2 lsl 8) lor (b3 lsl 16) lor (b4 lsl 24) land 0xFFFFFFFF
        in
        let%map mnemonic, operands, opsz = parse_opcode mode b opsz in
        (Bytes.subo bytes ~len, mnemonic, operands, opsz)
    else
      let%bind bytes =
        Option.try_with (fun () -> Bytes.subo bytes ~len:I.max_length)
        |> Result.of_option ~error:`OOB
      in
      let b1 = Char.to_int @@ Bytes.get bytes 0 in
      let b2 = Char.to_int @@ Bytes.get bytes 1 in
      let b3 = Char.to_int @@ Bytes.get bytes 2 in
      let b4 = Char.to_int @@ Bytes.get bytes 3 in
      let b =
        b1 lor (b2 lsl 8) lor (b3 lsl 16) lor (b4 lsl 24) land 0xFFFFFFFF
      in
      let%map mnemonic, operands, opsz = parse_opcode mode b opsz in
      (bytes, mnemonic, operands, opsz)
  in
  I.{mode; mnemonic; bytes; operands; opsz}

let decode_exn mode bytes =
  match decode mode bytes with
  | Ok instr -> instr
  | Error err -> raise (Riscv_decode_error err)
