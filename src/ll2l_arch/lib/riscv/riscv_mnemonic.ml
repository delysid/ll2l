open Core_kernel

type t =
  | ADD
  | ADDI
  | ADDIW
  | ADDW
  | AMOADD_D
  | AMOADD_D_AQ
  | AMOADD_D_AQ_RL
  | AMOADD_D_RL
  | AMOADD_W
  | AMOADD_W_AQ
  | AMOADD_W_AQ_RL
  | AMOADD_W_RL
  | AMOAND_D
  | AMOAND_D_AQ
  | AMOAND_D_AQ_RL
  | AMOAND_D_RL
  | AMOAND_W
  | AMOAND_W_AQ
  | AMOAND_W_AQ_RL
  | AMOAND_W_RL
  | AMOMAXU_D
  | AMOMAXU_D_AQ
  | AMOMAXU_D_AQ_RL
  | AMOMAXU_D_RL
  | AMOMAXU_W
  | AMOMAXU_W_AQ
  | AMOMAXU_W_AQ_RL
  | AMOMAXU_W_RL
  | AMOMAX_D
  | AMOMAX_D_AQ
  | AMOMAX_D_AQ_RL
  | AMOMAX_D_RL
  | AMOMAX_W
  | AMOMAX_W_AQ
  | AMOMAX_W_AQ_RL
  | AMOMAX_W_RL
  | AMOMINU_D
  | AMOMINU_D_AQ
  | AMOMINU_D_AQ_RL
  | AMOMINU_D_RL
  | AMOMINU_W
  | AMOMINU_W_AQ
  | AMOMINU_W_AQ_RL
  | AMOMINU_W_RL
  | AMOMIN_D
  | AMOMIN_D_AQ
  | AMOMIN_D_AQ_RL
  | AMOMIN_D_RL
  | AMOMIN_W
  | AMOMIN_W_AQ
  | AMOMIN_W_AQ_RL
  | AMOMIN_W_RL
  | AMOOR_D
  | AMOOR_D_AQ
  | AMOOR_D_AQ_RL
  | AMOOR_D_RL
  | AMOOR_W
  | AMOOR_W_AQ
  | AMOOR_W_AQ_RL
  | AMOOR_W_RL
  | AMOSWAP_D
  | AMOSWAP_D_AQ
  | AMOSWAP_D_AQ_RL
  | AMOSWAP_D_RL
  | AMOSWAP_W
  | AMOSWAP_W_AQ
  | AMOSWAP_W_AQ_RL
  | AMOSWAP_W_RL
  | AMOXOR_D
  | AMOXOR_D_AQ
  | AMOXOR_D_AQ_RL
  | AMOXOR_D_RL
  | AMOXOR_W
  | AMOXOR_W_AQ
  | AMOXOR_W_AQ_RL
  | AMOXOR_W_RL
  | AND
  | ANDI
  | AUIPC
  | BEQ
  | BGE
  | BGEU
  | BLT
  | BLTU
  | BNE
  | CSRRC
  | CSRRCI
  | CSRRS
  | CSRRSI
  | CSRRW
  | CSRRWI
  | C_ADD
  | C_ADDI
  | C_ADDI16SP
  | C_ADDI4SPN
  | C_ADDIW
  | C_ADDW
  | C_AND
  | C_ANDI
  | C_BEQZ
  | C_BNEZ
  | C_EBREAK
  | C_FLD
  | C_FLDSP
  | C_FLW
  | C_FLWSP
  | C_FSD
  | C_FSDSP
  | C_FSW
  | C_FSWSP
  | C_J
  | C_JAL
  | C_JALR
  | C_JR
  | C_LD
  | C_LDSP
  | C_LI
  | C_LUI
  | C_LW
  | C_LWSP
  | C_MV
  | C_NOP
  | C_OR
  | C_SD
  | C_SDSP
  | C_SLLI
  | C_SRAI
  | C_SRLI
  | C_SUB
  | C_SUBW
  | C_SW
  | C_SWSP
  | C_XOR
  | DIV
  | DIVU
  | DIVUW
  | DIVW
  | EBREAK
  | ECALL
  | FADD_D
  | FADD_Q
  | FADD_S
  | FCLASS_D
  | FCLASS_Q
  | FCLASS_S
  | FCVT_D_L
  | FCVT_D_LU
  | FCVT_D_Q
  | FCVT_D_S
  | FCVT_D_W
  | FCVT_D_WU
  | FCVT_LU_D
  | FCVT_LU_Q
  | FCVT_LU_S
  | FCVT_L_D
  | FCVT_L_Q
  | FCVT_L_S
  | FCVT_Q_D
  | FCVT_Q_LU
  | FCVT_Q_L
  | FCVT_Q_S
  | FCVT_Q_WU
  | FCVT_Q_W
  | FCVT_S_D
  | FCVT_S_L
  | FCVT_S_LU
  | FCVT_S_Q
  | FCVT_S_W
  | FCVT_S_WU
  | FCVT_WU_D
  | FCVT_WU_Q
  | FCVT_WU_S
  | FCVT_W_D
  | FCVT_W_Q
  | FCVT_W_S
  | FDIV_D
  | FDIV_Q
  | FDIV_S
  | FENCE
  | FENCE_I
  | FENCE_TSO
  | FEQ_D
  | FEQ_Q
  | FEQ_S
  | FLD
  | FLE_D
  | FLE_Q
  | FLE_S
  | FLQ
  | FLT_D
  | FLT_Q
  | FLT_S
  | FLW
  | FMADD_D
  | FMADD_Q
  | FMADD_S
  | FMAX_D
  | FMAX_Q
  | FMAX_S
  | FMIN_D
  | FMIN_Q
  | FMIN_S
  | FMSUB_D
  | FMSUB_Q
  | FMSUB_S
  | FMUL_D
  | FMUL_Q
  | FMUL_S
  | FMV_D_X
  | FMV_W_X
  | FMV_X_D
  | FMV_X_W
  | FNMADD_D
  | FNMADD_Q
  | FNMADD_S
  | FNMSUB_D
  | FNMSUB_Q
  | FNMSUB_S
  | FSD
  | FSGNJN_D
  | FSGNJN_Q
  | FSGNJN_S
  | FSGNJX_D
  | FSGNJX_Q
  | FSGNJX_S
  | FSGNJ_D
  | FSGNJ_Q
  | FSGNJ_S
  | FSQ
  | FSQRT_D
  | FSQRT_Q
  | FSQRT_S
  | FSUB_D
  | FSUB_Q
  | FSUB_S
  | FSW
  | HFENCE_BVMA
  | HFENCE_GVMA
  | J
  | JAL
  | JALR
  | JR
  | LB
  | LBU
  | LD
  | LH
  | LHU
  | LR_D
  | LR_D_AQ
  | LR_D_AQ_RL
  | LR_D_RL
  | LR_W
  | LR_W_AQ
  | LR_W_AQ_RL
  | LR_W_RL
  | LUI
  | LW
  | LWU
  | MRET
  | MUL
  | MULH
  | MULHSU
  | MULHU
  | MULW
  | OR
  | ORI
  | REM
  | REMU
  | REMUW
  | REMW
  | SB
  | SC_D
  | SC_D_AQ
  | SC_D_AQ_RL
  | SC_D_RL
  | SC_W
  | SC_W_AQ
  | SC_W_AQ_RL
  | SC_W_RL
  | SD
  | SFENCE_VMA
  | SH
  | SLL
  | SLLI
  | SLLIW
  | SLLW
  | SLT
  | SLTI
  | SLTIU
  | SLTU
  | SRA
  | SRAI
  | SRAIW
  | SRAW
  | SRET
  | SRL
  | SRLI
  | SRLIW
  | SRLW
  | SUB
  | SUBW
  | SW
  | URET
  | WFI
  | XOR
  | XORI
[@@deriving equal]

let to_string = function
  | ADD -> "ADD"
  | ADDI -> "ADDI"
  | ADDIW -> "ADDIW"
  | ADDW -> "ADDW"
  | AMOADD_D -> "AMOADD.D"
  | AMOADD_D_AQ -> "AMOADD.D.AQ"
  | AMOADD_D_AQ_RL -> "AMOADD.D.AQ.RL"
  | AMOADD_D_RL -> "AMOADD.D.RL"
  | AMOADD_W -> "AMOADD.W"
  | AMOADD_W_AQ -> "AMOADD.W.AQ"
  | AMOADD_W_AQ_RL -> "AMOADD.W.AQ.RL"
  | AMOADD_W_RL -> "AMOADD.W.RL"
  | AMOAND_D -> "AMOAND.D"
  | AMOAND_D_AQ -> "AMOAND.D.AQ"
  | AMOAND_D_AQ_RL -> "AMOAND.D.AQ.RL"
  | AMOAND_D_RL -> "AMOAND.D.RL"
  | AMOAND_W -> "AMOAND.W"
  | AMOAND_W_AQ -> "AMOAND.W.AQ"
  | AMOAND_W_AQ_RL -> "AMOAND.W.AQ.RL"
  | AMOAND_W_RL -> "AMOAND.W.RL"
  | AMOMAXU_D -> "AMOMAXU.D"
  | AMOMAXU_D_AQ -> "AMOMAXU.D_AQ"
  | AMOMAXU_D_AQ_RL -> "AMOMAXU.D.AQ.RL"
  | AMOMAXU_D_RL -> "AMOMAXU.D.RL"
  | AMOMAXU_W -> "AMOMAXU.W"
  | AMOMAXU_W_AQ -> "AMOMAXU.W.AQ"
  | AMOMAXU_W_AQ_RL -> "AMOMAXU.W.AQ.RL"
  | AMOMAXU_W_RL -> "AMOMAXU.W.RL"
  | AMOMAX_D -> "AMOMAX.D"
  | AMOMAX_D_AQ -> "AMOMAX.D.AQ"
  | AMOMAX_D_AQ_RL -> "AMOMAX.D.AQ.RL"
  | AMOMAX_D_RL -> "AMOMAX.D.RL"
  | AMOMAX_W -> "AMOMAX.W"
  | AMOMAX_W_AQ -> "AMOMAX.W.AQ"
  | AMOMAX_W_AQ_RL -> "AMOMAX.W.AQ.RL"
  | AMOMAX_W_RL -> "AMOMAX.W.RL"
  | AMOMINU_D -> "AMOMINU.D"
  | AMOMINU_D_AQ -> "AMOMINU.D.AQ"
  | AMOMINU_D_AQ_RL -> "AMOMINU.D.AQ.RL"
  | AMOMINU_D_RL -> "AMOMINU.D.RL"
  | AMOMINU_W -> "AMOMINU.W"
  | AMOMINU_W_AQ -> "AMOMINU.W.AQ"
  | AMOMINU_W_AQ_RL -> "AMOMINU.W.AQ.RL"
  | AMOMINU_W_RL -> "AMOMINU.W.RL"
  | AMOMIN_D -> "AMOMIN.D"
  | AMOMIN_D_AQ -> "AMOMIN.D.AQ"
  | AMOMIN_D_AQ_RL -> "AMOMIN.D.AQ.RL"
  | AMOMIN_D_RL -> "AMOMIN.D.RL"
  | AMOMIN_W -> "AMOMIN.W"
  | AMOMIN_W_AQ -> "AMOMIN.W.AQ"
  | AMOMIN_W_AQ_RL -> "AMOMIN.W.AQ.RL"
  | AMOMIN_W_RL -> "AMOMIN.W.RL"
  | AMOOR_D -> "AMOOR.D"
  | AMOOR_D_AQ -> "AMOOR.D.AQ"
  | AMOOR_D_AQ_RL -> "AMOOR.D.AQ.RL"
  | AMOOR_D_RL -> "AMOOR.D.RL"
  | AMOOR_W -> "AMOOR.W"
  | AMOOR_W_AQ -> "AMOOR.W.AQ"
  | AMOOR_W_AQ_RL -> "AMOOR.W.AQ.RL"
  | AMOOR_W_RL -> "AMOOR.W.RL"
  | AMOSWAP_D -> "AMOSWAP.D"
  | AMOSWAP_D_AQ -> "AMOSWAP.D.AQ"
  | AMOSWAP_D_AQ_RL -> "AMOSWAP.D.AQ.RL"
  | AMOSWAP_D_RL -> "AMOSWAP.D.RL"
  | AMOSWAP_W -> "AMOSWAP.W"
  | AMOSWAP_W_AQ -> "AMOSWAP.W.AQ"
  | AMOSWAP_W_AQ_RL -> "AMOSWAP.W.AQ.RL"
  | AMOSWAP_W_RL -> "AMOSWAP.W.RL"
  | AMOXOR_D -> "AMOXOR.D"
  | AMOXOR_D_AQ -> "AMOXOR.D.AQ"
  | AMOXOR_D_AQ_RL -> "AMOXOR.D.AQ.RL"
  | AMOXOR_D_RL -> "AMOXOR.D.RL"
  | AMOXOR_W -> "AMOXOR.W"
  | AMOXOR_W_AQ -> "AMOXOR.W.AQ"
  | AMOXOR_W_AQ_RL -> "AMOXOR.W.AQ.RL"
  | AMOXOR_W_RL -> "AMOXOR.W.RL"
  | AND -> "AND"
  | ANDI -> "ANDI"
  | AUIPC -> "AUIPC"
  | BEQ -> "BEQ"
  | BGE -> "BGE"
  | BGEU -> "BGEU"
  | BLT -> "BLT"
  | BLTU -> "BLTU"
  | BNE -> "BNE"
  | CSRRC -> "CSRRC"
  | CSRRCI -> "CSRRCI"
  | CSRRS -> "CSRRS"
  | CSRRSI -> "CSRRSI"
  | CSRRW -> "CSRRW"
  | CSRRWI -> "CSRRWI"
  | C_ADD -> "C.ADD"
  | C_ADDI -> "C.ADDI"
  | C_ADDI16SP -> "C.ADDI16SP"
  | C_ADDI4SPN -> "C.ADDI4SPN"
  | C_ADDIW -> "C.ADDIW"
  | C_ADDW -> "C.ADDW"
  | C_AND -> "C.AND"
  | C_ANDI -> "C.ANDI"
  | C_BEQZ -> "C.BEQZ"
  | C_BNEZ -> "C.BNEZ"
  | C_EBREAK -> "C.EBREAK"
  | C_FLD -> "C.FLD"
  | C_FLDSP -> "C.FLDSP"
  | C_FLW -> "C.FLW"
  | C_FLWSP -> "C.FLWSP"
  | C_FSD -> "C.FSD"
  | C_FSDSP -> "C.FSDSP"
  | C_FSW -> "C.FSW"
  | C_FSWSP -> "C.FSWSP"
  | C_J -> "C.J"
  | C_JAL -> "C.JAL"
  | C_JALR -> "C.JALR"
  | C_JR -> "C.JR"
  | C_LD -> "C.LD"
  | C_LDSP -> "C.LDSP"
  | C_LI -> "C.LI"
  | C_LUI -> "C.LUI"
  | C_LW -> "C.LW"
  | C_LWSP -> "C.LWSP"
  | C_MV -> "C.MV"
  | C_NOP -> "C.NOP"
  | C_OR -> "C.OR"
  | C_SD -> "C.SD"
  | C_SDSP -> "C.SDSP"
  | C_SLLI -> "C.SLLI"
  | C_SRAI -> "C.SRAI"
  | C_SRLI -> "C.SRLI"
  | C_SUB -> "C.SUB"
  | C_SUBW -> "C.SUBW"
  | C_SW -> "C.SW"
  | C_SWSP -> "C.SWSP"
  | C_XOR -> "C.XOR"
  | DIV -> "DIV"
  | DIVU -> "DIVU"
  | DIVUW -> "DIVUW"
  | DIVW -> "DIVW"
  | EBREAK -> "EBREAK"
  | ECALL -> "ECALL"
  | FADD_D -> "FADD.D"
  | FADD_Q -> "FADD.Q"
  | FADD_S -> "FADD.S"
  | FCLASS_D -> "FCLASS.D"
  | FCLASS_Q -> "FCLASS.Q"
  | FCLASS_S -> "FCLASS.S"
  | FCVT_D_L -> "FCVT.D.L"
  | FCVT_D_LU -> "FCVT.D.LU"
  | FCVT_D_Q -> "FCVT.D.Q"
  | FCVT_D_S -> "FCVT.D.S"
  | FCVT_D_W -> "FCVT.D.W"
  | FCVT_D_WU -> "FCVT.D.WU"
  | FCVT_LU_D -> "FCVT.LU.D"
  | FCVT_LU_Q -> "FCVT.LU.Q"
  | FCVT_LU_S -> "FCVT.LU.S"
  | FCVT_L_D -> "FCVT.L.D"
  | FCVT_L_Q -> "FCVT.L.Q"
  | FCVT_L_S -> "FCVT.L.S"
  | FCVT_Q_D -> "FCVT.Q.D"
  | FCVT_Q_LU -> "FCVT.Q.LU"
  | FCVT_Q_L -> "FCVT.Q.L"
  | FCVT_Q_S -> "FCVT.Q.S"
  | FCVT_Q_WU -> "FCVT.Q.WU"
  | FCVT_Q_W -> "FCVT.Q.W"
  | FCVT_S_D -> "FCVT.S.D"
  | FCVT_S_L -> "FCVT.S.L"
  | FCVT_S_LU -> "FCVT.S.LU"
  | FCVT_S_Q -> "FCVT.S.Q"
  | FCVT_S_W -> "FCVT.S.W"
  | FCVT_S_WU -> "FCVT.S.WU"
  | FCVT_WU_D -> "FCVT.WU.D"
  | FCVT_WU_Q -> "FCVT.WU.Q"
  | FCVT_WU_S -> "FCVT.WU.S"
  | FCVT_W_D -> "FCVT.W.D"
  | FCVT_W_Q -> "FCVT.W.Q"
  | FCVT_W_S -> "FCVT.W.S"
  | FDIV_D -> "FDIV.D"
  | FDIV_Q -> "FDIV.Q"
  | FDIV_S -> "FDIV.S"
  | FENCE -> "FENCE"
  | FENCE_I -> "FENCE.I"
  | FENCE_TSO -> "FENCE.TSO"
  | FEQ_D -> "FEQ.D"
  | FEQ_Q -> "FEQ.Q"
  | FEQ_S -> "FEQ.S"
  | FLD -> "FLD"
  | FLE_D -> "FLE.D"
  | FLE_Q -> "FLE.Q"
  | FLE_S -> "FLE.S"
  | FLQ -> "FLQ"
  | FLT_D -> "FLT.D"
  | FLT_Q -> "FLT.Q"
  | FLT_S -> "FLT.S"
  | FLW -> "FLW"
  | FMADD_D -> "FMADD.D"
  | FMADD_Q -> "FMADD.Q"
  | FMADD_S -> "FMADD.S"
  | FMAX_D -> "FMAX.D"
  | FMAX_Q -> "FMAX.Q"
  | FMAX_S -> "FMAX.S"
  | FMIN_D -> "FMIN.D"
  | FMIN_Q -> "FMIN.Q"
  | FMIN_S -> "FMIN.S"
  | FMSUB_D -> "FMSUB.D"
  | FMSUB_Q -> "FMSUB.Q"
  | FMSUB_S -> "FMSUB.S"
  | FMUL_D -> "FMUL.D"
  | FMUL_Q -> "FMUL.Q"
  | FMUL_S -> "FMUL.S"
  | FMV_D_X -> "FMV.D.X"
  | FMV_W_X -> "FMV.W.X"
  | FMV_X_D -> "FMV.X.D"
  | FMV_X_W -> "FMV.X.W"
  | FNMADD_D -> "FNMADD.D"
  | FNMADD_Q -> "FNMADD.Q"
  | FNMADD_S -> "FNMADD.S"
  | FNMSUB_D -> "FNMSUB.D"
  | FNMSUB_Q -> "FNMSUB.Q"
  | FNMSUB_S -> "FNMSUB.S"
  | FSD -> "FSD"
  | FSGNJN_D -> "FSGNJN.D"
  | FSGNJN_Q -> "FSGNJN.Q"
  | FSGNJN_S -> "FSGNJN.S"
  | FSGNJX_D -> "FSGNJX.D"
  | FSGNJX_Q -> "FSGNJX.Q"
  | FSGNJX_S -> "FSGNJX.S"
  | FSGNJ_D -> "FSGNJ.D"
  | FSGNJ_Q -> "FSGNQ.Q"
  | FSGNJ_S -> "FSGNJ.S"
  | FSQ -> "FSQ"
  | FSQRT_D -> "FSQRT.D"
  | FSQRT_Q -> "FSQRT.Q"
  | FSQRT_S -> "FSQRT.S"
  | FSUB_D -> "FSUB.D"
  | FSUB_Q -> "FSUB.Q"
  | FSUB_S -> "FSUB.S"
  | FSW -> "FSW"
  | HFENCE_BVMA -> "HFENCE.BVMA"
  | HFENCE_GVMA -> "HFENCE.GVMA"
  | J -> "J"
  | JAL -> "JAL"
  | JALR -> "JALR"
  | JR -> "JR"
  | LB -> "LB"
  | LBU -> "LBU"
  | LD -> "LD"
  | LH -> "LH"
  | LHU -> "LHU"
  | LR_D -> "LR.D"
  | LR_D_AQ -> "LR.D.AQ"
  | LR_D_AQ_RL -> "LR.D.AQ.RL"
  | LR_D_RL -> "LR.D.RL"
  | LR_W -> "LR.W"
  | LR_W_AQ -> "LR.W.AQ"
  | LR_W_AQ_RL -> "LR.W.AQ.RL"
  | LR_W_RL -> "LR.W.RL"
  | LUI -> "LUI"
  | LW -> "LW"
  | LWU -> "LWU"
  | MRET -> "MRET"
  | MUL -> "MUL"
  | MULH -> "MULH"
  | MULHSU -> "MULHSU"
  | MULHU -> "MULHU"
  | MULW -> "MULW"
  | OR -> "OR"
  | ORI -> "ORI"
  | REM -> "REM"
  | REMU -> "REMU"
  | REMUW -> "REMUW"
  | REMW -> "REMW"
  | SB -> "SB"
  | SC_D -> "SC.D"
  | SC_D_AQ -> "SC.D.AQ"
  | SC_D_AQ_RL -> "SC.D.AQ.RL"
  | SC_D_RL -> "SC.D.RL"
  | SC_W -> "SC.W"
  | SC_W_AQ -> "SC.W.AQ"
  | SC_W_AQ_RL -> "SC.W.AQ.RL"
  | SC_W_RL -> "SC.W.RL"
  | SD -> "SD"
  | SFENCE_VMA -> "SFENCE.VMA"
  | SH -> "SH"
  | SLL -> "SLL"
  | SLLI -> "SLLI"
  | SLLIW -> "SLLIW"
  | SLLW -> "SLLW"
  | SLT -> "SLT"
  | SLTI -> "SLTI"
  | SLTIU -> "SLTIU"
  | SLTU -> "SLTU"
  | SRA -> "SRA"
  | SRAI -> "SRAI"
  | SRAIW -> "SRAIW"
  | SRAW -> "SRAW"
  | SRET -> "SRET"
  | SRL -> "SRL"
  | SRLI -> "SRLI"
  | SRLIW -> "SRLIW"
  | SRLW -> "SRLW"
  | SUB -> "SUB"
  | SUBW -> "SUBW"
  | SW -> "SW"
  | URET -> "URET"
  | WFI -> "WFI"
  | XOR -> "XOR"
  | XORI -> "XORI"

let of_string = function
  | "ADD" -> Some ADD
  | "ADDI" -> Some ADDI
  | "ADDIW" -> Some ADDIW
  | "ADDW" -> Some ADDW
  | "AMOADD.D" -> Some AMOADD_D
  | "AMOADD.D.AQ" -> Some AMOADD_D_AQ
  | "AMOADD.D.AQ.RL" -> Some AMOADD_D_AQ_RL
  | "AMOADD.D.RL" -> Some AMOADD_D_RL
  | "AMOADD.W" -> Some AMOADD_W
  | "AMOADD.W.AQ" -> Some AMOADD_W_AQ
  | "AMOADD.W.AQ.RL" -> Some AMOADD_W_AQ_RL
  | "AMOADD.W.RL" -> Some AMOADD_W_RL
  | "AMOAND.D" -> Some AMOAND_D
  | "AMOAND.D.AQ" -> Some AMOAND_D_AQ
  | "AMOAND.D.AQ.RL" -> Some AMOAND_D_AQ_RL
  | "AMOAND.D.RL" -> Some AMOAND_D_RL
  | "AMOAND.W" -> Some AMOAND_W
  | "AMOAND.W.AQ" -> Some AMOAND_W_AQ
  | "AMOAND.W.AQ.RL" -> Some AMOAND_W_AQ_RL
  | "AMOAND.W.RL" -> Some AMOAND_W_RL
  | "AMOMAXU.D" -> Some AMOMAXU_D
  | "AMOMAXU.D.AQ" -> Some AMOMAXU_D_AQ
  | "AMOMAXU.D.AQ.RL" -> Some AMOMAXU_D_AQ_RL
  | "AMOMAXU.D.RL" -> Some AMOMAXU_D_RL
  | "AMOMAXU.W" -> Some AMOMAXU_W
  | "AMOMAXU.W.AQ" -> Some AMOMAXU_W_AQ
  | "AMOMAXU.W.AQ.RL" -> Some AMOMAXU_W_AQ_RL
  | "AMOMAXU.W.RL" -> Some AMOMAXU_W_RL
  | "AMOMAX.D" -> Some AMOMAX_D
  | "AMOMAX.D.AQ" -> Some AMOMAX_D_AQ
  | "AMOMAX.D.AQ.RL" -> Some AMOMAX_D_AQ_RL
  | "AMOMAX.D.RL" -> Some AMOMAX_D_RL
  | "AMOMAX.W" -> Some AMOMAX_W
  | "AMOMAX.W.AQ" -> Some AMOMAX_W_AQ
  | "AMOMAX.W.AQ.RL" -> Some AMOMAX_W_AQ_RL
  | "AMOMAX.W.RL" -> Some AMOMAX_W_RL
  | "AMOMINU.D" -> Some AMOMINU_D
  | "AMOMINU.D.AQ" -> Some AMOMINU_D_AQ
  | "AMOMINU.D.AQ.RL" -> Some AMOMINU_D_AQ_RL
  | "AMOMINU.D.RL" -> Some AMOMINU_D_RL
  | "AMOMINU.W" -> Some AMOMINU_W
  | "AMOMINU.W.AQ" -> Some AMOMINU_W_AQ
  | "AMOMINU.W.AQ.RL" -> Some AMOMINU_W_AQ_RL
  | "AMOMINU.W.RL" -> Some AMOMINU_W_RL
  | "AMOMIN.D" -> Some AMOMIN_D
  | "AMOMIN.D.AQ" -> Some AMOMIN_D_AQ
  | "AMOMIN.D.AQ.RL" -> Some AMOMIN_D_AQ_RL
  | "AMOMIN.D.RL" -> Some AMOMIN_D_RL
  | "AMOMIN.W" -> Some AMOMIN_W
  | "AMOMIN.W.AQ" -> Some AMOMIN_W_AQ
  | "AMOMIN.W.AQ.RL" -> Some AMOMIN_W_AQ_RL
  | "AMOMIN.W.RL" -> Some AMOMIN_W_RL
  | "AMOOR.D" -> Some AMOOR_D
  | "AMOOR.D.AQ" -> Some AMOOR_D_AQ
  | "AMOOR.D.AQ.RL" -> Some AMOOR_D_AQ_RL
  | "AMOOR.D.RL" -> Some AMOOR_D_RL
  | "AMOOR.W" -> Some AMOOR_W
  | "AMOOR.W.AQ" -> Some AMOOR_W_AQ
  | "AMOOR.W.AQ.RL" -> Some AMOOR_W_AQ_RL
  | "AMOOR.W.RL" -> Some AMOOR_W_RL
  | "AMOSWAP.D" -> Some AMOSWAP_D
  | "AMOSWAP.D.AQ" -> Some AMOSWAP_D_AQ
  | "AMOSWAP.D.AQ.RL" -> Some AMOSWAP_D_AQ_RL
  | "AMOSWAP.D.RL" -> Some AMOSWAP_D_RL
  | "AMOSWAP.W" -> Some AMOSWAP_W
  | "AMOSWAP.W.AQ" -> Some AMOSWAP_W_AQ
  | "AMOSWAP.W.AQ.RL" -> Some AMOSWAP_W_AQ_RL
  | "AMOSWAP.W.RL" -> Some AMOSWAP_W_RL
  | "AMOXOR.D" -> Some AMOXOR_D
  | "AMOXOR.D.AQ" -> Some AMOXOR_D_AQ
  | "AMOXOR.D.AQ.RL" -> Some AMOXOR_D_AQ_RL
  | "AMOXOR.D.RL" -> Some AMOXOR_D_RL
  | "AMOXOR.W" -> Some AMOXOR_W
  | "AMOXOR.W.AQ" -> Some AMOXOR_W_AQ
  | "AMOXOR.W.AQ.RL" -> Some AMOXOR_W_AQ_RL
  | "AMOXOR.W.RL" -> Some AMOXOR_W_RL
  | "AND" -> Some AND
  | "ANDI" -> Some ANDI
  | "AUIPC" -> Some AUIPC
  | "BEQ" -> Some BEQ
  | "BGE" -> Some BGE
  | "BGEU" -> Some BGEU
  | "BLT" -> Some BLT
  | "BLTU" -> Some BLTU
  | "BNE" -> Some BNE
  | "CSRRC" -> Some CSRRC
  | "CSRRCI" -> Some CSRRCI
  | "CSRRS" -> Some CSRRS
  | "CSRRSI" -> Some CSRRSI
  | "CSRRW" -> Some CSRRW
  | "CSRRWI" -> Some CSRRWI
  | "C.ADD" -> Some C_ADD
  | "C.ADDI" -> Some C_ADDI
  | "C.ADDI16SP" -> Some C_ADDI16SP
  | "C.ADDI4SPN" -> Some C_ADDI4SPN
  | "C.ADDIW" -> Some C_ADDIW
  | "C.ADDW" -> Some C_ADDW
  | "C.AND" -> Some C_AND
  | "C.ANDI" -> Some C_ANDI
  | "C.BEQZ" -> Some C_BEQZ
  | "C.BNEZ" -> Some C_BNEZ
  | "C.EBREAK" -> Some C_EBREAK
  | "C.FLD" -> Some C_FLD
  | "C.FLDSP" -> Some C_FLDSP
  | "C.FLW" -> Some C_FLW
  | "C.FLWSP" -> Some C_FLWSP
  | "C.FSD" -> Some C_FSD
  | "C.FSDSP" -> Some C_FSDSP
  | "C.FSW" -> Some C_FSW
  | "C.FSWSP" -> Some C_FSWSP
  | "C.J" -> Some C_J
  | "C.JAL" -> Some C_JAL
  | "C.JALR" -> Some C_JALR
  | "C.JR" -> Some C_JR
  | "C.LD" -> Some C_LD
  | "C.LDSP" -> Some C_LDSP
  | "C.LI" -> Some C_LI
  | "C.LUI" -> Some C_LUI
  | "C.LW" -> Some C_LW
  | "C.LWSP" -> Some C_LWSP
  | "C.MV" -> Some C_MV
  | "C.NOP" -> Some C_NOP
  | "C.OR" -> Some C_OR
  | "C.SD" -> Some C_SD
  | "C.SDSP" -> Some C_SDSP
  | "C.SLLI" -> Some C_SLLI
  | "C.SRAI" -> Some C_SRAI
  | "C.SRLI" -> Some C_SRLI
  | "C.SUB" -> Some C_SUB
  | "C.SUBW" -> Some C_SUBW
  | "C.SW" -> Some C_SW
  | "C.SWSP" -> Some C_SWSP
  | "C.XOR" -> Some C_XOR
  | "DIV" -> Some DIV
  | "DIVU" -> Some DIVU
  | "DIVUW" -> Some DIVUW
  | "DIVW" -> Some DIVW
  | "EBREAK" -> Some EBREAK
  | "ECALL" -> Some ECALL
  | "FADD.D" -> Some FADD_D
  | "FADD.Q" -> Some FADD_Q
  | "FADD.S" -> Some FADD_S
  | "FCLASS.D" -> Some FCLASS_D
  | "FCLASS.Q" -> Some FCLASS_Q
  | "FCLASS.S" -> Some FCLASS_S
  | "FCVT.D.L" -> Some FCVT_D_L
  | "FCVT.D.LU" -> Some FCVT_D_LU
  | "FCVT.D.Q" -> Some FCVT_D_Q
  | "FCVT.D.S" -> Some FCVT_D_S
  | "FCVT.D.W" -> Some FCVT_D_W
  | "FCVT.D.WU" -> Some FCVT_D_WU
  | "FCVT.LU.D" -> Some FCVT_LU_D
  | "FCVT.LU.Q" -> Some FCVT_LU_Q
  | "FCVT.LU.S" -> Some FCVT_LU_S
  | "FCVT.L.D" -> Some FCVT_L_D
  | "FCVT.L.Q" -> Some FCVT_L_Q
  | "FCVT.L.S" -> Some FCVT_L_S
  | "FCVT.Q.D" -> Some FCVT_Q_D
  | "FCVT.Q.LU" -> Some FCVT_Q_LU
  | "FCVT.Q.L" -> Some FCVT_Q_L
  | "FCVT.Q.S" -> Some FCVT_Q_S
  | "FCVT.Q.WU" -> Some FCVT_Q_WU
  | "FCVT.Q.W" -> Some FCVT_Q_W
  | "FCVT.S.D" -> Some FCVT_S_D
  | "FCVT.S.L" -> Some FCVT_S_L
  | "FCVT_S_LU" -> Some FCVT_S_LU
  | "FCVT.S.Q" -> Some FCVT_S_Q
  | "FCVT.S.W" -> Some FCVT_S_W
  | "FCVT.S.WU" -> Some FCVT_S_WU
  | "FCVT.WU.D" -> Some FCVT_WU_D
  | "FCVT.WU.Q" -> Some FCVT_WU_Q
  | "FCVT.WU.S" -> Some FCVT_WU_S
  | "FCVT.W.D" -> Some FCVT_W_D
  | "FCVT.W.Q" -> Some FCVT_W_Q
  | "FCVT.W.S" -> Some FCVT_W_S
  | "FDIV.D" -> Some FDIV_D
  | "FDIV.Q" -> Some FDIV_Q
  | "FDIV.S" -> Some FDIV_S
  | "FENCE" -> Some FENCE
  | "FENCE.I" -> Some FENCE_I
  | "FENCE.TSO" -> Some FENCE_TSO
  | "FEQ.D" -> Some FEQ_D
  | "FEQ.Q" -> Some FEQ_Q
  | "FEQ.S" -> Some FEQ_S
  | "FLD" -> Some FLD
  | "FLE.D" -> Some FLE_D
  | "FLE.Q" -> Some FLE_Q
  | "FLE.S" -> Some FLE_S
  | "FLQ" -> Some FLQ
  | "FLT.D" -> Some FLT_D
  | "FLT.Q" -> Some FLT_Q
  | "FLT.S" -> Some FLT_S
  | "FLW" -> Some FLW
  | "FMADD.D" -> Some FMADD_D
  | "FMADD.Q" -> Some FMADD_Q
  | "FMADD.S" -> Some FMADD_S
  | "FMAX.D" -> Some FMAX_D
  | "FMAX.Q" -> Some FMAX_Q
  | "FMAX.S" -> Some FMAX_S
  | "FMIN.D" -> Some FMIN_D
  | "FMIN.Q" -> Some FMIN_Q
  | "FMIN.S" -> Some FMIN_S
  | "FMSUB.D" -> Some FMSUB_D
  | "FMSUB.Q" -> Some FMSUB_Q
  | "FMSUB.S" -> Some FMSUB_S
  | "FMUL.D" -> Some FMUL_D
  | "FMUL.Q" -> Some FMUL_Q
  | "FMUL.S" -> Some FMUL_S
  | "FMV.D.X" -> Some FMV_D_X
  | "FMV.W.X" -> Some FMV_W_X
  | "FMV.X.D" -> Some FMV_X_D
  | "FMV.X.W" -> Some FMV_X_W
  | "FNMADD.D" -> Some FNMADD_D
  | "FNMADD.Q" -> Some FNMADD_Q
  | "FNMADD.S" -> Some FNMADD_S
  | "FNMSUB.D" -> Some FNMSUB_D
  | "FNMSUB.Q" -> Some FNMSUB_Q
  | "FNMSUB.S" -> Some FNMSUB_S
  | "FSD" -> Some FSD
  | "FSGNJN.D" -> Some FSGNJN_D
  | "FSGNJN.Q" -> Some FSGNJN_Q
  | "FSGNJN.S" -> Some FSGNJN_S
  | "FSGNJX.D" -> Some FSGNJX_D
  | "FSGNJX.Q" -> Some FSGNJX_Q
  | "FSGNJX.S" -> Some FSGNJX_S
  | "FSGNJ.D" -> Some FSGNJ_D
  | "FSGNJ.Q" -> Some FSGNJ_Q
  | "FSGNJ.S" -> Some FSGNJ_S
  | "FSQ" -> Some FSQ
  | "FSQRT.D" -> Some FSQRT_D
  | "FSQRT.Q" -> Some FSQRT_Q
  | "FSQRT.S" -> Some FSQRT_S
  | "FSUB.D" -> Some FSUB_D
  | "FSUB.Q" -> Some FSUB_Q
  | "FSUB.S" -> Some FSUB_S
  | "FSW" -> Some FSW
  | "HFENCE.BVMA" -> Some HFENCE_BVMA
  | "HFENCE.GVMA" -> Some HFENCE_GVMA
  | "J" -> Some J
  | "JAL" -> Some JAL
  | "JALR" -> Some JALR
  | "JR" -> Some JR
  | "LB" -> Some LB
  | "LBU" -> Some LBU
  | "LD" -> Some LD
  | "LH" -> Some LH
  | "LHU" -> Some LHU
  | "LR.D" -> Some LR_D
  | "LR.D.AQ" -> Some LR_D_AQ
  | "LR.D.AQ.RL" -> Some LR_D_AQ_RL
  | "LR.D.RL" -> Some LR_D_RL
  | "LR.W" -> Some LR_W
  | "LR.W.AQ" -> Some LR_W_AQ
  | "LR.W.AQ.RL" -> Some LR_W_AQ_RL
  | "LR.W.RL" -> Some LR_W_RL
  | "LUI" -> Some LUI
  | "LW" -> Some LW
  | "LWU" -> Some LWU
  | "MRET" -> Some MRET
  | "MUL" -> Some MUL
  | "MULH" -> Some MULH
  | "MULHSU" -> Some MULHSU
  | "MULHU" -> Some MULHU
  | "MULW" -> Some MULW
  | "OR" -> Some OR
  | "ORI" -> Some ORI
  | "REM" -> Some REM
  | "REMU" -> Some REMU
  | "REMUW" -> Some REMUW
  | "REMW" -> Some REMW
  | "SB" -> Some SB
  | "SC.D" -> Some SC_D
  | "SC.D.AQ" -> Some SC_D_AQ
  | "SC.D.AQ.RL" -> Some SC_D_AQ_RL
  | "SC.D.RL" -> Some SC_D_RL
  | "SC.W" -> Some SC_W
  | "SC.W.AQ" -> Some SC_W_AQ
  | "SC.W.AQ.RL" -> Some SC_W_AQ_RL
  | "SC.W.RL" -> Some SC_W_RL
  | "SD" -> Some SD
  | "SFENCE.VMA" -> Some SFENCE_VMA
  | "SH" -> Some SH
  | "SLL" -> Some SLL
  | "SLLI" -> Some SLLI
  | "SLLIW" -> Some SLLIW
  | "SLLW" -> Some SLLW
  | "SLT" -> Some SLT
  | "SLTI" -> Some SLTI
  | "SLTIU" -> Some SLTIU
  | "SLTU" -> Some SLTU
  | "SRA" -> Some SRA
  | "SRAI" -> Some SRAI
  | "SRAIW" -> Some SRAIW
  | "SRAW" -> Some SRAW
  | "SRET" -> Some SRET
  | "SRL" -> Some SRL
  | "SRLI" -> Some SRLI
  | "SRLIW" -> Some SRLIW
  | "SRLW" -> Some SRLW
  | "SUB" -> Some SUB
  | "SUBW" -> Some SUBW
  | "SW" -> Some SW
  | "URET" -> Some URET
  | "WFI" -> Some WFI
  | "XOR" -> Some XOR
  | "XORI" -> Some XORI
  | _ -> None
