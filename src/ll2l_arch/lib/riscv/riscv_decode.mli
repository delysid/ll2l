open Core_kernel
open Ll2l_std

type error = [`OOB | `Invalid | `BadOperand | `Unimpl]

val string_of_error : error -> string

exception Riscv_decode_error of error

val decode : Riscv.Mode.t -> bytes -> (Riscv.Instruction.t, error) Result.t

val decode_exn : Riscv.Mode.t -> bytes -> Riscv.Instruction.t
