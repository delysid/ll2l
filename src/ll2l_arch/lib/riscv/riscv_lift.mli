open Core_kernel
open Ll2l_ir
open Ll2l_std

type error = [`BadInstr | `BadOperand of int]

val string_of_error : error -> string

exception Riscv_lift_error of error

val lift :
  Riscv.Instruction.t -> Addr.t -> Il.Context.t -> (Il.t, error) Result.t

val lift_exn : Riscv.Instruction.t -> Addr.t -> Il.Context.t -> Il.t
