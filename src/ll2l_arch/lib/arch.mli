open Base
open Ll2l_ir
open Ll2l_std

type (_, _, _, _) t =
  | X86 :
      X86.Mode.t
      -> (X86.Instruction.t, X86_decode.error, X86_lift.error, unit) t
  | MIPS :
      Mips.Mode.t * Endian.t
      -> (Mips.Instruction.t, Mips_decode.error, Mips_lift.error, unit) t
  | ARM :
      Arm.Mode.t * Endian.t
      -> ( Arm.Instruction.t
         , Arm_decode.error
         , Arm_lift.error
         , Arm_decode.Context.t )
         t
  | RISCV :
      Riscv.Mode.t
      -> (Riscv.Instruction.t, Riscv_decode.error, Riscv_lift.error, unit) t

(* existential type for arch *)
type et = T : ('a, 'b, 'c, 'd) t -> et [@@unboxed]

(* existential type for arch + instruction *)
type ei = I : ('a, 'b, 'c, 'd) t * 'a -> ei

(* existential type for arch + decode context *)
type ec = C : ('a, 'b, 'c, 'd) t * 'd -> ec

(* existential type for arch + instruction + decode context *)
type eic = IC : ('a, 'b, 'c, 'd) t * 'a * 'd -> eic

val of_string : string -> et

val to_string : ('a, 'b, 'c, 'd) t -> string

val compare : ('a, 'b, 'c, 'd) t -> ('e, 'f, 'g, 'h) t -> int

val equal : ('a, 'b, 'c, 'd) t -> ('e, 'f, 'g, 'h) t -> bool

val word_size_of : ('a, 'b, 'c, 'd) t -> int

val endian_of : ('a, 'b, 'c, 'd) t -> Endian.t

val create_decode_ctx : ('a, 'b, 'c, 'd) t -> 'd

val clone_decode_ctx : ('a, 'b, 'c, 'd) t -> 'd -> 'd

val switch_decode_ctx : ('a, 'b, 'c, 'd) t -> ('e, 'f, 'g, 'h) t -> 'd -> 'h

val addr_of_arch : et -> Addr.t -> Addr.t * et

val decode : ('a, 'b, 'c, 'd) t -> 'd -> bytes -> ('a, 'b) Result.t

val decode_exn : ('a, 'b, 'c, 'd) t -> 'd -> bytes -> 'a

val lift :
     ?delay:'a list
  -> ('a, 'b, 'c, 'd) t
  -> 'a
  -> Addr.t
  -> Il.Context.t
  -> (Il.t, 'c) Result.t

val lift_exn :
     ?delay:'a list
  -> ('a, 'b, 'c, 'd) t
  -> 'a
  -> Addr.t
  -> Il.Context.t
  -> Il.t

val instr_length : ('a, 'b, 'c, 'd) t -> 'a -> int

val min_instr_length : ('a, 'b, 'c, 'd) t -> int

val max_instr_length : ('a, 'b, 'c, 'd) t -> int

val instr_end_addr : ('a, 'b, 'c, 'd) t -> 'a -> addr:Addr.t -> Addr.t

val no_fallthrough_instr : ('a, 'b, 'c, 'd) t -> 'a -> bool

val may_fallthrough_instr : ('a, 'b, 'c, 'd) t -> 'a -> bool

val must_fallthrough_instr : ('a, 'b, 'c, 'd) t -> 'a -> bool

val is_terminator_instr : ('a, 'b, 'c, 'd) t -> 'a -> bool

val delay_slot_size : ('a, 'b, 'c, 'd) t -> 'a -> int

val is_conditional_branch : ('a, 'b, 'c, 'd) t -> 'a -> bool

val has_conditional_delay : ('a, 'b, 'c, 'd) t -> 'a -> bool

val branch_address_of_instr :
  ('a, 'b, 'c, 'd) t -> 'a -> addr:Addr.t -> Addr.t option

val string_of_instr : ('a, 'b, 'c, 'd) t -> 'a -> addr:Addr.t -> string

val string_of_instr_full : ('a, 'b, 'c, 'd) t -> 'a -> addr:Addr.t -> string

val string_of_decode_err : ('a, 'b, 'c, 'd) t -> 'b -> string

val string_of_lift_err : ('a, 'b, 'c, 'd) t -> 'c -> string

val is_stack_pointer_var : ('a, 'b, 'c, 'd) t -> Il.var -> bool
