open Core_kernel
open Ll2l_std
module Register = Arm_register
module Mnemonic = Arm_mnemonic
module Condition = Arm_condition
module Operand = Arm_operand

module Mode = struct
  type t = Armv7 | Thumbv7 | Armv8 [@@deriving equal]

  let word_size = 32

  let of_string = function
    | "armv7" -> Armv7
    | "thumb7" -> Thumbv7
    | "armv8" -> Armv8
    | _ -> invalid_arg "bad ARM mode"

  let to_string = function
    | Armv7 -> "armv7"
    | Thumbv7 -> "thumbv7"
    | Armv8 -> "armv8"

  let is_thumb = function
    | Thumbv7 -> true
    | _ -> false

  let min_instr_length = function
    | Thumbv7 -> 2
    | _ -> 4

  let pc mode ~addr =
    if is_thumb mode then Addr.(addr + int 4) else Addr.(addr + int 8)
end

module Qualifier = struct
  type t = W | N [@@deriving equal]

  let to_string = function
    | W -> "W"
    | N -> "N"
end

module Simd_data = struct
  type kind =
    | Any8
    | Any16
    | Any32
    | Any64
    | F16
    | F32
    | F64
    | I8
    | I16
    | I32
    | I64
    | P8
    | S8
    | S16
    | S32
    | S64
    | U8
    | U16
    | U32
    | U64
  [@@deriving equal]

  type t = One of kind | Two of kind * kind [@@deriving equal]

  let string_of_kind = function
    | Any8 -> "8"
    | Any16 -> "16"
    | Any32 -> "32"
    | Any64 -> "64"
    | F16 -> "F16"
    | F32 -> "F32"
    | F64 -> "F64"
    | I8 -> "I8"
    | I16 -> "I16"
    | I32 -> "I32"
    | I64 -> "I64"
    | P8 -> "P8"
    | S8 -> "S8"
    | S16 -> "S16"
    | S32 -> "S32"
    | S64 -> "S64"
    | U8 -> "U8"
    | U16 -> "U16"
    | U32 -> "U32"
    | U64 -> "U64"

  let to_string = function
    | One k -> string_of_kind k
    | Two (k1, k2) ->
        Printf.sprintf "%s.%s" (string_of_kind k1) (string_of_kind k2)
end

module Instruction = struct
  type t =
    { mode: Mode.t
    ; endian: Endian.t
    ; mnemonic: Mnemonic.t
    ; bytes: bytes
    ; cond: Condition.t option
    ; write_back: bool option
    ; qualifier: Qualifier.t option
    ; simd_data: Simd_data.t option
    ; operands: Operand.t array
    ; it_state: int
    ; cflag: bool option }

  let max_length = 4

  let length instr = Bytes.length instr.bytes [@@inline]

  let end_addr instr ~addr = Addr.(addr + int (length instr)) [@@inline]

  let opcode instr =
    let b1 = Char.to_int @@ Bytes.get instr.bytes 0 in
    let b2 = Char.to_int @@ Bytes.get instr.bytes 1 in
    if Mode.is_thumb instr.mode then
      let n =
        match instr.endian with
        | `LE -> b1 lor (b2 lsl 8) land 0xFFFF
        | `BE -> (b1 lsl 8) lor b2 land 0xFFFF
      in
      if length instr > 4 then
        let b3 = Char.to_int @@ Bytes.get instr.bytes 2 in
        let b4 = Char.to_int @@ Bytes.get instr.bytes 3 in
        let n' =
          match instr.endian with
          | `LE -> b3 lor (b4 lsl 8) land 0xFFFF
          | `BE -> (b3 lsl 8) lor b4 land 0xFFFF
        in
        (n' lsl 16) lor n land 0xFFFFFFFF
      else n
    else
      let b3 = Char.to_int @@ Bytes.get instr.bytes 2 in
      let b4 = Char.to_int @@ Bytes.get instr.bytes 3 in
      let n =
        match instr.endian with
        | `BE -> (b1 lsl 24) lor (b2 lsl 16) lor (b3 lsl 8) lor b4
        | `LE -> b1 lor (b2 lsl 8) lor (b3 lsl 16) lor (b4 lsl 24)
      in
      n land 0xFFFFFFFF

  let is_conditional instr =
    Option.(
      value ~default:false
        (instr.cond >>| fun cond -> Condition.is_unconditional cond |> not))

  let is_conditional_branch instr =
    let module M = Mnemonic in
    if M.is_conditional_branch instr.mnemonic then true
    else M.is_branch instr.mnemonic && not (is_conditional instr)

  let no_fallthrough instr =
    let module M = Mnemonic in
    let is_branch = M.is_unconditional_branch instr.mnemonic in
    if is_branch && not (is_conditional instr) then true
    else
      (* XXX: this list may be incomplete *)
      match instr.mnemonic with
      | M.ERET | M.RFEDA | M.RFEDB | M.RFEIA | M.RFEIB -> true
      | _ -> false

  let may_fallthrough instr =
    let module M = Mnemonic in
    let is_link_branch = M.is_link_branch instr.mnemonic in
    let is_uncond_branch = M.is_unconditional_branch instr.mnemonic in
    let is_cond_branch = M.is_conditional_branch instr.mnemonic in
    if
      is_link_branch
      || (is_uncond_branch && is_conditional instr)
      || is_cond_branch
    then true
    else
      (* XXX: this list may be incomplete *)
      match instr.mnemonic with
      | M.BKPT | M.HVC | M.SMC | M.SVC -> true
      | _ -> false

  let must_fallthrough instr =
    not (no_fallthrough instr || may_fallthrough instr)
    [@@inline]

  let is_terminator instr = not (must_fallthrough instr) [@@inline]

  let branch_address instr ~addr =
    let module M = Mnemonic in
    let open Option.Let_syntax in
    let label_op = function
      | Operand.(Mem Memory.(Label lbl)) -> Some lbl
      | _ -> None
    in
    let%map align_pc, lbl =
      match instr.mnemonic with
      | M.B ->
          let%map lbl = label_op instr.operands.(0) in
          (false, lbl)
      | M.BL ->
          let%map lbl = label_op instr.operands.(0) in
          (not (Mode.is_thumb instr.mode), lbl)
      | M.BLX ->
          let%map lbl = label_op instr.operands.(0) in
          (Int64.(lbl land one <> one), lbl)
      | M.CBNZ | M.CBZ ->
          let%map lbl = label_op instr.operands.(1) in
          (false, lbl)
      | _ -> None
    in
    let pc = Mode.pc instr.mode ~addr in
    let addr = if not align_pc then pc else Addr.(pc / int 4 * int 4) in
    Addr.((addr + int64 lbl) land int64 0xFFFFFFFEL)

  let to_string instr ~addr =
    let m_str = Mnemonic.to_string instr.mnemonic in
    let cond_str =
      match instr.cond with
      | Some cond -> Condition.to_string cond
      | None -> ""
    in
    let qual_str =
      match instr.qualifier with
      | Some qual -> "." ^ Qualifier.to_string qual
      | None -> ""
    in
    let simd_str =
      match instr.simd_data with
      | Some simd -> "." ^ Simd_data.to_string simd
      | None -> ""
    in
    let op_str =
      let module M = Mnemonic in
      let thumb = Mode.is_thumb instr.mode in
      let is_rfe_or_srs =
        match instr.mnemonic with
        | M.RFEDA
         |M.RFEDB
         |M.RFEIA
         |M.RFEIB
         |M.SRS
         |M.SRSDA
         |M.SRSDB
         |M.SRSIA
         |M.SRSIB -> true
        | _ -> false
      in
      let has_reg_list =
        match instr.operands with
        | [|_; Operand.Reg_list _|] -> true
        | _ -> false
      in
      let align_pc =
        match instr.mnemonic with
        | M.ADR -> true
        | M.BL -> not thumb
        | M.BLX -> (
          match instr.operands.(0) with
          | Operand.(Mem Memory.(Label l)) -> Int64.(l land one = zero)
          | _ -> not thumb )
        | _ -> false
      in
      let rel = (addr, Mode.pc instr.mode ~addr, align_pc) in
      Array.to_list instr.operands
      |> List.map ~f:(fun op ->
             let write_back =
               match instr.write_back with
               | Some true ->
                   let is_reg_list =
                     match op with
                     | Operand.Reg_list _ -> true
                     | _ -> false
                   in
                   (has_reg_list && not is_reg_list) || is_rfe_or_srs
               | _ -> false
             in
             Operand.to_string op ~rel ~write_back)
      |> List.filter ~f:(fun s -> not (String.is_empty s))
      |> String.concat ~sep:", "
    in
    Printf.sprintf "%s%s%s%s%s" m_str cond_str qual_str simd_str
      (if String.is_empty op_str then "" else " " ^ op_str)

  let to_string_full instr ~addr =
    let addr_str = Printf.sprintf "0x%08lX" (Addr.to_int32 addr) in
    let bytes_str =
      Bytes.to_list instr.bytes |> List.map ~f:Char.to_int
      |> List.map ~f:(Printf.sprintf "%02X")
      |> String.concat ~sep:" "
    in
    let bytes_str =
      if Mode.is_thumb instr.mode && length instr = 2 then
        bytes_str ^ "      "
      else bytes_str
    in
    Printf.sprintf "%s:  %s  %s" addr_str bytes_str (to_string instr ~addr)
end
