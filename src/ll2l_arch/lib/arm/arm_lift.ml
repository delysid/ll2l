open Core_kernel
open Ll2l_ir
open Ll2l_std
module C = Arm.Condition
module Q = Arm.Qualifier
module S = Arm.Simd_data
module Mode = Arm.Mode
module M = Arm.Mnemonic
module O = Arm.Operand
module Sh = O.Shift_kind
module Psr = O.Psr_flag
module Oo = O.Option_op
module Oi = O.Iflag
module R = Arm.Register
module I = Arm.Instruction

type error = [`BadInstr | `BadOperand of int]

let string_of_error = function
  | `BadInstr -> "bad instruction"
  | `BadOperand n -> Printf.sprintf "bad operand (%d)" n

exception Arm_lift_error of error

let r0 = Il.reg "R0" 32

let r1 = Il.reg "R1" 32

let r2 = Il.reg "R2" 32

let r3 = Il.reg "R3" 32

let r4 = Il.reg "R4" 32

let r5 = Il.reg "R5" 32

let r6 = Il.reg "R6" 32

let r7 = Il.reg "R7" 32

let r8 = Il.reg "R8" 32

let r9 = Il.reg "R9" 32

let r10 = Il.reg "R10" 32

let r11 = Il.reg "R11" 32

let r12 = Il.reg "R12" 32

let lr = Il.reg "LR" 32

let sp = Il.reg "SP" 32

let pc = Il.reg "PC" 32

let nf = Il.reg "NF" 1

let zf = Il.reg "ZF" 1

let cf = Il.reg "CF" 1

let vf = Il.reg "VF" 1

let qf = Il.reg "QF" 1

let jf = Il.reg "JF" 1

let ef = Il.reg "EF" 1

let af = Il.reg "AF" 1

let iF = Il.reg "IF" 1

let ff = Il.reg "FF" 1

let tf = Il.reg "TF" 1

let ge0 = Il.reg "GE0" 1

let ge1 = Il.reg "GE1" 1

let ge2 = Il.reg "GE2" 1

let ge3 = Il.reg "GE3" 1

let cond_to_expr = function
  | C.EQ -> zf
  | C.NE -> Il.(~~zf)
  | C.CS -> cf
  | C.HS -> cf
  | C.CC -> Il.(~~cf)
  | C.LO -> Il.(~~cf)
  | C.MI -> nf
  | C.PL -> Il.(~~nf)
  | C.VS -> vf
  | C.VC -> Il.(~~vf)
  | C.HI -> Il.(cf & ~~zf)
  | C.LS -> Il.(~~cf || zf)
  | C.GE -> Il.(nf = vf)
  | C.LT -> Il.(nf <> vf)
  | C.GT -> Il.(~~zf & nf = vf)
  | C.LE -> Il.(zf || nf <> vf)
  | C.AL -> Il.bit true
  | C.NV -> Il.bit true
  | C.UN -> Il.bit true

let cond_to_expr_inv = function
  | C.EQ -> Il.(~~zf)
  | C.NE -> zf
  | C.CS -> Il.(~~cf)
  | C.HS -> Il.(~~cf)
  | C.CC -> cf
  | C.LO -> cf
  | C.MI -> Il.(~~nf)
  | C.PL -> nf
  | C.VS -> Il.(~~vf)
  | C.VC -> vf
  | C.HI -> Il.(~~cf || zf)
  | C.LS -> Il.(cf & ~~zf)
  | C.GE -> Il.(nf <> vf)
  | C.LT -> Il.(nf = vf)
  | C.GT -> Il.(zf || nf <> vf)
  | C.LE -> Il.(~~zf & nf = vf)
  | C.AL -> Il.bit false
  | C.NV -> Il.bit false
  | C.UN -> Il.bit false

let make_reg (r : R.t) =
  let open Option.Let_syntax in
  let%map width = R.width_of r in
  (Il.reg (R.to_string r) width, width)

let op_reg (instr : I.t) i =
  match instr.operands.(i) with
  | O.Reg r -> (
    match make_reg r with
    | Some (r, _) -> Ok r
    | None -> Error (`BadOperand i) )
  | _ -> Error (`BadOperand i)

let op_reglist (instr : I.t) i =
  match instr.operands.(i) with
  | O.Reg_list rl ->
      List.fold_result (List.rev rl) ~init:[] ~f:(fun acc r ->
          match make_reg r with
          | Some (r, _) -> Ok (r :: acc)
          | None -> Error (`BadOperand i) )
  | _ -> Error (`BadOperand i)

let op_reg_or_reglist (instr : I.t) i =
  match op_reg instr i with
  | Error _ -> op_reglist instr i
  | Ok op -> Ok [op]

let op_imm (instr : I.t) i sz =
  match instr.operands.(i) with
  | O.Imm imm -> Ok Il.(num64 imm sz)
  | _ -> Error (`BadOperand i)

let op_reg_or_imm (instr : I.t) i sz =
  match op_reg instr i with
  | Error _ -> op_imm instr i sz
  | op -> op

let shift_c new_label new_tmp sz sh ~lhs ~rhs =
  let nth_bit n e = Il.(lo 1 (e >> n)) in
  match sh with
  | Sh.LSL ->
      let l1 = new_label () in
      let l2 = new_label () in
      let l3 = new_label () in
      let t1 = new_tmp 1 in
      let res = Il.(lhs << rhs) in
      let stmts =
        Il.
          [ goto_if (rhs <> num 0 sz) l1 l2
          ; ( @ ) l1
          ; t1 := nth_bit (num sz sz - rhs) lhs
          ; goto l3
          ; ( @ ) l2
          ; t1 := cf
          ; goto l3
          ; ( @ ) l3 ]
      in
      (res, t1, stmts)
  | Sh.LSR ->
      let res = Il.(lhs >> rhs) in
      let carry = nth_bit Il.(rhs - num 1 sz) lhs in
      (res, carry, [])
  | Sh.ASR ->
      let res = Il.(lhs >>> rhs) in
      let carry = nth_bit Il.(rhs - num 1 sz) lhs in
      (res, carry, [])
  | Sh.ROR ->
      let res = Il.(lhs >> rhs || lhs << num sz sz - rhs) in
      let carry = nth_bit Il.(rhs - num 1 sz) lhs in
      (res, carry, [])
  | Sh.RRX ->
      let res = Il.(lhs >> num 1 sz || ze sz cf << num Int.(sz - 1) sz) in
      let carry = Il.lo 1 lhs in
      (res, carry, [])

let shift' sz sh ~lhs ~rhs =
  match sh with
  | Sh.LSL -> Il.(lhs << rhs)
  | Sh.LSR -> Il.(lhs >> rhs)
  | Sh.ASR -> Il.(lhs >>> rhs)
  | Sh.ROR -> Il.(lhs >> rhs || lhs << num sz sz - rhs)
  | Sh.RRX -> Il.(lhs >> num 1 sz || ze sz cf << num Int.(sz - 1) sz)

let op_shift_c new_label new_tmp (instr : I.t) i sz ~lhs =
  match instr.operands.(i) with
  | O.Shift (_, 0) -> Ok (lhs, cf, [])
  | O.Shift (sh, n) ->
      let rhs = Il.num n sz in
      Ok (shift_c new_label new_tmp sz sh ~lhs ~rhs)
  | _ -> Error (`BadOperand i)

let op_shift (instr : I.t) i sz ~lhs =
  match instr.operands.(i) with
  | O.Shift (_, 0) -> Ok lhs
  | O.Shift (sh, n) ->
      let rhs = Il.num n sz in
      Ok (shift' sz sh ~lhs ~rhs)
  | _ -> Error (`BadOperand i)

let op_reg_shift_c new_label new_tmp (instr : I.t) i sz ~lhs =
  match instr.operands.(i) with
  | O.Reg_shift (sh, r) -> (
    match make_reg r with
    | Some (rhs, _) -> Ok (shift_c new_label new_tmp sz sh ~lhs ~rhs)
    | None -> Error (`BadOperand i) )
  | _ -> Error (`BadOperand i)

let op_reg_shift (instr : I.t) i sz ~lhs =
  match instr.operands.(i) with
  | O.Reg_shift (sh, r) -> (
    match make_reg r with
    | Some (rhs, _) -> Ok (shift' sz sh ~lhs ~rhs)
    | None -> Error (`BadOperand i) )
  | _ -> Error (`BadOperand i)

let op_shift_or_reg_shift (instr : I.t) i sz ~lhs =
  match op_shift instr i sz ~lhs with
  | Error _ -> op_reg_shift instr i sz ~lhs
  | op -> op

let op_shift_or_reg_shift_c new_label new_tmp (instr : I.t) i sz ~lhs =
  match op_shift_c new_label new_tmp instr i sz ~lhs with
  | Error _ -> op_reg_shift_c new_label new_tmp instr i sz ~lhs
  | op -> op

let mem_offset i sz = function
  | O.Memory.Offset.Imm {reg; neg; imm} -> (
    match make_reg reg with
    | None -> Error (`BadOperand i)
    | Some (r, w) -> (
      match imm with
      | Some imm -> Ok (r, Some (neg, Il.num64 imm w))
      | None -> Ok (r, None) ) )
  | O.Memory.Offset.Reg {reg; neg; reg_off; shift} -> (
    match make_reg reg with
    | None -> Error (`BadOperand i)
    | Some (rb, w) -> (
      match make_reg reg_off with
      | None -> Error (`BadOperand i)
      | Some (ro, _) -> (
        match shift with
        | None | Some (_, 0) -> Ok (rb, Some (neg, ro))
        | Some (sh, n) ->
            let sh = shift' sz sh ~lhs:ro ~rhs:Il.(num n w) in
            Ok (rb, Some (neg, sh)) ) ) )
  (* todo *)
  | O.Memory.Offset.Align _ -> Error (`BadOperand i)

let op_mem_off (instr : I.t) i sz =
  match instr.operands.(i) with
  | O.(Mem Memory.(Off off)) ->
      let open Result.Let_syntax in
      let%map base, disp = mem_offset i sz off in
      let loc =
        match disp with
        | None -> base
        | Some (neg, disp) ->
            if neg then Il.(base - disp) else Il.(base + disp)
      in
      Il.(load instr.endian Int.(sz lsr 3) mu loc)
  | _ -> Error (`BadOperand i)

let op_mem_preidx (instr : I.t) i sz =
  match instr.operands.(i) with
  | O.(Mem Memory.(Preindex off)) ->
      let open Result.Let_syntax in
      let%map base, disp = mem_offset i sz off in
      let loc, stmts =
        match disp with
        | None -> (base, [])
        | Some (neg, disp) ->
            let loc = if neg then Il.(base - disp) else Il.(base + disp) in
            let stmts = Il.[base := loc] in
            (loc, stmts)
      in
      (Il.(load instr.endian Int.(sz lsr 3) mu loc), stmts)
  | _ -> Error (`BadOperand i)

let op_mem_postidx (instr : I.t) i sz =
  match instr.operands.(i) with
  | O.(Mem Memory.(Postindex off)) ->
      let open Result.Let_syntax in
      let%map base, disp = mem_offset i sz off in
      let stmts =
        match disp with
        | None -> []
        | Some (neg, disp) ->
            let loc = if neg then Il.(base - disp) else Il.(base + disp) in
            Il.[base := loc]
      in
      (Il.(load instr.endian Int.(sz lsr 3) mu base), stmts)
  | _ -> Error (`BadOperand i)

let op_mem_label (instr : I.t) i addr =
  match instr.operands.(i) with
  | O.(Mem Memory.(Label lbl)) ->
      let align_pc =
        match instr.mnemonic with
        | M.B | M.CBNZ | M.CBZ -> false
        | M.ADR -> true
        | M.BL -> not (Mode.is_thumb instr.mode)
        | M.BLX -> Int64.(lbl land one <> one)
        | _ -> false
      in
      let pc' = Mode.pc instr.mode ~addr in
      let addr = if not align_pc then pc' else Addr.(pc' / int 4 * int 4) in
      let addr = Addr.to_int64 addr in
      Ok Il.(num64 Int64.(addr + lbl) 32)
  | _ -> Error (`BadOperand i)

let op_mem (instr : I.t) i sz addr =
  match op_mem_off instr i sz with
  | Error _ -> (
    match op_mem_preidx instr i sz with
    | Error _ -> (
      match op_mem_postidx instr i sz with
      | Error _ ->
          Result.(
            op_mem_label instr i addr
            >>| fun op -> (Il.(load instr.endian Int.(sz lsr 3) mu op), []))
      | op -> op )
    | op -> op )
  | Ok op -> Ok (op, [])

let op_reg_or_mem_label (instr : I.t) i addr =
  match op_reg instr i with
  | Error _ -> op_mem_label instr i addr
  | op -> op

(* the manual will actually mask the destination, but this makes
 * it less trivial to know which ISA we're switching to.
 * Arch.addr_of_arch will actually handle this for us, so just omit the mask. *)
let mask_pc_dst = function
  (* | Il.Const c ->
   *     let m = Bitvec.modulus c.size in
   *     let mask = Bitvec.(int32 0xFFFFFFFEl mod m) in
   *     let value = Bitvec.(c.value land mask mod m) in
   *     Il.Const {c with value}
   * | dst -> Il.(dst & num32 0xFFFFFFFEl 32) *)
  | dst -> dst

let write_pc ?(relative = true) ?(call = false) ?(switch = None) mode addr
    dst =
  let pc' = if relative then Mode.pc mode ~addr else Addr.zero in
  let pc' = Il.bitv pc' 32 in
  let dst = mask_pc_dst Il.(pc' + dst) in
  match switch with
  | None -> if call then Il.(call dst) else Il.(jmp dst)
  | Some s -> if call then Il.(call_switch dst s) else Il.(jmp_switch dst s)

let switch_of_op (instr : I.t) addr i =
  let switch addr =
    let e_str = Endian.to_string instr.endian in
    if Addr.(addr land one = zero) then
      let mode =
        match instr.mode with
        | Mode.Armv7 as mode -> mode
        | Mode.Thumbv7 -> Mode.Armv7
        | Mode.Armv8 as mode -> mode
      in
      Some (Mode.to_string mode ^ e_str)
    else Some (Mode.(to_string Thumbv7) ^ e_str)
  in
  match instr.operands.(i) with
  | O.Reg R.PC -> switch (Mode.pc instr.mode ~addr)
  | O.(Mem Memory.(Label lbl)) -> Addr.int64 lbl |> switch
  | _ -> None

let switch_of_expr (instr : I.t) e =
  let switch value =
    let e_str = Endian.to_string instr.endian in
    if Bitvec.(value land one mod m32 = zero) then
      let mode =
        match instr.mode with
        | Mode.Armv7 as mode -> mode
        | Mode.Thumbv7 -> Mode.Armv7
        | Mode.Armv8 as mode -> mode
      in
      Some (Mode.to_string mode ^ e_str)
    else Some (Mode.(to_string Thumbv7) ^ e_str)
  in
  match e with
  | Il.Const c -> switch c.value
  | _ -> None

(* we don't model the program counter as
 * an explicit register/variable in our IR.
 * instead, a write to the PC is just modeled
 * as a jump to the source operand.
 * conversely, a read from the PC is substituted
 * with the actual value of the PC
 * (addr + 4 on thumb, addr + 8 otherwise) *)
let replace_pc (instr : I.t) addr ctx stmts =
  let pc' = Mode.pc instr.mode ~addr in
  let sub = (pc, Il.bitv pc' 32) in
  (* from section A4.1.1:
   *
   *  "A processor in Thumb state can enter ARM state
   *   by executing any of the following instructions:
   *   BX, BLX, or an LDR or LDM that loads the PC.
   *
   *   A processor in ARM state can enter Thumb state
   *   by executing any of the same instructions.
   *
   *   In ARMv7, a processor in ARM state can also
   *   enter Thumb state by executing an ADC, ADD,
   *   AND, ASR, BIC, EOR, LSL, LSR, MOV, MVN, ORR,
   *   ROR, RRX, RSB, RSC, SBC, or SUB instruction
   *   that has the PC as destination register and
   *   does not set the condition flags." *)
  let can_switch =
    match instr.mnemonic with
    | M.ADC
     |M.ADD
     |M.AND
     |M.ASR
     |M.BIC
     |M.EOR
     |M.LSL
     |M.LSR
     |M.MOV
     |M.MVN
     |M.ORR
     |M.ROR
     |M.RRX
     |M.RSB
     |M.RSC
     |M.SBC
     |M.SUB -> not (Mode.is_thumb instr.mode)
    | M.LDM | M.LDR -> true
    (* we handle BX and BLX manually *)
    | _ -> false
  in
  let rec aux ?(found_write = false) res = function
    | [] -> Ok (List.rev res)
    | stmt :: rest -> (
      match stmt with
      | Il.Assign (dst, src) when Il.equal_expr dst pc ->
          (* XXX: we've found the first write to the PC,
           * but there could be more of them, or a read
           * from the PC afterwards.
           * for now, we should say that the provided
           * semantics is not well-formed. *)
          if found_write then Error `BadInstr
          else
            let src' = Il.substitute_expr src ~sub in
            (* don't store to a temp var if we've already
             * resolved the new PC to a constant *)
            let dst', stmt' =
              match src' with
              | Il.Const _ -> (src', None)
              | _ ->
                  let t1 = Il.Context.tmp ctx 32 in
                  (t1, Some Il.(t1 := src'))
            in
            let jmp' =
              (* if the new PC is a non-constant operand
               * then we can say that there still may
               * be a switch, but we statically
               * resolve the target ISA later *)
              let switch =
                if can_switch then Some (switch_of_expr instr src') else None
              in
              write_pc instr.mode addr dst' ~relative:false ~switch
            in
            (* XXX: what if we hit a goto/jmp
             * before we reach the end or a label? *)
            let rec aux2 res = function
              | [] ->
                  (* we reached the absolute end *)
                  List.rev (jmp' :: res)
              | Il.(Label _ as l) :: rest ->
                  (* we reached the end of this block *)
                  List.rev (jmp' :: res) @ l :: rest
              | stmt :: rest ->
                  (* continue *)
                  aux2 (stmt :: res) rest
            in
            let rest' = aux2 [] rest in
            let res =
              match stmt' with
              | None -> res
              | Some stmt -> stmt :: res
            in
            aux res rest' ~found_write:true
      | _ ->
          let stmt' = Il.substitute_expr_in_stmt stmt ~sub in
          if found_write && not (Il.equal_stmt stmt stmt') then
            Error `BadInstr
          else aux (stmt' :: res) rest ~found_write )
  in
  aux [] stmts

let lift (instr : I.t) addr ctx =
  let open Result.Let_syntax in
  let new_label () = Il.Context.label ctx addr in
  let new_tmp size = Il.Context.tmp ctx size in
  let endaddr = I.end_addr instr ~addr in
  let start_lbl = new_label () in
  let init_stmts =
    match instr.cond with
    | None -> Il.[( @ ) start_lbl]
    | Some c when C.is_unconditional c -> Il.[( @ ) start_lbl]
    | Some c ->
        let c = cond_to_expr_inv c in
        let l1 = new_label () in
        let l2 = new_label () in
        Il.
          [ ( @ ) start_lbl
          ; goto_if c l1 l2
          ; ( @ ) l1
          ; jmp (bitv endaddr 32)
          ; ( @ ) l2 ]
  in
  let shift_c = shift_c new_label new_tmp in
  let op_shift_c = op_shift_c new_label new_tmp in
  let op_shift_or_reg_shift_c = op_shift_or_reg_shift_c new_label new_tmp in
  let is_pc e = Il.equal_expr e pc in
  let is_sp e = Il.equal_expr e sp in
  let new_cf () =
    match instr.cflag with
    | None -> cf
    | Some c -> Il.bit c
  in
  let assign_mem dst src =
    match dst with
    | Il.Load r -> Il.(r.mem := store_r r src)
    | _ -> Il.(dst := src)
  in
  let%bind stmts =
    match instr.mnemonic with
    | M.ADC when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn + rm + ze 32 cf]
    | M.ADC when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := rn + imm + ze 32 cf]
    | M.ADC when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        Ok Il.[rd := rn + sh + ze 32 cf]
    | M.ADCS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        if is_pc rdn then Ok Il.[rdn := rdn + rm + ze 32 cf]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := rdn + rm + ze 32 cf
              ; nf := t1 <$ num 0 32
              ; cf := t1 < rdn
              ; vf := hi 1 (~~(rdn ^ rm) & (rdn ^ t1))
              ; rdn := t1
              ; zf := rdn = num 0 32 ]
    | M.ADCS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn + imm + ze 32 cf]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := rn + imm + ze 32 cf
              ; nf := t1 <$ num 0 32
              ; cf := t1 < rn
              ; vf := hi 1 (~~(rn ^ imm) & (rn ^ t1))
              ; rd := t1
              ; zf := rd = num 0 32 ]
    | M.ADCS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let is_reg_shift =
          match instr.operands.(3) with
          | O.Reg_shift _ -> true
          | _ -> false
        in
        if (not is_reg_shift) && is_pc rd then
          Ok Il.[rd := rn + sh + ze 32 cf]
        else
          let t1 = new_tmp 32 in
          let t2 = new_tmp 32 in
          Ok
            Il.
              [ t2 := sh
              ; t1 := rn + t2 + ze 32 cf
              ; nf := t1 <$ num 0 32
              ; cf := t1 < rn
              ; vf := hi 1 (~~(rn ^ t2) & (rn ^ t1))
              ; rd := t1
              ; zf := rd = num 0 32 ]
    | M.ADD when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        Ok Il.[rdn := rdn + rm_or_imm]
    | (M.ADD | M.ADDW) when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        Ok Il.[rd := rn + rm_or_imm]
    | M.ADD when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        Ok Il.[rd := rn + sh]
    | M.ADDS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        let is_imm =
          match instr.operands.(1) with
          | O.Imm _ -> true
          | _ -> false
        in
        if (not is_imm) && Il.equal_expr rdn pc then
          Ok Il.[rdn := rdn + rm_or_imm]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := rdn + rm_or_imm
              ; nf := t1 <$ num 0 32
              ; nf := t1 < rdn
              ; vf := hi 1 (~~(rdn ^ rm_or_imm) & (rdn ^ t1))
              ; rdn := t1
              ; zf := rdn = num 0 32 ]
    | M.ADDS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn + rm_or_imm]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := rn + rm_or_imm
              ; nf := t1 <$ num 0 32
              ; nf := t1 < rn
              ; vf := hi 1 (~~(rn ^ rm_or_imm) & (rn ^ t1))
              ; rd := t1
              ; zf := rd = num 0 32 ]
    | M.ADDS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let is_reg_shift =
          match instr.operands.(3) with
          | O.Reg_shift _ -> true
          | _ -> false
        in
        if (not is_reg_shift) && is_pc rd then Ok Il.[rd := rn + sh]
        else
          let t1 = new_tmp 32 in
          let t2 = new_tmp 32 in
          Ok
            Il.
              [ t2 := sh
              ; t1 := rn + t2
              ; nf := t1 <$ num 0 32
              ; cf := t1 < rn
              ; vf := hi 1 (~~(rn ^ t2) & (rn ^ t1))
              ; rd := t1
              ; zf := rd = num 0 32 ]
    | M.ADR ->
        let%bind rd = op_reg instr 0 in
        let%bind lbl = op_mem_label instr 1 addr in
        Ok Il.[rd := lbl]
    | M.AND when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn & rm]
    | M.AND when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := rn & imm]
    | M.AND when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        Ok Il.[rd := rn & sh]
    | M.ANDS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        if is_pc rdn then Ok Il.[rdn := rdn & rm]
        else
          Ok Il.[rdn := rdn & rm; nf := rdn <$ num 0 32; zf := rdn = num 0 32]
    | M.ANDS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn & imm]
        else
          Ok
            Il.
              [ rd := rn & imm
              ; nf := rd <$ num 0 32
              ; zf := rd = num 0 32
              ; cf := new_cf () ]
    | M.ANDS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh, carry, stmts =
          op_shift_or_reg_shift_c instr 3 32 ~lhs:rm
        in
        if is_pc rd then Ok Il.[rd := rn & sh]
        else
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := rn & sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.ASR when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn >>> rm]
    | M.ASR when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        Ok Il.[rd := rn >>> rm_or_imm]
    | M.ASRS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let sh, carry, stmts = shift_c 32 Sh.ASR ~lhs:rdn ~rhs:rm in
        let t1 = new_tmp 32 in
        Ok
          ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
          @ stmts
          @ Il.[cf := carry; rdn := t1] )
    | M.ASRS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn >>> rm_or_imm]
        else
          let sh, carry, stmts = shift_c 32 Sh.ASR ~lhs:rn ~rhs:rm_or_imm in
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.B ->
        let%bind lbl = op_mem_label instr 0 addr in
        Ok [write_pc instr.mode addr lbl ~relative:false]
    | M.BFC ->
        let%bind rd = op_reg instr 0 in
        let%bind lsb = op_imm instr 1 32 in
        let%bind width = op_imm instr 2 32 in
        (* this should foldable to a constant *)
        let n1 = Il.num 1 32 in
        let mask = Il.((n1 << width - n1 << n1) - n1 << lsb) in
        Ok Il.[rd := rd & ~~mask]
    | M.BFI ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind lsb = op_imm instr 2 32 in
        let%bind width = op_imm instr 3 32 in
        let%bind high =
          match width with
          | Il.Const c -> Ok (Bitvec.to_int c.value - 1)
          | _ -> Error (`BadOperand 3)
        in
        let t1 = new_tmp 32 in
        let n1 = Il.num 1 32 in
        let mask = Il.((n1 << width - n1 << n1) - n1 << lsb) in
        Ok Il.[t1 := ze 32 (ex high 0 rn); rd := (rd & ~~mask) || t1 << lsb]
    | M.BIC when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn & ~~rm]
    | M.BIC when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := rn & ~~imm]
    | M.BIC when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        Ok Il.[rd := rn & ~~sh]
    | M.BICS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        if is_pc rdn then Ok Il.[rdn := rdn & ~~rm]
        else
          Ok
            Il.
              [rdn := rdn & ~~rm; nf := rdn <$ num 0 32; zf := rdn = num 0 32]
    | M.BICS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn & ~~imm]
        else
          Ok
            Il.
              [ rd := rn & ~~imm
              ; nf := rd <$ num 0 32
              ; zf := rd = num 0 32
              ; cf := new_cf () ]
    | M.BICS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh, carry, stmts =
          op_shift_or_reg_shift_c instr 3 32 ~lhs:rm
        in
        if is_pc rd then Ok Il.[rd := rn & ~~sh]
        else
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := rn & ~~sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.BKPT -> Ok Il.[breakpoint ()]
    | M.BL when Mode.is_thumb instr.mode ->
        let%bind rm_or_lbl = op_reg_or_mem_label instr 0 addr in
        let retaddr = Addr.to_int32 endaddr in
        let retaddr = Int32.(retaddr land 0xFFFFFFFEl lor 1l) in
        let b =
          write_pc instr.mode addr rm_or_lbl ~relative:false ~call:true
        in
        Ok Il.[lr := num32 retaddr 32; b]
    | M.BL ->
        let%bind rm_or_lbl = op_reg_or_mem_label instr 0 addr in
        let retaddr = Addr.to_int32 endaddr in
        let b =
          write_pc instr.mode addr rm_or_lbl ~relative:false ~call:true
        in
        Ok Il.[lr := num32 retaddr 32; b]
    | M.BLX when Mode.is_thumb instr.mode ->
        let%bind rm_or_lbl = op_reg_or_mem_label instr 0 addr in
        let switch = Some (switch_of_op instr addr 0) in
        let retaddr = Addr.to_int32 endaddr in
        let retaddr = Int32.(retaddr land 0xFFFFFFFEl lor 1l) in
        let b =
          write_pc instr.mode addr rm_or_lbl ~relative:false ~call:true
            ~switch
        in
        Ok Il.[lr := num32 retaddr 32; b]
    | M.BLX ->
        let%bind rm_or_lbl = op_reg_or_mem_label instr 0 addr in
        let switch = Some (switch_of_op instr addr 0) in
        let retaddr = Addr.to_int32 endaddr in
        let b =
          write_pc instr.mode addr rm_or_lbl ~relative:false ~call:true
            ~switch
        in
        Ok Il.[lr := num32 retaddr 32; b]
    | M.BX | M.BXJ ->
        let%bind rm = op_reg instr 0 in
        let switch = Some (switch_of_op instr addr 0) in
        let b = write_pc instr.mode addr rm ~relative:false ~switch in
        Ok [b]
    | M.CBNZ ->
        let%bind rn = op_reg instr 0 in
        let%bind lbl = op_mem_label instr 1 addr in
        Ok Il.[jmp_if (rn <> num 0 32) (mask_pc_dst lbl) (bitv endaddr 32)]
    | M.CBZ ->
        let%bind rn = op_reg instr 0 in
        let%bind lbl = op_mem_label instr 1 addr in
        Ok Il.[jmp_if (rn = num 0 32) (mask_pc_dst lbl) (bitv endaddr 32)]
    | M.CLZ ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let l6 = new_label () in
        let l7 = new_label () in
        let l8 = new_label () in
        let l9 = new_label () in
        let l10 = new_label () in
        let l11 = new_label () in
        let l12 = new_label () in
        let n0 = Il.num 0 32 in
        Ok
          Il.
            [ t1 := rn
            ; goto_if (t1 = num 0 32) l1 l2
            ; ( @ ) l1
            ; t2 := num 32 32
            ; jmp (bitv endaddr 32)
            ; ( @ ) l2
            ; t2 := num 0 32
            ; goto_if ((t1 & num32 0xFFFF0000l 32) = n0) l3 l4
            ; ( @ ) l3
            ; t1 := t1 << num 16 32
            ; t2 := t2 + num 16 32
            ; goto l4
            ; ( @ ) l4
            ; goto_if ((t1 & num32 0xFF000000l 32) = n0) l5 l6
            ; ( @ ) l5
            ; t1 := t1 << num 8 32
            ; t2 := t2 + num 8 32
            ; goto l6
            ; ( @ ) l6
            ; goto_if ((t1 & num32 0xF0000000l 32) = n0) l7 l8
            ; ( @ ) l7
            ; t1 := t1 << num 4 32
            ; t2 := t2 + num 4 32
            ; goto l8
            ; ( @ ) l8
            ; goto_if ((t1 & num32 0xC0000000l 32) = n0) l9 l10
            ; ( @ ) l9
            ; t1 := t1 << num 2 32
            ; t2 := t2 + num 2 32
            ; goto l10
            ; ( @ ) l10
            ; goto_if ((t1 & num32 0x80000000l 32) = n0) l11 l12
            ; ( @ ) l11
            ; t2 := t2 + num 1 32
            ; goto l12
            ; ( @ ) l12
            ; rd := t2 ]
    | M.CMN when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        let t1 = new_tmp 32 in
        Ok
          Il.
            [ t1 := rdn + rm_or_imm
            ; cf := t1 < rdn
            ; nf := rdn <$ ~-rm_or_imm
            ; zf := rdn = ~-rm_or_imm
            ; vf := hi 1 (~~(rdn ^ rm_or_imm) & (rdn ^ t1)) ]
    | M.CMN when Array.length instr.operands = 3 ->
        let%bind rn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift_or_reg_shift instr 2 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok
          Il.
            [ t1 := rn + sh
            ; cf := t1 < rn
            ; nf := rn <$ ~-sh
            ; zf := rn = ~-sh
            ; vf := hi 1 (~~(rn ^ sh) & (rn ^ t1)) ]
    | M.CMP when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        let t1 = new_tmp 32 in
        Ok
          Il.
            [ t1 := rdn - rm_or_imm
            ; cf := rdn >= rm_or_imm
            ; nf := rdn <$ rm_or_imm
            ; zf := rdn = rm_or_imm
            ; vf := hi 1 (~~(rdn ^ rm_or_imm) & (rdn ^ t1)) ]
    | M.CMP when Array.length instr.operands = 3 ->
        let%bind rn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift_or_reg_shift instr 2 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok
          Il.
            [ t1 := rn - sh
            ; cf := rn >= sh
            ; nf := rn <$ sh
            ; zf := rn = sh
            ; vf := hi 1 (~~(rn ^ sh) & (rn ^ t1)) ]
    | M.EOR when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn ^ rm]
    | M.EOR when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := rn ^ imm]
    | M.EOR when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        Ok Il.[rd := rn ^ sh]
    | M.EORS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        if is_pc rdn then Ok Il.[rdn := rdn ^ rm]
        else
          Ok Il.[rdn := rdn ^ rm; nf := rdn <$ num 0 32; zf := rdn = num 0 32]
    | M.EORS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn ^ imm]
        else
          Ok
            Il.
              [ rd := rn ^ imm
              ; nf := rd <$ num 0 32
              ; zf := rd = num 0 32
              ; cf := new_cf () ]
    | M.EORS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh, carry, stmts =
          op_shift_or_reg_shift_c instr 3 32 ~lhs:rm
        in
        if is_pc rd then Ok Il.[rd := rn ^ sh]
        else
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := rn ^ sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.ERET -> Ok Il.[jmp lr] (* not exactly correct *)
    | M.HLT -> Ok Il.[halt ()]
    | M.IT
     |M.ITE
     |M.ITT
     |M.ITET
     |M.ITTE
     |M.ITEE
     |M.ITETT
     |M.ITTET
     |M.ITEET
     |M.ITTTE
     |M.ITETE
     |M.ITTEE
     |M.ITEEE -> Ok []
    | M.LDA ->
        let%bind rt = op_reg instr 0 in
        let%bind m = op_mem_off instr 1 32 in
        Ok Il.[rt := m]
    | M.LDM | M.LDMIA -> (
        let%bind rn = op_reg instr 0 in
        let%bind rl = op_reglist instr 1 in
        let stmts =
          List.mapi rl ~f:(fun i r ->
              let loc = Il.(rn + num Int.(i lsl 2) 32) in
              Il.(r := load instr.endian 4 mu loc) )
        in
        match instr.write_back with
        | Some true ->
            if List.mem rl rn ~equal:Il.equal_expr then
              Ok (stmts @ Il.[rn := undefined 32])
            else
              let n = List.length rl in
              Ok (stmts @ Il.[rn := rn + num Int.(n * 4) 32])
        | _ -> Ok stmts )
    | M.LDMDA -> (
        let%bind rn = op_reg instr 0 in
        let%bind rl = op_reglist instr 1 in
        let n = List.length rl in
        let stmts =
          let disp = (n - 1) lsl 2 in
          List.mapi rl ~f:(fun i r ->
              let off = disp - (i lsl 2) in
              let loc = Il.(rn - num off 32) in
              Il.(r := load instr.endian 4 mu loc) )
        in
        match instr.write_back with
        | Some true ->
            if List.mem rl rn ~equal:Il.equal_expr then
              Ok (stmts @ Il.[rn := undefined 32])
            else Ok (stmts @ Il.[rn := rn - num Int.(n lsl 2) 32])
        | _ -> Ok stmts )
    | M.LDMDB -> (
        let%bind rn = op_reg instr 0 in
        let%bind rl = op_reglist instr 1 in
        let n = List.length rl in
        let stmts =
          let disp = n lsl 2 in
          List.mapi rl ~f:(fun i r ->
              let off = disp - (i lsl 2) in
              let loc = Il.(rn - num off 32) in
              Il.(r := load instr.endian 4 mu loc) )
        in
        match instr.write_back with
        | Some true ->
            if List.mem rl rn ~equal:Il.equal_expr then
              Ok (stmts @ Il.[rn := undefined 32])
            else Ok (stmts @ Il.[rn := rn - num Int.(n lsl 2) 32])
        | _ -> Ok stmts )
    | M.LDMIB -> (
        let%bind rn = op_reg instr 0 in
        let%bind rl = op_reglist instr 1 in
        let stmts =
          List.mapi rl ~f:(fun i r ->
              let loc = Il.(rn - num Int.((i + 1) lsl 2) 32) in
              Il.(r := load instr.endian 4 mu loc) )
        in
        match instr.write_back with
        | Some true ->
            if List.mem rl rn ~equal:Il.equal_expr then
              Ok (stmts @ Il.[rn := undefined 32])
            else
              let n = List.length rl in
              Ok (stmts @ Il.[rn := rn + num Int.(n lsl 2) 32])
        | _ -> Ok stmts )
    | M.LDR | M.LDREX | M.LDRT ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 32 addr in
        let t1 = new_tmp 32 in
        Ok (Il.[t1 := m] @ stmts @ Il.[rt := t1])
    | M.LDRB | M.LDRBT | M.LDREXB ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 8 addr in
        let t1 = new_tmp 8 in
        Ok (Il.[t1 := m] @ stmts @ Il.[rt := ze 32 t1])
    | M.LDRD | M.LDREXD ->
        let%bind rt = op_reg instr 0 in
        let%bind rt2 = op_reg instr 1 in
        let%bind m, stmts = op_mem instr 2 64 addr in
        let t1 = new_tmp 64 in
        Ok
          ( Il.[t1 := m]
          @ stmts
          @
          match instr.endian with
          | `LE -> Il.[rt := lo 32 t1; rt2 := hi 32 t1]
          | `BE -> Il.[rt := hi 32 t1; rt2 := lo 32 t1] )
    | M.LDRH | M.LDRHT | M.LDREXH ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 16 addr in
        let t1 = new_tmp 16 in
        Ok (Il.[t1 := m] @ stmts @ Il.[rt := ze 32 t1])
    | M.LDRSB | M.LDRSBT ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 8 addr in
        let t1 = new_tmp 8 in
        Ok (Il.[t1 := m] @ stmts @ Il.[rt := se 32 t1])
    | M.LDRSH | M.LDRSHT ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 16 addr in
        let t1 = new_tmp 16 in
        Ok (Il.[t1 := m] @ stmts @ Il.[rt := se 32 t1])
    | M.LSL when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn << rm]
    | M.LSL when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        Ok Il.[rd := rn << rm_or_imm]
    | M.LSLS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let sh, carry, stmts = shift_c 32 Sh.LSL ~lhs:rdn ~rhs:rm in
        let t1 = new_tmp 32 in
        Ok
          ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
          @ stmts
          @ Il.[cf := carry; rdn := t1] )
    | M.LSLS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn << rm_or_imm]
        else
          let sh, carry, stmts = shift_c 32 Sh.LSL ~lhs:rn ~rhs:rm_or_imm in
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.LSR when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn >> rm]
    | M.LSR when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        Ok Il.[rd := rn >> rm_or_imm]
    | M.LSRS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let sh, carry, stmts = shift_c 32 Sh.LSR ~lhs:rdn ~rhs:rm in
        let t1 = new_tmp 32 in
        Ok
          ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
          @ stmts
          @ Il.[cf := carry; rdn := t1] )
    | M.LSRS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn >> rm_or_imm]
        else
          let sh, carry, stmts = shift_c 32 Sh.LSR ~lhs:rn ~rhs:rm_or_imm in
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.MLA ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        Ok Il.[rd := (rn * rm) + ra]
    | M.MLAS ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        Ok
          Il.[rd := (rn * rm) + ra; nf := rd <$ num 0 32; zf := rd = num 0 32]
    | M.MLS ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        Ok Il.[rd := ra - (rn * rm)]
    | M.MOV when Array.length instr.operands = 2 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        Ok Il.[rd := rm_or_imm]
    | M.MOV when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift instr 1 32 ~lhs:rm in
        Ok Il.[rd := sh]
    | M.MOVS when Array.length instr.operands = 2 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        if is_pc rd then Ok Il.[rd := rm_or_imm]
        else
          Ok
            Il.
              [ rd := rm_or_imm
              ; nf := rd <$ num 0 32
              ; zf := rd = num 0 32
              ; cf := new_cf () ]
    | M.MOVS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh, carry, stmts =
          op_shift_or_reg_shift_c instr 1 32 ~lhs:rm
        in
        if is_pc rd then Ok Il.[rd := sh]
        else
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.MOVT ->
        let%bind rd = op_reg instr 0 in
        let%bind imm = op_imm instr 1 16 in
        let t1 = new_tmp 16 in
        Ok Il.[t1 := lo 16 rd; rd := imm @@ t1]
    | M.MUL ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        Ok Il.[rd := rn * rm]
    | M.MULS ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        Ok Il.[rd := rn * rm; nf := rd <$ num 0 32; zf := rd = num 0 32]
    | M.MVN when Array.length instr.operands = 2 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm_or_imm = op_imm instr 1 32 in
        Ok Il.[rd := ~~rm_or_imm]
    | M.MVN when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift_or_reg_shift instr 2 32 ~lhs:rm in
        Ok Il.[rd := ~~sh]
    | M.MVNS when Array.length instr.operands = 2 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm_or_imm = op_imm instr 1 32 in
        if is_pc rd then Ok Il.[rd := ~~rm_or_imm]
        else
          Ok
            Il.
              [ rd := ~~rm_or_imm
              ; nf := rd <$ num 0 32
              ; zf := rd = num 0 32
              ; cf := new_cf () ]
    | M.MVNS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh, carry, stmts =
          op_shift_or_reg_shift_c instr 1 32 ~lhs:rm
        in
        if is_pc rd then Ok Il.[rd := ~~sh]
        else
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := ~~sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.NOP -> Ok []
    | M.ORN when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := rn || ~~imm]
    | M.ORN when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := rn || ~~t1]
    | M.ORNS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok
          Il.
            [ rd := rn || ~~imm
            ; nf := rd <$ num 0 32
            ; zf := rd = num 0 32
            ; cf := new_cf () ]
    | M.ORNS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh, carry, stmts = op_shift_c instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          ( Il.
              [ t1 := sh
              ; t2 := rn || ~~t1
              ; nf := t2 <$ num 0 32
              ; zf := t2 = num 0 32 ]
          @ stmts
          @ Il.[cf := carry; rd := t2] )
    | M.ORR when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn || rm]
    | M.ORR when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := rn || imm]
    | M.ORR when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := rn || t1]
    | M.ORRS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        if is_pc rdn then Ok Il.[rdn := rdn || rm]
        else
          Ok
            Il.[rdn := rdn || rm; nf := rdn <$ num 0 32; zf := rdn = num 0 32]
    | M.ORRS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok
          Il.
            [ rd := rn || imm
            ; nf := rd <$ num 0 32
            ; zf := rd = num 0 32
            ; cf := new_cf () ]
    | M.ORRS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh, carry, stmts =
          op_shift_or_reg_shift_c instr 3 32 ~lhs:rm
        in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          ( Il.
              [ t1 := sh
              ; t2 := rn || t1
              ; nf := t2 <$ num 0 32
              ; zf := t2 = num 0 32 ]
          @ stmts
          @ Il.[cf := carry; rd := t2] )
    | M.PKHBT ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        Ok Il.[t1 := lo 16 rn; t2 := hi 16 sh; rd := t2 @@ t1]
    | M.PKHTB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        Ok Il.[t1 := lo 16 sh; t2 := hi 16 rn; rd := t2 @@ t1]
    | M.POP ->
        let%bind rl = op_reg_or_reglist instr 0 in
        let stmts =
          List.mapi rl ~f:(fun i r ->
              let loc = Il.(sp + num Int.(i lsl 2) 32) in
              Il.(r := load instr.endian 4 mu loc) )
        in
        if List.mem rl sp ~equal:Il.equal_expr then
          Ok (stmts @ Il.[sp := undefined 32])
        else
          let n = List.length rl in
          Ok (stmts @ Il.[sp := sp + num Int.(n * 4) 32])
    | M.PUSH ->
        let%bind rl = op_reg_or_reglist instr 0 in
        let n = List.length rl in
        let stmts =
          let disp = n lsl 2 in
          List.mapi rl ~f:(fun i r ->
              let off = disp - (i lsl 2) in
              let loc = Il.(sp - num off 32) in
              let m = Il.(load instr.endian 4 mu loc) in
              if is_sp r && i > 0 then assign_mem m Il.(undefined 32)
              else assign_mem m r )
        in
        Ok (stmts @ Il.[sp := sp - num Int.(n * 4) 32])
    | M.REV ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        Ok
          Il.
            [ t1 := lo 8 rm
            ; t2 := ex 15 8 rm
            ; t3 := ex 23 16 rm
            ; t4 := hi 8 rm
            ; rd := t1 @@ t2 @@ t3 @@ t4 ]
    | M.REV16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        Ok
          Il.
            [ t1 := lo 8 rm
            ; t2 := ex 15 8 rm
            ; t3 := ex 23 16 rm
            ; t4 := hi 8 rm
            ; rd := t3 @@ t4 @@ t1 @@ t2 ]
    | M.REVSH ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 24 in
        Ok
          Il.[t1 := lo 8 rm; t2 := ex 15 8 rm; t3 := se 24 t1; rd := t3 @@ t2]
    | M.RFEDA ->
        let%bind rn = op_reg instr 0 in
        let wback =
          match instr.write_back with
          | Some true -> Il.[rn := rn - num 8 32]
          | _ -> []
        in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          ( Il.[t1 := rn - num 4 32]
          @ wback
          @ Il.
              [ t2 := load instr.endian 4 mu t1
              ; (* TODO: handle SPSR *)
                write_pc instr.mode addr t2 ] )
    | M.RFEDB ->
        let%bind rn = op_reg instr 0 in
        let wback =
          match instr.write_back with
          | Some true -> Il.[rn := rn - num 8 32]
          | _ -> []
        in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          ( Il.[t1 := rn - num 8 32]
          @ wback
          @ Il.
              [ t2 := load instr.endian 4 mu t1
              ; (* TODO: handle SPSR *)
                write_pc instr.mode addr t2 ] )
    | M.RFEIA ->
        let%bind rn = op_reg instr 0 in
        let wback =
          match instr.write_back with
          | Some true -> Il.[rn := rn + num 8 32]
          | _ -> []
        in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          ( Il.[t1 := rn]
          @ wback
          @ Il.
              [ t2 := load instr.endian 4 mu t1
              ; (* TODO: handle SPSR *)
                write_pc instr.mode addr t2 ] )
    | M.RFEIB ->
        let%bind rn = op_reg instr 0 in
        let wback =
          match instr.write_back with
          | Some true -> Il.[rn := rn + num 8 32]
          | _ -> []
        in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          ( Il.[t1 := rn + num 4 32]
          @ wback
          @ Il.
              [ t2 := load instr.endian 4 mu t1
              ; (* TODO: handle SPSR *)
                write_pc instr.mode addr t2 ] )
    | M.ROR when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := shift' 32 Sh.ROR ~lhs:rdn ~rhs:rm]
    | M.ROR when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        Ok Il.[rd := shift' 32 Sh.ROR ~lhs:rn ~rhs:rm_or_imm]
    | M.RORS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let sh, carry, stmts = shift_c 32 Sh.ROR ~lhs:rdn ~rhs:rm in
        let t1 = new_tmp 32 in
        Ok
          ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
          @ stmts
          @ Il.[cf := carry; rdn := t1] )
    | M.RORS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        let sh, carry, stmts = shift_c 32 Sh.ROR ~lhs:rn ~rhs:rm_or_imm in
        if is_pc rd then Ok Il.[rd := sh]
        else
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.RRX ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rd := shift' 32 Sh.RRX ~lhs:rm ~rhs:(num 1 32)]
    | M.RRXS ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let sh, carry, stmts =
          shift_c 32 Sh.RRX ~lhs:rm ~rhs:Il.(num 1 32)
        in
        if is_pc rd then Ok Il.[rd := sh]
        else
          let t1 = new_tmp 32 in
          Ok
            ( Il.[t1 := sh; nf := t1 <$ num 0 32; zf := t1 = num 0 32]
            @ stmts
            @ Il.[cf := carry; rd := t1] )
    | M.RSB when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := imm - rn]
    | M.RSB when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := t1 - rn]
    | M.RSBS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := imm - rn]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := imm - rn
              ; cf := rn <= imm
              ; nf := rn >$ imm
              ; zf := rn = imm
              ; vf := hi 1 (~~(imm ^ rn) & (imm ^ t1))
              ; rd := t1 ]
    | M.RSBS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        if is_pc rd then Ok Il.[t1 := sh; rd := t1 - rn]
        else
          let t2 = new_tmp 32 in
          Ok
            Il.
              [ t1 := sh
              ; t2 := t1 - rn
              ; cf := rn <= t1
              ; nf := rn >$ t1
              ; zf := rn = t1
              ; vf := hi 1 (~~(t1 ^ rn) & (t1 ^ t2))
              ; rd := t2 ]
    | M.RSC when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := imm - rn - ze 32 cf]
    | M.RSC when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := t1 - rn - ze 32 cf]
    | M.RSCS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := imm - rn - ze 32 cf]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := imm - rn - ze 32 cf
              ; cf := rn + ze 32 cf <= imm
              ; nf := imm <$ rn + ze 32 cf
              ; vf := hi 1 (~~(imm ^ rn) & (imm ^ t1))
              ; rd := t1
              ; zf := rd = num 0 32 ]
    | M.RSCS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        if is_pc rd then Ok Il.[t1 := sh; rd := t1 - rn - ze 32 cf]
        else
          let t2 = new_tmp 32 in
          Ok
            Il.
              [ t1 := sh
              ; t2 := t1 - rn - ze 32 cf
              ; cf := rn + ze 32 cf <= t1
              ; nf := t1 <$ rn + ze 32 cf
              ; vf := hi 1 (~~(t1 ^ rn) & (t1 ^ t2))
              ; rd := t2
              ; zf := rd = num 0 32 ]
    | M.SADD8 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        let t9 = new_tmp 32 in
        let t10 = new_tmp 32 in
        let t11 = new_tmp 32 in
        let t12 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 8 rn
            ; t2 := ex 15 8 rn
            ; t3 := ex 23 16 rn
            ; t4 := hi 8 rn
            ; t5 := lo 8 rm
            ; t6 := ex 15 8 rm
            ; t7 := ex 23 16 rm
            ; t8 := hi 8 rm
            ; t9 := se 32 t1 + se 32 t5
            ; t10 := se 32 t2 + se 32 t6
            ; t11 := se 32 t3 + se 32 t7
            ; t12 := se 32 t4 + se 32 t8
            ; t1 := lo 8 t9
            ; t2 := lo 8 t10
            ; t3 := lo 8 t11
            ; t4 := lo 8 t12
            ; rd := t4 @@ t3 @@ t2 @@ t1
            ; ge0 := t9 >=$ num 0 32
            ; ge1 := t10 >=$ num 0 32
            ; ge2 := t11 >=$ num 0 32
            ; ge3 := t12 >=$ num 0 32 ]
    | M.SADD16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := se 32 t1 + se 32 t3
            ; t6 := se 32 t2 + se 32 t4
            ; t1 := lo 16 t5
            ; t2 := lo 16 t6
            ; rd := t2 @@ t1
            ; ge0 := t5 >=$ num 0 32
            ; ge1 := ge0
            ; ge2 := t6 >=$ num 0 32
            ; ge3 := ge2 ]
    | M.SASX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := se 32 t1 - se 32 t4
            ; t6 := se 32 t2 + se 32 t3
            ; t1 := lo 16 t5
            ; t2 := lo 16 t6
            ; rd := t2 @@ t1
            ; ge0 := t5 >=$ num 0 32
            ; ge1 := ge0
            ; ge2 := t6 >=$ num 0 32
            ; ge3 := ge2 ]
    | M.SBC when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rdn := rdn - rm - ze 32 cf]
    | M.SBC when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        Ok Il.[rd := rn - imm - ze 32 cf]
    | M.SBC when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        Ok Il.[rd := rn - sh - ze 32 cf]
    | M.SBCS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        if is_pc rdn then Ok Il.[rdn := rdn - rm - ze 32 cf]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := rdn - rm - ze 32 cf
              ; cf := rm + ze 32 cf <= rdn
              ; nf := rdn <$ rm + ze 32 cf
              ; vf := hi 1 (~~(rdn ^ rm) & (rdn ^ t1))
              ; rdn := t1
              ; zf := rdn = num 0 32 ]
    | M.SBCS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind imm = op_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn - imm - ze 32 cf]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := rn - imm - ze 32 cf
              ; cf := imm + ze 32 cf <= rn
              ; nf := rn <$ imm + ze 32 cf
              ; vf := hi 1 (~~(rn ^ imm) & (rn ^ t1))
              ; rd := t1
              ; zf := rd = num 0 32 ]
    | M.SBCS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let is_reg_shift =
          match instr.operands.(3) with
          | O.Reg_shift _ -> true
          | _ -> false
        in
        if (not is_reg_shift) && is_pc rd then
          Ok Il.[rd := rn - sh - ze 32 cf]
        else
          let t1 = new_tmp 32 in
          let t2 = new_tmp 32 in
          Ok
            Il.
              [ t2 := sh
              ; t1 := rn - t2 - ze 32 cf
              ; cf := t2 + ze 32 cf <= rn
              ; nf := rn <$ t2 + ze 32 cf
              ; vf := hi 1 (~~(rn ^ t2) & (rn ^ t1))
              ; rd := t1
              ; zf := rd = num 0 32 ]
    | M.SBFX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind lsb = op_imm instr 2 32 in
        let%bind width = op_imm instr 3 32 in
        let%bind low =
          match lsb with
          | Il.Const c -> Ok (Bitvec.to_int c.value)
          | _ -> Error (`BadOperand 3)
        in
        let%bind high =
          match width with
          | Il.Const c -> Ok (low + Bitvec.to_int c.value - 1)
          | _ -> Error (`BadOperand 3)
        in
        Ok Il.[rd := se 32 (ex high low rn)]
    | M.SDIV ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ goto_if (rm = num 0 32) l1 l2
            ; ( @ ) l1
            ; (* TODO: generate exn? *)
              rd := num 0 32
            ; jmp (bitv endaddr 32)
            ; ( @ ) l2
            ; rd := rn /$ rm ]
    | M.SEL ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        let l6 = new_label () in
        let l7 = new_label () in
        let l8 = new_label () in
        let l9 = new_label () in
        let l10 = new_label () in
        let l11 = new_label () in
        let l12 = new_label () in
        Ok
          Il.
            [ goto_if ge0 l1 l2
            ; ( @ ) l1
            ; t1 := lo 8 rn
            ; goto l3
            ; ( @ ) l2
            ; t1 := lo 8 rm
            ; goto l3
            ; ( @ ) l3
            ; goto_if ge1 l4 l5
            ; ( @ ) l4
            ; t2 := ex 15 8 rn
            ; goto l6
            ; ( @ ) l5
            ; t2 := ex 15 8 rm
            ; goto l6
            ; ( @ ) l6
            ; goto_if ge2 l7 l8
            ; ( @ ) l7
            ; t3 := ex 23 16 rn
            ; goto l9
            ; ( @ ) l8
            ; t3 := ex 23 16 rm
            ; goto l9
            ; ( @ ) l9
            ; goto_if ge3 l10 l11
            ; ( @ ) l10
            ; t4 := hi 8 rn
            ; goto l12
            ; ( @ ) l11
            ; t4 := hi 8 rm
            ; goto l12
            ; ( @ ) l12
            ; rd := t4 @@ t3 @@ t2 @@ t1 ]
    | M.SETEND ->
        let%bind endian =
          match instr.operands.(0) with
          | O.Endian e -> Ok e
          | _ -> Error (`BadOperand 0)
        in
        let e_str = Endian.to_string endian in
        let m_str = Mode.to_string instr.mode in
        let switch = Some (m_str ^ e_str) in
        Ok Il.[jmp_switch (bitv endaddr 32) switch]
    | M.SHADD8 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        let t9 = new_tmp 32 in
        let t10 = new_tmp 32 in
        let t11 = new_tmp 32 in
        let t12 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 8 rn
            ; t2 := ex 15 8 rn
            ; t3 := ex 23 16 rn
            ; t4 := hi 8 rn
            ; t5 := lo 8 rm
            ; t6 := ex 15 8 rm
            ; t7 := ex 23 16 rm
            ; t8 := hi 8 rm
            ; t9 := se 32 t1 + se 32 t5
            ; t10 := se 32 t2 + se 32 t6
            ; t11 := se 32 t3 + se 32 t7
            ; t12 := se 32 t4 + se 32 t8
            ; t1 := ex 8 1 t9
            ; t2 := ex 8 1 t10
            ; t3 := ex 8 1 t11
            ; t4 := ex 8 1 t12
            ; rd := t4 @@ t3 @@ t2 @@ t1 ]
    | M.SHADD16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := se 32 t1 + se 32 t3
            ; t6 := se 32 t2 + se 32 t4
            ; t1 := ex 16 1 t5
            ; t2 := ex 16 1 t6
            ; rd := t2 @@ t1 ]
    | M.SHASX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := se 32 t1 - se 32 t4
            ; t6 := se 32 t2 + se 32 t3
            ; t1 := ex 16 1 t5
            ; t2 := ex 16 1 t6
            ; rd := t2 @@ t1 ]
    | M.SHSAX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := se 32 t1 + se 32 t4
            ; t6 := se 32 t2 - se 32 t3
            ; t1 := ex 16 1 t5
            ; t2 := ex 16 1 t6
            ; rd := t2 @@ t1 ]
    | M.SHSUB8 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        let t9 = new_tmp 32 in
        let t10 = new_tmp 32 in
        let t11 = new_tmp 32 in
        let t12 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 8 rn
            ; t2 := ex 15 8 rn
            ; t3 := ex 23 16 rn
            ; t4 := hi 8 rn
            ; t5 := lo 8 rm
            ; t6 := ex 15 8 rm
            ; t7 := ex 23 16 rm
            ; t8 := hi 8 rm
            ; t9 := se 32 t1 - se 32 t5
            ; t10 := se 32 t2 - se 32 t6
            ; t11 := se 32 t3 - se 32 t7
            ; t12 := se 32 t4 - se 32 t8
            ; t1 := ex 8 1 t9
            ; t2 := ex 8 1 t10
            ; t3 := ex 8 1 t11
            ; t4 := ex 8 1 t12
            ; rd := t4 @@ t3 @@ t2 @@ t1 ]
    | M.SHSUB16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := se 32 t1 - se 32 t3
            ; t6 := se 32 t2 - se 32 t4
            ; t1 := ex 16 1 t5
            ; t2 := ex 16 1 t6
            ; rd := t2 @@ t1 ]
    | M.SMC -> (
      match instr.operands.(0) with
      | O.Imm imm ->
          let imm = Int64.to_int_exn imm in
          Ok Il.[interrupt imm]
      | _ -> Error (`BadOperand 0) )
    | M.SMLABB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 32 (lo 16 rn)
            ; t2 := se 32 (lo 16 rm)
            ; t3 := se 64 ((t1 * t2) + ra)
            ; rd := lo 32 t3
            ; qf := qf || t3 <> se 64 rd ]
    | M.SMLABT ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 32 (lo 16 rn)
            ; t2 := se 32 (hi 16 rm)
            ; t3 := se 64 ((t1 * t2) + ra)
            ; rd := lo 32 t3
            ; qf := qf || t3 <> se 64 rd ]
    | M.SMLATB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 32 (hi 16 rn)
            ; t2 := se 32 (lo 16 rm)
            ; t3 := se 64 ((t1 * t2) + ra)
            ; rd := lo 32 t3
            ; qf := qf || t3 <> se 64 rd ]
    | M.SMLATT ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 32 (hi 16 rn)
            ; t2 := se 32 (hi 16 rm)
            ; t3 := se 64 ((t1 * t2) + ra)
            ; rd := lo 32 t3
            ; qf := qf || t3 <> se 64 rd ]
    | M.SMLAD ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t5 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 32 (lo 16 rn)
            ; t2 := se 32 (hi 16 rn)
            ; t3 := se 32 (lo 16 rm)
            ; t4 := se 32 (hi 16 rm)
            ; t5 := se 64 ((t1 * t3) + (t2 * t4) + ra)
            ; rd := lo 32 t5
            ; qf := qf || t5 <> se 64 rd ]
    | M.SMLADX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t5 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 32 (lo 16 rn)
            ; t2 := se 32 (hi 16 rn)
            ; t3 := se 32 (hi 16 rm)
            ; t4 := se 32 (lo 16 rm)
            ; t5 := se 64 ((t1 * t3) + (t2 * t4) + ra)
            ; rd := lo 32 t5
            ; qf := qf || t5 <> se 64 rd ]
    | M.SMLAL ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := rdhi @@ rdlo
            ; t4 := (t1 * t2) + t3
            ; rdhi := hi 32 t4
            ; rdlo := lo 32 t4 ]
    | M.SMLALS ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := rdhi @@ rdlo
            ; t4 := (t1 * t2) + t3
            ; rdhi := hi 32 t4
            ; rdlo := lo 32 t4
            ; nf := t4 <$ num 0 64
            ; zf := t4 = num 0 64 ]
    | M.SMLALBB ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (lo 16 rm)
            ; t3 := rdhi @@ rdlo
            ; t4 := (t1 * t2) + t3
            ; rdhi := hi 32 t4
            ; rdlo := lo 32 t4 ]
    | M.SMLALBT ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rm)
            ; t3 := rdhi @@ rdlo
            ; t4 := (t1 * t2) + t3
            ; rdhi := hi 32 t4
            ; rdlo := lo 32 t4 ]
    | M.SMLALTB ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (hi 16 rn)
            ; t2 := se 64 (lo 16 rm)
            ; t3 := rdhi @@ rdlo
            ; t4 := (t1 * t2) + t3
            ; rdhi := hi 32 t4
            ; rdlo := lo 32 t4 ]
    | M.SMLALTT ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (hi 16 rn)
            ; t2 := se 64 (hi 16 rm)
            ; t3 := rdhi @@ rdlo
            ; t4 := (t1 * t2) + t3
            ; rdhi := hi 32 t4
            ; rdlo := lo 32 t4 ]
    | M.SMLALD ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (lo 16 rm)
            ; t4 := se 64 (hi 16 rm)
            ; t5 := rdhi @@ rdlo
            ; t6 := (t1 * t3) + (t2 * t4) + t5
            ; rdhi := hi 32 t6
            ; rdlo := lo 32 t6 ]
    | M.SMLALDX ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (hi 16 rm)
            ; t4 := se 64 (lo 16 rm)
            ; t5 := rdhi @@ rdlo
            ; t6 := (t1 * t3) + (t2 * t4) + t5
            ; rdhi := hi 32 t6
            ; rdlo := lo 32 t6 ]
    | M.SMLAWB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rm)
            ; t2 := se 64 rn
            ; t3 := se 64 ra << num 16 64
            ; t4 := (t2 * t1) + t3
            ; rd := ex 47 16 t4
            ; qf := qf || t4 >> num 16 64 <> se 64 rd ]
    | M.SMLAWT ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (hi 16 rm)
            ; t2 := se 64 rn
            ; t3 := se 64 ra << num 16 64
            ; t4 := (t2 * t1) + t3
            ; rd := ex 47 16 t4
            ; qf := qf || t4 >> num 16 64 <> se 64 rd ]
    | M.SMLSD ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (lo 16 rm)
            ; t4 := se 64 (hi 16 rm)
            ; t5 := se 64 ra
            ; t6 := (t1 * t3) - (t2 * t4) + t5
            ; rd := lo 32 t6
            ; qf := qf || t6 <> se 64 rd ]
    | M.SMLSDX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (hi 16 rm)
            ; t4 := se 64 (lo 16 rm)
            ; t5 := se 64 ra
            ; t6 := (t1 * t3) - (t2 * t4) + t5
            ; rd := lo 32 t6
            ; qf := qf || t6 <> se 64 rd ]
    | M.SMLSLD ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (lo 16 rm)
            ; t4 := se 64 (hi 16 rm)
            ; t5 := rdhi @@ rdlo
            ; t6 := (t1 * t3) - (t2 * t4) + t5
            ; rdhi := hi 32 t6
            ; rdlo := lo 32 t6 ]
    | M.SMLSLDX ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (hi 16 rm)
            ; t4 := se 64 (lo 16 rm)
            ; t5 := rdhi @@ rdlo
            ; t6 := (t1 * t3) - (t2 * t4) + t5
            ; rdhi := hi 32 t6
            ; rdlo := lo 32 t6 ]
    | M.SMMLA ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := se 64 ra << num 32 64
            ; t4 := (t1 * t2) + t3
            ; rd := hi 32 t4 ]
    | M.SMMLAR ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := se 64 ra << num 32 64
            ; t4 := (t1 * t2) + t3 + num64 0x80000000L 64
            ; rd := hi 32 t4 ]
    | M.SMMLS ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := se 64 ra << num 32 64
            ; t4 := t3 - (t1 * t2)
            ; rd := hi 32 t4 ]
    | M.SMMLSR ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind ra = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := se 64 ra << num 32 64
            ; t4 := t3 - (t1 * t2) + num64 0x80000000L 64
            ; rd := hi 32 t4 ]
    | M.SMMUL ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok Il.[t1 := se 64 rn; t2 := se 64 rm; t3 := t1 * t2; rd := hi 32 t3]
    | M.SMMULR ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := (t1 * t2) + num64 0x80000000L 64
            ; rd := hi 32 t3 ]
    | M.SMUAD ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (lo 16 rm)
            ; t4 := se 64 (hi 16 rm)
            ; t5 := (t1 * t3) + (t2 * t4)
            ; rd := lo 32 t5
            ; qf := qf || t5 <> se 64 rd ]
    | M.SMUADX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (hi 16 rm)
            ; t4 := se 64 (lo 16 rm)
            ; t5 := (t1 * t3) + (t2 * t4)
            ; rd := lo 32 t5
            ; qf := qf || t5 <> se 64 rd ]
    | M.SMULBB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (lo 16 rm)
            ; t3 := t1 * t2
            ; rd := lo 32 t3 ]
    | M.SMULBT ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rm)
            ; t3 := t1 * t2
            ; rd := lo 32 t3 ]
    | M.SMULTB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (hi 16 rn)
            ; t2 := se 64 (lo 16 rm)
            ; t3 := t1 * t2
            ; rd := lo 32 t3 ]
    | M.SMULTT ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (hi 16 rn)
            ; t2 := se 64 (hi 16 rm)
            ; t3 := t1 * t2
            ; rd := lo 32 t3 ]
    | M.SMULL ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := t1 * t2
            ; rdhi := hi 32 t3
            ; rdlo := lo 32 t3 ]
    | M.SMULLS ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 rm
            ; t3 := t1 * t2
            ; rdhi := hi 32 t3
            ; rdlo := lo 32 t3
            ; nf := t3 <$ num 0 64
            ; zf := t3 = num 0 64 ]
    | M.SMULWB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 (lo 16 rm)
            ; t3 := t1 * t2
            ; rd := ex 47 16 t3 ]
    | M.SMULWT ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rn
            ; t2 := se 64 (hi 16 rm)
            ; t3 := t1 * t2
            ; rd := ex 47 16 t3 ]
    | M.SMUSD ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (lo 16 rm)
            ; t4 := se 64 (hi 16 rm)
            ; t5 := (t1 * t3) - (t2 * t4)
            ; rd := lo 32 t5 ]
    | M.SMUSDX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        let t5 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 (lo 16 rn)
            ; t2 := se 64 (hi 16 rn)
            ; t3 := se 64 (hi 16 rm)
            ; t4 := se 64 (lo 16 rm)
            ; t5 := (t1 * t3) - (t2 * t4)
            ; rd := lo 32 t5 ]
    | M.SSAX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        let t7 = new_tmp 16 in
        let t8 = new_tmp 16 in
        Ok
          Il.
            [ t1 := se 32 (lo 16 rn)
            ; t2 := se 32 (hi 16 rn)
            ; t3 := se 32 (lo 16 rm)
            ; t4 := se 32 (hi 16 rm)
            ; t5 := t1 + t4
            ; t6 := t2 - t3
            ; t7 := lo 16 t5
            ; t8 := lo 16 t6
            ; rd := t8 @@ t7
            ; ge0 := t5 >=$ num 0 32
            ; ge1 := ge0
            ; ge2 := t6 >=$ num 0 32
            ; ge3 := ge2 ]
    | M.SSUB8 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        let t9 = new_tmp 32 in
        let t10 = new_tmp 32 in
        let t11 = new_tmp 32 in
        let t12 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 8 rn
            ; t2 := ex 15 8 rn
            ; t3 := ex 23 16 rn
            ; t4 := hi 8 rn
            ; t5 := lo 8 rm
            ; t6 := ex 15 8 rm
            ; t7 := ex 23 16 rm
            ; t8 := hi 8 rm
            ; t9 := se 32 t1 - se 32 t5
            ; t10 := se 32 t2 - se 32 t6
            ; t11 := se 32 t3 - se 32 t7
            ; t12 := se 32 t4 - se 32 t8
            ; t1 := lo 8 t9
            ; t2 := lo 8 t10
            ; t3 := lo 8 t11
            ; t4 := lo 8 t12
            ; rd := t4 @@ t3 @@ t2 @@ t1
            ; ge0 := t9 >=$ num 0 32
            ; ge1 := t10 >=$ num 0 32
            ; ge2 := t11 >=$ num 0 32
            ; ge3 := t12 >=$ num 0 32 ]
    | M.SSUB16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := se 32 t1 - se 32 t3
            ; t6 := se 32 t2 - se 32 t4
            ; t1 := lo 16 t5
            ; t2 := lo 16 t6
            ; rd := t2 @@ t1
            ; ge0 := t5 >=$ num 0 32
            ; ge1 := ge0
            ; ge2 := t6 >=$ num 0 32
            ; ge3 := ge2 ]
    | M.STL ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 32 addr in
        Ok (assign_mem m rt :: stmts)
    | M.STLB ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 8 addr in
        Ok (assign_mem m Il.(lo 8 rt) :: stmts)
    | M.STLH ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 16 addr in
        Ok (assign_mem m Il.(lo 16 rt) :: stmts)
    | M.STM | M.STMIA | M.STMEA ->
        let%bind rn = op_reg instr 0 in
        let%bind rl = op_reglist instr 1 in
        let wback =
          match instr.write_back with
          | Some true -> true
          | _ -> false
        in
        let stmts =
          List.mapi rl ~f:(fun i r ->
              let loc = Il.(rn + num Int.(i lsl 2) 32) in
              let ld = Il.(load instr.endian 4 mu loc) in
              if wback && i > 0 && Il.equal_expr r rn then
                assign_mem ld Il.(undefined 32)
              else assign_mem ld r )
        in
        if not wback then Ok stmts
        else
          let n = List.length rl in
          Ok (stmts @ Il.[rn := rn + num Int.(n * 4) 32])
    | M.STMDA ->
        let%bind rn = op_reg instr 0 in
        let%bind rl = op_reglist instr 1 in
        let wback =
          match instr.write_back with
          | Some true -> true
          | _ -> false
        in
        let n = List.length rl in
        let stmts =
          let disp = (n - 1) lsl 2 in
          List.mapi rl ~f:(fun i r ->
              let off = disp - (i lsl 2) in
              let loc = Il.(rn - num off 32) in
              let ld = Il.(load instr.endian 4 mu loc) in
              if wback && i > 0 && Il.equal_expr r rn then
                assign_mem ld Il.(undefined 32)
              else assign_mem ld r )
        in
        if not wback then Ok stmts
        else Ok (stmts @ Il.[rn := rn - num Int.(n * 4) 32])
    | M.STMDB ->
        let%bind rn = op_reg instr 0 in
        let%bind rl = op_reglist instr 1 in
        let wback =
          match instr.write_back with
          | Some true -> true
          | _ -> false
        in
        let n = List.length rl in
        let stmts =
          let disp = n lsl 2 in
          List.mapi rl ~f:(fun i r ->
              let off = disp - (i lsl 2) in
              let loc = Il.(rn - num off 32) in
              let ld = Il.(load instr.endian 4 mu loc) in
              if wback && i > 0 && Il.equal_expr r rn then
                assign_mem ld Il.(undefined 32)
              else assign_mem ld r )
        in
        if not wback then Ok stmts
        else Ok (stmts @ Il.[rn := rn - num Int.(n * 4) 32])
    | M.STMIB ->
        let%bind rn = op_reg instr 0 in
        let%bind rl = op_reglist instr 1 in
        let wback =
          match instr.write_back with
          | Some true -> true
          | _ -> false
        in
        let stmts =
          List.mapi rl ~f:(fun i r ->
              let loc = Il.(rn + num Int.((i + 1) lsl 2) 32) in
              let ld = Il.(load instr.endian 4 mu loc) in
              if wback && i > 0 && Il.equal_expr r rn then
                assign_mem ld Il.(undefined 32)
              else assign_mem ld r )
        in
        if not wback then Ok stmts
        else
          let n = List.length rl in
          Ok (stmts @ Il.[rn := rn + num Int.(n * 4) 32])
    | M.STR | M.STRT ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 32 addr in
        Ok (assign_mem m rt :: stmts)
    | M.STRB | M.STRBT ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 8 addr in
        Ok (assign_mem m Il.(lo 8 rt) :: stmts)
    | M.STRD ->
        let%bind rt = op_reg instr 0 in
        let%bind rt2 = op_reg instr 1 in
        let%bind m, stmts = op_mem instr 2 64 addr in
        let t1 = new_tmp 64 in
        Ok
          (( match instr.endian with
           | `LE -> Il.(t1 := rt2 @@ rt)
           | `BE -> Il.(t1 := rt @@ rt2) )
           :: assign_mem m t1 :: stmts )
    | M.STRH | M.STRHT ->
        let%bind rt = op_reg instr 0 in
        let%bind m, stmts = op_mem instr 1 16 addr in
        Ok (assign_mem m Il.(lo 16 rt) :: stmts)
    | M.SUB when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        Ok Il.[rdn := rdn - rm_or_imm]
    | (M.SUB | M.SUBW) when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        Ok Il.[rd := rn - rm_or_imm]
    | M.SUB when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        Ok Il.[rd := rn - sh]
    | M.SUBS when Array.length instr.operands = 2 ->
        let%bind rdn = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        let is_imm =
          match instr.operands.(1) with
          | O.Imm _ -> true
          | _ -> false
        in
        if (not is_imm) && Il.equal_expr rdn pc then
          Ok Il.[rdn := rdn - rm_or_imm]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := rdn - rm_or_imm
              ; cf := rdn >= rm_or_imm
              ; nf := rdn <$ rm_or_imm
              ; zf := rdn = rm_or_imm
              ; vf := hi 1 (~~(rdn ^ rm_or_imm) & (rdn ^ t1))
              ; rdn := t1 ]
    | M.SUBS when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm_or_imm = op_reg_or_imm instr 2 32 in
        if is_pc rd then Ok Il.[rd := rn + rm_or_imm]
        else
          let t1 = new_tmp 32 in
          Ok
            Il.
              [ t1 := rn - rm_or_imm
              ; cf := rn >= rm_or_imm
              ; nf := rn <$ rm_or_imm
              ; zf := rn = rm_or_imm
              ; vf := hi 1 (~~(rn ^ rm_or_imm) & (rn ^ t1))
              ; rd := t1 ]
    | M.SUBS when Array.length instr.operands = 4 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift_or_reg_shift instr 3 32 ~lhs:rm in
        let is_reg_shift =
          match instr.operands.(3) with
          | O.Reg_shift _ -> true
          | _ -> false
        in
        if (not is_reg_shift) && is_pc rd then Ok Il.[rd := rn - sh]
        else
          let t1 = new_tmp 32 in
          let t2 = new_tmp 32 in
          Ok
            Il.
              [ t2 := sh
              ; t1 := rn - t2
              ; cf := rn >= t2
              ; nf := rn <$ t2
              ; zf := rn = t2
              ; vf := hi 1 (~~(rn ^ t2) & (rn ^ t1))
              ; rd := t1 ]
    | M.SVC -> (
      match instr.operands.(0) with
      | O.Imm imm ->
          let imm = Int64.to_int_exn imm in
          Ok Il.[interrupt imm]
      | _ -> Error (`BadOperand 0) )
    | M.SXTAB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := rn + se 32 (lo 8 t1)]
    | M.SXTAB16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        Ok
          Il.
            [ t1 := sh
            ; t2 := lo 16 rn + se 16 (lo 8 t1)
            ; t3 := hi 16 rn + se 16 (ex 23 16 t1)
            ; rd := t3 @@ t2 ]
    | M.SXTAH ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := rn + se 32 (lo 16 t1)]
    | M.SXTB when Array.length instr.operands = 2 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rd := se 32 (lo 8 rm)]
    | M.SXTB when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift instr 2 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := se 32 (lo 8 t1)]
    | M.SXTB16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift instr 2 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        Ok
          Il.
            [ t1 := sh
            ; t2 := se 16 (lo 8 t1)
            ; t3 := se 16 (ex 23 16 t1)
            ; rd := t3 @@ t2 ]
    | M.SXTB when Array.length instr.operands = 2 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rd := se 32 (lo 16 rm)]
    | M.SXTH when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift instr 2 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := se 32 (lo 16 t1)]
    | M.TBB ->
        let%bind m = op_mem_off instr 0 8 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := m
            ; t2 := ze 32 t1
            ; write_pc instr.mode addr (t2 * num 2 32) ~relative:true ]
    | M.TBH ->
        let%bind m = op_mem_off instr 0 16 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := m
            ; t2 := ze 32 t1
            ; write_pc instr.mode addr (t2 * num 2 32) ~relative:true ]
    | M.TEQ when Array.length instr.operands = 2 ->
        let%bind rn = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        let t1 = new_tmp 32 in
        Ok
          Il.
            [ t1 := rn || rm_or_imm
            ; nf := t1 <$ num 0 32
            ; zf := t1 = num 0 32
            ; cf := new_cf () ]
    | M.TEQ when Array.length instr.operands = 3 ->
        let%bind rn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh, carry, stmts =
          op_shift_or_reg_shift_c instr 2 32 ~lhs:rm
        in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          ( Il.
              [ t2 := sh
              ; t1 := rn || sh
              ; nf := t1 <$ num 0 32
              ; zf := t1 = num 0 32 ]
          @ stmts
          @ Il.[cf := carry] )
    | M.TST when Array.length instr.operands = 2 ->
        let%bind rn = op_reg instr 0 in
        let%bind rm_or_imm = op_reg_or_imm instr 1 32 in
        let t1 = new_tmp 32 in
        Ok
          Il.
            [ t1 := rn & rm_or_imm
            ; nf := t1 <$ num 0 32
            ; zf := t1 = num 0 32
            ; cf := new_cf () ]
    | M.TST when Array.length instr.operands = 3 ->
        let%bind rn = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh, carry, stmts =
          op_shift_or_reg_shift_c instr 2 32 ~lhs:rm
        in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          ( Il.
              [ t2 := sh
              ; t1 := rn & sh
              ; nf := t1 <$ num 0 32
              ; zf := t1 = num 0 32 ]
          @ stmts
          @ Il.[cf := carry] )
    | M.UADD8 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        let t9 = new_tmp 32 in
        let t10 = new_tmp 32 in
        let t11 = new_tmp 32 in
        let t12 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 8 rn
            ; t2 := ex 15 8 rn
            ; t3 := ex 23 16 rn
            ; t4 := hi 8 rn
            ; t5 := lo 8 rm
            ; t6 := ex 15 8 rm
            ; t7 := ex 23 16 rm
            ; t8 := hi 8 rm
            ; t9 := ze 32 t1 + ze 32 t5
            ; t10 := ze 32 t2 + ze 32 t6
            ; t11 := ze 32 t3 + ze 32 t7
            ; t12 := ze 32 t4 + ze 32 t8
            ; t1 := lo 8 t9
            ; t2 := lo 8 t10
            ; t3 := lo 8 t11
            ; t4 := lo 8 t12
            ; rd := t4 @@ t3 @@ t2 @@ t1
            ; ge0 := t9 >= num 0x100 32
            ; ge1 := t10 >= num 0x100 32
            ; ge2 := t11 >= num 0x100 32
            ; ge3 := t12 >= num 0x100 32 ]
    | M.UADD16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := ze 32 t1 + ze 32 t3
            ; t6 := ze 32 t2 + ze 32 t4
            ; t1 := lo 16 t5
            ; t2 := lo 16 t6
            ; rd := t2 @@ t1
            ; ge0 := t5 >= num 0x10000 32
            ; ge1 := ge0
            ; ge2 := t6 >= num 0x10000 32
            ; ge3 := ge2 ]
    | M.UASX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := ze 32 t1 - ze 32 t4
            ; t6 := ze 32 t2 + ze 32 t3
            ; t1 := lo 16 t5
            ; t2 := lo 16 t6
            ; rd := t2 @@ t1
            ; ge0 := t5 >=$ num 0 32
            ; ge1 := ge0
            ; ge2 := t6 >= num 0x10000 32
            ; ge3 := ge2 ]
    | M.UBFX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind lsb = op_imm instr 2 32 in
        let%bind width = op_imm instr 3 32 in
        let%bind low =
          match lsb with
          | Il.Const c -> Ok (Bitvec.to_int c.value)
          | _ -> Error (`BadOperand 3)
        in
        let%bind high =
          match width with
          | Il.Const c -> Ok (low + Bitvec.to_int c.value - 1)
          | _ -> Error (`BadOperand 3)
        in
        Ok Il.[rd := ze 32 (ex high low rn)]
    | M.UDF -> Ok Il.[exn ()]
    | M.UDIV ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ goto_if (rm = num 0 32) l1 l2
            ; ( @ ) l1
            ; (* TODO: generate exn? *)
              rd := num 0 32
            ; jmp (bitv endaddr 32)
            ; ( @ ) l2
            ; rd := rn / rm ]
    | M.UHADD8 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        let t9 = new_tmp 32 in
        let t10 = new_tmp 32 in
        let t11 = new_tmp 32 in
        let t12 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 8 rn
            ; t2 := ex 15 8 rn
            ; t3 := ex 23 16 rn
            ; t4 := hi 8 rn
            ; t5 := lo 8 rm
            ; t6 := ex 15 8 rm
            ; t7 := ex 23 16 rm
            ; t8 := hi 8 rm
            ; t9 := ze 32 t1 + ze 32 t5
            ; t10 := ze 32 t2 + ze 32 t6
            ; t11 := ze 32 t3 + ze 32 t7
            ; t12 := ze 32 t4 + ze 32 t8
            ; t1 := ex 8 1 t9
            ; t2 := ex 8 1 t10
            ; t3 := ex 8 1 t11
            ; t4 := ex 8 1 t12
            ; rd := t4 @@ t3 @@ t2 @@ t1 ]
    | M.UHADD16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := ze 32 t1 + ze 32 t3
            ; t6 := ze 32 t2 + ze 32 t4
            ; t1 := ex 16 1 t5
            ; t2 := ex 16 1 t6
            ; rd := t2 @@ t1 ]
    | M.UHASX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := ze 32 t1 - ze 32 t4
            ; t6 := ze 32 t2 + ze 32 t3
            ; t1 := ex 16 1 t5
            ; t2 := ex 16 1 t6
            ; rd := t2 @@ t1 ]
    | M.UHSAX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := ze 32 t1 + ze 32 t4
            ; t6 := ze 32 t2 - ze 32 t3
            ; t1 := ex 16 1 t5
            ; t2 := ex 16 1 t6
            ; rd := t2 @@ t1 ]
    | M.UHSUB8 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        let t9 = new_tmp 32 in
        let t10 = new_tmp 32 in
        let t11 = new_tmp 32 in
        let t12 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 8 rn
            ; t2 := ex 15 8 rn
            ; t3 := ex 23 16 rn
            ; t4 := hi 8 rn
            ; t5 := lo 8 rm
            ; t6 := ex 15 8 rm
            ; t7 := ex 23 16 rm
            ; t8 := hi 8 rm
            ; t9 := ze 32 t1 - ze 32 t5
            ; t10 := ze 32 t2 - ze 32 t6
            ; t11 := ze 32 t3 - ze 32 t7
            ; t12 := ze 32 t4 - ze 32 t8
            ; t1 := ex 8 1 t9
            ; t2 := ex 8 1 t10
            ; t3 := ex 8 1 t11
            ; t4 := ex 8 1 t12
            ; rd := t4 @@ t3 @@ t2 @@ t1 ]
    | M.UHSUB16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 16 rn
            ; t2 := hi 16 rn
            ; t3 := lo 16 rm
            ; t4 := hi 16 rm
            ; t5 := ze 32 t1 - ze 32 t3
            ; t6 := ze 32 t2 - ze 32 t4
            ; t1 := ex 16 1 t5
            ; t2 := ex 16 1 t6
            ; rd := t2 @@ t1 ]
    | M.UMAAL ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        Ok
          Il.
            [ t1 := (ze 64 rn * ze 64 rm) + ze 64 rdhi + ze 64 rdlo
            ; rdhi := hi 32 t1
            ; rdlo := lo 32 t1 ]
    | M.UMLAL ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := ze 64 rn
            ; t2 := ze 64 rm
            ; t3 := rdhi @@ rdlo
            ; t4 := (t1 * t2) + t3
            ; rdhi := hi 32 t4
            ; rdlo := lo 32 t4 ]
    | M.UMLALS ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let t4 = new_tmp 64 in
        Ok
          Il.
            [ t1 := ze 64 rn
            ; t2 := ze 64 rm
            ; t3 := rdhi @@ rdlo
            ; t4 := (t1 * t2) + t3
            ; rdhi := hi 32 t4
            ; rdlo := lo 32 t4
            ; nf := t4 <$ num 0 64
            ; zf := t4 = num 0 64 ]
    | M.UMULL ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := ze 64 rn * ze 64 rm; rdhi := hi 32 t1; rdlo := lo 32 t1]
    | M.UMULLS ->
        let%bind rdlo = op_reg instr 0 in
        let%bind rdhi = op_reg instr 1 in
        let%bind rn = op_reg instr 2 in
        let%bind rm = op_reg instr 3 in
        let t1 = new_tmp 64 in
        Ok
          Il.
            [ t1 := ze 64 rn * ze 64 rm
            ; rdhi := hi 32 t1
            ; rdlo := lo 32 t1
            ; nf := t1 <$ num 0 32
            ; zf := t1 = num 0 64 ]
    | M.USAX ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t5 = new_tmp 32 in
        let t6 = new_tmp 32 in
        let t7 = new_tmp 16 in
        let t8 = new_tmp 16 in
        Ok
          Il.
            [ t1 := ze 32 (lo 16 rn)
            ; t2 := ze 32 (hi 16 rn)
            ; t3 := ze 32 (lo 16 rm)
            ; t4 := ze 32 (hi 16 rm)
            ; t5 := t1 + t4
            ; t6 := t2 - t3
            ; t7 := lo 16 t5
            ; t8 := lo 16 t6
            ; rd := t8 @@ t7
            ; ge0 := t5 >= num 0x10000 32
            ; ge1 := ge0
            ; ge2 := t6 >=$ num 0 32
            ; ge3 := ge2 ]
    | M.UXTAB ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := rn + ze 32 (lo 8 t1)]
    | M.UXTAB16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        Ok
          Il.
            [ t1 := sh
            ; t2 := lo 16 rn + ze 16 (lo 8 t1)
            ; t3 := hi 16 rn + ze 16 (ex 23 16 t1)
            ; rd := t3 @@ t2 ]
    | M.UXTAH ->
        let%bind rd = op_reg instr 0 in
        let%bind rn = op_reg instr 1 in
        let%bind rm = op_reg instr 2 in
        let%bind sh = op_shift instr 3 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := rn + ze 32 (lo 16 t1)]
    | M.UXTB when Array.length instr.operands = 2 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rd := ze 32 (lo 8 rm)]
    | M.UXTB when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift instr 2 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := ze 32 (lo 8 t1)]
    | M.UXTB16 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift instr 2 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        Ok
          Il.
            [ t1 := sh
            ; t2 := ze 16 (lo 8 t1)
            ; t3 := ze 16 (ex 23 16 t1)
            ; rd := t3 @@ t2 ]
    | M.UXTH when Array.length instr.operands = 2 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        Ok Il.[rd := ze 32 (lo 16 rm)]
    | M.UXTH when Array.length instr.operands = 3 ->
        let%bind rd = op_reg instr 0 in
        let%bind rm = op_reg instr 1 in
        let%bind sh = op_shift instr 2 32 ~lhs:rm in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := sh; rd := ze 32 (lo 16 t1)]
    | _ -> Ok Il.[unk ()]
  in
  let stmts = init_stmts @ stmts in
  let%map stmts = replace_pc instr addr ctx stmts in
  Il.fold_constants_in_stmts stmts
  |> Il.normalize_end endaddr Mode.word_size ctx
  |> Il.normalize_conditions ctx
  |> Il.remove_identity_assigns
  |> (fun stmts -> Il.Context.reset_tmp ctx; stmts)
  |> Il.make_blocks

let lift_exn instr addr ctx =
  match lift instr addr ctx with
  | Ok il -> il
  | Error err -> raise (Arm_lift_error err)
