(* this code is borrowed from the B2R2 project *)

open Core_kernel
open Ll2l_std

module Psr_flag = struct
  type t =
    | C
    | X
    | XC
    | S
    | SC
    | SX
    | SXC
    | F
    | FC
    | FX
    | FXC
    | FS
    | FSC
    | FSX
    | FSXC
    | NZCV
    | NZCVQ
    | G
    | NZCVQG
  [@@deriving equal]

  let to_string = function
    | C -> "C"
    | X -> "X"
    | XC -> "XC"
    | S -> "S"
    | SC -> "SC"
    | SX -> "SX"
    | SXC -> "SXC"
    | F -> "F"
    | FC -> "FC"
    | FX -> "FX"
    | FXC -> "FXC"
    | FS -> "FS"
    | FSC -> "FSC"
    | FSX -> "FSX"
    | FSXC -> "FSXC"
    | NZCV -> "NZCV"
    | NZCVQ -> "NZCVQ"
    | G -> "G"
    | NZCVQG -> "NZCVQG"
end

module Simd = struct
  module Reg = struct
    type t =
      | Vector of Arm_register.t
      | Scalar of Arm_register.t * int option
    [@@deriving equal]

    let to_string ~write_back = function
      | Vector r -> Arm_register.to_string r ~write_back
      | Scalar (r, None) ->
          Printf.sprintf "%s[]" (Arm_register.to_string r ~write_back)
      | Scalar (r, Some s) ->
          Printf.sprintf "%s[%d]" (Arm_register.to_string r ~write_back) s
  end

  type t = Single of Reg.t | Regs of Reg.t list [@@deriving equal]

  let to_string ~write_back = function
    | Single r -> Reg.to_string r ~write_back
    | Regs rs ->
        List.map rs ~f:(Reg.to_string ~write_back)
        |> String.concat ~sep:", " |> Printf.sprintf "{%s}"
end

module Shift_kind = struct
  type t = LSL | LSR | ASR | ROR | RRX [@@deriving equal]

  let to_string = function
    | LSL -> "LSL"
    | LSR -> "LSR"
    | ASR -> "ASR"
    | ROR -> "ROR"
    | RRX -> "RRX"
end

type shift = Shift_kind.t * int [@@deriving equal]

let string_of_shift : shift -> string = function
  | _, 0 -> ""
  | s, i -> Printf.sprintf "%s #%d" (Shift_kind.to_string s) i

module Memory = struct
  module Offset = struct
    type t =
      | Imm of {reg: Arm_register.t; neg: bool; imm: int64 option}
      | Reg of
          { reg: Arm_register.t
          ; neg: bool
          ; reg_off: Arm_register.t
          ; shift: shift option }
      | Align of
          { reg: Arm_register.t
          ; align: int64 option
          ; reg_align: Arm_register.t option }
    [@@deriving equal]
  end

  type t =
    | Off of Offset.t
    | Preindex of Offset.t
    | Postindex of Offset.t
    | Unindex of Arm_register.t * int64
    | Label of int64
  [@@deriving equal]

  let to_string mem ~rel ~write_back =
    let offset_str ?(post = false) = function
      | Offset.Imm {reg; neg; imm= None} ->
          Arm_register.to_string reg ~write_back
      | Offset.Imm {reg; neg; imm= Some imm} ->
          Printf.sprintf "%s%s #%s0x%LX"
            (Arm_register.to_string reg ~write_back)
            (if post then "]," else ",")
            (if neg then "-" else "")
            imm
      | Offset.Reg {reg; neg; reg_off; shift= None} ->
          Printf.sprintf "%s%s %s%s"
            (Arm_register.to_string reg ~write_back)
            (if post then "]," else ",")
            (if neg then "-" else "")
            (Arm_register.to_string reg_off ~write_back)
      | Offset.Reg {reg; neg; reg_off; shift= Some s} ->
          let shift_str =
            match string_of_shift s with
            | "" -> ""
            | s -> ", " ^ s
          in
          Printf.sprintf "%s%s %s%s%s"
            (Arm_register.to_string reg ~write_back)
            (if post then "]," else ",")
            (if neg then "-" else "")
            (Arm_register.to_string reg_off ~write_back)
            shift_str
      | Offset.Align {reg; align= None; reg_align= None} ->
          Arm_register.to_string reg ~write_back
      | Offset.Align {reg; align= Some a; reg_align= None} ->
          Printf.sprintf "%s:%Lu" (Arm_register.to_string reg ~write_back) a
      | Offset.Align {reg; align= None; reg_align= Some r} ->
          Printf.sprintf "%s], %s"
            (Arm_register.to_string reg ~write_back)
            (Arm_register.to_string r ~write_back)
      | Offset.Align {reg; align= Some a; reg_align= Some r} ->
          Printf.sprintf "%s:%Lu], %s"
            (Arm_register.to_string reg ~write_back)
            a
            (Arm_register.to_string r ~write_back)
    in
    match mem with
    | Off off -> Printf.sprintf "[%s]" (offset_str off)
    | Preindex off -> Printf.sprintf "[%s]!" (offset_str off)
    | Postindex off -> Printf.sprintf "[%s" (offset_str off ~post:true)
    | Unindex (r, opt) ->
        Printf.sprintf "[%s], {0x%LX}"
          (Arm_register.to_string r ~write_back)
          opt
    | Label l ->
        let addr, pc, align_pc = rel in
        let addr = if not align_pc then pc else Addr.(pc / int 4 * int 4) in
        let addr = Addr.to_int64 addr in
        Printf.sprintf "=0x%08LX" Int64.((addr + l) land 0xFFFFFFFEL)
end

module Option_op = struct
  type t =
    | SY
    | ST
    | LD
    | ISH
    | ISHST
    | ISHLD
    | NSH
    | NSHST
    | NSHLD
    | OSH
    | OSHST
    | OSHLD
  [@@deriving equal]

  let to_string = function
    | SY -> "SY"
    | ST -> "ST"
    | LD -> "LD"
    | ISH -> "ISH"
    | ISHST -> "ISHST"
    | ISHLD -> "ISHLD"
    | NSH -> "NSH"
    | NSHST -> "NSHST"
    | NSHLD -> "NSHLD"
    | OSH -> "OSH"
    | OSHST -> "OSHST"
    | OSHLD -> "OSHLD"
end

module Iflag = struct
  type t = A | I | F | AI | AF | IF | AIF [@@deriving equal]

  let to_string = function
    | A -> "A"
    | I -> "I"
    | F -> "F"
    | AI -> "AI"
    | AF -> "AF"
    | IF -> "IF"
    | AIF -> "AIF"
end

type t =
  | Reg of Arm_register.t
  | Reg_special of Arm_register.t * Psr_flag.t option
  | Reg_list of Arm_register.t list
  | SIMD of Simd.t
  | Imm of int64
  | Fp_imm of float
  | Shift of shift
  | Reg_shift of Shift_kind.t * Arm_register.t
  | Mem of Memory.t
  | Opt of Option_op.t
  | Iflg of Iflag.t
  | Endian of Endian.t
  | Cond of Arm_condition.t
[@@deriving equal]

let to_string op ~rel ~write_back =
  match op with
  | Reg r | Reg_special (r, None) -> Arm_register.to_string r ~write_back
  | Reg_special (r, Some psr) ->
      Printf.sprintf "%s_%s"
        (Arm_register.to_string r ~write_back)
        (Psr_flag.to_string psr)
  | Reg_list rs ->
      List.map rs ~f:(Arm_register.to_string ~write_back)
      |> String.concat ~sep:", " |> Printf.sprintf "{%s}"
  | SIMD simd -> Simd.to_string ~write_back simd
  | Imm imm -> Printf.sprintf "#0x%LX" imm
  | Fp_imm imm -> Printf.sprintf "#%f" imm
  | Shift s -> string_of_shift s
  | Reg_shift (s, r) ->
      Printf.sprintf "%s %s" (Shift_kind.to_string s)
        (Arm_register.to_string r ~write_back)
  | Mem m -> Memory.to_string m ~rel ~write_back
  | Opt o -> Option_op.to_string o
  | Iflg i -> Iflag.to_string i
  | Endian `LE -> "LE"
  | Endian `BE -> "BE"
  | Cond c -> Arm_condition.to_string c
