open Base

type t =
  | EQ (* equal (Z == 1) *)
  | NE (* not equal (Z == 0) *)
  | CS (* carry set/greater than (C == 1) *)
  | HS (* unsigned higher or same (C == 1) *)
  | CC (* carry clear/less than (C == 0) *)
  | LO (* unsigned lower (C == 0) *)
  | MI (* minus/negative/less than (N == 1) *)
  | PL (* plus/positive or zero/greater than/equal/unordered (N == 0) *)
  | VS (* overflow/unordered (V == 1) *)
  | VC (* no overflow/not unordered (V == 0) *)
  | HI (* unsigned higher/greater than/unordered (C == 1 and Z == 0) *)
  | LS (* unsigned lower or same/less than or equal (C == 0 or Z == 1) *)
  | GE (* signed greater than or equal (N == V) *)
  | LT (* signed less than or unordered (N != V) *)
  | GT (* signed greater than (Z == 0 and N == V) *)
  | LE (* signed less than or equal/unordered (Z == 1 or N != V) *)
  | AL (* always (unconditional)/any *)
  | NV (* same as AL *)
  | UN
(* unconditional *) [@@deriving equal]

let is_unconditional = function
  | AL | NV | UN -> true
  | _ -> false

let to_string = function
  | EQ -> "EQ"
  | NE -> "NE"
  | CS -> "CS"
  | HS -> "HS"
  | CC -> "CC"
  | LO -> "LO"
  | MI -> "MI"
  | PL -> "PL"
  | VS -> "VS"
  | VC -> "VC"
  | HI -> "HI"
  | LS -> "LS"
  | GE -> "GE"
  | LT -> "LT"
  | GT -> "GT"
  | LE -> "LE"
  | AL -> ""
  | NV -> "NV"
  | UN -> ""
