open Core_kernel
open Ll2l_std

type error =
  [`OOB | `Invalid | `BadOperand | `Unpredictable | `Undefined | `Unimpl]

val string_of_error : error -> string

exception Arm_decode_error of error

module Context : sig
  type t

  val create : unit -> t

  val clone : t -> t
end

val decode :
     Context.t
  -> Arm.Mode.t
  -> Endian.t
  -> bytes
  -> (Arm.Instruction.t, error) Result.t

val decode_exn :
  Context.t -> Arm.Mode.t -> Endian.t -> bytes -> Arm.Instruction.t
