open Core_kernel
open Ll2l_ir
open Ll2l_std

type error = [`BadInstr | `BadOperand of int]

val string_of_error : error -> string

exception Arm_lift_error of error

val lift :
  Arm.Instruction.t -> Addr.t -> Il.Context.t -> (Il.t, error) Result.t

val lift_exn : Arm.Instruction.t -> Addr.t -> Il.Context.t -> Il.t
