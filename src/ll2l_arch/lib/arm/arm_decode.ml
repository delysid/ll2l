(* this code is borrowed from the B2R2 project *)

open Core_kernel
open Arm_helpers

type error =
  [`OOB | `Invalid | `BadOperand | `Unpredictable | `Undefined | `Unimpl]

let string_of_error = function
  | `OOB -> "out of bounds"
  | `Invalid -> "invalid"
  | `BadOperand -> "bad operand"
  | `Unpredictable -> "unpredictable"
  | `Undefined -> "undefined"
  | `Unimpl -> "unimplemented"

exception Arm_decode_error of error

let cf_thumb (b1, b2) =
  let imm1 = tst b1 10 |> Bool.to_int in
  let imm3 = ex b2 14 12 in
  let imm8 = ex b2 7 0 in
  let imm12 = concat (concat imm1 imm3 3) imm8 8 in
  let chk1 = ex imm12 11 10 in
  let chk2 = ex imm12 9 8 in
  match (chk1, chk2) with
  | 0b00, 0b00 | 0b00, 0b01 | 0b00, 0b10 | 0b00, 0b11 -> None
  | _ ->
      let imm7 = ex imm12 6 0 in
      let unrot = imm7 lor 0b10000000 in
      let amount = ex imm12 11 7 in
      let m = amount mod 32 in
      let result = (unrot lsl (32 - m)) lor (unrot lsr m) in
      Some (tst result 31)

let cf_arm b =
  let imm8 = ex b 7 0 in
  let imm5 = ex b 11 8 in
  let amount = 2 * imm5 in
  if amount = 0 then None
  else
    let result = (imm8 lsl (32 - amount)) lor (imm8 lsr amount) in
    Some (tst result 31)

module Context = struct
  type t = {mutable it: int list; mutable it_started: bool}

  let create () = {it= []; it_started= false}

  let clone ctx = {it= ctx.it; it_started= ctx.it_started}

  let update_it ctx =
    if ctx.it_started then ctx.it_started <- false
    else
      match ctx.it with
      | [] -> ()
      | _ :: tl -> ctx.it <- tl

  let cond_with_it ctx =
    match List.hd ctx.it with
    | Some st -> Result.(parse_cond st >>| fun cond -> Some cond)
    | _ -> Ok (Some C.AL)
end

module V7 = struct
  let parse_data_proc_reg b op =
    let open Result.Let_syntax in
    let chk_imm5 = ex b 11 7 = 0b00000 in
    match op with
    | 0b00000 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.AND, None, ops)
    | 0b00001 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ANDS, None, ops)
    | 0b00010 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.EOR, None, ops)
    | 0b00011 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.EORS, None, ops)
    | 0b00100 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.SUB, None, ops)
    | 0b00101 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.SUBS, None, ops)
    | 0b00110 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.RSB, None, ops)
    | 0b00111 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.RSBS, None, ops)
    | 0b01000 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ADD, None, ops)
    | 0b01001 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ADDS, None, ops)
    | 0b01010 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ADC, None, ops)
    | 0b01011 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ADCS, None, ops)
    | 0b01100 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.SBC, None, ops)
    | 0b01101 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.SBCS, None, ops)
    | 0b01110 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.RSC, None, ops)
    | 0b01111 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.RSCS, None, ops)
    | 0b10001 ->
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_b) in
        (M.TST, None, ops)
    | 0b10011 ->
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_b) in
        (M.TEQ, None, ops)
    | 0b10101 ->
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_b) in
        (M.CMP, None, ops)
    | 0b10111 ->
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_b) in
        (M.CMN, None, ops)
    | 0b11000 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ORR, None, ops)
    | 0b11001 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ORRS, None, ops)
    | 0b11010 when ex b 6 5 = 0b00 && chk_imm5 ->
        let%map ops = p2oprs b dummy_chk (reg_d, reg_a) in
        (M.MOV, None, ops)
    | 0b11010 when ex b 6 5 = 0b00 && not chk_imm5 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, imm_5a) in
        (M.LSL, None, ops)
    | 0b11011 when ex b 6 5 = 0b00 && chk_imm5 ->
        let%map ops = p2oprs b dummy_chk (reg_d, reg_a) in
        (M.MOVS, None, ops)
    | 0b11011 when ex b 6 5 = 0b00 && not chk_imm5 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, imm_5a) in
        (M.LSLS, None, ops)
    | 0b11010 when ex b 6 5 = 0b01 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, imm_5a) in
        (M.LSR, None, ops)
    | 0b11011 when ex b 6 5 = 0b01 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, imm_5a) in
        (M.LSRS, None, ops)
    | 0b11010 when ex b 6 5 = 0b10 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, imm_5a) in
        (M.ASR, None, ops)
    | 0b11011 when ex b 6 5 = 0b10 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, imm_5a) in
        (M.ASRS, None, ops)
    | 0b11010 when ex b 6 5 = 0b11 && chk_imm5 ->
        let%map ops = p2oprs b dummy_chk (reg_d, reg_a) in
        (M.RRX, None, ops)
    | 0b11010 when ex b 6 5 = 0b11 && not chk_imm5 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, imm_5a) in
        (M.ROR, None, ops)
    | 0b11011 when ex b 6 5 = 0b11 && chk_imm5 ->
        let%map ops = p2oprs b dummy_chk (reg_d, reg_a) in
        (M.RRXS, None, ops)
    | 0b11011 when ex b 6 5 = 0b11 && not chk_imm5 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, imm_5a) in
        (M.RORS, None, ops)
    | 0b11100 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.BIC, None, ops)
    | 0b11101 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.BICS, None, ops)
    | 0b11110 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_b) in
        (M.MVN, None, ops)
    | 0b11111 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_b) in
        (M.MVNS, None, ops)
    | _ -> Error `Invalid

  let parse_data_proc_reg_sreg b op =
    let open Result.Let_syntax in
    let chk = ex b 6 5 in
    match op with
    | 0b00000 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.AND, None, ops)
    | 0b00001 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ANDS, None, ops)
    | 0b00010 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.EOR, None, ops)
    | 0b00011 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.EORS, None, ops)
    | 0b00100 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.SUB, None, ops)
    | 0b00101 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.SUBS, None, ops)
    | 0b00110 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.RSB, None, ops)
    | 0b00111 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.RSBS, None, ops)
    | 0b01000 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ADD, None, ops)
    | 0b01001 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ADDS, None, ops)
    | 0b01010 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ADC, None, ops)
    | 0b01011 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ADCS, None, ops)
    | 0b01100 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.SBC, None, ops)
    | 0b01101 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.SBCS, None, ops)
    | 0b01110 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.RSC, None, ops)
    | 0b01111 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.RSCS, None, ops)
    | 0b10001 ->
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_a) in
        (M.TST, None, ops)
    | 0b10011 ->
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_a) in
        (M.TEQ, None, ops)
    | 0b10101 ->
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_a) in
        (M.CMP, None, ops)
    | 0b10111 ->
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_a) in
        (M.CMN, None, ops)
    | 0b11000 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ORR, None, ops)
    | 0b11001 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ORRS, None, ops)
    | 0b11010 when chk = 0b00 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
        (M.LSL, None, ops)
    | 0b11011 when chk = 0b00 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
        (M.LSLS, None, ops)
    | 0b11010 when chk = 0b01 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
        (M.LSR, None, ops)
    | 0b11011 when chk = 0b01 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
        (M.LSRS, None, ops)
    | 0b11010 when chk = 0b10 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
        (M.ASR, None, ops)
    | 0b11011 when chk = 0b10 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
        (M.ASRS, None, ops)
    | 0b11010 when chk = 0b11 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
        (M.ROR, None, ops)
    | 0b11011 when chk = 0b11 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
        (M.RORS, None, ops)
    | 0b11100 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.BIC, None, ops)
    | 0b11101 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.BICS, None, ops)
    | 0b11110 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_a) in
        (M.MVN, None, ops)
    | 0b11111 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_a) in
        (M.MVNS, None, ops)
    | _ -> Error `Invalid

  let parse_misc cond b =
    let open Result.Let_syntax in
    let chk = tst b 9 in
    let pick u q = Bool.equal (tst b u) q && not chk in
    match concat (ex b 6 4) (ex b 22 21) 2 with
    | 0b00000 when chk ->
        let%map ops = p2oprs b chk_g (reg_d, banked_reg_a) in
        (M.MRS, None, ops)
    | 0b00010 when chk ->
        let%map ops = p2oprs b chk_g (reg_d, banked_reg_a) in
        (M.MRS, None, ops)
    | 0b00001 when chk ->
        let%map ops = p2oprs b chk_f (banked_reg_a, reg_a) in
        (M.MSR, None, ops)
    | 0b00011 when chk ->
        let%map ops = p2oprs b chk_f (banked_reg_a, reg_a) in
        (M.MSR, None, ops)
    | 0b00000 | 0b00010 ->
        let%map ops = p2oprs b dummy_chk (reg_d, reg_e) in
        (M.MRS, None, ops)
    | 0b00001 when ex b 17 16 = 0 && not chk ->
        let%map ops = p2oprs b chk_f (apsr_xa, reg_a) in
        (M.MSR, None, ops)
    | 0b00001 when ex b 17 16 = 1 && not chk ->
        let%map ops = p2oprs b chk_h (reg_k, reg_a) in
        (M.MSR, None, ops)
    | 0b00001 when pick 17 true ->
        let%map ops = p2oprs b chk_h (reg_k, reg_a) in
        (M.MSR, None, ops)
    | 0b00011 ->
        let%map ops = p2oprs b chk_h (reg_k, reg_a) in
        (M.MSR, None, ops)
    | 0b00101 ->
        let%map ops = p1opr b dummy_chk reg_a in
        (M.BX, None, ops)
    | 0b00111 ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.CLZ, None, ops)
    | 0b01001 ->
        let%map ops = p1opr b chk_d reg_a in
        (M.BXJ, None, ops)
    | 0b01101 ->
        let%map ops = p1opr b chk_d reg_a in
        (M.BLX, None, ops)
    | 0b10100 ->
        let%map ops = p3oprs b chk_a (reg_d, reg_c, reg_a) in
        (M.QADD, None, ops)
    | 0b10101 ->
        let%map ops = p3oprs b chk_a (reg_d, reg_c, reg_a) in
        (M.QSUB, None, ops)
    | 0b10110 ->
        let%map ops = p3oprs b chk_a (reg_d, reg_c, reg_a) in
        (M.QDADD, None, ops)
    | 0b10111 ->
        let%map ops = p3oprs b chk_a (reg_d, reg_c, reg_a) in
        (M.QDSUB, None, ops)
    | 0b11101 when C.(equal cond AL) ->
        let%map ops = p1opr b dummy_chk imm_12d in
        (M.BKPT, None, ops)
    | 0b11111 ->
        let%map ops = p1opr b dummy_chk imm_4a in
        (M.SMC, None, ops)
    | _ -> Error `Invalid

  let parse_syn_primitives b =
    let open Result.Let_syntax in
    match ex b 23 20 with
    | 0b0000 ->
        let%map ops = p3oprs b chk_j (reg_d, reg_a, mem_a) in
        (M.SWP, None, ops)
    | 0b0100 ->
        let%map ops = p3oprs b chk_j (reg_d, reg_a, mem_a) in
        (M.SWPB, None, ops)
    | 0b1000 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STREX, None, ops)
    | 0b1001 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDREX, None, ops)
    | 0b1010 ->
        let%map ops = p4oprs b chk_store_ex2 (reg_d, reg_a, reg_f, mem_a) in
        (M.STREXD, None, ops)
    | 0b1011 ->
        let%map ops = p3oprs b chk_l (reg_d, reg_l, mem_a) in
        (M.LDREXD, None, ops)
    | 0b1100 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STREXB, None, ops)
    | 0b1110 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STREXH, None, ops)
    | 0b1111 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDREXH, None, ops)
    | _ -> Error `Invalid

  let parse_ex_load_store b =
    let open Result.Let_syntax in
    let rn = ex b 19 16 = 0b1111 in
    let mask = 0b1100101 in
    let unpriv = (not (tst b 24)) && tst b 21 in
    let wback = Some ((not (tst b 24)) || tst b 21) in
    match concat (ex b 6 5) (ex b 24 20) 5 with
    | o when o land mask = 0b0100000 ->
        if unpriv then
          let%map ops = p2oprs b chk_v (reg_d, mem_i) in
          (M.STRHT, None, ops)
        else
          let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
          (M.STRH, wback, ops)
    | o when o land mask = 0b0100001 ->
        if unpriv then
          let%map ops = p2oprs b chk_v (reg_d, mem_i) in
          (M.LDRHT, None, ops)
        else
          let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
          (M.LDRH, wback, ops)
    | o when o land mask = 0b0100100 ->
        if unpriv then
          let%map ops = p2oprs b chk_w (reg_d, mem_j) in
          (M.STRHT, None, ops)
        else
          let%map ops = p2oprs b chk_ad (reg_d, mem_o) in
          (M.STRH, wback, ops)
    | o when o land mask = 0b0100101 && rn ->
        if unpriv then
          let%map ops = p2oprs b chk_w (reg_d, mem_j) in
          (M.LDRHT, None, ops)
        else
          let%map ops = p2oprs b chk_t (reg_d, mem_h) in
          (M.LDRH, wback, ops)
    | o when o land mask = 0b0100101 ->
        if unpriv then
          let%map ops = p2oprs b chk_w (reg_d, mem_j) in
          (M.LDRHT, None, ops)
        else
          let%map ops = p2oprs b chk_ah (reg_d, mem_o) in
          (M.LDRH, None, ops)
    | o when o land mask = 0b1000000 ->
        let%map ops = p3oprs b chk_ae (reg_d, reg_l, mem_n) in
        (M.LDRD, wback, ops)
    | o when o land mask = 0b1000001 ->
        if unpriv then
          let%map ops = p2oprs b chk_v (reg_d, mem_i) in
          (M.LDRSBT, None, ops)
        else
          let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
          (M.LDRSB, wback, ops)
    | o when o land mask = 0b1000100 && rn ->
        let%map ops = p3oprs b chk_u (reg_d, reg_l, mem_h) in
        (M.LDRD, wback, ops)
    | o when o land mask = 0b1000100 ->
        let%map ops = p3oprs b chk_ai (reg_d, reg_l, mem_o) in
        (M.LDRD, None, ops)
    | o when o land mask = 0b1000101 && rn ->
        let%map ops = p2oprs b chk_g (reg_d, mem_h) in
        (M.LDRSB, None, ops)
    | o when o land mask = 0b1000101 ->
        if unpriv then
          let%map ops = p2oprs b chk_w (reg_d, mem_j) in
          (M.LDRSBT, None, ops)
        else
          let%map ops = p2oprs b chk_ah (reg_d, mem_o) in
          (M.LDRSB, wback, ops)
    | o when o land mask = 0b1100000 ->
        let%map ops = p3oprs b chk_af (reg_d, reg_l, mem_n) in
        (M.STRD, wback, ops)
    | o when o land mask = 0b1100001 ->
        if unpriv then
          let%map ops = p2oprs b chk_v (reg_d, mem_i) in
          (M.LDRSHT, None, ops)
        else
          let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
          (M.LDRSH, wback, ops)
    | o when o land mask = 0b1100100 ->
        let%map ops = p3oprs b chk_aj (reg_d, reg_l, mem_o) in
        (M.STRD, wback, ops)
    | o when o land mask = 0b1100101 && rn ->
        let%map ops = p2oprs b chk_g (reg_d, mem_h) in
        (M.LDRSH, None, ops)
    | o when o land mask = 0b1100101 ->
        if unpriv then
          let%map ops = p2oprs b chk_w (reg_d, mem_j) in
          (M.LDRSHT, None, ops)
        else
          let%map ops = p2oprs b chk_ah (reg_d, mem_o) in
          (M.LDRSH, wback, ops)
    | _ -> Error `Invalid

  let parse_ex_load_store_unpriv b =
    let open Result.Let_syntax in
    let chk22 = tst b 22 in
    let chk12 = tst b 12 in
    match concat (ex b 6 5) (tst b 20 |> Bool.to_int) 1 with
    | 0b010 when chk22 ->
        let%map ops = p2oprs b chk_v (reg_d, mem_i) in
        (M.STRHT, None, ops)
    | 0b101 when not chk22 ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.STRHT, None, ops)
    | 0b011 when chk22 ->
        let%map ops = p2oprs b chk_v (reg_d, mem_i) in
        (M.LDRHT, None, ops)
    | 0b011 when not chk22 ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.LDRHT, None, ops)
    | 0b100 when chk12 -> Error `Invalid
    | 0b100 when not chk12 -> Error `Invalid
    | 0b110 when chk12 -> Error `Invalid
    | 0b110 when not chk12 -> Error `Invalid
    | 0b101 when chk22 ->
        let%map ops = p2oprs b chk_v (reg_d, mem_i) in
        (M.LDRSBT, None, ops)
    | 0b101 when not chk22 ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.LDRSBT, None, ops)
    | 0b111 when chk22 ->
        let%map ops = p2oprs b chk_v (reg_d, mem_i) in
        (M.LDRSHT, None, ops)
    | 0b111 when not chk22 ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.LDRSHT, None, ops)
    | _ -> Error `Invalid

  let parse_group000 cond b =
    let open Result.Let_syntax in
    let o1 = ex b 24 20 in
    let o2 = ex b 7 4 in
    let is_data_proc = o1 land 0b11001 <> 0b10000 in
    let is_misc = o1 land 0b11001 = 0b10000 && o2 land 0b1000 = 0 in
    let is_halfword = o1 land 0b11001 = 0b10000 && o2 land 0b1001 = 0b1000 in
    let is_exload = o1 land 0b10010 <> 0b00010 in
    let is_exload_unpriv = o1 land 0b10010 = 0b00010 in
    let%map mnemonic, wback, ops =
      match (o1, o2) with
      | b1, 0b1001 when b1 land 0b10000 = 0 -> parse_mul_n_mul_acc b
      | b1, 0b1001 when b1 land 0b10000 = 0b10000 -> parse_syn_primitives b
      | _, 0b1011 when is_exload -> parse_ex_load_store b
      | _, 0b1101 when is_exload -> parse_ex_load_store b
      | _, 0b1111 when is_exload -> parse_ex_load_store b
      | _, 0b1011 when is_exload_unpriv -> parse_ex_load_store_unpriv b
      | _, 0b1101 when is_exload_unpriv -> parse_ex_load_store b
      | _, 0b1111 when is_exload_unpriv -> parse_ex_load_store b
      | _, x when is_data_proc && x land 1 = 0 -> parse_data_proc_reg b o1
      | _, x when is_data_proc && x land 0b1001 = 1 ->
          parse_data_proc_reg_sreg b o1
      | _ when is_misc -> parse_misc cond b
      | _ when is_halfword -> parse_half_mul_n_mul_acc b
      | _ -> Error `Invalid
    in
    (mnemonic, wback, None, ops)

  let msr_hints b =
    let open Result.Let_syntax in
    let op12 = concat (ex b 19 16) (ex b 7 0) 9 in
    let op = concat (tst b 22 |> Bool.to_int) op12 12 in
    match op with
    | 0b0000000000000 -> Ok (M.NOP, e)
    | 0b0000000000001 -> Ok (M.YIELD, e)
    | 0b0000000000010 -> Ok (M.WFE, e)
    | 0b0000000000011 -> Ok (M.WFI, e)
    | 0b0000000000100 -> Ok (M.SEV, e)
    | op when op land 0b1111111110000 = 0b0000011110000 ->
        let%map ops = p1opr b dummy_chk imm_4a in
        (M.DBG, ops)
    | op when op land 0b1111100000000 = 0b0010000000000 ->
        let%map ops = p2oprs b dummy_chk (apsr_xb, imm_12c) in
        (M.MSR, ops)
    | op when op land 0b1101100000000 = 0b0100000000000 ->
        let%map ops = p2oprs b dummy_chk (apsr_xb, imm_12c) in
        (M.MSR, ops)
    | op when op land 0b1001100000000 = 0b0000100000000 ->
        let%map ops = p2oprs b chk_x (reg_q, imm_12e) in
        (M.MSR, ops)
    | op when op land 0b1001000000000 = 0b0001000000000 ->
        let%map ops = p2oprs b chk_x (reg_q, imm_12e) in
        (M.MSR, ops)
    | op when op land 0b1000000000000 = 0b1000000000000 ->
        let%map ops = p2oprs b chk_x (reg_q, imm_12e) in
        (M.MSR, ops)
    | _ -> Error `Invalid

  let data_proc_imm op b =
    let open Result.Let_syntax in
    match op with
    | 0b00000 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.AND, ops, None)
    | 0b00001 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ANDS, ops, cf_arm b)
    | 0b00010 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.EOR, ops, None)
    | 0b00011 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.EORS, ops, cf_arm b)
    | 0b00100 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.SUB, ops, None)
    | 0b00101 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.SUBS, ops, None)
    | 0b00110 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.RSB, ops, None)
    | 0b00111 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.RSBS, ops, None)
    | 0b01000 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ADD, ops, None)
    | 0b01001 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ADDS, ops, None)
    | 0b01010 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ADC, ops, None)
    | 0b01011 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ADCS, ops, None)
    | 0b01100 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.SBC, ops, None)
    | 0b01101 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.SBCS, ops, None)
    | 0b01110 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.RSC, ops, None)
    | 0b01111 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.RSCS, ops, None)
    | 0b10001 ->
        let%map ops = p2oprs b dummy_chk (reg_c, imm_12a) in
        (M.TST, ops, cf_arm b)
    | 0b10011 ->
        let%map ops = p2oprs b dummy_chk (reg_c, imm_12a) in
        (M.TEQ, ops, cf_arm b)
    | 0b10101 ->
        let%map ops = p2oprs b dummy_chk (reg_c, imm_12a) in
        (M.CMP, ops, None)
    | 0b10111 ->
        let%map ops = p2oprs b dummy_chk (reg_c, imm_12a) in
        (M.CMN, ops, None)
    | 0b11000 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ORR, ops, None)
    | 0b11001 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ORRS, ops, cf_arm b)
    | 0b11010 ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MOV, ops, None)
    | 0b11011 ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MOVS, ops, cf_arm b)
    | 0b11100 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.BIC, ops, None)
    | 0b11101 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.BICS, ops, cf_arm b)
    | 0b11110 ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MVN, ops, None)
    | 0b11111 ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MVNS, ops, cf_arm b)
    | _ -> Error `Invalid

  let vmovvorr b =
    let open Result.Let_syntax in
    let n = tst b 7 in
    let m = tst b 5 in
    let vn = ex b 19 16 in
    let vm = ex b 3 0 in
    if Bool.equal n m && vn = vm then
      let%map ops = p2oprs b chk_undef_h (reg_x, reg_z) in
      (M.VMOV, ops)
    else
      let%map ops = p3oprs b chk_undef_d (reg_x, reg_y, reg_z) in
      (M.VORR, ops)

  let xyz_reg_oprs b chk_undef = p3oprs b chk_undef (reg_x, reg_y, reg_z)

  let xzy_reg_oprs b chk_undef = p3oprs b chk_undef (reg_x, reg_z, reg_y)

  let reg3_bitwise b k =
    let open Result.Let_syntax in
    match concat k (ex b 21 20) 2 with
    | 0b000 ->
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VAND, ops)
    | 0b001 ->
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VBIC, ops)
    | 0b010 -> vmovvorr b
    | 0b011 ->
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VORN, ops)
    | 0b100 ->
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VEOR, ops)
    | 0b101 ->
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VBSL, ops)
    | 0b110 ->
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VBIT, ops)
    | 0b111 ->
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VBIF, ops)
    | _ -> Error `Invalid

  let reg3_float b k =
    let open Result.Let_syntax in
    match (tst b 4, k, tst b 21) with
    | false, 0, false ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VADD, ops)
    | false, 0, true ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VSUB, ops)
    | false, 1, false ->
        let%map ops = xyz_reg_oprs b chk_undef_m in
        (M.VPADD, ops)
    | false, 1, true ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VABD, ops)
    | true, 0, false ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VMLA, ops)
    | true, 0, true ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VMLS, ops)
    | true, 1, false ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VMUL, ops)
    | _ -> Error `Invalid

  let reg3_compare b k =
    let open Result.Let_syntax in
    let%bind mnemonic =
      match (tst b 4, k, tst b 21) with
      | false, 0, false -> Ok M.VCEQ
      | false, 1, false -> Ok M.VCGE
      | false, 1, true -> Ok M.VCGT
      | true, 1, false -> Ok M.VACGE
      | true, 1, true -> Ok M.VACGT
      | _ -> Error `Invalid
    in
    let%bind ops = xyz_reg_oprs b chk_undef_l in
    let%map dt = one_dt_g b in
    (mnemonic, None, dt, ops)

  let reg3_max_min_n_reciprocal b k =
    let open Result.Let_syntax in
    let%bind dt = one_dt_g b in
    match (tst b 4, k, tst b 21) with
    | false, 0, false ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VMAX, None, dt, ops)
    | false, 0, true ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VMIN, None, dt, ops)
    | false, 1, false ->
        let%map ops = xyz_reg_oprs b chk_undef_m in
        (M.VPMAX, None, dt, ops)
    | false, 1, true ->
        let%map ops = xyz_reg_oprs b chk_undef_m in
        (M.VPMIN, None, dt, ops)
    | true, 0, false ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VRECPS, None, dt, ops)
    | true, 0, true ->
        let%map ops = xyz_reg_oprs b chk_undef_l in
        (M.VRSQRTS, None, dt, ops)
    | _ -> Error `Invalid

  let parse_3reg b k =
    let open Result.Let_syntax in
    let chku = k = 0 in
    let kb = if k = 0 then false else true in
    match concat (ex b 11 8) (tst b 4 |> Bool.to_int) 1 with
    | 0b00000 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VHADD, None, dt, ops)
    | 0b00001 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VQADD, None, dt, ops)
    | 0b00010 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VRHADD, None, dt, ops)
    | 0b00011 ->
        let%map mnemonic, ops = reg3_bitwise b k in
        (mnemonic, None, None, ops)
    | 0b00100 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VHSUB, None, dt, ops)
    | 0b00101 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VQSUB, None, dt, ops)
    | 0b00110 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VCGT, None, dt, ops)
    | 0b00111 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VCGE, None, dt, ops)
    | 0b01000 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xzy_reg_oprs b chk_undef_d in
        (M.VSHL, None, dt, ops)
    | 0b01001 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xzy_reg_oprs b chk_undef_d in
        (M.VQSHL, None, dt, ops)
    | 0b01010 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xzy_reg_oprs b chk_undef_d in
        (M.VRSHL, None, dt, ops)
    | 0b01011 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xzy_reg_oprs b chk_undef_d in
        (M.VQRSHL, None, dt, ops)
    | 0b01100 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VMAX, None, dt, ops)
    | 0b01101 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VMIN, None, dt, ops)
    | 0b01110 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VABD, None, dt, ops)
    | 0b01111 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_f in
        (M.VABA, None, dt, ops)
    | 0b10000 when chku ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VADD, None, dt, ops)
    | 0b10000 ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VSUB, None, dt, ops)
    | 0b10001 when chku ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VTST, None, dt, ops)
    | 0b10001 ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VCEQ, None, dt, ops)
    | 0b10010 when chku ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VMLA, None, dt, ops)
    | 0b10010 ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VMLS, None, dt, ops)
    | 0b10011 ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VMUL, None, dt, ops)
    | 0b10100 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_j in
        (M.VPMAX, None, dt, ops)
    | 0b10101 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = xyz_reg_oprs b chk_undef_j in
        (M.VPMIN, None, dt, ops)
    | 0b10110 when chku ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_k in
        (M.VQDMULH, None, dt, ops)
    | 0b10110 ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_k in
        (M.VQRDMULH, None, dt, ops)
    | 0b10111 ->
        let%bind dt = one_dt_f b in
        let%map ops = xyz_reg_oprs b chk_undef_d in
        (M.VPADD, None, dt, ops)
    | op when op land 0b11110 = 0b11010 ->
        let%bind dt = one_dt_g b in
        let%map mnemonic, ops = reg3_float b k in
        (mnemonic, None, dt, ops)
    | op when op land 0b11110 = 0b11100 -> reg3_compare b k
    | op when op land 0b11110 = 0b11110 -> reg3_max_min_n_reciprocal b k
    | _ -> Error `Invalid

  let parse_1reg b k =
    let open Result.Let_syntax in
    let b5 = tst b 5 |> Bool.to_int in
    let%bind mnemonic =
      match concat b5 (ex b 11 8) 4 with
      | op when op land 0b11001 = 0b00000 -> Ok M.VMOV
      | op when op land 0b11001 = 0b00001 -> Ok M.VORR
      | op when op land 0b11101 = 0b01000 -> Ok M.VMOV
      | op when op land 0b11101 = 0b01001 -> Ok M.VORR
      | op when op land 0b11100 = 0b01100 -> Ok M.VMOV
      | op when op land 0b11001 = 0b10000 -> Ok M.VMVN
      | op when op land 0b11001 = 0b10001 -> Ok M.VBIC
      | op when op land 0b11101 = 0b11000 -> Ok M.VMVN
      | op when op land 0b11101 = 0b11001 -> Ok M.VBIC
      | op when op land 0b11110 = 0b11100 -> Ok M.VMVN
      | 0b11110 -> Ok M.VMOV
      | 0b11111 -> Error `Invalid
      | _ -> Error `Invalid
    in
    let%bind dt = one_dt_h b in
    let%map ops = p2oprs b chk_undef_n (rx_ia mnemonic k) in
    (mnemonic, None, dt, ops)

  let parse_2reg b k =
    let open Result.Let_syntax in
    let chk = ex b 18 16 = 0 in
    match concat (ex b 11 6) k 1 with
    | op when op land 0b1111000 = 0b0000000 ->
        let%bind dt = one_dt_j k b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_b) in
        (M.VSHR, None, dt, ops)
    | op when op land 0b1111000 = 0b0001000 ->
        let%bind dt = one_dt_j k b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_b) in
        (M.VSRA, None, dt, ops)
    | op when op land 0b1111000 = 0b0010000 ->
        let%bind dt = one_dt_j k b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_b) in
        (M.VRSHR, None, dt, ops)
    | op when op land 0b1111000 = 0b0011000 ->
        let%bind dt = one_dt_j k b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_b) in
        (M.VRSRA, None, dt, ops)
    | op when op land 0b1111001 = 0b0100001 ->
        let%bind dt = one_dt_k b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_b) in
        (M.VSRI, None, dt, ops)
    | op when op land 0b1111001 = 0b0101000 ->
        let%bind dt = one_dt_l b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_c) in
        (M.VSHL, None, dt, ops)
    | op when op land 0b1111001 = 0b0101001 ->
        let%bind dt = one_dt_k b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_b) in
        (M.VSLI, None, dt, ops)
    | op when op land 0b1111000 = 0b0110000 ->
        let%bind dt = one_dt_j k b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_b) in
        (M.VQSHLU, None, dt, ops)
    | op when op land 0b1111000 = 0b0111000 ->
        let%bind dt = one_dt_j k b in
        let%map ops = p3oprs b chk_undef_h (reg_x, reg_z, imm_b) in
        (M.VQSHL, None, dt, ops)
    | 0b1000000 ->
        let%bind dt = one_dt_m b in
        let%map ops = p3oprs b chk_undef_o (reg_ac, reg_ad, imm_d) in
        (M.VSHRN, None, dt, ops)
    | 0b1000010 ->
        let%bind dt = one_dt_m b in
        let%map ops = p3oprs b chk_undef_o (reg_ac, reg_ad, imm_d) in
        (M.VRSHRN, None, dt, ops)
    | 0b1000001 ->
        let%bind dt = one_dt_n b in
        let%map ops = p3oprs b chk_undef_o (reg_ac, reg_ad, imm_d) in
        (M.VQSHRUN, None, dt, ops)
    | 0b1000011 ->
        let%bind dt = one_dt_n b in
        let%map ops = p3oprs b chk_undef_o (reg_ac, reg_ad, imm_d) in
        (M.VQRSHRUN, None, dt, ops)
    | op when op land 0b1111010 = 0b1001000 ->
        let%bind dt = one_dt_o k b in
        let%map ops = p3oprs b chk_undef_o (reg_ac, reg_ad, imm_e) in
        (M.VQSHRN, None, dt, ops)
    | op when op land 0b1111010 = 0b1001010 ->
        let%bind dt = one_dt_o k b in
        let%map ops = p3oprs b chk_undef_o (reg_ac, reg_ad, imm_e) in
        (M.VQRSHRN, None, dt, ops)
    | op when op land 0b1111010 = 0b1010000 && chk ->
        let%bind dt = one_dt_p k b in
        let%map ops = p3oprs b chk_undef_o (reg_ae, reg_af, imm_f) in
        (M.VMOVL, None, dt, ops)
    | op when op land 0b1111010 = 0b1010000 ->
        let%bind dt = one_dt_p k b in
        let%map ops = p3oprs b chk_undef_o (reg_ae, reg_af, imm_f) in
        (M.VSHLL, None, dt, ops)
    | op when op land 0b1110000 = 0b1110000 ->
        let%bind dt = two_dt_a k b in
        let%map ops = p3oprs b chk_undef_p (reg_x, reg_z, imm_g) in
        (M.VCVT, None, dt, ops)
    | _ -> Error `Invalid

  let parse_3reg_diff_len b k =
    let open Result.Let_syntax in
    let chk = ex b 11 8 in
    let kb = if k = 0 then false else true in
    match concat chk k 1 with
    | op when op land 0b11110 = 0b00000 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_q (reg_ae, reg_ag, reg_af) in
        (M.VADDL, None, dt, ops)
    | op when op land 0b11110 = 0b00010 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_q (reg_ae, reg_ag, reg_af) in
        (M.VADDW, None, dt, ops)
    | op when op land 0b11110 = 0b00100 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_q (reg_ae, reg_ag, reg_af) in
        (M.VSUBL, None, dt, ops)
    | op when op land 0b11110 = 0b00110 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_q (reg_ae, reg_ag, reg_af) in
        (M.VSUBW, None, dt, ops)
    | 0b01000 ->
        let%bind dt = one_dt_q b in
        let%map ops = p3oprs b chk_undef_r (reg_ac, reg_u, reg_ad) in
        (M.VADDHN, None, dt, ops)
    | 0b01001 ->
        let%bind dt = one_dt_q b in
        let%map ops = p3oprs b chk_undef_r (reg_ac, reg_u, reg_ad) in
        (M.VRADDHN, None, dt, ops)
    | op when op land 0b11110 = 0b01010 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_s (reg_ae, reg_v, reg_af) in
        (M.VABAL, None, dt, ops)
    | 0b01100 ->
        let%bind dt = one_dt_q b in
        let%map ops = p3oprs b chk_undef_r (reg_ac, reg_u, reg_ad) in
        (M.VSUBHN, None, dt, ops)
    | 0b01101 ->
        let%bind dt = one_dt_q b in
        let%map ops = p3oprs b chk_undef_r (reg_ac, reg_u, reg_ad) in
        (M.VRSUBHN, None, dt, ops)
    | op when op land 0b11110 = 0b01110 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_s (reg_ae, reg_v, reg_af) in
        (M.VABDL, None, dt, ops)
    | op when op land 0b11110 = 0b10000 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_s (reg_ae, reg_v, reg_af) in
        (M.VMLAL, None, dt, ops)
    | op when op land 0b11110 = 0b10100 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_s (reg_ae, reg_v, reg_af) in
        (M.VMLSL, None, dt, ops)
    | op when op land 0b11110 = 0b10010 ->
        let%bind dt = one_dt_a b in
        let%map ops = p3oprs b chk_undef_t (reg_ae, reg_u, reg_ad) in
        (M.VQDMLAL, None, dt, ops)
    | op when op land 0b11110 = 0b10110 ->
        let%bind dt = one_dt_a b in
        let%map ops = p3oprs b chk_undef_t (reg_ae, reg_u, reg_ad) in
        (M.VQDMLSL, None, dt, ops)
    | op when op land 0b11110 = 0b11000 ->
        let%bind dt = one_dt_r k b in
        let%map ops = p3oprs b chk_undef_s (reg_ae, reg_v, reg_af) in
        (M.VMULL, None, dt, ops)
    | 0b11010 ->
        let%bind dt = one_dt_a b in
        let%map ops = p3oprs b chk_undef_t (reg_ae, reg_u, reg_ad) in
        (M.VQDMULL, None, dt, ops)
    | op when op land 0b11110 = 0b11100 ->
        let%bind dt = one_dt_r k b in
        let%map ops = p3oprs b chk_undef_s (reg_ae, reg_v, reg_af) in
        (M.VMULL, None, dt, ops)
    | _ -> Error `Invalid

  let parse_2reg_scalar b k =
    let open Result.Let_syntax in
    let chk = ex b 11 8 in
    let kb = if k = 0 then false else true in
    match concat chk k 1 with
    | op when op land 0b11100 = 0b00000 ->
        let%bind dt = one_dt_b b in
        let%map ops = p3oprs b (chk_undef_b k) (rr_rs_sca kb) in
        (M.VMLA, None, dt, ops)
    | op when op land 0b11100 = 0b01000 ->
        let%bind dt = one_dt_b b in
        let%map ops = p3oprs b (chk_undef_b k) (rr_rs_sca kb) in
        (M.VMLS, None, dt, ops)
    | op when op land 0b11110 = 0b00100 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_c (reg_ae, reg_v, scalar_a) in
        (M.VMLAL, None, dt, ops)
    | op when op land 0b111100 = 0b01100 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_c (reg_ae, reg_v, scalar_a) in
        (M.VMLSL, None, dt, ops)
    | 0b00110 ->
        let%bind dt = one_dt_a b in
        let%map ops = p3oprs b chk_undef_c (reg_ae, reg_v, scalar_a) in
        (M.VQDMLAL, None, dt, ops)
    | 0b01110 ->
        let%bind dt = one_dt_a b in
        let%map ops = p3oprs b chk_undef_c (reg_ae, reg_v, scalar_a) in
        (M.VQDMLSL, None, dt, ops)
    | op when op land 0b11100 = 0b10000 ->
        let%bind dt = one_dt_b b in
        let%map ops = p3oprs b (chk_undef_b k) (rr_rs_sca kb) in
        (M.VMUL, None, dt, ops)
    | op when op land 0b11110 = 0b10100 ->
        let%bind dt = one_dt_d kb b in
        let%map ops = p3oprs b chk_undef_c (reg_ae, reg_v, scalar_a) in
        (M.VMULL, None, dt, ops)
    | 0b10110 ->
        let%bind dt = one_dt_a b in
        let%map ops = p3oprs b chk_undef_c (reg_ae, reg_v, scalar_a) in
        (M.VQDMULL, None, dt, ops)
    | op when op land 0b11110 = 0b11000 ->
        let%bind dt = one_dt_a b in
        let%map ops = p3oprs b (chk_undef_a k) (rr_rs_sca kb) in
        (M.VQDMULH, None, dt, ops)
    | op when op land 0b11110 = 0b11010 ->
        let%bind dt = one_dt_a b in
        let%map ops = p3oprs b (chk_undef_a k) (rr_rs_sca kb) in
        (M.VQRDMULH, None, dt, ops)
    | _ -> Error `Invalid

  let parse_2reg_misc b =
    let open Result.Let_syntax in
    let bit6 () = tst b 6 in
    match concat (ex b 17 16) (ex b 10 7) 4 with
    | 0b000000 ->
        let%bind dt = one_dt_s b in
        let%map ops = p2oprs b chk_undef_u (reg_x, reg_z) in
        (M.VREV64, None, dt, ops)
    | 0b000001 ->
        let%bind dt = one_dt_s b in
        let%map ops = p2oprs b chk_undef_u (reg_x, reg_z) in
        (M.VREV32, None, dt, ops)
    | 0b000010 ->
        let%bind dt = one_dt_s b in
        let%map ops = p2oprs b chk_undef_u (reg_x, reg_z) in
        (M.VREV16, None, dt, ops)
    | o when o land 0b111110 = 0b000100 ->
        let%bind dt = one_dt_c b in
        let%map ops = p2oprs b chk_undef_v (reg_x, reg_z) in
        (M.VPADDL, None, dt, ops)
    | 0b001000 ->
        let%bind dt = one_dt_t b in
        let%map ops = p2oprs b chk_undef_x (reg_x, reg_z) in
        (M.VCLS, None, dt, ops)
    | 0b001001 ->
        let%bind dt = one_dt_u b in
        let%map ops = p2oprs b chk_undef_x (reg_x, reg_z) in
        (M.VCLZ, None, dt, ops)
    | 0b001010 ->
        let%bind dt = one_dt_e () in
        let%map ops = p2oprs b chk_undef_y (reg_x, reg_z) in
        (M.VCNT, None, dt, ops)
    | 0b001011 ->
        let%map ops = p2oprs b chk_undef_y (reg_x, reg_z) in
        (M.VMVN, None, None, ops)
    | o when o land 0b111110 = 0b001100 ->
        let%bind dt = one_dt_c b in
        let%map ops = p2oprs b chk_undef_v (reg_x, reg_z) in
        (M.VPADAL, None, dt, ops)
    | 0b001110 ->
        let%bind dt = one_dt_t b in
        let%map ops = p2oprs b chk_undef_x (reg_x, reg_z) in
        (M.VQABS, None, dt, ops)
    | 0b001111 ->
        let%bind dt = one_dt_t b in
        let%map ops = p2oprs b chk_undef_x (reg_x, reg_z) in
        (M.VQNEG, None, dt, ops)
    | o when o land 0b110111 = 0b010000 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCGT, None, dt, ops)
    | o when o land 0b110111 = 0b010001 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCGE, None, dt, ops)
    | o when o land 0b110111 = 0b010010 ->
        let%bind dt = one_dt_w b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCEQ, None, dt, ops)
    | o when o land 0b110111 = 0b010011 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCLE, None, dt, ops)
    | o when o land 0b110111 = 0b010100 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCLT, None, dt, ops)
    | o when o land 0b110111 = 0b010110 ->
        let%bind dt = one_dt_v b in
        let%map ops = p2oprs b chk_undef_ac (reg_x, reg_z) in
        (M.VABS, None, dt, ops)
    | o when o land 0b110111 = 0b010111 ->
        let%bind dt = one_dt_v b in
        let%map ops = p2oprs b chk_undef_ac (reg_x, reg_z) in
        (M.VNEG, None, dt, ops)
    | 0b100000 ->
        let%map ops = p2oprs b chk_undef_z (reg_x, reg_z) in
        (M.VSWP, None, None, ops)
    | 0b100001 ->
        let%bind dt = one_dt_s b in
        let%map ops = p2oprs b chk_undef_aa (reg_x, reg_z) in
        (M.VTRN, None, dt, ops)
    | 0b100010 ->
        let%bind dt = one_dt_s b in
        let%map ops = p2oprs b chk_undef_ab (reg_x, reg_z) in
        (M.VUZP, None, dt, ops)
    | 0b100011 ->
        let%bind dt = one_dt_s b in
        let%map ops = p2oprs b chk_undef_ab (reg_x, reg_z) in
        (M.VZIP, None, dt, ops)
    | 0b100100 when bit6 () ->
        let%bind dt = one_dt_x b in
        let%map ops = p2oprs b chk_undef_ad (reg_ac, reg_ad) in
        (M.VMOVN, None, dt, ops)
    | 0b100100 ->
        let%bind dt = one_dt_y b in
        let%map ops = p2oprs b chk_undef_ad (reg_ac, reg_ad) in
        (M.VQMOVUN, None, dt, ops)
    | 0b100101 when bit6 () ->
        let%bind dt = one_dt_y b in
        let%map ops = p2oprs b chk_undef_ad (reg_ac, reg_ad) in
        (M.VQMOVN, None, dt, ops)
    | 0b100101 ->
        let%bind dt = one_dt_y b in
        let%map ops = p2oprs b chk_undef_ad (reg_ac, reg_ad) in
        (M.VQMOVN, None, dt, ops)
    | 0b100110 when bit6 () ->
        let%bind dt = one_dt_u b in
        let%map ops = p2oprs b chk_undef_ad (reg_ac, reg_ad) in
        (M.VSHLL, None, dt, ops)
    | o when o land 0b111101 = 0b101100 && bit6 () ->
        let%bind dt = two_dt_c b in
        let%map ops = p2oprs b chk_undef_ae (reg_x, reg_z) in
        (M.VCVT, None, dt, ops)
    | o when o land 0b111101 = 0b111000 ->
        let%bind dt = one_dt_z b in
        let%map ops = p2oprs b chk_undef_af (reg_x, reg_z) in
        (M.VRECPE, None, dt, ops)
    | o when o land 0b111101 = 0b111001 ->
        let%bind dt = one_dt_z b in
        let%map ops = p2oprs b chk_undef_af (reg_x, reg_z) in
        (M.VRSQRTE, None, dt, ops)
    | o when o land 0b111100 = 0b111100 ->
        let%bind dt = two_dt_b b in
        let%map ops = p2oprs b chk_undef_w (reg_x, reg_z) in
        (M.VCVT, None, dt, ops)
    | _ -> Error `Invalid

  let parse_adv_simd_data_proc b mode =
    let open Result.Let_syntax in
    let ext f t v = ex b f t = v in
    let pick u v = Bool.equal (tst b u) v in
    let kb = if Mode.is_thumb mode then tst b 24 else tst b 28 in
    let k = Bool.to_int kb in
    match concat (ex b 23 19) (ex b 7 4) 4 with
    | op when op land 0b100000000 = 0b000000000 -> parse_3reg b k
    | op when op land 0b101111001 = 0b100000001 -> parse_1reg b k
    | op when op land 0b101111001 = 0b100010001 -> parse_2reg b k
    | op when op land 0b101101001 = 0b100100001 -> parse_2reg b k
    | op when op land 0b101001001 = 0b101000001 -> parse_2reg b k
    | op when op land 0b100001001 = 0b100001001 -> parse_2reg b k
    | op when op land 0b101000101 = 0b100000000 -> parse_3reg_diff_len b k
    | op when op land 0b101100101 = 0b101000000 -> parse_3reg_diff_len b k
    | op when op land 0b101000101 = 0b100000100 -> parse_2reg_scalar b k
    | op when op land 0b101100101 = 0b101000100 -> parse_2reg_scalar b k
    | op when op land 0b101100001 = 0b101100000 && not kb ->
        let%bind dt = one_dt_e () in
        let%map ops = p4oprs b chk_undef_g (reg_x, reg_y, reg_z, imm_4c) in
        (M.VEXT, None, dt, ops)
    | op when op land 0b101100001 = 0b101100000 && pick 11 false ->
        parse_2reg_misc b
    | op when op land 0b101100101 = 0b101100000 && ext 11 10 0b10 ->
        let%bind dt = one_dt_e () in
        let%map ops = p3oprs b dummy_chk (reg_ac, reglist_a, reg_af) in
        (M.VTBL, None, dt, ops)
    | op when op land 0b101100101 = 0b101100100 && ext 11 10 0b10 ->
        let%bind dt = one_dt_e () in
        let%map ops = p3oprs b dummy_chk (reg_ac, reglist_a, reg_af) in
        (M.VTBX, None, dt, ops)
    | op when op land 0b101101001 = 0b101100000 && ext 11 8 0b1100 ->
        let%bind dt = one_dt_ab b in
        let%map ops = p2oprs b chk_undef_ag (reg_x, scalar_b) in
        (M.VDUP, None, dt, ops)
    | _ -> Error `Invalid

  let parse_group001 b =
    let open Result.Let_syntax in
    let op = ex b 24 20 in
    let%map mnemonic, ops, cflag =
      match op with
      | op when op land 0b11001 <> 0b10000 -> data_proc_imm op b
      | 0b10000 ->
          let%map ops = p2oprs b dummy_chk (reg_d, imm_12b) in
          (M.MOVW, ops, None)
      | 0b10100 ->
          let%map ops = p2oprs b dummy_chk (reg_d, imm_12b) in
          (M.MOVT, ops, None)
      | op when op land 0b11011 = 0b10010 ->
          let%map mnemonic, ops = msr_hints b in
          (mnemonic, ops, None)
      | _ -> Error `Invalid
    in
    (mnemonic, None, None, ops, cflag)

  let adv_simd_or_struct b =
    let open Result.Let_syntax in
    let op = concat (tst b 23 |> Bool.to_int) (ex b 11 8) 4 in
    let wback = Some (ex b 3 0 <> 15) in
    match concat op (tst b 21 |> Bool.to_int) 1 with
    | 0b000100 | 0b001100 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ah (reglist_b, mem_s) in
        (M.VST1, wback, dt, ops)
    | 0b001110 | 0b010100 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ah (reglist_b, mem_s) in
        (M.VST1, wback, dt, ops)
    | 0b000110 | 0b010000 | 0b010010 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ai (reglist_b, mem_s) in
        (M.VST2, wback, dt, ops)
    | 0b001000 | 0b001010 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_aj (reglist_b, mem_s) in
        (M.VST2, wback, dt, ops)
    | 0b000000 | 0b000010 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ak (reglist_b, mem_s) in
        (M.VST4, wback, dt, ops)
    | 0b100000 | 0b101000 | 0b110000 ->
        let%bind dt = one_dt_ad b in
        let%map ops = p2oprs b chk_undef_al (reglist_c, mem_t) in
        (M.VST1, wback, dt, ops)
    | 0b100010 | 0b101010 | 0b110010 ->
        let%bind dt = one_dt_ad b in
        let%map ops = p2oprs b chk_undef_ad (reglist_d, mem_u) in
        (M.VST2, wback, dt, ops)
    | 0b100100 | 0b101100 | 0b110100 ->
        let%bind dt = one_dt_ad b in
        let%map ops = p2oprs b chk_undef_an (reglist_e, mem_v) in
        (M.VST3, wback, dt, ops)
    | 0b100110 | 0b101110 | 0b110110 ->
        let%bind dt = one_dt_ad b in
        let%map ops = p2oprs b chk_undef_ao (reglist_f, mem_w) in
        (M.VST4, wback, dt, ops)
    | 0b000101 | 0b001101 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ah (reglist_b, mem_s) in
        (M.VLD1, wback, dt, ops)
    | 0b001111 | 0b010101 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ah (reglist_b, mem_s) in
        (M.VLD1, wback, dt, ops)
    | 0b000111 | 0b010001 | 0b010011 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ai (reglist_b, mem_s) in
        (M.VLD2, wback, dt, ops)
    | 0b001001 | 0b001011 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_aj (reglist_b, mem_s) in
        (M.VLD3, wback, dt, ops)
    | 0b000001 | 0b000011 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ak (reglist_b, mem_s) in
        (M.VLD4, wback, dt, ops)
    | 0b100001 | 0b101001 | 0b110001 ->
        let%bind dt = one_dt_ad b in
        let%map ops = p2oprs b chk_undef_al (reglist_c, mem_t) in
        (M.VLD1, wback, dt, ops)
    | 0b100011 | 0b101011 | 0b110011 ->
        let%bind dt = one_dt_ad b in
        let%map ops = p2oprs b chk_undef_am (reglist_d, mem_u) in
        (M.VLD2, wback, dt, ops)
    | 0b100101 | 0b101101 | 0b110101 ->
        let%bind dt = one_dt_ad b in
        let%map ops = p2oprs b chk_undef_an (reglist_e, mem_v) in
        (M.VLD3, wback, dt, ops)
    | 0b100111 | 0b101111 | 0b110111 ->
        let%bind dt = one_dt_ad b in
        let%map ops = p2oprs b chk_undef_ao (reglist_f, mem_w) in
        (M.VLD4, wback, dt, ops)
    | 0b111001 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ap (reglist_g, mem_x) in
        (M.VLD1, wback, dt, ops)
    | 0b111011 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_aq (reglist_h, mem_y) in
        (M.VLD2, wback, dt, ops)
    | 0b111101 ->
        let%bind dt = one_dt_ac b in
        let%map ops = p2oprs b chk_undef_ar (reglist_i, mem_z) in
        (M.VLD3, wback, dt, ops)
    | 0b111111 ->
        let%bind dt = one_dt_ae b in
        let%map ops = p2oprs b chk_undef_as (reglist_j, mem_aa) in
        (M.VLD4, wback, dt, ops)
    | _ -> Error `Invalid

  let uncond010 b =
    let open Result.Let_syntax in
    let op = ex b 24 20 in
    let chkrn () = ex b 19 16 = 0b1111 in
    let chk1 op = op land 0b10001 in
    let chk2 op = op land 0b10111 in
    match op with
    | op when chk1 op = 0b00000 -> adv_simd_or_struct b
    | op when chk2 op = 0b00001 -> Ok (M.NOP, None, None, e)
    | op when chk2 op = 0b00101 ->
        let%map ops = p1opr b dummy_chk mem_ab in
        (M.PLI, None, None, ops)
    | op when op land 0b10011 = 0b00011 -> Error `Invalid
    | op when chk2 op = 0b10001 && chkrn () -> Error `Invalid
    | op when chk2 op = 0b10001 ->
        let%map ops = p1opr b dummy_chk mem_ab in
        (M.PLDW, None, None, ops)
    | op when chk2 op = 0b10101 && chkrn () ->
        let%map ops = p1opr b dummy_chk mem_m in
        (M.PLD, None, None, ops)
    | op when chk2 op = 0b10101 ->
        let%map ops = p1opr b dummy_chk mem_ab in
        (M.PLD, None, None, ops)
    | 0b10011 -> Error `Invalid
    | 0b10111 when ex b 7 4 = 0b0001 -> Ok (M.CLREX, None, None, e)
    | 0b10111 when ex b 7 4 = 0b0100 ->
        let%map ops = p1opr b dummy_chk opt_a in
        (M.DSB, None, None, ops)
    | 0b10111 when ex b 7 4 = 0b0101 ->
        let%map ops = p1opr b dummy_chk opt_a in
        (M.DMB, None, None, ops)
    | 0b10111 when ex b 7 4 = 0b0110 ->
        let%map ops = p1opr b dummy_chk opt_a in
        (M.ISB, None, None, ops)
    | 0b10111 -> Error `Invalid
    | op when op land 0b11011 = 0b11011 -> Error `Invalid
    | _ -> Error `Invalid

  let parse_group010 b =
    let open Result.Let_syntax in
    let is_push_pop () = ex b 19 16 = 0b1101 && ex b 11 0 = 0b100 in
    let chkrn () = ex b 19 16 = 0b1111 in
    let wback = (not (tst b 24)) || tst b 21 in
    let%map mnemonic, wback, ops =
      match ex b 24 20 with
      | 0b01001 when is_push_pop () ->
          let%map ops = p1opr b chk_y reg_d in
          (M.POP, None, ops)
      | 0b10010 when is_push_pop () ->
          let%map ops = p1opr b chk_y reg_d in
          (M.PUSH, None, ops)
      | op when op land 0b10111 = 0b00010 ->
          let%map ops = p2oprs b chk_z (reg_d, mem_k) in
          (M.STRT, None, ops)
      | op when op land 0b00101 = 0b00000 ->
          let%map ops = p2oprs b chk_aa (reg_d, mem_l) in
          (M.STR, Some wback, ops)
      | op when op land 0b10111 = 0b00011 ->
          let%map ops = p2oprs b chk_w (reg_d, mem_k) in
          (M.LDRT, None, ops)
      | op when op land 0b00101 = 0b00001 && chkrn () ->
          let%map ops = p2oprs b dummy_chk (reg_d, mem_m) in
          (M.LDR, None, ops)
      | op when op land 0b00101 = 0b00001 ->
          let%map ops = p2oprs b chk_aa (reg_d, mem_l) in
          (M.LDR, Some wback, ops)
      | op when op land 0b10111 = 0b00110 ->
          let%map ops = p2oprs b chk_w (reg_d, mem_k) in
          (M.STRBT, None, ops)
      | op when op land 0b00101 = 0b00100 ->
          let%map ops = p2oprs b chk_ac (reg_d, mem_l) in
          (M.STRB, Some wback, ops)
      | op when op land 0b10111 = 0b00111 ->
          let%map ops = p2oprs b chk_w (reg_d, mem_k) in
          (M.LDRBT, None, ops)
      | op when op land 0b00101 = 0b00101 && chkrn () ->
          let%map ops = p2oprs b chk_g (reg_d, mem_m) in
          (M.LDRB, None, ops)
      | op when op land 0b00101 = 0b00101 ->
          let%map ops = p2oprs b chk_ab (reg_d, mem_l) in
          (M.LDRB, Some wback, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, wback, None, ops)

  let uncond0110 b =
    let open Result.Let_syntax in
    let%map mnemonic, ops =
      match ex b 24 20 with
      | op when op land 0b10111 = 0b00001 -> Ok (M.NOP, e)
      | op when op land 0b10111 = 0b00101 ->
          let%map ops = p1opr b chk_d mem_ac in
          (M.PLI, ops)
      | op when op land 0b10111 = 0b10001 ->
          let%map ops = p1opr b chk_d mem_ac in
          (M.PLDW, ops)
      | op when op land 0b10111 = 0b10101 ->
          let%map ops = p1opr b chk_d mem_ac in
          (M.PLD, ops)
      | op when op land 0b00011 = 0b00011 -> Error `Invalid
      | _ -> Error `Invalid
    in
    (mnemonic, None, None, ops)

  let parse_group0110 b =
    let open Result.Let_syntax in
    let wback () = Some ((not (tst b 24)) || tst b 21) in
    let%map mnemonic, wback, ops =
      match ex b 24 20 with
      | o when o land 0b10111 = 0b00010 ->
          let%map ops = p2oprs b chk_al (reg_d, mem_q) in
          (M.STRT, None, ops)
      | o when o land 0b00101 = 0b00000 ->
          let%map ops = p2oprs b chk_am (reg_d, mem_r) in
          (M.STR, wback (), ops)
      | o when o land 0b10111 = 0b00011 ->
          let%map ops = p2oprs b chk_v (reg_d, mem_q) in
          (M.LDRT, None, ops)
      | o when o land 0b00101 = 0b00001 ->
          let%map ops = p2oprs b chk_am (reg_d, mem_r) in
          (M.LDR, wback (), ops)
      | o when o land 0b10111 = 0b00110 ->
          let%map ops = p2oprs b chk_v (reg_d, mem_q) in
          (M.STRBT, None, ops)
      | o when o land 0b00101 = 0b00100 ->
          let%map ops = p2oprs b chk_an (reg_d, mem_r) in
          (M.STRB, wback (), ops)
      | o when o land 0b10111 = 0b00111 ->
          let%map ops = p2oprs b chk_v (reg_d, mem_q) in
          (M.LDRBT, None, ops)
      | o when o land 0b00101 = 0b00101 ->
          let%map ops = p2oprs b chk_an (reg_d, mem_r) in
          (M.LDRB, wback (), ops)
      | _ -> Error `Invalid
    in
    (mnemonic, wback, None, ops)

  let parse_parallel_add_n_sub_signed b =
    let open Result.Let_syntax in
    let b1 = ex b 21 20 in
    let b2 = ex b 7 5 in
    let%bind mnemonic =
      match concat b1 b2 3 with
      | 0b01000 -> Ok M.SADD16
      | 0b01001 -> Ok M.SASX
      | 0b01010 -> Ok M.SSAX
      | 0b01011 -> Ok M.SSUB16
      | 0b01100 -> Ok M.SADD8
      | 0b01111 -> Ok M.SSUB8
      | 0b10000 -> Ok M.QADD16
      | 0b10001 -> Ok M.QASX
      | 0b10010 -> Ok M.QSAX
      | 0b10011 -> Ok M.QSUB16
      | 0b10100 -> Ok M.QADD8
      | 0b10111 -> Ok M.QSUB8
      | 0b11000 -> Ok M.SHADD16
      | 0b11001 -> Ok M.SHASX
      | 0b11010 -> Ok M.SHSAX
      | 0b11011 -> Ok M.SHSUB16
      | 0b11100 -> Ok M.SHADD8
      | 0b11111 -> Ok M.SHSUB8
      | _ -> Error `Invalid
    in
    let%map ops = p3oprs b chk_a (reg_d, reg_c, reg_a) in
    (mnemonic, ops)

  let parse_parallel_add_n_sub_unsigned b =
    let open Result.Let_syntax in
    let b1 = ex b 21 20 in
    let b2 = ex b 7 5 in
    let%bind mnemonic =
      match concat b1 b2 3 with
      | 0b01000 -> Ok M.UADD16
      | 0b01001 -> Ok M.UASX
      | 0b01010 -> Ok M.USAX
      | 0b01011 -> Ok M.USUB16
      | 0b01100 -> Ok M.UADD8
      | 0b01111 -> Ok M.USUB8
      | 0b10000 -> Ok M.UQADD16
      | 0b10001 -> Ok M.UQASX
      | 0b10010 -> Ok M.UQSAX
      | 0b10011 -> Ok M.UQSUB16
      | 0b10100 -> Ok M.UQADD8
      | 0b10111 -> Ok M.UQSUB8
      | 0b11000 -> Ok M.UHADD16
      | 0b11001 -> Ok M.UHASX
      | 0b11010 -> Ok M.UHSAX
      | 0b11011 -> Ok M.UHSUB16
      | 0b11100 -> Ok M.UHADD8
      | 0b11111 -> Ok M.UHSUB8
      | _ -> Error `Invalid
    in
    let%map ops = p3oprs b chk_a (reg_d, reg_c, reg_a) in
    (mnemonic, ops)

  let parse_packing_saturation_reversal b =
    let open Result.Let_syntax in
    let chk = ex b 19 16 in
    match (concat (ex b 22 20) (ex b 7 5) 3, chk) with
    | op, _ when op land 0b111011 = 0b000000 ->
        let%map ops = p4oprs b chk_c (reg_d, reg_c, reg_a, shift_d) in
        (M.PKHBT, ops)
    | op, _ when op land 0b111011 = 0b000010 ->
        let%map ops = p4oprs b chk_c (reg_d, reg_c, reg_a, shift_d) in
        (M.PKHTB, ops)
    | op, _ when op land 0b110001 = 0b010000 ->
        let%map ops = p4oprs b chk_o (reg_d, imm_5c, reg_a, shift_d) in
        (M.SSAT, ops)
    | op, _ when op land 0b110001 = 0b110000 ->
        let%map ops = p4oprs b chk_o (reg_d, imm_5c, reg_a, shift_d) in
        (M.USAT, ops)
    | 0b000011, 0b1111 ->
        let%map ops = p3oprs b chk_p (reg_d, reg_a, shift_c) in
        (M.SXTB16, ops)
    | 0b000011, _ ->
        let%map ops = p4oprs b chk_o (reg_d, reg_c, reg_a, shift_c) in
        (M.SXTAB16, ops)
    | 0b000101, _ ->
        let%map ops = p3oprs b chk_a (reg_d, reg_c, reg_a) in
        (M.SEL, ops)
    | 0b010001, _ ->
        let%map ops = p3oprs b chk_m (reg_a, imm_4b, reg_a) in
        (M.SSAT16, ops)
    | 0b010011, 0b1111 ->
        let%map ops = p3oprs b chk_p (reg_d, reg_a, shift_c) in
        (M.SXTB, ops)
    | 0b010011, _ ->
        let%map ops = p4oprs b chk_o (reg_d, reg_c, reg_a, shift_c) in
        (M.SXTAB, ops)
    | 0b011001, _ ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.REV, ops)
    | 0b011011, 0b1111 ->
        let%map ops = p3oprs b chk_p (reg_d, reg_a, shift_c) in
        (M.SXTH, ops)
    | 0b011011, _ ->
        let%map ops = p4oprs b chk_o (reg_d, reg_c, reg_a, shift_c) in
        (M.SXTAH, ops)
    | 0b011101, _ ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.REV16, ops)
    | 0b100011, 0b1111 ->
        let%map ops = p3oprs b chk_p (reg_d, reg_a, shift_c) in
        (M.UXTB16, ops)
    | 0b100011, _ ->
        let%map ops = p4oprs b chk_o (reg_d, reg_c, reg_a, shift_c) in
        (M.UXTAB16, ops)
    | 0b110001, _ ->
        let%map ops = p3oprs b chk_m (reg_a, imm_4b, reg_a) in
        (M.USAT16, ops)
    | 0b110011, 0b1111 ->
        let%map ops = p3oprs b chk_p (reg_d, reg_a, shift_c) in
        (M.UXTB, ops)
    | 0b110011, _ ->
        let%map ops = p4oprs b chk_o (reg_d, reg_c, reg_a, shift_c) in
        (M.UXTAB, ops)
    | 0b111001, _ ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.RBIT, ops)
    | 0b111011, 0b1111 ->
        let%map ops = p3oprs b chk_p (reg_d, reg_a, shift_c) in
        (M.UXTH, ops)
    | 0b111011, _ ->
        let%map ops = p4oprs b chk_o (reg_d, reg_c, reg_a, shift_c) in
        (M.UXTAH, ops)
    | 0b111101, _ ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.REVSH, ops)
    | _ -> Error `Invalid

  let parse_signed_multiplies b =
    let open Result.Let_syntax in
    let chk = ex b 15 12 = 0b1111 in
    match concat (ex b 22 20) (ex b 7 5) 3 with
    | 0b000000 when chk ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMUAD, ops)
    | 0b000000 ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMLAD, ops)
    | 0b000001 when chk ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMUADX, ops)
    | 0b000001 ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMLADX, ops)
    | 0b000010 when chk ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMUSD, ops)
    | 0b000010 ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMLSD, ops)
    | 0b000011 when chk ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMUSDX, ops)
    | 0b000011 ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMLSDX, ops)
    | 0b100000 ->
        let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
        (M.SMLALD, ops)
    | 0b100001 ->
        let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
        (M.SMLALDX, ops)
    | 0b100010 ->
        let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
        (M.SMLSLD, ops)
    | 0b100011 ->
        let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
        (M.SMLSLDX, ops)
    | 0b101000 when chk ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMMUL, ops)
    | 0b101000 ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMMLA, ops)
    | 0b101001 when chk ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMMULR, ops)
    | 0b101001 ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMMLAR, ops)
    | 0b101110 ->
        let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
        (M.SMMLS, ops)
    | 0b101111 ->
        let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
        (M.SMMLSR, ops)
    | _ -> Error `Invalid

  let parse_group0111 cond b =
    let open Result.Let_syntax in
    let chkrd = ex b 15 12 = 0b1111 in
    let chkrn = ex b 3 0 = 0b1111 in
    let is_bitfield op = op land 0b11110011 = 0b11100000 in
    let%map mnemonic, ops =
      match concat (ex b 24 20) (ex b 7 5) 3 with
      | o when o land 0b11100000 = 0b00000000 ->
          parse_parallel_add_n_sub_signed b
      | o when o land 0b11100000 = 0b00100000 ->
          parse_parallel_add_n_sub_unsigned b
      | o when o land 0b11000000 = 0b01000000 ->
          parse_packing_saturation_reversal b
      | o when o land 0b11000000 = 0b10000000 -> parse_signed_multiplies b
      | 0b11000000 when chkrd ->
          let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
          (M.USAD8, ops)
      | 0b11000000 ->
          let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
          (M.USADA8, ops)
      | o when o land 0b11110011 = 0b11010010 ->
          let%map ops = p4oprs b chk_q (reg_d, reg_a, imm_5a, imm_5c) in
          (M.SBFX, ops)
      | o when is_bitfield o && chkrn ->
          let%map ops = p3oprs b chk_ap (reg_d, imm_5a, imm_5f) in
          (M.BFC, ops)
      | o when is_bitfield o ->
          let%map ops = p4oprs b chk_aq (reg_d, reg_a, imm_5a, imm_5f) in
          (M.BFI, ops)
      | o when o land 0b11110011 = 0b11110010 ->
          let%map ops = p4oprs b chk_q (reg_d, reg_a, imm_5a, imm_5c) in
          (M.UBFX, ops)
      | 0b11111111 when C.(equal cond AL) ->
          let%map ops = p1opr b dummy_chk imm_12d in
          (M.UDF, ops)
      | 0b11111111 -> Error `Invalid
      | _ -> Error `Invalid
    in
    (mnemonic, None, None, ops)

  let stm b =
    let open Result.Let_syntax in
    let op = ex b 24 23 in
    match op with
    | 0b00 ->
        let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
        (M.STMDA, None, ops)
    | 0b01 ->
        let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
        (M.STMIA, None, ops)
    | 0b10 ->
        let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
        (M.STMDB, None, ops)
    | 0b11 ->
        let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
        (M.STMIB, None, ops)
    | _ -> Error `Invalid

  let ldm_user b =
    let open Result.Let_syntax in
    let op = ex b 24 23 in
    match op with
    | 0b00 ->
        let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
        (M.LDMDA, None, ops)
    | 0b01 ->
        let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
        (M.LDMIA, None, ops)
    | 0b10 ->
        let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
        (M.LDMDB, None, ops)
    | 0b11 ->
        let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
        (M.LDMIB, None, ops)
    | _ -> Error `Invalid

  let ldm_exception b =
    let open Result.Let_syntax in
    let op = ex b 24 23 in
    let wback = Some (tst b 21) in
    match op with
    | 0b00 ->
        let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
        (M.LDMDA, wback, ops)
    | 0b01 ->
        let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
        (M.LDMIA, wback, ops)
    | 0b10 ->
        let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
        (M.LDMDB, wback, ops)
    | 0b11 ->
        let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
        (M.LDMIB, wback, ops)
    | _ -> Error `Invalid

  let uncond100 b =
    let open Result.Let_syntax in
    let wback = Some (tst b 21) in
    let%map mnemonic, ops =
      match ex b 24 20 with
      | op when op land 0b11101 = 0b00100 ->
          let%map ops = p2oprs b dummy_chk (reg_m, imm_5b) in
          (M.SRSDA, ops)
      | op when op land 0b11101 = 0b01100 ->
          let%map ops = p2oprs b dummy_chk (reg_m, imm_5b) in
          (M.SRSIA, ops)
      | op when op land 0b11101 = 0b10100 ->
          let%map ops = p2oprs b dummy_chk (reg_m, imm_5b) in
          (M.SRSDB, ops)
      | op when op land 0b11101 = 0b11100 ->
          let%map ops = p2oprs b dummy_chk (reg_m, imm_5b) in
          (M.SRSIB, ops)
      | op when op land 0b11101 = 0b00001 ->
          let%map ops = p1opr b chk_n reg_n in
          (M.RFEDA, ops)
      | op when op land 0b11101 = 0b01001 ->
          let%map ops = p1opr b chk_n reg_n in
          (M.RFEIA, ops)
      | op when op land 0b11101 = 0b10001 ->
          let%map ops = p1opr b chk_n reg_n in
          (M.RFEDB, ops)
      | op when op land 0b11101 = 0b11001 ->
          let%map ops = p1opr b chk_n reg_n in
          (M.RFEIB, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, wback, None, ops)

  let parse_group100 b =
    let open Result.Let_syntax in
    let%bind is_push_pop =
      let%map rl = reglist (ex b 15 0) in
      ex b 19 16 = 0b1101 && List.length rl >= 2
    in
    let chkr = not (tst b 15) in
    let wback = Some (tst b 21) in
    let%map mnemonic, wback, ops =
      match ex b 24 20 with
      | op when op land 0b11101 = 0b00000 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STMDA, wback, ops)
      | op when op land 0b11101 = 0b00001 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMDA, wback, ops)
      | op when op land 0b11101 = 0b01000 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STM, wback, ops)
      | 0b01001 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDM, wback, ops)
      | 0b01011 when is_push_pop ->
          let%map ops = p1opr b chk_at reglist_k in
          (M.POP, None, ops)
      | 0b01011 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDM, wback, ops)
      | 0b10000 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STMDB, wback, ops)
      | 0b10010 when is_push_pop ->
          let%map ops = p1opr b dummy_chk reglist_k in
          (M.PUSH, None, ops)
      | 0b10010 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STMDB, wback, ops)
      | op when op land 0b11101 = 0b10001 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMDB, wback, ops)
      | op when op land 0b11101 = 0b11000 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STMIB, wback, ops)
      | op when op land 0b11101 = 0b11001 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMIB, wback, ops)
      | op when op land 0b00101 = 0b00100 -> stm b
      | op when op land 0b00101 = 0b00101 && chkr -> ldm_user b
      | op when op land 0b00101 = 0b00101 -> ldm_exception b
      | _ -> Error `Invalid
    in
    (mnemonic, wback, None, ops)

  let parse_group101 b =
    let open Result.Let_syntax in
    let op = tst b 24 in
    match op with
    | false ->
        let%map ops = p1opr b dummy_chk lbl_a in
        (M.B, None, None, ops)
    | true ->
        let%map ops = p1opr b dummy_chk lbl_24b in
        (M.BL, None, None, ops)

  let uncond110 b =
    let open Result.Let_syntax in
    let op = ex b 24 20 in
    let chkrn () = ex b 19 16 = 0b1111 in
    let chkldc op = op land 0b00101 = 0b00001 in
    let chkldcl op = op land 0b00101 = 0b00101 in
    let wback () = Some (tst b 21) in
    let%map mnemonic, wback, ops =
      match op with
      | op when chkldc op && chkrn () ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ad) in
          (M.LDC2, None, ops)
      | op when chkldc op ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.LDC2, wback (), ops)
      | op when chkldcl op && chkrn () ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ad) in
          (M.LDC2L, None, ops)
      | op when chkldcl op ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.LDC2L, wback (), ops)
      | op when op land 0b11101 = 0b01000 ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.STC2, wback (), ops)
      | op when op land 0b11101 = 0b01100 ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.STC2L, wback (), ops)
      | op when op land 0b10101 = 0b10000 ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.STC2, wback (), ops)
      | op when op land 0b10101 = 0b10100 ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.STC2L, wback (), ops)
      | 0b00010 ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.STC2, wback (), ops)
      | 0b00110 ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.STC2L, wback (), ops)
      | 0b00100 ->
          let%map ops =
            p5oprs b chk_au (preg_a, imm_4d, reg_d, reg_c, creg_b)
          in
          (M.MCRR2, None, ops)
      | 0b00101 ->
          let%map ops =
            p5oprs b chk_av (preg_a, imm_4d, reg_d, reg_c, creg_b)
          in
          (M.MRRC2, None, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, wback, None, ops)

  let parse_64bit_transfer b =
    let open Result.Let_syntax in
    let op () = not (tst b 20) in
    match ex b 8 4 land 0b11101 with
    | 0b00001 when op () ->
        let%map ops = p4oprs b chk_aw (reg_ai, reg_aj, reg_d, reg_c) in
        (M.VMOV, None, ops)
    | 0b00001 ->
        let%map ops = p4oprs b chk_ax (reg_d, reg_c, reg_ai, reg_aj) in
        (M.VMOV, None, ops)
    | 0b10001 when op () ->
        let%map ops = p3oprs b chk_p (reg_af, reg_d, reg_c) in
        (M.VMOV, None, ops)
    | 0b10001 ->
        let%map ops = p3oprs b chk_ay (reg_d, reg_c, reg_af) in
        (M.VMOV, None, ops)
    | _ -> Error `Invalid

  let parse_ext_reg_load_store b =
    let open Result.Let_syntax in
    let chkrn = ex b 19 16 = 0b1101 in
    let chk8 = not (tst b 8) in
    let wback = Some (tst b 21) in
    match ex b 24 20 with
    | op when op land 0b11110 = 0b00100 -> parse_64bit_transfer b
    | op when op land 0b11011 = 0b01000 && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VSTMIA, wback, ops)
    | op when op land 0b11011 = 0b01000 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VSTMIA, wback, ops)
    | op when op land 0b11011 = 0b01010 && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VSTMIA, wback, ops)
    | op when op land 0b11011 = 0b01010 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VSTMIA, wback, ops)
    | op when op land 0b10011 = 0b10000 ->
        let%map ops = p2oprs b dummy_chk (reg_al, mem_ar) in
        (M.VSTR, None, ops)
    | op when op land 0b11011 = 0b10010 && chkrn && chk8 ->
        let%map ops = p1opr b chk_ba reglist_m in
        (M.VPUSH, None, ops)
    | op when op land 0b11011 = 0b10010 && chkrn ->
        let%map ops = p1opr b chk_az reglist_l in
        (M.VPUSH, None, ops)
    | op when op land 0b11011 = 0b10010 && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VSTMDB, wback, ops)
    | op when op land 0b11011 = 0b10010 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VSTMDB, wback, ops)
    | op when op land 0b11011 = 0b01001 && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VLDMIA, wback, ops)
    | op when op land 0b11011 = 0b01001 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VLDMIA, wback, ops)
    | op when op land 0b11011 = 0b01011 && chkrn && chk8 ->
        let%map ops = p1opr b chk_ba reglist_m in
        (M.VPOP, None, ops)
    | op when op land 0b11011 = 0b01011 && chkrn ->
        let%map ops = p1opr b chk_az reglist_l in
        (M.VPOP, None, ops)
    | op when op land 0b11011 = 0b01011 && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VLDMIA, wback, ops)
    | op when op land 0b11011 = 0b01011 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VLDMIA, wback, ops)
    | op when op land 0b10011 = 0b10001 ->
        let%map ops = p2oprs b dummy_chk (reg_al, mem_ar) in
        (M.VLDR, None, ops)
    | op when op land 0b11011 = 0b10011 && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VLDMDB, wback, ops)
    | op when op land 0b11011 = 0b10011 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VLDMDB, wback, ops)
    | _ -> Error `Invalid

  let parse_group110 b =
    let open Result.Let_syntax in
    let chkrn = ex b 19 16 <> 0b1111 in
    let chkcop = ex b 11 9 <> 0b101 in
    let chkldc op = op land 0b00101 = 0b00001 in
    let chkldcl op = op land 0b00101 = 0b00101 in
    let wback = Some (tst b 21) in
    let%map mnemonic, wback, ops =
      match ex b 24 20 with
      | op when op land 0b11110 = 0b00000 -> Error `Invalid
      | 0b00100 when chkcop ->
          let%map ops =
            p5oprs b chk_au (preg_a, imm_4d, reg_d, reg_c, creg_b)
          in
          (M.MCRR, None, ops)
      | 0b00101 when chkcop ->
          let%map ops =
            p5oprs b chk_av (preg_a, imm_4d, reg_d, reg_c, creg_b)
          in
          (M.MRRC, None, ops)
      | op when op land 0b00101 = 0 && chkcop ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.STC, wback, ops)
      | op when op land 0b00101 = 4 && chkcop ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.STCL, wback, ops)
      | op when chkldc op && chkcop && chkrn ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.LDC, None, ops)
      | op when chkldc op && chkcop ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ad) in
          (M.LDC, wback, ops)
      | op when chkldcl op && chkcop && chkrn ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
          (M.LDCL, None, ops)
      | op when chkldcl op && chkcop ->
          let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ad) in
          (M.LDCL, wback, ops)
      | op when op land 0b100000 = 0b000000 -> parse_ext_reg_load_store b
      | _ -> Error `Invalid
    in
    (mnemonic, wback, None, ops)

  let parse_other_vfp b =
    let open Result.Let_syntax in
    let b1 = ex b 19 16 in
    let b2 = ex b 7 6 in
    match concat b1 b2 2 with
    | op when op land 0b000001 = 0b000000 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, imm_h) in
        (M.VMOV, None, dt, ops)
    | 0b000001 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VMOV, None, dt, ops)
    | 0b000011 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VABS, None, dt, ops)
    | 0b000101 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VNEG, None, dt, ops)
    | 0b000111 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VSQRT, None, dt, ops)
    | op when op land 0b111011 = 0b001001 ->
        let%bind dt = two_dt_e b in
        let%map ops = p2oprs b dummy_chk (reg_ao, reg_aj) in
        (M.VCVTB, None, dt, ops)
    | op when op land 0b111011 = 0b001011 ->
        let%bind dt = two_dt_e b in
        let%map ops = p2oprs b dummy_chk (reg_ao, reg_aj) in
        (M.VCVTT, None, dt, ops)
    | 0b010001 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VCMP, None, dt, ops)
    | 0b010011 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VCMPE, None, dt, ops)
    | 0b010101 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, fpimm0) in
        (M.VCMP, None, dt, ops)
    | 0b010111 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, fpimm0) in
        (M.VCMPE, None, dt, ops)
    | 0b011111 ->
        let%bind dt = two_dt_d b in
        let%map ops = p2oprs b dummy_chk (reg_al_r, reg_an) in
        (M.VCVT, None, dt, ops)
    | op when op land 0b111101 = 0b100001 ->
        let%bind dt = two_dt_f b in
        let%map ops = p2oprs b dummy_chk (reg_ap, reg_aq) in
        (M.VCVT, None, dt, ops)
    | op when op land 0b111001 = 0b101001 ->
        let%bind dt = two_dt_h b in
        let%map ops = p3oprs b dummy_chk (reg_at, reg_at, imm_i) in
        (M.VCVT, None, dt, ops)
    | op when op land 0b111011 = 0b110001 ->
        let%bind dt = two_dt_f b in
        let%map ops = p2oprs b dummy_chk (reg_ap, reg_aq) in
        (M.VCVT, None, dt, ops)
    | op when op land 0b111011 = 0b110011 ->
        let%bind dt = two_dt_g b in
        let%map ops = p2oprs b dummy_chk (reg_ar, reg_as) in
        (M.VCVTR, None, dt, ops)
    | op when op land 0b111001 = 0b111001 ->
        let%bind dt = two_dt_h b in
        let%map ops = p3oprs b dummy_chk (reg_at, reg_at, imm_i) in
        (M.VCVT, None, dt, ops)
    | _ -> Error `Invalid

  let parse_vfp b =
    let open Result.Let_syntax in
    let%bind simdtyp = one_dt_af b in
    match concat (ex b 23 20) (ex b 7 6) 2 with
    | op when op land 0b101101 = 0b000000 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VMLA, None, simdtyp, ops)
    | op when op land 0b101101 = 0b000001 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VMLS, None, simdtyp, ops)
    | op when op land 0b101101 = 0b000100 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VNMLS, None, simdtyp, ops)
    | op when op land 0b101101 = 0b000101 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VNMLA, None, simdtyp, ops)
    | op when op land 0b101101 = 0b001001 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VNMUL, None, simdtyp, ops)
    | op when op land 0b101101 = 0b001000 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VMUL, None, simdtyp, ops)
    | op when op land 0b101101 = 0b001100 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VADD, None, simdtyp, ops)
    | op when op land 0b101101 = 0b001101 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VSUB, None, simdtyp, ops)
    | op when op land 0b101101 = 0b100000 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VDIV, None, simdtyp, ops)
    | op when op land 0b101101 = 0b100100 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VFNMS, None, simdtyp, ops)
    | op when op land 0b101101 = 0b100101 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VFNMA, None, simdtyp, ops)
    | op when op land 0b101101 = 0b101000 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VFMA, None, simdtyp, ops)
    | op when op land 0b101101 = 0b101001 ->
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VFMS, None, simdtyp, ops)
    | op when op land 0b101100 = 0b101100 -> parse_other_vfp b
    | _ -> Error `Invalid

  let parse_81632b_transfer mode b =
    let open Result.Let_syntax in
    let chkb () = not (tst b 6) in
    let chkop () = not (tst b 20) in
    match concat (ex b 23 20) (tst b 8 |> Bool.to_int) 1 with
    | 0b00000 when chkop () ->
        let%map ops = p2oprs b chk_f (reg_au, reg_d) in
        (M.VMOV, None, None, ops)
    | 0b00000 ->
        let%map ops = p2oprs b chk_g (reg_d, reg_au) in
        (M.VMOV, None, None, ops)
    | 0b00010 when chkop () ->
        let%map ops = p2oprs b chk_f (reg_au, reg_d) in
        (M.VMOV, None, None, ops)
    | 0b00010 ->
        let%map ops = p2oprs b chk_g (reg_d, reg_au) in
        (M.VMOV, None, None, ops)
    | 0b11100 ->
        let%map ops = p2oprs b chk_f (reg_fpscr, reg_d) in
        (M.VMSR, None, None, ops)
    | 0b11110 ->
        let%map ops = p2oprs b (chk_dl mode) (reg_az, reg_fpscr) in
        (M.VMRS, None, None, ops)
    | o when o land 0b10011 = 0b00001 ->
        let%bind dt = one_dt_ag b in
        let%map ops = p2oprs b dummy_chk (scalar_c, reg_d) in
        (M.VMOV, None, dt, ops)
    | o when o land 0b10011 = 0b10001 && chkb () ->
        let%bind dt = one_dt_i b in
        let%map ops = p2oprs b chk_ao (reg_ab, reg_d) in
        (M.VDUP, None, dt, ops)
    | o when o land 0b00011 = 0b00011 ->
        let%bind dt = one_dt_ah b in
        let%map ops = p2oprs b dummy_chk (reg_d, scalar_d) in
        (M.VMOV, None, dt, ops)
    | _ -> Error `Invalid

  let uncond111 b =
    let open Result.Let_syntax in
    let b1 = ex b 24 20 in
    let b2 = tst b 4 |> Bool.to_int in
    let%map mnemonic, ops =
      match concat b1 b2 1 with
      | op when op land 0b100001 = 0 ->
          let%map ops =
            p6oprs b dummy_chk
              (preg_a, imm_4e, creg_a, creg_c, creg_b, imm_3b)
          in
          (M.CDP2, ops)
      | op when op land 0b100011 = 0b000001 ->
          let%map ops =
            p6oprs b chk_bb (preg_a, imm_3c, reg_d, creg_c, creg_b, imm_3b)
          in
          (M.MCR2, ops)
      | op when op land 0b100011 = 0b000011 ->
          let%map ops =
            p6oprs b dummy_chk (preg_a, imm_3c, reg_d, creg_c, creg_b, imm_3b)
          in
          (M.MRC2, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, None, None, ops)

  let parse_group111 b =
    let open Result.Let_syntax in
    let chkcoprc () = ex b 11 9 <> 0b101 in
    let b1 = ex b 24 20 in
    let b2 = tst b 4 |> Bool.to_int in
    let%map mnemonic, wback, simdtyp, ops =
      match concat b1 b2 1 with
      | op when op land 0b100000 = 0b100000 ->
          let%map ops = p1opr b dummy_chk imm_24a in
          (M.SVC, None, None, ops)
      | op when op land 0b100001 = 0b000000 && chkcoprc () ->
          let%map ops =
            p6oprs b dummy_chk
              (preg_a, imm_4e, creg_a, creg_c, creg_b, imm_3b)
          in
          (M.CDP, None, None, ops)
      | op when op land 0b100011 = 0b000001 && chkcoprc () ->
          let%map ops =
            p6oprs b chk_bb (preg_a, imm_3c, reg_d, creg_c, creg_b, imm_3b)
          in
          (M.MCR, None, None, ops)
      | op when op land 0b100011 = 0b000011 && chkcoprc () ->
          let%map ops =
            p6oprs b dummy_chk (preg_a, imm_3c, reg_d, creg_c, creg_b, imm_3b)
          in
          (M.MRC, None, None, ops)
      | op when op land 0b100001 = 0b000000 -> parse_vfp b
      | op when op land 0b100001 = 0b000001 ->
          parse_81632b_transfer Mode.Armv7 b (* ensure ARM mode *)
      | _ -> Error `Invalid
    in
    (mnemonic, wback, simdtyp, ops)

  let uncond000 b =
    let open Result.Let_syntax in
    let chkrn () = tst b 16 in
    let%map mnemonic, ops =
      match ex b 7 4 with
      | op when op land 0b0010 = 0b0000 && not (chkrn ()) -> cps b
      | 0b0000 when chkrn () ->
          let%map ops = p1opr b dummy_chk endian_a in
          (M.SETEND, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, None, None, ops)

  let parse_v7_uncond b =
    let open Result.Let_syntax in
    let%map mnemonic, wback, dt, ops =
      match ex b 27 25 with
      | op when op land 0b111 = 0b000 -> uncond000 b
      | op when op land 0b111 = 0b001 ->
          parse_adv_simd_data_proc b Mode.Armv7
      | op when op land 0b111 = 0b010 -> uncond010 b
      | op when op land 0b111 = 0b011 -> uncond0110 b
      | op when op land 0b111 = 0b100 -> uncond100 b
      | op when op land 0b111 = 0b101 ->
          let%map ops = p1opr b dummy_chk lbl_26a in
          (M.BLX, None, None, ops)
      | op when op land 0b111 = 0b110 -> uncond110 b
      | op when op land 0b111 = 0b111 -> uncond111 b
      | _ -> Error `Invalid
    in
    (mnemonic, None, 0, wback, None, dt, ops, None)

  let parse b =
    let open Result.Let_syntax in
    let%bind cond = parse_cond (ex b 31 28) in
    let op = concat (ex b 27 25) (tst b 4 |> Bool.to_int) 1 in
    match cond with
    | C.UN -> parse_v7_uncond b
    | _ ->
        let%map (mnemonic, wback, simdtyp, ops), cflag =
          match op with
          | op when op land 0b1110 = 0b0000 ->
              let%map x = parse_group000 cond b in
              (x, None)
          | op when op land 0b1110 = 0b0010 ->
              let%map m, wb, tp, ops, cf = parse_group001 b in
              ((m, wb, tp, ops), cf)
          | op when op land 0b1110 = 0b0100 ->
              let%map x = parse_group010 b in
              (x, None)
          | op when op land 0b1111 = 0b0110 ->
              let%map x = parse_group0110 b in
              (x, None)
          | op when op land 0b1111 = 0b0111 ->
              let%map x = parse_group0111 cond b in
              (x, None)
          | op when op land 0b1110 = 0b1000 ->
              let%map x = parse_group100 b in
              (x, None)
          | op when op land 0b1110 = 0b1010 ->
              let%map x = parse_group101 b in
              (x, None)
          | op when op land 0b1110 = 0b1100 ->
              let%map x = parse_group110 b in
              (x, None)
          | op when op land 0b1110 = 0b1110 ->
              let%map x = parse_group111 b in
              (x, None)
          | _ -> Error `Invalid
        in
        (mnemonic, Some cond, 0, wback, None, simdtyp, ops, cflag)
end

module ThumbV7 = struct
  open V7

  let group0_lsl_in_it_block b =
    let open Result.Let_syntax in
    match ex b 10 6 with
    | 0 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.MOV, ops)
    | _ ->
        let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_5d) in
        (M.LSL, ops)

  let parse_group0_in_it_block cond b =
    let open Result.Let_syntax in
    let%map mnemonic, ops =
      match ex b 13 9 with
      | op when op land 0b11100 = 0b00000 -> group0_lsl_in_it_block b
      | op when op land 0b11100 = 0b00100 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_5e) in
          (M.LSR, ops)
      | op when op land 0b11100 = 0b01000 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_5e) in
          (M.ASR, ops)
      | 0b01100 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, reg_g) in
          (M.ADD, ops)
      | 0b01101 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, reg_g) in
          (M.SUB, ops)
      | 0b01110 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_3a) in
          (M.ADD, ops)
      | 0b01111 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_3a) in
          (M.SUB, ops)
      | op when op land 0b11100 = 0b10000 ->
          let%map ops = p2oprs b dummy_chk (reg_j, imm_8a) in
          (M.MOV, ops)
      | op when op land 0b11100 = 0b11000 ->
          let%map ops = p2oprs b dummy_chk (reg_j, imm_8a) in
          (M.ADD, ops)
      | op when op land 0b11100 = 0b11100 ->
          let%map ops = p2oprs b dummy_chk (reg_j, imm_8a) in
          (M.SUB, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, cond, 0, None, None, ops)

  let group0_lsl_out_it_block b =
    let open Result.Let_syntax in
    match ex b 10 6 with
    | 0 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.MOVS, ops)
    | _ ->
        let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_5d) in
        (M.LSLS, ops)

  let parse_group0_out_it_block b =
    let open Result.Let_syntax in
    let%map mnemonic, ops =
      match ex b 13 9 with
      | op when op land 0b11100 = 0b00000 -> group0_lsl_out_it_block b
      | op when op land 0b11100 = 0b00100 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_5e) in
          (M.LSRS, ops)
      | op when op land 0b11100 = 0b01000 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_5e) in
          (M.ASRS, ops)
      | 0b01100 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, reg_g) in
          (M.ADDS, ops)
      | 0b01101 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, reg_g) in
          (M.SUBS, ops)
      | 0b01110 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_3a) in
          (M.ADDS, ops)
      | 0b01111 ->
          let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm_3a) in
          (M.SUBS, ops)
      | op when op land 0b11100 = 0b10000 ->
          let%map ops = p2oprs b dummy_chk (reg_j, imm_8a) in
          (M.MOVS, ops)
      | op when op land 0b11100 = 0b11000 ->
          let%map ops = p2oprs b dummy_chk (reg_j, imm_8a) in
          (M.ADDS, ops)
      | op when op land 0b11100 = 0b11100 ->
          let%map ops = p2oprs b dummy_chk (reg_j, imm_8a) in
          (M.SUBS, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, None, 0, None, None, ops)

  let parse_group0 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    if ex b 13 9 land 0b11100 = 0b10100 then
      let%map ops = p2oprs b dummy_chk (reg_j, imm_8a) in
      (M.CMP, cond, 0, None, None, ops)
    else if List.is_empty ctx.it then parse_group0_out_it_block b
    else parse_group0_in_it_block cond b

  let parse_group1_in_it_block b =
    let open Result.Let_syntax in
    match ex b 9 6 with
    | 0b0000 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.AND, ops)
    | 0b0001 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.EOR, ops)
    | 0b0010 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.LSL, ops)
    | 0b0011 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.LSR, ops)
    | 0b0100 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.ASR, ops)
    | 0b0101 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.ADC, ops)
    | 0b0110 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.SBC, ops)
    | 0b0111 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.ROR, ops)
    | 0b1001 ->
        let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm0) in
        (M.RSB, ops)
    | 0b1100 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.ORR, ops)
    | 0b1101 ->
        let%map ops = p3oprs b dummy_chk (reg_i, reg_h, reg_i) in
        (M.MUL, ops)
    | 0b1110 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.BIC, ops)
    | 0b1111 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.MVN, ops)
    | _ -> Error `Invalid

  let parse_group1_out_it_block b =
    let open Result.Let_syntax in
    match ex b 9 6 with
    | 0b0000 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.ANDS, ops)
    | 0b0001 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.EORS, ops)
    | 0b0010 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.LSLS, ops)
    | 0b0011 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.LSRS, ops)
    | 0b0100 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.ASRS, ops)
    | 0b0101 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.ADCS, ops)
    | 0b0110 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.SBCS, ops)
    | 0b0111 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.RORS, ops)
    | 0b1001 ->
        let%map ops = p3oprs b dummy_chk (reg_i, reg_h, imm0) in
        (M.RSBS, ops)
    | 0b1100 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.ORRS, ops)
    | 0b1101 ->
        let%map ops = p3oprs b dummy_chk (reg_i, reg_h, reg_i) in
        (M.MULS, ops)
    | 0b1110 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.BICS, ops)
    | 0b1111 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.MVNS, ops)
    | _ -> Error `Invalid

  let parse_group1 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let parse_with_it () =
      match ctx.it with
      | [] ->
          let%map m, ops = parse_group1_out_it_block b in
          (m, cond, 0, None, None, ops)
      | _ ->
          let%map m, ops = parse_group1_in_it_block b in
          (m, cond, 0, None, None, ops)
    in
    match ex b 9 6 with
    | 0b1000 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.TST, cond, 0, None, None, ops)
    | 0b1010 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.CMP, cond, 0, None, None, ops)
    | 0b1011 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.CMN, cond, 0, None, None, ops)
    | _ -> parse_with_it ()

  let parse_group2_add (ctx : Context.t) b =
    let open Result.Let_syntax in
    let b1 = tst b 7 |> Bool.to_int in
    let b2 = ex b 2 0 in
    let b3 = ex b 6 3 in
    match (concat b1 b2 3, b3) with
    | 0b1101, _ ->
        let%map ops = p2oprs b dummy_chk (reg_o, reg_p) in
        (M.ADD, ops)
    | _, 0b1101 ->
        let%map ops = p3oprs b (chk_df ctx.it) (reg_o, reg_p, reg_o) in
        (M.ADD, ops)
    | _ ->
        let%map ops = p2oprs b (chk_r ctx.it) (reg_o, reg_p) in
        (M.ADD, ops)

  let parse_group2 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let%map mnemonic, ops =
      match ex b 9 7 with
      | 0b000 | 0b001 -> parse_group2_add ctx b
      | 0b010 | 0b011 ->
          let%map ops = p2oprs b chk_s (reg_o, reg_p) in
          (M.CMP, ops)
      | 0b100 | 0b101 ->
          let%map ops = p2oprs b (chk_df ctx.it) (reg_o, reg_p) in
          (M.MOV, ops)
      | 0b110 ->
          let%map ops = p1opr b (chk_dg ctx.it) reg_p in
          (M.BX, ops)
      | 0b111 ->
          let%map ops = p1opr b (chk_dh ctx.it) reg_p in
          (M.BLX, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, cond, 0, None, None, ops)

  let parse_group3_sub cond b =
    let open Result.Let_syntax in
    match ex b 15 11 with
    | 0b01100 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_e) in
        (M.STR, cond, 0, Some false, None, ops)
    | 0b01101 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_e) in
        (M.LDR, cond, 0, Some false, None, ops)
    | 0b01110 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_f) in
        (M.STRB, cond, 0, Some false, None, ops)
    | 0b01111 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_f) in
        (M.LDRB, cond, 0, Some false, None, ops)
    | 0b10000 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_g) in
        (M.STRH, cond, 0, Some false, None, ops)
    | 0b10001 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_g) in
        (M.LDRH, cond, 0, Some false, None, ops)
    | 0b10010 ->
        let%map ops = p2oprs b dummy_chk (reg_j, mem_c) in
        (M.STR, cond, 0, Some false, None, ops)
    | 0b10011 ->
        let%map ops = p2oprs b dummy_chk (reg_j, mem_c) in
        (M.LDR, cond, 0, Some false, None, ops)
    | _ -> Error `Invalid

  let parse_group3 cond b =
    let open Result.Let_syntax in
    match concat (ex b 15 12) (ex b 11 9) 3 with
    | 0b0101000 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_d) in
        (M.STR, cond, 0, Some false, None, ops)
    | 0b0101001 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_d) in
        (M.STRH, cond, 0, Some false, None, ops)
    | 0b0101010 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_d) in
        (M.STRB, cond, 0, Some false, None, ops)
    | 0b0101011 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_d) in
        (M.LDRSB, cond, 0, Some false, None, ops)
    | 0b0101100 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_d) in
        (M.LDR, cond, 0, None, None, ops)
    | 0b0101101 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_d) in
        (M.LDRH, cond, 0, Some false, None, ops)
    | 0b0101110 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_d) in
        (M.LDRB, cond, 0, Some false, None, ops)
    | 0b0101111 ->
        let%map ops = p2oprs b dummy_chk (reg_i, mem_d) in
        (M.LDRSH, cond, 0, Some false, None, ops)
    | _ -> parse_group3_sub cond b

  let inv_cond cond = cond land 0xE lor (lnot cond land 0b1)

  let it_opc_with_x cond x =
    let condi = inv_cond cond in
    if x then (M.ITT, [cond; cond]) else (M.ITE, [cond; condi])

  let it_opc_with_xy cond x y =
    let condi = inv_cond cond in
    match (x, y) with
    | true, true -> (M.ITTT, [cond; cond; cond])
    | true, false -> (M.ITTE, [cond; cond; condi])
    | false, true -> (M.ITET, [cond; condi; cond])
    | false, false -> (M.ITEE, [cond; condi; condi])

  let it_opc_with_xyz cond x y z =
    let condi = inv_cond cond in
    match (x, y, z) with
    | true, true, true -> (M.ITTTT, [cond; cond; cond; cond])
    | true, true, false -> (M.ITTTE, [cond; cond; cond; condi])
    | true, false, true -> (M.ITTET, [cond; cond; condi; cond])
    | true, false, false -> (M.ITTEE, [cond; cond; condi; condi])
    | false, true, true -> (M.ITTTT, [cond; condi; cond; cond])
    | false, true, false -> (M.ITTTE, [cond; condi; cond; condi])
    | false, false, true -> (M.ITTET, [cond; condi; condi; cond])
    | false, false, false -> (M.ITTEE, [cond; condi; condi; condi])

  let get_it fst_cond cond mask =
    let mask0 = tst mask 0 in
    let mask1 = tst mask 1 in
    let mask2 = tst mask 2 in
    let mask3 = tst mask 3 in
    let x = Bool.equal fst_cond mask3 in
    let y = Bool.equal fst_cond mask2 in
    let z = Bool.equal fst_cond mask1 in
    match (mask3, mask2, mask1, mask0) with
    | true, false, false, false -> Ok (M.IT, [cond])
    | _, true, false, false -> Ok (it_opc_with_x cond x)
    | _, _, true, false -> Ok (it_opc_with_xy cond x y)
    | _, _, _, true -> Ok (it_opc_with_xyz cond x y z)
    | _ -> Error `Invalid

  let if_then_hints cond (ctx : Context.t) b =
    let open Result.Let_syntax in
    match (ex b 7 4, ex b 3 0) with
    | o1, o2 when o2 <> 0b0000 ->
        let%bind m, it = get_it (tst o1 0) o1 o2 in
        let%map ops = p1opr b (chk_bd m ctx.it) first_cond in
        ctx.it <- it;
        ctx.it_started <- true;
        (m, None, b land 0xFF, None, None, ops)
    | 0b0000, _ -> Ok (M.NOP, cond, 0, None, None, e)
    | 0b0001, _ -> Ok (M.YIELD, cond, 0, None, None, e)
    | 0b0010, _ -> Ok (M.WFE, cond, 0, None, None, e)
    | 0b0011, _ -> Ok (M.WFI, cond, 0, None, None, e)
    | 0b0100, _ -> Ok (M.SEV, cond, 0, None, None, e)
    | _ -> Error `Invalid

  let parse_group4 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let b1 = ex b 11 5 in
    match b1 with
    | op when op land 0b1111100 = 0b0000000 ->
        let%map ops = p3oprs b dummy_chk (reg_sp, reg_sp, imm_7a) in
        (M.ADD, cond, 0, None, None, ops)
    | op when op land 0b1111100 = 0b0000100 ->
        let%map ops = p3oprs b dummy_chk (reg_sp, reg_sp, imm_7a) in
        (M.SUB, cond, 0, None, None, ops)
    | op when op land 0b1111000 = 0b0001000 ->
        let%map ops = p2oprs b (chk_de ctx.it) (reg_i, lbl_7a) in
        (M.CBZ, None, 0, None, None, ops)
    | op when op land 0b1111110 = 0b0010000 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.SXTH, cond, 0, None, None, ops)
    | op when op land 0b1111110 = 0b0010010 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.SXTB, cond, 0, None, None, ops)
    | op when op land 0b1111110 = 0b0010100 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.UXTH, cond, 0, None, None, ops)
    | op when op land 0b1111110 = 0b0010110 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.UXTB, cond, 0, None, None, ops)
    | op when op land 0b1111000 = 0b0011000 ->
        let%map ops = p2oprs b (chk_de ctx.it) (reg_i, lbl_7a) in
        (M.CBZ, None, 0, None, None, ops)
    | op when op land 0b1110000 = 0b0100000 ->
        let%map ops = p1opr b chk_bc reglist_n in
        (M.PUSH, cond, 0, None, None, ops)
    | 0b0110010 ->
        let%map ops = p1opr b (chk_de ctx.it) endian_b in
        (M.SETEND, None, 0, None, None, ops)
    | 0b0110011 when not (tst b 4) ->
        let%map ops = p1opr b (chk_de ctx.it) flag_b in
        (M.CPSIE, None, 0, None, None, ops)
    | 0b0110011 ->
        let%map ops = p1opr b (chk_de ctx.it) flag_b in
        (M.CPSID, None, 0, None, None, ops)
    | op when op land 0b1111000 = 0b1001000 ->
        let%map ops = p2oprs b (chk_de ctx.it) (reg_i, lbl_7a) in
        (M.CBNZ, None, 0, None, None, ops)
    | op when op land 0b1111110 = 0b1010000 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.REV, cond, 0, None, None, ops)
    | op when op land 0b1111110 = 0b1010010 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.REV16, cond, 0, None, None, ops)
    | op when op land 0b1111110 = 0b1010110 ->
        let%map ops = p2oprs b dummy_chk (reg_i, reg_h) in
        (M.REVSH, cond, 0, None, None, ops)
    | op when op land 0b1111000 = 0b1011000 ->
        let%map ops = p2oprs b (chk_de ctx.it) (reg_i, lbl_7a) in
        (M.CBNZ, None, 0, None, None, ops)
    | op when op land 0b1110000 = 0b1100000 ->
        let%map ops = p1opr b chk_bc reglist_o in
        (M.POP, cond, 0, None, None, ops)
    | op when op land 0b1111000 = 0b1110000 ->
        let%map ops = p1opr b dummy_chk imm_8a in
        (M.BKPT, None, 0, None, None, ops)
    | op when op land 0b1111000 = 0b1111000 -> if_then_hints cond ctx b
    | _ -> Error `Invalid

  let parse_group5 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let bcond c =
      let%map c = parse_cond c in
      Some c
    in
    match ex b 11 8 with
    | 0b1110 ->
        let%map ops = p1opr b dummy_chk imm_8a in
        (M.UDF, cond, 0, None, None, ops)
    | 0b1111 ->
        let%map ops = p1opr b dummy_chk imm_8a in
        (M.SVC, cond, 0, None, None, ops)
    | c ->
        let%bind cond = bcond c in
        let%map ops = p1opr b (chk_be ctx.it) lbl_9a in
        (M.B, cond, 0, None, qf_n (), ops)

  let parse_group6 (ctx : Context.t) b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let b' = concat b1 b2 16 in
    let b15 = tst b1 5 |> Bool.to_int in
    let b14 = tst b1 4 |> Bool.to_int in
    let chkwrn = concat b15 (ex b1 3 0) 4 = 0b11101 in
    let wback = Some (tst b1 5) in
    match concat (ex b1 8 7) b14 1 with
    | 0b000 ->
        let%map ops = p2oprs b' dummy_chk (reg_m, imm_5b) in
        (M.SRSDB, wback, None, None, ops)
    | 0b001 ->
        let%map ops = p1opr b' (chk_di ctx.it) reg_aa in
        (M.RFEDB, wback, None, None, ops)
    | 0b010 ->
        let%map ops = p2oprs (b1, b2) chk_bf (reg_wb, reglist_p) in
        (M.STM, wback, qf_w (), None, ops)
    | 0b011 when chkwrn ->
        let%map ops = p1opr (b1, b2) (chk_bg ctx.it) reglist_q in
        (M.POP, None, qf_w (), None, ops)
    | 0b011 ->
        let%map ops = p2oprs (b1, b2) (chk_bh ctx.it) (reg_wb, reglist_q) in
        (M.LDM, wback, qf_w (), None, ops)
    | 0b100 when chkwrn ->
        let%map ops = p1opr (b1, b2) chk_bi reglist_p in
        (M.PUSH, None, qf_w (), None, ops)
    | 0b100 ->
        let%map ops = p2oprs (b1, b2) chk_bf (reg_wb, reglist_p) in
        (M.STMDB, wback, None, None, ops)
    | 0b101 ->
        let%map ops = p2oprs (b1, b2) (chk_bh ctx.it) (reg_wb, reglist_q) in
        (M.LDMDB, wback, None, None, ops)
    | 0b110 ->
        let%map ops = p2oprs b' dummy_chk (reg_m, imm_5b) in
        (M.SRSIA, wback, None, None, ops)
    | 0b111 ->
        let%map ops = p1opr b' (chk_di ctx.it) reg_aa in
        (M.RFEIA, wback, None, None, ops)
    | _ -> Error `Invalid

  let parse_group7_not_010 b1 b2 =
    let open Result.Let_syntax in
    let op12 = concat (ex b1 8 7) (ex b1 5 4) 2 in
    let isrn1111 = ex b1 3 0 = 0b1111 in
    let wback () = Some (tst b1 5) in
    match op12 with
    | o when o land 0b1111 = 0b0000 ->
        let%map ops = p3oprs (b1, b2) chk_bj (reg_av, reg_aw, mem_af) in
        (M.STREX, None, ops)
    | o when o land 0b1111 = 0b0001 ->
        let%map ops = p2oprs (b1, b2) chk_bk (reg_aw, mem_af) in
        (M.LDREX, None, ops)
    | o when o land 0b1011 = 0b0010 ->
        let%map ops = p3oprs (b1, b2) chk_bm (reg_aw, reg_av, mem_ah) in
        (M.STRD, wback (), ops)
    | o when o land 0b1001 = 0b1000 ->
        let%map ops = p3oprs (b1, b2) chk_bm (reg_aw, reg_av, mem_ah) in
        (M.STRD, wback (), ops)
    | o when o land 0b1011 = 0b0011 && not isrn1111 ->
        let%map ops = p3oprs (b1, b2) chk_bn (reg_aw, reg_av, mem_ah) in
        (M.LDRD, wback (), ops)
    | o when o land 0b1001 = 0b1001 && not isrn1111 ->
        let%map ops = p3oprs (b1, b2) chk_bn (reg_aw, reg_av, mem_ah) in
        (M.LDRD, wback (), ops)
    | o when o land 0b1011 = 0b0011 && isrn1111 ->
        let%map ops = p3oprs (b1, b2) chk_bo (reg_aw, reg_av, mem_ai) in
        (M.LDRD, wback (), ops)
    | o when o land 0b1001 = 0b1001 && isrn1111 ->
        let%map ops = p3oprs (b1, b2) chk_bo (reg_aw, reg_av, mem_ai) in
        (M.LDRD, wback (), ops)
    | _ -> Error `Invalid

  let parse_group7_with_010 (ctx : Context.t) b1 b2 =
    let open Result.Let_syntax in
    let b14 = tst b1 4 |> Bool.to_int in
    match concat b14 (ex b2 6 4) 3 with
    | 0b0100 ->
        let%map ops = p3oprs (b1, b2) chk_bj (reg_ax, reg_aw, mem_aj) in
        (M.STREXB, None, ops)
    | 0b0101 ->
        let%map ops = p3oprs (b1, b2) chk_bj (reg_ax, reg_aw, mem_aj) in
        (M.STREXH, None, ops)
    | 0b0111 ->
        let%map ops =
          p4oprs (b1, b2) chk_bq (reg_ax, reg_aw, reg_av, mem_aj)
        in
        (M.STREXD, None, ops)
    | 0b1000 ->
        let%map ops = p1opr (b1, b2) (chk_br ctx.it) mem_ak in
        (M.TBB, None, ops)
    | 0b1001 ->
        let%map ops = p1opr (b1, b2) (chk_br ctx.it) mem_al in
        (M.TBH, None, ops)
    | 0b1100 ->
        let%map ops = p2oprs (b1, b2) chk_bs (reg_aw, mem_aj) in
        (M.LDREXB, None, ops)
    | 0b1101 ->
        let%map ops = p2oprs (b1, b2) chk_bs (reg_aw, mem_aj) in
        (M.LDREXH, None, ops)
    | 0b1111 ->
        let%map ops = p3oprs (b1, b2) chk_bp (reg_aw, reg_av, mem_aj) in
        (M.LDREXD, None, ops)
    | _ -> Error `Invalid

  let parse_group7 (ctx : Context.t) b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let%map mnemonic, wback, ops =
      match (ex b1 8 7, tst b1 5) with
      | 0b01, false -> parse_group7_with_010 ctx b1 b2
      | _ -> parse_group7_not_010 b1 b2
    in
    (mnemonic, wback, None, None, ops)

  let parse_mov_reg_imm_shift b1 b2 =
    let open Result.Let_syntax in
    let imm = concat (ex b2 14 12) (ex b2 7 6) 2 in
    let oprs2 () = p2oprs (b1, b2) in
    let oprs3 () = p3oprs (b1, b2) in
    match (ex b2 5 4, imm, tst b1 4) with
    | 0b00, 0, false ->
        let%map ops = oprs2 () chk_bt (reg_av, reg_ax) in
        (M.MOV, qf_w (), ops)
    | 0b00, 0, true ->
        let%map ops = oprs2 () chk_bu (reg_av, reg_ax) in
        (M.MOVS, qf_w (), ops)
    | 0b00, _, false ->
        let%map ops = oprs3 () chk_bv (reg_av, reg_ax, imm_5g) in
        (M.LSL, qf_w (), ops)
    | 0b00, _, true ->
        let%map ops = oprs3 () chk_bv (reg_av, reg_ax, imm_5g) in
        (M.LSLS, qf_w (), ops)
    | 0b01, _, false ->
        let%map ops = oprs3 () chk_bv (reg_av, reg_ax, imm_5g) in
        (M.LSR, qf_w (), ops)
    | 0b01, _, true ->
        let%map ops = oprs3 () chk_bv (reg_av, reg_ax, imm_5g) in
        (M.LSRS, qf_w (), ops)
    | 0b10, _, false ->
        let%map ops = oprs3 () chk_bv (reg_av, reg_ax, imm_5g) in
        (M.ASR, qf_w (), ops)
    | 0b10, _, true ->
        let%map ops = oprs3 () chk_bv (reg_av, reg_ax, imm_5g) in
        (M.ASRS, qf_w (), ops)
    | 0b11, 0, false ->
        let%map ops = oprs2 () chk_bu (reg_av, reg_ax) in
        (M.RRX, None, ops)
    | 0b11, 0, true ->
        let%map ops = oprs2 () chk_bu (reg_av, reg_ax) in
        (M.RRXS, None, ops)
    | 0b11, _, false ->
        let%map ops = oprs3 () chk_bv (reg_av, reg_ax, imm_5g) in
        (M.ROR, qf_w (), ops)
    | 0b11, _, true ->
        let%map ops = oprs3 () chk_bv (reg_av, reg_ax, imm_5g) in
        (M.RORS, qf_w (), ops)
    | _ -> Error `Invalid

  let parse_group8_with_rd_sub b1 b2 =
    let open Result.Let_syntax in
    let oprs = (reg_av, reg_ay, reg_ax, shift_f) in
    match ex b1 8 4 with
    | 0b00000 ->
        let%map ops = p4oprs (b1, b2) chk_bx oprs in
        (M.AND, qf_w (), ops)
    | 0b00001 ->
        let%map ops = p4oprs (b1, b2) chk_by oprs in
        (M.ANDS, qf_w (), ops)
    | 0b01000 ->
        let%map ops = p4oprs (b1, b2) chk_bx oprs in
        (M.EOR, qf_w (), ops)
    | 0b01001 ->
        let%map ops = p4oprs (b1, b2) chk_by oprs in
        (M.EORS, qf_w (), ops)
    | 0b10000 ->
        let%map ops = p4oprs (b1, b2) chk_ca oprs in
        (M.ADD, qf_w (), ops)
    | 0b10001 ->
        let%map ops = p4oprs (b1, b2) chk_cb oprs in
        (M.ADDS, qf_w (), ops)
    | 0b11010 ->
        let%map ops = p4oprs (b1, b2) chk_ca oprs in
        (M.SUB, qf_w (), ops)
    | 0b11011 ->
        let%map ops = p4oprs (b1, b2) chk_cb oprs in
        (M.SUBS, qf_w (), ops)
    | _ -> Error `Invalid

  let parse_group8_with_rd b1 b2 =
    let open Result.Let_syntax in
    let b14 = tst b1 4 |> Bool.to_int in
    let isnotrds11111 = concat (ex b2 11 8) b14 1 <> 0b11111 in
    let opr chk = p3oprs (b1, b2) chk (reg_ay, reg_ax, shift_f) in
    if isnotrds11111 then parse_group8_with_rd_sub b1 b2
    else
      match ex b1 8 5 with
      | 0b0000 ->
          let%map ops = opr chk_bv in
          (M.TST, qf_w (), ops)
      | 0b0100 ->
          let%map ops = opr chk_bv in
          (M.TEQ, qf_w (), ops)
      | 0b1000 ->
          let%map ops = opr chk_bw in
          (M.CMN, qf_w (), ops)
      | 0b1101 ->
          let%map ops = opr chk_bw in
          (M.CMP, qf_w (), ops)
      | _ -> Error `Invalid

  let parse_group8_with_rn_sub b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic =
      match (ex b1 6 5, tst b1 4) with
      | 0b10, false -> Ok M.ORR
      | 0b10, true -> Ok M.ORRS
      | 0b11, false -> Ok M.ORN
      | 0b11, true -> Ok M.ORNS
      | _ -> Error `Invalid
    in
    let%map ops = p4oprs (b1, b2) chk_bx (reg_av, reg_ay, reg_ax, shift_f) in
    (mnemonic, qf_w (), ops)

  let parse_group8_with_rn b1 b2 =
    let open Result.Let_syntax in
    if ex b1 3 0 <> 0b1111 then parse_group8_with_rn_sub b1 b2
    else
      match ex b1 6 4 with
      | 0b100 | 0b101 -> parse_mov_reg_imm_shift b1 b2
      | 0b110 ->
          let%map ops = p3oprs (b1, b2) chk_bv (reg_av, reg_ax, shift_f) in
          (M.MVN, qf_w (), ops)
      | 0b111 ->
          let%map ops = p3oprs (b1, b2) chk_bv (reg_av, reg_ax, shift_f) in
          (M.MVNS, qf_w (), ops)
      | _ -> Error `Invalid

  let parse_group8_pkh b1 b2 =
    let open Result.Let_syntax in
    let%map ops =
      p4oprs (b1, b2) chk_both_b (reg_av, reg_ay, reg_ax, shift_f)
    in
    let mnemonic = if tst b2 5 then M.PKHTB else M.PKHBT in
    (mnemonic, None, ops)

  let parse_group8_with_s b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic =
      match ex b1 8 4 with
      | 0b00010 -> Ok M.BIC
      | 0b00011 -> Ok M.BICS
      | 0b10100 -> Ok M.ADC
      | 0b10101 -> Ok M.ADCS
      | 0b10110 -> Ok M.SBC
      | 0b10111 -> Ok M.SBCS
      | 0b11100 -> Ok M.RSB
      | 0b11101 -> Ok M.RSBS
      | _ -> Error `Invalid
    in
    let%map ops = p4oprs (b1, b2) chk_bx (reg_av, reg_ay, reg_ax, shift_f) in
    (mnemonic, qf_w (), ops)

  let parse_group8 b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let%map mnemonic, q, ops =
      match ex b1 8 5 with
      | 0b0000 -> parse_group8_with_rd b1 b2
      | 0b0001 -> parse_group8_with_s b1 b2
      | 0b0010 | 0b0011 -> parse_group8_with_rn b1 b2
      | 0b0100 -> parse_group8_with_rd b1 b2
      | 0b0110 -> parse_group8_pkh b1 b2
      | 0b1000 -> parse_group8_with_rd b1 b2
      | 0b1010 | 0b1011 -> parse_group8_with_s b1 b2
      | 0b1101 -> parse_group8_with_rd b1 b2
      | 0b1110 -> parse_group8_with_s b1 b2
      | _ -> Error `Invalid
    in
    (mnemonic, None, q, None, ops)

  let parse_group9_mcrr b =
    let open Result.Let_syntax in
    if tst b 28 then
      let%map ops = p5oprs b chk_au (preg_a, imm_4d, reg_d, reg_c, creg_b) in
      (M.MCRR2, None, ops)
    else
      let%map ops = p5oprs b chk_au (preg_a, imm_4d, reg_d, reg_c, creg_b) in
      (M.MCRR, None, ops)

  let parse_group9_mrrc b =
    let open Result.Let_syntax in
    if tst b 28 then
      let%map ops = p5oprs b chk_au (preg_a, imm_4d, reg_d, reg_c, creg_b) in
      (M.MRRC2, None, ops)
    else
      let%map ops = p5oprs b chk_au (preg_a, imm_4d, reg_d, reg_c, creg_b) in
      (M.MRRC, None, ops)

  let parse_group9_stc b =
    let open Result.Let_syntax in
    let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
    let wback = Some (tst b 21) in
    let mnemonic =
      match (tst b 28, tst b 22) with
      | false, false -> M.STC
      | false, true -> M.STCL
      | true, false -> M.STC2
      | true, true -> M.STC2L
    in
    (mnemonic, wback, ops)

  let parse_group9_ldc b =
    let open Result.Let_syntax in
    let chk = ex b 19 16 = 0b1111 in
    let%map ops =
      if chk then p3oprs b dummy_chk (preg_a, creg_a, mem_ad)
      else p3oprs b dummy_chk (preg_a, creg_a, mem_ae)
    in
    let wback = if chk then None else Some (tst b 21) in
    let mnemonic =
      match (tst b 28, tst b 22) with
      | false, false -> M.LDC
      | false, true -> M.LDCL
      | true, false -> M.LDC2
      | true, true -> M.LDC2L
    in
    (mnemonic, wback, ops)

  let parse_group9_cdpmrc b =
    let open Result.Let_syntax in
    match (tst b 28, tst b 20, tst b 4) with
    | false, _, false ->
        let%map ops =
          p6oprs b dummy_chk (preg_a, imm_4e, creg_a, creg_c, creg_b, imm_3b)
        in
        (M.CDP, None, ops)
    | true, _, false ->
        let%map ops =
          p6oprs b dummy_chk (preg_a, imm_4e, creg_a, creg_c, creg_b, imm_3b)
        in
        (M.CDP2, None, ops)
    | false, false, true ->
        let%map ops =
          p6oprs b dummy_chk (preg_a, imm_3c, reg_d, creg_c, creg_b, imm_3b)
        in
        (M.MCR, None, ops)
    | true, false, true ->
        let%map ops =
          p6oprs b dummy_chk (preg_a, imm_3c, reg_d, creg_c, creg_b, imm_3b)
        in
        (M.MCR2, None, ops)
    | false, true, true ->
        let%map ops =
          p6oprs b dummy_chk (preg_a, imm_3c, reg_d, creg_c, creg_b, imm_3b)
        in
        (M.MRC, None, ops)
    | true, true, true ->
        let%map ops =
          p6oprs b dummy_chk (preg_a, imm_3c, reg_d, creg_c, creg_b, imm_3b)
        in
        (M.MRC2, None, ops)

  let parse_group9_sub2 b1 b2 =
    let open Result.Let_syntax in
    let b = concat b1 b2 16 in
    let%map mnemonic, wback, ops =
      match ex b1 9 4 with
      | 0b000100 -> parse_group9_mcrr b
      | 0b000101 -> parse_group9_mrrc b
      | op when op land 0b100001 = 0b000000 -> parse_group9_stc b
      | op when op land 0b100001 = 0b000001 -> parse_group9_ldc b
      | op when op land 0b110000 = 0b100000 -> parse_group9_cdpmrc b
      | _ -> Error `Invalid
    in
    (mnemonic, wback, None, ops)

  let parse_group9_sub b1 b2 =
    let b = concat b1 b2 16 in
    match tst b1 9 with
    | false -> Error `Invalid
    | true -> V7.parse_adv_simd_data_proc b Mode.Thumbv7

  let parse_group9_sub3 b1 b2 =
    let open Result.Let_syntax in
    let b24 = tst b2 4 |> Bool.to_int in
    let op = concat (ex b1 9 4) b24 1 in
    let b = concat b1 b2 16 in
    let chk () = op land 0b1110100 <> 0b0000000 in
    match tst b1 12 with
    | true -> Error `Invalid
    | false -> (
      match op with
      | o when o land 0b1000000 = 0b0000000 && chk () ->
          let%map mnemonic, wback, ops = parse_ext_reg_load_store b in
          (mnemonic, wback, None, ops)
      | o when o land 0b1111100 = 0b0001000 ->
          let%map mnemonic, wback, ops = parse_64bit_transfer b in
          (mnemonic, wback, None, ops)
      | o when o land 0b1000001 = 0b1000000 -> parse_vfp b
      | o when o land 0b1000001 = 0b1000001 ->
          parse_81632b_transfer Mode.Thumbv7 b
      | _ -> Error `Invalid )

  let parse_group9 b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let op1 = ex b1 9 4 in
    let chkcoproc = ex b2 11 8 land 0b1110 <> 0b1010 in
    let chksub = op1 = 0 || op1 = 1 || op1 land 0b110000 = 0b110000 in
    let%map mnemonic, wback, dt, ops =
      if chksub then parse_group9_sub b1 b2
      else if chkcoproc then parse_group9_sub2 b1 b2
      else parse_group9_sub3 b1 b2
    in
    (mnemonic, wback, None, dt, ops)

  let parse_group10_with_rd_sub b1 b2 =
    let open Result.Let_syntax in
    let op = ex b1 8 4 in
    match op with
    | 0b00000 ->
        let%map ops = p3oprs (b1, b2) chk_bv (reg_av, reg_ay, imm_j) in
        (M.AND, None, ops, None)
    | 0b00001 ->
        let%map ops = p3oprs (b1, b2) chk_cd (reg_av, reg_ay, imm_j) in
        (M.ANDS, None, ops, cf_thumb (b1, b2))
    | 0b01000 ->
        let%map ops = p3oprs (b1, b2) chk_bv (reg_av, reg_ay, imm_j) in
        (M.EOR, None, ops, None)
    | 0b01001 ->
        let%map ops = p3oprs (b1, b2) chk_cd (reg_av, reg_ay, imm_j) in
        (M.EORS, None, ops, cf_thumb (b1, b2))
    | 0b10000 ->
        let%map ops = p3oprs (b1, b2) chk_cf (reg_av, reg_ay, imm_j) in
        (M.ADD, qf_w (), ops, None)
    | 0b10001 ->
        let%map ops = p3oprs (b1, b2) chk_cg (reg_av, reg_ay, imm_j) in
        (M.ADDS, qf_w (), ops, None)
    | 0b11010 ->
        let%map ops = p3oprs (b1, b2) chk_cf (reg_av, reg_ay, imm_j) in
        (M.SUB, qf_w (), ops, None)
    | 0b11011 ->
        let%map ops = p3oprs (b1, b2) chk_cg (reg_av, reg_ay, imm_j) in
        (M.SUBS, qf_w (), ops, None)
    | _ -> Error `Invalid

  let parse_group10_with_rd b1 b2 =
    let open Result.Let_syntax in
    let b14 = tst b1 4 |> Bool.to_int in
    let isrds11111 = concat (ex b2 11 8) b14 1 = 0b11111 in
    if not isrds11111 then parse_group10_with_rd_sub b1 b2
    else
      match ex b1 8 5 with
      | 0b0000 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_ay, imm_j) in
          (M.TST, None, ops, cf_thumb (b1, b2))
      | 0b0100 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_ay, imm_j) in
          (M.TEQ, None, ops, cf_thumb (b1, b2))
      | 0b1000 ->
          let%map ops = p2oprs (b1, b2) chk_cc (reg_ay, imm_j) in
          (M.CMN, None, ops, None)
      | 0b1101 ->
          let%map ops = p2oprs (b1, b2) chk_cc (reg_ay, imm_j) in
          (M.CMP, qf_w (), ops, None)
      | _ -> Error `Invalid

  let parse_group10_with_rn_sub b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic, cflag =
      match ex b1 6 4 with
      | 0b100 -> Ok (M.ORR, None)
      | 0b101 -> Ok (M.ORRS, cf_thumb (b1, b2))
      | 0b110 -> Ok (M.ORN, None)
      | 0b111 -> Ok (M.ORNS, cf_thumb (b1, b2))
      | _ -> Error `Invalid
    in
    let%map ops = p3oprs (b1, b2) chk_ce (reg_av, reg_ay, imm_j) in
    (mnemonic, None, ops, cflag)

  let parse_group10_with_rn b1 b2 =
    let open Result.Let_syntax in
    if ex b1 3 0 <> 0b1111 then parse_group10_with_rn_sub b1 b2
    else
      match ex b1 6 4 with
      | 0b100 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_av, imm_j) in
          (M.MOV, qf_w (), ops, None)
      | 0b101 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_av, imm_j) in
          (M.MOVS, qf_w (), ops, cf_thumb (b1, b2))
      | 0b110 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_av, imm_j) in
          (M.MVN, None, ops, None)
      | 0b111 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_av, imm_j) in
          (M.MVNS, None, ops, cf_thumb (b1, b2))
      | _ -> Error `Invalid

  let parse_group10_with_s b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic, aux, cflag =
      match ex b1 8 4 with
      | 0b00010 -> Ok (M.BIC, None, None)
      | 0b00011 -> Ok (M.BICS, None, cf_thumb (b1, b2))
      | 0b10100 -> Ok (M.ADC, None, None)
      | 0b10101 -> Ok (M.ADCS, None, None)
      | 0b10110 -> Ok (M.SBC, None, None)
      | 0b10111 -> Ok (M.SBCS, None, None)
      | 0b11100 -> Ok (M.RSB, qf_w (), None)
      | 0b11101 -> Ok (M.RSBS, qf_w (), None)
      | _ -> Error `Invalid
    in
    let%map ops = p3oprs (b1, b2) chk_bv (reg_av, reg_ay, imm_j) in
    (mnemonic, aux, ops, cflag)

  let parse_group10 cond b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let%map mnemonic, q, ops, cflag =
      match ex b1 8 5 with
      | 0b0000 -> parse_group10_with_rd b1 b2
      | 0b0001 -> parse_group10_with_s b1 b2
      | 0b0010 | 0b0011 -> parse_group10_with_rn b1 b2
      | 0b0100 | 0b1000 -> parse_group10_with_rd b1 b2
      | 0b1010 | 0b1011 -> parse_group10_with_s b1 b2
      | 0b1101 -> parse_group10_with_rd b1 b2
      | 0b1110 -> parse_group10_with_s b1 b2
      | _ -> Error `Invalid
    in
    (mnemonic, cond, q, ops, cflag)

  let parse_group11 cond b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let chkrn = ex b1 3 0 <> 0b1111 in
    let chka = concat (ex b2 14 12) (ex b2 7 6) 2 <> 0b00000 in
    let%map mnemonic, ops =
      match ex b1 8 4 with
      | 0b00000 when not chkrn ->
          let%map ops = p3oprs (b1, b2) chk_dm (reg_av, reg_ay, imm_12f) in
          (M.ADDW, ops)
      | 0b00000 ->
          let%map ops = p3oprs (b1, b2) chk_ch (reg_av, reg_ay, imm_12f) in
          (M.ADDW, ops)
      | 0b00100 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_av, imm_16a) in
          (M.MOVW, ops)
      | 0b01010 when not chkrn ->
          let%map ops = p3oprs (b1, b2) chk_dm (reg_av, reg_ay, imm_12f) in
          (M.SUBW, ops)
      | 0b01010 ->
          let%map ops = p3oprs (b1, b2) chk_ch (reg_av, reg_ay, imm_12f) in
          (M.SUBW, ops)
      | 0b01100 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_av, imm_16a) in
          (M.MOVT, ops)
      | 0b10000 ->
          let%map ops =
            p4oprs (b1, b2) chk_ci (reg_av, imm_4f, reg_ay, shift_i)
          in
          (M.SSAT, ops)
      | 0b10010 when chka ->
          let%map ops =
            p4oprs (b1, b2) chk_ci (reg_av, imm_4f, reg_ay, shift_i)
          in
          (M.SSAT, ops)
      | 0b10010 ->
          let%map ops = p3oprs (b1, b2) chk_cj (reg_av, imm_4f, reg_ay) in
          (M.SSAT16, ops)
      | 0b10100 ->
          let%map ops =
            p4oprs (b1, b2) chk_ck (reg_av, reg_ay, imm_5g, imm_4f)
          in
          (M.SBFX, ops)
      | 0b10110 when chkrn ->
          let%map ops =
            p4oprs (b1, b2) chk_cl (reg_av, reg_ay, imm_5g, imm_k)
          in
          (M.BFI, ops)
      | 0b10110 ->
          let%map ops = p3oprs (b1, b2) chk_cm (reg_av, imm_5g, imm_k) in
          (M.BFC, ops)
      | 0b11000 ->
          let%map ops =
            p4oprs (b1, b2) chk_ci (reg_av, imm_4f, reg_ay, shift_i)
          in
          (M.USAT, ops)
      | 0b11010 when chka ->
          let%map ops =
            p4oprs (b1, b2) chk_ci (reg_av, imm_4f, reg_ay, shift_i)
          in
          (M.USAT, ops)
      | 0b11010 ->
          let%map ops = p3oprs (b1, b2) chk_cj (reg_av, imm_4f, reg_ay) in
          (M.USAT16, ops)
      | 0b11100 ->
          let%map ops =
            p4oprs (b1, b2) chk_ck (reg_av, reg_ay, imm_5g, imm_4f)
          in
          (M.UBFX, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, cond, None, ops)

  let parse_change_proc_state_hints_cps (ctx : Context.t) b1 b2 =
    let open Result.Let_syntax in
    let%map mnemonic, ops =
      match ex b2 10 8 with
      | 0b100 ->
          let%map ops = p1opr (b1, b2) (chk_cs ctx.it) flag_c in
          (M.CPSIE, ops)
      | 0b101 ->
          let%map ops = p2oprs (b1, b2) (chk_cs ctx.it) (flag_c, imm_5h) in
          (M.CPSIE, ops)
      | 0b110 ->
          let%map ops = p1opr (b1, b2) (chk_cs ctx.it) flag_c in
          (M.CPSID, ops)
      | 0b111 ->
          let%map ops = p2oprs (b1, b2) (chk_cs ctx.it) (flag_c, imm_5h) in
          (M.CPSID, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, None, qf_w (), ops)

  let parse_change_proc_state_hints (ctx : Context.t) cond b1 b2 =
    let open Result.Let_syntax in
    match (ex b2 10 8, ex b2 7 0) with
    | 0b000, 0b00000000 -> Ok (M.NOP, cond, qf_w (), e)
    | 0b000, 0b00000001 -> Ok (M.YIELD, cond, qf_w (), e)
    | 0b000, 0b00000010 -> Ok (M.WFE, cond, qf_w (), e)
    | 0b000, 0b00000011 -> Ok (M.WFI, cond, qf_w (), e)
    | 0b000, 0b00000100 -> Ok (M.SEV, cond, qf_w (), e)
    | 0b000, o2 when o2 land 0b11110000 = 0b11110000 ->
        let%map ops = p1opr b2 dummy_chk imm_4a in
        (M.DBG, cond, None, ops)
    | 0b001, _ ->
        let%map ops = p1opr (b1, b2) (chk_cs ctx.it) imm_5h in
        (M.CPS, None, None, ops)
    | 0b010, _ -> Error `Invalid
    | 0b011, _ -> Error `Invalid
    | _ -> parse_change_proc_state_hints_cps ctx b1 b2

  let parse_misc cond b2 =
    let open Result.Let_syntax in
    let%map mnemonic, cond, ops =
      match ex b2 7 4 with
      | 0b0000 -> Ok (M.LEAVEX, None, e)
      | 0b0001 -> Ok (M.ENTERX, None, e)
      | 0b0010 -> Ok (M.CLREX, cond, e)
      | 0b0100 ->
          let%map ops = p1opr b2 dummy_chk opt_a in
          (M.DSB, cond, ops)
      | 0b0101 ->
          let%map ops = p1opr b2 dummy_chk opt_a in
          (M.DMB, cond, ops)
      | 0b0110 ->
          let%map ops = p1opr b2 dummy_chk opt_a in
          (M.ISB, cond, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, cond, None, ops)

  let parse_group12_sub (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let chkbit5 = tst b2 5 in
    let chkop2 = ex b2 9 8 = 0b00 in
    let chki8 = ex b2 7 0 = 0b00000000 in
    let%map mnemonic, cond, q, ops =
      match ex b1 10 4 with
      | op when op land 0b0111000 <> 0b0111000 ->
          let%bind cond = parse_cond (ex b1 9 6) in
          let%map ops = p1opr (b1, b2) (chk_de ctx.it) lbl_21a in
          (M.B, Some cond, qf_w (), ops)
      | op when op land 0b1111110 = 0b0111000 && chkbit5 ->
          let%map ops = p2oprs (b1, b2) chk_cn (banked_reg_b, reg_ay) in
          (M.MSR, cond, None, ops)
      | 0b0111000 when (not chkbit5) && chkop2 ->
          let%map ops = p2oprs (b1, b2) chk_co (apsr_xc, reg_ay) in
          (M.MSR, cond, None, ops)
      | 0b0111000 when not chkbit5 ->
          let%map ops = p2oprs (b1, b2) chk_cp (psr_xa, reg_ay) in
          (M.MSR, cond, None, ops)
      | 0b0111010 -> parse_change_proc_state_hints ctx cond b1 b2
      | 0b0111011 -> parse_misc cond b2
      | 0b0111100 ->
          let%map ops = p1opr (b1, b2) chk_cq reg_ay in
          (M.BXJ, cond, None, ops)
      | 0b0111101 when chki8 -> Ok (M.ERET, cond, None, e)
      | 0b0111101 ->
          let%map ops = p3oprs b2 dummy_chk (reg_pc, reg_lr, imm_8a) in
          (M.SUBS, cond, None, ops)
      | op when op land 0b1111110 = 0b0111110 && chkbit5 ->
          let%map ops = p2oprs (b1, b2) chk_cn (banked_reg_c, reg_av) in
          (M.MRS, cond, None, ops)
      | op when op land 0b1111110 = 0b0111110 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_av, psr_xb) in
          (M.MRS, cond, None, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, cond, q, ops)

  let parse_group12 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let chka = ex b1 10 4 = 0b1111110 in
    let chkb = ex b1 10 4 = 0b1111111 in
    match ex b2 14 12 with
    | 0b000 when chka ->
        let%map ops = p1opr (b1, b2) dummy_chk imm_16b in
        (M.HVC, None, None, ops)
    | 0b000 when chkb ->
        let%map ops = p1opr b dummy_chk imm_4a in
        (M.SMC, cond, None, ops)
    | 0b010 when chkb ->
        let%map ops = p1opr (b1, b2) dummy_chk imm_16b in
        (M.UDF, cond, None, ops)
    | op when op land 0b101 = 0b000 -> parse_group12_sub ctx cond b
    | op when op land 0b101 = 0b001 ->
        let%map ops = p1opr (b1, b2) (chk_dg ctx.it) lbl_25a in
        (M.B, cond, qf_w (), ops)
    | op when op land 0b101 = 0b100 ->
        let%map ops = p1opr (b1, b2) chk_cr lbl_25b in
        (M.BLX, cond, None, ops)
    | op when op land 0b101 = 0b101 ->
        let%map ops = p1opr (b1, b2) dummy_chk lbl_25c in
        (M.BL, cond, None, ops)
    | _ -> Error `Invalid

  let parse_group13_sub b1 b2 =
    let open Result.Let_syntax in
    let crn = ex b1 3 0 = 0b1101 in
    let cpush = ex b2 5 0 = 0b000100 in
    let wback = Some (tst b2 8) in
    if ex b1 3 0 = 0b1111 then Error `Invalid
    else
      match ex b2 11 6 with
      | 0b000000 ->
          let%map ops = p2oprs (b1, b2) chk_both_h (reg_aw, mem_ao) in
          (M.STR, Some false, qf_w (), None, ops)
      | 0b110100 when crn && cpush ->
          let%map ops = p1opr (b1, b2) chk_cq reg_aw in
          (M.PUSH, None, qf_w (), None, ops)
      | o2 when o2 land 0b100100 = 0b100100 ->
          let%map ops = p2oprs (b1, b2) chk_both_d (reg_aw, mem_am) in
          (M.STR, wback, None, None, ops)
      | o2 when o2 land 0b111100 = 0b110000 ->
          let%map ops = p2oprs (b1, b2) chk_both_d (reg_aw, mem_am) in
          (M.STR, wback, None, None, ops)
      | o2 when o2 land 0b111100 = 0b111000 ->
          let%map ops = p2oprs (b1, b2) chk_both_a (reg_aw, mem_ag) in
          (M.STRT, None, None, None, ops)
      | _ -> Error `Invalid

  let parse_group13 b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let wback () = Some (tst b2 8) in
    match concat (ex b1 7 5) (ex b2 11 6) 6 with
    | op when op land 0b111100100 = 0b000100100 ->
        let%map ops = p2oprs (b1, b2) chk_both_c (reg_aw, mem_am) in
        (M.STRB, wback (), None, None, ops)
    | op when op land 0b111111100 = 0b000110000 ->
        let%map ops = p2oprs (b1, b2) chk_both_c (reg_aw, mem_am) in
        (M.STRB, wback (), None, None, ops)
    | op when op land 0b111000000 = 0b100000000 ->
        let%map ops = p2oprs (b1, b2) chk_both_e (reg_aw, mem_an) in
        (M.STRB, Some false, qf_w (), None, ops)
    | 0b000000000 ->
        let%map ops = p2oprs (b1, b2) chk_both_g (reg_aw, mem_ao) in
        (M.STRB, Some false, qf_w (), None, ops)
    | op when op land 0b111111100 = 0b000111000 ->
        let%map ops = p2oprs (b1, b2) chk_both_a (reg_aw, mem_ag) in
        (M.STRBT, None, None, None, ops)
    | op when op land 0b111100100 = 0b001100100 ->
        let%map ops = p2oprs (b1, b2) chk_both_c (reg_aw, mem_am) in
        (M.STRH, wback (), None, None, ops)
    | op when op land 0b111111100 = 0b001110000 ->
        let%map ops = p2oprs (b1, b2) chk_both_c (reg_aw, mem_am) in
        (M.STRH, wback (), None, None, ops)
    | op when op land 0b111000000 = 0b101000000 ->
        let%map ops = p2oprs (b1, b2) chk_both_e (reg_aw, mem_an) in
        (M.STRH, Some false, qf_w (), None, ops)
    | 0b001000000 ->
        let%map ops = p2oprs (b1, b2) chk_both_g (reg_aw, mem_ao) in
        (M.STRH, Some false, qf_w (), None, ops)
    | op when op land 0b111111100 = 0b001111000 ->
        let%map ops = p2oprs (b1, b2) chk_both_a (reg_aw, mem_ag) in
        (M.STRHT, None, None, None, ops)
    | op when op land 0b111000000 = 0b110000000 ->
        let%map ops = p2oprs (b1, b2) chk_both_f (reg_aw, mem_an) in
        (M.STR, Some false, qf_w (), None, ops)
    | op when op land 0b111000000 = 0b010000000 -> parse_group13_sub b1 b2
    | _ -> Error `Invalid

  let parse_group14 b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let chkrn = ex b1 3 0 <> 0b1111 in
    let chkrt = ex b2 15 12 <> 0b1111 in
    let wback = Some (tst b2 8) in
    let%map mnemonic, wback, q, ops =
      match concat (ex b1 8 7) (ex b2 11 6) 6 with
      | 0b00000000 when chkrn && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_cw (reg_aw, mem_ao) in
          (M.LDRB, Some false, qf_w (), ops)
      | 0b00000000 when chkrn ->
          let%map ops = p1opr (b1, b2) chk_ak mem_p in
          (M.PLD, None, None, ops)
      | op when op land 0b11100100 = 0b00100100 && chkrn ->
          let%map ops = p2oprs (b1, b2) chk_ct (reg_aw, mem_am) in
          (M.LDRB, wback, None, ops)
      | op when op land 0b11111100 = 0b00110000 && chkrn && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_ct (reg_aw, mem_am) in
          (M.LDRB, wback, None, ops)
      | op when op land 0b11111100 = 0b00110000 && chkrn ->
          let%map ops = p1opr (b1, b2) dummy_chk mem_ap in
          (M.PLD, None, None, ops)
      | op when op land 0b11111100 = 0b00111000 && chkrn ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_aw, mem_ag) in
          (M.LDRBT, None, None, ops)
      | op when op land 0b11000000 = 0b01000000 && chkrn && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_cv (reg_aw, mem_an) in
          (M.LDRB, Some false, qf_w (), ops)
      | op when op land 0b11000000 = 0b01000000 && chkrn ->
          let%map ops = p1opr (b1, b2) dummy_chk mem_an in
          (M.PLD, None, None, ops)
      | op when op land 0b10000000 = 0b00000000 && chkrt ->
          let%map ops = p2oprs (b1, b2) dummy_chk (reg_aw, mem_aq) in
          (M.LDRB, None, qf_w (), ops)
      | op when op land 0b10000000 = 0b00000000 ->
          let%map ops = p1opr (b1, b2) dummy_chk mem_aq in
          (M.PLD, None, None, ops)
      | 0b10000000 when chkrn && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_cw (reg_aw, mem_ao) in
          (M.LDRSB, Some false, qf_w (), ops)
      | 0b10000000 when chkrn ->
          let%map ops = p1opr (b1, b2) chk_ak mem_p in
          (M.PLI, None, None, ops)
      | op when op land 0b11100100 = 0b10100100 && chkrn ->
          let%map ops = p2oprs (b1, b2) chk_ct (reg_aw, mem_am) in
          (M.LDRSB, wback, None, ops)
      | op when op land 0b11111100 = 0b10110000 && chkrn && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_ct (reg_aw, mem_am) in
          (M.LDRSB, wback, None, ops)
      | op when op land 0b11111100 = 0b10110000 && chkrn ->
          let%map ops = p1opr (b1, b2) dummy_chk mem_ap in
          (M.PLI, None, None, ops)
      | op when op land 0b11111100 = 0b10111000 && chkrn ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_aw, mem_ag) in
          (M.LDRSBT, None, None, ops)
      | op when op land 0b11000000 = 0b11000000 && chkrn && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_cv (reg_aw, mem_an) in
          (M.LDRSB, Some false, None, ops)
      | op when op land 0b11000000 = 0b11000000 && chkrn ->
          let%map ops = p1opr (b1, b2) dummy_chk mem_an in
          (M.PLI, None, None, ops)
      | op when op land 0b10000000 = 0b10000000 && chkrt ->
          let%map ops = p2oprs (b1, b2) dummy_chk (reg_aw, mem_aq) in
          (M.LDRSB, None, None, ops)
      | op when op land 0b10000000 = 0b10000000 ->
          let%map ops = p1opr (b1, b2) dummy_chk mem_aq in
          (M.PLI, None, None, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, wback, q, None, ops)

  let parse_group15_with_rn b1 b2 =
    let open Result.Let_syntax in
    let chkrt = ex b2 15 12 <> 0b1111 in
    match ex b1 8 7 with
    | op when op land 0b10 = 0b00 && chkrt ->
        let%map ops = p2oprs (b1, b2) chk_cv (reg_aw, mem_aq) in
        (M.LDRH, None, None, None, ops)
    | op when op land 0b10 = 0b00 ->
        let%map ops = p1opr (b1, b2) dummy_chk mem_aq in
        (M.PLD, None, None, None, ops)
    | op when op land 0b10 = 0b10 && chkrt ->
        let%map ops = p2oprs (b1, b2) chk_cv (reg_aw, mem_aq) in
        (M.LDRSH, None, None, None, ops)
    | op when op land 0b10 = 0b10 -> Ok (M.NOP, None, None, None, e)
    | _ -> Error `Invalid

  let parse_group15 b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let chkrt = ex b2 15 12 <> 0b1111 in
    let wback = Some (tst b2 8) in
    if ex b1 3 0 = 0b1111 then parse_group15_with_rn b1 b2
    else
      match concat (ex b1 8 7) (ex b2 11 6) 6 with
      | op when op land 0b11100100 = 0b00100100 ->
          let%map ops = p2oprs (b1, b2) chk_ct (reg_aw, mem_am) in
          (M.LDRH, wback, None, None, ops)
      | op when op land 0b11111100 = 0b00110000 && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_ct (reg_aw, mem_am) in
          (M.LDRH, wback, None, None, ops)
      | op when op land 0b11000000 = 0b01000000 && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_cv (reg_aw, mem_an) in
          (M.LDRH, Some false, qf_w (), None, ops)
      | 0b00000000 when chkrt ->
          let%map ops = p2oprs (b1, b2) chk_cw (reg_aw, mem_ao) in
          (M.LDRH, Some false, qf_w (), None, ops)
      | op when op land 0b11111100 = 0b00111000 ->
          let%map ops = p2oprs (b1, b2) chk_bl (reg_aw, mem_ag) in
          (M.LDRHT, None, None, None, ops)
      | 0b00000000 ->
          let%map ops = p1opr (b1, b2) chk_ak mem_p in
          (M.PLDW, None, None, None, ops)
      | op when op land 0b11111100 = 0b00110000 ->
          let%map ops = p1opr (b1, b2) dummy_chk mem_ap in
          (M.PLDW, None, None, None, ops)
      | op when op land 0b11000000 = 0b01000000 ->
          let%map ops = p1opr (b1, b2) dummy_chk mem_an in
          (M.PLDW, None, None, None, ops)
      | op when op land 0b11100100 = 0b10100100 ->
          let%map ops = p2oprs (b1, b2) chk_ct (reg_aw, mem_am) in
          (M.LDRSH, wback, None, None, ops)
      | op when op land 0b11110000 = 0b10110000 && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_ct (reg_aw, mem_am) in
          (M.LDRSH, wback, None, None, ops)
      | op when op land 0b11000000 = 0b11000000 && chkrt ->
          let%map ops = p2oprs (b1, b2) chk_cv (reg_aw, mem_an) in
          (M.LDRSH, Some false, None, None, ops)
      | 0b10000000 when chkrt ->
          let%map ops = p2oprs (b1, b2) chk_cw (reg_aw, mem_ao) in
          (M.LDRSH, Some false, qf_w (), None, ops)
      | op when op land 0b11111100 = 0b10111000 ->
          let%map ops = p2oprs (b2, b2) chk_bl (reg_aw, mem_ag) in
          (M.LDRSHT, None, None, None, ops)
      | 0b10000000 -> Ok (M.NOP, None, None, None, e)
      | op when op land 0b11111100 = 0b10110000 ->
          Ok (M.NOP, None, None, None, e)
      | op when op land 0b11000000 = 0b11000000 ->
          Ok (M.NOP, None, None, None, e)
      | _ -> Error `Invalid

  let parse_group16 (ctx : Context.t) b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let chkrn = ex b1 3 0 = 0b1111 in
    let chkrn2 = ex b1 3 0 = 0b1101 in
    let chkpop = ex b2 5 0 = 0b000100 in
    let wback = Some (tst b2 8) in
    match concat (ex b1 8 7) (ex b2 11 6) 6 with
    | op when op land 0b10000000 = 0b0 && chkrn ->
        let%map ops = p2oprs (b1, b2) (chk_dk ctx.it) (reg_aw, mem_aq) in
        (M.LDR, Some false, qf_w (), None, ops)
    | 0b00000000 ->
        let%map ops = p2oprs (b1, b2) (chk_cx ctx.it) (reg_aw, mem_ao) in
        (M.LDR, None, qf_w (), None, ops)
    | 0b00101100 when chkrn2 && chkpop ->
        let%map ops = p1opr (b1, b2) (chk_dj ctx.it) reg_aw in
        (M.POP, None, qf_w (), None, ops)
    | op when op land 0b11100100 = 0b00100100 ->
        let%map ops = p2oprs (b1, b2) (chk_cu ctx.it) (reg_aw, mem_am) in
        (M.LDR, wback, None, None, ops)
    | op when op land 0b11111100 = 0b00110000 ->
        let%map ops = p2oprs (b1, b2) (chk_cu ctx.it) (reg_aw, mem_am) in
        (M.LDR, wback, None, None, ops)
    | op when op land 0b11000000 = 0b01000000 ->
        let%map ops = p2oprs (b1, b2) (chk_dk ctx.it) (reg_aw, mem_an) in
        (M.LDR, Some false, qf_w (), None, ops)
    | op when op land 0b11111100 = 0b00111000 ->
        let%map ops = p2oprs (b1, b2) chk_bl (reg_aw, mem_ag) in
        (M.LDRT, None, None, None, ops)
    | _ -> Error `Invalid

  let parse_group17 b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let%map mnemonic, wback, dt, ops =
      adv_simd_or_struct (concat b1 b2 16)
    in
    (mnemonic, wback, None, dt, ops)

  let parse_parallel_add_sub_signed b1 b2 =
    match concat (ex b1 6 4) (ex b2 5 4) 2 with
    | 0b00100 -> Ok M.SADD16
    | 0b01000 -> Ok M.SASX
    | 0b11000 -> Ok M.SSAX
    | 0b10100 -> Ok M.SSUB16
    | 0b00000 -> Ok M.SADD8
    | 0b10000 -> Ok M.SSUB8
    | 0b00101 -> Ok M.QADD16
    | 0b01001 -> Ok M.QASX
    | 0b11001 -> Ok M.QSAX
    | 0b10101 -> Ok M.QSUB16
    | 0b00001 -> Ok M.QADD8
    | 0b10001 -> Ok M.QSUB8
    | 0b00110 -> Ok M.SHADD16
    | 0b01010 -> Ok M.SHASX
    | 0b11010 -> Ok M.SHSAX
    | 0b10110 -> Ok M.SHSUB16
    | 0b00010 -> Ok M.SHADD8
    | 0b10010 -> Ok M.SHSUB8
    | _ -> Error `Invalid

  let parse_parallel_add_sub_unsigned b1 b2 =
    match concat (ex b1 6 4) (ex b2 5 4) 2 with
    | 0b00100 -> Ok M.UADD16
    | 0b01000 -> Ok M.UASX
    | 0b11000 -> Ok M.USAX
    | 0b10100 -> Ok M.USUB16
    | 0b00000 -> Ok M.UADD8
    | 0b10000 -> Ok M.USUB8
    | 0b00101 -> Ok M.UQADD16
    | 0b01001 -> Ok M.UQASX
    | 0b11001 -> Ok M.UQSAX
    | 0b10101 -> Ok M.UQSUB16
    | 0b00001 -> Ok M.UQADD8
    | 0b10001 -> Ok M.UQSUB8
    | 0b00110 -> Ok M.UHADD16
    | 0b01010 -> Ok M.UHASX
    | 0b11010 -> Ok M.UHSAX
    | 0b10110 -> Ok M.UHSUB16
    | 0b00010 -> Ok M.UHADD8
    | 0b10010 -> Ok M.UHSUB8
    | _ -> Error `Invalid

  let parse_parallel_add_sub b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic =
      match tst b2 6 with
      | false -> parse_parallel_add_sub_signed b1 b2
      | true -> parse_parallel_add_sub_unsigned b1 b2
    in
    let%map ops = p3oprs (b1, b2) chk_cy (reg_av, reg_ay, reg_ax) in
    (mnemonic, None, None, None, ops)

  let parse_misc b1 b2 =
    let open Result.Let_syntax in
    match concat (ex b1 5 4) (ex b2 5 4) 2 with
    | 0b0000 ->
        let%map ops = p3oprs (b1, b2) chk_cy (reg_av, reg_ax, reg_ay) in
        (M.QADD, None, None, None, ops)
    | 0b0001 ->
        let%map ops = p3oprs (b1, b2) chk_cy (reg_av, reg_ax, reg_ay) in
        (M.QDADD, None, None, None, ops)
    | 0b0010 ->
        let%map ops = p3oprs (b1, b2) chk_cy (reg_av, reg_ax, reg_ay) in
        (M.QSUB, None, None, None, ops)
    | 0b0011 ->
        let%map ops = p3oprs (b1, b2) chk_cy (reg_av, reg_ax, reg_ay) in
        (M.QDSUB, None, None, None, ops)
    | 0b0100 ->
        let%map ops = p2oprs (b1, b2) chk_cz (reg_av, reg_ax) in
        (M.REV, None, qf_w (), None, ops)
    | 0b0101 ->
        let%map ops = p2oprs (b1, b2) chk_cz (reg_av, reg_ax) in
        (M.REV16, None, qf_w (), None, ops)
    | 0b0110 ->
        let%map ops = p2oprs (b1, b2) chk_cz (reg_av, reg_ax) in
        (M.RBIT, None, None, None, ops)
    | 0b0111 ->
        let%map ops = p2oprs (b1, b2) chk_cz (reg_av, reg_ax) in
        (M.REVSH, None, qf_w (), None, ops)
    | 0b1000 ->
        let%map ops = p3oprs (b1, b2) chk_cy (reg_av, reg_ax, reg_ay) in
        (M.SEL, None, None, None, ops)
    | 0b1100 ->
        let%map ops = p2oprs (b1, b2) chk_cz (reg_av, reg_ax) in
        (M.CLZ, None, None, None, ops)
    | _ -> Error `Invalid

  let parse_group18_sub b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic =
      match ex b1 6 4 with
      | 0b000 -> Ok M.LSL
      | 0b001 -> Ok M.LSLS
      | 0b010 -> Ok M.LSR
      | 0b011 -> Ok M.LSRS
      | 0b100 -> Ok M.ASR
      | 0b101 -> Ok M.ASRS
      | 0b110 -> Ok M.ROR
      | 0b111 -> Ok M.RORS
      | _ -> Error `Invalid
    in
    let%map ops = p3oprs (b1, b2) chk_cy (reg_av, reg_ay, reg_ax) in
    (mnemonic, None, qf_w (), None, ops)

  let parse_group18_with_rn b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic, q =
      match ex b1 6 4 with
      | 0b000 -> Ok (M.SXTH, qf_w ())
      | 0b001 -> Ok (M.UXTH, qf_w ())
      | 0b010 -> Ok (M.SXTB16, None)
      | 0b011 -> Ok (M.UXTB16, None)
      | 0b100 -> Ok (M.SXTB, qf_w ())
      | 0b101 -> Ok (M.UXTB, qf_w ())
      | _ -> Error `Invalid
    in
    let%map ops = p3oprs (b1, b2) chk_bv (reg_av, reg_ax, shift_j) in
    (mnemonic, None, q, None, ops)

  let parse_group18_without_rn b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic =
      match ex b1 6 4 with
      | 0b000 -> Ok M.SXTAH
      | 0b001 -> Ok M.UXTAH
      | 0b010 -> Ok M.SXTAB16
      | 0b011 -> Ok M.UXTAB16
      | 0b100 -> Ok M.SXTAB
      | 0b101 -> Ok M.UXTAB
      | _ -> Error `Invalid
    in
    let%map ops = p4oprs (b1, b2) chk_bz (reg_av, reg_ay, reg_ax, shift_j) in
    (mnemonic, None, None, None, ops)

  let parse_group18_by_rn b1 b2 =
    match ex b1 3 0 with
    | 0b1111 -> parse_group18_with_rn b1 b2
    | _ -> parse_group18_without_rn b1 b2

  let parse_group18 b =
    let b1, b2 = halve b in
    let b17 = tst b1 7 |> Bool.to_int in
    match concat b17 (ex b2 7 4) 4 with
    | 0b00000 -> parse_group18_sub b1 b2
    | op when op land 0b11000 = 0b01000 -> parse_group18_by_rn b1 b2
    | op when op land 0b11000 = 0b10000 -> parse_parallel_add_sub b1 b2
    | op when op land 0b11100 = 0b11000 -> parse_misc b1 b2
    | _ -> Error `Invalid

  let parse_group19_sub b1 b2 =
    let open Result.Let_syntax in
    let b24 = tst b2 4 |> Bool.to_int in
    let%bind mnemonic =
      match concat (ex b1 6 4) b24 1 with
      | 0b0001 -> Ok M.MLS
      | 0b1100 -> Ok M.SMMLS
      | 0b1101 -> Ok M.SMMLSR
      | _ -> Error `Invalid
    in
    let%map ops = p4oprs (b1, b2) chk_db (reg_av, reg_ay, reg_ax, reg_aw) in
    (mnemonic, None, None, None, ops)

  let parse_group19_without_ra b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic =
      match concat (ex b1 6 4) (ex b2 5 4) 2 with
      | 0b00000 -> Ok M.MLA
      | 0b00100 -> Ok M.SMLABB
      | 0b00101 -> Ok M.SMLABT
      | 0b00110 -> Ok M.SMLATB
      | 0b00111 -> Ok M.SMLATT
      | 0b01000 -> Ok M.SMLAD
      | 0b01001 -> Ok M.SMLADX
      | 0b01100 -> Ok M.SMLAWB
      | 0b01101 -> Ok M.SMLAWT
      | 0b10000 -> Ok M.SMLSD
      | 0b10001 -> Ok M.SMLSDX
      | 0b10100 -> Ok M.SMMLA
      | 0b10101 -> Ok M.SMMLAR
      | 0b11100 -> Ok M.USADA8
      | _ -> Error `Invalid
    in
    let%map ops = p4oprs (b1, b2) chk_da (reg_av, reg_ay, reg_ax, reg_aw) in
    (mnemonic, None, None, None, ops)

  let parse_group19_with_ra b1 b2 =
    let open Result.Let_syntax in
    let%bind mnemonic =
      match concat (ex b1 6 4) (ex b2 5 4) 2 with
      | 0b00000 -> Ok M.MUL
      | 0b00100 -> Ok M.SMULBB
      | 0b00101 -> Ok M.SMULBT
      | 0b00110 -> Ok M.SMULTB
      | 0b00111 -> Ok M.SMULTT
      | 0b01000 -> Ok M.SMUAD
      | 0b01001 -> Ok M.SMUADX
      | 0b01100 -> Ok M.SMULWB
      | 0b01101 -> Ok M.SMULWT
      | 0b10000 -> Ok M.SMUSD
      | 0b10001 -> Ok M.SMUSDX
      | 0b10100 -> Ok M.SMMUL
      | 0b10101 -> Ok M.SMMULR
      | 0b11100 -> Ok M.USAD8
      | _ -> Error `Invalid
    in
    let%map ops = p3oprs (b1, b2) chk_cy (reg_av, reg_ay, reg_ax) in
    (mnemonic, None, None, None, ops)

  let parse_group19 b =
    let b1, b2 = halve b in
    let op = concat (ex b1 6 4) (ex b2 5 4) 2 in
    match op with
    | 1 | 0b11000 | 0b11001 -> parse_group19_sub b1 b2
    | _ when ex b2 15 12 = 0b1111 -> parse_group19_with_ra b1 b2
    | _ -> parse_group19_without_ra b1 b2

  let parse_group20 b =
    let open Result.Let_syntax in
    let b1, b2 = halve b in
    let op4 () = p4oprs (b1, b2) chk_dc (reg_aw, reg_av, reg_ay, reg_ax) in
    let op3 () = p3oprs (b1, b2) chk_cy (reg_av, reg_ay, reg_ax) in
    match concat (ex b1 6 4) (ex b2 7 4) 4 with
    | 0b0000000 ->
        let%map ops = op4 () in
        (M.SMULL, None, None, None, ops)
    | 0b0011111 ->
        let%map ops = op3 () in
        (M.SDIV, None, None, None, ops)
    | 0b0100000 ->
        let%map ops = op4 () in
        (M.UMULL, None, None, None, ops)
    | 0b0111111 ->
        let%map ops = op3 () in
        (M.UDIV, None, None, None, ops)
    | 0b1000000 ->
        let%map ops = op4 () in
        (M.SMLAL, None, None, None, ops)
    | 0b1001000 ->
        let%map ops = op4 () in
        (M.SMLALBB, None, None, None, ops)
    | 0b1001001 ->
        let%map ops = op4 () in
        (M.SMLALBT, None, None, None, ops)
    | 0b1001010 ->
        let%map ops = op4 () in
        (M.SMLALTB, None, None, None, ops)
    | 0b1001011 ->
        let%map ops = op4 () in
        (M.SMLALTT, None, None, None, ops)
    | 0b1001100 ->
        let%map ops = op4 () in
        (M.SMLALD, None, None, None, ops)
    | 0b1001101 ->
        let%map ops = op4 () in
        (M.SMLALDX, None, None, None, ops)
    | 0b1011100 ->
        let%map ops = op4 () in
        (M.SMLSLD, None, None, None, ops)
    | 0b1011101 ->
        let%map ops = op4 () in
        (M.SMLSLDX, None, None, None, ops)
    | 0b1100000 ->
        let%map ops = op4 () in
        (M.UMLAL, None, None, None, ops)
    | 0b1100110 ->
        let%map ops = op4 () in
        (M.UMAAL, None, None, None, ops)
    | _ -> Error `Invalid

  let parse32_group01 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let%map mnemonic, wback, q, dt, ops =
      match ex b 10 9 with
      | 0b00 when not (tst b 6) -> parse_group6 ctx b
      | 0b00 -> parse_group7 ctx b
      | 0b01 -> parse_group8 b
      | 0b10 | 0b11 -> parse_group9 b
      | _ -> Error `Invalid
    in
    (mnemonic, cond, 0, wback, q, dt, ops, None)

  let parse32_group10 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let%map mnemonic, cond, q, ops, cflag =
      match (tst b 9, tst b 31) with
      | false, false -> parse_group10 cond b
      | true, false ->
          let%map m, c, q, ops = parse_group11 cond b in
          (m, c, q, ops, None)
      | _, true ->
          let%map m, c, q, ops = parse_group12 ctx cond b in
          (m, c, q, ops, None)
    in
    (mnemonic, cond, 0, None, q, None, ops, cflag)

  let parse32_group11 (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let%map mnemonic, wback, q, dt, ops =
      match ex b 10 4 with
      | op when op land 0b1110001 = 0b0000000 -> parse_group13 b
      | op when op land 0b1100111 = 0b0000001 -> parse_group14 b
      | op when op land 0b1100111 = 0b0000011 -> parse_group15 b
      | op when op land 0b1100111 = 0b0000101 -> parse_group16 ctx b
      | op when op land 0b1100111 = 0b0000111 -> Error `Invalid
      | op when op land 0b1110001 = 0b0010000 -> parse_group17 b
      | op when op land 0b1110000 = 0b0100000 -> parse_group18 b
      | op when op land 0b1111000 = 0b0110000 -> parse_group19 b
      | op when op land 0b1111000 = 0b0111000 -> parse_group20 b
      | op when op land 0b1000000 = 0b1000000 -> parse_group9 b
      | _ -> Error `Invalid
    in
    (mnemonic, cond, 0, wback, q, dt, ops, None)

  let parse32 (ctx : Context.t) b =
    let open Result.Let_syntax in
    let%bind cond = Context.cond_with_it ctx in
    let%map result =
      match ex b 12 11 with
      | 0b01 -> parse32_group01 ctx cond b
      | 0b10 -> parse32_group10 ctx cond b
      | 0b11 -> parse32_group11 ctx cond b
      | _ -> Error `Invalid
    in
    Context.update_it ctx; result

  let parse16 (ctx : Context.t) b =
    let open Result.Let_syntax in
    let%bind cond = Context.cond_with_it ctx in
    let%map mnemonic, cond, it, wback, q, ops =
      match ex b 15 11 with
      | op when op land 0b11000 = 0b00000 -> parse_group0 ctx cond b
      | 0b01000 when not (tst b 10) -> parse_group1 ctx cond b
      | 0b01000 -> parse_group2 ctx cond b
      | 0b01001 ->
          let%map ops = p2oprs b dummy_chk (reg_j, lbl_8a) in
          (M.LDR, cond, 0, None, None, ops)
      | op when op land 0b11110 = 0b01010 -> parse_group3 cond b
      | op when op land 0b11100 = 0b01100 -> parse_group3 cond b
      | op when op land 0b11100 = 0b10000 -> parse_group3 cond b
      | 0b10100 ->
          let%map ops = p2oprs b dummy_chk (reg_j, lbl_8a) in
          (M.ADR, cond, 0, None, None, ops)
      | 0b10101 ->
          let%map ops = p3oprs b dummy_chk (reg_j, reg_sp, imm_8b) in
          (M.ADD, cond, 0, None, None, ops)
      | op when op land 0b11110 = 0b10110 -> parse_group4 ctx cond b
      | 0b11000 ->
          let%map ops = p2oprs b chk_dd (reg_wc, reglist_r) in
          (M.STM, cond, 0, Some true, None, ops)
      | 0b11001 ->
          let regs = concat 0b00000000 (ex b 7 0) 8 in
          let n = ex b 10 8 in
          let wback = Some (not (tst regs n)) in
          let%map ops = p2oprs b chk_dd (reg_wd, reglist_r) in
          (M.LDM, cond, 0, wback, None, ops)
      | op when op land 0b11110 = 0b11010 -> parse_group5 ctx cond b
      | 0b11100 ->
          let%map ops = p1opr b dummy_chk lbl_12a in
          (M.B, cond, 0, None, qf_n (), ops)
      | _ -> Error `Invalid
    in
    Context.update_it ctx;
    (mnemonic, cond, it, wback, q, None, ops, None)
end

module V8 = struct
  let reg_dca_oprs_with_unpre_a b = p3oprs b chk_a (reg_d, reg_c, reg_a)

  let parse_parallel_arithmetic b =
    let open Result.Let_syntax in
    let op = concat (ex b 22 20) (ex b 6 5) 2 in
    let b7 = tst b 7 |> Bool.to_int in
    match concat op b7 1 with
    | x when x land 0b111000 = 0b000000 -> Error `Invalid
    | 0b001000 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SADD16, ops)
    | 0b001001 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SADD8, ops)
    | 0b001010 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SASX, ops)
    | 0b001011 -> Error `Invalid
    | 0b001100 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SSAX, ops)
    | 0b001101 -> Error `Invalid
    | 0b001110 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SSUB16, ops)
    | 0b001111 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SSUB8, ops)
    | 0b010000 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QADD16, ops)
    | 0b010001 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QADD8, ops)
    | 0b010010 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QASX, ops)
    | 0b010011 -> Error `Invalid
    | 0b010100 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QSAX, ops)
    | 0b010101 -> Error `Invalid
    | 0b010110 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QSUB16, ops)
    | 0b010111 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QSUB8, ops)
    | 0b011000 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SHADD16, ops)
    | 0b011001 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SHADD8, ops)
    | 0b011010 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SHASX, ops)
    | 0b011011 -> Error `Invalid
    | 0b011100 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SHSAX, ops)
    | 0b011101 -> Error `Invalid
    | 0b011110 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SHSUB16, ops)
    | 0b011111 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.SHSUB8, ops)
    | x when x land 0b111000 = 0b100000 -> Error `Invalid
    | 0b101000 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UADD16, ops)
    | 0b101001 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UADD8, ops)
    | 0b101010 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UASX, ops)
    | 0b101011 -> Error `Invalid
    | 0b101100 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.USAX, ops)
    | 0b101101 -> Error `Invalid
    | 0b101110 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.USUB16, ops)
    | 0b101111 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.USUB8, ops)
    | 0b110000 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UQADD16, ops)
    | 0b110001 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UQADD8, ops)
    | 0b110010 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UQASX, ops)
    | 0b110011 -> Error `Invalid
    | 0b110100 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UQSAX, ops)
    | 0b110101 -> Error `Invalid
    | 0b110110 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UQSUB16, ops)
    | 0b110111 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UQSUB8, ops)
    | 0b111000 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UHADD16, ops)
    | 0b111001 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UHADD8, ops)
    | 0b111010 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UHASX, ops)
    | 0b111011 -> Error `Invalid
    | 0b111100 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UHSAX, ops)
    | 0b111101 -> Error `Invalid
    | 0b111110 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UHSUB16, ops)
    | 0b111111 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.UHSUB8, ops)
    | _ -> Error `Invalid

  let parse_saturate_16bit b =
    let open Result.Let_syntax in
    let%map ops = p3oprs b chk_m (reg_a, imm_4b, reg_a) in
    let mnemonic = if tst b 22 then M.USAT16 else M.SSAT16 in
    (mnemonic, ops)

  let parse_reverse_bit_byte b =
    let open Result.Let_syntax in
    let b22 = tst b 22 |> Bool.to_int in
    let b7 = tst b 7 |> Bool.to_int in
    match concat b22 b7 1 with
    | 0b00 ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.REV, ops)
    | 0b01 ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.REV16, ops)
    | 0b10 ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.RBIT, ops)
    | 0b11 ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.REVSH, ops)
    | _ -> Error `Invalid

  let parse_saturate_32bit b =
    let open Result.Let_syntax in
    let%map ops = p4oprs b chk_o (reg_d, imm_5c, reg_a, shift_d) in
    let mnemonic = if tst b 22 then M.USAT else M.SSAT in
    (mnemonic, ops)

  let parse_extend_and_add b =
    let open Result.Let_syntax in
    let b22 = tst b 22 |> Bool.to_int in
    let opu = concat (ex b 21 20) b22 1 in
    let op3 () = p3oprs b chk_p (reg_d, reg_a, shift_c) in
    let op4 () = p4oprs b chk_o (reg_d, reg_c, reg_a, shift_c) in
    match (opu, ex b 19 16) with
    | 0b000, 0b1111 ->
        let%map ops = op3 () in
        (M.SXTB16, ops)
    | 0b000, _ ->
        let%map ops = op4 () in
        (M.SXTAB16, ops)
    | 0b001, 0b1111 ->
        let%map ops = op3 () in
        (M.UXTB16, ops)
    | 0b001, _ ->
        let%map ops = op4 () in
        (M.UXTAB16, ops)
    | 0b100, 0b1111 ->
        let%map ops = op3 () in
        (M.SXTB, ops)
    | 0b100, _ ->
        let%map ops = op4 () in
        (M.SXTAB, ops)
    | 0b101, 0b1111 ->
        let%map ops = op3 () in
        (M.UXTB, ops)
    | 0b101, _ ->
        let%map ops = op4 () in
        (M.UXTAB, ops)
    | 0b110, 0b1111 ->
        let%map ops = op3 () in
        (M.SXTH, ops)
    | 0b110, _ ->
        let%map ops = op4 () in
        (M.SXTAH, ops)
    | 0b111, 0b1111 ->
        let%map ops = op3 () in
        (M.UXTH, ops)
    | 0b111, _ ->
        let%map ops = op4 () in
        (M.UXTAH, ops)
    | _ -> Error `Invalid

  let parse_signed_multiply_divide b =
    let open Result.Let_syntax in
    let op2 = ex b 7 5 in
    let op = concat (ex b 22 20) op2 3 in
    let ra = ex b 15 12 in
    let chkop2 () = op2 <> 0b000 in
    let chkra () = ra <> 0b1111 in
    match op with
    | 0b000000 when chkra () ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMLAD, ops)
    | 0b000000 ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMUAD, ops)
    | 0b000001 when chkra () ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMLADX, ops)
    | 0b000001 ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMUADX, ops)
    | 0b000010 when chkra () ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMLSD, ops)
    | 0b000010 ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMUSD, ops)
    | 0b000011 when chkra () ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMLSDX, ops)
    | 0b000011 ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMUSDX, ops)
    | op when op land 0b111100 = 0b000100 -> Error `Invalid
    | op when op land 0b111000 = 0b001000 && chkop2 () -> Error `Invalid
    | 0b001000 ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SDIV, ops)
    | op when op land 0b111000 = 0b010000 -> Error `Invalid
    | op when op land 0b111000 = 0b011000 && chkop2 () -> Error `Invalid
    | 0b011000 ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.UDIV, ops)
    | 0b100000 ->
        let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
        (M.SMLALD, ops)
    | 0b100001 ->
        let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
        (M.SMLALDX, ops)
    | 0b100010 ->
        let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
        (M.SMLSLD, ops)
    | 0b100011 ->
        let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
        (M.SMLSLDX, ops)
    | op when op land 0b111100 = 0b100100 -> Error `Invalid
    | 0b101000 when chkra () ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMMLA, ops)
    | 0b101000 ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMMUL, ops)
    | 0b101001 when chkra () ->
        let%map ops = p4oprs b chk_c (reg_c, reg_a, reg_b, reg_d) in
        (M.SMMLAR, ops)
    | 0b101001 ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.SMMULR, ops)
    | op when op land 0b111110 = 0b101010 -> Error `Invalid
    | op when op land 0b111110 = 0b101100 -> Error `Invalid
    | 0b101110 ->
        let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
        (M.SMMLS, ops)
    | 0b101111 ->
        let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
        (M.SMMLSR, ops)
    | op when op land 0b110000 = 0b110000 -> Error `Invalid
    | _ -> Error `Invalid

  let unsigned_sum_absolute_diff b =
    let open Result.Let_syntax in
    match ex b 15 12 with
    | 0b1111 ->
        let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
        (M.USADA8, ops)
    | _ ->
        let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
        (M.USAD8, ops)

  let parse_bitfield_insert b =
    let open Result.Let_syntax in
    match ex b 3 0 with
    | 0b1111 ->
        let%map ops = p3oprs b chk_ap (reg_d, imm_5a, imm_5f) in
        (M.BFC, ops)
    | _ ->
        let%map ops = p4oprs b chk_aq (reg_d, reg_a, imm_5a, imm_5f) in
        (M.BFI, ops)

  let parse_permanently_undefined cond b =
    let open Result.Let_syntax in
    if C.(equal cond AL) then
      let%map ops = p1opr b dummy_chk imm_12d in
      (M.UDF, ops)
    else Error `Invalid

  let parse_bitfield_extract b =
    let open Result.Let_syntax in
    let%map ops = p4oprs b chk_q (reg_d, reg_a, imm_5a, imm_5c) in
    let mnemonic = if tst b 22 then M.UBFX else M.SBFX in
    (mnemonic, ops)

  let adv_simd_float_64bit b =
    let open Result.Let_syntax in
    let op2 = concat (tst b 20 |> Bool.to_int) (ex b 7 6) 2 in
    let o3d = concat (tst b 4 |> Bool.to_int) (tst b 22 |> Bool.to_int) 1 in
    let op = concat op2 o3d 2 in
    let chksz = not (tst b 8) in
    let chkop = not (tst b 20) in
    match op with
    | op when op land 0b00001 = 0b00000 -> Error `Invalid
    | 0b00011 when chksz && chkop ->
        let%map ops = p4oprs b chk_aw (reg_ai, reg_aj, reg_d, reg_c) in
        (M.VMOV, ops)
    | 0b00011 when chksz ->
        let%map ops = p4oprs b chk_ax (reg_d, reg_c, reg_ai, reg_aj) in
        (M.VMOV, ops)
    | 0b00011 when chkop ->
        let%map ops = p3oprs b chk_p (reg_af, reg_d, reg_c) in
        (M.VMOV, ops)
    | 0b00011 ->
        let%map ops = p3oprs b chk_ay (reg_d, reg_c, reg_af) in
        (M.VMOV, ops)
    | op when op land 0b00011 = 0b00001 -> Error `Invalid
    | op when op land 0b01101 = 0b00101 -> Error `Invalid
    | op when op land 0b01001 = 0b01001 -> Error `Invalid
    | 0b10011 when chksz && chkop ->
        let%map ops = p4oprs b chk_aw (reg_ai, reg_aj, reg_d, reg_c) in
        (M.VMOV, ops)
    | 0b10011 when chksz ->
        let%map ops = p4oprs b chk_ax (reg_d, reg_c, reg_ai, reg_aj) in
        (M.VMOV, ops)
    | 0b10011 when chksz && chkop ->
        let%map ops = p3oprs b chk_p (reg_af, reg_d, reg_c) in
        (M.VMOV, ops)
    | 0b10011 when chksz ->
        let%map ops = p3oprs b chk_ay (reg_d, reg_c, reg_af) in
        (M.VMOV, ops)
    | _ -> Error `Invalid

  let adv_simd_float_load_store b =
    let open Result.Let_syntax in
    let pul = concat (ex b 24 23) (tst b 20 |> Bool.to_int) 1 in
    let op = concat pul (tst b 8 |> Bool.to_int) 1 in
    let chkw = tst b 21 in
    let chkimm8 = not (tst b 0) in
    let chkrn = ex b 19 16 <> 0b1111 in
    let chkpushpop = ex b 19 16 = 0b1101 in
    let chk8 = not (tst b 8) in
    match op with
    | op when op land 0b1100 = 0b0000 && chkw -> Error `Invalid
    | op when op land 0b1100 = 0b0100 && chkw && chkpushpop && chk8 ->
        let%map ops = p1opr b chk_ba reglist_m in
        (M.VPOP, ops)
    | op when op land 0b1100 = 0b0100 && chkw && chkpushpop ->
        let%map ops = p1opr b chk_az reglist_l in
        (M.VPOP, ops)
    | 0b0100 when chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VSTMIA, ops)
    | 0b0100 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VSTMIA, ops)
    | 0b0101 when chkimm8 && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VSTMIA, ops)
    | 0b0101 when chkimm8 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VSTMIA, ops)
    | 0b0101 when chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.FSTMIAX, ops)
    | 0b0101 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.FSTMIAX, ops)
    | 0b0110 when chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VLDMIA, ops)
    | 0b0110 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VLDMIA, ops)
    | 0b0111 when chkimm8 && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VLDMIA, ops)
    | 0b0111 when chkimm8 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VLDMIA, ops)
    | 0b0111 when chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.FSTMIAX, ops)
    | 0b0111 ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.FSTMIAX, ops)
    | op when op land 0b1010 = 0b1000 && chkw ->
        let%map ops = p2oprs b dummy_chk (reg_al, mem_ar) in
        (M.VSTR, ops)
    | op when op land 0b1100 = 0b1000 && chkw && chkpushpop && chk8 ->
        let%map ops = p1opr b chk_ba reglist_m in
        (M.VPUSH, ops)
    | op when op land 0b1100 = 0b1000 && chkw && chkpushpop ->
        let%map ops = p1opr b chk_az reglist_l in
        (M.VPUSH, ops)
    | 0b1000 when (not chkw) && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VSTMDB, ops)
    | 0b1000 when not chkw ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VSTMDB, ops)
    | 0b1001 when chkimm8 && (not chkw) && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VSTMDB, ops)
    | 0b1001 when chkimm8 && not chkw ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VSTMDB, ops)
    | 0b1001 when (not chkimm8) && (not chkw) && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.FSTMDBX, ops)
    | 0b1001 when (not chkimm8) && not chkw ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.FSTMDBX, ops)
    | 0b1010 when (not chkw) && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VLDMDB, ops)
    | 0b1010 when not chkw ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VLDMDB, ops)
    | 0b1011 when chkimm8 && (not chkw) && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VLDMDB, ops)
    | 0b1011 when chkimm8 && not chkw ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VLDMDB, ops)
    | 0b1011 when (not chkimm8) && (not chkw) && chk8 ->
        let%map ops = p2oprs b chk_ba (reg_wa, reglist_m) in
        (M.VLDMDB, ops)
    | 0b1011 when (not chkimm8) && not chkw ->
        let%map ops = p2oprs b chk_az (reg_wa, reglist_l) in
        (M.VLDMDB, ops)
    | op when op land 0b1010 = 0b1010 && chkw && chkrn ->
        let%map ops = p2oprs b dummy_chk (reg_al, mem_ar) in
        (M.VLDR, ops)
    | op when op land 0b1100 = 0b1100 && not chkw -> Error `Invalid
    | _ -> Error `Invalid

  let system_register_64bit b =
    let open Result.Let_syntax in
    let b22 = tst b 22 |> Bool.to_int in
    let b20 = tst b 20 |> Bool.to_int in
    let dl = concat b22 b20 1 in
    let oprs chk = p5oprs b chk (preg_a, imm_4d, reg_d, reg_c, creg_b) in
    match dl with
    | 0b00 -> Error `Invalid
    | 0b01 -> Error `Invalid
    | 0b10 ->
        let%map ops = oprs chk_au in
        (M.MCRR, ops)
    | 0b11 ->
        let%map ops = oprs chk_av in
        (M.MRRC, ops)
    | _ -> Error `Invalid

  let system_register_load_store b =
    let open Result.Let_syntax in
    let puw = concat (ex b 24 23) (tst b 21 |> Bool.to_int) 1 in
    let chkcrd = ex b 15 12 = 0b0101 in
    let chkl = tst b 20 in
    let chkrn = ex b 19 16 = 0b1111 in
    match puw with
    | 0b000 -> Error `Invalid
    | _ when not chkcrd -> Error `Invalid
    | _ when chkcrd && not chkl ->
        let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
        (M.STC, ops)
    | _ when chkcrd && chkl && not chkrn ->
        let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ae) in
        (M.LDC, ops)
    | _ when chkcrd && chkl && chkrn ->
        let%map ops = p3oprs b dummy_chk (preg_a, creg_a, mem_ad) in
        (M.LDC, ops)
    | _ -> Error `Invalid

  let parse_load_store_reg b =
    let open Result.Let_syntax in
    let op3 chk = p3oprs b chk (reg_d, reg_l, mem_n) in
    let b20 = tst b 20 |> Bool.to_int in
    let b24 = tst b 24 |> Bool.to_int in
    let b21 = tst b 21 |> Bool.to_int in
    match (b20, ex b 6 5, b24, b21) with
    | 0, 1, 0, 0 ->
        let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
        (M.STRH, ops)
    | 0, 1, 0, 1 ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.STRHT, ops)
    | 0, 1, 1, _ ->
        let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
        (M.STRH, ops)
    | 0, 2, 0, 0 ->
        let%map ops = op3 chk_ae in
        (M.LDRD, ops)
    | 0, 2, 0, 1 -> Error `Invalid
    | 0, 2, 1, _ ->
        let%map ops = op3 chk_ae in
        (M.LDRD, ops)
    | 0, 3, 0, 0 ->
        let%map ops = op3 chk_af in
        (M.STRD, ops)
    | 0, 3, 1, 1 -> Error `Invalid
    | 0, 3, 1, _ ->
        let%map ops = op3 chk_af in
        (M.STRD, ops)
    | 1, 1, 0, 0 ->
        let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
        (M.LDRH, ops)
    | 1, 1, 0, 1 ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.LDRHT, ops)
    | 1, 1, 1, _ ->
        let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
        (M.LDRH, ops)
    | 1, 2, 0, 0 ->
        let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
        (M.LDRSB, ops)
    | 1, 2, 0, 1 ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.LDRSBT, ops)
    | 1, 2, 1, _ ->
        let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
        (M.LDRSB, ops)
    | 1, 3, 0, 0 ->
        let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
        (M.LDRSH, ops)
    | 1, 3, 1, 1 ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.LDRSHT, ops)
    | 1, 3, 1, _ ->
        let%map ops = p2oprs b chk_ad (reg_d, mem_n) in
        (M.LDRSH, ops)
    | _ -> Error `Invalid

  let parse_load_store_imm b =
    let open Result.Let_syntax in
    let b24 = tst b 24 |> Bool.to_int in
    let b21 = tst b 21 |> Bool.to_int in
    let b20 = tst b 20 |> Bool.to_int in
    let pw = concat b24 b21 1 in
    match (b20, ex b 6 5, pw, ex b 19 16) with
    | 0, 1, 1, _ ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.STRHT, ops)
    | 0, 1, _, _ ->
        let%map ops = p2oprs b chk_ag (reg_d, mem_o) in
        (M.STRH, ops)
    | 0, 2, _, 15 ->
        let%map ops = p3oprs b chk_u (reg_d, reg_l, mem_h) in
        (M.LDRD, ops)
    | 0, 2, 0, rn when rn <> 15 ->
        let%map ops = p3oprs b chk_ai (reg_d, reg_l, mem_o) in
        (M.LDRD, ops)
    | 0, 2, 1, rn when rn <> 15 -> Error `Invalid
    | 0, 2, _, rn when rn <> 15 ->
        let%map ops = p3oprs b chk_ai (reg_d, reg_l, mem_o) in
        (M.LDRD, ops)
    | 0, 3, 1, _ -> Error `Invalid
    | 0, 3, _, _ ->
        let%map ops = p3oprs b chk_aj (reg_d, reg_l, mem_o) in
        (M.STRD, ops)
    | 1, 1, pw, 15 when pw <> 1 ->
        let%map ops = p2oprs b chk_g (reg_d, mem_h) in
        (M.LDRH, ops)
    | 1, 1, 0, rn when rn <> 15 ->
        let%map ops = p2oprs b chk_t (reg_d, mem_h) in
        (M.LDRH, ops)
    | 1, 1, 1, _ ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.LDRHT, ops)
    | 1, 1, 2, rn when rn <> 15 ->
        let%map ops = p2oprs b chk_ah (reg_d, mem_o) in
        (M.LDRH, ops)
    | 1, 1, 3, rn when rn <> 15 ->
        let%map ops = p2oprs b chk_ah (reg_d, mem_o) in
        (M.LDRH, ops)
    | 1, 2, pw, 15 when pw <> 1 ->
        let%map ops = p2oprs b chk_t (reg_d, mem_h) in
        (M.LDRSB, ops)
    | 1, 2, 1, _ ->
        let%map ops = p2oprs b chk_w (reg_d, mem_j) in
        (M.LDRSBT, ops)
    | 1, 2, _, rn when rn <> 15 ->
        let%map ops = p2oprs b chk_ah (reg_d, mem_o) in
        (M.LDRSB, ops)
    | 1, 3, pw, 15 when pw <> 1 ->
        let%map ops = p2oprs b chk_t (reg_d, mem_h) in
        (M.LDRSH, ops)
    | 1, 3, 0, rn when rn <> 15 ->
        let%map ops = p2oprs b chk_ah (reg_d, mem_o) in
        (M.LDRSH, ops)
    | _ -> Error `Invalid

  let parse_extra_load_store b =
    match tst b 22 with
    | false -> parse_load_store_reg b
    | true -> parse_load_store_imm b

  let parse_syn_primitives b =
    let open Result.Let_syntax in
    let op0 = tst b 23 |> Bool.to_int in
    let typ = ex b 22 21 in
    let l = tst b 20 |> Bool.to_int in
    let ex = tst b 9 |> Bool.to_int in
    let ord = tst b 8 |> Bool.to_int in
    match (op0, typ, l, ex, ord) with
    | 0, _, _, _, _ -> Error `Invalid
    | 1, 0, 0, 0, 0 ->
        let%map ops = p2oprs b chk_k (reg_c, mem_a) in
        (M.STL, ops)
    | 1, 0, 0, 0, 1 -> Error `Invalid
    | 1, 0, 0, 1, 0 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STLEX, ops)
    | 1, 0, 0, 1, 1 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STREX, ops)
    | 1, 0, 1, 0, 0 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDA, ops)
    | 1, 0, 1, 0, 1 -> Error `Invalid
    | 1, 0, 1, 1, 0 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDAEX, ops)
    | 1, 0, 1, 1, 1 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDREX, ops)
    | 1, 1, 0, 0, _ -> Error `Invalid
    | 1, 1, 0, 1, 0 ->
        let%map ops = p4oprs b chk_store_ex2 (reg_d, reg_a, reg_f, mem_a) in
        (M.STLEXD, ops)
    | 1, 1, 0, 1, 1 ->
        let%map ops = p4oprs b chk_store_ex2 (reg_d, reg_a, reg_f, mem_a) in
        (M.STREXD, ops)
    | 1, 1, 1, 0, _ -> Error `Invalid
    | 1, 1, 1, 1, 0 ->
        let%map ops = p3oprs b chk_l (reg_d, reg_l, mem_a) in
        (M.LDAEXD, ops)
    | 1, 1, 1, 1, 1 ->
        let%map ops = p3oprs b chk_l (reg_d, reg_l, mem_a) in
        (M.LDREXD, ops)
    | 1, 2, 0, 0, 0 ->
        let%map ops = p2oprs b chk_k (reg_c, mem_a) in
        (M.STLB, ops)
    | 1, 2, 0, 0, 1 -> Error `Invalid
    | 1, 2, 0, 1, 0 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STLEXB, ops)
    | 1, 2, 0, 1, 1 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STREXB, ops)
    | 1, 2, 1, 0, 0 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDAB, ops)
    | 1, 2, 1, 0, 1 -> Error `Invalid
    | 1, 2, 1, 1, 0 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDAEXB, ops)
    | 1, 2, 1, 1, 1 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDREXB, ops)
    | 1, 3, 0, 0, 0 ->
        let%map ops = p2oprs b chk_k (reg_c, mem_a) in
        (M.STLH, ops)
    | 1, 3, 0, 0, 1 -> Error `Invalid
    | 1, 3, 0, 1, 0 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STLEXH, ops)
    | 1, 3, 0, 1, 1 ->
        let%map ops = p3oprs b chk_store_ex1 (reg_d, reg_a, mem_a) in
        (M.STREXH, ops)
    | 1, 3, 1, 0, 0 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDAH, ops)
    | 1, 3, 1, 0, 1 -> Error `Invalid
    | 1, 3, 1, 1, 0 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDAEXH, ops)
    | 1, 3, 1, 1, 1 ->
        let%map ops = p2oprs b chk_k (reg_d, mem_a) in
        (M.LDREXH, ops)
    | _ -> Error `Invalid

  let parse_exception_gen cond b =
    let open Result.Let_syntax in
    match ex b 22 21 with
    | 0 when C.(equal cond AL) ->
        let%map ops = p1opr b dummy_chk imm_12d in
        (M.HLT, ops)
    | 1 when C.(equal cond AL) ->
        let%map ops = p1opr b dummy_chk imm_12d in
        (M.BKPT, ops)
    | 2 when C.(equal cond AL) ->
        let%map ops = p1opr b dummy_chk imm_12d in
        (M.HVC, ops)
    | 3 when C.(equal cond AL) ->
        let%map ops = p1opr b dummy_chk imm_4a in
        (M.SMC, ops)
    | _ -> Error `Invalid

  let parse_move_spec_reg b =
    let open Result.Let_syntax in
    match (tst b 21, tst b 9) with
    | false, false ->
        let%map ops = p2oprs b dummy_chk (reg_d, reg_e) in
        (M.MRS, ops)
    | false, true ->
        let%map ops = p2oprs b chk_g (reg_d, banked_reg_a) in
        (M.MRS, ops)
    | true, false ->
        let%map ops = p2oprs b chk_h (reg_k, reg_a) in
        (M.MSR, ops)
    | true, true ->
        let%map ops = p2oprs b chk_f (banked_reg_a, reg_a) in
        (M.MSR, ops)

  let parse_crc cond b =
    let open Result.Let_syntax in
    match cond with
    | C.AL -> (
      match ex b 22 21 lsl 8 with
      | 64 -> Error `Invalid
      | _ -> (
        match (ex b 22 21, tst b 9) with
        | 0, false ->
            let%map ops = reg_dca_oprs_with_unpre_a b in
            (M.CRC32B, ops)
        | 0, true ->
            let%map ops = reg_dca_oprs_with_unpre_a b in
            (M.CRC32CB, ops)
        | 1, false ->
            let%map ops = reg_dca_oprs_with_unpre_a b in
            (M.CRC32H, ops)
        | 1, true ->
            let%map ops = reg_dca_oprs_with_unpre_a b in
            (M.CRC32CH, ops)
        | 2, false ->
            let%map ops = reg_dca_oprs_with_unpre_a b in
            (M.CRC32W, ops)
        | 2, true ->
            let%map ops = reg_dca_oprs_with_unpre_a b in
            (M.CRC32CW, ops)
        | 3, _ -> Error `Invalid
        | _ -> Error `Invalid ) )
    | _ -> Error `Invalid

  let parse_int_arithmetic b =
    let open Result.Let_syntax in
    match ex b 22 21 with
    | 0 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QADD, ops)
    | 1 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QSUB, ops)
    | 2 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QDADD, ops)
    | 3 ->
        let%map ops = reg_dca_oprs_with_unpre_a b in
        (M.QDSUB, ops)
    | _ -> Error `Invalid

  let parse_misc cond b =
    let open Result.Let_syntax in
    match (ex b 22 21, ex b 6 4) with
    | _, 0 -> parse_move_spec_reg b
    | _, 4 -> parse_crc cond b
    | _, 5 -> parse_int_arithmetic b
    | _, 7 -> parse_exception_gen cond b
    | 0, _ -> Error `Invalid
    | 1, 1 ->
        let%map ops = p1opr b dummy_chk reg_a in
        (M.BX, ops)
    | 1, 2 ->
        let%map ops = p1opr b chk_d reg_a in
        (M.BXJ, ops)
    | 1, 3 ->
        let%map ops = p1opr b chk_d reg_a in
        (M.BLX, ops)
    | 1, 6 -> Error `Invalid
    | 2, _ -> Error `Invalid
    | 3, 1 ->
        let%map ops = p2oprs b chk_e (reg_d, reg_a) in
        (M.CLZ, ops)
    | 3, 2 -> Error `Invalid
    | 3, 3 -> Error `Invalid
    | 3, 6 -> Ok (M.ERET, e)
    | _ -> Error `Invalid

  let logic_arith_imm_shift b =
    let open Result.Let_syntax in
    match ex b 22 20 with
    | 0 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ORR, ops)
    | 1 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.ORRS, ops)
    | 2 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_b) in
        (M.MOV, ops)
    | 3 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_b) in
        (M.MOVS, ops)
    | 4 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.BIC, ops)
    | 5 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (M.BICS, ops)
    | 6 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_b) in
        (M.MVN, ops)
    | 7 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_b) in
        (M.MVNS, ops)
    | _ -> Error `Invalid

  let int_data_proc b =
    match ex b 23 20 with
    | 0b0000 -> Ok M.AND
    | 0b0001 -> Ok M.ANDS
    | 0b0010 -> Ok M.EOR
    | 0b0011 -> Ok M.EORS
    | 0b0100 -> Ok M.SUB
    | 0b0101 -> Ok M.SUBS
    | 0b0110 -> Ok M.RSB
    | 0b0111 -> Ok M.RSBS
    | 0b1000 -> Ok M.ADD
    | 0b1001 -> Ok M.ADDS
    | 0b1010 -> Ok M.ADC
    | 0b1011 -> Ok M.ADCS
    | 0b1100 -> Ok M.SBC
    | 0b1101 -> Ok M.SBCS
    | 0b1110 -> Ok M.RSC
    | 0b1111 -> Ok M.RSCS
    | _ -> Error `Invalid

  let int_test_comp b =
    match ex b 22 21 with
    | 0 -> Ok M.TST
    | 1 -> Ok M.TEQ
    | 2 -> Ok M.CMP
    | 3 -> Ok M.CMN
    | _ -> Error `Invalid

  let logic_arith_reg_shift b =
    let open Result.Let_syntax in
    match ex b 22 20 with
    | 0 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ORR, ops)
    | 1 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.ORRS, ops)
    | 2 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_a) in
        (M.MOV, ops)
    | 3 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_a) in
        (M.MOVS, ops)
    | 4 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.BIC, ops)
    | 5 ->
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_a) in
        (M.BICS, ops)
    | 6 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_a) in
        (M.MVN, ops)
    | 7 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_a, shift_a) in
        (M.MVNS, ops)
    | _ -> Error `Invalid

  let parse_data_proc_imm_sreg b =
    let open Result.Let_syntax in
    match ex b 24 23 with
    | 0 | 1 ->
        let%bind m = int_data_proc b in
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (m, ops)
    | 2 when tst b 20 ->
        let%bind m = int_test_comp b in
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_b) in
        (m, ops)
    | 3 -> logic_arith_imm_shift b
    | _ -> Error `Invalid

  let parse_data_proc_reg_sreg b =
    let open Result.Let_syntax in
    match (ex b 24 23, tst b 20) with
    | 0, _ | 1, _ ->
        let%bind m = int_data_proc b in
        let%map ops = p4oprs b dummy_chk (reg_d, reg_c, reg_a, shift_b) in
        (m, ops)
    | 2, true ->
        let%bind m = int_test_comp b in
        let%map ops = p3oprs b dummy_chk (reg_c, reg_a, shift_b) in
        (m, ops)
    | 3, _ -> logic_arith_reg_shift b
    | _ -> Error `Invalid

  let parse_uncond_misc b =
    let open Result.Let_syntax in
    match (tst b 16, tst b 4) with
    | false, _ -> cps b
    | true, false ->
        let%map ops = p1opr b dummy_chk endian_a in
        (M.SETEND, ops)
    | _ -> Error `Invalid

  let parse_group000v8 cond b =
    let open Result.Let_syntax in
    let o1 = ex b 24 20 in
    let o2 = tst b 7 |> Bool.to_int in
    let o3 = ex b 6 5 in
    let o4 = tst b 4 |> Bool.to_int in
    let o5 = tst b 5 |> Bool.to_int in
    let%map mnemonic, ops =
      match (o1, o2, o3, o4) with
      | 0b10000, _, _, _ when C.(equal cond UN) && o5 = 0 ->
          parse_uncond_misc b
      | _, 1, x, 1 when x <> 0 -> parse_extra_load_store b
      | x, 1, 0, 1 when x land 0b10000 = 0 -> Error `Unimpl
      | x, 1, 0, 1 when x land 0b10000 = 0b10000 -> parse_syn_primitives b
      | x, 0, _, _ when x land 0b11001 = 0b10000 -> parse_misc cond b
      | x, 1, _, 0 when x land 0b11001 = 0b10000 -> Error `Unimpl
      | x, _, _, 0 when x land 0b11001 <> 0b10000 ->
          parse_data_proc_imm_sreg b
      | x, 0, _, 1 when x land 0b11001 <> 0b10000 ->
          parse_data_proc_reg_sreg b
      | _ -> Error `Invalid
    in
    (mnemonic, None, ops)

  let parse_int_data_proc_imm b =
    let open Result.Let_syntax in
    let%bind m = int_data_proc b in
    let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
    (m, None, ops)

  let parse_mov_halfword_imm b =
    let open Result.Let_syntax in
    match (tst b 22, tst b 20) with
    | false, false ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MOV, None, ops)
    | false, true ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MOVS, None, ops)
    | true, _ ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12b) in
        (M.MOVT, None, ops)

  let parse_move_spec_reg_imm b =
    match ex b 7 0 with
    | 0 -> Ok (M.NOP, None, e)
    | 1 -> Ok (M.YIELD, None, e)
    | 2 -> Ok (M.WFE, None, e)
    | 3 -> Ok (M.WFI, None, e)
    | 4 -> Ok (M.SEV, None, e)
    | 5 -> Ok (M.SEVL, None, e)
    | 6 | 7 -> Ok (M.NOP, None, e)
    | op when op land 0b11111000 = 0b00001000 -> Ok (M.NOP, None, e)
    | op when op land 0b11110000 = 0b00010000 -> Ok (M.NOP, None, e)
    | op when op land 0b11100000 = 0b00100000 -> Ok (M.NOP, None, e)
    | op when op land 0b11000000 = 0b01000000 -> Ok (M.NOP, None, e)
    | op when op land 0b11000000 = 0b10000000 -> Ok (M.NOP, None, e)
    | op when op land 0b11100000 = 0b11000000 -> Ok (M.NOP, None, e)
    | op when op land 0b11110000 = 0b11100000 -> Ok (M.NOP, None, e)
    | op when op land 0b11110000 = 0b11110000 -> Ok (M.DBG, None, e)
    | _ -> Error `Invalid

  let parse_int_test_comp_imm b =
    let open Result.Let_syntax in
    match ex b 22 21 with
    | 0 ->
        let%map ops = p2oprs b dummy_chk (reg_c, imm_12a) in
        (M.TST, None, ops)
    | 1 ->
        let%map ops = p2oprs b dummy_chk (reg_c, imm_12a) in
        (M.TEQ, None, ops)
    | 2 ->
        let%map ops = p2oprs b dummy_chk (reg_c, imm_12a) in
        (M.CMP, None, ops)
    | 3 ->
        let%map ops = p2oprs b dummy_chk (reg_c, imm_12a) in
        (M.CMN, None, ops)
    | _ -> Error `Invalid

  let parse_logic_arith_imm b =
    let open Result.Let_syntax in
    match ex b 22 20 with
    | 0 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ORR, None, ops)
    | 1 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.ORRS, None, ops)
    | 2 ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MOV, None, ops)
    | 3 ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MOVS, None, ops)
    | 4 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.BIC, None, ops)
    | 5 ->
        let%map ops = p3oprs b dummy_chk (reg_d, reg_c, imm_12a) in
        (M.BICS, None, ops)
    | 6 ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MVN, None, ops)
    | 7 ->
        let%map ops = p2oprs b dummy_chk (reg_d, imm_12a) in
        (M.MVNS, None, ops)
    | _ -> Error `Invalid

  let parse_adv_simd_misc bin =
    let open Result.Let_syntax in
    let size = ex bin 19 18 in
    let b6 = tst bin 6 |> Bool.to_int in
    match (ex bin 17 16, ex bin 10 7, b6) with
    | 0, 0, _ ->
        let%bind dt = one_dt_s bin in
        let%map ops = p2oprs bin chk_undef_u (reg_x, reg_z) in
        (M.VREV64, dt, ops)
    | 0, 1, _ ->
        let%bind dt = one_dt_s bin in
        let%map ops = p2oprs bin chk_undef_u (reg_x, reg_z) in
        (M.VREV32, dt, ops)
    | 0, 2, _ ->
        let%bind dt = one_dt_s bin in
        let%map ops = p2oprs bin chk_undef_u (reg_x, reg_z) in
        (M.VREV16, dt, ops)
    | 0, 3, _ -> Error `Invalid
    | 0, 4, _ | 0, 5, _ ->
        let%bind dt = one_dt_c bin in
        let%map ops = p2oprs bin chk_undef_v (reg_x, reg_z) in
        (M.VPADDL, dt, ops)
    | 0, 6, 0 ->
        let%bind dt = one_dt_e () in
        let%map ops = p2oprs bin chk_undef_at (reg_ac, reg_ad) in
        (M.AESE, dt, ops)
    | 0, 6, 1 ->
        let%bind dt = one_dt_e () in
        let%map ops = p2oprs bin chk_undef_at (reg_ac, reg_ad) in
        (M.AESD, dt, ops)
    | 0, 7, 0 ->
        let%bind dt = one_dt_e () in
        let%map ops = p2oprs bin chk_undef_at (reg_ac, reg_ad) in
        (M.AESMC, dt, ops)
    | 0, 7, 1 ->
        let%bind dt = one_dt_e () in
        let%map ops = p2oprs bin chk_undef_at (reg_ac, reg_ad) in
        (M.AESIMC, dt, ops)
    | 0, 8, _ ->
        let%bind dt = one_dt_t bin in
        let%map ops = p2oprs bin chk_undef_x (reg_x, reg_z) in
        (M.VCLS, dt, ops)
    | 0, 9, _ ->
        let%bind dt = one_dt_u bin in
        let%map ops = p2oprs bin chk_undef_x (reg_x, reg_z) in
        (M.VCLZ, dt, ops)
    | 0, 10, _ ->
        let%bind dt = one_dt_e () in
        let%map ops = p2oprs bin chk_undef_y (reg_x, reg_z) in
        (M.VCNT, dt, ops)
    | 0, 11, _ ->
        let%map ops = p2oprs bin chk_undef_y (reg_x, reg_z) in
        (M.VMVN, None, ops)
    | 0, 12, _ | 0, 13, _ ->
        let%bind dt = one_dt_c bin in
        let%map ops = p2oprs bin chk_undef_v (reg_x, reg_z) in
        (M.VPADAL, dt, ops)
    | 0, 14, _ ->
        let%bind dt = one_dt_t bin in
        let%map ops = p2oprs bin chk_undef_x (reg_x, reg_z) in
        (M.VQABS, dt, ops)
    | 0, 15, _ ->
        let%bind dt = one_dt_t bin in
        let%map ops = p2oprs bin chk_undef_x (reg_x, reg_z) in
        (M.VQNEG, dt, ops)
    | 1, b, _ when b land 7 = 0 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCGT, dt, ops)
    | 1, b, _ when b land 7 = 1 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCGE, dt, ops)
    | 1, b, _ when b land 7 = 2 ->
        let%bind dt = one_dt_w b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCEQ, dt, ops)
    | 1, b, _ when b land 7 = 3 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCLE, dt, ops)
    | 1, b, _ when b land 7 = 4 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VCLT, dt, ops)
    | 1, b, _ when b land 7 = 6 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VABS, dt, ops)
    | 1, b, _ when b land 7 = 7 ->
        let%bind dt = one_dt_v b in
        let%map ops = p3oprs b chk_undef_ac (reg_x, reg_z, imm0) in
        (M.VNEG, dt, ops)
    | 1, 5, 1 ->
        let%bind dt = one_dt_ai () in
        let%map ops = p2oprs bin chk_undef_au (reg_ac, reg_ad) in
        (M.SHA1H, dt, ops)
    | 2, 0, _ when size = 0 ->
        let%map ops = p2oprs bin chk_undef_z (reg_x, reg_z) in
        (M.VSWP, None, ops)
    | 2, 1, _ ->
        let%bind dt = one_dt_s bin in
        let%map ops = p2oprs bin chk_undef_aa (reg_x, reg_z) in
        (M.VTRN, dt, ops)
    | 2, 2, _ ->
        let%bind dt = one_dt_s bin in
        let%map ops = p2oprs bin chk_undef_ab (reg_x, reg_z) in
        (M.VUZP, dt, ops)
    | 2, 3, _ ->
        let%bind dt = one_dt_s bin in
        let%map ops = p2oprs bin chk_undef_ab (reg_x, reg_z) in
        (M.VZIP, dt, ops)
    | 2, 4, 0 ->
        let%bind dt = one_dt_x bin in
        let%map ops = p2oprs bin chk_undef_ad (reg_ac, reg_ad) in
        (M.VMOVN, dt, ops)
    | 2, 4, 1 | 2, 5, _ ->
        let%bind dt = one_dt_y bin in
        let%map ops = p2oprs bin chk_undef_ad (reg_ac, reg_ad) in
        (M.VQMOVN, dt, ops)
    | 2, 6, 0 ->
        let%bind dt = one_dt_u bin in
        let%map ops = p2oprs bin chk_undef_ad (reg_ac, reg_ad) in
        (M.VSHLL, dt, ops)
    | 2, 7, 0 ->
        let%bind dt = one_dt_ai () in
        let%map ops = p2oprs bin chk_undef_au (reg_ac, reg_ad) in
        (M.SHA1SU1, dt, ops)
    | 2, 7, 1 ->
        let%bind dt = one_dt_ai () in
        let%map ops = p2oprs bin chk_undef_au (reg_ac, reg_ad) in
        (M.SHA256SU0, dt, ops)
    | 2, 8, _ ->
        let%bind dt = one_dt_aa () in
        let%map ops = p2oprs bin chk_undef_af (reg_x, reg_z) in
        (M.VRINTN, dt, ops)
    | 2, 9, _ ->
        let%bind dt = one_dt_aa () in
        let%map ops = p2oprs bin chk_undef_af (reg_x, reg_z) in
        (M.VRINTX, dt, ops)
    | 2, 10, _ ->
        let%bind dt = one_dt_aa () in
        let%map ops = p2oprs bin chk_undef_af (reg_x, reg_z) in
        (M.VRINTA, dt, ops)
    | 2, 11, _ ->
        let%bind dt = one_dt_aa () in
        let%map ops = p2oprs bin chk_undef_af (reg_x, reg_z) in
        (M.VRINTZ, dt, ops)
    | 2, 12, 0 | 2, 14, 0 ->
        let%bind dt = two_dt_c bin in
        let%map ops = p2oprs bin chk_undef_ae (reg_x, reg_z) in
        (M.VCVT, dt, ops)
    | 2, 12, _ -> Error `Invalid
    | 2, 13, _ ->
        let%bind dt = one_dt_aa () in
        let%map ops = p2oprs bin chk_undef_af (reg_x, reg_z) in
        (M.VRINTM, dt, ops)
    | 2, 14, 1 -> Error `Invalid
    | 2, 15, _ ->
        let%bind dt = one_dt_aa () in
        let%map ops = p2oprs bin chk_undef_af (reg_x, reg_z) in
        (M.VRINTP, dt, ops)
    | 3, b, _ when b land 14 = 0 ->
        let%bind dt = two_dt_c bin in
        let%map ops = p2oprs bin chk_undef_af (reg_ai, reg_ah) in
        (M.VCVTA, dt, ops)
    | 3, b, _ when b land 14 = 2 ->
        let%bind dt = two_dt_c bin in
        let%map ops = p2oprs bin chk_undef_af (reg_ai, reg_ah) in
        (M.VCVTN, dt, ops)
    | 3, b, _ when b land 14 = 4 ->
        let%bind dt = two_dt_c bin in
        let%map ops = p2oprs bin chk_undef_af (reg_ai, reg_ah) in
        (M.VCVTP, dt, ops)
    | 3, b, _ when b land 14 = 5 ->
        let%bind dt = two_dt_c bin in
        let%map ops = p2oprs bin chk_undef_af (reg_ai, reg_ah) in
        (M.VCVTM, dt, ops)
    | 3, b, _ when b land 13 = 8 ->
        let%bind dt = one_dt_z b in
        let%map ops = p2oprs b chk_undef_af (reg_x, reg_z) in
        (M.VRECPE, dt, ops)
    | 3, b, _ when b land 13 = 9 ->
        let%bind dt = one_dt_z b in
        let%map ops = p2oprs b chk_undef_af (reg_x, reg_z) in
        (M.VRSQRTE, dt, ops)
    | 3, b, _ when b land 12 = 12 ->
        let%bind dt = two_dt_b bin in
        let%map ops = p2oprs bin chk_undef_w (reg_x, reg_z) in
        (M.VCVT, dt, ops)
    | _ -> Error `Invalid

  let parse_adv_simd_dupl _ = Error `Unimpl

  let parse_adv_simd_same _ = Error `Unimpl

  let parse_adv_simd_modi _ = Error `Unimpl

  let parse_adv_simd_diff _ = Error `Unimpl

  let parse_adv_simd_scal _ = Error `Unimpl

  let parse_adv_simd_shift _ = Error `Unimpl

  let parse_adv_simd_data_proc_v8 bin =
    let open Result.Let_syntax in
    let pick b = tst bin b |> Bool.to_int in
    let isvext b = b land 0b110000000000000 = 0b110000000000000 in
    let ismisc b = b land 0b110000000010000 = 0b110000000000000 in
    let isvtb b = b land 0b110000000011000 = 0b110000000010000 in
    let isdupl b = b land 0b110000000011000 = 0b110000000011000 in
    let isdiff b = b land 0b110000000000000 <> 0b110000000000000 in
    let ismodi b = b land 0b111000000000001 = 0 in
    let chkop b = b land 1 = 1 in
    match (ex bin 24 23, ex bin 21 7, pick 6, pick 4) with
    | 1, b, _, 0 when isvext b ->
        let%bind dt = one_dt_e () in
        let%map ops = p4oprs bin chk_undef_g (reg_x, reg_y, reg_z, imm_4c) in
        (M.VEXT, dt, ops)
    | 3, b, _, 0 when ismisc b -> parse_adv_simd_misc bin
    | 3, b, 0, 0 when isvtb b ->
        let%bind dt = one_dt_e () in
        let%map ops = p3oprs bin dummy_chk (reg_ac, reglist_a, reg_af) in
        (M.VTBL, dt, ops)
    | 3, b, 1, 0 when isvtb b ->
        let%bind dt = one_dt_e () in
        let%map ops = p3oprs bin dummy_chk (reg_ac, reglist_a, reg_af) in
        (M.VTBX, dt, ops)
    | 3, b, _, 0 when isdupl b -> parse_adv_simd_dupl bin
    | b, _, _, _ when b land 1 = 0 -> parse_adv_simd_same bin
    | b, b2, _, 1 when chkop b && ismodi b2 -> parse_adv_simd_modi bin
    | b, b2, 0, 0 when chkop b && isdiff b2 -> parse_adv_simd_diff bin
    | b, b2, 1, 0 when chkop b && isdiff b2 -> parse_adv_simd_scal bin
    | b, b2, _, 1 when chkop b && not (ismodi b2) -> parse_adv_simd_shift bin
    | _ -> Error `Invalid

  let parse_group001v8 cond b =
    let open Result.Let_syntax in
    match (ex b 24 23, ex b 21 20) with
    | _ when C.(equal cond UN) -> parse_adv_simd_data_proc_v8 b
    | 1, _ | 0, _ -> parse_int_data_proc_imm b
    | 2, 0 -> parse_mov_halfword_imm b
    | 2, 2 when not (tst b 22) -> parse_move_spec_reg_imm b
    | 2, 2 ->
        let%map ops = p2oprs b dummy_chk (apsr_xb, imm_12c) in
        (M.MSR, None, ops)
    | 2, 1 | 2, 3 -> parse_int_test_comp_imm b
    | 3, _ -> parse_logic_arith_imm b
    | _ -> Error `Invalid

  let parse_group010v8 cond b =
    let open Result.Let_syntax in
    let b20 = tst b 20 |> Bool.to_int in
    let b22 = tst b 22 |> Bool.to_int in
    let op = concat b20 b22 1 in
    let b24 = tst b 24 |> Bool.to_int in
    let b21 = tst b 21 |> Bool.to_int in
    let pw = concat b24 b21 1 in
    let chkrn () = ex b 19 16 = 0b1111 in
    let chkpw () = pw = 0b01 in
    let ispushpop () = ex b 19 16 = 0b1101 in
    let%map mnemonic, ops =
      match op with
      | _ when C.(equal cond UN) -> Error `Invalid
      | 0b00 when chkpw () ->
          let%map ops = p2oprs b chk_z (reg_d, mem_k) in
          (M.STRT, ops)
      | 0b00 when ispushpop () ->
          let%map ops = p1opr b chk_y reg_d in
          (M.PUSH, ops)
      | 0b00 ->
          let%map ops = p2oprs b chk_aa (reg_d, mem_l) in
          (M.STR, ops)
      | 0b01 when chkpw () ->
          let%map ops = p2oprs b chk_w (reg_d, mem_k) in
          (M.STRBT, ops)
      | 0b01 ->
          let%map ops = p2oprs b chk_ac (reg_d, mem_l) in
          (M.STRB, ops)
      | 0b10 when (not (chkpw ())) && chkrn () ->
          let%map ops = p2oprs b dummy_chk (reg_d, mem_m) in
          (M.LDR, ops)
      | 0b10 when chkpw () ->
          let%map ops = p2oprs b chk_w (reg_d, mem_k) in
          (M.LDRT, ops)
      | 0b10 when ispushpop () ->
          let%map ops = p1opr b chk_y reg_d in
          (M.POP, ops)
      | 0b10 when not (chkrn ()) ->
          let%map ops = p2oprs b chk_aa (reg_d, mem_l) in
          (M.LDR, ops)
      | 0b11 when (not (chkpw ())) && chkrn () ->
          let%map ops = p2oprs b chk_g (reg_d, mem_m) in
          (M.LDRB, ops)
      | 0b11 when chkpw () ->
          let%map ops = p2oprs b chk_w (reg_d, mem_k) in
          (M.LDRBT, ops)
      | 0b11 when not (chkrn ()) ->
          let%map ops = p2oprs b chk_ab (reg_d, mem_l) in
          (M.LDRB, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, None, ops)

  let parse_group0110v8 cond b =
    let open Result.Let_syntax in
    let b20 = tst b 20 |> Bool.to_int in
    let b22 = tst b 22 |> Bool.to_int in
    let op = concat b20 b22 1 in
    let b24 = tst b 24 |> Bool.to_int in
    let b21 = tst b 21 |> Bool.to_int in
    let pw = concat b24 b21 1 in
    let chkpw () = pw = 0b01 in
    let%map mnemonic, ops =
      match op with
      | _ when C.(equal cond UN) -> Error `Invalid
      | 0b00 when chkpw () ->
          let%map ops = p2oprs b chk_al (reg_d, mem_q) in
          (M.STRT, ops)
      | 0b00 ->
          let%map ops = p2oprs b chk_am (reg_d, mem_r) in
          (M.STR, ops)
      | 0b01 when chkpw () ->
          let%map ops = p2oprs b chk_v (reg_d, mem_q) in
          (M.STRBT, ops)
      | 0b01 ->
          let%map ops = p2oprs b chk_an (reg_d, mem_r) in
          (M.STRB, ops)
      | 0b10 when chkpw () ->
          let%map ops = p2oprs b chk_v (reg_d, mem_q) in
          (M.LDRT, ops)
      | 0b10 ->
          let%map ops = p2oprs b chk_am (reg_d, mem_r) in
          (M.LDR, ops)
      | 0b11 when chkpw () ->
          let%map ops = p2oprs b chk_v (reg_d, mem_q) in
          (M.LDRBT, ops)
      | 0b11 ->
          let%map ops = p2oprs b chk_an (reg_d, mem_r) in
          (M.LDRB, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, None, ops)

  let parse_group0111v8 cond b =
    let open Result.Let_syntax in
    let op = concat (ex b 24 20) (ex b 7 5) 3 in
    let%map mnemonic, ops =
      match op with
      | op when op land 0b11000000 = 0b00000000 ->
          parse_parallel_arithmetic b
      | 0b01000101 ->
          let%map ops = reg_dca_oprs_with_unpre_a b in
          (M.SEL, ops)
      | 0b01000001 -> Error `Invalid
      | op when op land 0b11111011 = 0b01000000 ->
          let%map ops = p4oprs b chk_c (reg_d, reg_c, reg_a, shift_d) in
          (M.PKHBT, ops)
      | op when op land 0b11111011 = 0b01000010 ->
          let%map ops = p4oprs b chk_c (reg_d, reg_c, reg_a, shift_d) in
          (M.PKHTB, ops)
      | op when op land 0b11111011 = 0b01001001 -> Error `Invalid
      | op when op land 0b11111001 = 0b01001000 -> Error `Invalid
      | op when op land 0b11110011 = 0b01100001 -> Error `Invalid
      | op when op land 0b11110001 = 0b01100000 -> Error `Invalid
      | op when op land 0b11011111 = 0b01010001 -> parse_saturate_16bit b
      | op when op land 0b11011111 = 0b01010101 -> Error `Invalid
      | op when op land 0b11011011 = 0b01011001 -> parse_reverse_bit_byte b
      | op when op land 0b11010001 = 0b01010000 -> parse_saturate_32bit b
      | op when op land 0b11000111 = 0b01000111 -> Error `Invalid
      | op when op land 0b11000111 = 0b01000011 -> parse_extend_and_add b
      | op when op land 0b11000000 = 0b10000000 ->
          parse_signed_multiply_divide b
      | 0b11000000 -> unsigned_sum_absolute_diff b
      | 0b11000100 -> Error `Invalid
      | op when op land 0b11111011 = 0b11001000 -> Error `Invalid
      | op when op land 0b11110011 = 0b11010000 -> Error `Invalid
      | op when op land 0b11100111 = 0b11000111 -> Error `Invalid
      | op when op land 0b11110111 = 0b11100111 -> Error `Invalid
      | op when op land 0b11110011 = 0b11100000 -> parse_bitfield_insert b
      | 0b11110111 -> Error `Invalid
      | 0b11111111 -> parse_permanently_undefined cond b
      | op when op land 0b11110011 = 0b11110000 -> Error `Invalid
      | op when op land 0b11010011 = 0b11000010 -> Error `Invalid
      | op when op land 0b11010011 = 0b11010010 -> parse_bitfield_extract b
      | op when op land 0b11000111 = 0b11000011 -> Error `Invalid
      | op when op land 0b11000011 = 0b11000001 -> Error `Invalid
      | _ -> Error `Invalid
    in
    (mnemonic, None, ops)

  let parse_group100v8 cond b =
    let open Result.Let_syntax in
    let b22 = tst b 22 |> Bool.to_int in
    let b24 = tst b 24 |> Bool.to_int in
    let b23 = tst b 23 |> Bool.to_int in
    let b20 = tst b 20 |> Bool.to_int in
    let opp = concat b22 b24 1 in
    let ul = concat b23 b20 1 in
    let op = concat opp ul 2 in
    let chkrl () = not (tst b 15) in
    let ispushpop () = ex b 19 16 = 0b1101 in
    let%map mnemonic, ops =
      match op with
      | _ when C.(equal cond UN) -> Error `Invalid
      | 0b0000 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STMDA, ops)
      | 0b0001 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMDA, ops)
      | 0b0010 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STM, ops)
      | 0b0011 when ispushpop () ->
          let%map ops = p1opr b chk_at reglist_k in
          (M.POP, ops)
      | 0b0011 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDM, ops)
      | 0b0100 when ispushpop () ->
          let%map ops = p1opr b dummy_chk reglist_k in
          (M.PUSH, ops)
      | 0b0100 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STMDB, ops)
      | 0b0101 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMDB, ops)
      | 0b0110 ->
          let%map ops = p2oprs b chk_ar (reg_wa, reglist_k) in
          (M.STMIB, ops)
      | 0b0111 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMIB, ops)
      | 0b1000 ->
          let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
          (M.STMDA, ops)
      | 0b1100 ->
          let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
          (M.STMDB, ops)
      | 0b1010 ->
          let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
          (M.STMIA, ops)
      | 0b1110 ->
          let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
          (M.STMIB, ops)
      | 0b1001 when chkrl () ->
          let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
          (M.LDMDA, ops)
      | 0b1001 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMDA, ops)
      | 0b1101 when chkrl () ->
          let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
          (M.LDMDB, ops)
      | 0b1101 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMDB, ops)
      | 0b1011 when chkrl () ->
          let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
          (M.LDMIA, ops)
      | 0b1011 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMIA, ops)
      | 0b1111 when chkrl () ->
          let%map ops = p2oprs b chk_ar (reg_c, reglist_k) in
          (M.LDMIA, ops)
      | 0b1111 ->
          let%map ops = p2oprs b chk_as (reg_wa, reglist_k) in
          (M.LDMIA, ops)
      | _ -> Error `Invalid
    in
    (mnemonic, None, ops)

  let parse_group110v8 cond b =
    let open Result.Let_syntax in
    let op = ex b 24 21 in
    let chkfloat = ex b 11 9 = 0b101 in
    let chksys = ex b 11 9 = 0b111 in
    let%map mnemonic, ops =
      match op with
      | _ when C.(equal cond UN) -> Error `Invalid
      | op when op land 0b1101 = 0b0000 && chkfloat -> adv_simd_float_64bit b
      | op when op land 0b1101 <> 0b0000 && chkfloat ->
          adv_simd_float_load_store b
      | op when op land 0b1101 = 0b0000 && chksys -> system_register_64bit b
      | op when op land 0b1101 <> 0b0000 && chksys ->
          system_register_load_store b
      | _ -> Error `Invalid
    in
    (mnemonic, None, ops)

  let other_floating_point_data_proc (ctx : Context.t) cond b =
    let open Result.Let_syntax in
    let op2 = ex b 19 16 in
    let op3 = ex b 7 6 in
    let op = concat op2 op3 2 in
    match op with
    | op when op land 0b111011 = 0b001001 && not (tst b 16) ->
        let%bind dt = two_dt_i b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an_r) in
        (M.VCVTB, dt, ops)
    | op when op land 0b111011 = 0b001001 ->
        let%bind dt = two_dt_i b in
        let%map ops = p2oprs b dummy_chk (reg_al_r, reg_an) in
        (M.VCVTB, dt, ops)
    | op when op land 0b111011 = 0b001011 && not (tst b 16) ->
        let%bind dt = two_dt_i b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an_r) in
        (M.VCVTT, dt, ops)
    | op when op land 0b111011 = 0b001011 ->
        let%bind dt = two_dt_i b in
        let%map ops = p2oprs b dummy_chk (reg_al_r, reg_an) in
        (M.VCVTT, dt, ops)
    | op when op land 0b110111 = 0b010101 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, fpimm0) in
        (M.VCMP, dt, ops)
    | op when op land 0b110111 = 0b010111 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, fpimm0) in
        (M.VCMPE, dt, ops)
    | op when op land 0b111001 = 0b011001 && tst b 7 ->
        let%bind dt = two_dt_j b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VRINTZ, dt, ops)
    | op when op land 0b111001 = 0b011001 ->
        let%bind dt = two_dt_j b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VRINTR, dt, ops)
    | op when op land 0b110001 = 0b110001 && C.(equal cond UN) ->
        let%bind dt = two_dt_k b in
        let%map ops = p2oprs b (chk_dg ctx.it) (reg_ao, reg_an) in
        (M.VCVTA, dt, ops)
    | op when op land 0b110101 = 0b110001 && C.(equal cond UN) ->
        let%bind dt = two_dt_k b in
        let%map ops = p2oprs b (chk_dg ctx.it) (reg_ao, reg_an) in
        (M.VCVTN, dt, ops)
    | op when op land 0b111001 = 0b110001 && C.(equal cond UN) ->
        let%bind dt = two_dt_k b in
        let%map ops = p2oprs b (chk_dg ctx.it) (reg_ao, reg_an) in
        (M.VCVTP, dt, ops)
    | op when op land 0b111101 = 0b110001 && C.(equal cond UN) ->
        let%bind dt = two_dt_k b in
        let%map ops = p2oprs b (chk_dg ctx.it) (reg_ao, reg_an) in
        (M.VCVTM, dt, ops)
    | op when op land 0b110001 = 0b110001 && tst b 7 ->
        let%bind dt = two_dt_g b in
        let%map ops = p2oprs b dummy_chk (reg_ar, reg_as) in
        (M.VCVT, dt, ops)
    | op when op land 0b110001 = 0b110001 ->
        let%bind dt = two_dt_g b in
        let%map ops = p2oprs b dummy_chk (reg_ar, reg_as) in
        (M.VCVTR, dt, ops)
    | op when op land 0b101001 = 0b101001 ->
        let%bind dt = two_dt_h b in
        let%map ops = p3oprs b dummy_chk (reg_at, reg_at, imm_i) in
        (M.VCVT, dt, ops)
    | 0b000101 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VNEG, dt, ops)
    | 0b000111 ->
        let%bind dt = one_dt_af b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VSQRT, dt, ops)
    | 0b011101 ->
        let%bind dt = two_dt_j b in
        let%map ops = p2oprs b dummy_chk (reg_al, reg_an) in
        (M.VRINTX, dt, ops)
    | 0b011111 ->
        let%bind dt = two_dt_d b in
        let%map ops = p2oprs b dummy_chk (reg_al_r, reg_an) in
        (M.VCVT, dt, ops)
    | 0b100001 when C.(equal cond UN) ->
        let%bind dt = two_dt_j b in
        let%map ops = p2oprs b (chk_dg ctx.it) (reg_al, reg_an) in
        (M.VRINTA, dt, ops)
    | 0b100101 when C.(equal cond UN) ->
        let%bind dt = two_dt_j b in
        let%map ops = p2oprs b (chk_dg ctx.it) (reg_al, reg_an) in
        (M.VRINTN, dt, ops)
    | 0b101001 when C.(equal cond UN) ->
        let%bind dt = two_dt_j b in
        let%map ops = p2oprs b (chk_dg ctx.it) (reg_al, reg_an) in
        (M.VRINTP, dt, ops)
    | 0b101101 when C.(equal cond UN) ->
        let%bind dt = two_dt_j b in
        let%map ops = p2oprs b (chk_dg ctx.it) (reg_al, reg_an) in
        (M.VRINTM, dt, ops)
    | 0b100001 ->
        let%bind dt = two_dt_g b in
        let%map ops = p2oprs b dummy_chk (reg_ar, reg_as) in
        (M.VCVTR, dt, ops)
    | 0b100011 ->
        let%bind dt = two_dt_g b in
        let%map ops = p2oprs b dummy_chk (reg_ar, reg_as) in
        (M.VCVT, dt, ops)
    | _ -> Error `Invalid

  let floating_point_data_proc ctx cond b =
    let open Result.Let_syntax in
    let op1 = ex b 23 20 in
    let op3 = ex b 7 6 in
    let op = concat op1 op3 2 in
    match op with
    | op when op land 0b100001 = 0b000000 ->
        let%bind dt = one_dt_af b in
        let%map ops = p3oprs b dummy_chk (reg_al, reg_am, reg_an) in
        (M.VSEL, dt, ops)
    | op when op land 0b101111 = 0b101100 ->
        other_floating_point_data_proc ctx cond b
    | _ -> Error `Invalid

  let parse_group111v8 ctx cond b =
    let open Result.Let_syntax in
    let op = tst b 24 in
    let coproc = ex b 11 9 in
    match (op, coproc) with
    | false, 0b101 -> floating_point_data_proc ctx cond b
    | false, _ -> Error `Unimpl (* coprocessor data operations *)
    | true, x when x <> 0b101 -> Error `Unimpl (* supervisor call *)
    | _ -> Error `Invalid

  let parse (ctx : Context.t) b =
    let open Result.Let_syntax in
    let b4 = tst b 4 |> Bool.to_int in
    let op = concat (ex b 27 25) b4 1 in
    let%bind cond = parse_cond (ex b 31 28) in
    let%map mnemonic, simdtyp, ops =
      match op with
      | op when op land 0b1110 = 0b0000 -> parse_group000v8 cond b
      | op when op land 0b1110 = 0b0010 -> parse_group001v8 cond b
      | op when op land 0b1110 = 0b0100 -> parse_group010v8 cond b
      | op when op land 0b1111 = 0b0110 -> parse_group0110v8 cond b
      | op when op land 0b1111 = 0b0111 -> parse_group0111v8 cond b
      | op when op land 0b1110 = 0b1000 -> parse_group100v8 cond b
      | op when op land 0b1110 = 0b1010 -> Error `Unimpl
      | op when op land 0b1110 = 0b1100 -> parse_group110v8 cond b
      | op when op land 0b1110 = 0b1110 -> parse_group111v8 ctx cond b
      | 0b1111 -> Error `Unimpl (* Advanced SIMD, floating-point *)
      | _ -> Error `Invalid
    in
    (mnemonic, Some cond, 0, None, None, simdtyp, ops, None)
end

let decode (ctx : Context.t) mode endian bytes =
  let open Result.Let_syntax in
  (* this will fail on a 32-bit machine *)
  assert (Int.num_bits > 31);
  let%bind bytes2 =
    Option.try_with (fun () -> Bytes.subo bytes ~len:2)
    |> Result.of_option ~error:`OOB
  in
  let b1 = Char.to_int @@ Bytes.get bytes2 0 in
  let b2 = Char.to_int @@ Bytes.get bytes2 1 in
  let%bind enc, len =
    match mode with
    | Mode.Thumbv7 -> (
        let n =
          match endian with
          | `LE -> b1 lor (b2 lsl 8) land 0xFFFF
          | `BE -> (b1 lsl 8) lor b2 land 0xFFFF
        in
        match n lsr 11 with
        | 0x1D | 0x1E | 0x1F ->
            if Bytes.length bytes < 4 then Error `OOB
            else
              let b3 = Char.to_int @@ Bytes.get bytes 2 in
              let b4 = Char.to_int @@ Bytes.get bytes 3 in
              let n' =
                match endian with
                | `LE -> b3 lor (b4 lsl 8) land 0xFFFF
                | `BE -> (b3 lsl 8) lor b4 land 0xFFFF
              in
              return ((n' lsl 16) lor n land 0xFFFFFFFF, 4)
        | _ -> return (n, 2) )
    | _ when Bytes.length bytes >= 4 ->
        let b3 = Char.to_int @@ Bytes.get bytes 2 in
        let b4 = Char.to_int @@ Bytes.get bytes 3 in
        let n =
          match endian with
          | `BE -> (b1 lsl 24) lor (b2 lsl 16) lor (b3 lsl 8) lor b4
          | `LE -> b1 lor (b2 lsl 8) lor (b3 lsl 16) lor (b4 lsl 24)
        in
        return (n land 0xFFFFFFFF, 4)
    | _ -> Error `OOB
  in
  let bytes = Bytes.subo bytes ~len in
  let%map ( mnemonic
          , cond
          , it_state
          , write_back
          , qualifier
          , simd_data
          , operands
          , cflag ) =
    match mode with
    | Mode.Armv7 -> V7.parse enc
    | Mode.Thumbv7 when len = 2 -> ThumbV7.parse16 ctx enc
    | Mode.Thumbv7 -> ThumbV7.parse32 ctx enc
    | Mode.Armv8 -> V8.parse ctx enc
  in
  I.
    { mode
    ; endian
    ; mnemonic
    ; bytes
    ; cond
    ; write_back
    ; qualifier
    ; simd_data
    ; operands
    ; it_state
    ; cflag }

let decode_exn ctx mode endian bytes =
  match decode ctx mode endian bytes with
  | Ok instr -> instr
  | Error err -> raise (Arm_decode_error err)
