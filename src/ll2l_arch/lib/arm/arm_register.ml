open Base

module Kind = struct
  type t =
    | GP
    | FP_single
    | FP_double
    | FP_vector
    | CopC
    | CopP
    | Special
    | Banked
  [@@deriving equal]
end

type t =
  | R0
  | R1
  | R2
  | R3
  | R4
  | R5
  | R6
  | R7
  | R8
  | R9
  | R10
  | R11
  | R12
  | SP
  | LR
  | PC
  | S0
  | S1
  | S2
  | S3
  | S4
  | S5
  | S6
  | S7
  | S8
  | S9
  | S10
  | S11
  | S12
  | S13
  | S14
  | S15
  | S16
  | S17
  | S18
  | S19
  | S20
  | S21
  | S22
  | S23
  | S24
  | S25
  | S26
  | S27
  | S28
  | S29
  | S30
  | S31
  | D0
  | D1
  | D2
  | D3
  | D4
  | D5
  | D6
  | D7
  | D8
  | D9
  | D10
  | D11
  | D12
  | D13
  | D14
  | D15
  | D16
  | D17
  | D18
  | D19
  | D20
  | D21
  | D22
  | D23
  | D24
  | D25
  | D26
  | D27
  | D28
  | D29
  | D30
  | D31
  | Q0
  | Q1
  | Q2
  | Q3
  | Q4
  | Q5
  | Q6
  | Q7
  | Q8
  | Q9
  | Q10
  | Q11
  | Q12
  | Q13
  | Q14
  | Q15
  | C0
  | C1
  | C2
  | C3
  | C4
  | C5
  | C6
  | C7
  | C8
  | C9
  | C10
  | C11
  | C12
  | C13
  | C14
  | C15
  | P0
  | P1
  | P2
  | P3
  | P4
  | P5
  | P6
  | P7
  | P8
  | P9
  | P10
  | P11
  | P12
  | P13
  | P14
  | P15
  | APSR
  | CPSR
  | SPSR
  | SCR
  | SCTLR
  | NSACR
  | FPSCR
  | R8USR
  | R9USR
  | R10USR
  | R11USR
  | R12USR
  | SPUSR
  | LRUSR
  | R8FIQ
  | R9FIQ
  | R10FIQ
  | R11FIQ
  | R12FIQ
  | SPFIQ
  | LRFIQ
  | LRIRQ
  | SPIRQ
  | LRSVC
  | SPSVC
  | LRABT
  | SPABT
  | LRUND
  | SPUND
  | LRMON
  | SPMON
  | ELRHYP
  | SPHYP
  | SPSRFIQ
  | SPSRIRQ
  | SPSRSVC
  | SPSRABT
  | SPSRUND
  | SPSRMON
  | SPSRHYP
[@@deriving equal]

let gp_reg_of_int = function
  | 0 -> Some R0
  | 1 -> Some R1
  | 2 -> Some R2
  | 3 -> Some R3
  | 4 -> Some R4
  | 5 -> Some R5
  | 6 -> Some R6
  | 7 -> Some R7
  | 8 -> Some R8
  | 9 -> Some R9
  | 10 -> Some R10
  | 11 -> Some R11
  | 12 -> Some R12
  | 13 -> Some SP
  | 14 -> Some LR
  | 15 -> Some PC
  | _ -> None

let int_of_gp_reg = function
  | R0 -> Some 0
  | R1 -> Some 1
  | R2 -> Some 2
  | R3 -> Some 3
  | R4 -> Some 4
  | R5 -> Some 5
  | R6 -> Some 6
  | R7 -> Some 7
  | R8 -> Some 8
  | R9 -> Some 9
  | R10 -> Some 10
  | R11 -> Some 11
  | R12 -> Some 12
  | SP -> Some 13
  | LR -> Some 14
  | PC -> Some 15
  | _ -> None

let fp_single_reg_of_int = function
  | 0 -> Some S0
  | 1 -> Some S1
  | 2 -> Some S2
  | 3 -> Some S3
  | 4 -> Some S4
  | 5 -> Some S5
  | 6 -> Some S6
  | 7 -> Some S7
  | 8 -> Some S8
  | 9 -> Some S9
  | 10 -> Some S10
  | 11 -> Some S11
  | 12 -> Some S12
  | 13 -> Some S13
  | 14 -> Some S14
  | 15 -> Some S15
  | 16 -> Some S16
  | 17 -> Some S17
  | 18 -> Some S18
  | 19 -> Some S19
  | 20 -> Some S20
  | 21 -> Some S21
  | 22 -> Some S22
  | 23 -> Some S23
  | 24 -> Some S24
  | 25 -> Some S25
  | 26 -> Some S26
  | 27 -> Some S27
  | 28 -> Some S28
  | 29 -> Some S29
  | 30 -> Some S30
  | 31 -> Some S31
  | _ -> None

let int_of_fp_single_reg = function
  | S0 -> Some 0
  | S1 -> Some 1
  | S2 -> Some 2
  | S3 -> Some 3
  | S4 -> Some 4
  | S5 -> Some 5
  | S6 -> Some 6
  | S7 -> Some 7
  | S8 -> Some 8
  | S9 -> Some 9
  | S10 -> Some 10
  | S11 -> Some 11
  | S12 -> Some 12
  | S13 -> Some 13
  | S14 -> Some 14
  | S15 -> Some 15
  | S16 -> Some 16
  | S17 -> Some 17
  | S18 -> Some 18
  | S19 -> Some 19
  | S20 -> Some 20
  | S21 -> Some 21
  | S22 -> Some 22
  | S23 -> Some 23
  | S24 -> Some 24
  | S25 -> Some 25
  | S26 -> Some 26
  | S27 -> Some 27
  | S28 -> Some 28
  | S29 -> Some 29
  | S30 -> Some 30
  | S31 -> Some 31
  | _ -> None

let fp_double_reg_of_int = function
  | 0 -> Some D0
  | 1 -> Some D1
  | 2 -> Some D2
  | 3 -> Some D3
  | 4 -> Some D4
  | 5 -> Some D5
  | 6 -> Some D6
  | 7 -> Some D7
  | 8 -> Some D8
  | 9 -> Some D9
  | 10 -> Some D10
  | 11 -> Some D11
  | 12 -> Some D12
  | 13 -> Some D13
  | 14 -> Some D14
  | 15 -> Some D15
  | 16 -> Some D16
  | 17 -> Some D17
  | 18 -> Some D18
  | 19 -> Some D19
  | 20 -> Some D20
  | 21 -> Some D21
  | 22 -> Some D22
  | 23 -> Some D23
  | 24 -> Some D24
  | 25 -> Some D25
  | 26 -> Some D26
  | 27 -> Some D27
  | 28 -> Some D28
  | 29 -> Some D29
  | 30 -> Some D30
  | 31 -> Some D31
  | _ -> None

let int_of_fp_double_reg = function
  | D0 -> Some 0
  | D1 -> Some 1
  | D2 -> Some 2
  | D3 -> Some 3
  | D4 -> Some 4
  | D5 -> Some 5
  | D6 -> Some 6
  | D7 -> Some 7
  | D8 -> Some 8
  | D9 -> Some 9
  | D10 -> Some 10
  | D11 -> Some 11
  | D12 -> Some 12
  | D13 -> Some 13
  | D14 -> Some 14
  | D15 -> Some 15
  | D16 -> Some 16
  | D17 -> Some 17
  | D18 -> Some 18
  | D19 -> Some 19
  | D20 -> Some 20
  | D21 -> Some 21
  | D22 -> Some 22
  | D23 -> Some 23
  | D24 -> Some 24
  | D25 -> Some 25
  | D26 -> Some 26
  | D27 -> Some 27
  | D28 -> Some 28
  | D29 -> Some 29
  | D30 -> Some 30
  | D31 -> Some 31
  | _ -> None

let fp_vector_reg_of_int = function
  | 0 -> Some Q0
  | 1 -> Some Q1
  | 2 -> Some Q2
  | 3 -> Some Q3
  | 4 -> Some Q4
  | 5 -> Some Q5
  | 6 -> Some Q6
  | 7 -> Some Q7
  | 8 -> Some Q8
  | 9 -> Some Q9
  | 10 -> Some Q10
  | 11 -> Some Q11
  | 12 -> Some Q12
  | 13 -> Some Q13
  | 14 -> Some Q14
  | 15 -> Some Q15
  | _ -> None

let int_of_fp_vector_reg = function
  | Q0 -> Some 0
  | Q1 -> Some 1
  | Q2 -> Some 2
  | Q3 -> Some 3
  | Q4 -> Some 4
  | Q5 -> Some 5
  | Q6 -> Some 6
  | Q7 -> Some 7
  | Q8 -> Some 8
  | Q9 -> Some 9
  | Q10 -> Some 10
  | Q11 -> Some 11
  | Q12 -> Some 12
  | Q13 -> Some 13
  | Q14 -> Some 14
  | Q15 -> Some 15
  | _ -> None

let copc_reg_of_int = function
  | 0 -> Some C0
  | 1 -> Some C1
  | 2 -> Some C2
  | 3 -> Some C3
  | 4 -> Some C4
  | 5 -> Some C5
  | 6 -> Some C6
  | 7 -> Some C7
  | 8 -> Some C8
  | 9 -> Some C9
  | 10 -> Some C10
  | 11 -> Some C11
  | 12 -> Some C12
  | 13 -> Some C13
  | 14 -> Some C14
  | 15 -> Some C15
  | _ -> None

let int_of_copc_reg = function
  | C0 -> Some 0
  | C1 -> Some 1
  | C2 -> Some 2
  | C3 -> Some 3
  | C4 -> Some 4
  | C5 -> Some 5
  | C6 -> Some 6
  | C7 -> Some 7
  | C8 -> Some 8
  | C9 -> Some 9
  | C10 -> Some 10
  | C11 -> Some 11
  | C12 -> Some 12
  | C13 -> Some 13
  | C14 -> Some 14
  | C15 -> Some 15
  | _ -> None

let copp_reg_of_int = function
  | 0 -> Some P0
  | 1 -> Some P1
  | 2 -> Some P2
  | 3 -> Some P3
  | 4 -> Some P4
  | 5 -> Some P5
  | 6 -> Some P6
  | 7 -> Some P7
  | 8 -> Some P8
  | 9 -> Some P9
  | 10 -> Some P10
  | 11 -> Some P11
  | 12 -> Some P12
  | 13 -> Some P13
  | 14 -> Some P14
  | 15 -> Some P15
  | _ -> None

let int_of_copp_reg = function
  | P0 -> Some 0
  | P1 -> Some 1
  | P2 -> Some 2
  | P3 -> Some 3
  | P4 -> Some 4
  | P5 -> Some 5
  | P6 -> Some 6
  | P7 -> Some 7
  | P8 -> Some 8
  | P9 -> Some 9
  | P10 -> Some 10
  | P11 -> Some 11
  | P12 -> Some 12
  | P13 -> Some 13
  | P14 -> Some 14
  | P15 -> Some 15
  | _ -> None

let to_string ?(write_back = false) r =
  let s =
    match r with
    | R0 -> "R0"
    | R1 -> "R1"
    | R2 -> "R2"
    | R3 -> "R3"
    | R4 -> "R4"
    | R5 -> "R5"
    | R6 -> "R6"
    | R7 -> "R7"
    | R8 -> "R8"
    | R9 -> "R9"
    | R10 -> "R10"
    | R11 -> "R11"
    | R12 -> "R12"
    | SP -> "SP"
    | LR -> "LR"
    | PC -> "PC"
    | S0 -> "S0"
    | S1 -> "S1"
    | S2 -> "S2"
    | S3 -> "S3"
    | S4 -> "S4"
    | S5 -> "S5"
    | S6 -> "S6"
    | S7 -> "S7"
    | S8 -> "S8"
    | S9 -> "S9"
    | S10 -> "S10"
    | S11 -> "S11"
    | S12 -> "S12"
    | S13 -> "S13"
    | S14 -> "S14"
    | S15 -> "S15"
    | S16 -> "S16"
    | S17 -> "S17"
    | S18 -> "S18"
    | S19 -> "S19"
    | S20 -> "S20"
    | S21 -> "S21"
    | S22 -> "S22"
    | S23 -> "S23"
    | S24 -> "S24"
    | S25 -> "S25"
    | S26 -> "S26"
    | S27 -> "S27"
    | S28 -> "S28"
    | S29 -> "S29"
    | S30 -> "S30"
    | S31 -> "S31"
    | D0 -> "D0"
    | D1 -> "D1"
    | D2 -> "D2"
    | D3 -> "D3"
    | D4 -> "D4"
    | D5 -> "D5"
    | D6 -> "D6"
    | D7 -> "D7"
    | D8 -> "D8"
    | D9 -> "D9"
    | D10 -> "D10"
    | D11 -> "D11"
    | D12 -> "D12"
    | D13 -> "D13"
    | D14 -> "D14"
    | D15 -> "D15"
    | D16 -> "D16"
    | D17 -> "D17"
    | D18 -> "D18"
    | D19 -> "D19"
    | D20 -> "D20"
    | D21 -> "D21"
    | D22 -> "D22"
    | D23 -> "D23"
    | D24 -> "D24"
    | D25 -> "D25"
    | D26 -> "D26"
    | D27 -> "D27"
    | D28 -> "D28"
    | D29 -> "D29"
    | D30 -> "D30"
    | D31 -> "D31"
    | Q0 -> "Q0"
    | Q1 -> "Q1"
    | Q2 -> "Q2"
    | Q3 -> "Q3"
    | Q4 -> "Q4"
    | Q5 -> "Q5"
    | Q6 -> "Q6"
    | Q7 -> "Q7"
    | Q8 -> "Q8"
    | Q9 -> "Q9"
    | Q10 -> "Q10"
    | Q11 -> "Q11"
    | Q12 -> "Q12"
    | Q13 -> "Q13"
    | Q14 -> "Q14"
    | Q15 -> "Q15"
    | C0 -> "C0"
    | C1 -> "C1"
    | C2 -> "C2"
    | C3 -> "C3"
    | C4 -> "C4"
    | C5 -> "C5"
    | C6 -> "C6"
    | C7 -> "C7"
    | C8 -> "C8"
    | C9 -> "C9"
    | C10 -> "C10"
    | C11 -> "C11"
    | C12 -> "C12"
    | C13 -> "C13"
    | C14 -> "C14"
    | C15 -> "C15"
    | P0 -> "P0"
    | P1 -> "P1"
    | P2 -> "P2"
    | P3 -> "P3"
    | P4 -> "P4"
    | P5 -> "P5"
    | P6 -> "P6"
    | P7 -> "P7"
    | P8 -> "P8"
    | P9 -> "P9"
    | P10 -> "P10"
    | P11 -> "P11"
    | P12 -> "P12"
    | P13 -> "P13"
    | P14 -> "P14"
    | P15 -> "P15"
    | APSR -> "APSR"
    | CPSR -> "CPSR"
    | SPSR -> "SPSR"
    | SCR -> "SCR"
    | SCTLR -> "SCTLR"
    | NSACR -> "NSACR"
    | FPSCR -> "FPSCR"
    | R8USR -> "R8USR"
    | R9USR -> "R9USR"
    | R10USR -> "R10USR"
    | R11USR -> "R11USR"
    | R12USR -> "R12USR"
    | SPUSR -> "SPUSR"
    | LRUSR -> "LRUSR"
    | R8FIQ -> "R8FIQ"
    | R9FIQ -> "R9FIQ"
    | R10FIQ -> "R10FIQ"
    | R11FIQ -> "R11FIQ"
    | R12FIQ -> "R12FIQ"
    | SPFIQ -> "SPFIQ"
    | LRFIQ -> "LRFIQ"
    | LRIRQ -> "LRIRQ"
    | SPIRQ -> "SPIRQ"
    | LRSVC -> "LRSVC"
    | SPSVC -> "SPSVC"
    | LRABT -> "LRABT"
    | SPABT -> "SPABT"
    | LRUND -> "LRUND"
    | SPUND -> "SPUND"
    | LRMON -> "LRMON"
    | SPMON -> "SPMON"
    | ELRHYP -> "ELRHYP"
    | SPHYP -> "SPHYP"
    | SPSRFIQ -> "SPSRFIQ"
    | SPSRIRQ -> "SPSRIRQ"
    | SPSRSVC -> "SPSRSVC"
    | SPSRABT -> "SPSRABT"
    | SPSRUND -> "SPSRUND"
    | SPSRMON -> "SPSRMON"
    | SPSRHYP -> "SPSRHYP"
  in
  if write_back then s ^ "!" else s

let of_string = function
  | "R0" -> Some R0
  | "R1" -> Some R1
  | "R2" -> Some R2
  | "R3" -> Some R3
  | "R4" -> Some R4
  | "R5" -> Some R5
  | "R6" -> Some R6
  | "R7" -> Some R7
  | "R8" -> Some R8
  | "R9" -> Some R9
  | "R10" -> Some R10
  | "R11" -> Some R11
  | "R12" -> Some R12
  | "SP" -> Some SP
  | "LR" -> Some LR
  | "PC" -> Some PC
  | "S0" -> Some S0
  | "S1" -> Some S1
  | "S2" -> Some S2
  | "S3" -> Some S3
  | "S4" -> Some S4
  | "S5" -> Some S5
  | "S6" -> Some S6
  | "S7" -> Some S7
  | "S8" -> Some S8
  | "S9" -> Some S9
  | "S10" -> Some S10
  | "S11" -> Some S11
  | "S12" -> Some S12
  | "S13" -> Some S13
  | "S14" -> Some S14
  | "S15" -> Some S15
  | "S16" -> Some S16
  | "S17" -> Some S17
  | "S18" -> Some S18
  | "S19" -> Some S19
  | "S20" -> Some S20
  | "S21" -> Some S21
  | "S22" -> Some S22
  | "S23" -> Some S23
  | "S24" -> Some S24
  | "S25" -> Some S25
  | "S26" -> Some S26
  | "S27" -> Some S27
  | "S28" -> Some S28
  | "S29" -> Some S29
  | "S30" -> Some S30
  | "S31" -> Some S31
  | "D0" -> Some D0
  | "D1" -> Some D1
  | "D2" -> Some D2
  | "D3" -> Some D3
  | "D4" -> Some D4
  | "D5" -> Some D5
  | "D6" -> Some D6
  | "D7" -> Some D7
  | "D8" -> Some D8
  | "D9" -> Some D9
  | "D10" -> Some D10
  | "D11" -> Some D11
  | "D12" -> Some D12
  | "D13" -> Some D13
  | "D14" -> Some D14
  | "D15" -> Some D15
  | "D16" -> Some D16
  | "D17" -> Some D17
  | "D18" -> Some D18
  | "D19" -> Some D19
  | "D20" -> Some D20
  | "D21" -> Some D21
  | "D22" -> Some D22
  | "D23" -> Some D23
  | "D24" -> Some D24
  | "D25" -> Some D25
  | "D26" -> Some D26
  | "D27" -> Some D27
  | "D28" -> Some D28
  | "D29" -> Some D29
  | "D30" -> Some D30
  | "D31" -> Some D31
  | "Q0" -> Some Q0
  | "Q1" -> Some Q1
  | "Q2" -> Some Q2
  | "Q3" -> Some Q3
  | "Q4" -> Some Q4
  | "Q5" -> Some Q5
  | "Q6" -> Some Q6
  | "Q7" -> Some Q7
  | "Q8" -> Some Q8
  | "Q9" -> Some Q9
  | "Q10" -> Some Q10
  | "Q11" -> Some Q11
  | "Q12" -> Some Q12
  | "Q13" -> Some Q13
  | "Q14" -> Some Q14
  | "Q15" -> Some Q15
  | "C0" -> Some C0
  | "C1" -> Some C1
  | "C2" -> Some C2
  | "C3" -> Some C3
  | "C4" -> Some C4
  | "C5" -> Some C5
  | "C6" -> Some C6
  | "C7" -> Some C7
  | "C8" -> Some C8
  | "C9" -> Some C9
  | "C10" -> Some C10
  | "C11" -> Some C11
  | "C12" -> Some C12
  | "C13" -> Some C13
  | "C14" -> Some C14
  | "C15" -> Some C15
  | "P0" -> Some P0
  | "P1" -> Some P1
  | "P2" -> Some P2
  | "P3" -> Some P3
  | "P4" -> Some P4
  | "P5" -> Some P5
  | "P6" -> Some P6
  | "P7" -> Some P7
  | "P8" -> Some P8
  | "P9" -> Some P9
  | "P10" -> Some P10
  | "P11" -> Some P11
  | "P12" -> Some P12
  | "P13" -> Some P13
  | "P14" -> Some P14
  | "P15" -> Some P15
  | "APSR" -> Some APSR
  | "CPSR" -> Some CPSR
  | "SPSR" -> Some SPSR
  | "SCR" -> Some SCR
  | "SCTLR" -> Some SCTLR
  | "NSACR" -> Some NSACR
  | "FPSCR" -> Some FPSCR
  | "R8USR" -> Some R8USR
  | "R9USR" -> Some R9USR
  | "R10USR" -> Some R10USR
  | "R11USR" -> Some R11USR
  | "R12USR" -> Some R12USR
  | "SPUSR" -> Some SPUSR
  | "LRUSR" -> Some LRUSR
  | "R8FIQ" -> Some R8FIQ
  | "R9FIQ" -> Some R9FIQ
  | "R10FIQ" -> Some R10FIQ
  | "R11FIQ" -> Some R11FIQ
  | "R12FIQ" -> Some R12FIQ
  | "SPFIQ" -> Some SPFIQ
  | "LRFIQ" -> Some LRFIQ
  | "LRIRQ" -> Some LRIRQ
  | "SPIRQ" -> Some SPIRQ
  | "LRSVC" -> Some LRSVC
  | "SPSVC" -> Some SPSVC
  | "LRABT" -> Some LRABT
  | "SPABT" -> Some SPABT
  | "LRUND" -> Some LRUND
  | "SPUND" -> Some SPUND
  | "LRMON" -> Some LRMON
  | "SPMON" -> Some SPMON
  | "ELRHYP" -> Some ELRHYP
  | "SPHYP" -> Some SPHYP
  | "SPSRFIQ" -> Some SPSRFIQ
  | "SPSRIRQ" -> Some SPSRIRQ
  | "SPSRSVC" -> Some SPSRSVC
  | "SPSRABT" -> Some SPSRABT
  | "SPSRUND" -> Some SPSRUND
  | "SPSRMON" -> Some SPSRMON
  | "SPSRHYP" -> Some SPSRHYP
  | _ -> None

let kind_of = function
  | R0
   |R1
   |R2
   |R3
   |R4
   |R5
   |R6
   |R7
   |R8
   |R9
   |R10
   |R11
   |R12
   |LR
   |PC
   |SP -> Kind.GP
  | S0
   |S1
   |S2
   |S3
   |S4
   |S5
   |S6
   |S7
   |S8
   |S9
   |S10
   |S11
   |S12
   |S13
   |S14
   |S15
   |S16
   |S17
   |S18
   |S19
   |S20
   |S21
   |S22
   |S23
   |S24
   |S25
   |S26
   |S27
   |S28
   |S29
   |S30
   |S31 -> Kind.FP_single
  | D0
   |D1
   |D2
   |D3
   |D4
   |D5
   |D6
   |D7
   |D8
   |D9
   |D10
   |D11
   |D12
   |D13
   |D14
   |D15
   |D16
   |D17
   |D18
   |D19
   |D20
   |D21
   |D22
   |D23
   |D24
   |D25
   |D26
   |D27
   |D28
   |D29
   |D30
   |D31 -> Kind.FP_double
  | Q0
   |Q1
   |Q2
   |Q3
   |Q4
   |Q5
   |Q6
   |Q7
   |Q8
   |Q9
   |Q10
   |Q11
   |Q12
   |Q13
   |Q14
   |Q15 -> Kind.FP_vector
  | C0
   |C1
   |C2
   |C3
   |C4
   |C5
   |C6
   |C7
   |C8
   |C9
   |C10
   |C11
   |C12
   |C13
   |C14
   |C15 -> Kind.CopC
  | P0
   |P1
   |P2
   |P3
   |P4
   |P5
   |P6
   |P7
   |P8
   |P9
   |P10
   |P11
   |P12
   |P13
   |P14
   |P15 -> Kind.CopP
  | APSR | CPSR | SPSR | SCR | SCTLR | NSACR | FPSCR -> Kind.Special
  | R8USR
   |R9USR
   |R10USR
   |R11USR
   |R12USR
   |SPUSR
   |LRUSR
   |R8FIQ
   |R9FIQ
   |R10FIQ
   |R11FIQ
   |R12FIQ
   |SPFIQ
   |LRFIQ
   |LRIRQ
   |SPIRQ
   |LRSVC
   |SPSVC
   |LRABT
   |SPABT
   |LRUND
   |SPUND
   |LRMON
   |SPMON
   |ELRHYP
   |SPHYP
   |SPSRFIQ
   |SPSRIRQ
   |SPSRSVC
   |SPSRABT
   |SPSRUND
   |SPSRMON
   |SPSRHYP -> Kind.Banked

let width_of r =
  match kind_of r with
  | Kind.GP -> Some 32
  | Kind.FP_single -> Some 32
  | Kind.FP_double -> Some 64
  | Kind.FP_vector -> Some 128
  | Kind.CopC -> None
  | Kind.CopP -> None
  | Kind.Special -> Some 32
  | Kind.Banked -> None
