open Base

type t =
  | ADC
  | ADCS
  | ADD
  | ADDS
  | ADDW
  | ADR
  | AESD
  | AESE
  | AESIMC
  | AESMC
  | AND
  | ANDS
  | ASR
  | ASRS
  | B
  | BFC
  | BFI
  | BIC
  | BICS
  | BKPT
  | BL
  | BLX
  | BX
  | BXJ
  | CBNZ
  | CBZ
  | CDP
  | CDP2
  | CLREX
  | CLZ
  | CMN
  | CMP
  | CPS
  | CPSID
  | CPSIE
  | CRC32B
  | CRC32CB
  | CRC32CH
  | CRC32CW
  | CRC32H
  | CRC32W
  | DBG
  | DMB
  | DSB
  | ENTERX
  | EOR
  | EORS
  | ERET
  | FSTMDBX
  | FSTMIAX
  | HLT
  | HVC
  | ISB
  | IT
  | ITE
  | ITEE
  | ITEEE
  | ITEET
  | ITET
  | ITETE
  | ITETT
  | ITT
  | ITTE
  | ITTEE
  | ITTET
  | ITTT
  | ITTTE
  | ITTTT
  | LDA
  | LDAB
  | LDAEX
  | LDAEXB
  | LDAEXD
  | LDAEXH
  | LDAH
  | LDC
  | LDC2
  | LDC2L
  | LDCL
  | LDM
  | LDMDA
  | LDMDB
  | LDMIA
  | LDMIB
  | LDR
  | LDRB
  | LDRBT
  | LDRD
  | LDREX
  | LDREXB
  | LDREXD
  | LDREXH
  | LDRH
  | LDRHT
  | LDRSB
  | LDRSBT
  | LDRSH
  | LDRSHT
  | LDRT
  | LEAVEX
  | LSL
  | LSLS
  | LSR
  | LSRS
  | MCR
  | MCR2
  | MCRR
  | MCRR2
  | MLA
  | MLAS
  | MLS
  | MOV
  | MOVS
  | MOVT
  | MOVW
  | MRC
  | MRC2
  | MRRC
  | MRRC2
  | MRS
  | MSR
  | MUL
  | MULS
  | MVN
  | MVNS
  | NOP
  | ORN
  | ORNS
  | ORR
  | ORRS
  | PKHBT
  | PKHTB
  | PLD
  | PLDW
  | PLI
  | POP
  | PUSH
  | QADD
  | QADD16
  | QADD8
  | QASX
  | QDADD
  | QDSUB
  | QSAX
  | QSUB
  | QSUB16
  | QSUB8
  | RBIT
  | REV
  | REV16
  | REVSH
  | RFEDA
  | RFEDB
  | RFEIA
  | RFEIB
  | ROR
  | RORS
  | RRX
  | RRXS
  | RSB
  | RSBS
  | RSC
  | RSCS
  | SADD16
  | SADD8
  | SASX
  | SBC
  | SBCS
  | SBFX
  | SDIV
  | SEL
  | SETEND
  | SEV
  | SEVL
  | SHA1H
  | SHA1SU1
  | SHA256SU0
  | SHADD16
  | SHADD8
  | SHASX
  | SHSAX
  | SHSUB16
  | SHSUB8
  | SMC
  | SMLABB
  | SMLABT
  | SMLAD
  | SMLADX
  | SMLAL
  | SMLALBB
  | SMLALBT
  | SMLALD
  | SMLALDX
  | SMLALS
  | SMLALTB
  | SMLALTT
  | SMLATB
  | SMLATT
  | SMLAWB
  | SMLAWT
  | SMLSD
  | SMLSDX
  | SMLSLD
  | SMLSLDX
  | SMMLA
  | SMMLAR
  | SMMLS
  | SMMLSR
  | SMMUL
  | SMMULR
  | SMUAD
  | SMUADX
  | SMULBB
  | SMULBT
  | SMULL
  | SMULLS
  | SMULTB
  | SMULTT
  | SMULWB
  | SMULWT
  | SMUSD
  | SMUSDX
  | SRS
  | SRSDA
  | SRSDB
  | SRSIA
  | SRSIB
  | SSAT
  | SSAT16
  | SSAX
  | SSUB16
  | SSUB8
  | STC
  | STC2
  | STC2L
  | STCL
  | STL
  | STLB
  | STLEX
  | STLEXB
  | STLEXD
  | STLEXH
  | STLH
  | STM
  | STMDA
  | STMDB
  | STMEA
  | STMIA
  | STMIB
  | STR
  | STRB
  | STRBT
  | STRD
  | STREX
  | STREXB
  | STREXD
  | STREXH
  | STRH
  | STRHT
  | STRT
  | SUB
  | SUBS
  | SUBW
  | SVC
  | SWP
  | SWPB
  | SXTAB
  | SXTAB16
  | SXTAH
  | SXTB
  | SXTB16
  | SXTH
  | TBB
  | TBH
  | TEQ
  | TST
  | UADD16
  | UADD8
  | UASX
  | UBFX
  | UDF
  | UDIV
  | UHADD16
  | UHADD8
  | UHASX
  | UHSAX
  | UHSUB16
  | UHSUB8
  | UMAAL
  | UMLAL
  | UMLALS
  | UMULL
  | UMULLS
  | UQADD16
  | UQADD8
  | UQASX
  | UQSAX
  | UQSUB16
  | UQSUB8
  | USAD8
  | USADA8
  | USAT
  | USAT16
  | USAX
  | USUB16
  | USUB8
  | UXTAB
  | UXTAB16
  | UXTAH
  | UXTB
  | UXTB16
  | UXTH
  | VABA
  | VABAL
  | VABD
  | VABDL
  | VABS
  | VACGE
  | VACGT
  | VACLE
  | VACLT
  | VADD
  | VADDHN
  | VADDL
  | VADDW
  | VAND
  | VBIC
  | VBIF
  | VBIT
  | VBSL
  | VCEQ
  | VCGE
  | VCGT
  | VCLE
  | VCLS
  | VCLT
  | VCLZ
  | VCMP
  | VCMPE
  | VCNT
  | VCVT
  | VCVTA
  | VCVTB
  | VCVTM
  | VCVTN
  | VCVTP
  | VCVTR
  | VCVTT
  | VDIV
  | VDUP
  | VEOR
  | VEXT
  | VFMA
  | VFMS
  | VFNMA
  | VFNMS
  | VHADD
  | VHSUB
  | VLD1
  | VLD2
  | VLD3
  | VLD4
  | VLDM
  | VLDMDB
  | VLDMIA
  | VLDR
  | VMAX
  | VMIN
  | VMLA
  | VMLAL
  | VMLS
  | VMLSL
  | VMOV
  | VMOVL
  | VMOVN
  | VMRS
  | VMSR
  | VMUL
  | VMULL
  | VMVN
  | VNEG
  | VNMLA
  | VNMLS
  | VNMUL
  | VORN
  | VORR
  | VPADAL
  | VPADD
  | VPADDL
  | VPMAX
  | VPMIN
  | VPOP
  | VPUSH
  | VQABS
  | VQADD
  | VQDMLAL
  | VQDMLSL
  | VQDMULH
  | VQDMULL
  | VQMOVN
  | VQMOVUN
  | VQNEG
  | VQRDMULH
  | VQRSHL
  | VQRSHRN
  | VQRSHRUN
  | VQSHL
  | VQSHLU
  | VQSHRN
  | VQSHRUN
  | VQSUB
  | VRADDHN
  | VRECPE
  | VRECPS
  | VREV16
  | VREV32
  | VREV64
  | VRHADD
  | VRINTA
  | VRINTM
  | VRINTN
  | VRINTP
  | VRINTR
  | VRINTX
  | VRINTZ
  | VRSHL
  | VRSHR
  | VRSHRN
  | VRSQRTE
  | VRSQRTS
  | VRSRA
  | VRSUBHN
  | VSEL
  | VSHL
  | VSHLL
  | VSHR
  | VSHRN
  | VSLI
  | VSQRT
  | VSRA
  | VSRI
  | VST1
  | VST2
  | VST3
  | VST4
  | VSTM
  | VSTMDB
  | VSTMIA
  | VSTR
  | VSUB
  | VSUBHN
  | VSUBL
  | VSUBW
  | VSWP
  | VTBL
  | VTBX
  | VTRN
  | VTST
  | VUZP
  | VZIP
  | WFE
  | WFI
  | YIELD
[@@deriving equal]

let is_branch = function
  | B | CBNZ | CBZ | BL | BLX | BX | BXJ | TBB | TBH -> true
  | _ -> false

let is_link_branch = function
  | BL | BLX -> true
  | _ -> false

(* ignore the 'cond' field of the instruction *)
let is_unconditional_branch = function
  | B | BL | BLX | BX | BXJ | TBB | TBH -> true
  | _ -> false

let is_conditional_branch = function
  | CBNZ | CBZ -> true
  | _ -> false

let is_it = function
  | IT
   |ITE
   |ITET
   |ITTE
   |ITEE
   |ITETT
   |ITTET
   |ITEET
   |ITTTE
   |ITETE
   |ITTEE
   |ITEEE -> true
  | _ -> false

let is_it_multiple = function
  | ITE
   |ITET
   |ITTE
   |ITEE
   |ITETT
   |ITTET
   |ITEET
   |ITTTE
   |ITETE
   |ITTEE
   |ITEEE -> true
  | _ -> false

let to_string = function
  | ADC -> "ADC"
  | ADCS -> "ADCS"
  | ADD -> "ADD"
  | ADDS -> "ADDS"
  | ADDW -> "ADDW"
  | ADR -> "ADR"
  | AESD -> "AESD"
  | AESE -> "AESE"
  | AESIMC -> "AESIMC"
  | AESMC -> "AESMC"
  | AND -> "AND"
  | ANDS -> "ANDS"
  | ASR -> "ASR"
  | ASRS -> "ASRS"
  | B -> "B"
  | BFC -> "BFC"
  | BFI -> "BFI"
  | BIC -> "BIC"
  | BICS -> "BICS"
  | BKPT -> "BKPT"
  | BL -> "BL"
  | BLX -> "BLX"
  | BX -> "BX"
  | BXJ -> "BXJ"
  | CBNZ -> "CBNZ"
  | CBZ -> "CBZ"
  | CDP -> "CDP"
  | CDP2 -> "CDP2"
  | CLREX -> "CLREX"
  | CLZ -> "CLZ"
  | CMN -> "CMN"
  | CMP -> "CMP"
  | CPS -> "CPS"
  | CPSID -> "CPSID"
  | CPSIE -> "CPSIE"
  | CRC32B -> "CRC32B"
  | CRC32CB -> "CRC32CB"
  | CRC32CH -> "CRC32CH"
  | CRC32CW -> "CRC32CW"
  | CRC32H -> "CRC32H"
  | CRC32W -> "CRC32W"
  | DBG -> "DBG"
  | DMB -> "DMB"
  | DSB -> "DSB"
  | ENTERX -> "ENTERX"
  | EOR -> "EOR"
  | EORS -> "EORS"
  | ERET -> "ERET"
  | FSTMDBX -> "FSTMDBX"
  | FSTMIAX -> "FSTMIAX"
  | HLT -> "HLT"
  | HVC -> "HVC"
  | ISB -> "ISB"
  | IT -> "IT"
  | ITE -> "ITE"
  | ITEE -> "ITEE"
  | ITEEE -> "ITEEE"
  | ITEET -> "ITEET"
  | ITET -> "ITET"
  | ITETE -> "ITETE"
  | ITETT -> "ITETT"
  | ITT -> "ITT"
  | ITTE -> "ITTE"
  | ITTEE -> "ITTEE"
  | ITTET -> "ITTET"
  | ITTT -> "ITTT"
  | ITTTE -> "ITTTE"
  | ITTTT -> "ITTTT"
  | LDA -> "LDA"
  | LDAB -> "LDAB"
  | LDAEX -> "LDAEX"
  | LDAEXB -> "LDAEXB"
  | LDAEXD -> "LDAEXD"
  | LDAEXH -> "LDAEXH"
  | LDAH -> "LDAH"
  | LDC -> "LDC"
  | LDC2 -> "LDC2"
  | LDC2L -> "LDC2L"
  | LDCL -> "LDCL"
  | LDM -> "LDM"
  | LDMDA -> "LDMDA"
  | LDMDB -> "LDMDB"
  | LDMIA -> "LDMIA"
  | LDMIB -> "LDMIB"
  | LDR -> "LDR"
  | LDRB -> "LDRB"
  | LDRBT -> "LDRBT"
  | LDRD -> "LDRD"
  | LDREX -> "LDREX"
  | LDREXB -> "LDREXB"
  | LDREXD -> "LDREXD"
  | LDREXH -> "LDREXH"
  | LDRH -> "LDRH"
  | LDRHT -> "LDRHT"
  | LDRSB -> "LDRSB"
  | LDRSBT -> "LDRSBT"
  | LDRSH -> "LDRSH"
  | LDRSHT -> "LDRSHT"
  | LDRT -> "LDRT"
  | LEAVEX -> "LEAVEX"
  | LSL -> "LSL"
  | LSLS -> "LSLS"
  | LSR -> "LSR"
  | LSRS -> "LSRS"
  | MCR -> "MCR"
  | MCR2 -> "MCR2"
  | MCRR -> "MCRR"
  | MCRR2 -> "MCRR2"
  | MLA -> "MLA"
  | MLAS -> "MLAS"
  | MLS -> "MLS"
  | MOV -> "MOV"
  | MOVS -> "MOVS"
  | MOVT -> "MOVT"
  | MOVW -> "MOVW"
  | MRC -> "MRC"
  | MRC2 -> "MRC2"
  | MRRC -> "MRRC"
  | MRRC2 -> "MRRC2"
  | MRS -> "MRS"
  | MSR -> "MSR"
  | MUL -> "MUL"
  | MULS -> "MULS"
  | MVN -> "MVN"
  | MVNS -> "MVNS"
  | NOP -> "NOP"
  | ORN -> "ORN"
  | ORNS -> "ORNS"
  | ORR -> "ORR"
  | ORRS -> "ORRS"
  | PKHBT -> "PKHBT"
  | PKHTB -> "PKHTB"
  | PLD -> "PLD"
  | PLDW -> "PLDW"
  | PLI -> "PLI"
  | POP -> "POP"
  | PUSH -> "PUSH"
  | QADD -> "QADD"
  | QADD16 -> "QADD16"
  | QADD8 -> "QADD8"
  | QASX -> "QASX"
  | QDADD -> "QDADD"
  | QDSUB -> "QDSUB"
  | QSAX -> "QSAX"
  | QSUB -> "QSUB"
  | QSUB16 -> "QSUB16"
  | QSUB8 -> "QSUB8"
  | RBIT -> "RBIT"
  | REV -> "REV"
  | REV16 -> "REV16"
  | REVSH -> "REVSH"
  | RFEDA -> "RFEDA"
  | RFEDB -> "RFEDB"
  | RFEIA -> "RFEIA"
  | RFEIB -> "RFEIB"
  | ROR -> "ROR"
  | RORS -> "RORS"
  | RRX -> "RRX"
  | RRXS -> "RRXS"
  | RSB -> "RSB"
  | RSBS -> "RSBS"
  | RSC -> "RSC"
  | RSCS -> "RSCS"
  | SADD16 -> "SADD16"
  | SADD8 -> "SADD8"
  | SASX -> "SASX"
  | SBC -> "SBC"
  | SBCS -> "SBCS"
  | SBFX -> "SBFX"
  | SDIV -> "SDIV"
  | SEL -> "SEL"
  | SETEND -> "SETEND"
  | SEV -> "SEV"
  | SEVL -> "SEVL"
  | SHA1H -> "SHA1H"
  | SHA1SU1 -> "SHA1SU1"
  | SHA256SU0 -> "SHA256SU0"
  | SHADD16 -> "SHADD16"
  | SHADD8 -> "SHADD8"
  | SHASX -> "SHASX"
  | SHSAX -> "SHSAX"
  | SHSUB16 -> "SHSUB16"
  | SHSUB8 -> "SHSUB8"
  | SMC -> "SMC"
  | SMLABB -> "SMLABB"
  | SMLABT -> "SMLABT"
  | SMLAD -> "SMLAD"
  | SMLADX -> "SMLADX"
  | SMLAL -> "SMLAL"
  | SMLALBB -> "SMLALBB"
  | SMLALBT -> "SMLALBT"
  | SMLALD -> "SMLALD"
  | SMLALDX -> "SMLALDX"
  | SMLALS -> "SMLALS"
  | SMLALTB -> "SMLALTB"
  | SMLALTT -> "SMLALTT"
  | SMLATB -> "SMLATB"
  | SMLATT -> "SMLATT"
  | SMLAWB -> "SMLAWB"
  | SMLAWT -> "SMLAWT"
  | SMLSD -> "SMLSD"
  | SMLSDX -> "SMLSDX"
  | SMLSLD -> "SMLSLD"
  | SMLSLDX -> "SMLSLDX"
  | SMMLA -> "SMMLA"
  | SMMLAR -> "SMMLAR"
  | SMMLS -> "SMMLS"
  | SMMLSR -> "SMMLSR"
  | SMMUL -> "SMMUL"
  | SMMULR -> "SMMULR"
  | SMUAD -> "SMUAD"
  | SMUADX -> "SMUADX"
  | SMULBB -> "SMULBB"
  | SMULBT -> "SMULBT"
  | SMULL -> "SMULL"
  | SMULLS -> "SMULLS"
  | SMULTB -> "SMULTB"
  | SMULTT -> "SMULTT"
  | SMULWB -> "SMULWB"
  | SMULWT -> "SMULWT"
  | SMUSD -> "SMUSD"
  | SMUSDX -> "SMUSDX"
  | SRS -> "SRS"
  | SRSDA -> "SRSDA"
  | SRSDB -> "SRSDB"
  | SRSIA -> "SRSIA"
  | SRSIB -> "SRSIB"
  | SSAT -> "SSAT"
  | SSAT16 -> "SSAT16"
  | SSAX -> "SSAX"
  | SSUB16 -> "SSUB16"
  | SSUB8 -> "SSUB8"
  | STC -> "STC"
  | STC2 -> "STC2"
  | STC2L -> "STC2L"
  | STCL -> "STCL"
  | STL -> "STL"
  | STLB -> "STLB"
  | STLEX -> "STLEX"
  | STLEXB -> "STLEXB"
  | STLEXD -> "STLEXD"
  | STLEXH -> "STLEXH"
  | STLH -> "STLH"
  | STM -> "STM"
  | STMDA -> "STMDA"
  | STMDB -> "STMDB"
  | STMEA -> "STMEA"
  | STMIA -> "STMIA"
  | STMIB -> "STMIB"
  | STR -> "STR"
  | STRB -> "STRB"
  | STRBT -> "STRBT"
  | STRD -> "STRD"
  | STREX -> "STREX"
  | STREXB -> "STREXB"
  | STREXD -> "STREXD"
  | STREXH -> "STREXH"
  | STRH -> "STRH"
  | STRHT -> "STRHT"
  | STRT -> "STRT"
  | SUB -> "SUB"
  | SUBS -> "SUBS"
  | SUBW -> "SUBW"
  | SVC -> "SVC"
  | SWP -> "SWP"
  | SWPB -> "SWPB"
  | SXTAB -> "SXTAB"
  | SXTAB16 -> "SXTAB16"
  | SXTAH -> "SXTAH"
  | SXTB -> "SXTB"
  | SXTB16 -> "SXTB16"
  | SXTH -> "SXTH"
  | TBB -> "TBB"
  | TBH -> "TBH"
  | TEQ -> "TEQ"
  | TST -> "TST"
  | UADD16 -> "UADD16"
  | UADD8 -> "UADD8"
  | UASX -> "UASX"
  | UBFX -> "UBFX"
  | UDF -> "UDF"
  | UDIV -> "UDIV"
  | UHADD16 -> "UHADD16"
  | UHADD8 -> "UHADD8"
  | UHASX -> "UHASX"
  | UHSAX -> "UHSAX"
  | UHSUB16 -> "UHSUB16"
  | UHSUB8 -> "UHSUB8"
  | UMAAL -> "UMAAL"
  | UMLAL -> "UMLAL"
  | UMLALS -> "UMLALS"
  | UMULL -> "UMULL"
  | UMULLS -> "UMULLS"
  | UQADD16 -> "UQADD16"
  | UQADD8 -> "UQADD8"
  | UQASX -> "UQASX"
  | UQSAX -> "UQSAX"
  | UQSUB16 -> "UQSUB16"
  | UQSUB8 -> "UQSUB8"
  | USAD8 -> "USAD8"
  | USADA8 -> "USADA8"
  | USAT -> "USAT"
  | USAT16 -> "USAT16"
  | USAX -> "USAX"
  | USUB16 -> "USUB16"
  | USUB8 -> "USUB8"
  | UXTAB -> "UXTAB"
  | UXTAB16 -> "UXTAB16"
  | UXTAH -> "UXTAH"
  | UXTB -> "UXTB"
  | UXTB16 -> "UXTB16"
  | UXTH -> "UXTH"
  | VABA -> "VABA"
  | VABAL -> "VABAL"
  | VABD -> "VABD"
  | VABDL -> "VABDL"
  | VABS -> "VABS"
  | VACGE -> "VACGE"
  | VACGT -> "VACGT"
  | VACLE -> "VACLE"
  | VACLT -> "VACLT"
  | VADD -> "VADD"
  | VADDHN -> "VADDHN"
  | VADDL -> "VADDL"
  | VADDW -> "VADDW"
  | VAND -> "VAND"
  | VBIC -> "VBIC"
  | VBIF -> "VBIF"
  | VBIT -> "VBIT"
  | VBSL -> "VBSL"
  | VCEQ -> "VCEQ"
  | VCGE -> "VCGE"
  | VCGT -> "VCGT"
  | VCLE -> "VCLE"
  | VCLS -> "VCLS"
  | VCLT -> "VCLT"
  | VCLZ -> "VCLZ"
  | VCMP -> "VCMP"
  | VCMPE -> "VCMPE"
  | VCNT -> "VCNT"
  | VCVT -> "VCVT"
  | VCVTA -> "VCVTA"
  | VCVTB -> "VCVTB"
  | VCVTM -> "VCVTM"
  | VCVTN -> "VCVTN"
  | VCVTP -> "VCVTP"
  | VCVTR -> "VCVTR"
  | VCVTT -> "VCVTT"
  | VDIV -> "VDIV"
  | VDUP -> "VDUP"
  | VEOR -> "VEOR"
  | VEXT -> "VEXT"
  | VFMA -> "VFMA"
  | VFMS -> "VFMS"
  | VFNMA -> "VFNMA"
  | VFNMS -> "VFNMS"
  | VHADD -> "VHADD"
  | VHSUB -> "VHSUB"
  | VLD1 -> "VLD1"
  | VLD2 -> "VLD2"
  | VLD3 -> "VLD3"
  | VLD4 -> "VLD4"
  | VLDM -> "VLDM"
  | VLDMDB -> "VLDMDB"
  | VLDMIA -> "VLDMIA"
  | VLDR -> "VLDR"
  | VMAX -> "VMAX"
  | VMIN -> "VMIN"
  | VMLA -> "VMLA"
  | VMLAL -> "VMLAL"
  | VMLS -> "VMLS"
  | VMLSL -> "VMLSL"
  | VMOV -> "VMOV"
  | VMOVL -> "VMOVL"
  | VMOVN -> "VMOVN"
  | VMRS -> "VMRS"
  | VMSR -> "VMSR"
  | VMUL -> "VMUL"
  | VMULL -> "VMULL"
  | VMVN -> "VMVN"
  | VNEG -> "VNEG"
  | VNMLA -> "VNMLA"
  | VNMLS -> "VNMLS"
  | VNMUL -> "VNMUL"
  | VORN -> "VORN"
  | VORR -> "VORR"
  | VPADAL -> "VPADAL"
  | VPADD -> "VPADD"
  | VPADDL -> "VPADDL"
  | VPMAX -> "VPMAX"
  | VPMIN -> "VPMIN"
  | VPOP -> "VPOP"
  | VPUSH -> "VPUSH"
  | VQABS -> "VQABS"
  | VQADD -> "VQADD"
  | VQDMLAL -> "VQDMLAL"
  | VQDMLSL -> "VQDMLSL"
  | VQDMULH -> "VQDMULH"
  | VQDMULL -> "VQDMULL"
  | VQMOVN -> "VQMOVN"
  | VQMOVUN -> "VQMOVUN"
  | VQNEG -> "VQNEG"
  | VQRDMULH -> "VQRDMULH"
  | VQRSHL -> "VQRSHL"
  | VQRSHRN -> "VQRSHRN"
  | VQRSHRUN -> "VQRSHRUN"
  | VQSHL -> "VQSHL"
  | VQSHLU -> "VQSHLU"
  | VQSHRN -> "VQSHRN"
  | VQSHRUN -> "VQSHRUN"
  | VQSUB -> "VQSUB"
  | VRADDHN -> "VRADDHN"
  | VRECPE -> "VRECPE"
  | VRECPS -> "VRECPS"
  | VREV16 -> "VREV16"
  | VREV32 -> "VREV32"
  | VREV64 -> "VREV64"
  | VRHADD -> "VRHADD"
  | VRINTA -> "VRINTA"
  | VRINTM -> "VRINTM"
  | VRINTN -> "VRINTN"
  | VRINTP -> "VRINTP"
  | VRINTR -> "VRINTR"
  | VRINTX -> "VRINTX"
  | VRINTZ -> "VRINTZ"
  | VRSHL -> "VRSHL"
  | VRSHR -> "VRSHR"
  | VRSHRN -> "VRSHRN"
  | VRSQRTE -> "VRSQRTE"
  | VRSQRTS -> "VRSQRTS"
  | VRSRA -> "VRSRA"
  | VRSUBHN -> "VRSUBHN"
  | VSEL -> "VSEL"
  | VSHL -> "VSHL"
  | VSHLL -> "VSHLL"
  | VSHR -> "VSHR"
  | VSHRN -> "VSHRN"
  | VSLI -> "VSLI"
  | VSQRT -> "VSQRT"
  | VSRA -> "VSRA"
  | VSRI -> "VSRI"
  | VST1 -> "VST1"
  | VST2 -> "VST2"
  | VST3 -> "VST3"
  | VST4 -> "VST4"
  | VSTM -> "VSTM"
  | VSTMDB -> "VSTMDB"
  | VSTMIA -> "VSTMIA"
  | VSTR -> "VSTR"
  | VSUB -> "VSUB"
  | VSUBHN -> "VSUBHN"
  | VSUBL -> "VSUBL"
  | VSUBW -> "VSUBW"
  | VSWP -> "VSWP"
  | VTBL -> "VTBL"
  | VTBX -> "VTBX"
  | VTRN -> "VTRN"
  | VTST -> "VTST"
  | VUZP -> "VUZP"
  | VZIP -> "VZIP"
  | WFE -> "WFE"
  | WFI -> "WFI"
  | YIELD -> "YIELD"
