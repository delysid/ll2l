(* this code is borrowed from the B2R2 project *)

open Core_kernel
open Ll2l_std
module C = Arm.Condition
module Q = Arm.Qualifier
module S = Arm.Simd_data
module Mode = Arm.Mode
module M = Arm.Mnemonic
module O = Arm.Operand
module Sh = O.Shift_kind
module Psr = O.Psr_flag
module Oo = O.Option_op
module Oi = O.Iflag
module R = Arm.Register
module I = Arm.Instruction

let sign_ext bitsize extsize imm =
  assert (bitsize <= extsize);
  if Int64.((imm lsr Int.(bitsize - 1)) = 0L) then imm
  else
    let module M64 = Bitvec.Make (Bitvec.M64) in
    let mask1 = M64.((one lsl int extsize) - one) in
    let mask2 = M64.((one lsl int bitsize) - one) in
    let result = M64.((mask1 - mask2) lor int64 imm) in
    M64.to_int64 result

let e : O.t array = [||]

let ex enc hi lo =
  let hi, lo = if max hi lo = hi then (hi, lo) else (lo, hi) in
  let off = hi - lo + 1 in
  if off > 31 then failwith "invalid range"
  else (enc lsr lo) land (Int.pow 2 off - 1)

let tst enc pos =
  match (enc lsr pos) land 1 with
  | 0 -> false
  | _ -> true

let concat n1 n2 shift = (n1 lsl shift) lor n2 [@@inline]

let halve n = (n land 0xFFFF, (n lsr 16) land 0xFFFF) [@@inline]

let align x y = x / y * y [@@inline]

let parse_imm_shift typ imm5 =
  match typ with
  | 0b00 -> Ok (Sh.LSL, imm5)
  | 0b01 -> Ok (Sh.LSR, if imm5 = 0 then 32 else imm5)
  | 0b10 -> Ok (Sh.ASR, if imm5 = 0 then 32 else imm5)
  | 0b11 when imm5 = 0 -> Ok (Sh.RRX, 1)
  | 0b11 -> Ok (Sh.ROR, imm5)
  | _ -> Error `BadOperand

let parse_reg_shift = function
  | 0b00 -> Ok Sh.LSL
  | 0b01 -> Ok Sh.LSR
  | 0b10 -> Ok Sh.ASR
  | 0b11 -> Ok Sh.ROR
  | _ -> Error `BadOperand

let parse_cond = function
  | 0x0 -> Ok C.EQ
  | 0x1 -> Ok C.NE
  | 0x2 -> Ok C.CS
  | 0x3 -> Ok C.CC
  | 0x4 -> Ok C.MI
  | 0x5 -> Ok C.PL
  | 0x6 -> Ok C.VS
  | 0x7 -> Ok C.VC
  | 0x8 -> Ok C.HI
  | 0x9 -> Ok C.LS
  | 0xA -> Ok C.GE
  | 0xB -> Ok C.LT
  | 0xC -> Ok C.GT
  | 0xD -> Ok C.LE
  | 0xE -> Ok C.AL
  | 0xF -> Ok C.UN
  | _ -> Error `Invalid

let mem_off_imm reg neg imm =
  Ok O.(Mem Memory.(Off Offset.(Imm {reg; neg; imm})))
  [@@inline]

let mem_off_reg reg neg reg_off shift =
  Ok O.(Mem Memory.(Off Offset.(Reg {reg; neg; reg_off; shift})))
  [@@inline]

let mem_off_align reg align reg_align =
  Ok O.(Mem Memory.(Off Offset.(Align {reg; align; reg_align})))
  [@@inline]

let mem_pre_imm reg neg imm =
  Ok O.(Mem Memory.(Preindex Offset.(Imm {reg; neg; imm})))
  [@@inline]

let mem_pre_reg reg neg reg_off shift =
  Ok O.(Mem Memory.(Preindex Offset.(Reg {reg; neg; reg_off; shift})))
  [@@inline]

let mem_pre_align reg align reg_align =
  Ok O.(Mem Memory.(Preindex Offset.(Align {reg; align; reg_align})))
  [@@inline]

let mem_post_imm reg neg imm =
  Ok O.(Mem Memory.(Postindex Offset.(Imm {reg; neg; imm})))
  [@@inline]

let mem_post_reg reg neg reg_off shift =
  Ok O.(Mem Memory.(Postindex Offset.(Reg {reg; neg; reg_off; shift})))
  [@@inline]

let mem_post_align reg align reg_align =
  Ok O.(Mem Memory.(Postindex Offset.(Align {reg; align; reg_align})))
  [@@inline]

let mem_un r opt = Ok O.(Mem (Unindex (r, opt))) [@@inline]

let mem_lbl l = Ok O.(Mem (Label l)) [@@inline]

let simd_vreg r = Ok O.(SIMD Simd.(Single (Reg.Vector r))) [@@inline]

let simd_sreg r s = Ok O.(SIMD Simd.(Single (Reg.Scalar (r, s)))) [@@inline]

let apsr = function
  | 0b00 -> Ok (R.APSR, None)
  | 0b01 -> Ok (R.APSR, Some Psr.C)
  | 0b10 -> Ok (R.APSR, Some Psr.NZCVQ)
  | 0b11 -> Ok (R.APSR, Some Psr.NZCVQG)
  | _ -> Error `BadOperand

let cpsr = function
  | 0x1 -> Ok (R.CPSR, None)
  | 0x2 -> Ok (R.CPSR, Some Psr.C)
  | 0x3 -> Ok (R.CPSR, Some Psr.X)
  | 0x4 -> Ok (R.CPSR, Some Psr.XC)
  | 0x5 -> Ok (R.CPSR, Some Psr.S)
  | 0x6 -> Ok (R.CPSR, Some Psr.SC)
  | 0x7 -> Ok (R.CPSR, Some Psr.SX)
  | 0x8 -> Ok (R.CPSR, Some Psr.F)
  | 0x9 -> Ok (R.CPSR, Some Psr.FC)
  | 0xA -> Ok (R.CPSR, Some Psr.FX)
  | 0xB -> Ok (R.CPSR, Some Psr.FXC)
  | 0xC -> Ok (R.CPSR, Some Psr.FS)
  | 0xD -> Ok (R.CPSR, Some Psr.FSC)
  | 0xE -> Ok (R.CPSR, Some Psr.FSX)
  | 0xF -> Ok (R.CPSR, Some Psr.FSXC)
  | _ -> Error `BadOperand

let spsr = function
  | 0x1 -> Ok (R.SPSR, None)
  | 0x2 -> Ok (R.SPSR, Some Psr.C)
  | 0x3 -> Ok (R.SPSR, Some Psr.X)
  | 0x4 -> Ok (R.SPSR, Some Psr.XC)
  | 0x5 -> Ok (R.SPSR, Some Psr.S)
  | 0x6 -> Ok (R.SPSR, Some Psr.SC)
  | 0x7 -> Ok (R.SPSR, Some Psr.SX)
  | 0x8 -> Ok (R.SPSR, Some Psr.F)
  | 0x9 -> Ok (R.SPSR, Some Psr.FC)
  | 0xA -> Ok (R.SPSR, Some Psr.FX)
  | 0xB -> Ok (R.SPSR, Some Psr.FXC)
  | 0xC -> Ok (R.SPSR, Some Psr.FS)
  | 0xD -> Ok (R.SPSR, Some Psr.FSC)
  | 0xE -> Ok (R.SPSR, Some Psr.FSX)
  | 0xF -> Ok (R.SPSR, Some Psr.FSXC)
  | _ -> Error `BadOperand

let banked r sysm =
  match (r, sysm) with
  | false, 0b00000 -> Ok R.R8USR
  | false, 0b00001 -> Ok R.R9USR
  | false, 0b00010 -> Ok R.R10USR
  | false, 0b00011 -> Ok R.R11USR
  | false, 0b00100 -> Ok R.R12USR
  | false, 0b00101 -> Ok R.SPUSR
  | false, 0b00110 -> Ok R.LRUSR
  | false, 0b01000 -> Ok R.R8FIQ
  | false, 0b01001 -> Ok R.R9FIQ
  | false, 0b01010 -> Ok R.R10FIQ
  | false, 0b01011 -> Ok R.R11FIQ
  | false, 0b01100 -> Ok R.R12FIQ
  | false, 0b01101 -> Ok R.SPFIQ
  | false, 0b01110 -> Ok R.LRFIQ
  | false, 0b10000 -> Ok R.LRIRQ
  | false, 0b10001 -> Ok R.SPIRQ
  | false, 0b10010 -> Ok R.LRSVC
  | false, 0b10011 -> Ok R.SPSVC
  | false, 0b10100 -> Ok R.LRABT
  | false, 0b10101 -> Ok R.SPABT
  | false, 0b10110 -> Ok R.LRUND
  | false, 0b10111 -> Ok R.SPUND
  | false, 0b11100 -> Ok R.LRMON
  | false, 0b11101 -> Ok R.SPMON
  | false, 0b11110 -> Ok R.ELRHYP
  | false, 0b11111 -> Ok R.SPHYP
  | true, 0b01110 -> Ok R.SPSRFIQ
  | true, 0b10000 -> Ok R.SPSRIRQ
  | true, 0b10010 -> Ok R.SPSRSVC
  | true, 0b10100 -> Ok R.SPSRABT
  | true, 0b10110 -> Ok R.SPSRUND
  | true, 0b11100 -> Ok R.SPSRMON
  | true, 0b11110 -> Ok R.SPSRHYP
  | _ -> Error `BadOperand

let opt = function
  | 0b0010 -> Ok Oo.OSHST
  | 0b0011 -> Ok Oo.OSH
  | 0b0110 -> Ok Oo.NSHST
  | 0b0111 -> Ok Oo.NSH
  | 0b1010 -> Ok Oo.ISHST
  | 0b1011 -> Ok Oo.ISH
  | 0b1110 -> Ok Oo.ST
  | 0b1111 -> Ok Oo.SY
  | _ -> Error `BadOperand

let iflag = function
  | 0b100 -> Ok Oi.A
  | 0b010 -> Ok Oi.I
  | 0b001 -> Ok Oi.F
  | 0b110 -> Ok Oi.AI
  | 0b101 -> Ok Oi.AF
  | 0b011 -> Ok Oi.IF
  | 0b111 -> Ok Oi.AIF
  | _ -> Error `BadOperand

let endianness = function
  | false -> `LE
  | true -> `BE

let float_size = function
  | false -> S.F32
  | true -> S.F64

let signedness_size_32 = function
  | false -> S.U32
  | true -> S.S32

let signedness_size_u size u =
  match (size, u) with
  | 0b00, false -> Ok S.S8
  | 0b01, false -> Ok S.S16
  | 0b10, false -> Ok S.S32
  | 0b11, false -> Ok S.S64
  | 0b00, true -> Ok S.U8
  | 0b01, true -> Ok S.U16
  | 0b10, true -> Ok S.U32
  | 0b11, true -> Ok S.U64
  | _ -> Error `BadOperand

let integer_size = function
  | 0b00 -> Ok S.I8
  | 0b01 -> Ok S.I16
  | 0b10 -> Ok S.I32
  | 0b11 -> Ok S.I64
  | _ -> Error `BadOperand

let integer_size2 = function
  | 0b00 -> Ok S.I16
  | 0b01 -> Ok S.I32
  | 0b10 -> Ok S.I64
  | _ -> Error `BadOperand

let signed_size = function
  | 0b00 -> Ok S.S8
  | 0b01 -> Ok S.S16
  | 0b10 -> Ok S.S32
  | 0b11 -> Ok S.S64
  | _ -> Error `BadOperand

let any_size = function
  | 0b00 -> Ok S.Any8
  | 0b01 -> Ok S.Any16
  | 0b10 -> Ok S.Any32
  | 0b11 -> Ok S.Any64
  | _ -> Error `BadOperand

let integer_size_f size f =
  match (size, f) with
  | 0b00, false -> Ok S.I8
  | 0b01, false -> Ok S.I16
  | 0b10, false -> Ok S.I32
  | 0b01, true -> Ok S.F16
  | 0b10, true -> Ok S.F32
  | _ -> Error `BadOperand

let vld4_size = function
  | 0b00 -> Ok S.Any8
  | 0b01 -> Ok S.Any16
  | 0b10 -> Ok S.Any32
  | 0b11 -> Ok S.Any32
  | _ -> Error `BadOperand

let signedness_size_unsx u sx =
  match (u, sx) with
  | false, false -> S.S16
  | true, false -> S.U16
  | false, true -> S.S32
  | true, true -> S.U32

let vfp_bits bit vbits = function
  | 32 -> Ok (concat vbits bit 1)
  | 64 -> Ok (concat bit vbits 4)
  | 128 -> Ok (concat bit (vbits lsr 1) 3)
  | _ -> Error `BadOperand

let vfp_reg n = function
  | 32 -> Result.of_option ~error:`BadOperand (R.fp_single_reg_of_int n)
  | 64 -> Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int n)
  | 128 -> Result.of_option ~error:`BadOperand (R.fp_vector_reg_of_int n)
  | _ -> Error `BadOperand

let vreg_off e1 e2 offset size =
  let open Result.Let_syntax in
  let%bind vbits = vfp_bits e1 e2 size in
  vfp_reg (vbits + offset) size

let vreg e1 e2 size =
  let open Result.Let_syntax in
  let%bind vbits = vfp_bits e1 e2 size in
  vfp_reg vbits size

let reglist b =
  let open Result.Let_syntax in
  let rec loop acc = function
    | n when n > 15 -> (
      match acc with
      | [] -> Error `BadOperand
      | _ -> return acc )
    | n when (b lsr n) land 1 <> 0 ->
        let%bind reg =
          Result.of_option ~error:`BadOperand (R.gp_reg_of_int n)
        in
        loop (reg :: acc) (succ n)
    | n -> loop acc (succ n)
  in
  let%map acc = loop [] 0 in
  List.rev acc

let simd_vfp_opr_reglist d vd rs sz =
  let open Result.Let_syntax in
  let rec aux acc i rs =
    if rs > 0 then
      let%bind reg = vreg_off d vd i sz in
      aux (reg :: acc) (succ i) (pred rs)
    else acc |> List.rev |> return
  in
  aux [] 0 rs

let simd_vector l =
  match l with
  | [_] | [_; _] | [_; _; _] | [_; _; _; _] ->
      let l = List.map l ~f:(fun r -> O.Simd.Reg.Vector r) in
      Ok O.(SIMD Simd.(Regs l))
  | _ -> Error `BadOperand

let simd_scalar l i =
  match l with
  | [_] | [_; _] | [_; _; _] | [_; _; _; _] ->
      let l = List.map l ~f:(fun r -> O.Simd.Reg.Scalar (r, i)) in
      Ok O.(SIMD Simd.(Regs l))
  | _ -> Error `BadOperand

let shift_opr_rotate = function
  | 0b00 -> Ok O.(Shift (Sh.ROR, 0))
  | 0b01 -> Ok O.(Shift (Sh.ROR, 8))
  | 0b10 -> Ok O.(Shift (Sh.ROR, 16))
  | 0b11 -> Ok O.(Shift (Sh.ROR, 24))
  | _ -> Error `BadOperand

let vstoreload1_idx ia = function
  | 0b00 when not (tst ia 0) -> Ok (ex ia 3 1)
  | 0b01 when ex ia 1 0 = 0b00 -> Ok (ex ia 3 2)
  | 0b01 when ex ia 1 0 = 0b01 -> Ok (ex ia 3 2)
  | 0b10 when ex ia 2 0 = 0b000 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b011 -> Ok (tst ia 3 |> Bool.to_int)
  | _ -> Error `Invalid

let vstoreload2_idx ia = function
  | 0b00 -> Ok (ex ia 3 1)
  | 0b01 -> Ok (ex ia 3 2)
  | 0b10 when ex ia 2 0 = 0b000 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b001 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b100 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b101 -> Ok (tst ia 3 |> Bool.to_int)
  | _ -> Error `Invalid

let vstoreload3_idx ia = function
  | 0b00 when not (tst ia 0) -> Ok (ex ia 3 1)
  | 0b01 when ex ia 1 0 = 0b00 -> Ok (ex ia 3 2)
  | 0b01 when ex ia 1 0 = 0b10 -> Ok (ex ia 3 2)
  | 0b10 when ex ia 2 0 = 0b000 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b100 -> Ok (tst ia 3 |> Bool.to_int)
  | _ -> Error `Invalid

let vstoreload4_idx ia = function
  | 0b00 -> Ok (ex ia 3 1)
  | 0b01 -> Ok (ex ia 3 1)
  | 0b10 when ex ia 2 0 = 0b000 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b001 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b010 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b100 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b101 -> Ok (tst ia 3 |> Bool.to_int)
  | 0b10 when ex ia 2 0 = 0b110 -> Ok (tst ia 3 |> Bool.to_int)
  | _ -> Error `Invalid

let vstoreload2_space ia = function
  | 0b00 -> Ok `Single
  | 0b01 when ex ia 1 0 = 0b00 -> Ok `Single
  | 0b01 when ex ia 1 0 = 0b01 -> Ok `Single
  | 0b01 when ex ia 1 0 = 0b10 -> Ok `Double
  | 0b01 when ex ia 1 0 = 0b11 -> Ok `Double
  | 0b10 when ex ia 2 0 = 0b000 -> Ok `Single
  | 0b10 when ex ia 2 0 = 0b001 -> Ok `Single
  | 0b10 when ex ia 2 0 = 0b100 -> Ok `Double
  | 0b10 when ex ia 2 0 = 0b101 -> Ok `Double
  | _ -> Error `Invalid

let vstoreload3_space ia = function
  | 0b00 when not (tst ia 0) -> Ok `Single
  | 0b01 when ex ia 1 0 = 0b00 -> Ok `Single
  | 0b01 when ex ia 1 0 = 0b10 -> Ok `Double
  | 0b10 when ex ia 2 0 = 0b000 -> Ok `Single
  | 0b10 when ex ia 2 0 = 0b100 -> Ok `Double
  | _ -> Error `Invalid

let vstoreload4_space ia = function
  | 0b00 -> Ok `Single
  | 0b01 when ex ia 1 0 = 0b00 -> Ok `Single
  | 0b01 when ex ia 1 0 = 0b01 -> Ok `Single
  | 0b01 when ex ia 1 0 = 0b10 -> Ok `Double
  | 0b01 when ex ia 1 0 = 0b11 -> Ok `Double
  | 0b10 when ex ia 2 0 = 0b000 -> Ok `Single
  | 0b10 when ex ia 2 0 = 0b001 -> Ok `Single
  | 0b10 when ex ia 2 0 = 0b010 -> Ok `Single
  | 0b10 when ex ia 2 0 = 0b100 -> Ok `Double
  | 0b10 when ex ia 2 0 = 0b101 -> Ok `Double
  | 0b10 when ex ia 2 0 = 0b110 -> Ok `Double
  | _ -> Error `Invalid

let imm11110 mnemonic i =
  match mnemonic with
  | M.VMOV ->
      let imm n = tst i n |> Bool.to_int |> Int.to_int64 in
      Ok
        Int64.(
          (0xF0000000L * imm 7)
          + (0x0F000000L * imm 6)
          + (0x00F00000L * imm 5)
          + (0x000F0000L * imm 4)
          + (0x0000F000L * imm 3)
          + (0x00000F00L * imm 2)
          + (0x000000F0L * imm 1)
          + (0x0000000FL * imm 0))
  | _ -> Error `Invalid

let imm01111 mnemonic i =
  match mnemonic with
  | M.VMOV ->
      let a = tst i 7 |> Bool.to_int |> Int.to_int64 in
      let b = tst i 6 |> Bool.to_int |> Int.to_int64 in
      let b5 = Int64.(b + (b lsl 1) + (b lsl 2) + (b lsl 3) + (b lsl 4)) in
      let cdefg = ex i 5 0 |> Int.to_int64 in
      Ok
        Int64.(
          (a lsl 63)
          + ((b lxor 1L) lsl 62)
          + (b5 lsl 57) + (cdefg lsl 51) + (a lsl 31)
          + ((b lxor 1L) lsl 30)
          + (b5 lsl 25) + (cdefg lsl 19))
  | _ -> Error `Invalid

let fpimm64 i =
  let a = tst i 7 |> Bool.to_int |> Int.to_int64 in
  let b = tst i 6 |> Bool.to_int |> Int.to_int64 in
  let b8 =
    Int64.(
      b + (b lsl 1) + (b lsl 2) + (b lsl 3) + (b lsl 4) + (b lsl 5)
      + (b lsl 6) + (b lsl 7))
  in
  let cdefg = ex i 5 0 |> Int.to_int64 in
  Int64.((a lsl 63) + ((b lxor 1L) lsl 62) + (b8 lsl 54) + (cdefg lsl 48))

let fpimm32 i =
  let a = tst i 7 |> Bool.to_int |> Int.to_int64 in
  let b = tst i 6 |> Bool.to_int |> Int.to_int64 in
  let b5 = Int64.(b + (b lsl 1) + (b lsl 2) + (b lsl 3) + (b lsl 4)) in
  let cdefg = ex i 5 0 |> Int.to_int64 in
  Int64.((a lsl 31) + ((b lxor 1L) lsl 30) + (b5 lsl 25) + (cdefg lsl 19))

let reg b s e =
  Result.of_option ~error:`BadOperand (R.gp_reg_of_int (ex b s e))

(* should we allow unpredictable instructions? *)
let do_chk f b opr =
  match f b opr with
  (* | Error `Unpredictable -> Ok () *)
  | x -> x
  [@@inline]

let p1opr b f opr =
  let open Result.Let_syntax in
  let%bind () = do_chk f b opr in
  let%bind op = opr b in
  return [|op|]

let p2oprs b f ((opr1, opr2) as opr) =
  let open Result.Let_syntax in
  let%bind () = do_chk f b opr in
  let%bind op1 = opr1 b in
  let%bind op2 = opr2 b in
  return [|op1; op2|]

let p3oprs b f ((opr1, opr2, opr3) as opr) =
  let open Result.Let_syntax in
  let%bind () = do_chk f b opr in
  let%bind op1 = opr1 b in
  let%bind op2 = opr2 b in
  let%bind op3 = opr3 b in
  return [|op1; op2; op3|]

let p4oprs b f ((opr1, opr2, opr3, opr4) as opr) =
  let open Result.Let_syntax in
  let%bind () = do_chk f b opr in
  let%bind op1 = opr1 b in
  let%bind op2 = opr2 b in
  let%bind op3 = opr3 b in
  let%bind op4 = opr4 b in
  return [|op1; op2; op3; op4|]

let p5oprs b f ((opr1, opr2, opr3, opr4, opr5) as opr) =
  let open Result.Let_syntax in
  let%bind () = do_chk f b opr in
  let%bind op1 = opr1 b in
  let%bind op2 = opr2 b in
  let%bind op3 = opr3 b in
  let%bind op4 = opr4 b in
  let%bind op5 = opr5 b in
  return [|op1; op2; op3; op4; op5|]

let p6oprs b f ((opr1, opr2, opr3, opr4, opr5, opr6) as opr) =
  let open Result.Let_syntax in
  let%bind () = do_chk f b opr in
  let%bind op1 = opr1 b in
  let%bind op2 = opr2 b in
  let%bind op3 = opr3 b in
  let%bind op4 = opr4 b in
  let%bind op5 = opr5 b in
  let%bind op6 = opr6 b in
  return [|op1; op2; op3; op4; op5; op6|]

let reg_a b =
  let open Result.Let_syntax in
  let%bind r = reg b 3 0 in
  return O.(Reg r)

let reg_b b =
  let open Result.Let_syntax in
  let%bind r = reg b 11 8 in
  return O.(Reg r)

let reg_c b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  return O.(Reg r)

let reg_d b =
  let open Result.Let_syntax in
  let%bind r = reg b 15 12 in
  return O.(Reg r)

let reg_e _ = Ok O.(Reg R.APSR) [@@inline]

let reg_f b =
  let open Result.Let_syntax in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.gp_reg_of_int (ex b 3 0 + 1))
  in
  return O.(Reg r)

let reg_g b =
  let open Result.Let_syntax in
  let%bind r = reg b 8 6 in
  return O.(Reg r)

let reg_h b =
  let open Result.Let_syntax in
  let%bind r = reg b 5 3 in
  return O.(Reg r)

let reg_i b =
  let open Result.Let_syntax in
  let%bind r = reg b 2 0 in
  return O.(Reg r)

let reg_j b =
  let open Result.Let_syntax in
  let%bind r = reg b 10 8 in
  return O.(Reg r)

let reg_k b =
  let open Result.Let_syntax in
  let mask = ex b 19 16 in
  let%bind r, psr = if tst b 22 then spsr mask else cpsr mask in
  return O.(Reg_special (r, psr))

let reg_l b =
  let open Result.Let_syntax in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.gp_reg_of_int (ex b 15 12 + 1))
  in
  return O.(Reg r)

let reg_m _ = Ok O.(Reg R.SP) [@@inline]

let reg_n b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  return O.(Reg r)

let reg_o b =
  let open Result.Let_syntax in
  let mask = concat (tst b 7 |> Bool.to_int) (ex b 2 0) 3 in
  let%bind r = Result.of_option ~error:`BadOperand (R.gp_reg_of_int mask) in
  return O.(Reg r)

let reg_p b =
  let open Result.Let_syntax in
  let%bind r = reg b 6 3 in
  return O.(Reg r)

let reg_q b =
  let open Result.Let_syntax in
  let mask = ex b 19 16 in
  let%bind r, psr = if tst b 22 then spsr mask else cpsr mask in
  return O.(Reg_special (r, psr))

let reg_r q b =
  let open Result.Let_syntax in
  let size = if q then 128 else 64 in
  let%bind r = vreg (tst b 22 |> Bool.to_int) (ex b 15 12) size in
  simd_vreg r

let reg_s q b =
  let open Result.Let_syntax in
  let size = if q then 128 else 64 in
  let%bind r = vreg (tst b 7 |> Bool.to_int) (ex b 19 16) size in
  simd_vreg r

let reg_t q b =
  let open Result.Let_syntax in
  let size = if q then 128 else 64 in
  let%bind r = vreg (tst b 5 |> Bool.to_int) (ex b 3 0) size in
  simd_vreg r

let reg_u b =
  let open Result.Let_syntax in
  let mask = concat (tst b 7 |> Bool.to_int) (ex b 19 16 lsr 1) 3 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_vector_reg_of_int mask)
  in
  simd_vreg r

let reg_v b =
  let open Result.Let_syntax in
  let mask = concat (tst b 7 |> Bool.to_int) (ex b 19 16) 4 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int mask)
  in
  simd_vreg r

let reg_x b =
  let open Result.Let_syntax in
  let size = if tst b 6 then 128 else 64 in
  let%bind r = vreg (tst b 22 |> Bool.to_int) (ex b 15 12) size in
  simd_vreg r

let reg_y b =
  let open Result.Let_syntax in
  let size = if tst b 6 then 128 else 64 in
  let%bind r = vreg (tst b 7 |> Bool.to_int) (ex b 19 16) size in
  simd_vreg r

let reg_z b =
  let open Result.Let_syntax in
  let size = if tst b 6 then 128 else 64 in
  let%bind r = vreg (tst b 5 |> Bool.to_int) (ex b 3 0) size in
  simd_vreg r

let reg_aa b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  return O.(Reg r)

let reg_ab b =
  let open Result.Let_syntax in
  let size = if tst b 21 then 128 else 64 in
  let%bind r = vreg (tst b 7 |> Bool.to_int) (ex b 19 16) size in
  simd_vreg r

let reg_ac b =
  let open Result.Let_syntax in
  let mask = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int mask)
  in
  simd_vreg r

let reg_ad b =
  let open Result.Let_syntax in
  let mask = concat (tst b 5 |> Bool.to_int) (ex b 3 0 lsr 1) 3 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_vector_reg_of_int mask)
  in
  simd_vreg r

let reg_ae b =
  let open Result.Let_syntax in
  let mask = concat (tst b 22 |> Bool.to_int) (ex b 15 12 lsr 1) 3 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_vector_reg_of_int mask)
  in
  simd_vreg r

let reg_af b =
  let open Result.Let_syntax in
  let mask = concat (tst b 5 |> Bool.to_int) (ex b 3 0) 4 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int mask)
  in
  simd_vreg r

let reg_ag b =
  let open Result.Let_syntax in
  let size = if tst b 8 then 128 else 64 in
  let%bind r = vreg (tst b 7 |> Bool.to_int) (ex b 19 16) size in
  simd_vreg r

let reg_ah b =
  let open Result.Let_syntax in
  let size = if tst b 8 then 128 else 64 in
  let%bind r = vreg (tst b 5 |> Bool.to_int) (ex b 3 0) size in
  simd_vreg r

let reg_ai b =
  let open Result.Let_syntax in
  let size = if tst b 8 then 128 else 64 in
  let%bind r = vreg (tst b 22 |> Bool.to_int) (ex b 15 12) size in
  simd_vreg r

let reg_aj b =
  let open Result.Let_syntax in
  let mask = concat (ex b 3 0) (tst b 5 |> Bool.to_int) 1 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_single_reg_of_int mask)
  in
  simd_vreg r

let reg_ak b =
  let open Result.Let_syntax in
  let mask = concat (ex b 15 12) (tst b 22 |> Bool.to_int) 1 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_single_reg_of_int (succ mask))
  in
  simd_vreg r

let reg_al b =
  let open Result.Let_syntax in
  let size = if tst b 8 then 64 else 32 in
  let%bind r = vreg (tst b 22 |> Bool.to_int) (ex b 15 12) size in
  simd_vreg r

(* XXX: is this a real fix? *)
let reg_al_r b =
  let open Result.Let_syntax in
  let size = if tst b 8 |> not then 64 else 32 in
  let%bind r = vreg (tst b 22 |> Bool.to_int) (ex b 15 12) size in
  simd_vreg r

let reg_am b =
  let open Result.Let_syntax in
  let size = if tst b 8 then 64 else 32 in
  let%bind r = vreg (tst b 7 |> Bool.to_int) (ex b 19 16) size in
  simd_vreg r

let reg_an b =
  let open Result.Let_syntax in
  let size = if tst b 8 then 64 else 32 in
  let%bind r = vreg (tst b 5 |> Bool.to_int) (ex b 3 0) size in
  simd_vreg r

let reg_an_r b =
  let open Result.Let_syntax in
  let size = if tst b 8 |> not then 64 else 32 in
  let%bind r = vreg (tst b 5 |> Bool.to_int) (ex b 3 0) size in
  simd_vreg r

let reg_ao b =
  let open Result.Let_syntax in
  let mask = concat (ex b 15 12) (tst b 22 |> Bool.to_int) 1 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_single_reg_of_int mask)
  in
  simd_vreg r

let reg_ap b =
  let open Result.Let_syntax in
  let%bind size =
    match (ex b 18 16, tst b 8) with
    | 0b000, true -> Ok 64
    | 0b101, _ | 0b100, _ | 0b000, false -> Ok 32
    | _ -> Error `BadOperand
  in
  let%bind r = vreg (tst b 22 |> Bool.to_int) (ex b 15 12) size in
  simd_vreg r

let reg_aq b =
  let open Result.Let_syntax in
  let%bind size =
    match (ex b 18 16, tst b 8) with
    | 0b101, true | 0b100, true -> Ok 64
    | 0b101, false | 0b100, false | 0b000, _ -> Ok 32
    | _ -> Error `BadOperand
  in
  let%bind r = vreg (tst b 5 |> Bool.to_int) (ex b 3 0) size in
  simd_vreg r

let reg_ar b =
  let open Result.Let_syntax in
  match (ex b 18 16, tst b 8) with
  | 0b101, _ | 0b100, _ ->
      let mask = concat (ex b 15 12) (tst b 22 |> Bool.to_int) 1 in
      let%bind r =
        Result.of_option ~error:`BadOperand (R.fp_single_reg_of_int mask)
      in
      simd_vreg r
  | _ -> Error `BadOperand

let reg_as b =
  let open Result.Let_syntax in
  let%bind size =
    match (ex b 18 16, tst b 8) with
    | 0b101, true | 0b100, true -> Ok 64
    | 0b101, false | 0b100, false -> Ok 32
    | _ -> Error `BadOperand
  in
  let%bind r = vreg (tst b 5 |> Bool.to_int) (ex b 3 0) size in
  simd_vreg r

let reg_at b =
  let open Result.Let_syntax in
  let size = if tst b 8 then 64 else 32 in
  let%bind r = vreg (tst b 22 |> Bool.to_int) (ex b 15 12) size in
  simd_vreg r

let reg_au b =
  let open Result.Let_syntax in
  let mask = concat (ex b 19 16) (tst b 7 |> Bool.to_int) 1 in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_single_reg_of_int mask)
  in
  simd_vreg r

let reg_av (_, b) =
  let open Result.Let_syntax in
  let%bind r = reg b 11 8 in
  return O.(Reg r)

let reg_aw (_, b) =
  let open Result.Let_syntax in
  let%bind r = reg b 15 12 in
  return O.(Reg r)

let reg_ax (_, b) =
  let open Result.Let_syntax in
  let%bind r = reg b 3 0 in
  return O.(Reg r)

let reg_ay (b, _) =
  let open Result.Let_syntax in
  let%bind r = reg b 3 0 in
  return O.(Reg r)

let reg_az b =
  let open Result.Let_syntax in
  let%map r = reg b 15 12 in
  match r with
  | R.PC -> O.(Reg_special (R.APSR, Some Psr.NZCV))
  | _ -> O.(Reg r)

let reg_sp _ = Ok O.(Reg R.SP) [@@inline]

let reg_pc _ = Ok O.(Reg R.PC) [@@inline]

let reg_lr _ = Ok O.(Reg R.LR) [@@inline]

let apsr_xa b =
  let open Result.Let_syntax in
  let mask = ex b 19 18 in
  match mask with
  | 0 -> Error `BadOperand
  | _ ->
      let%map r, psr = apsr mask in
      O.(Reg_special (r, psr))

let apsr_xb b =
  let open Result.Let_syntax in
  let%map r, psr = apsr (ex b 19 18) in
  O.(Reg_special (r, psr))

let apsr_xc (_, b) =
  let open Result.Let_syntax in
  let%map r, psr = apsr (ex b 11 10) in
  O.(Reg_special (r, psr))

let psr_xa (b1, b2) =
  let open Result.Let_syntax in
  let mask = ex b2 11 8 in
  let%map r, psr = if tst b1 4 then spsr mask else cpsr mask in
  O.(Reg_special (r, psr))

let psr_xb (b, _) =
  let r = if tst b 4 then R.SPSR else R.APSR in
  Ok O.(Reg r)

let reg_fpscr _ = Ok O.(Reg R.FPSCR) [@@inline]

let banked_reg_a b =
  let open Result.Let_syntax in
  let sysm = concat (tst b 8 |> Bool.to_int) (ex b 19 16) 4 in
  let%map r = banked (tst b 22) sysm in
  O.(Reg r)

let banked_reg_b (_, b) =
  let open Result.Let_syntax in
  let sysm = concat (tst b 4 |> Bool.to_int) (ex b 11 8) 4 in
  let%map r = banked true sysm in
  O.(Reg r)

let banked_reg_c (b1, b2) =
  let open Result.Let_syntax in
  let sysm = concat (tst b2 4 |> Bool.to_int) (ex b1 3 0) 4 in
  let%map r = banked (tst b1 4) sysm in
  O.(Reg r)

let reg_wa b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  Ok O.(Reg r)

let reg_wb (b, _) =
  let open Result.Let_syntax in
  let%bind r = reg b 3 0 in
  Ok O.(Reg r)

let reg_wc b =
  let open Result.Let_syntax in
  let%bind r = reg b 10 8 in
  Ok O.(Reg r)

let reg_wd b =
  let open Result.Let_syntax in
  let%bind rn = reg b 10 8 in
  let%bind rl = reglist (ex b 7 0) in
  if List.mem rl rn ~equal:R.equal then Ok O.(Reg rn) else Ok O.(Reg rn)

(* is this correct? *)

let creg b s e =
  Result.of_option ~error:`BadOperand (R.copc_reg_of_int (ex b s e))

let preg b s e =
  Result.of_option ~error:`BadOperand (R.copp_reg_of_int (ex b s e))

let creg_a b =
  let open Result.Let_syntax in
  let%bind r = creg b 15 12 in
  Ok O.(Reg r)

let creg_b b =
  let open Result.Let_syntax in
  let%bind r = creg b 3 0 in
  Ok O.(Reg r)

let creg_c b =
  let open Result.Let_syntax in
  let%bind r = creg b 19 16 in
  Ok O.(Reg r)

let preg_a b =
  let open Result.Let_syntax in
  let%bind r = preg b 11 8 in
  Ok O.(Reg r)

let reglist_a b =
  let open Result.Let_syntax in
  let d = concat (tst b 7 |> Bool.to_int) (ex b 19 16) 4 in
  match ex b 9 8 with
  | 0b00 ->
      let%bind r =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      simd_vector [r]
  | 0b01 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      simd_vector [r1; r2]
  | 0b10 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      simd_vector [r1; r2; r3]
  | 0b11 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r4 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 3))
      in
      simd_vector [r1; r2; r3; r4]
  | _ -> Error `BadOperand

let reglist_b b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  match ex b 11 8 with
  | 0b0111 ->
      let%bind r =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      simd_vector [r]
  | 0b1010 | 0b1000 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      simd_vector [r1; r2]
  | 0b1001 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      simd_vector [r1; r2]
  | 0b0110 | 0b0100 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      simd_vector [r1; r2; r3]
  | 0b0101 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 4))
      in
      simd_vector [r1; r2; r3]
  | 0b0010 | 0b0011 | 0b0000 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r4 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 3))
      in
      simd_vector [r1; r2; r3; r4]
  | 0b0001 ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 4))
      in
      let%bind r4 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 6))
      in
      simd_vector [r1; r2; r3; r4]
  | _ -> Error `BadOperand

let reglist_c b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  let%bind i = vstoreload1_idx (ex b 7 4) (ex b 11 10) in
  let%bind r =
    Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
  in
  simd_scalar [r] (Some i)

let reglist_d b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  let ia = ex b 7 4 in
  let sz = ex b 11 10 in
  let%bind i = vstoreload2_idx ia sz in
  let%bind s = vstoreload2_space ia sz in
  match s with
  | `Single ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      simd_scalar [r1; r2] (Some i)
  | `Double ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      simd_scalar [r1; r2] (Some i)

let reglist_e b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  let ia = ex b 7 4 in
  let sz = ex b 11 10 in
  let%bind i = vstoreload3_idx ia sz in
  let%bind s = vstoreload3_space ia sz in
  match s with
  | `Single ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      simd_scalar [r1; r2; r3] (Some i)
  | `Double ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 4))
      in
      simd_scalar [r1; r2; r3] (Some i)

let reglist_f b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  let ia = ex b 7 4 in
  let sz = ex b 11 10 in
  let%bind i = vstoreload4_idx ia sz in
  let%bind s = vstoreload4_space ia sz in
  match s with
  | `Single ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r4 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 3))
      in
      simd_scalar [r1; r2; r3; r4] (Some i)
  | `Double ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 4))
      in
      let%bind r4 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 6))
      in
      simd_scalar [r1; r2; r3; r4] (Some i)

let reglist_g b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  match tst b 5 with
  | false ->
      let%bind r =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      simd_scalar [r] None
  | true ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      simd_scalar [r1; r2] None

let reglist_h b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  match tst b 5 with
  | false ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      simd_scalar [r1; r2] None
  | true ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      simd_scalar [r1; r2] None

let reglist_i b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  match tst b 5 with
  | false ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      simd_scalar [r1; r2; r3] None
  | true ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 4))
      in
      simd_scalar [r1; r2; r3] None

let reglist_j b =
  let open Result.Let_syntax in
  let d = concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 in
  match tst b 5 with
  | false ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 1))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r4 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 3))
      in
      simd_scalar [r1; r2; r3; r4] None
  | true ->
      let%bind r1 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int d)
      in
      let%bind r2 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 2))
      in
      let%bind r3 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 4))
      in
      let%bind r4 =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int (d + 6))
      in
      simd_scalar [r1; r2; r3; r4] None

let reglist_k b =
  let open Result.Let_syntax in
  let%map l = reglist (ex b 15 0) in
  O.(Reg_list l)

let reglist_l b =
  let open Result.Let_syntax in
  let%map l =
    simd_vfp_opr_reglist
      (tst b 22 |> Bool.to_int)
      (ex b 15 12)
      (ex b 7 0 / 2)
      64
  in
  O.(Reg_list l)

let reglist_m b =
  let open Result.Let_syntax in
  let%map l =
    simd_vfp_opr_reglist (tst b 22 |> Bool.to_int) (ex b 15 12) (ex b 7 0) 32
  in
  O.(Reg_list l)

let reglist_n b =
  let open Result.Let_syntax in
  let%map l = reglist ((Bool.to_int (tst b 8) lsl 14) + ex b 7 0) in
  O.(Reg_list l)

let reglist_o b =
  let open Result.Let_syntax in
  let%map l = reglist ((Bool.to_int (tst b 8) lsl 15) + ex b 7 0) in
  O.(Reg_list l)

let reglist_p (_, b) =
  let open Result.Let_syntax in
  let%map l =
    concat (Bool.to_int (tst b 14) lsl 1) (ex b 12 0) 13 |> reglist
  in
  O.(Reg_list l)

let reglist_q (_, b) =
  let open Result.Let_syntax in
  let%map l = concat (ex b 15 14 lsl 1) (ex b 12 0) 13 |> reglist in
  O.(Reg_list l)

let reglist_r b =
  let open Result.Let_syntax in
  let%map l = reglist (ex b 7 0) in
  O.(Reg_list l)

let shiftimm5_a b = concat (ex b 14 12) (ex b 7 6) 2 [@@inline]

let shift typ imm =
  let open Result.Let_syntax in
  let%map shift, imm = parse_imm_shift typ imm in
  O.(Shift (shift, imm))

let shift_a b =
  let open Result.Let_syntax in
  let%bind shift = parse_reg_shift (ex b 6 5) in
  let%map r = reg b 11 8 in
  O.(Reg_shift (shift, r))

let shift_b b = shift (ex b 6 5) (ex b 11 7) [@@inline]

let shift_c b = shift_opr_rotate (ex b 11 10) [@@inline]

let shift_d b = shift ((tst b 6 |> Bool.to_int) lsl 1) (ex b 11 7) [@@inline]

let shift_f (_, b) = shift (ex b 5 4) (shiftimm5_a b) [@@inline]

let shift_i (b1, b2) =
  shift ((tst 1 5 |> Bool.to_int) lsl 1) (shiftimm5_a b2)
  [@@inline]

let shift_j (_, b) = shift_opr_rotate (ex b 5 4) [@@inline]

let imm0 _ = Ok (O.Imm 0L) [@@inline]

let fpimm0 _ = Ok (O.Fp_imm 0.0) [@@inline]

let replicate value width n =
  let rec loop acc idx =
    if idx <= 0 then acc
    else
      let sh = width * idx in
      let acc = Int64.(acc lor (acc lsl sh)) in
      loop acc (idx - 1)
  in
  loop value (n - 1)

let imm_a mnemonic i b =
  let open Result.Let_syntax in
  let chk1 i =
    match i with
    | 0L -> Error `Unpredictable
    | _ -> Ok ()
  in
  let chk2 i =
    match mnemonic with
    | M.VMOV | M.VMVN -> chk1 i
    | _ -> Error `Invalid
  in
  let ii = concat (concat i (ex b 18 16) 3) (ex b 3 0) 4 in
  let i = Int64.of_int ii in
  match concat (tst b 5 |> Bool.to_int) (ex b 11 8) 4 with
  | r when r land 0b00011 = 0b00000 -> Ok O.(Imm i)
  | r when r land 0b01110 = 0b00010 ->
      let%bind _ = chk1 i in
      let i = Int64.(i lsl 8) in
      Ok O.(Imm i)
  | r when r land 0b01110 = 0b00100 ->
      let%bind _ = chk1 i in
      let i = Int64.(i lsl 16) in
      Ok O.(Imm i)
  | r when r land 0b01110 = 0b00110 ->
      let%bind _ = chk1 i in
      let i = Int64.(i lsl 24) in
      Ok O.(Imm i)
  | r when r land 0b01110 = 0b01010 ->
      let%bind _ = chk1 i in
      let i = Int64.(i lsl 8) in
      Ok O.(Imm i)
  | r when r land 0b01111 = 0b01100 ->
      let%bind _ = chk2 i in
      let i = Int64.(0xFFL + (i lsl 8)) in
      Ok O.(Imm i)
  | r when r land 0b01111 = 0b01101 ->
      let%bind _ = chk2 i in
      let i = Int64.(0xFFL + (0xFFL lsl 8) + (i lsl 16)) in
      Ok O.(Imm i)
  | 0b01110 when M.(equal mnemonic VMOV) -> Ok O.(Imm (replicate i 8 8))
  | 0b11110 ->
      let%bind i = imm11110 mnemonic ii in
      Ok O.(Imm i)
  | 0b01111 ->
      let%bind i = imm01111 mnemonic ii in
      Ok O.(Imm i)
  | _ -> Error `BadOperand

let imm_b b =
  let open Result.Let_syntax in
  let%map imm =
    match concat (tst b 7 |> Bool.to_int) (ex b 21 16) 6 with
    | i when i land 0b1111000 = 0b1000 -> Ok Int64.(8L - of_int (ex b 18 16))
    | i when i land 0b1110000 = 0b10000 ->
        Ok Int64.(16L - of_int (ex b 19 16))
    | i when i land 0b1100000 = 0b100000 ->
        Ok Int64.(32L - of_int (ex b 20 16))
    | i when i land 0b1000000 > 0 -> Ok Int64.(64L - of_int (ex b 21 16))
    | _ -> Error `BadOperand
  in
  O.(Imm imm)

let imm_c b =
  let open Result.Let_syntax in
  let%map imm =
    match concat (tst b 7 |> Bool.to_int) (ex b 21 19) 3 with
    | 1 -> Ok Int64.(of_int (ex b 18 16))
    | i when i land 0b1110 = 0b0010 -> Ok Int64.(of_int (ex b 19 16))
    | i when i land 0b1100 = 0b0100 -> Ok Int64.(of_int (ex b 20 16))
    | i when i land 0b1000 = 0b1000 -> Ok Int64.(of_int (ex b 21 16))
    | _ -> Error `BadOperand
  in
  O.(Imm imm)

let imm_d b =
  let open Result.Let_syntax in
  let%map imm =
    match ex b 18 16 with
    | 1 -> Ok Int64.(8L - of_int (ex b 18 16))
    | i when i land 0b110 = 0b010 -> Ok Int64.(16L - of_int (ex b 19 16))
    | i when i land 0b100 = 0b100 -> Ok Int64.(32L - of_int (ex b 20 16))
    | _ -> Error `BadOperand
  in
  O.(Imm imm)

let imm_e b =
  let open Result.Let_syntax in
  let%map imm =
    match ex b 21 19 with
    | 1 -> Ok Int64.(8L - of_int (ex b 18 16))
    | i when i land 0b111 = 0b001 -> Ok Int64.(8L - of_int (ex b 18 16))
    | i when i land 0b110 = 0b010 -> Ok Int64.(16L - of_int (ex b 19 16))
    | i when i land 0b100 = 0b100 -> Ok Int64.(32L - of_int (ex b 20 16))
    | _ -> Error `BadOperand
  in
  O.(Imm imm)

let imm_f b =
  let open Result.Let_syntax in
  let%map imm =
    match ex b 21 19 with
    | 1 -> Ok Int64.(8L - of_int (ex b 18 16))
    | i when i land 0b111 = 0b001 -> Ok Int64.(of_int (ex b 18 16))
    | i when i land 0b110 = 0b010 -> Ok Int64.(of_int (ex b 19 16))
    | i when i land 0b100 = 0b100 -> Ok Int64.(of_int (ex b 20 16))
    | _ -> Error `BadOperand
  in
  O.(Imm imm)

let imm_g b =
  let imm = Int64.(64L - of_int (ex b 21 16)) in
  Ok O.(Imm imm)
  [@@inline]

let imm_h b =
  let imm = concat (ex b 19 16) (ex b 3 0) 4 in
  match tst b 8 with
  | false -> Ok (O.Imm (fpimm32 imm))
  | true -> Ok (O.Imm (fpimm64 imm))

let imm_i b =
  let size = if tst b 7 then 32L else 16L in
  let imm = concat (ex b 3 0) (tst b 5 |> Bool.to_int) 1 in
  Ok (O.Imm Int64.(size - of_int imm))

let imm_j (b1, b2) =
  let i = tst b1 10 |> Bool.to_int in
  let i3 = ex b2 14 12 in
  let tp = concat i i3 3 in
  let rot = concat tp (tst b2 7 |> Bool.to_int) 1 in
  let imm = ex b2 7 0 in
  match rot with
  | 0b00000 | 0b00001 -> Ok (O.Imm Int64.(of_int imm))
  | 0b00010 | 0b00011 -> Ok (O.Imm (Int64.of_int ((imm lsl 16) + imm)))
  | 0b00100 | 0b00101 ->
      let imm = (imm lsl 24) + (imm lsl 8) in
      Ok (O.Imm (Int64.of_int imm))
  | 0b00110 | 0b00111 ->
      let imm = (imm lsl 24) + (imm lsl 16) + (imm lsl 8) + imm in
      Ok (O.Imm (Int64.of_int imm))
  | rot ->
      let u = 0b10000000 lor imm in
      Ok (O.Imm (Int64.of_int ((u lsl (32 - rot)) lor (u lsr rot))))

let imm_k (_, b) =
  let imm = ex b 4 0 in
  let imm = imm - concat (ex b 14 12) (ex b 7 6) 2 + 1 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_3a b = Ok (O.Imm Int64.(of_int (ex b 8 6))) [@@inline]

let imm_3b b = Ok (O.Imm Int64.(of_int (ex b 7 5))) [@@inline]

let imm_3c b = Ok (O.Imm Int64.(of_int (ex b 23 21))) [@@inline]

let imm_4a b = Ok (O.Imm Int64.(of_int (ex b 3 0))) [@@inline]

let imm_4b b = Ok (O.Imm Int64.(succ (of_int (ex b 19 16)))) [@@inline]

let imm_4c b = Ok (O.Imm Int64.(of_int (ex b 11 8))) [@@inline]

let imm_4d b = Ok (O.Imm Int64.(of_int (ex b 7 4))) [@@inline]

let imm_4e b = Ok (O.Imm Int64.(of_int (ex b 23 20))) [@@inline]

let imm_4f (_, b) = Ok (O.Imm Int64.(succ (of_int (ex b 4 0)))) [@@inline]

let imm_5a b = Ok (O.Imm Int64.(of_int (ex b 11 7))) [@@inline]

let imm_5b b = Ok (O.Imm Int64.(of_int (ex b 4 0))) [@@inline]

let imm_5c b = Ok (O.Imm Int64.(succ (of_int (ex b 20 16)))) [@@inline]

let imm_5d b = Ok (O.Imm Int64.(of_int (ex b 10 6))) [@@inline]

let imm_5e b =
  let i5 = ex b 10 6 in
  let imm = if i5 = 0 then 32L else Int64.of_int i5 in
  Ok (O.Imm imm)
  [@@inline]

let imm_5f b =
  let imm = ex b 20 16 - ex b 11 7 + 1 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_5g (_, b) =
  let imm = concat (ex b 14 12) (ex b 7 6) 2 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_5h (_, b) = Ok (O.Imm Int64.(of_int (ex b 4 0))) [@@inline]

let imm_7a b =
  let imm = ex b 6 0 lsl 2 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_8a b = Ok (O.Imm Int64.(of_int (ex b 7 0))) [@@inline]

let imm_8b b =
  let imm = ex b 7 0 lsl 2 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_12a b =
  let rot = 2 * ex b 11 8 in
  let imm = ex b 7 0 in
  let imm = if rot = 0 then imm else (imm lsl (32 - rot)) + (imm lsr rot) in
  Ok (O.Imm Int64.(of_int imm))

let imm_12b b =
  let imm = (ex b 19 16 lsl 12) + ex b 11 0 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_12c b = Ok (O.Imm Int64.(of_int (ex b 11 0))) [@@inline]

let imm_12d b =
  let imm = (ex b 19 8 lsl 12) + ex b 3 0 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_12e b = Ok (O.Imm Int64.(of_int (ex b 11 0))) [@@inline]

let imm_12f (b1, b2) =
  let imm =
    concat (concat (tst b1 10 |> Bool.to_int) (ex b2 14 12) 3) (ex b2 7 0) 8
  in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_16a (b1, b2) =
  let i1 = concat (ex b1 3 0) (tst b1 10 |> Bool.to_int) 1 in
  let i2 = concat (ex b2 14 12) (ex b2 7 0) 8 in
  let imm = concat i1 i2 11 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_16b (b1, b2) =
  let imm = concat (ex b1 3 0) (ex b2 11 0) 12 in
  Ok (O.Imm Int64.(of_int imm))
  [@@inline]

let imm_24a b = Ok (O.Imm Int64.(of_int (ex b 23 0))) [@@inline]

let lbl_a b =
  let lbl = ex b 23 0 lsl 2 |> Int64.of_int in
  mem_lbl (sign_ext 26 32 lbl)
  [@@inline]

let lbl_7a b =
  let b9 = tst b 9 |> Bool.to_int in
  let lbl = concat b9 (ex b 7 3) 5 lsl 1 in
  mem_lbl Int64.(of_int lbl)
  [@@inline]

let lbl_8a b =
  let lbl = ex b 7 0 lsl 2 in
  mem_lbl Int64.(of_int lbl)
  [@@inline]

let lbl_9a b =
  let lbl = ex b 7 0 lsl 1 |> Int64.of_int in
  mem_lbl (sign_ext 9 32 lbl)
  [@@inline]

let lbl_12a b =
  let lbl = ex b 10 0 lsl 1 |> Int64.of_int in
  mem_lbl (sign_ext 12 32 lbl)
  [@@inline]

let lbl_24b b =
  let lbl = ex b 23 0 lsl 2 |> Int64.of_int in
  mem_lbl (sign_ext 26 32 lbl)
  [@@inline]

let lbl_21a (b1, b2) =
  let b110 = tst b1 10 |> Bool.to_int in
  let b211 = tst b2 11 |> Bool.to_int in
  let b213 = tst b2 13 |> Bool.to_int in
  let i1 = concat b110 b211 1 in
  let i2 = concat b213 (ex b1 5 0) 6 in
  let i = concat (concat i1 i2 7) (ex b2 10 0 lsl 1) 12 in
  mem_lbl (sign_ext 21 32 Int64.(of_int i))

let lbl_25a (b1, b2) =
  let s = tst b1 10 |> Bool.to_int in
  let b213 = tst b2 13 |> Bool.to_int in
  let b211 = tst b2 11 |> Bool.to_int in
  let i1 = lnot (b213 lxor s) land 1 in
  let i1 = concat s i1 1 in
  let i2 = lnot (b211 lxor s) land 1 in
  let i2 = concat i2 (ex b1 9 0) 10 in
  let i = concat (concat i1 i2 11) (ex b2 10 0 lsl 1) 12 in
  mem_lbl (sign_ext 25 32 Int64.(of_int i))

let lbl_25b (b1, b2) =
  let s = tst b1 10 |> Bool.to_int in
  let b213 = tst b2 13 |> Bool.to_int in
  let b211 = tst b2 11 |> Bool.to_int in
  let i1 = lnot (b213 lxor s) land 1 in
  let i1 = concat s i1 1 in
  let i2 = lnot (b211 lxor s) land 1 in
  let i2 = concat i2 (ex b1 9 0) 10 in
  let i = concat (concat i1 i2 11) (ex b2 10 1 lsl 2) 12 in
  mem_lbl (sign_ext 25 32 Int64.(of_int i))

let lbl_25c (b1, b2) =
  let s = tst b1 10 |> Bool.to_int in
  let b213 = tst b2 13 |> Bool.to_int in
  let b211 = tst b2 11 |> Bool.to_int in
  let i1 = lnot (b213 lxor s) land 1 in
  let i1 = concat s i1 1 in
  let i2 = lnot (b211 lxor s) land 1 in
  let i2 = concat i2 (ex b1 9 0) 10 in
  let i = concat (concat i1 i2 11) (ex b2 10 0 lsl 1) 12 in
  mem_lbl (sign_ext 25 32 Int64.(of_int i))

let lbl_26a b =
  let b24 = tst b 24 |> Bool.to_int in
  let imm = concat (ex b 23 0) b24 1 in
  let imm = Int64.of_int (imm lsl 1) in
  mem_lbl (sign_ext 26 32 imm)

let mem_a b =
  let open Result.Let_syntax in
  let%bind reg = reg b 19 16 in
  mem_off_imm reg false None

let mem_c b =
  let imm = ex b 7 0 lsl 2 |> Int64.of_int in
  mem_off_imm R.SP false (Some imm)
  [@@inline]

let mem_d b =
  let open Result.Let_syntax in
  let%bind r = reg b 5 3 in
  let%bind reg_off = reg b 8 6 in
  mem_off_reg r false reg_off None

let mem_e b =
  let open Result.Let_syntax in
  let%bind r = reg b 5 3 in
  let imm = ex b 10 6 lsl 2 |> Int64.of_int in
  mem_off_imm r false (Some imm)

let mem_f b =
  let open Result.Let_syntax in
  let%bind r = reg b 5 3 in
  let imm = ex b 10 6 |> Int64.of_int in
  mem_off_imm r false (Some imm)

let mem_g b =
  let open Result.Let_syntax in
  let%bind r = reg b 5 3 in
  let imm = ex b 10 6 lsl 1 |> Int64.of_int in
  mem_off_imm r false (Some imm)

let mem_h b =
  let i4h = ex b 11 8 |> Int64.of_int in
  let i4l = ex b 3 0 |> Int64.of_int in
  let imm =
    if tst b 23 then Int64.((i4h lsl 4) + i4l)
    else Int64.(((i4h lsl 4) + i4l) * -1L)
  in
  mem_lbl imm

let mem_i b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  mem_post_reg rn (not (tst b 23)) rm None

let mem_j b =
  let open Result.Let_syntax in
  let i4h = ex b 11 8 |> Int64.of_int in
  let i4l = ex b 3 0 |> Int64.of_int in
  let imm = Int64.((i4h lsl 4) + i4l) in
  let%bind r = reg b 19 16 in
  mem_post_imm r (not (tst b 23)) (Some imm)

let mem_k b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  let imm12 = ex b 11 0 |> Int64.of_int in
  mem_post_imm r (not (tst b 23)) (Some imm12)

let mem_l b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  let imm12 = ex b 11 0 |> Int64.of_int in
  let neg = not (tst b 23) in
  match (tst b 24, tst b 21) with
  | false, _ -> mem_post_imm r neg (Some imm12)
  | true, false -> mem_off_imm r neg (Some imm12)
  | true, true -> mem_pre_imm r neg (Some imm12)

let mem_m b =
  let imm12 = ex b 11 0 |> Int64.of_int in
  match tst b 23 with
  | false -> mem_lbl Int64.(imm12 * -1L)
  | true -> mem_lbl imm12

let mem_n b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let neg = not (tst b 23) in
  match (tst b 24, tst b 21) with
  | false, _ -> mem_post_reg rn neg rm None
  | true, false -> mem_off_reg rn neg rm None
  | true, true -> mem_pre_reg rn neg rm None

let mem_o b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  let i4h = ex b 11 8 |> Int64.of_int in
  let i4l = ex b 3 0 |> Int64.of_int in
  let i8 = Int64.((i4h lsl 4) + i4l) in
  let neg = not (tst b 23) in
  match (tst b 24, tst b 21) with
  | false, _ -> mem_post_imm r neg (Some i8)
  | true, false -> mem_off_imm r neg (Some i8)
  | true, true -> mem_pre_imm r neg (Some i8)

let mem_p (b1, b2) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  let%bind rm = reg b2 3 0 in
  let i = ex b2 5 4 in
  mem_off_reg rn false rm (Some (Sh.LSL, i))

let mem_q b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let imm5 = ex b 11 7 in
  let%bind shift, imm = parse_imm_shift (ex b 6 5) imm5 in
  let neg = not (tst b 23) in
  mem_post_reg rn neg rm (Some (shift, imm))

let mem_r b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let imm5 = ex b 11 7 in
  let%bind shift, imm = parse_imm_shift (ex b 6 5) imm5 in
  let shift_off = Some (shift, imm) in
  let neg = not (tst b 23) in
  match (tst b 24, tst b 21) with
  | false, _ -> mem_post_reg rn neg rm shift_off
  | true, false -> mem_off_reg rn neg rm shift_off
  | true, true -> mem_pre_reg rn neg rm shift_off

let mem_s b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let align =
    match ex b 5 4 with
    | 0b01 -> Some 64L
    | 0b10 -> Some 128L
    | 0b11 -> Some 256L
    | _ -> None
  in
  match rm with
  | R.PC -> mem_off_align rn align None
  | R.SP -> mem_pre_align rn align None
  | _ -> mem_post_align rn align (Some rm)

let mem_t b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let ia = ex b 7 4 in
  let%bind align =
    match ex b 11 10 with
    | 0b00 when not (tst ia 0) -> Ok None
    | 0b01 when ex ia 1 0 = 0b00 -> Ok None
    | 0b01 when ex ia 1 0 = 0b01 -> Ok (Some 16L)
    | 0b10 when ex ia 2 0 = 0b000 -> Ok None
    | 0b10 when ex ia 2 0 = 0b011 -> Ok (Some 32L)
    | _ -> Error `BadOperand
  in
  match rm with
  | R.PC -> mem_off_align rn align None
  | R.SP -> mem_pre_align rn align None
  | _ -> mem_post_align rn align (Some rm)

let mem_u b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let%bind align =
    match (ex b 11 10, tst b 5, tst b 4) with
    | 0b00, _, _ | 0b01, _, false | 0b10, false, false -> Ok None
    | 0b01, _, true -> Ok (Some 32L)
    | 0b10, false, true -> Ok (Some 64L)
    | _ -> Error `BadOperand
  in
  match rm with
  | R.PC -> mem_off_align rn align None
  | R.SP -> mem_pre_align rn align None
  | _ -> mem_post_align rn align (Some rm)

let mem_v b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let%bind align =
    match (ex b 11 10, tst b 5, tst b 4) with
    | 0b00, _, _ | 0b01, _, false | 0b10, false, false -> Ok None
    | _ -> Error `BadOperand
  in
  match rm with
  | R.PC -> mem_off_align rn align None
  | R.SP -> mem_pre_align rn align None
  | _ -> mem_post_align rn align (Some rm)

let mem_w b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let%bind align =
    match (ex b 11 10, tst b 6, tst b 5, tst b 4) with
    | 0b00, _, _, false | 0b01, _, _, false | 0b10, _, false, false ->
        Ok None
    | 0b01, _, _, true | 0b10, _, false, true -> Ok (Some 64L)
    | 0b10, _, true, false -> Ok (Some 128L)
    | 0b00, _, _, true -> Ok (Some 32L)
    | _ -> Error `BadOperand
  in
  match rm with
  | R.PC -> mem_off_align rn align None
  | R.SP -> mem_pre_align rn align None
  | _ -> mem_post_align rn align (Some rm)

let mem_x b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let%bind align =
    match (ex b 7 6, tst b 4) with
    | _, false -> Ok None
    | 0b01, true -> Ok (Some 16L)
    | 0b10, true -> Ok (Some 32L)
    | _ -> Error `BadOperand
  in
  match rm with
  | R.PC -> mem_off_align rn align None
  | R.SP -> mem_pre_align rn align None
  | _ -> mem_post_align rn align (Some rm)

let mem_y b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let%bind align =
    match (ex b 7 6, tst b 4) with
    | _, false -> Ok None
    | 0b00, true -> Ok (Some 16L)
    | 0b01, true -> Ok (Some 32L)
    | 0b10, true -> Ok (Some 64L)
    | _ -> Error `BadOperand
  in
  match rm with
  | R.PC -> mem_off_align rn align None
  | R.SP -> mem_pre_align rn align None
  | _ -> mem_post_align rn align (Some rm)

let mem_z b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  match rm with
  | R.PC -> mem_off_align rn None None
  | R.SP -> mem_pre_align rn None None
  | _ -> mem_post_align rn None (Some rm)

let mem_aa b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let%bind align =
    match (ex b 7 6, tst b 4) with
    | _, false -> Ok None
    | 0b00, true -> Ok (Some 32L)
    | 0b01, true | 0b10, true -> Ok (Some 64L)
    | 0b11, true -> Ok (Some 128L)
    | _ -> Error `BadOperand
  in
  match rm with
  | R.PC -> mem_off_align rn align None
  | R.SP -> mem_pre_align rn align None
  | _ -> mem_post_align rn align (Some rm)

let mem_ab b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  let i = ex b 11 0 |> Int64.of_int in
  mem_off_imm r (not (tst b 23)) (Some i)

let mem_ac b =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rm = reg b 3 0 in
  let imm5 = ex b 11 7 in
  let%bind shift, imm = parse_imm_shift (ex b 6 5) imm5 in
  mem_off_reg rn (not (tst b 23)) rm (Some (shift, imm))

let mem_ad b =
  let i8 = ex b 7 0 |> Int64.of_int in
  match (tst b 24, tst b 21, tst b 23) with
  | true, false, false -> mem_lbl Int64.(i8 * -4L)
  | true, false, true -> mem_lbl Int64.(i8 * 4L)
  | false, false, true -> mem_un R.PC Int64.(i8 * 4L)
  | _ -> Error `BadOperand

let mem_ae b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  let i8 = ex b 7 0 |> Int64.of_int in
  let neg = not (tst b 23) in
  match (tst b 24, tst b 21) with
  | false, false when not neg -> mem_un r i8
  | false, true -> mem_post_imm r neg (Some Int64.(i8 * 4L))
  | true, false -> mem_off_imm r neg (Some Int64.(i8 * 4L))
  | true, true -> mem_pre_imm r neg (Some Int64.(i8 * 4L))
  | _ -> Error `BadOperand

let mem_af (b1, b2) =
  let open Result.Let_syntax in
  let%bind r = reg b1 3 0 in
  let imm = ex b2 7 0 lsl 2 |> Int64.of_int in
  mem_off_imm r false (Some imm)

let mem_ag (b1, b2) =
  let open Result.Let_syntax in
  let%bind r = reg b1 3 0 in
  let imm = ex b2 7 0 |> Int64.of_int in
  mem_off_imm r false (Some imm)

let mem_ah (b1, b2) =
  let open Result.Let_syntax in
  let%bind r = reg b1 3 0 in
  let i8 = ex b2 7 0 lsl 2 |> Int64.of_int in
  let neg = not (tst b1 7) in
  match (tst b1 8, tst b1 5) with
  | false, _ -> mem_post_imm r neg (Some i8)
  | true, false -> mem_off_imm r neg (Some i8)
  | true, true -> mem_pre_imm r neg (Some i8)

let mem_ai (b1, b2) =
  let i8 = ex b2 7 0 lsl 2 |> Int64.of_int in
  if tst b1 7 then mem_lbl i8 else mem_lbl Int64.(i8 * -1L)

let mem_aj (b, _) =
  let open Result.Let_syntax in
  let%bind r = reg b 3 0 in
  mem_off_imm r false None

let mem_ak (b1, b2) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  let%bind rm = reg b2 3 0 in
  mem_off_reg rn false rm None

let mem_al (b1, b2) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  let%bind rm = reg b2 3 0 in
  mem_off_reg rn false rm (Some (Sh.LSL, 1))

let mem_am (b1, b2) =
  let open Result.Let_syntax in
  let%bind r = reg b1 3 0 in
  let i8 = ex b2 7 0 |> Int64.of_int in
  let neg = not (tst b2 9) in
  match (tst b2 10, tst b2 8) with
  | false, false -> Error `BadOperand
  | false, true -> mem_post_imm r neg (Some i8)
  | true, false -> mem_off_imm r neg (Some i8)
  | true, true -> mem_pre_imm r neg (Some i8)

let mem_an (b1, b2) =
  let open Result.Let_syntax in
  let%bind r = reg b1 3 0 in
  let i12 = ex b2 11 0 |> Int64.of_int in
  mem_off_imm r false (Some i12)

let mem_ao (b1, b2) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  let%bind rm = reg b2 3 0 in
  mem_off_reg rn false rm (Some (Sh.LSL, ex b2 5 4))

let mem_ap (b1, b2) =
  let open Result.Let_syntax in
  let%bind r = reg b1 3 0 in
  let i8 = ex b2 7 0 |> Int64.of_int in
  mem_off_imm r true (Some i8)

let mem_aq (b1, b2) =
  let i12 = ex b2 11 0 |> Int64.of_int in
  if tst b1 7 then mem_lbl i12 else mem_lbl Int64.(i12 * -1L)

let mem_ar b =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  let i = ex b 7 0 lsl 2 |> Int64.of_int in
  mem_off_imm r (not (tst b 23)) (Some i)

let flag_a b =
  let open Result.Let_syntax in
  let%map iflg = iflag (ex b 8 6) in
  O.Iflg iflg

let flag_b b =
  let open Result.Let_syntax in
  let%map iflg = iflag (ex b 2 0) in
  O.Iflg iflg

let flag_c (_, b) =
  let open Result.Let_syntax in
  let%map iflg = iflag (ex b 7 5) in
  O.Iflg iflg

let endian_a b = Ok (O.Endian (tst b 9 |> endianness)) [@@inline]

let endian_b b = Ok (O.Endian (tst b 3 |> endianness)) [@@inline]

let opt_a b =
  let open Result.Let_syntax in
  let%map o = opt (ex b 3 0) in
  O.Opt o

let first_cond b =
  let open Result.Let_syntax in
  let%map c = parse_cond (ex b 7 4) in
  O.Cond c

let scalar_a b =
  let open Result.Let_syntax in
  let m = tst b 5 |> Bool.to_int in
  let vm = ex b 3 0 in
  match ex b 21 20 with
  | 0b01 ->
      let%bind r =
        Result.of_option ~error:`BadOperand
          (R.fp_double_reg_of_int (ex vm 2 0))
      in
      simd_sreg r (Some (concat m (tst vm 3 |> Bool.to_int) 1))
  | 0b10 ->
      let%bind r =
        Result.of_option ~error:`BadOperand (R.fp_double_reg_of_int vm)
      in
      simd_sreg r (Some m)
  | _ -> Error `BadOperand

let scalar_b b =
  let open Result.Let_syntax in
  let%bind r =
    Result.of_option ~error:`BadOperand
      (R.fp_double_reg_of_int (concat (tst b 5 |> Bool.to_int) (ex b 3 4) 4))
  in
  match ex b 19 16 with
  | i4 when i4 land 0b0001 = 0b0001 -> simd_sreg r (Some (ex i4 3 1))
  | i4 when i4 land 0b0011 = 0b0010 -> simd_sreg r (Some (ex i4 3 2))
  | i4 when i4 land 0b0111 = 0b0100 ->
      simd_sreg r (Some (tst i4 3 |> Bool.to_int))
  | _ -> Error `BadOperand

let scalar_c b =
  let open Result.Let_syntax in
  let%bind dd =
    Result.of_option ~error:`BadOperand
      (R.fp_double_reg_of_int
         (concat (tst b 7 |> Bool.to_int) (ex b 19 16) 4))
  in
  let%bind x =
    match concat (ex b 22 21) (ex b 6 5) 2 with
    | opc when opc land 0b1000 = 0b1000 ->
        Ok (concat (tst b 21 |> Bool.to_int) (ex b 6 5) 2)
    | opc when opc land 0b1001 = 0b0001 ->
        Ok (concat (tst b 21 |> Bool.to_int) (tst b 6 |> Bool.to_int) 1)
    | opc when opc land 0b1011 = 0b0000 -> Ok (tst b 21 |> Bool.to_int)
    | opc when opc land 0b1011 = 0b0010 -> Error `BadOperand
    | _ -> Error `BadOperand
  in
  simd_sreg dd (Some x)

let scalar_d b =
  let open Result.Let_syntax in
  let%bind dd =
    Result.of_option ~error:`BadOperand
      (R.fp_double_reg_of_int
         (concat (tst b 7 |> Bool.to_int) (ex b 19 16) 4))
  in
  let opc = concat (ex b 22 21) (ex b 6 5) 2 in
  let%bind x =
    match concat (tst b 23 |> Bool.to_int) opc 4 with
    | uopc when uopc land 0b11000 = 0b01000 ->
        Ok (concat (tst b 21 |> Bool.to_int) (ex b 6 5) 2)
    | uopc when uopc land 0b11000 = 0b11000 ->
        Ok (concat (tst b 21 |> Bool.to_int) (ex b 6 5) 2)
    | uopc when uopc land 0b11001 = 0b00001 ->
        Ok (concat (tst b 21 |> Bool.to_int) (tst b 6 |> Bool.to_int) 1)
    | uopc when uopc land 0b11001 = 0b10001 ->
        Ok (concat (tst b 21 |> Bool.to_int) (tst b 6 |> Bool.to_int) 1)
    | uopc when uopc land 0b11011 = 0b00000 -> Ok (tst b 21 |> Bool.to_int)
    | uopc when uopc land 0b11011 = 0b10000 -> Error `BadOperand
    | uopc when uopc land 0b01011 = 0b00010 -> Error `BadOperand
    | _ -> Error `BadOperand
  in
  simd_sreg dd (Some x)

let dummy_chk _ _ = Ok ()

let chk_store_ex1 b (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg_c b in
          match rn with
          | O.Reg R.PC -> Error `Unpredictable
          | _ when O.equal rn o1 || O.equal o1 o2 -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_store_ex2 b (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when tst b 0 -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.LR -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg_c b in
          let%bind o3 = op3 b in
          match rn with
          | O.Reg R.PC -> Error `Unpredictable
          | _ when O.equal o1 rn || O.equal o1 o2 || O.equal o1 o3 ->
              Error `Unpredictable
          | _ -> Ok () ) )

let chk_a b (op1, op2, op3) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 b in
          match o3 with
          | O.Reg R.PC -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_b b (op1, op2, op3, op4) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 b in
          match o3 with
          | O.Reg R.PC -> Error `Unpredictable
          | _ -> (
              let%bind o4 = op4 b in
              match o4 with
              | O.Reg R.PC -> Error `Unpredictable
              | _ -> Ok () ) ) )

let chk_c b (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 b in
          match o3 with
          | O.Reg R.PC -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_d b op =
  let open Result.Let_syntax in
  let%bind o = op b in
  match o with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_e b (op1, op2) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_f b (_, op2) =
  let open Result.Let_syntax in
  let%bind o2 = op2 b in
  match o2 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_g b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_h b (_, op2) =
  let open Result.Let_syntax in
  let%bind o2 = op2 b in
  match o2 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when ex b 19 16 = 0 -> Error `Unpredictable
  | _ -> Ok ()

let chk_i b (op1, op2, op3, op4) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 b in
          match o3 with
          | O.Reg R.PC -> Error `Unpredictable
          | _ -> (
              let%bind o4 = op4 b in
              match o4 with
              | O.Reg R.PC -> Error `Unpredictable
              | _ when O.equal o1 o2 -> Error `Unpredictable
              | _ -> Ok () ) ) )

let chk_j b (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg_c b in
          match rn with
          | O.Reg R.PC -> Error `Unpredictable
          | _ when O.equal rn o1 || O.equal rn o2 -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_k b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      match rn with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_l b (op1, _, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.LR -> Error `Unpredictable
  | _ when tst b 12 -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      match rn with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_m b (op1, _, op3) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o3 = op3 b in
      match o3 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_n b _ =
  let open Result.Let_syntax in
  let%bind rn = reg_c b in
  match rn with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_o b (op1, _, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o3 = op3 b in
      match o3 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_p b (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_q b (op1, op2, _, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_r it b _ =
  let d = concat (tst b 7 |> Bool.to_int) (ex b 2 0) 3 in
  match ex b 6 3 with
  | 15 when d = 15 -> Error `Unpredictable
  | _ when d = 15 -> Error `Unpredictable
  | _ when List.length it > 1 -> Error `Unpredictable
  | _ -> Ok ()

let chk_s b _ =
  let rnd = concat (tst b 7 |> Bool.to_int) (ex b 2 0) 3 in
  let rm = ex b 6 3 in
  match (rnd, rm) with
  | 15, _ | _, 15 -> Error `Unpredictable
  | _ when rnd < 8 && rm < 8 -> Error `Unpredictable
  | _ -> Ok ()

let chk_t b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when Bool.equal (tst b 24) (tst b 21) -> Error `Unpredictable
  | _ -> Ok ()

let chk_u b (_, op2, _) =
  let open Result.Let_syntax in
  let%bind o2 = op2 b in
  match o2 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_v b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      match rn with
      | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 rn -> Error `Unpredictable
      | _ -> (
          let%bind rm = reg_a b in
          match rm with
          | O.Reg R.PC -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_w b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      match rn with
      | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 rn -> Error `Unpredictable
      | _ -> Ok () )

let chk_x b _ =
  match ex b 19 16 with
  | 0 -> Error `Unpredictable
  | _ -> Ok ()

let chk_y b op =
  let open Result.Let_syntax in
  let%bind o = op b in
  match o with
  | O.Reg R.SP -> Error `Unpredictable
  | _ -> Ok ()

let chk_z b (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg_c b in
  let%bind o1 = op1 b in
  match rn with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when O.equal o1 rn -> Error `Unpredictable
  | _ -> Ok ()

let chk_aa b (op1, _) =
  let open Result.Let_syntax in
  match (tst b 24, tst b 21) with
  | false, _ | _, true -> (
      let%bind rn = reg_c b in
      let%bind o1 = op1 b in
      match rn with
      | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 rn -> Error `Unpredictable
      | _ -> Ok () )
  | _ -> Ok ()

let chk_ab b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      match (tst b 24, tst b 21) with
      | (false, _ | _, true) when O.equal rn o1 -> Error `Unpredictable
      | _ -> Ok () )

let chk_ac b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      match (tst b 24, tst b 21) with
      | (false, _ | _, true) when O.equal rn o1 -> Error `Unpredictable
      | (false, _ | _, true) when O.(equal rn (Reg R.PC)) ->
          Error `Unpredictable
      | _ -> Ok () )

let chk_ad b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rm = reg_a b in
      match rm with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg_c b in
          match (tst b 24, tst b 21) with
          | (false, _ | _, true) when O.equal rn o1 -> Error `Unpredictable
          | (false, _ | _, true) when O.(equal rn (Reg R.PC)) ->
              Error `Unpredictable
          | _ -> Ok () ) )

let chk_ae b (op1, op2, _) =
  let open Result.Let_syntax in
  let b1 = tst b 24 in
  let b2 = tst b 21 in
  match (b1, b2) with
  | false, true -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind rm = reg_a b in
          let%bind o1 = op1 b in
          match rm with
          | O.Reg R.PC -> Error `Unpredictable
          | _ when O.equal rm o1 || O.equal rm o2 -> Error `Unpredictable
          | _ -> (
              let%bind rn = reg_c b in
              match (b1, b2) with
              | (false, _ | _, true)
                when O.(equal rn (Reg R.PC))
                     || O.equal rn o1 || O.equal rn o2 ->
                  Error `Unpredictable
              | _ -> Ok () ) ) )

let chk_af b (op1, op2, _) =
  let open Result.Let_syntax in
  let p = tst b 24 in
  let w = tst b 21 in
  match (p, w, tst b 12) with
  | false, true, _ | _, _, true -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind rm = reg_a b in
          match rm with
          | O.Reg R.PC -> Error `Unpredictable
          | _ -> (
              let%bind rn = reg_c b in
              let%bind o1 = op1 b in
              match (p, w) with
              | (false, _ | _, true)
                when O.(equal rn (Reg R.PC))
                     || O.equal rn o1 || O.equal rn o2 ->
                  Error `Unpredictable
              | _ -> Ok () ) ) )

let chk_ag b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      match (tst b 24, tst b 21) with
      | (false, _ | _, true) when O.(equal rn (Reg R.PC)) ->
          Error `Unpredictable
      | (false, _ | _, true) when O.equal rn o1 -> Error `Unpredictable
      | _ -> Ok () )

let chk_ah b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      match (tst b 24, tst b 21) with
      | (false, _ | _, true) when O.equal rn o1 -> Error `Unpredictable
      | _ -> Ok () )

let chk_ai b (op1, op2, _) =
  let open Result.Let_syntax in
  let p = tst b 24 in
  let w = tst b 21 in
  match (p, w) with
  | false, true -> Error `Unpredictable
  | _ -> (
      let%bind o1 = op1 b in
      let%bind o2 = op2 b in
      let%bind rn = reg_c b in
      match (p, w) with
      | (false, _ | _, true) when O.equal rn o1 || O.equal rn o2 ->
          Error `Unpredictable
      | _ -> Ok () )

let chk_aj b (op1, op2, _) =
  let open Result.Let_syntax in
  let p = tst b 24 in
  let w = tst b 21 in
  match (p, w) with
  | false, true -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o1 = op1 b in
          let%bind rn = reg_c b in
          match (p, w) with
          | (false, _ | _, true)
            when O.(equal rn (Reg R.PC)) || O.equal rn o1 || O.equal rn o2 ->
              Error `Unpredictable
          | _ -> Ok () ) )

let chk_ak (_, b2) _ =
  let open Result.Let_syntax in
  let%bind rm = reg_a b2 in
  match rm with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> Ok ()

let chk_al b (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg_c b in
  let%bind o1 = op1 b in
  match rn with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when O.equal rn o1 -> Error `Unpredictable
  | _ -> (
      let%bind rm = reg_a b in
      match rm with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_am b (op1, _) =
  let open Result.Let_syntax in
  let%bind rm = reg_a b in
  match rm with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg_c b in
      let%bind o1 = op1 b in
      match (tst b 21, tst b 21) with
      | (false, _ | _, true) when O.(equal rn (Reg R.PC)) || O.equal rn o1 ->
          Error `Unpredictable
      | _ -> Ok () )

let chk_an b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> chk_am b (op1, ())

let chk_ao b _ =
  let b1 = tst b 22 |> Bool.to_int in
  let b2 = tst b 5 |> Bool.to_int in
  match concat b1 b2 1 with
  | 0b11 -> Error `Unpredictable
  | _ -> Ok ()

let chk_ap b (op1, _, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  let msb = ex b 20 16 in
  let lsb = ex b 11 7 in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when msb < lsb -> Error `Unpredictable
  | _ -> Ok ()

let chk_aq b (op1, _, _, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  let msb = ex b 20 16 in
  let lsb = ex b 11 7 in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when msb < lsb -> Error `Unpredictable
  | _ -> Ok ()

let chk_ar b _ =
  let open Result.Let_syntax in
  let%bind r = reg b 19 16 in
  match r with
  | R.PC -> Error `Unpredictable
  | _ -> (
      let%bind l = reglist (ex b 15 0) in
      match l with
      | [] -> Error `Unpredictable
      | _ -> Ok () )

let chk_as b _ =
  let open Result.Let_syntax in
  let%bind rn = reg b 19 16 in
  let%bind rl = reglist (ex b 15 0) in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> (
    match rl with
    | [] -> Error `Unpredictable
    | _ -> (
      match tst b 21 with
      | true when List.mem rl rn ~equal:R.equal -> Error `Unpredictable
      | _ -> Ok () ) )

let chk_at b _ =
  match tst b 13 with
  | true -> Error `Unpredictable
  | false -> Ok ()

let chk_au b (_, _, op3, op4, _) =
  let open Result.Let_syntax in
  let%bind o3 = op3 b in
  match o3 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o4 = op4 b in
      match o4 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_av b (_, _, op3, op4, _) =
  let open Result.Let_syntax in
  let%bind o3 = op3 b in
  match o3 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o4 = op4 b in
      match o4 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o3 o4 -> Error `Unpredictable
      | _ -> Ok () )

let chk_aw b (op1, _, op3, op4) =
  let open Result.Let_syntax in
  let%bind o3 = op3 b in
  match o3 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o4 = op4 b in
      match o4 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o1 = op1 b in
          match o1 with
          | O.Reg R.S31 -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_ax b (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 b in
          match o3 with
          | O.Reg R.S31 -> Error `Unpredictable
          | _ when O.equal o1 o2 -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_ay b (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 b in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 o2 -> Error `Unpredictable
      | _ -> Ok () )

let chk_az b _ =
  match ex b 7 0 / 2 with
  | 0 -> Error `Unpredictable
  | regs when regs > 16 -> Error `Unpredictable
  | regs -> (
    match concat (tst b 22 |> Bool.to_int) (ex b 15 12) 4 + regs with
    | x when x > 32 -> Error `Unpredictable
    | _ -> Ok () )

let chk_ba b _ =
  match ex b 7 0 with
  | 0 -> Error `Unpredictable
  | imm8 -> (
    match concat (ex b 15 12) (tst b 22 |> Bool.to_int) 1 + imm8 with
    | x when x > 32 -> Error `Unpredictable
    | _ -> Ok () )

let chk_bb b (_, _, op3, _, _, _) =
  let open Result.Let_syntax in
  let%bind o3 = op3 b in
  match o3 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_bc b _ =
  let open Result.Let_syntax in
  let b8 = tst b 8 |> Bool.to_int in
  let%bind rl = reglist ((b8 lsl 14) + ex b 7 0) in
  match rl with
  | [] -> Error `Unpredictable
  | _ -> Ok ()

let chk_bd mnemonic it b op1 =
  let open Result.Let_syntax in
  match List.length it with
  | n when n > 0 -> Error `Unpredictable
  | _ -> (
      let%bind o1 = op1 b in
      match o1 with
      | O.Cond C.UN -> Error `Unpredictable
      | O.Cond C.AL when M.is_it_multiple mnemonic -> Error `Unpredictable
      | _ -> Ok () )

let chk_be it b _ =
  match ex b 11 8 with
  | 14 -> Error `Undefined
  | _ when List.length it > 0 -> Error `Unpredictable
  | _ -> Ok ()

let chk_bf (b1, b2) _ =
  let open Result.Let_syntax in
  let b2_14 = tst b2 14 |> Bool.to_int in
  let%bind rl = reglist (concat (b2_14 lsl 1) (ex b2 12 0) 13) in
  let n = ex b1 3 0 in
  match n with
  | 15 -> Error `Unpredictable
  | _ -> (
    match rl with
    | [] | [_] -> Error `Unpredictable
    | _ -> (
      match (tst b1 5, tst b2 n) with
      | true, true -> Error `Unpredictable
      | _ -> Ok () ) )

let chk_bg it (_, b2) _ =
  let open Result.Let_syntax in
  let pm = ex b2 15 14 in
  let%bind rl = reglist (concat (pm lsl 1) (ex b2 12 0) 13) in
  match rl with
  | [] | [_] -> Error `Unpredictable
  | _ when pm = 0b11 -> Error `Unpredictable
  | _ when tst b2 15 && List.length it > 1 -> Error `Unpredictable
  | _ -> Ok ()

let chk_bh it (b1, b2) _ =
  let open Result.Let_syntax in
  let n = ex b1 3 0 in
  let w = tst b1 5 in
  let pm = ex b2 15 14 in
  let%bind rl = reglist (concat (pm lsl 1) (ex b2 12 0) 13) in
  match n with
  | 15 -> Error `Unpredictable
  | _ when pm = 0b11 -> Error `Unpredictable
  | _ -> (
    match rl with
    | [] | [_] -> Error `Unpredictable
    | _ -> (
      match tst b2 15 with
      | true when List.length it > 1 -> Error `Unpredictable
      | _ when w && tst b2 n -> Error `Unpredictable
      | _ -> Ok () ) )

let chk_bi (_, b2) _ =
  let open Result.Let_syntax in
  let m = tst b2 14 |> Bool.to_int in
  let%bind rl = reglist (concat (m lsl 1) (ex b2 12 0) 13) in
  match rl with
  | [] | [_] -> Error `Unpredictable
  | _ when tst b2 15 || tst b2 13 -> Error `Unpredictable
  | _ -> Ok ()

let chk_bj (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 o2 -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg b1 3 0 in
          match rn with
          | R.PC -> Error `Unpredictable
          | _ when O.(equal o1 (Reg rn)) -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_bk (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rn = reg b1 3 0 in
      match rn with
      | R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_bl (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_bm (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg b1 3 0 in
          match rn with
          | R.PC -> Error `Unpredictable
          | _ when tst b1 5 && O.(equal o1 (Reg rn)) -> Error `Unpredictable
          | _ when tst b1 5 && O.(equal o2 (Reg rn)) -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_bn (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 o2 -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg b1 3 0 in
          match rn with
          | R.PC -> Error `Unpredictable
          | _ when tst b1 5 && O.(equal o1 (Reg rn)) -> Error `Unpredictable
          | _ when tst b1 5 && O.(equal o2 (Reg rn)) -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_bo (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 o2 -> Error `Unpredictable
      | _ -> Ok () )

let chk_bp (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 o2 -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg b1 3 0 in
          match rn with
          | R.PC -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_bq (b1, b2) (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ when O.equal o1 o2 -> Error `Unpredictable
      | _ -> (
          let%bind rn = reg b1 3 0 in
          match rn with
          | R.PC -> Error `Unpredictable
          | _ when O.(equal o1 (Reg rn)) -> Error `Unpredictable
          | _ -> (
              let%bind o3 = op3 (b1, b2) in
              match o3 with
              | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
              | _ -> Ok () ) ) )

let chk_br it (b1, b2) _ =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.SP -> Error `Unpredictable
  | _ -> (
      let%bind rm = reg b2 3 0 in
      match rm with
      | R.SP | R.PC -> Error `Unpredictable
      | _ when List.length it > 1 -> Error `Unpredictable
      | _ -> Ok () )

let chk_bs (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o1 = op1 (b1, b2) in
      match o1 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_bt (b1, b2) (op1, op2) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | O.Reg R.SP when O.equal o1 o2 -> Error `Unpredictable
      | _ -> Ok () )

let chk_bu (b1, b2) (op1, op2) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> Ok () )

let chk_bv (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> Ok () )

let chk_bw (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> Ok () )

let chk_bx (b1, b2) (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 (b1, b2) in
          match o3 with
          | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_by (b1, b2) (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 (b1, b2) in
          match o3 with
          | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_bz (b1, b2) (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 (b1, b2) in
          match o3 with
          | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_ca (b1, b2) (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o2 = op2 (b1, b2) in
  match o2 with
  | O.Reg R.SP -> Ok ()
  | _ -> (
      let%bind o1 = op1 (b1, b2) in
      match o1 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
        match o2 with
        | O.Reg R.PC -> Error `Unpredictable
        | _ -> (
            let%bind o3 = op3 (b1, b2) in
            match o3 with
            | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
            | _ -> Ok () ) ) )

let chk_cb (b1, b2) (op1, op2, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 (b1, b2) in
          match o3 with
          | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_cc (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_cd (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> Ok () )

let chk_ce (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP -> Error `Unpredictable
      | _ -> Ok () )

let chk_cf (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o2 = op2 (b1, b2) in
  match o2 with
  | O.Reg R.SP -> Ok ()
  | _ -> (
      let%bind o1 = op1 (b1, b2) in
      match o1 with
      | O.Reg R.PC when not (tst b1 4) -> Error `Unpredictable
      | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
        match o2 with
        | O.Reg R.PC -> Error `Unpredictable
        | _ -> Ok () ) )

let chk_cg (b1, b2) (op1, op2, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_ch (b1, b2) (op1, op2, op3) =
  let open Result.Let_syntax in
  let%bind o2 = op2 (b1, b2) in
  match o2 with
  | O.Reg R.SP -> Ok ()
  | _ -> (
      let%bind o1 = op1 (b1, b2) in
      match o1 with
      | O.Reg R.PC when not (tst b1 4) -> Error `Unpredictable
      | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
        match o2 with
        | O.Reg R.PC -> Error `Unpredictable
        | _ -> (
            let%bind o3 = op3 (b1, b2) in
            match o3 with
            | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
            | _ -> Ok () ) ) )

let chk_ci (b1, b2) (op1, _, op3, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o3 = op3 (b1, b2) in
      match o3 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_cj (b1, b2) (op1, _, op3) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o3 = op3 (b1, b2) in
      match o3 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_ck (b1, b2) (op1, op2, _, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_cl (b1, b2) (op1, op2, _, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  let msb = ex b2 4 0 in
  let lsb = concat (ex b2 14 12) (ex b2 7 6) 2 in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ when msb < lsb -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.SP -> Error `Unpredictable
      | _ -> Ok () )

let chk_cm (b1, b2) (op1, _, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  let msb = ex b2 4 0 in
  let lsb = concat (ex b2 14 12) (ex b2 7 6) 2 in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ when msb < lsb -> Error `Unpredictable
  | _ -> Ok ()

let chk_cn (b1, b2) (_, op2) =
  let open Result.Let_syntax in
  let%bind o2 = op2 (b1, b2) in
  match o2 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_co (_, b2) _ =
  let open Result.Let_syntax in
  let%bind a = apsr (ex b2 11 10) in
  match a with
  | R.APSR, None -> Error `Unpredictable
  | _ -> Ok ()

let chk_cp (b1, b2) (_, op2) =
  let open Result.Let_syntax in
  let%bind o2 = op2 (b1, b2) in
  match o2 with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when ex b2 11 8 = 0b0000 -> Error `Unpredictable
  | _ -> Ok ()

let chk_cq (b1, b2) op1 =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> Ok ()

let chk_cr (_, b2) _ =
  match tst b2 0 with
  | true -> Error `Unpredictable
  | false -> Ok ()

let chk_cs it (_, b2) _ =
  match (ex b2 4 0, tst b2 8) with
  | x, false when x <> 0 -> Error `Unpredictable
  | _ -> (
    match (tst b2 10, ex b2 7 5) with
    | true, 0 -> Error `Unpredictable
    | false, x when x <> 0 -> Error `Unpredictable
    | _ when List.length it > 0 -> Error `Unpredictable
    | _ when ex b2 10 9 = 1 -> Error `Unpredictable
    | _ -> Ok () )

let chk_ct (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  let%bind o1 = op1 (b1, b2) in
  let w = tst b2 8 in
  match o1 with
  | O.Reg R.SP -> Error `Unpredictable
  | O.Reg R.PC when w -> Error `Unpredictable
  | _ when w && O.(equal o1 (Reg rn)) -> Error `Unpredictable
  | _ -> Ok ()

let chk_cu it (b1, b2) _ =
  let n = ex b1 3 0 in
  let t = ex b2 15 12 in
  match tst b2 8 with
  | true when n = t -> Error `Unpredictable
  | _ when t = 15 && List.length it > 1 -> Error `Unpredictable
  | _ -> Ok ()

let chk_cv (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP -> Error `Unpredictable
  | _ -> Ok ()

let chk_cw (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind rm = reg b2 3 0 in
      match rm with
      | R.SP | R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_cx it (_, b2) _ =
  let open Result.Let_syntax in
  let%bind rm = reg b2 3 0 in
  match rm with
  | R.SP | R.PC -> Error `Unpredictable
  | _ when ex b2 15 12 = 15 && List.length it > 1 -> Error `Unpredictable
  | _ -> Ok ()

let chk_cy (b1, b2) (op1, op2, op3) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 (b1, b2) in
          match o3 with
          | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_cz (b1, b2) (op1, op2) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      let%bind rn = reg b1 3 0 in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ when not O.(equal (Reg rn) o2) -> Error `Unpredictable
      | _ -> Ok () )

let chk_da (b1, b2) (op1, op2, op3, op4) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 (b1, b2) in
          match o3 with
          | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
          | _ -> (
              let%bind o4 = op4 (b1, b2) in
              match o4 with
              | O.Reg R.SP -> Error `Unpredictable
              | _ -> Ok () ) ) )

let chk_db (b1, b2) (op1, op2, op3, op4) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 (b1, b2) in
          match o3 with
          | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
          | _ -> (
              let%bind o4 = op4 (b1, b2) in
              match o4 with
              | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
              | _ -> Ok () ) ) )

let chk_dc (b1, b2) (op1, op2, op3, op4) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
  | _ -> (
      let%bind o2 = op2 (b1, b2) in
      match o2 with
      | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
      | _ when O.equal o1 o2 -> Error `Unpredictable
      | _ -> (
          let%bind o3 = op3 (b1, b2) in
          match o3 with
          | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
          | _ -> (
              let%bind o4 = op4 (b1, b2) in
              match o4 with
              | O.Reg R.PC | O.Reg R.SP -> Error `Unpredictable
              | _ -> Ok () ) ) )

let chk_dd b _ =
  let open Result.Let_syntax in
  let%bind rl = reglist (ex b 7 0) in
  match rl with
  | [] -> Error `Unpredictable
  | _ -> Ok ()

let chk_de it _ _ =
  Result.ok_if_true ~error:`Unpredictable (not (List.length it > 0))

let chk_df it b _ =
  let d = concat (tst b 7 |> Bool.to_int) (ex b 2 0) 3 in
  Result.ok_if_true ~error:`Unpredictable
    (not (d = 15 && List.length it > 1))

let chk_dg it _ _ =
  Result.ok_if_true ~error:`Unpredictable (not (List.length it > 1))

let chk_dh it b op =
  let open Result.Let_syntax in
  let%bind o = op b in
  match o with
  | O.Reg R.PC -> Error `Unpredictable
  | _ when List.length it > 1 -> Error `Unpredictable
  | _ -> Ok ()

let chk_di it b _ =
  match ex b 19 16 with
  | 15 -> Error `Unpredictable
  | _ when List.length it > 1 -> Error `Unpredictable
  | _ -> Ok ()

let chk_dj it (b1, b2) op =
  let open Result.Let_syntax in
  let%bind o = op (b1, b2) in
  match o with
  | O.Reg R.SP -> Error `Unpredictable
  | O.Reg R.PC when List.length it > 1 -> Error `Unpredictable
  | _ -> Ok ()

let chk_dk it b (op, _) =
  let open Result.Let_syntax in
  let%bind o = op b in
  match o with
  | O.Reg R.PC when List.length it > 1 -> Error `Unpredictable
  | _ -> Ok ()

let chk_dl mode b (op1, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 b in
  match o1 with
  | O.Reg R.SP when Mode.is_thumb mode -> Error `Unpredictable
  | _ -> Ok ()

let chk_dm (b1, b2) (op1, _, _) =
  let open Result.Let_syntax in
  let%bind o1 = op1 (b1, b2) in
  match o1 with
  | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
  | _ -> Ok ()

let chk_undef_a q b _ =
  match ex b 21 20 with
  | 0 | 3 -> Error `Undefined
  | _ when q = 1 && (tst b 16 || tst b 12) -> Error `Undefined
  | _ -> Ok ()

let chk_undef_b q b _ =
  match ex b 21 20 with
  | 0 -> Error `Undefined
  | 1 when tst b 8 -> Error `Undefined
  | _ when q = 1 && (tst b 16 || tst b 12) -> Error `Undefined
  | _ -> Ok ()

let chk_undef_c b _ =
  Result.ok_if_true ~error:`Undefined
    ((not (ex b 21 20 = 0)) && not (tst b 12))

let chk_undef_d b _ =
  Result.ok_if_true ~error:`Undefined
    (not (tst b 6 && (tst b 12 || tst b 16 || tst b 0)))

let chk_undef_e b _ =
  match ex b 21 20 with
  | 3 -> Error `Undefined
  | _ -> Ok ()

let chk_undef_f b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_d b () in
  chk_undef_e b ()

let chk_undef_g b _ =
  let open Result.Let_syntax in
  let%bind _ =
    Result.ok_if_true ~error:`Undefined (not ((not (tst b 6)) && tst b 10))
  in
  chk_undef_d b ()

let chk_undef_h b _ =
  Result.ok_if_true ~error:`Undefined
    (not (tst b 6 && (tst b 12 || tst b 0)))

let chk_undef_j b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 21 20 = 3 || tst b 6))

let chk_undef_k b _ =
  let open Result.Let_syntax in
  let size = ex b 21 20 in
  let%bind _ =
    Result.ok_if_true ~error:`Undefined (not (size = 0 || size = 3))
  in
  chk_undef_d b ()

let chk_undef_l b _ =
  let open Result.Let_syntax in
  let%bind _ = Result.ok_if_true ~error:`Undefined (not (tst b 20)) in
  chk_undef_d b ()

let chk_undef_m b _ =
  Result.ok_if_true ~error:`Undefined (not (tst b 20 || tst b 6))

let chk_undef_n b _ =
  Result.ok_if_true ~error:`Undefined (not (tst b 12 && tst b 6))

let chk_undef_o b _ =
  match ex b 3 0 mod 2 with
  | 1 -> Error `Undefined
  | _ -> Ok ()

let chk_undef_p b _ =
  let open Result.Let_syntax in
  let%bind _ = Result.ok_if_true ~error:`Undefined (tst b 21) in
  chk_undef_h b ()

let chk_undef_q b _ =
  Result.ok_if_true ~error:`Undefined
    (not (tst b 12 || (tst b 8 && tst b 16)))

let chk_undef_r b _ =
  Result.ok_if_true ~error:`Undefined (not (tst b 16 || tst b 0))

let chk_undef_s b _ = Result.ok_if_true ~error:`Undefined (not (tst b 12))

let chk_undef_t b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 21 20 = 0 || tst b 12))

let chk_undef_u b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined
    (not ((not (tst b 6)) && ex b 8 7 + ex b 19 18 >= 3))

let chk_undef_v b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined (not (ex b 19 18 = 0b11))

let chk_undef_w b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined (not (ex b 19 18 <> 0b10))

let chk_undef_x b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined
    (not ((not (tst b 6)) && ex b 19 18 = 0b11))

let chk_undef_y b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined
    (not ((not (tst b 6)) && ex b 19 18 <> 0b00))

let chk_undef_z b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined (not (ex b 19 18 <> 0b00))

let chk_undef_aa b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined (not (ex b 19 18 = 0b11))

let chk_undef_ab b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined
    (not (ex b 19 18 = 0b11 || ((not (tst b 6)) && ex b 19 18 = 0b10)))

let chk_undef_ac b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined
    (not (ex b 19 18 = 0b11 || (tst b 10 && ex b 19 18 <> 0b10)))

let chk_undef_ad b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 19 18 = 3 || tst b 0))

let chk_undef_ae b _ =
  let op = tst b 8 in
  Result.ok_if_true ~error:`Undefined
    (not (ex b 19 18 <> 1 || (op && tst b 12) || ((not op) && tst b 0)))

let chk_undef_af b _ =
  let open Result.Let_syntax in
  let%bind _ = chk_undef_h b () in
  Result.ok_if_true ~error:`Undefined (not (ex b 19 18 <> 0b10))

let chk_undef_ag b _ =
  let q = tst b 6 in
  let i = ex b 19 16 in
  Result.ok_if_true ~error:`Undefined
    (not (((not q) && (i = 0 || i = 8)) || (q && tst b 12)))

let chk_undef_ah b _ =
  let typ = ex b 11 8 in
  let align = ex b 5 4 in
  Result.ok_if_true ~error:`Undefined
    (not
       ( (typ = 0b0111 && tst align 1)
       || (typ = 0b1010 && align = 0b11)
       || (typ = 0b0110 && tst align 1) ))

let chk_undef_ai b _ =
  let typ = ex b 11 8 in
  let align = ex b 5 4 in
  Result.ok_if_true ~error:`Undefined
    (not
       ( ex b 7 6 = 0b11
       || (typ = 0b1000 && align = 0b11)
       || (typ = 0b1001 && align = 0b11) ))

let chk_undef_aj b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 7 6 = 3 || tst b 5))

let chk_undef_ak b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 7 6 = 0b11))

let chk_undef_al b _ =
  let size = ex b 11 10 in
  let ia = ex b 7 4 in
  Result.ok_if_true ~error:`Undefined
    (not
       ( (size = 0b00 && tst ia 0)
       || (size = 0b01 && tst ia 1)
       || (size = 0b10 && tst ia 2)
       || (size = 0b10 && ex ia 1 0 = 0b01)
       || (size = 0b10 && ex ia 1 0 = 0b10) ))

let chk_undef_am b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 11 10 = 0b10 && tst b 5))

let chk_undef_an b _ =
  let size = ex b 11 10 in
  let ia = ex b 7 4 in
  Result.ok_if_true ~error:`Undefined
    (not
       ( (size = 0b00 && tst ia 0)
       || (size = 0b01 && tst ia 0)
       || (size = 0b10 && ex ia 1 0 <> 0b00) ))

let chk_undef_ao b _ =
  Result.ok_if_true ~error:`Undefined
    (not (ex b 11 10 = 0b10 && ex b 5 4 = 0b11))

let chk_undef_ap b _ =
  let size = ex b 7 6 in
  Result.ok_if_true ~error:`Undefined
    (not (size = 0b11 || (size = 0b00 && tst b 4)))

let chk_undef_aq b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 7 6 = 0b11))

let chk_undef_ar b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 7 6 = 3 || tst b 4))

let chk_undef_as b _ =
  Result.ok_if_true ~error:`Undefined (not (ex b 7 6 = 3 && not (tst b 4)))

let chk_undef_at b _ =
  Result.ok_if_true ~error:`Undefined
    (not (tst b 12 || tst b 0 || ex b 19 18 <> 0))

let chk_undef_au b _ =
  Result.ok_if_true ~error:`Undefined
    (not (tst b 12 || tst b 0 || ex b 19 18 <> 2))

let chk_both_a (b1, b2) (op1, op2) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> chk_bl (b1, b2) (op1, op2)

let chk_both_b (b1, b2) (op1, op2, op3, op4) =
  let open Result.Let_syntax in
  let%bind _ = chk_bx (b1, b2) (op1, op2, op3, op4) in
  Result.ok_if_true ~error:`Undefined (not (tst b1 4 || tst b2 4))

let chk_both_c (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o1 = op1 (b1, b2) in
      match o1 with
      | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
      | _ when tst b2 8 && O.(equal o1 (Reg rn)) -> Error `Unpredictable
      | _ -> Ok () )

let chk_both_d (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o1 = op1 (b1, b2) in
      match o1 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ when tst b2 8 && O.(equal o1 (Reg rn)) -> Error `Unpredictable
      | _ -> Ok () )

let chk_both_e (b1, b2) (op1, op2) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> chk_bl (b1, b2) (op1, op2)

let chk_both_f (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> (
      let%bind o1 = op1 (b1, b2) in
      match o1 with
      | O.Reg R.PC -> Error `Unpredictable
      | _ -> Ok () )

let chk_both_g (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rm = reg b2 3 0 in
      match rm with
      | R.SP | R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o1 = op1 (b1, b2) in
          match o1 with
          | O.Reg R.SP | O.Reg R.PC -> Error `Unpredictable
          | _ -> Ok () ) )

let chk_both_h (b1, b2) (op1, _) =
  let open Result.Let_syntax in
  let%bind rn = reg b1 3 0 in
  match rn with
  | R.PC -> Error `Unpredictable
  | _ -> (
      let%bind rm = reg b2 3 0 in
      match rm with
      | R.SP | R.PC -> Error `Unpredictable
      | _ -> (
          let%bind o1 = op1 (b1, b2) in
          match o1 with
          | O.Reg R.PC -> Error `Unpredictable
          | _ -> Ok () ) )

let one_dt dt = Some (S.One dt)

let two_dt dt1 dt2 = Some (S.Two (dt1, dt2))

let one_dt_a b =
  let open Result.Let_syntax in
  let%map dt = signed_size (ex b 21 20) in
  one_dt dt

let one_dt_b b =
  let open Result.Let_syntax in
  let%map dt = integer_size_f (ex b 21 20) (tst b 8) in
  one_dt dt

let one_dt_c b =
  let open Result.Let_syntax in
  let%map dt = signedness_size_u (ex b 19 18) (tst b 7) in
  one_dt dt

let one_dt_d u b =
  let open Result.Let_syntax in
  let%map dt = signedness_size_u (ex b 21 20) u in
  one_dt dt

let one_dt_e () = Ok (one_dt S.Any8)

let one_dt_f b =
  let open Result.Let_syntax in
  let%map dt = integer_size (ex b 21 20) in
  one_dt dt

let one_dt_g b = Ok (one_dt (float_size (tst b 20)))

let one_dt_h b =
  let open Result.Let_syntax in
  let%map dt =
    match concat (tst b 5 |> Bool.to_int) (ex b 11 9) 3 with
    | r when r land 0b0100 = 0b0000 -> Ok S.I32
    | r when r land 0b0111 = 0b0110 -> Ok S.I32
    | r when r land 0b0110 = 0b0100 -> Ok S.I16
    | 0b1111 when not (tst b 8) -> Ok S.I64
    | 0b0111 when not (tst b 8) -> Ok S.I8
    | 0b0111 when tst b 8 -> Ok S.F32
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_i b =
  let open Result.Let_syntax in
  let b1 = tst b 22 |> Bool.to_int in
  let b2 = tst b 5 |> Bool.to_int in
  let%map dt =
    match concat b1 b2 1 with
    | 0b00 -> Ok S.Any32
    | 0b01 -> Ok S.Any16
    | 0b10 -> Ok S.Any8
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_j u b =
  let open Result.Let_syntax in
  let b1 = tst b 7 |> Bool.to_int in
  let%map dt =
    match (u, concat b1 (ex b 21 19) 3) with
    | 0, 1 -> Ok S.S8
    | 1, 1 -> Ok S.U8
    | 0, i when i land 0b1110 = 0b0010 -> Ok S.S16
    | 1, i when i land 0b1110 = 0b0010 -> Ok S.U16
    | 0, i when i land 0b1100 = 0b0100 -> Ok S.S32
    | 1, i when i land 0b1100 = 0b0100 -> Ok S.U32
    | 0, i when i land 0b1000 = 0b1000 -> Ok S.S64
    | 1, i when i land 0b1000 = 0b1000 -> Ok S.U64
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_k b =
  let open Result.Let_syntax in
  let b1 = tst b 7 |> Bool.to_int in
  let%map dt =
    match concat b1 (ex b 21 19) 3 with
    | 1 -> Ok S.Any8
    | i when i land 0b1110 = 0b0010 -> Ok S.Any16
    | i when i land 0b1100 = 0b0100 -> Ok S.Any32
    | i when i land 0b1000 = 0b1000 -> Ok S.Any64
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_l b =
  let open Result.Let_syntax in
  let b1 = tst b 7 |> Bool.to_int in
  let%map dt =
    match concat b1 (ex b 21 19) 3 with
    | i when i land 0b1111 = 0b0001 -> Ok S.I8
    | i when i land 0b1110 = 0b0010 -> Ok S.I16
    | i when i land 0b1100 = 0b0100 -> Ok S.I32
    | i when i land 0b1000 = 0b1000 -> Ok S.I64
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_m b =
  let open Result.Let_syntax in
  let%map dt =
    match ex b 21 19 with
    | 1 -> Ok S.I16
    | i when i land 0b110 = 0b010 -> Ok S.I32
    | i when i land 0b100 = 0b100 -> Ok S.I64
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_n b =
  let open Result.Let_syntax in
  let%map dt =
    match ex b 21 19 with
    | 1 -> Ok S.S16
    | i when i land 0b110 = 0b010 -> Ok S.S32
    | i when i land 0b100 = 0b100 -> Ok S.S64
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_o u b =
  let open Result.Let_syntax in
  let%map dt =
    match (u, ex b 21 19) with
    | 0, i when i land 0b111 = 0b001 -> Ok S.S16
    | 1, i when i land 0b111 = 0b001 -> Ok S.U16
    | 0, i when i land 0b110 = 0b010 -> Ok S.S32
    | 1, i when i land 0b110 = 0b010 -> Ok S.U32
    | 0, i when i land 0b100 = 0b100 -> Ok S.S64
    | 1, i when i land 0b100 = 0b100 -> Ok S.U64
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_p u b =
  let open Result.Let_syntax in
  let%map dt =
    match (u, ex b 21 19) with
    | 0, i when i land 0b111 = 0b001 -> Ok S.S8
    | 1, i when i land 0b111 = 0b001 -> Ok S.U8
    | 0, i when i land 0b110 = 0b010 -> Ok S.S16
    | 1, i when i land 0b110 = 0b010 -> Ok S.U16
    | 0, i when i land 0b100 = 0b100 -> Ok S.S32
    | 1, i when i land 0b100 = 0b100 -> Ok S.U32
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_q b =
  let open Result.Let_syntax in
  let%map dt = integer_size2 (ex b 21 20) in
  one_dt dt

let one_dt_r u b =
  let open Result.Let_syntax in
  let%map dt =
    match (tst b 9, u, ex b 21 20) with
    | false, 0, 0b00 -> Ok S.S8
    | false, 0, 0b01 -> Ok S.S16
    | false, 0, 0b10 -> Ok S.S32
    | false, 1, 0b00 -> Ok S.U8
    | false, 1, 0b01 -> Ok S.U16
    | false, 1, 0b10 -> Ok S.U32
    | true, 0, 0b00 -> Ok S.P8
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_s b =
  let open Result.Let_syntax in
  let%map dt = any_size (ex b 19 18) in
  one_dt dt

let one_dt_t b =
  let open Result.Let_syntax in
  let%map dt = signed_size (ex b 19 18) in
  one_dt dt

let one_dt_u b =
  let open Result.Let_syntax in
  let%map dt = integer_size (ex b 19 18) in
  one_dt dt

let one_dt_v b =
  let open Result.Let_syntax in
  let%map dt =
    match (ex b 19 18, tst b 10) with
    | 0b00, false -> Ok S.S8
    | 0b01, false -> Ok S.S16
    | 0b10, false -> Ok S.S32
    | 0b10, true -> Ok S.F32
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_w b =
  let open Result.Let_syntax in
  let%map dt = integer_size_f (ex b 19 18) (tst b 10) in
  one_dt dt

let one_dt_x b =
  let open Result.Let_syntax in
  let%map dt = integer_size2 (ex b 19 18) in
  one_dt dt

let one_dt_y b =
  let open Result.Let_syntax in
  let%map dt =
    match (ex b 7 6, ex b 19 18) with
    | 0b01, 0b00 -> Ok S.S16
    | 0b01, 0b01 -> Ok S.S32
    | 0b01, 0b10 -> Ok S.S64
    | 0b11, 0b00 -> Ok S.U16
    | 0b11, 0b01 -> Ok S.U32
    | 0b11, 0b10 -> Ok S.U64
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_z b =
  let open Result.Let_syntax in
  let%map dt =
    match (ex b 19 18, tst b 8) with
    | 0b10, false -> Ok S.U32
    | 0b10, true -> Ok S.F32
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_aa () = Ok (one_dt S.F32)

let one_dt_ab b =
  let open Result.Let_syntax in
  let%map dt =
    match ex b 19 16 with
    | i4 when i4 land 0b0001 = 0b0001 -> Ok S.Any8
    | i4 when i4 land 0b0011 = 0b0010 -> Ok S.Any16
    | i4 when i4 land 0b0111 = 0b0100 -> Ok S.Any32
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_ac b =
  let open Result.Let_syntax in
  let%map dt = any_size (ex b 7 6) in
  one_dt dt

let one_dt_ad b =
  let open Result.Let_syntax in
  let%map dt = any_size (ex b 11 10) in
  one_dt dt

let one_dt_ae b =
  let open Result.Let_syntax in
  let%map dt = vld4_size (ex b 7 6) in
  one_dt dt

let one_dt_af b = Ok (one_dt (float_size (tst b 8)))

let one_dt_ag b =
  let open Result.Let_syntax in
  let%map dt =
    match concat (ex b 22 21) (ex b 6 5) 2 with
    | opc when opc land 0b1000 = 0b1000 -> Ok S.Any8
    | opc when opc land 0b1001 = 0b0001 -> Ok S.Any16
    | opc when opc land 0b1011 = 0b0000 -> Ok S.Any32
    | opc when opc land 0b1011 = 0b0010 -> Error `Invalid
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_ah b =
  let open Result.Let_syntax in
  let opc = concat (ex b 22 21) (ex b 6 5) 2 in
  let%map dt =
    match concat (tst b 32 |> Bool.to_int) opc 4 with
    | o when o land 0b11000 = 0b01000 -> Ok S.S8
    | o when o land 0b11000 = 0b11000 -> Ok S.U8
    | o when o land 0b11001 = 0b00001 -> Ok S.S16
    | o when o land 0b11001 = 0b10001 -> Ok S.U16
    | o when o land 0b11011 = 0b00000 -> Ok S.Any32
    | o when o land 0b11011 = 0b10000 -> Error `Invalid
    | o when o land 0b11011 = 0b00010 -> Error `Invalid
    | _ -> Error `Invalid
  in
  one_dt dt

let one_dt_ai () = Ok (one_dt S.Any32)

let qf_w () = Some Q.W

let qf_n () = Some Q.N

let two_dt_a u b =
  let open Result.Let_syntax in
  let%map dt1, dt2 =
    match (u, tst b 8) with
    | 0, true -> Ok (S.S32, S.F32)
    | 1, true -> Ok (S.U32, S.F32)
    | 0, false -> Ok (S.F32, S.S32)
    | 1, false -> Ok (S.F32, S.U32)
    | _ -> Error `Invalid
  in
  two_dt dt1 dt2

let two_dt_b b =
  let open Result.Let_syntax in
  let%map dt1, dt2 =
    match (ex b 8 7, ex b 19 18) with
    | 0b10, 0b10 -> Ok (S.S32, S.F32)
    | 0b11, 0b10 -> Ok (S.U32, S.F32)
    | 0b00, 0b10 -> Ok (S.F32, S.S32)
    | 0b01, 0b10 -> Ok (S.F32, S.U32)
    | _ -> Error `Invalid
  in
  two_dt dt1 dt2

let two_dt_c b =
  match tst b 8 with
  | false -> Ok (two_dt S.F16 S.F32)
  | true -> Ok (two_dt S.F32 S.F16)

let two_dt_d b =
  match tst b 8 with
  | false -> Ok (two_dt S.F64 S.F32)
  | true -> Ok (two_dt S.F32 S.F64)

let two_dt_e b =
  match tst b 16 with
  | false -> Ok (two_dt S.F32 S.F16)
  | true -> Ok (two_dt S.F16 S.F32)

let two_dt_f b =
  let open Result.Let_syntax in
  let%map dt1, dt2 =
    match (ex b 18 16, tst b 8) with
    | 0b101, true -> Ok (S.S32, S.F64)
    | 0b101, false -> Ok (S.S32, S.F32)
    | 0b100, true -> Ok (S.U32, S.F64)
    | 0b100, false -> Ok (S.U32, S.F32)
    | 0b000, true -> Ok (S.F64, signedness_size_32 (tst b 7))
    | 0b000, false -> Ok (S.F32, signedness_size_32 (tst b 7))
    | _ -> Error `Invalid
  in
  two_dt dt1 dt2

let two_dt_g b =
  let open Result.Let_syntax in
  let%map dt1, dt2 =
    match (ex b 18 16, tst b 8) with
    | 0b101, true -> Ok (S.S32, S.F64)
    | 0b101, false -> Ok (S.S32, S.F32)
    | 0b100, true -> Ok (S.U32, S.F64)
    | 0b100, false -> Ok (S.U32, S.F32)
    | _ -> Error `Invalid
  in
  two_dt dt1 dt2

let two_dt_h b =
  let u = tst b 16 in
  let sx = tst b 7 in
  let dt1, dt2 =
    match (tst b 18, tst b 8) with
    | true, true -> (signedness_size_unsx u sx, S.F64)
    | true, false -> (signedness_size_unsx u sx, S.F32)
    | false, true -> (S.F64, signedness_size_unsx u sx)
    | false, false -> (S.F32, signedness_size_unsx u sx)
  in
  Ok (two_dt dt1 dt2)

let two_dt_i b =
  let dt1, dt2 =
    match (tst b 16, tst b 8) with
    | false, false -> (S.F32, S.F16)
    | true, false -> (S.F16, S.F32)
    | false, true -> (S.F64, S.F16)
    | true, true -> (S.F16, S.F64)
  in
  Ok (two_dt dt1 dt2)

let two_dt_j b =
  let dt =
    match tst b 8 with
    | false -> S.F32
    | true -> S.F64
  in
  Ok (two_dt dt dt)

let two_dt_k b =
  let dt1, dt2 =
    match (tst b 7, tst b 8) with
    | false, false -> (S.U32, S.F32)
    | false, true -> (S.U32, S.F64)
    | true, false -> (S.S32, S.F32)
    | true, true -> (S.S32, S.F64)
  in
  Ok (two_dt dt1 dt2)

let rr_rs_sca q = (reg_r q, reg_s q, scalar_a)

let rx_ia mnemonic i = (reg_x, imm_a mnemonic i)

let parse_mul_n_mul_acc b =
  let open Result.Let_syntax in
  match ex b 23 20 with
  | 0b0000 ->
      let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
      (M.MUL, None, ops)
  | 0b0001 ->
      let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
      (M.MULS, None, ops)
  | 0b0010 ->
      let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
      (M.MLA, None, ops)
  | 0b0011 ->
      let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
      (M.MLAS, None, ops)
  | 0b0100 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.UMAAL, None, ops)
  | 0b0101 -> Error `Invalid
  | 0b0110 ->
      let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
      (M.MLS, None, ops)
  | 0b0111 -> Error `Invalid
  | 0b1000 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.UMULL, None, ops)
  | 0b1001 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.UMULLS, None, ops)
  | 0b1010 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.UMLAL, None, ops)
  | 0b1011 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.UMLALS, None, ops)
  | 0b1100 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.SMULL, None, ops)
  | 0b1101 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.SMULLS, None, ops)
  | 0b1110 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.SMLAL, None, ops)
  | 0b1111 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.SMLALS, None, ops)
  | _ -> Error `Invalid

let parse_half_mul_n_mul_acc b =
  let open Result.Let_syntax in
  match concat (ex b 22 21) (ex b 6 5) 2 with
  | 0b0000 ->
      let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
      (M.SMLABB, None, ops)
  | 0b0001 ->
      let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
      (M.SMLATB, None, ops)
  | 0b0010 ->
      let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
      (M.SMLABT, None, ops)
  | 0b0011 ->
      let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
      (M.SMLATT, None, ops)
  | 0b0100 ->
      let%map ops = p4oprs b chk_b (reg_c, reg_a, reg_b, reg_d) in
      (M.SMLAWB, None, ops)
  | 0b0101 ->
      let%map ops = p3oprs b dummy_chk (reg_d, reg_a, reg_b) in
      (M.SMULWB, None, ops)
  | 0b0111 ->
      let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
      (M.SMULWT, None, ops)
  | 0b1000 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.SMLALBB, None, ops)
  | 0b1001 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.SMLALTB, None, ops)
  | 0b1010 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.SMLALBT, None, ops)
  | 0b1011 ->
      let%map ops = p4oprs b chk_i (reg_d, reg_c, reg_a, reg_b) in
      (M.SMLALTT, None, ops)
  | 0b1100 ->
      let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
      (M.SMULBB, None, ops)
  | 0b1101 ->
      let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
      (M.SMULBT, None, ops)
  | 0b1110 ->
      let%map ops = p3oprs b chk_a (reg_c, reg_a, reg_b) in
      (M.SMULTT, None, ops)
  | _ -> Error `Invalid

let cps b =
  let open Result.Let_syntax in
  match (ex b 19 18, tst b 17) with
  | 0, false -> Error `Invalid
  | 0, true ->
      let%map ops = p1opr b dummy_chk imm_5b in
      (M.CPS, ops)
  | 1, _ -> Error `Invalid
  | 2, false ->
      let%map ops = p1opr b dummy_chk flag_a in
      (M.CPSIE, ops)
  | 2, true ->
      let%map ops = p2oprs b dummy_chk (flag_a, imm_5b) in
      (M.CPSIE, ops)
  | 3, false ->
      let%map ops = p1opr b dummy_chk flag_a in
      (M.CPSID, ops)
  | 3, true ->
      let%map ops = p2oprs b dummy_chk (flag_a, imm_5b) in
      (M.CPSID, ops)
  | _ -> Error `Invalid
