open Base

module Kind = struct
  type t = GP | FP | WR | Fcc | Accum | PC
end

type t =
  | ZERO
  | AT
  | V0
  | V1
  | A0
  | A1
  | A2
  | A3
  | T0
  | T1
  | T2
  | T3
  | T4
  | T5
  | T6
  | T7
  | S0
  | S1
  | S2
  | S3
  | S4
  | S5
  | S6
  | S7
  | T8
  | T9
  | K0
  | K1
  | GP
  | SP
  | FP
  | RA
  | F0
  | F1
  | F2
  | F3
  | F4
  | F5
  | F6
  | F7
  | F8
  | F9
  | F10
  | F11
  | F12
  | F13
  | F14
  | F15
  | F16
  | F17
  | F18
  | F19
  | F20
  | F21
  | F22
  | F23
  | F24
  | F25
  | F26
  | F27
  | F28
  | F29
  | F30
  | F31
  | W0
  | W1
  | W2
  | W3
  | W4
  | W5
  | W6
  | W7
  | W8
  | W9
  | W10
  | W11
  | W12
  | W13
  | W14
  | W15
  | W16
  | W17
  | W18
  | W19
  | W20
  | W21
  | W22
  | W23
  | W24
  | W25
  | W26
  | W27
  | W28
  | W29
  | W30
  | W31
  | FCC0
  | FCC1
  | FCC2
  | FCC3
  | FCC4
  | FCC5
  | FCC6
  | FCC7
  | HI
  | LO
  | PC
[@@deriving equal]

let gp_reg_of_int = function
  | 0 -> Some ZERO
  | 1 -> Some AT
  | 2 -> Some V0
  | 3 -> Some V1
  | 4 -> Some A0
  | 5 -> Some A1
  | 6 -> Some A2
  | 7 -> Some A3
  | 8 -> Some T0
  | 9 -> Some T1
  | 10 -> Some T2
  | 11 -> Some T3
  | 12 -> Some T4
  | 13 -> Some T5
  | 14 -> Some T6
  | 15 -> Some T7
  | 16 -> Some S0
  | 17 -> Some S1
  | 18 -> Some S2
  | 19 -> Some S3
  | 20 -> Some S4
  | 21 -> Some S5
  | 22 -> Some S6
  | 23 -> Some S7
  | 24 -> Some T8
  | 25 -> Some T9
  | 26 -> Some K0
  | 27 -> Some K1
  | 28 -> Some GP
  | 29 -> Some SP
  | 30 -> Some FP
  | 31 -> Some RA
  | _ -> None

let int_of_gp_reg = function
  | ZERO -> Some 0
  | AT -> Some 1
  | V0 -> Some 2
  | V1 -> Some 3
  | A0 -> Some 4
  | A1 -> Some 5
  | A2 -> Some 6
  | A3 -> Some 7
  | T0 -> Some 8
  | T1 -> Some 9
  | T2 -> Some 10
  | T3 -> Some 11
  | T4 -> Some 12
  | T5 -> Some 13
  | T6 -> Some 14
  | T7 -> Some 15
  | S0 -> Some 16
  | S1 -> Some 17
  | S2 -> Some 18
  | S3 -> Some 19
  | S4 -> Some 20
  | S5 -> Some 21
  | S6 -> Some 22
  | S7 -> Some 23
  | T8 -> Some 24
  | T9 -> Some 25
  | K0 -> Some 26
  | K1 -> Some 27
  | GP -> Some 28
  | SP -> Some 29
  | FP -> Some 30
  | RA -> Some 31
  | _ -> None

let fp_reg_of_int = function
  | 0 -> Some F0
  | 1 -> Some F1
  | 2 -> Some F2
  | 3 -> Some F3
  | 4 -> Some F4
  | 5 -> Some F5
  | 6 -> Some F6
  | 7 -> Some F7
  | 8 -> Some F8
  | 9 -> Some F9
  | 10 -> Some F10
  | 11 -> Some F11
  | 12 -> Some F12
  | 13 -> Some F13
  | 14 -> Some F14
  | 15 -> Some F15
  | 16 -> Some F16
  | 17 -> Some F17
  | 18 -> Some F18
  | 19 -> Some F19
  | 20 -> Some F20
  | 21 -> Some F21
  | 22 -> Some F22
  | 23 -> Some F23
  | 24 -> Some F24
  | 25 -> Some F25
  | 26 -> Some F26
  | 27 -> Some F27
  | 28 -> Some F28
  | 29 -> Some F29
  | 30 -> Some F30
  | 31 -> Some F31
  | _ -> None

let int_of_fp_reg = function
  | F0 -> Some 0
  | F1 -> Some 1
  | F2 -> Some 2
  | F3 -> Some 3
  | F4 -> Some 4
  | F5 -> Some 5
  | F6 -> Some 6
  | F7 -> Some 7
  | F8 -> Some 8
  | F9 -> Some 9
  | F10 -> Some 10
  | F11 -> Some 11
  | F12 -> Some 12
  | F13 -> Some 13
  | F14 -> Some 14
  | F15 -> Some 15
  | F16 -> Some 16
  | F17 -> Some 17
  | F18 -> Some 18
  | F19 -> Some 19
  | F20 -> Some 20
  | F21 -> Some 21
  | F22 -> Some 22
  | F23 -> Some 23
  | F24 -> Some 24
  | F25 -> Some 25
  | F26 -> Some 26
  | F27 -> Some 27
  | F28 -> Some 28
  | F29 -> Some 29
  | F30 -> Some 30
  | F31 -> Some 31
  | _ -> None

let wr_reg_of_int = function
  | 0 -> Some W0
  | 1 -> Some W1
  | 2 -> Some W2
  | 3 -> Some W3
  | 4 -> Some W4
  | 5 -> Some W5
  | 6 -> Some W6
  | 7 -> Some W7
  | 8 -> Some W8
  | 9 -> Some W9
  | 10 -> Some W10
  | 11 -> Some W11
  | 12 -> Some W12
  | 13 -> Some W13
  | 14 -> Some W14
  | 15 -> Some W15
  | 16 -> Some W16
  | 17 -> Some W17
  | 18 -> Some W18
  | 19 -> Some W19
  | 20 -> Some W20
  | 21 -> Some W21
  | 22 -> Some W22
  | 23 -> Some W23
  | 24 -> Some W24
  | 25 -> Some W25
  | 26 -> Some W26
  | 27 -> Some W27
  | 28 -> Some W28
  | 29 -> Some W29
  | 30 -> Some W30
  | 31 -> Some W31
  | _ -> None

let int_of_wr_reg = function
  | W0 -> Some 0
  | W1 -> Some 1
  | W2 -> Some 2
  | W3 -> Some 3
  | W4 -> Some 4
  | W5 -> Some 5
  | W6 -> Some 6
  | W7 -> Some 7
  | W8 -> Some 8
  | W9 -> Some 9
  | W10 -> Some 10
  | W11 -> Some 11
  | W12 -> Some 12
  | W13 -> Some 13
  | W14 -> Some 14
  | W15 -> Some 15
  | W16 -> Some 16
  | W17 -> Some 17
  | W18 -> Some 18
  | W19 -> Some 19
  | W20 -> Some 20
  | W21 -> Some 21
  | W22 -> Some 22
  | W23 -> Some 23
  | W24 -> Some 24
  | W25 -> Some 25
  | W26 -> Some 26
  | W27 -> Some 27
  | W28 -> Some 28
  | W29 -> Some 29
  | W30 -> Some 30
  | W31 -> Some 31
  | _ -> None

let fcc_reg_of_int = function
  | 0 -> Some FCC0
  | 1 -> Some FCC1
  | 2 -> Some FCC2
  | 3 -> Some FCC3
  | 4 -> Some FCC4
  | 5 -> Some FCC5
  | 6 -> Some FCC6
  | 7 -> Some FCC7
  | _ -> None

let int_of_fcc_reg = function
  | FCC0 -> Some 0
  | FCC1 -> Some 1
  | FCC2 -> Some 2
  | FCC3 -> Some 3
  | FCC4 -> Some 4
  | FCC5 -> Some 5
  | FCC6 -> Some 6
  | FCC7 -> Some 7
  | _ -> None

let to_string = function
  | ZERO -> "ZERO"
  | AT -> "AT"
  | V0 -> "V0"
  | V1 -> "V1"
  | A0 -> "A0"
  | A1 -> "A1"
  | A2 -> "A2"
  | A3 -> "A3"
  | T0 -> "T0"
  | T1 -> "T1"
  | T2 -> "T2"
  | T3 -> "T3"
  | T4 -> "T4"
  | T5 -> "T5"
  | T6 -> "T6"
  | T7 -> "T7"
  | S0 -> "S0"
  | S1 -> "S1"
  | S2 -> "S2"
  | S3 -> "S3"
  | S4 -> "S4"
  | S5 -> "S5"
  | S6 -> "S6"
  | S7 -> "S7"
  | T8 -> "T8"
  | T9 -> "T9"
  | K0 -> "K0"
  | K1 -> "K1"
  | GP -> "GP"
  | SP -> "SP"
  | FP -> "FP"
  | RA -> "RA"
  | F0 -> "F0"
  | F1 -> "F1"
  | F2 -> "F2"
  | F3 -> "F3"
  | F4 -> "F4"
  | F5 -> "F5"
  | F6 -> "F6"
  | F7 -> "F7"
  | F8 -> "F8"
  | F9 -> "F9"
  | F10 -> "F10"
  | F11 -> "F11"
  | F12 -> "F12"
  | F13 -> "F13"
  | F14 -> "F14"
  | F15 -> "F15"
  | F16 -> "F16"
  | F17 -> "F17"
  | F18 -> "F18"
  | F19 -> "F19"
  | F20 -> "F20"
  | F21 -> "F21"
  | F22 -> "F22"
  | F23 -> "F23"
  | F24 -> "F24"
  | F25 -> "F25"
  | F26 -> "F26"
  | F27 -> "F27"
  | F28 -> "F28"
  | F29 -> "F29"
  | F30 -> "F30"
  | F31 -> "F31"
  | W0 -> "W0"
  | W1 -> "W1"
  | W2 -> "W2"
  | W3 -> "W3"
  | W4 -> "W4"
  | W5 -> "W5"
  | W6 -> "W6"
  | W7 -> "W7"
  | W8 -> "W8"
  | W9 -> "W9"
  | W10 -> "W10"
  | W11 -> "W11"
  | W12 -> "W12"
  | W13 -> "W13"
  | W14 -> "W14"
  | W15 -> "W15"
  | W16 -> "W16"
  | W17 -> "W17"
  | W18 -> "W18"
  | W19 -> "W19"
  | W20 -> "W20"
  | W21 -> "W21"
  | W22 -> "W22"
  | W23 -> "W23"
  | W24 -> "W24"
  | W25 -> "W25"
  | W26 -> "W26"
  | W27 -> "W27"
  | W28 -> "W28"
  | W29 -> "W29"
  | W30 -> "W30"
  | W31 -> "W31"
  | FCC0 -> "FCC0"
  | FCC1 -> "FCC1"
  | FCC2 -> "FCC2"
  | FCC3 -> "FCC3"
  | FCC4 -> "FCC4"
  | FCC5 -> "FCC5"
  | FCC6 -> "FCC6"
  | FCC7 -> "FCC7"
  | HI -> "HI"
  | LO -> "LO"
  | PC -> "PC"

let of_string = function
  | "ZERO" -> Some ZERO
  | "AT" -> Some AT
  | "V0" -> Some V0
  | "V1" -> Some V1
  | "A0" -> Some A0
  | "A1" -> Some A1
  | "A2" -> Some A2
  | "A3" -> Some A3
  | "T0" -> Some T0
  | "T1" -> Some T1
  | "T2" -> Some T2
  | "T3" -> Some T3
  | "T4" -> Some T4
  | "T5" -> Some T5
  | "T6" -> Some T6
  | "T7" -> Some T7
  | "S0" -> Some S0
  | "S1" -> Some S1
  | "S2" -> Some S2
  | "S3" -> Some S3
  | "S4" -> Some S4
  | "S5" -> Some S5
  | "S6" -> Some S6
  | "S7" -> Some S7
  | "T8" -> Some T8
  | "T9" -> Some T9
  | "K0" -> Some K0
  | "K1" -> Some K1
  | "GP" -> Some GP
  | "SP" -> Some SP
  | "FP" -> Some FP
  | "RA" -> Some RA
  | "F0" -> Some F0
  | "F1" -> Some F1
  | "F2" -> Some F2
  | "F3" -> Some F3
  | "F4" -> Some F4
  | "F5" -> Some F5
  | "F6" -> Some F6
  | "F7" -> Some F7
  | "F8" -> Some F8
  | "F9" -> Some F9
  | "F10" -> Some F10
  | "F11" -> Some F11
  | "F12" -> Some F12
  | "F13" -> Some F13
  | "F14" -> Some F14
  | "F15" -> Some F15
  | "F16" -> Some F16
  | "F17" -> Some F17
  | "F18" -> Some F18
  | "F19" -> Some F19
  | "F20" -> Some F20
  | "F21" -> Some F21
  | "F22" -> Some F22
  | "F23" -> Some F23
  | "F24" -> Some F24
  | "F25" -> Some F25
  | "F26" -> Some F26
  | "F27" -> Some F27
  | "F28" -> Some F28
  | "F29" -> Some F29
  | "F30" -> Some F30
  | "F31" -> Some F31
  | "W0" -> Some W0
  | "W1" -> Some W1
  | "W2" -> Some W2
  | "W3" -> Some W3
  | "W4" -> Some W4
  | "W5" -> Some W5
  | "W6" -> Some W6
  | "W7" -> Some W7
  | "W8" -> Some W8
  | "W9" -> Some W9
  | "W10" -> Some W10
  | "W11" -> Some W11
  | "W12" -> Some W12
  | "W13" -> Some W13
  | "W14" -> Some W14
  | "W15" -> Some W15
  | "W16" -> Some W16
  | "W17" -> Some W17
  | "W18" -> Some W18
  | "W19" -> Some W19
  | "W20" -> Some W20
  | "W21" -> Some W21
  | "W22" -> Some W22
  | "W23" -> Some W23
  | "W24" -> Some W24
  | "W25" -> Some W25
  | "W26" -> Some W26
  | "W27" -> Some W27
  | "W28" -> Some W28
  | "W29" -> Some W29
  | "W30" -> Some W30
  | "W31" -> Some W31
  | "FCC0" -> Some FCC0
  | "FCC1" -> Some FCC1
  | "FCC2" -> Some FCC2
  | "FCC3" -> Some FCC3
  | "FCC4" -> Some FCC4
  | "FCC5" -> Some FCC5
  | "FCC6" -> Some FCC6
  | "FCC7" -> Some FCC7
  | "HI" -> Some HI
  | "LO" -> Some LO
  | "PC" -> Some PC
  | _ -> None

let kind_of = function
  | ZERO
   |AT
   |V0
   |V1
   |A0
   |A1
   |A2
   |A3
   |T0
   |T1
   |T2
   |T3
   |T4
   |T5
   |T6
   |T7
   |S0
   |S1
   |S2
   |S3
   |S4
   |S5
   |S6
   |S7
   |T8
   |T9
   |K0
   |K1
   |GP
   |SP
   |FP
   |RA -> Kind.GP
  | F0
   |F1
   |F2
   |F3
   |F4
   |F5
   |F6
   |F7
   |F8
   |F9
   |F10
   |F11
   |F12
   |F13
   |F14
   |F15
   |F16
   |F17
   |F18
   |F19
   |F20
   |F21
   |F22
   |F23
   |F24
   |F25
   |F26
   |F27
   |F28
   |F29
   |F30
   |F31 -> Kind.FP
  | W0
   |W1
   |W2
   |W3
   |W4
   |W5
   |W6
   |W7
   |W8
   |W9
   |W10
   |W11
   |W12
   |W13
   |W14
   |W15
   |W16
   |W17
   |W18
   |W19
   |W20
   |W21
   |W22
   |W23
   |W24
   |W25
   |W26
   |W27
   |W28
   |W29
   |W30
   |W31 -> Kind.WR
  | FCC0 | FCC1 | FCC2 | FCC3 | FCC4 | FCC5 | FCC6 | FCC7 -> Kind.Fcc
  | HI | LO -> Kind.Accum
  | PC -> Kind.PC

type width = {width: int; width64: int}

let width_of r =
  match kind_of r with
  | Kind.GP -> {width= 32; width64= 64}
  | Kind.FP -> {width= 32; width64= 64}
  | Kind.WR -> {width= 128; width64= 128}
  | Kind.Fcc -> {width= 1; width64= 1}
  | Kind.Accum -> {width= 32; width64= 64}
  | Kind.PC -> {width= 32; width64= 64}
