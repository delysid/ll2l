open Core_kernel
open Ll2l_ir
open Ll2l_std
module C = Mips.Condition
module F = Mips.Format
module I = Mips.Instruction
module Imm = Mips.Immediate
module M = Mips.Mnemonic
module Mode = Mips.Mode
module O = Mips.Operand
module R = Mips.Register

type error = [`BadInstr | `BadDelay | `BadOperand of int]

let string_of_error = function
  | `BadInstr -> "bad instruction"
  | `BadDelay -> "bad delay slot"
  | `BadOperand i -> Printf.sprintf "bad operand (%d)" i

exception Mips_lift_error of error

let make_reg mode r is64 =
  let w = R.width_of r in
  let s =
    match R.kind_of r with
    | R.Kind.FP when Mode.is_rel2or6 mode -> w.width64
    | _ -> if is64 then w.width64 else w.width
  in
  Il.reg (R.to_string r) s

let make_operand (instr : I.t) addr i =
  let word_sz = Mode.word_size_of instr.mode in
  let is64 = word_sz = 64 in
  match instr.operands.(i) with
  | O.Register r -> Ok (make_reg instr.mode r is64)
  | O.Immediate imm -> (
    match imm.kind with
    | Imm.Relative -> Ok (Il.bitv Addr.(addr + int64 imm.value) word_sz)
    | Imm.Absolute ->
        let v =
          let hi_bits =
            let addr = Addr.to_int64 addr in
            if is64 then Int64.((addr + 4L) land 0x7FFFFFFFF0000000L)
            else Int64.((addr + 4L) land 0xF0000000L)
          in
          Int64.(hi_bits lor imm.value)
        in
        Ok (Il.num64 v word_sz)
    | Imm.Shift | Imm.Literal -> Ok (Il.num64 imm.value imm.size) )
  | O.Memory mem -> (
      let b = make_reg instr.mode mem.base is64 in
      let loc =
        match mem.addend with
        | Some (Disp disp) ->
            if Int64.is_negative disp then
              let disp = Int64.(lnot disp + 1L) in
              Il.(b - num64 disp word_sz)
            else Il.(b + num64 disp word_sz)
        | Some (Index idx) ->
            let i = make_reg instr.mode idx is64 in
            Il.(b + i)
        | None -> b
      in
      match mem.size with
      | None -> Ok loc
      | Some sz -> Ok Il.(load instr.endian Int.(sz lsr 3) mu loc) )
  (* this has to be handled manually *)
  | Slide _ -> Error (`BadOperand i)

let fp_reg_double i = function
  | Il.Var v -> (
    match R.of_string v.name with
    | None -> Error (`BadOperand i)
    | Some r -> (
      match R.kind_of r with
      | R.Kind.FP -> (
        match R.int_of_fp_reg r with
        | Some n when Int.rem n 2 = 0 -> (
          match R.fp_reg_of_int (n + 1) with
          | None -> Error (`BadOperand i)
          | Some r -> Ok (Il.reg (R.to_string r) 32) )
        | _ -> Error (`BadOperand i) )
      | _ -> Error (`BadOperand i) ) )
  | _ -> Error (`BadOperand i)

let fcc_reg_double i = function
  | Il.Var v -> (
    match R.of_string v.name with
    | None -> Error (`BadOperand i)
    | Some r -> (
      match R.kind_of r with
      | R.Kind.Fcc -> (
        match R.int_of_fcc_reg r with
        | Some n when Int.rem n 2 = 0 -> (
          match R.fcc_reg_of_int (n + 1) with
          | None -> Error (`BadOperand i)
          | Some r -> Ok (Il.reg (R.to_string r) 1) )
        | _ -> Error (`BadOperand i) )
      | _ -> Error (`BadOperand i) ) )
  | _ -> Error (`BadOperand i)

let fp_cmp lhs rhs = function
  (* ignore signaling conditions for now *)
  | C.F | C.SF -> Ok Il.(bit false)
  | C.UN | C.NGLE -> Ok Il.(lhs <=>. rhs)
  | C.EQ | C.SEQ -> Ok Il.(lhs =. rhs)
  | C.UEQ | C.NGL -> Ok Il.(lhs <=>. rhs || lhs =. rhs)
  | C.OLT | C.LT -> Ok Il.(lhs <. rhs)
  | C.ULT | C.NGE -> Ok Il.(lhs <=>. rhs || lhs <. rhs)
  | C.OLE | C.LE -> Ok Il.(lhs <=. rhs)
  | C.ULE | C.NGT -> Ok Il.(lhs <=>. rhs || lhs <=. rhs)
  | _ -> Error `BadInstr

let sign_ext value size =
  let mask = Int64.(1L lsl Int.(size - 1)) in
  Int64.((value lxor mask) - mask)

let zero sz = Il.reg "ZERO" sz

let ra sz = Il.reg "RA" sz

let acc_lo sz = Il.reg "LO" sz

let acc_hi sz = Il.reg "HI" sz

let cop2cc n = Il.reg (Printf.sprintf "COP2CC%d" n) 1

let replace_zero word_sz stmts =
  let zero = zero word_sz in
  let zeroi = Il.num 0 word_sz in
  let sub = (zero, zeroi) in
  let rec aux res = function
    | [] -> List.rev res
    | stmt :: rest -> (
      match stmt with
      | Il.Assign (dst, _) when Il.equal_expr dst zero ->
          (* Chapter 1.4, Table 1.2:
           * "Hardware ignores software writes to an R0 field.
           *  Neither the occurrence of such writes, nor the
           *  values written, affects hardware behavior." *)
          aux res rest
      | _ ->
          let stmt = Il.substitute_expr_in_stmt stmt ~sub in
          aux (stmt :: res) rest )
  in
  aux [] stmts

let replace_delay_jmp addr word_sz lbl stmts =
  let rec aux res = function
    | [] -> List.rev res
    | stmt :: rest -> (
      match stmt with
      | Il.Jmp (Il.Const c, _) when c.size = word_sz && Addr.(c.value = addr)
        -> aux (Il.Goto lbl :: res) rest
      | _ -> aux (stmt :: res) rest )
  in
  aux [] stmts

let rec lift' ?(delay = None) ?(delay_addr = None) (instr : I.t) addr ctx =
  let open Result.Let_syntax in
  let slot_offset = I.length * I.delay_slot_size instr in
  let endaddr = I.end_addr ~addr in
  let retaddr = Addr.(int slot_offset + endaddr) in
  let%bind endaddr' =
    match delay_addr with
    | Some _ when slot_offset <> 0 -> Error `BadDelay
    | Some d -> Ok d
    | None when slot_offset <> 0 -> Ok retaddr
    | None -> Ok endaddr
  in
  let new_label () = Il.Context.label ctx addr in
  let new_tmp size = Il.Context.tmp ctx size in
  let start_lbl = new_label () in
  let word_sz = Mips.Mode.word_size_of instr.mode in
  let is64 = word_sz = 64 in
  let o = make_operand instr addr in
  let se_imm_literal ?(lsh = 0) sz i =
    match instr.operands.(i) with
    | O.Immediate imm when Imm.(equal_kind imm.kind Literal) ->
        let v = sign_ext Int64.(imm.value lsl lsh) sz in
        Ok (v, Il.num64 v imm.size)
    | _ -> Error (`BadOperand i)
  in
  let lift_delay lbl =
    match delay with
    | None -> Error `BadInstr
    | Some d ->
        let%bind il = lift' d endaddr ctx ~delay_addr:(Some addr) in
        Il.unmake_blocks il |> replace_delay_jmp addr word_sz lbl |> return
  in
  let assign_mem dst src =
    match dst with
    | Il.Load r -> Il.(r.mem := store_r r src)
    | _ -> Il.(dst := src)
  in
  let%map stmts =
    match instr.mnemonic with
    | M.ABS when is64 -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := lo 32 fs; t1 := fabs t1; fd := undefined 32 @@ t1]
        | Some F.D -> Ok Il.[fd := fabs fs]
        | Some F.PS ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t1 := fabs t1
                ; t2 := hi 32 fs
                ; t2 := fabs t2
                ; fd := t2 @@ t1 ]
        | _ -> Error `BadInstr )
    | M.ABS -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2or6 instr.mode) ->
            Ok Il.[fd := fabs fs]
        | Some F.S ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := lo 32 fs; t1 := fabs t1; fd := undefined 32 @@ t1]
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fd_hi = fp_reg_double 0 fd in
            let%bind fs_hi = fp_reg_double 1 fs in
            Ok Il.[fd := fabs fs; fd_hi := fabs fs_hi]
        | Some F.D -> Ok Il.[fd := fabs fs]
        | Some F.PS when Mode.is_rel2 instr.mode ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t1 := fabs t1
                ; t2 := hi 32 fs
                ; t2 := fabs t2
                ; fd := t2 @@ t1 ]
        | _ -> Error `BadInstr )
    | M.ADD when is64 && Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 1 in
        let t5 = new_tmp 1 in
        let t6 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := t1 + t2
            ; t4 := hi 1 t3
            ; t5 := hi 1 t1
            ; t6 := hi 1 t2
            ; goto_if (t5 = t6 & t4 <> t5) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rd := se instr.opsz t3 ]
    | M.ADD when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 1 in
        let t3 = new_tmp 1 in
        let t4 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := rs + rt
            ; t2 := hi 1 t1
            ; t3 := hi 1 rs
            ; t4 := hi 1 rt
            ; goto_if (t3 = t4 & t2 <> t3) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rd := t1 ]
    | M.ADD when is64 && Option.is_some instr.fmt -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; fd := undefined 32 @@ (t1 +. t2) ]
        | Some F.D -> Ok Il.[fd := fs +. ft]
        | Some F.PS ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            let t3 = new_tmp 32 in
            let t4 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; t3 := hi 32 fs
                ; t4 := hi 32 ft
                ; fd := (t3 +. t4) @@ (t1 +. t2) ]
        | _ -> Error `BadInstr )
    | M.ADD when Option.is_some instr.fmt -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2or6 instr.mode) ->
            Ok Il.[fd := fs +. ft]
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fd_hi = fp_reg_double 0 fd in
            let%bind fs_hi = fp_reg_double 1 fs in
            let%bind ft_hi = fp_reg_double 2 ft in
            Ok Il.[fd := fs +. ft; fd_hi := fs_hi +. ft_hi]
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; fd := undefined 32 @@ (t1 +. t2) ]
        | Some F.D -> Ok Il.[fd := fs +. ft]
        | _ -> Error `BadInstr )
    | M.ADDI when is64 ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind imm_v, imm = se_imm_literal 16 2 in
        let imm_s = Int64.is_negative imm_v in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 1 in
        let t4 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let t4_chk = if imm_s then t4 else Il.(~~t4) in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := t1 + imm
            ; t3 := hi 1 t2
            ; t4 := hi 1 t1
            ; goto_if (t4_chk & t3 <> t4) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rt := se instr.opsz t2 ]
    | M.ADDI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind imm_v, imm = se_imm_literal 16 2 in
        let imm_s = Int64.is_negative imm_v in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 1 in
        let t3 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let t3_chk = if imm_s then t3 else Il.(~~t3) in
        Ok
          Il.
            [ t1 := rs + imm
            ; t2 := hi 1 t1
            ; t3 := hi 1 rs
            ; goto_if (t3_chk & t2 <> t3) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rt := t1 ]
    | M.ADDIU when is64 ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind _, imm = se_imm_literal 16 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs; t2 := t1 + imm; rt := se instr.opsz t2]
    | M.ADDIU ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let rs = if is64 then Il.(lo 32 rs) else rs in
        let%bind _, imm = se_imm_literal 16 2 in
        Ok Il.[rt := rs + imm]
    | M.ADDIUPC ->
        let%bind rs = o 0 in
        let%bind imm_v, _ = se_imm_literal 21 1 ~lsh:2 in
        let rel = Il.bitv Addr.(addr + int64 imm_v) word_sz in
        Ok Il.[rs := rel]
    | M.ADDU when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := t1 + t2
            ; rd := se instr.opsz t3 ]
    | M.ADDU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := rs + rt; rd := se instr.opsz t1]
    | M.ALIGN when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let%bind bp = o 3 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let bp = Il.(num 8 32 * bp) in
        Ok
          Il.
            [ t1 := lo 32 rt
            ; t1 := t1 << bp
            ; t2 := lo 32 rs
            ; t2 := t2 >> num 32 32 - bp
            ; rd := se 64 (t1 || t2) ]
    | M.ALIGN ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let%bind bp = o 3 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let bp = Il.(num 8 32 * bp) in
        Ok Il.[t1 := rt << bp; t2 := rs >> num 32 32 - bp; rd := t1 || t2]
    | M.ALUIPC ->
        let%bind rs = o 0 in
        let%bind imm_v, _ = se_imm_literal 32 1 ~lsh:16 in
        let rel =
          let v = Addr.((addr + int64 imm_v) land lnot (int 0x0FFFF)) in
          Il.bitv v word_sz
        in
        Ok Il.[rs := rel]
    | M.AND ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs & rt]
    | M.ANDI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rt := rs & imm]
    | M.AUI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind _, imm = se_imm_literal 32 2 ~lsh:16 in
        Ok Il.[rt := se instr.opsz (rs + imm)]
    | M.AUIPC ->
        let%bind rs = o 0 in
        let%bind imm_v, _ = se_imm_literal 32 1 ~lsh:16 in
        let rel = Il.bitv Addr.(addr + int64 imm_v) word_sz in
        Ok Il.[rs := rel]
    | M.B ->
        let%bind rel = o 0 in
        let%bind del = lift_delay start_lbl in
        Ok (del @ Il.[( @ ) start_lbl; jmp rel])
    | M.BAL ->
        let%bind rel = o 0 in
        let%bind del = lift_delay start_lbl in
        Ok
          ( del
          @ Il.[( @ ) start_lbl; ra word_sz := bitv retaddr word_sz; call rel]
          )
    | M.BALC ->
        let%bind rel = o 0 in
        Ok Il.[ra word_sz := bitv endaddr word_sz; call rel]
    | M.BC ->
        let%bind rel = o 0 in
        Ok Il.[jmp rel]
    | M.BC1EQZ ->
        let%bind ft = o 0 in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := ~~(lo 1 ft); goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC1F ->
        let%bind fcc = o 0 in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := ~~fcc; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC1FL ->
        let%bind fcc = o 0 in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if fcc l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BC1NEZ ->
        let%bind ft = o 0 in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := lo 1 ft; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC1T ->
        let%bind fcc = o 0 in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := fcc; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC1TL ->
        let%bind fcc = o 0 in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if ~~fcc l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BC2EQZ ->
        let%bind cc =
          match instr.operands.(0) with
          | O.Immediate imm -> Ok (cop2cc (Int64.to_int_exn imm.value))
          | _ -> Error (`BadOperand 0)
        in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := ~~cc; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC2F when Array.length instr.operands = 1 ->
        let%bind rel = o 0 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := ~~(cop2cc 0); goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC2F ->
        let%bind cc =
          match instr.operands.(0) with
          | O.Immediate imm -> Ok (cop2cc (Int64.to_int_exn imm.value))
          | _ -> Error (`BadOperand 0)
        in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := ~~cc; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC2FL when Array.length instr.operands = 1 ->
        let%bind rel = o 0 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if (cop2cc 0) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BC2FL ->
        let%bind cc =
          match instr.operands.(0) with
          | O.Immediate imm -> Ok (cop2cc (Int64.to_int_exn imm.value))
          | _ -> Error (`BadOperand 0)
        in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if cc l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BC2NEZ ->
        let%bind cc =
          match instr.operands.(0) with
          | O.Immediate imm -> Ok (cop2cc (Int64.to_int_exn imm.value))
          | _ -> Error (`BadOperand 0)
        in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := cc; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC2T when Array.length instr.operands = 1 ->
        let%bind rel = o 0 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := cop2cc 0; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC2T ->
        let%bind cc =
          match instr.operands.(0) with
          | O.Immediate imm -> Ok (cop2cc (Int64.to_int_exn imm.value))
          | _ -> Error (`BadOperand 0)
        in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := cc; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv endaddr word_sz)] )
    | M.BC2TL when Array.length instr.operands = 1 ->
        let%bind rel = o 0 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if ~~(cop2cc 0) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BC2TL ->
        let%bind cc =
          match instr.operands.(0) with
          | O.Immediate imm -> Ok (cop2cc (Int64.to_int_exn imm.value))
          | _ -> Error (`BadOperand 0)
        in
        let%bind rel = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if ~~cc l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BEQ ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := rs = rt; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv retaddr word_sz)] )
    | M.BEQC ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs = rt) rel (bitv endaddr word_sz)]
    | M.BEQL ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if (rs <> rt) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BEQZALC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ ra word_sz := bitv endaddr word_sz
            ; goto_if (rt <> zero) l1 l2
            ; ( @ ) l1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; call rel ]
    | M.BEQZC ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        Ok Il.[jmp_if (rs = zero) rel (bitv endaddr word_sz)]
    | M.BGEC ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs >=$ rt) rel (bitv endaddr word_sz)]
    | M.BGEUC ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs >= rt) rel (bitv endaddr word_sz)]
    | M.BGEZ ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := rs >=$ zero; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv retaddr word_sz)] )
    | M.BGEZAL ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        let l3 = new_label () in
        let l4 = new_label () in
        Ok
          ( Il.[( @ ) start_lbl; t1 := rs <$ zero; goto l2]
          @ del
          @ Il.
              [ ( @ ) l1
              ; ra word_sz := bitv retaddr word_sz
              ; goto_if t1 l3 l4
              ; ( @ ) l3
              ; jmp (bitv retaddr word_sz)
              ; ( @ ) l4
              ; call rel ] )
    | M.BGEZALC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ ra word_sz := bitv endaddr word_sz
            ; goto_if (rt <$ zero) l1 l2
            ; ( @ ) l1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; call rel ]
    | M.BGEZALL ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; ra word_sz := bitv retaddr word_sz
              ; goto_if (rs <$ zero) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; call rel] )
    | M.BGEZC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        Ok Il.[jmp_if (rt >=$ zero) rel (bitv endaddr word_sz)]
    | M.BGEZL ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if (rs <$ zero) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BGTZ ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := rs >$ zero; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv retaddr word_sz)] )
    | M.BGTZALC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ ra word_sz := bitv endaddr word_sz
            ; goto_if (rt <=$ zero) l1 l2
            ; ( @ ) l1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; call rel ]
    | M.BGTZC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        Ok Il.[jmp_if (rt >$ zero) rel (bitv endaddr word_sz)]
    | M.BGTZL ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if (rs <=$ zero) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BITSWAP ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t1' = new_tmp 8 in
        let t2' = new_tmp 8 in
        let t3' = new_tmp 8 in
        let t4' = new_tmp 8 in
        let t5 = new_tmp 8 in
        let res = Il.(t4' @@ t3' @@ t2' @@ t1') in
        let res = if is64 then Il.(se 64 res) else res in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        Ok
          Il.
            [ t1 := lo 8 rt
            ; t2 := ex 15 8 rt
            ; t3 := ex 23 16 rt
            ; t4 := ex 31 24 rt
            ; t1' := num 0 8
            ; t2' := num 0 8
            ; t3' := num 0 8
            ; t4' := num 0 8
            ; t5 := num 0 8
            ; goto l1
            ; ( @ ) l1
            ; goto_if (t5 < num 8 8) l2 l3
            ; ( @ ) l2
            ; t1' := t1' || (t1 >> t5 & num 1 8) << num 7 8 - t5
            ; t2' := t2' || (t2 >> t5 & num 1 8) << num 7 8 - t5
            ; t3' := t3' || (t3 >> t5 & num 1 8) << num 7 8 - t5
            ; t4' := t4' || (t4 >> t5 & num 1 8) << num 7 8 - t5
            ; t5 := t5 + num 1 8
            ; goto l1
            ; ( @ ) l3
            ; rd := res ]
    | M.BLEZ ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := rs <=$ zero; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv retaddr word_sz)] )
    | M.BLEZALC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ ra word_sz := bitv endaddr word_sz
            ; goto_if (rt >$ zero) l1 l2
            ; ( @ ) l1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; call rel ]
    | M.BLEZC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        Ok Il.[jmp_if (rt <=$ zero) rel (bitv endaddr word_sz)]
    | M.BLEZL ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if (rs >$ zero) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BLTC ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs <$ rt) rel (bitv endaddr word_sz)]
    | M.BLTUC ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs < rt) rel (bitv endaddr word_sz)]
    | M.BLTZ ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := rs <$ zero; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv retaddr word_sz)] )
    | M.BLTZAL ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let l3 = new_label () in
        let l4 = new_label () in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := rs >=$ zero; goto l2]
          @ del
          @ Il.
              [ ( @ ) l1
              ; ra word_sz := bitv retaddr word_sz
              ; goto_if t1 l3 l4
              ; ( @ ) l3
              ; jmp (bitv retaddr word_sz)
              ; ( @ ) l4
              ; call rel ] )
    | M.BLTZALC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ ra word_sz := bitv endaddr word_sz
            ; goto_if (rt >=$ zero) l1 l2
            ; ( @ ) l1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; call rel ]
    | M.BLTZALL ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; ra word_sz := bitv retaddr word_sz
              ; goto_if (rs >=$ zero) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; call rel] )
    | M.BLTZC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        Ok Il.[jmp_if (rt <$ zero) rel (bitv endaddr word_sz)]
    | M.BLTZL ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if (rs >=$ zero) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BNE ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        let l1 = new_label () in
        let l2 = (endaddr, 1) in
        let t1 = new_tmp 1 in
        let%bind del = lift_delay l1 in
        Ok
          ( Il.[( @ ) start_lbl; t1 := rs <> rt; goto l2]
          @ del
          @ Il.[( @ ) l1; jmp_if t1 rel (bitv retaddr word_sz)] )
    | M.BNEC ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        Ok Il.[jmp_if (rs <> rt) rel (bitv endaddr word_sz)]
    | M.BNEL ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = (endaddr, 1) in
        let%bind del = lift_delay l2 in
        Ok
          ( Il.
              [ ( @ ) start_lbl
              ; goto_if (rs = rt) l1 l3
              ; ( @ ) l1
              ; jmp (bitv retaddr word_sz) ]
          @ del
          @ Il.[( @ ) l2; jmp rel] )
    | M.BNEZALC ->
        let%bind rt = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ ra word_sz := bitv endaddr word_sz
            ; goto_if (rt = zero) l1 l2
            ; ( @ ) l1
            ; jmp (bitv endaddr word_sz)
            ; ( @ ) l2
            ; call rel ]
    | M.BNEZC ->
        let%bind rs = o 0 in
        let%bind rel = o 1 in
        let zero = Il.num 0 word_sz in
        Ok Il.[jmp_if (rs <> zero) rel (bitv endaddr word_sz)]
    | M.BNVC when is64 ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 1 in
        let t5 = new_tmp 1 in
        let t6 = new_tmp 1 in
        let cond = Il.(t5 <> t6 || t4 = t5) in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := t1 + t2
            ; t4 := hi 1 t3
            ; t5 := hi 1 t1
            ; t6 := hi 1 t2
            ; jmp_if cond rel (bitv endaddr word_sz) ]
    | M.BNVC ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 1 in
        let t3 = new_tmp 1 in
        let t4 = new_tmp 1 in
        let cond = Il.(t3 <> t4 || t2 = t3) in
        Ok
          Il.
            [ t1 := rs + rt
            ; t2 := hi 1 t1
            ; t3 := hi 1 rs
            ; t4 := hi 1 rt
            ; jmp_if cond rel (bitv endaddr word_sz) ]
    | M.BOVC when is64 ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 1 in
        let t5 = new_tmp 1 in
        let t6 = new_tmp 1 in
        let cond = Il.(t5 = t6 & t4 <> t5) in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := t1 + t2
            ; t4 := hi 1 t3
            ; t5 := hi 1 t1
            ; t6 := hi 1 t2
            ; jmp_if cond rel (bitv endaddr word_sz) ]
    | M.BOVC ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let%bind rel = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 1 in
        let t3 = new_tmp 1 in
        let t4 = new_tmp 1 in
        let cond = Il.(t3 = t4 & t2 <> t3) in
        Ok
          Il.
            [ t1 := rs + rt
            ; t2 := hi 1 t1
            ; t3 := hi 1 rs
            ; t4 := hi 1 rt
            ; jmp_if cond rel (bitv endaddr word_sz) ]
    | M.BREAK -> Ok Il.[breakpoint ()]
    | M.C when is64 -> (
        let%bind cc = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        let%bind cond =
          match instr.cond with
          | Some cond -> Ok cond
          | None -> Error `BadInstr
        in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            let%bind cmp = fp_cmp t1 t2 cond in
            Ok Il.[t1 := lo 32 fs; t2 := lo 32 ft; cc := cmp]
        | Some F.D ->
            let%bind cmp = fp_cmp fs ft cond in
            Ok Il.[cc := cmp]
        | Some F.PS ->
            let%bind cc_hi = fcc_reg_double 0 cc in
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            let%bind cmp_lo = fp_cmp t1 t2 cond in
            let t3 = new_tmp 32 in
            let t4 = new_tmp 32 in
            let%bind cmp_hi = fp_cmp t3 t4 cond in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; t3 := hi 32 fs
                ; t4 := hi 32 ft
                ; cc := cmp_lo
                ; cc_hi := cmp_hi ]
        | _ -> Error `BadInstr )
    | M.C -> (
        let%bind cc = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        let%bind cond =
          match instr.cond with
          | Some cond -> Ok cond
          | None -> Error `BadInstr
        in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2 instr.mode) ->
            let%bind cmp = fp_cmp fs ft cond in
            Ok Il.[cc := cmp]
        | Some F.D when not (Mode.is_rel2 instr.mode) ->
            let%bind cc_hi = fcc_reg_double 0 cc in
            let%bind fs_hi = fp_reg_double 1 fs in
            let%bind ft_hi = fp_reg_double 2 ft in
            let%bind cmp_lo = fp_cmp fs ft cond in
            let%bind cmp_hi = fp_cmp fs_hi ft_hi cond in
            Ok Il.[cc := cmp_lo; cc_hi := cmp_hi]
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            let%bind cmp = fp_cmp t1 t2 cond in
            Ok Il.[t1 := lo 32 fs; t2 := lo 32 ft; cc := cmp]
        | Some F.D ->
            let%bind cmp = fp_cmp fs ft cond in
            Ok Il.[cc := cmp]
        | _ -> Error `BadInstr )
    | M.CLO ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let t1 = new_tmp word_sz in
        let t2 = new_tmp word_sz in
        let t3 = new_tmp word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          Il.
            [ t1 := num 32 word_sz
            ; t2 := num 31 word_sz
            ; goto l1
            ; ( @ ) l1
            ; goto_if (t2 >=$ num 0 word_sz) l2 l5
            ; ( @ ) l2
            ; t3 := num 1 word_sz << t2
            ; goto_if ((rs & t3) = num 0 word_sz) l3 l4
            ; ( @ ) l3
            ; t1 := num 31 word_sz - t2
            ; goto l5
            ; ( @ ) l4
            ; t2 := t2 - num 1 word_sz
            ; goto l1
            ; ( @ ) l5
            ; rd := t1 ]
    | M.CLZ ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let t1 = new_tmp word_sz in
        let t2 = new_tmp word_sz in
        let t3 = new_tmp word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          Il.
            [ t1 := num 32 word_sz
            ; t2 := num 31 word_sz
            ; goto l1
            ; ( @ ) l1
            ; goto_if (t2 >=$ num 0 word_sz) l2 l5
            ; ( @ ) l2
            ; t3 := num 1 word_sz << t2
            ; goto_if ((rs & t3) <> num 0 word_sz) l3 l4
            ; ( @ ) l3
            ; t1 := num 31 word_sz - t2
            ; goto l5
            ; ( @ ) l4
            ; t2 := t2 - num 1 word_sz
            ; goto l1
            ; ( @ ) l5
            ; rd := t1 ]
    | M.CVTS when is64 -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.D ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := ftof 32 fs; fd := undefined 32 @@ t1]
        | Some F.W ->
            let t1 = new_tmp 32 in
            Ok
              Il.[t1 := lo 32 fs; t1 := fptof 32 t1; fd := undefined 32 @@ t1]
        | Some F.L ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := fptof 32 fs; fd := undefined 32 @@ t1]
        | _ -> Error `BadInstr )
    | M.CVTS -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fs_hi = fp_reg_double 1 fs in
            let t1 = new_tmp 32 in
            Ok Il.[t1 := fs_hi @@ fs; fd := ftof 32 t1]
        | Some F.D ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := ftof 32 fs; fd := undefined 32 @@ t1]
        | Some F.W when not (Mode.is_rel2or6 instr.mode) ->
            Ok Il.[fd := fptof 32 fs]
        | Some F.W ->
            let t1 = new_tmp 32 in
            Ok
              Il.[t1 := lo 32 fs; t1 := fptof 32 t1; fd := undefined 32 @@ t1]
        | Some F.L when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fs_hi = fp_reg_double 1 fs in
            let t1 = new_tmp 32 in
            Ok Il.[t1 := fs_hi @@ fs; fd := fptof 32 t1]
        | Some F.L ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := fptof 32 fs; fd := undefined 32 @@ t1]
        | _ -> Error `BadInstr )
    | M.CVTW when is64 -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            Ok
              Il.[t1 := lo 32 fs; t1 := ftofp 32 t1; fd := undefined 32 @@ t1]
        | Some F.D ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := ftofp 32 fs; fd := undefined 32 @@ t1]
        | _ -> Error `BadInstr )
    | M.CVTW -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2or6 instr.mode) ->
            Ok Il.[fd := ftofp 32 fs]
        | Some F.S ->
            let t1 = new_tmp 32 in
            Ok
              Il.[t1 := lo 32 fs; t1 := ftofp 32 t1; fd := undefined 32 @@ t1]
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fs_hi = fp_reg_double 1 fs in
            let t1 = new_tmp 32 in
            Ok Il.[t1 := fs_hi @@ fs; fd := ftofp 32 t1]
        | Some F.D ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := ftofp 32 fs; fd := undefined 32 @@ t1]
        | _ -> Error `BadInstr )
    | M.DADD ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 1 in
        let t3 = new_tmp 1 in
        let t4 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := rs + rt
            ; t2 := hi 1 t1
            ; t3 := hi 1 rs
            ; t4 := hi 1 rt
            ; goto_if (t3 = t4 & t2 <> t3) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rd := t1 ]
    | M.DADDI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind imm_v, imm = se_imm_literal 16 2 in
        let imm_s = Int64.is_negative imm_v in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 1 in
        let t3 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        let t3_chk = if imm_s then t3 else Il.(~~t3) in
        Ok
          Il.
            [ t1 := rs + imm
            ; t2 := hi 1 t1
            ; t3 := hi 1 rs
            ; goto_if (t3_chk & t2 <> t3) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rt := t1 ]
    | M.DADDIU ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind _, imm = se_imm_literal 16 2 in
        Ok Il.[rt := rs + imm]
    | M.DADDU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs + rt]
    | M.DAHI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind _, imm = se_imm_literal 48 2 ~lsh:32 in
        Ok Il.[rt := rs + imm]
    | M.DALIGN ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let%bind bp = o 3 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let bp = Il.(num 8 64 * bp) in
        Ok Il.[t1 := rt << bp; t2 := rs >> num 64 64 - bp; rd := t1 || t2]
    | M.DATI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind _, imm = se_imm_literal 64 2 ~lsh:48 in
        Ok Il.[rt := rs + imm]
    | M.DAUI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind _, imm = se_imm_literal 32 2 ~lsh:16 in
        Ok Il.[rt := rs + imm]
    | M.DBITSWAP ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        let t1' = new_tmp 8 in
        let t2' = new_tmp 8 in
        let t3' = new_tmp 8 in
        let t4' = new_tmp 8 in
        let t5' = new_tmp 8 in
        let t6' = new_tmp 8 in
        let t7' = new_tmp 8 in
        let t8' = new_tmp 8 in
        let t9 = new_tmp 8 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        Ok
          Il.
            [ t1 := lo 8 rt
            ; t2 := ex 15 8 rt
            ; t3 := ex 23 16 rt
            ; t4 := ex 31 24 rt
            ; t5 := ex 39 32 rt
            ; t6 := ex 47 40 rt
            ; t7 := ex 55 48 rt
            ; t8 := hi 8 rt
            ; t1' := num 0 8
            ; t2' := num 0 8
            ; t3' := num 0 8
            ; t4' := num 0 8
            ; t5' := num 0 8
            ; t6' := num 0 8
            ; t7' := num 0 8
            ; t8' := num 0 8
            ; t9 := num 0 8
            ; goto l1
            ; ( @ ) l1
            ; goto_if (t9 < num 8 8) l2 l3
            ; ( @ ) l2
            ; t1' := t1' || (t1 >> t9 & num 1 8) << num 7 8 - t9
            ; t2' := t2' || (t2 >> t9 & num 1 8) << num 7 8 - t9
            ; t3' := t3' || (t3 >> t9 & num 1 8) << num 7 8 - t9
            ; t4' := t4' || (t4 >> t9 & num 1 8) << num 7 8 - t9
            ; t5' := t5' || (t5 >> t9 & num 1 8) << num 7 8 - t9
            ; t6' := t6' || (t6 >> t9 & num 1 8) << num 7 8 - t9
            ; t7' := t7' || (t7 >> t9 & num 1 8) << num 7 8 - t9
            ; t8' := t8' || (t8 >> t9 & num 1 8) << num 7 8 - t9
            ; t9 := t9 + num 1 8
            ; goto l1
            ; ( @ ) l3
            ; rd := t8' @@ t7' @@ t6' @@ t5' @@ t4' @@ t3' @@ t2' @@ t1' ]
    | M.DCLO ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let t1 = new_tmp word_sz in
        let t2 = new_tmp word_sz in
        let t3 = new_tmp word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          Il.
            [ t1 := num 64 word_sz
            ; t2 := num 63 word_sz
            ; goto l1
            ; ( @ ) l1
            ; goto_if (t2 >=$ num 0 word_sz) l2 l5
            ; ( @ ) l2
            ; t3 := num 1 word_sz << t2
            ; goto_if ((rs & t3) = num 0 word_sz) l3 l4
            ; ( @ ) l3
            ; t1 := num 63 word_sz - t2
            ; goto l5
            ; ( @ ) l4
            ; t2 := t2 - num 1 word_sz
            ; goto l1
            ; ( @ ) l5
            ; rd := t1 ]
    | M.DCLZ ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let t1 = new_tmp word_sz in
        let t2 = new_tmp word_sz in
        let t3 = new_tmp word_sz in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        let l4 = new_label () in
        let l5 = new_label () in
        Ok
          Il.
            [ t1 := num 64 word_sz
            ; t2 := num 63 word_sz
            ; goto l1
            ; ( @ ) l1
            ; goto_if (t2 >=$ num 0 word_sz) l2 l5
            ; ( @ ) l2
            ; t3 := num 1 word_sz << t2
            ; goto_if ((rs & t3) <> num 0 word_sz) l3 l4
            ; ( @ ) l3
            ; t1 := num 63 word_sz - t2
            ; goto l5
            ; ( @ ) l4
            ; t2 := t2 - num 1 word_sz
            ; goto l1
            ; ( @ ) l5
            ; rd := t1 ]
    | M.DDIV when not (Mode.is_rel6 instr.mode) ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        Ok Il.[acc_lo word_sz := rs /$ rt; acc_hi word_sz := rs %$ rt]
    | M.DDIV ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs /$ rt]
    | M.DDIVU when not (Mode.is_rel6 instr.mode) ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        Ok Il.[acc_lo word_sz := rs / rt; acc_hi word_sz := rs % rt]
    | M.DDIVU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs / rt]
    | M.DEXT | M.DEXTM | M.DEXTU | M.EXT ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind pos =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Literal) ->
              Ok Int64.(to_int_exn imm.value)
          | _ -> Error (`BadOperand 2)
        in
        let%bind size =
          match instr.operands.(3) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Literal) ->
              Ok Int64.(to_int_exn imm.value)
          | _ -> Error (`BadOperand 2)
        in
        let pos' = pos + size - 1 in
        Ok Il.[rt := ze 64 (ex pos' pos rs)]
    | M.DINS | M.DINSM | M.DINSU | M.INS ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind pos =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Literal) ->
              Ok Int64.(to_int_exn imm.value)
          | _ -> Error (`BadOperand 2)
        in
        let%bind size =
          match instr.operands.(3) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Literal) ->
              Ok Int64.(to_int_exn imm.value)
          | _ -> Error (`BadOperand 2)
        in
        let pos' = pos + size - 1 in
        let mask = Int64.(lnot ((1L lsl Int.(pos' + 1)) - (1L lsl pos))) in
        Ok Il.[rt := rt & num64 mask word_sz; rt := rt || rs]
    | M.DIV
      when is64 && Option.is_none instr.fmt && not (Mode.is_rel6 instr.mode)
      ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let lo' = acc_lo word_sz in
        let hi' = acc_hi word_sz in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; lo' := se 64 (t1 /$ rt)
            ; hi' := se 64 (rs %$ rt) ]
    | M.DIV when Option.is_none instr.fmt && not (Mode.is_rel6 instr.mode) ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let lo' = acc_lo word_sz in
        let hi' = acc_hi word_sz in
        Ok Il.[lo' := rs /$ rt; hi' := rs %$ rt]
    | M.DIV when is64 && Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs; t2 := lo 32 rt; rd := se 64 (t1 /$ t2)]
    | M.DIV when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs /$ rt]
    | M.DIV when is64 && Option.is_some instr.fmt -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; fd := undefined 32 @@ (t1 /. t2) ]
        | Some F.D -> Ok Il.[fd := fs /. ft]
        | _ -> Error `BadInstr )
    | M.DIV when Option.is_some instr.fmt -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2or6 instr.mode) ->
            Ok Il.[fd := fs /. ft]
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fd_hi = fp_reg_double 0 fd in
            let%bind fs_hi = fp_reg_double 1 fs in
            let%bind ft_hi = fp_reg_double 2 ft in
            Ok Il.[fd := fs /. ft; fd_hi := fs_hi /. ft_hi]
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; fd := undefined 32 @@ (t1 /. t2) ]
        | Some F.D -> Ok Il.[fd := fs /. ft]
        | _ -> Error `BadInstr )
    | M.DIVU when is64 && not (Mode.is_rel6 instr.mode) ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let lo' = acc_lo word_sz in
        let hi' = acc_hi word_sz in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; lo' := se 64 (t1 / t2)
            ; hi' := se 64 (t1 % t2) ]
    | M.DIVU when not (Mode.is_rel6 instr.mode) ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let lo' = acc_lo word_sz in
        let hi' = acc_hi word_sz in
        Ok Il.[lo' := rs / rt; hi' := rs % rt]
    | M.DIVU when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs; t2 := lo 32 rt; rd := se 64 (t1 /$ t2)]
    | M.DIVU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs /$ rt]
    | M.DLSA ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let%bind sa =
          match instr.operands.(3) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok (Int64.(to_int_exn imm.value) + 1)
          | _ -> Error (`BadOperand 3)
        in
        Ok Il.[rd := (rs << num sa 64) + rt]
    | M.DMFC1 ->
        let%bind rt = o 0 in
        let%bind fs = o 1 in
        Ok Il.[rt := fs]
    | M.DMOD ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs %$ rt]
    | M.DMODU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs % rt]
    | M.DMTC1 ->
        let%bind rt = o 0 in
        let%bind fs = o 1 in
        Ok Il.[fs := rt]
    | M.DMUH ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 128 in
        Ok Il.[t1 := se 128 rs * se 128 rt; rd := hi 64 t1]
    | M.DMUHU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 128 in
        Ok Il.[t1 := ze 128 rs * ze 128 rt; rd := hi 64 t1]
    | M.DMUL ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 128 in
        Ok Il.[t1 := se 128 rs * se 128 rt; rd := lo 64 t1]
    | M.DMULT ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 128 in
        Ok
          Il.
            [ t1 := se 128 rs * se 128 rt
            ; acc_lo 64 := lo 64 t1
            ; acc_hi 64 := hi 64 t1 ]
    | M.DMULTU ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 128 in
        Ok
          Il.
            [ t1 := ze 128 rs * ze 128 rt
            ; acc_lo 64 := lo 64 t1
            ; acc_hi 64 := hi 64 t1 ]
    | M.DMULU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 128 in
        Ok Il.[t1 := ze 128 rs * ze 128 rt; rd := lo 64 t1]
    | M.DROTR ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind s =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok (Int64.to_int_exn imm.value)
          | _ -> Error (`BadOperand 2)
        in
        Ok Il.[rd := ex Int.(s - 1) 0 rt @@ ex 63 s rt]
    | M.DROTR32 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind s =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok (Int64.(to_int_exn imm.value) + 32)
          | _ -> Error (`BadOperand 2)
        in
        Ok Il.[rd := ex Int.(s - 1) 0 rt @@ ex 63 s rt]
    | M.DROTRV ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        Ok
          Il.
            [ t1 := rs & num 0x3F 64
            ; t2 := num 64 64 - t1
            ; rd := rt >> t1 || rt << t2 ]
    | M.DSBH ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 8 in
        let t2 = new_tmp 8 in
        let t3 = new_tmp 8 in
        let t4 = new_tmp 8 in
        let t5 = new_tmp 8 in
        let t6 = new_tmp 8 in
        let t7 = new_tmp 8 in
        let t8 = new_tmp 8 in
        Ok
          Il.
            [ t1 := ex 55 48 rt
            ; t2 := ex 63 56 rt
            ; t3 := ex 39 32 rt
            ; t4 := ex 47 40 rt
            ; t5 := ex 23 16 rt
            ; t6 := ex 31 24 rt
            ; t7 := ex 7 0 rt
            ; t8 := ex 15 8 rt
            ; rd := t1 @@ t2 @@ t3 @@ t4 @@ t5 @@ t6 @@ t7 @@ t8 ]
    | M.DSHD ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 16 in
        let t2 = new_tmp 16 in
        let t3 = new_tmp 16 in
        let t4 = new_tmp 16 in
        Ok
          Il.
            [ t1 := ex 15 0 rt
            ; t2 := ex 31 16 rt
            ; t3 := ex 47 32 rt
            ; t4 := ex 63 48 rt
            ; rd := t1 @@ t2 @@ t3 @@ t4 ]
    | M.DSLL ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        Ok Il.[rd := rt << sa]
    | M.DSLL32 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok Il.(num64 Int64.(imm.value + 32L) word_sz)
          | _ -> Error (`BadOperand 2)
        in
        Ok Il.[rd := rt << sa]
    | M.DSLLV ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := rs & num 0x3F 64; rd := rt << t1]
    | M.DSRA ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        Ok Il.[rd := rt >>> sa]
    | M.DSRA32 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok Il.(num64 Int64.(imm.value + 32L) word_sz)
          | _ -> Error (`BadOperand 2)
        in
        Ok Il.[rd := rt >>> sa]
    | M.DSRAV ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := rs & num 0x3F 64; rd := rt >>> t1]
    | M.DSRL ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        Ok Il.[rd := rt >> sa]
    | M.DSRL32 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok Il.(num64 Int64.(imm.value + 32L) word_sz)
          | _ -> Error (`BadOperand 2)
        in
        Ok Il.[rd := rt >> sa]
    | M.DSRLV ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := rs & num 0x3F 64; rd := rt >> t1]
    | M.DSUB ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 1 in
        let t3 = new_tmp 1 in
        let t4 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := rs - rt
            ; t2 := hi 1 t1
            ; t3 := hi 1 rs
            ; t4 := hi 1 rt
            ; goto_if (t3 = t4 & t2 <> t3) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rd := t1 ]
    | M.DSUBU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs - rt]
    | M.J ->
        let%bind rel = o 0 in
        let%bind del = lift_delay start_lbl in
        Ok (del @ Il.[( @ ) start_lbl; jmp rel])
    | M.JAL ->
        let%bind rel = o 0 in
        let%bind del = lift_delay start_lbl in
        Ok
          ( del
          @ Il.[( @ ) start_lbl; ra word_sz := bitv retaddr word_sz; call rel]
          )
    | (M.JALR | M.JALRHB) when Array.length instr.operands > 1 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind del = lift_delay start_lbl in
        Ok (del @ Il.[( @ ) start_lbl; rd := bitv retaddr word_sz; call rs])
    | M.JALR | M.JALRHB ->
        let%bind rs = o 0 in
        let%bind del = lift_delay start_lbl in
        Ok
          ( del
          @ Il.[( @ ) start_lbl; ra word_sz := bitv retaddr word_sz; call rs]
          )
    | M.JIALC ->
        let%bind rt = o 0 in
        let%bind _, imm = se_imm_literal 16 1 in
        let t1 = new_tmp word_sz in
        let%bind del = lift_delay start_lbl in
        Ok
          ( del
          @ Il.
              [ ( @ ) start_lbl
              ; ra word_sz := bitv retaddr word_sz
              ; t1 := rt + imm
              ; call t1 ] )
    | M.JIC ->
        let%bind rt = o 0 in
        let%bind _, imm = se_imm_literal 16 1 in
        let t1 = new_tmp word_sz in
        let%bind del = lift_delay start_lbl in
        Ok (del @ Il.[( @ ) start_lbl; t1 := rt + imm; call t1])
    | M.JR | M.JRHB ->
        let%bind rs = o 0 in
        let%bind del = lift_delay start_lbl in
        Ok (del @ Il.[( @ ) start_lbl; jmp rs])
    | M.LB | M.LBE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 8 in
        Ok Il.[t1 := m; rt := se word_sz t1]
    | M.LBU | M.LBUE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 8 in
        Ok Il.[t1 := m; rt := ze word_sz t1]
    | M.LD when Option.is_none instr.fmt ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[rt := m]
    | M.LDL ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let%bind t1, loc, m =
          match m with
          | Il.Load r ->
              let t1 = new_tmp 64 in
              Ok (t1, r.loc, Il.(Load {r with loc= t1}))
          | _ -> Error (`BadOperand 1)
        in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let mask = Int64.(lnot 0x7L) in
        let n1 = Il.num 1 64 in
        Ok
          Il.
            [ t2 := loc
            ; t1 := t2 & num64 mask 64
            ; t2 := (n1 << (t2 - t1 << num 3 64)) - n1
            ; t3 := m
            ; t3 := t3 & ~~t2
            ; rt := (rt & t2) || t3 ]
    | M.LDPC ->
        let%bind rs = o 0 in
        let%bind off_v, _ = se_imm_literal 21 1 ~lsh:3 in
        let pc = Addr.(addr land lnot (int 0x7)) in
        let rel = Il.bitv Addr.(pc + int64 off_v) word_sz in
        Ok Il.[rs := load instr.endian 64 mu rel]
    | M.LDR ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let%bind t1, loc, m =
          match m with
          | Il.Load r ->
              let t1 = new_tmp 64 in
              Ok (t1, r.loc, Il.(Load {r with loc= t1}))
          | _ -> Error (`BadOperand 1)
        in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        let mask = Int64.(lnot 0x7L) in
        let n1 = Il.num 1 64 in
        Ok
          Il.
            [ t2 := loc
            ; t1 := t2 & num64 mask 64
            ; t2 := (n1 << (t2 - t1 << num 3 64)) - n1
            ; t3 := m
            ; t3 := t3 & t2
            ; rt := (rt & ~~t2) || t3 ]
    | M.LH | M.LHE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 16 in
        Ok Il.[t1 := m; rt := se word_sz t1]
    | M.LHU | M.LHUE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 16 in
        Ok Il.[t1 := m; rt := ze word_sz t1]
    | (M.LL | M.LLE) when is64 ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := m; rt := se 64 t1]
    | M.LL | M.LLE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[rt := m]
    | M.LLD ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[rt := m]
    | M.LLDP ->
        let%bind rt = o 0 in
        let%bind rd = o 1 in
        let%bind m = o 2 in
        let t1 = new_tmp 128 in
        Ok Il.[t1 := m; rd := lo 64 t1; rt := hi 64 t1]
    | (M.LLWP | M.LLWPE) when is64 ->
        let%bind rt = o 0 in
        let%bind rd = o 1 in
        let%bind m = o 2 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := m; rd := se 64 (lo 32 t1); rt := se 64 (hi 32 t1)]
    | M.LLWP | M.LLWPE ->
        let%bind rt = o 0 in
        let%bind rd = o 1 in
        let%bind m = o 2 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := m; rd := lo 32 t1; rt := hi 32 t1]
    | M.LSA when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let%bind sa =
          match instr.operands.(3) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok (Int64.(to_int_exn imm.value) + 1)
          | _ -> Error (`BadOperand 3)
        in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; rd := se 64 ((t1 << num sa 32) + t2) ]
    | M.LSA ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let%bind sa =
          match instr.operands.(3) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok (Int64.(to_int_exn imm.value) + 1)
          | _ -> Error (`BadOperand 3)
        in
        Ok Il.[rd := (rs << num sa 32) + rt]
    | M.LUI when is64 ->
        let%bind rt = o 0 in
        let%bind _, imm = se_imm_literal 32 1 ~lsh:16 in
        Ok Il.[rt := se 64 imm]
    | M.LUI ->
        let%bind rt = o 0 in
        let%bind _, imm = se_imm_literal 32 1 ~lsh:16 in
        Ok Il.[rt := imm]
    | (M.LW | M.LWE) when is64 ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := m; rt := se 64 t1]
    | M.LW | M.LWE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[rt := m]
    | M.LWC1 when is64 ->
        let%bind ft = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := m; ft := undefined 32 @@ t1]
    | M.LWC1 ->
        let%bind ft = o 0 in
        let%bind m = o 1 in
        Ok Il.[ft := m]
    | M.LWL ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let%bind t1, loc, m =
          match m with
          | Il.Load r ->
              let t1 = new_tmp word_sz in
              Ok (t1, r.loc, Il.(Load {r with loc= t1}))
          | _ -> Error (`BadOperand 1)
        in
        let t2 = new_tmp word_sz in
        let t3 = new_tmp word_sz in
        let mask = Int64.(lnot 0x3L) in
        let n1 = Il.num 1 word_sz in
        Ok
          Il.
            [ t2 := loc
            ; t1 := t2 & num64 mask word_sz
            ; t2 := (n1 << (t2 - t1 << num 2 word_sz)) - n1
            ; t3 := m
            ; t3 := t3 & ~~t2
            ; rt := (rt & t2) || t3 ]
    | M.LWPC when is64 ->
        let%bind rs = o 0 in
        let%bind off_v, _ = se_imm_literal 21 1 ~lsh:2 in
        let rel = Il.bitv Addr.(addr + int64 off_v) word_sz in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := load instr.endian 32 mu rel; rs := se 64 t1]
    | M.LWPC ->
        let%bind rs = o 0 in
        let%bind off_v, _ = se_imm_literal 21 1 ~lsh:2 in
        let rel = Il.bitv Addr.(addr + int64 off_v) word_sz in
        Ok Il.[rs := load instr.endian 32 mu rel]
    | M.LWR ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let%bind t1, loc, m =
          match m with
          | Il.Load r ->
              let t1 = new_tmp word_sz in
              Ok (t1, r.loc, Il.(Load {r with loc= t1}))
          | _ -> Error (`BadOperand 1)
        in
        let t2 = new_tmp word_sz in
        let t3 = new_tmp word_sz in
        let mask = Int64.(lnot 0x3L) in
        let n1 = Il.num 1 word_sz in
        Ok
          Il.
            [ t2 := loc
            ; t1 := t2 & num64 mask word_sz
            ; t2 := (n1 << (t2 - t1 << num 2 word_sz)) - n1
            ; t3 := m
            ; t3 := t3 & t2
            ; rt := (rt & ~~t2) || t3 ]
    | M.LWU when is64 ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := m; rt := ze word_sz t1]
    | M.LWU ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[rt := m]
    | M.LWUPC when is64 ->
        let%bind rs = o 0 in
        let%bind off_v, _ = se_imm_literal 21 1 ~lsh:2 in
        let rel = Il.bitv Addr.(addr + int64 off_v) word_sz in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := load instr.endian 32 mu rel; rs := ze 64 t1]
    | M.LWUPC ->
        let%bind rs = o 0 in
        let%bind off_v, _ = se_imm_literal 21 1 ~lsh:2 in
        let rel = Il.bitv Addr.(addr + int64 off_v) word_sz in
        Ok Il.[rs := load instr.endian 32 mu rel]
    | M.MADD when is64 && Option.is_none instr.fmt ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let hi' = acc_hi word_sz in
        let lo' = acc_lo word_sz in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        let t7 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := lo 32 hi'
            ; t4 := lo 32 lo'
            ; t5 := se 64 t1 * se 64 t2
            ; t6 := t3 @@ t4
            ; t7 := t5 + t6
            ; acc_hi 64 := se 64 (hi 32 t7)
            ; acc_lo 64 := se 64 (lo 32 t7) ]
    | M.MADD when Option.is_none instr.fmt ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let hi' = acc_hi word_sz in
        let lo' = acc_lo word_sz in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rs * se 64 rt
            ; t2 := hi' @@ lo'
            ; t3 := t1 + t2
            ; acc_hi 32 := hi 32 t3
            ; acc_lo 32 := lo 32 t3 ]
    | M.MADDU when is64 && Option.is_none instr.fmt ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let hi' = acc_hi word_sz in
        let lo' = acc_lo word_sz in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        let t7 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := lo 32 hi'
            ; t4 := lo 32 lo'
            ; t5 := ze 64 t1 * ze 64 t2
            ; t6 := t3 @@ t4
            ; t7 := t5 + t6
            ; acc_hi 64 := se 64 (hi 32 t7)
            ; acc_lo 64 := se 64 (lo 32 t7) ]
    | M.MADDU when Option.is_none instr.fmt ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let hi' = acc_hi word_sz in
        let lo' = acc_lo word_sz in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := ze 64 rs * ze 64 rt
            ; t2 := hi' @@ lo'
            ; t3 := t1 + t2
            ; acc_hi 32 := hi 32 t3
            ; acc_lo 32 := lo 32 t3 ]
    | M.MFC1 when is64 || Mode.is_rel2or6 instr.mode ->
        let%bind rt = o 0 in
        let%bind fs = o 1 in
        Ok Il.[rt := se 64 (lo 32 fs)]
    | M.MFC1 ->
        let%bind rt = o 0 in
        let%bind fs = o 1 in
        Ok Il.[rt := fs]
    | M.MFHI ->
        let%bind rd = o 0 in
        Ok Il.[rd := acc_hi word_sz]
    | M.MFLO ->
        let%bind rd = o 0 in
        Ok Il.[rd := acc_lo word_sz]
    | M.MOD when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs; t2 := lo 32 rt; rd := se 64 (t1 %$ t2)]
    | M.MOD ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs %$ rt]
    | M.MODU when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rs; t2 := lo 32 rt; rd := se 64 (t1 % t2)]
    | M.MODU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs % rt]
    | M.MOVF when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind cc = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if ~~cc l1 l2; ( @ ) l1; rd := rs; goto l2; ( @ ) l2]
    | M.MOVN when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ goto_if (rt <> num 0 word_sz) l1 l2
            ; ( @ ) l1
            ; rd := rs
            ; goto l2
            ; ( @ ) l2 ]
    | M.MOVT when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind cc = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if cc l1 l2; ( @ ) l1; rd := rs; goto l2; ( @ ) l2]
    | M.MOVZ when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ goto_if (rt = num 0 word_sz) l1 l2
            ; ( @ ) l1
            ; rd := rs
            ; goto l2
            ; ( @ ) l2 ]
    | M.MSUB when is64 && Option.is_none instr.fmt ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let hi' = acc_hi word_sz in
        let lo' = acc_lo word_sz in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        let t7 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := lo 32 hi'
            ; t4 := lo 32 lo'
            ; t5 := se 64 t1 * se 64 t2
            ; t6 := t3 @@ t4
            ; t7 := t5 - t6
            ; acc_hi 64 := se 64 (hi 32 t7)
            ; acc_lo 64 := se 64 (lo 32 t7) ]
    | M.MSUB when Option.is_none instr.fmt ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let hi' = acc_hi word_sz in
        let lo' = acc_lo word_sz in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rs * se 64 rt
            ; t2 := hi' @@ lo'
            ; t3 := t1 - t2
            ; acc_hi 32 := hi 32 t3
            ; acc_lo 32 := lo 32 t3 ]
    | M.MSUBU when is64 && Option.is_none instr.fmt ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let hi' = acc_hi word_sz in
        let lo' = acc_lo word_sz in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 32 in
        let t5 = new_tmp 64 in
        let t6 = new_tmp 64 in
        let t7 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := lo 32 hi'
            ; t4 := lo 32 lo'
            ; t5 := ze 64 t1 * ze 64 t2
            ; t6 := t3 @@ t4
            ; t7 := t5 - t6
            ; acc_hi 64 := se 64 (hi 32 t7)
            ; acc_lo 64 := se 64 (lo 32 t7) ]
    | M.MSUBU when Option.is_none instr.fmt ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let hi' = acc_hi word_sz in
        let lo' = acc_lo word_sz in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := ze 64 rs * ze 64 rt
            ; t2 := hi' @@ lo'
            ; t3 := t1 - t2
            ; acc_hi 32 := hi 32 t3
            ; acc_lo 32 := lo 32 t3 ]
    | M.MTC1 when is64 || Mode.is_rel2or6 instr.mode ->
        let%bind rt = o 0 in
        let%bind fs = o 1 in
        Ok Il.[fs := undefined 32 @@ lo 32 rt]
    | M.MTC1 ->
        let%bind rt = o 0 in
        let%bind fs = o 1 in
        Ok Il.[fs := rt]
    | M.MTHI ->
        let%bind rs = o 0 in
        Ok Il.[acc_hi word_sz := rs]
    | M.MTLO ->
        let%bind rs = o 0 in
        Ok Il.[acc_lo word_sz := rs]
    | M.MUH when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := se 64 t1 * se 64 t2
            ; rd := se 64 (hi 32 t3) ]
    | M.MUH ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := se 64 rs * se 64 rt; rd := hi 32 t1]
    | M.MUHU when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := ze 64 t1 * ze 64 t2
            ; rd := se 64 (hi 32 t3) ]
    | M.MUHU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := ze 64 rs * ze 64 rt; rd := hi 32 t1]
    | M.MUL
      when is64 && Option.is_none instr.fmt && not (Mode.is_rel6 instr.mode)
      ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := se 64 t1 * se 64 t2
            ; rd := se 64 (lo 32 t3)
            ; acc_lo 64 := undefined 64
            ; acc_hi 64 := undefined 64 ]
    | M.MUL when Option.is_none instr.fmt && not (Mode.is_rel6 instr.mode) ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rs * se 64 rt
            ; rd := lo 32 t1
            ; acc_lo 32 := undefined 32
            ; acc_hi 32 := undefined 32 ]
    | M.MUL when is64 && Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := se 64 t1 * se 64 t2
            ; rd := se 64 (lo 32 t3) ]
    | M.MUL when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 64 in
        Ok Il.[t1 := se 64 rs * se 64 rt; rd := lo 32 t1]
    | M.MUL when is64 && Option.is_some instr.fmt -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; fd := undefined 32 @@ (t1 *. t2) ]
        | Some F.D -> Ok Il.[fd := fs *. ft]
        | Some F.PS ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            let t3 = new_tmp 32 in
            let t4 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; t3 := hi 32 fs
                ; t4 := hi 32 ft
                ; fd := (t3 *. t4) @@ (t1 *. t2) ]
        | _ -> Error `BadInstr )
    | M.MUL when Option.is_some instr.fmt -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2or6 instr.mode) ->
            Ok Il.[fd := fs *. ft]
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fd_hi = fp_reg_double 0 fd in
            let%bind fs_hi = fp_reg_double 1 fs in
            let%bind ft_hi = fp_reg_double 2 ft in
            Ok Il.[fd := fs *. ft; fd_hi := fs_hi *. ft_hi]
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; fd := undefined 32 @@ (t1 *. t2) ]
        | Some F.D -> Ok Il.[fd := fs *. ft]
        | Some F.PS ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            let t3 = new_tmp 32 in
            let t4 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; t3 := hi 32 fs
                ; t4 := hi 32 ft
                ; fd := (t3 *. t4) @@ (t1 *. t2) ]
        | _ -> Error `BadInstr )
    | M.MULT when is64 ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := se 64 t1 * se 64 t2
            ; acc_lo 64 := se 64 (lo 32 t3)
            ; acc_hi 64 := se 64 (hi 32 t3) ]
    | M.MULT ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 64 in
        Ok
          Il.
            [ t1 := se 64 rs * se 64 rt
            ; acc_lo 32 := lo 32 t1
            ; acc_hi 32 := hi 32 t1 ]
    | M.MULTU when is64 ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := ze 64 t1 * ze 64 t2
            ; acc_lo 64 := se 64 (lo 32 t3)
            ; acc_hi 64 := se 64 (hi 32 t3) ]
    | M.MULTU ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let t1 = new_tmp 64 in
        Ok
          Il.
            [ t1 := ze 64 rs * ze 64 rt
            ; acc_lo 32 := lo 32 t1
            ; acc_hi 32 := hi 32 t1 ]
    | M.NAL ->
        let%bind del = lift_delay start_lbl in
        Ok (del @ Il.[( @ ) start_lbl; ra word_sz := bitv retaddr word_sz])
    | M.NOP | M.SSNOP -> Ok []
    | M.NOR ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := ~~(rs || rt)]
    | M.OR ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs || rt]
    | M.ORI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rt := rs || imm]
    | M.ROTR when is64 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind s =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok (Int64.to_int_exn imm.value)
          | _ -> Error (`BadOperand 2)
        in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := ex Int.(s - 1) 0 rt @@ ex 31 s rt; rd := se word_sz t1]
    | M.ROTR ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind s =
          match instr.operands.(2) with
          | O.Immediate imm when Imm.(equal_kind imm.kind Shift) ->
              Ok (Int64.to_int_exn imm.value)
          | _ -> Error (`BadOperand 2)
        in
        Ok Il.[rd := ex Int.(s - 1) 0 rt @@ ex 31 s rt]
    | M.ROTRV when is64 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 64 in
        let t3 = new_tmp 64 in
        Ok
          Il.
            [ t1 := rs & num 0x1F 64
            ; t2 := num 32 64 - t1
            ; t3 := rt >> t1 || rt << t2
            ; rd := se 64 (lo 32 t3) ]
    | M.ROTRV ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := rs & num 0x1F 32
            ; t2 := num 32 32 - t1
            ; rd := rt >> t1 || rt << t2 ]
    | M.SB | M.SBE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[assign_mem m (lo 8 rt)]
    | (M.SC | M.SCE) when is64 ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rt; assign_mem m t1; rt := num 1 64]
    | M.SC | M.SCE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[assign_mem m rt; rt := num 1 32]
    | M.SCD ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[assign_mem m rt; rt := num 1 64]
    | M.SCDP ->
        let%bind rt = o 0 in
        let%bind rd = o 1 in
        let%bind m = o 2 in
        Ok Il.[assign_mem m (rd @@ rt); rt := num 1 64]
    | (M.SCWP | M.SCWPE) when is64 ->
        let%bind rt = o 0 in
        let%bind rd = o 1 in
        let%bind m = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 rt
            ; t2 := lo 32 rd
            ; assign_mem m (t2 @@ t1)
            ; rt := num 1 64 ]
    | M.SCWP | M.SCWPE ->
        let%bind rt = o 0 in
        let%bind rd = o 1 in
        let%bind m = o 2 in
        Ok Il.[assign_mem m (rd @@ rt); rt := num 1 32]
    | M.SD ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok [assign_mem m rt]
    | M.SDBBP -> Ok Il.[breakpoint ()]
    | M.SEB ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        Ok Il.[rd := se word_sz (lo 8 rt)]
    | M.SEH ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        Ok Il.[rd := se word_sz (lo 16 rt)]
    | M.SELEQZ when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        Ok
          Il.
            [ goto_if (rt <> num 0 word_sz) l1 l2
            ; ( @ ) l1
            ; rd := num 0 word_sz
            ; goto l3
            ; ( @ ) l2
            ; rd := rs
            ; goto l3
            ; ( @ ) l3 ]
    | M.SELNEZ when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let l1 = new_label () in
        let l2 = new_label () in
        let l3 = new_label () in
        Ok
          Il.
            [ goto_if (rt = num 0 word_sz) l1 l2
            ; ( @ ) l1
            ; rd := num 0 word_sz
            ; goto l3
            ; ( @ ) l2
            ; rd := rs
            ; goto l3
            ; ( @ ) l3 ]
    | M.SH | M.SHE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[assign_mem m (lo 16 rt)]
    | M.SIGRIE -> Ok Il.[exn ()]
    | M.SLL when is64 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 (rt << sa); rd := se 64 t1]
    | M.SLL ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        Ok Il.[rd := rt << sa]
    | M.SLLV when is64 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 32 in
        Ok Il.[t1 := rs & num 0x1F 64; t2 := lo 32 (rt << t1); rd := se 64 t2]
    | M.SLLV ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := rs & num 0x1F 32; rd := rt << t1]
    | M.SLT ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := ze word_sz (rs <$ rt)]
    | M.SLTI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind _, imm = se_imm_literal 16 2 in
        Ok Il.[rt := ze word_sz (rs <$ imm)]
    | M.SLTIU ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind _, imm = se_imm_literal 16 2 in
        Ok Il.[rt := ze word_sz (rs < imm)]
    | M.SLTU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := ze word_sz (rs < rt)]
    | M.SRA when is64 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 (rt >>> sa); rd := se 64 t1]
    | M.SRA ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        Ok Il.[rd := rt >>> sa]
    | M.SRAV when is64 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 32 in
        Ok
          Il.[t1 := rs & num 0x1F 64; t2 := lo 32 (rt >>> rs); rd := se 64 t2]
    | M.SRAV ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := rs & num 0x1F 32; rd := rt >>> t1]
    | M.SRL when is64 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 (rt >> sa); rd := se 64 t1]
    | M.SRL ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind sa = o 2 in
        Ok Il.[rd := rt >> sa]
    | M.SRLV when is64 ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 64 in
        let t2 = new_tmp 32 in
        Ok Il.[t1 := rs & num 0x1F 64; t2 := lo 32 (rt >> rs); rd := se 64 t2]
    | M.SRLV ->
        let%bind rd = o 0 in
        let%bind rt = o 1 in
        let%bind rs = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := rs & num 0x1F 32; rd := rt >> t1]
    | M.SUB when is64 && Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        let t4 = new_tmp 1 in
        let t5 = new_tmp 1 in
        let t6 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := t1 - t2
            ; t4 := hi 1 t3
            ; t5 := hi 1 t1
            ; t6 := hi 1 t2
            ; goto_if (t5 = t6 & t4 <> t5) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rd := se instr.opsz t3 ]
    | M.SUB when Option.is_none instr.fmt ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 1 in
        let t3 = new_tmp 1 in
        let t4 = new_tmp 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok
          Il.
            [ t1 := rs - rt
            ; t2 := hi 1 t1
            ; t3 := hi 1 rs
            ; t4 := hi 1 rt
            ; goto_if (t3 = t4 & t2 <> t3) l1 l2
            ; ( @ ) l1
            ; exn ()
            ; ( @ ) l2
            ; rd := t1 ]
    | M.SUB when is64 && Option.is_some instr.fmt -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; fd := undefined 32 @@ (t1 -. t2) ]
        | Some F.D -> Ok Il.[fd := fs -. ft]
        | Some F.PS ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            let t3 = new_tmp 32 in
            let t4 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; t3 := hi 32 fs
                ; t4 := hi 32 ft
                ; fd := (t3 -. t4) @@ (t1 -. t2) ]
        | _ -> Error `BadInstr )
    | M.SUB when Option.is_some instr.fmt -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        let%bind ft = o 2 in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2or6 instr.mode) ->
            Ok Il.[fd := fs -. ft]
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fd_hi = fp_reg_double 0 fd in
            let%bind fs_hi = fp_reg_double 1 fs in
            let%bind ft_hi = fp_reg_double 2 ft in
            Ok Il.[fd := fs -. ft; fd_hi := fs_hi -. ft_hi]
        | Some F.S ->
            let t1 = new_tmp 32 in
            let t2 = new_tmp 32 in
            Ok
              Il.
                [ t1 := lo 32 fs
                ; t2 := lo 32 ft
                ; fd := undefined 32 @@ (t1 -. t2) ]
        | Some F.D -> Ok Il.[fd := fs -. ft]
        | _ -> Error `BadInstr )
    | M.SUBU when is64 ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        let t2 = new_tmp 32 in
        let t3 = new_tmp 32 in
        Ok
          Il.
            [ t1 := lo 32 rs
            ; t2 := lo 32 rt
            ; t3 := t1 - t2
            ; rd := se instr.opsz t3 ]
    | M.SUBU ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := rs - rt; rd := se instr.opsz t1]
    | (M.SW | M.SWE) when is64 ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        let t1 = new_tmp 32 in
        Ok Il.[t1 := lo 32 rt; assign_mem m t1]
    | M.SW | M.SWE ->
        let%bind rt = o 0 in
        let%bind m = o 1 in
        Ok Il.[assign_mem m rt]
    | M.SYSCALL -> Ok Il.[syscall ()]
    | M.TEQ ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs = rt) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TEQI ->
        let%bind rs = o 0 in
        let%bind _, imm = se_imm_literal 16 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs = imm) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TGE ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs >=$ rt) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TGEI ->
        let%bind rs = o 0 in
        let%bind _, imm = se_imm_literal 16 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs >=$ imm) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TGEIU ->
        let%bind rs = o 0 in
        let%bind _, imm = se_imm_literal 16 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs >= imm) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TGEU ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs >= rt) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TLT ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs <$ rt) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TLTI ->
        let%bind rs = o 0 in
        let%bind _, imm = se_imm_literal 16 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs <$ imm) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TLTIU ->
        let%bind rs = o 0 in
        let%bind _, imm = se_imm_literal 16 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs < imm) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TLTU ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs < rt) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TNE ->
        let%bind rs = o 0 in
        let%bind rt = o 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs <> rt) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TNEI ->
        let%bind rs = o 0 in
        let%bind _, imm = se_imm_literal 16 1 in
        let l1 = new_label () in
        let l2 = new_label () in
        Ok Il.[goto_if (rs <> imm) l1 l2; ( @ ) l1; exn (); ( @ ) l2]
    | M.TRUNCL when is64 -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := lo 32 fs; fd := ftofp 64 t1]
        | Some F.D -> Ok Il.[fd := ftofp 64 fs]
        | _ -> Error `BadInstr )
    | M.TRUNCL -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fd_hi = fp_reg_double 0 fd in
            let t1 = new_tmp 64 in
            Ok Il.[t1 := ftofp 64 fs; fd := lo 32 t1; fd_hi := hi 32 t1]
        | Some F.S ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := lo 32 fs; fd := ftofp 64 t1]
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fd_hi = fp_reg_double 0 fd in
            let%bind fs_hi = fp_reg_double 1 fs in
            let t1 = new_tmp 64 in
            Ok
              Il.
                [ t1 := ftofp 64 (fs_hi @@ fs)
                ; fd := lo 32 t1
                ; fd_hi := hi 32 t1 ]
        | Some F.D -> Ok Il.[fd := ftofp 64 fs]
        | _ -> Error `BadInstr )
    | M.TRUNCW when is64 -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.S ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := lo 32 fs; fd := undefined 32 @@ ftofp 32 t1]
        | Some F.D -> Ok Il.[fd := undefined 32 @@ ftofp 32 fs]
        | _ -> Error `BadInstr )
    | M.TRUNCW -> (
        let%bind fd = o 0 in
        let%bind fs = o 1 in
        match instr.fmt with
        | Some F.S when not (Mode.is_rel2or6 instr.mode) ->
            Ok Il.[fd := ftofp 32 fs]
        | Some F.S ->
            let t1 = new_tmp 32 in
            Ok Il.[t1 := lo 32 fs; fd := undefined 32 @@ ftofp 32 t1]
        | Some F.D when not (Mode.is_rel2or6 instr.mode) ->
            let%bind fd_hi = fp_reg_double 0 fd in
            let%bind fs_hi = fp_reg_double 1 fs in
            Ok Il.[fd := ftofp 32 (fs_hi @@ fs)]
        | Some F.D -> Ok Il.[fd := undefined 32 @@ ftofp 32 fs]
        | _ -> Error `BadInstr )
    | M.XOR ->
        let%bind rd = o 0 in
        let%bind rs = o 1 in
        let%bind rt = o 2 in
        Ok Il.[rd := rs ^ rt]
    | M.XORI ->
        let%bind rt = o 0 in
        let%bind rs = o 1 in
        let%bind imm = o 2 in
        Ok Il.[rt := rs ^ imm]
    | _ -> Ok Il.[unk ()]
  in
  let stmts =
    (* if the instruction has a delay slot
     * then it's your responsibility to
     * insert the appropriate label(s) *)
    if slot_offset <> 0 then stmts else Il.(( @ ) start_lbl) :: stmts
  in
  replace_zero word_sz stmts
  |> Il.fold_constants_in_stmts
  |> Il.normalize_end endaddr' word_sz ctx
  |> Il.normalize_conditions ctx
  |> Il.remove_identity_assigns
  |> (fun stmts -> Il.Context.reset_tmp ctx; stmts)
  |> Il.make_blocks

let lift ?(delay = None) (instr : I.t) addr ctx = lift' instr addr ctx ~delay

let lift_exn ?(delay = None) (instr : I.t) addr ctx =
  match lift instr addr ctx ~delay with
  | Ok il -> il
  | Error err -> raise (Mips_lift_error err)
