(* this code is borrowed (and extended/improved) from the B2R2 project *)

open Core_kernel
module C = Mips.Condition
module F = Mips.Format
module I = Mips.Instruction
module Imm = Mips.Immediate
module Mode = Mips.Mode
module M = Mips.Mnemonic
module O = Mips.Operand
module R = Mips.Register

type error = [`OOB | `Invalid | `BadOperand | `Unimpl]

let string_of_error = function
  | `OOB -> "out of bounds"
  | `Invalid -> "invalid"
  | `BadOperand -> "bad operand"
  | `Unimpl -> "unimplemented"

exception Mips_decode_error of error

let sign_ext value size =
  let mask = Int64.(1L lsl Int.(size - 1)) in
  Int64.((value lxor mask) - mask)

let e : O.t array = [||]

module Context = struct
  type t = {enc: int; mode: Mode.t; mutable opsz: int}

  let is_rel2or6 ctx = Mode.is_rel2or6 ctx.mode [@@inline]

  let is_rel6 ctx = Mode.is_rel6 ctx.mode [@@inline]

  let ex ctx hi lo =
    let hi, lo = if max hi lo = hi then (hi, lo) else (lo, hi) in
    let off = hi - lo + 1 in
    if off > 31 then failwith "invalid range"
    else (ctx.enc lsr lo) land (Int.pow 2 off - 1)

  let tst ctx pos =
    match (ctx.enc lsr pos) land 1 with
    | 0 -> false
    | _ -> true

  let num9 ctx = ex ctx 15 7 [@@inline]

  let num11 ctx = ex ctx 10 0 [@@inline]

  let num16 ctx = ex ctx 15 0 [@@inline]

  let num19 ctx = ex ctx 18 0 [@@inline]

  let num21 ctx = ex ctx 20 0 [@@inline]

  let num26 ctx = ex ctx 25 0 [@@inline]

  let gpr2521 ctx = ex ctx 25 21 |> R.gp_reg_of_int [@@inline]

  let gpr2016 ctx = ex ctx 20 16 |> R.gp_reg_of_int [@@inline]

  let gpr1511 ctx = ex ctx 15 11 |> R.gp_reg_of_int [@@inline]

  let gpr106 ctx = ex ctx 10 6 |> R.gp_reg_of_int [@@inline]

  let fpr2521 ctx = ex ctx 25 21 |> R.fp_reg_of_int [@@inline]

  let fpr2016 ctx = ex ctx 20 16 |> R.fp_reg_of_int [@@inline]

  let fpr1511 ctx = ex ctx 15 11 |> R.fp_reg_of_int [@@inline]

  let fpr106 ctx = ex ctx 10 6 |> R.fp_reg_of_int [@@inline]

  let fcc2018 ctx = ex ctx 20 18 |> R.fcc_reg_of_int [@@inline]

  let fcc108 ctx = ex ctx 10 8 |> R.fcc_reg_of_int [@@inline]

  let wr2016 ctx = ex ctx 20 16 |> R.wr_reg_of_int [@@inline]

  let wr1511 ctx = ex ctx 15 11 |> R.wr_reg_of_int [@@inline]

  let wr106 ctx = ex ctx 10 6 |> R.wr_reg_of_int [@@inline]

  let reg_int f n = Option.(f n >>| fun r -> O.Register r) [@@inline]

  let rs ctx = Option.(gpr2521 ctx >>| fun r -> O.Register r)

  let rt ctx = Option.(gpr2016 ctx >>| fun r -> O.Register r)

  let rd ctx = Option.(gpr1511 ctx >>| fun r -> O.Register r)

  let fr ctx = Option.(fpr2521 ctx >>| fun r -> O.Register r)

  let ft ctx = Option.(fpr2016 ctx >>| fun r -> O.Register r)

  let fs ctx = Option.(fpr1511 ctx >>| fun r -> O.Register r)

  let fd ctx = Option.(fpr106 ctx >>| fun r -> O.Register r)

  let cc1 ctx = Option.(fcc108 ctx >>| fun r -> O.Register r)

  let cc2 ctx = Option.(fcc2018 ctx >>| fun r -> O.Register r)

  let wt ctx = Option.(wr2016 ctx >>| fun r -> O.Register r)

  let ws ctx = Option.(wr1511 ctx >>| fun r -> O.Register r)

  let wd ctx = Option.(wr106 ctx >>| fun r -> O.Register r)

  let sa ctx =
    let value = Int64.of_int @@ ex ctx 10 6 in
    O.Immediate Imm.{value; kind= Shift; size= ctx.opsz}

  let bp ctx =
    let value = Int64.of_int @@ ex ctx 7 6 in
    O.Immediate Imm.{value; kind= Literal; size= ctx.opsz}

  let rel16 ctx =
    let value = Int64.(of_int (num16 ctx) lsl 2) in
    let value = sign_ext value 18 in
    let value = Int64.(value + 4L) in
    O.Immediate Imm.{value; kind= Relative; size= ctx.opsz}

  let rel21 ctx =
    let value = Int64.(of_int (num21 ctx) lsl 2) in
    let value = sign_ext value 23 in
    let value = Int64.(value + 4L) in
    O.Immediate Imm.{value; kind= Relative; size= ctx.opsz}

  let rel26 ctx =
    let value = Int64.(of_int (num26 ctx) lsl 2) in
    let value = sign_ext value 28 in
    let value = Int64.(value + 4L) in
    O.Immediate Imm.{value; kind= Relative; size= ctx.opsz}

  let abs26 ctx =
    let value = Int64.(of_int (num26 ctx) lsl 2) in
    O.Immediate Imm.{value; kind= Absolute; size= ctx.opsz}

  let immsel ctx hi lo =
    let value = Int64.of_int (ex ctx hi lo) in
    O.Immediate Imm.{value; kind= Literal; size= ctx.opsz}

  let u5 size ctx =
    let value = Int64.of_int (ex ctx 20 16) in
    O.Immediate Imm.{value; kind= Literal; size}

  let s5 size ctx =
    let value = Int64.of_int (ex ctx 20 16) in
    let value = sign_ext value 5 in
    O.Immediate Imm.{value; kind= Literal; size}

  let s10 size ctx =
    let value = Int64.of_int (ex ctx 20 11) in
    let value = sign_ext value 10 in
    O.Immediate Imm.{value; kind= Literal; size}

  let imm16 size ctx =
    let value = Int64.of_int (num16 ctx) in
    O.Immediate Imm.{value; kind= Literal; size}

  let imm18 size ctx =
    let value = Int64.of_int (ex ctx 17 0) in
    O.Immediate Imm.{value; kind= Literal; size}

  let imm19 size ctx =
    let value = Int64.of_int (ex ctx 18 0) in
    O.Immediate Imm.{value; kind= Literal; size}

  let membaseoff ctx size =
    Option.(
      gpr2521 ctx
      >>| fun base ->
      let disp = Int64.of_int (num16 ctx) in
      let disp = sign_ext disp 16 in
      O.(Memory {base; addend= Some (Disp disp); size}))

  let membaseoff9 ctx size =
    Option.(
      gpr2521 ctx
      >>| fun base ->
      let disp = Int64.of_int (num9 ctx) in
      let disp = sign_ext disp 9 in
      O.(Memory {base; addend= Some (Disp disp); size}))

  let membaseoff11 ctx size =
    Option.(
      gpr2521 ctx
      >>| fun base ->
      let disp = Int64.of_int (num11 ctx) in
      let disp = sign_ext disp 11 in
      O.(Memory {base; addend= Some (Disp disp); size}))

  let pos_size ctx =
    let msb = Int64.of_int @@ ex ctx 15 11 in
    let lsb = Int64.of_int @@ ex ctx 10 6 in
    let imm1 = O.Immediate Imm.{value= lsb; kind= Literal; size= 5} in
    let imm2 =
      O.Immediate Imm.{value= Int64.(msb + 1L - lsb); kind= Literal; size= 5}
    in
    (imm1, imm2)

  let pos_size2 ctx =
    let msb = Int64.of_int @@ ex ctx 15 11 in
    let lsb = Int64.of_int @@ ex ctx 10 6 in
    let imm1 = O.Immediate Imm.{value= lsb; kind= Literal; size= 5} in
    let imm2 =
      O.Immediate Imm.{value= Int64.(msb + 1L); kind= Literal; size= 5}
    in
    (imm1, imm2)

  let pos_size3 ctx =
    let msb = Int64.of_int @@ ex ctx 15 11 in
    let lsb = Int64.of_int @@ ex ctx 10 6 in
    let imm1 = O.Immediate Imm.{value= lsb; kind= Literal; size= 5} in
    let imm2 =
      O.Immediate
        Imm.{value= Int64.(msb + 33L - lsb); kind= Literal; size= 5}
    in
    (imm1, imm2)

  let pos_size4 ctx =
    let msb = Int64.of_int @@ ex ctx 15 11 in
    let lsb = Int64.of_int @@ ex ctx 10 6 in
    let lsb = Int64.(lsb + 32L) in
    let imm1 = O.Immediate Imm.{value= lsb; kind= Literal; size= 5} in
    let imm2 =
      O.Immediate
        Imm.{value= Int64.(msb + 33L - lsb); kind= Literal; size= 5}
    in
    (imm1, imm2)

  let pos_size5 ctx =
    let msb = Int64.of_int @@ ex ctx 15 11 in
    let lsb = Int64.of_int @@ ex ctx 10 6 in
    let imm1 = O.Immediate Imm.{value= lsb; kind= Literal; size= 5} in
    let imm2 =
      O.Immediate Imm.{value= Int64.(msb + 33L); kind= Literal; size= 5}
    in
    (imm1, imm2)

  let pos_size6 ctx =
    let msb = Int64.of_int @@ ex ctx 15 11 in
    let lsb = Int64.of_int @@ ex ctx 10 6 in
    let imm1 =
      O.Immediate Imm.{value= Int64.(lsb + 32L); kind= Literal; size= 5}
    in
    let imm2 =
      O.Immediate Imm.{value= Int64.(msb + 1L); kind= Literal; size= 5}
    in
    (imm1, imm2)

  let make_cdrs ctx =
    Option.(
      rd ctx
      >>| fun rs' ->
      (* we actually select 15-11 *)
      (* this seems like a hack *)
      let value = Int64.of_int (ex ctx 10 6) in
      let cd = O.Immediate Imm.{value; kind= Literal; size= -1} in
      [|cd; rs'|])

  let make_rdcs ctx =
    Option.(
      gpr106 ctx
      >>| fun rd' ->
      let rd' = O.Register rd' in
      (* this seems like a hack *)
      let value = Int64.of_int (ex ctx 15 11) in
      let cs = O.Immediate Imm.{value; kind= Literal; size= -1} in
      [|rd'; cs|])

  let make_rel16 ctx = [|rel16 ctx|] [@@inline]

  let make_rel26 ctx = [|rel26 ctx|] [@@inline]

  let make_abs26 ctx = [|abs26 ctx|] [@@inline]

  let make_rs ctx = Option.(rs ctx >>| fun rs' -> [|rs'|])

  let make_rd ctx = Option.(rd ctx >>| fun rd' -> [|rd'|])

  let make_rt ctx = Option.(rt ctx >>| fun rt' -> [|rt'|])

  let make_rdrs ctx =
    Option.(rd ctx >>= fun rd' -> rs ctx >>| fun rs' -> [|rd'; rs'|])

  let make_rdrtrs ctx =
    Option.(
      rd ctx
      >>= fun rd' ->
      rt ctx >>= fun rt' -> rs ctx >>| fun rs' -> [|rd'; rt'; rs'|])

  let make_rdrsrt ctx =
    Option.(
      rd ctx
      >>= fun rd' ->
      rs ctx >>= fun rs' -> rt ctx >>| fun rt' -> [|rd'; rs'; rt'|])

  let make_rsrt ctx =
    Option.(rs ctx >>= fun rs' -> rt ctx >>| fun rt' -> [|rs'; rt'|])

  let make_rdrt ctx =
    Option.(rd ctx >>= fun rd' -> rt ctx >>| fun rt' -> [|rd'; rt'|])

  let make_rtrd ctx =
    Option.(rt ctx >>= fun rt' -> rd ctx >>| fun rd' -> [|rt'; rd'|])

  let make_rtrdimmsel hi lo ctx =
    Option.(
      rt ctx >>| fun rt' -> [|rt'; immsel ctx 15 11; immsel ctx hi lo|])

  let make_rsrtrel16 ctx =
    Option.(
      rs ctx >>= fun rs' -> rt ctx >>| fun rt' -> [|rs'; rt'; rel16 ctx|])

  let make_rsrtimm16 size ctx =
    Option.(
      rs ctx >>= fun rs' -> rt ctx >>| fun rt' -> [|rs'; rt'; imm16 size ctx|])

  let make_rsrel16 ctx = Option.(rs ctx >>| fun rs' -> [|rs'; rel16 ctx|])

  let make_rsimm16 size ctx =
    Option.(rs ctx >>| fun rs' -> [|rs'; imm16 size ctx|])

  let make_rsimm18 size ctx =
    Option.(rs ctx >>| fun rs' -> [|rs'; imm18 size ctx|])

  let make_rsimm19 size ctx =
    Option.(rs ctx >>| fun rs' -> [|rs'; imm19 size ctx|])

  let make_rsrel21 ctx = Option.(rs ctx >>| fun rs' -> [|rs'; rel21 ctx|])

  let make_rtrel16 ctx = Option.(rt ctx >>| fun rt' -> [|rt'; rel16 ctx|])

  let make_ftrel16 ctx = Option.(ft ctx >>| fun ft' -> [|ft'; rel16 ctx|])

  let make_wtrel16 ctx = Option.(wt ctx >>| fun wt' -> [|wt'; rel16 ctx|])

  let make_rtimm16 size ctx =
    Option.(rt ctx >>| fun rt' -> [|rt'; imm16 size ctx|])

  let make_rtrsimm16 size ctx =
    Option.(
      rt ctx >>= fun rt' -> rs ctx >>| fun rs' -> [|rt'; rs'; imm16 size ctx|])

  let make_wdrs ctx =
    Option.(
      wd ctx
      >>= fun wd' ->
      rd ctx
      >>| fun rs' ->
      (* we actually select 15-11 *)
      [|wd'; rs'|])

  let make_wdws ctx =
    Option.(wd ctx >>= fun wd' -> ws ctx >>| fun ws' -> [|wd'; ws'|])

  let make_wdwsm hi lo size ctx =
    Option.(
      wd ctx
      >>= fun wd' ->
      ws ctx
      >>| fun ws' ->
      let value = Int64.of_int (ex ctx hi lo) in
      let m = O.Immediate Imm.{value; kind= Literal; size} in
      [|wd'; ws'; m|])

  let make_slidewdnws0 hi lo ctx =
    Option.(
      wr106 ctx
      >>= fun wd' ->
      wr1511 ctx
      >>| fun ws' ->
      let sl1 = O.(Slide {vector= wd'; column= Slide_n (ex ctx hi lo)}) in
      let sl2 = O.(Slide {vector= ws'; column= Slide_n 0}) in
      [|sl1; sl2|])

  let make_sliderdwsn hi lo ctx =
    Option.(
      rd ctx
      >>= fun rd' ->
      wr1511 ctx
      >>| fun vector ->
      let column = O.Slide_n (ex ctx hi lo) in
      let sl = O.(Slide {vector; column}) in
      [|rd'; sl|])

  let make_slidewdnrs hi lo ctx =
    Option.(
      wr106 ctx
      >>= fun vector ->
      rd ctx
      >>| fun rs' ->
      (* we actually select 15-11 *)
      let column = O.Slide_n (ex ctx hi lo) in
      let sl = O.(Slide {vector; column}) in
      [|sl; rs'|])

  let make_slidewdwsn hi lo ctx =
    Option.(
      wd ctx
      >>= fun wd' ->
      wr1511 ctx
      >>| fun vector ->
      let column = O.Slide_n (ex ctx hi lo) in
      let sl = O.(Slide {vector; column}) in
      [|wd'; sl|])

  let make_wdwswt ctx =
    Option.(
      wd ctx
      >>= fun wd' ->
      ws ctx >>= fun ws' -> wt ctx >>| fun wt' -> [|wd'; ws'; wt'|])

  let make_slidewdwsrt ctx =
    Option.(
      wd ctx
      >>= fun wd' ->
      wr1511 ctx
      >>= fun vector ->
      gpr2016 ctx
      >>| fun r ->
      let column = O.Slide_r r in
      let sl = O.(Slide {vector; column}) in
      [|wd'; sl|])

  let make_wdwsu5 size ctx =
    Option.(
      wd ctx >>= fun wd' -> ws ctx >>| fun ws' -> [|wd'; ws'; u5 size ctx|])

  let make_wdwss5 size ctx =
    Option.(
      wd ctx >>= fun wd' -> ws ctx >>| fun ws' -> [|wd'; ws'; s5 size ctx|])

  let make_wdwsi8 ctx =
    Option.(
      wd ctx
      >>= fun wd' ->
      ws ctx
      >>| fun ws' ->
      let value = Int64.of_int (ex ctx 23 16) in
      let i8 = O.Immediate Imm.{value; kind= Literal; size= 8} in
      [|wd'; ws'; i8|])

  let make_wds10 size ctx =
    Option.(wd ctx >>| fun wd' -> [|wd'; s10 size ctx|])

  let make_membaseoff size ctx =
    Option.(membaseoff ctx size >>| fun mem -> [|mem|])

  let make_rtmembaseoff size ctx =
    Option.(
      rt ctx
      >>= fun rt' -> membaseoff ctx (Some size) >>| fun mem -> [|rt'; mem|])

  let make_rtmembaseoff9 size ctx =
    Option.(
      rt ctx
      >>= fun rt' -> membaseoff9 ctx (Some size) >>| fun mem -> [|rt'; mem|])

  let make_rtmembaseoff11 size ctx =
    Option.(
      rt ctx
      >>= fun rt' -> membaseoff11 ctx (Some size) >>| fun mem -> [|rt'; mem|])

  let make_ftmembaseoff size ctx =
    Option.(
      ft ctx
      >>= fun ft' -> membaseoff ctx (Some size) >>| fun mem -> [|ft'; mem|])

  let make_fdmembaseindex size ctx =
    Option.(
      fd ctx
      >>= fun fd' ->
      gpr2521 ctx
      >>= fun base ->
      gpr2016 ctx
      >>| fun index ->
      let mem =
        O.Memory {base; addend= Some (Index index); size= Some size}
      in
      [|fd'; mem|])

  let make_hintmembaseoff ctx =
    Option.(
      membaseoff ctx None
      >>| fun mem ->
      let hint =
        O.Immediate
          Imm.
            { value= Int64.of_int (ex ctx 20 16)
            ; kind= Literal
            ; size= ctx.opsz }
      in
      [|hint; mem|])

  let make_hintmembaseoff9 ctx =
    Option.(
      membaseoff9 ctx None
      >>| fun mem ->
      let hint =
        O.Immediate
          Imm.
            { value= Int64.of_int (ex ctx 20 16)
            ; kind= Literal
            ; size= ctx.opsz }
      in
      [|hint; mem|])

  let make_hintmembaseindex ctx =
    Option.(
      gpr2521 ctx
      >>= fun base ->
      gpr2016 ctx
      >>| fun index ->
      let hint =
        O.Immediate
          Imm.
            { value= Int64.of_int (ex ctx 20 16)
            ; kind= Literal
            ; size= ctx.opsz }
      in
      let mem = O.Memory {base; addend= Some (Index index); size= None} in
      [|hint; mem|])

  let make_rtrdmembase size ctx =
    Option.(
      gpr2521 ctx
      >>= fun base ->
      rt ctx
      >>= fun rt' ->
      rd ctx
      >>| fun rd' ->
      let mem = O.Memory {base; addend= None; size= Some size} in
      [|rt'; rd'; mem|])

  let make_rdrtsa ctx =
    Option.(rd ctx >>= fun rd' -> rt ctx >>| fun rt' -> [|rd'; rt'; sa ctx|])

  let make_rdrsrtsa ctx =
    Option.(
      rd ctx
      >>= fun rd' ->
      rs ctx >>= fun rs' -> rt ctx >>| fun rt' -> [|rd'; rs'; rt'; sa ctx|])

  let make_rdrsrtbp ctx =
    Option.(
      rd ctx
      >>= fun rd' ->
      rs ctx >>= fun rs' -> rt ctx >>| fun rt' -> [|rd'; rs'; rt'; bp ctx|])

  let make_rtrspossize ctx =
    Option.(
      rt ctx
      >>= fun rt' ->
      rs ctx
      >>| fun rs' ->
      let p, s = pos_size ctx in
      [|rt'; rs'; p; s|])

  let make_rtrspossize2 ctx =
    Option.(
      rt ctx
      >>= fun rt' ->
      rs ctx
      >>| fun rs' ->
      let p, s = pos_size2 ctx in
      [|rt'; rs'; p; s|])

  let make_rtrspossize3 ctx =
    Option.(
      rt ctx
      >>= fun rt' ->
      rs ctx
      >>| fun rs' ->
      let p, s = pos_size3 ctx in
      [|rt'; rs'; p; s|])

  let make_rtrspossize4 ctx =
    Option.(
      rt ctx
      >>= fun rt' ->
      rs ctx
      >>| fun rs' ->
      let p, s = pos_size4 ctx in
      [|rt'; rs'; p; s|])

  let make_rtrspossize5 ctx =
    Option.(
      rt ctx
      >>= fun rt' ->
      rs ctx
      >>| fun rs' ->
      let p, s = pos_size5 ctx in
      [|rt'; rs'; p; s|])

  let make_rtrspossize6 ctx =
    Option.(
      rt ctx
      >>= fun rt' ->
      rs ctx
      >>| fun rs' ->
      let p, s = pos_size6 ctx in
      [|rt'; rs'; p; s|])

  let make_rtfs ctx =
    Option.(rt ctx >>= fun rt' -> fs ctx >>| fun fs' -> [|rt'; fs'|])

  let make_ccoff ?(reg = None) hi lo ctx =
    let open Option.Let_syntax in
    let cc = ex ctx hi lo in
    let%map cc =
      match reg with
      | Some reg -> reg cc
      | None ->
          let value = Int64.of_int cc in
          O.Immediate Imm.{value; kind= Literal; size= ctx.opsz} |> return
    in
    [|cc; rel16 ctx|]

  let make_fsft ctx =
    Option.(fs ctx >>= fun fs' -> ft ctx >>| fun ft' -> [|fs'; ft'|])

  let make_fdfs ctx =
    Option.(fd ctx >>= fun fd' -> fs ctx >>| fun fs' -> [|fd'; fs'|])

  let make_fdfsrt ctx =
    Option.(
      fd ctx
      >>= fun fd' ->
      fs ctx >>= fun fs' -> rt ctx >>| fun rt' -> [|fd'; fs'; rt'|])

  let make_fdfscc ctx =
    Option.(
      fd ctx
      >>= fun fd' ->
      fs ctx >>= fun fs' -> cc2 ctx >>| fun cc' -> [|fd'; fs'; cc'|])

  let make_ccfsft ctx =
    Option.(
      cc1 ctx
      >>= fun cc' ->
      fs ctx >>= fun fs' -> ft ctx >>| fun ft' -> [|cc'; fs'; ft'|])

  let make_rdrscc ctx =
    Option.(
      rd ctx
      >>= fun rd' ->
      rs ctx >>= fun rs' -> cc2 ctx >>| fun cc' -> [|rd'; rs'; cc'|])

  let make_fdfsft ctx =
    Option.(
      fd ctx
      >>= fun fd' ->
      fs ctx >>= fun fs' -> ft ctx >>| fun ft' -> [|fd'; fs'; ft'|])

  let make_fdfsftrs ctx =
    Option.(
      fd ctx
      >>= fun fd' ->
      fs ctx
      >>= fun fs' ->
      ft ctx >>= fun ft' -> rs ctx >>| fun rs' -> [|fd'; fs'; ft'; rs'|])

  let make_fdfrfsft ctx =
    Option.(
      fd ctx
      >>= fun fd' ->
      fr ctx
      >>= fun fr' ->
      fs ctx >>= fun fs' -> ft ctx >>| fun ft' -> [|fd'; fr'; fs'; ft'|])

  let make_mems10rswd size ctx =
    Option.(
      gpr1511 ctx
      >>= fun base ->
      wd ctx
      >>| fun wd' ->
      let disp = Int64.of_int (ex ctx 25 16) in
      let disp = sign_ext disp 10 in
      let size = Some size in
      let mem = O.(Memory {base; addend= Some (Disp disp); size}) in
      [|wd'; mem|])

  let nd ctx = tst ctx 17 [@@inline]

  let tf ctx = tst ctx 16 [@@inline]

  let cc ctx x = ex ctx 10 8 = x [@@inline]

  let chk10to0 ctx x = ex ctx 10 0 = x [@@inline]

  let chk10to3 ctx x = ex ctx 10 3 = x [@@inline]

  let chk10to6 ctx x = ex ctx 10 6 = x [@@inline]

  let chk15to6 ctx x = ex ctx 15 6 = x [@@inline]

  let chk20to16 ctx x = ex ctx 20 16 = x [@@inline]

  let chk25to21 ctx x = ex ctx 25 21 = x [@@inline]

  let ops_or_error ctx f =
    Result.of_option (f ctx) ~error:`BadOperand
    [@@inline]

  let parse_nop ctx =
    match ex ctx 10 6 with
    | 0 -> Ok (M.NOP, None, None, e)
    | 1 -> Ok (M.SSNOP, None, None, e)
    | 3 -> Ok (M.EHB, None, None, e)
    | 5 -> Ok (M.PAUSE, None, None, e)
    | _ -> Error `Invalid

  let parse_dsub_sll ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 11, ex ctx 10 6, ex ctx 5 0) with
    | 0, _, _ -> parse_nop ctx
    | _ when ex ctx 25 21 = 0 ->
        let%map ops = o make_rdrtsa in
        (M.SLL, None, None, ops)
    | _, 0, 0b101110 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrsrt in
          (M.DSUB, None, None, ops)
      | _ -> Error `Invalid )
    | _ -> Error `Invalid

  let parse_jalr ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 15 11, tst ctx 10) with
    | 0, false ->
        let%map ops = o make_rs in
        (M.JR, None, None, ops)
    | 31, false ->
        let%map ops = o make_rs in
        (M.JALR, None, None, ops)
    | 31, true when is_rel2or6 ctx ->
        let%map ops = o make_rs in
        (M.JALRHB, None, None, ops)
    | _, false ->
        let%map ops = o make_rdrs in
        (M.JALR, None, None, ops)
    | _, true when is_rel2or6 ctx ->
        let%map ops = o make_rdrs in
        (M.JALRHB, None, None, ops)
    | _ -> Error `Invalid

  let parse_jr ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match tst ctx 10 with
    | false ->
        let%map ops = o make_rs in
        (M.JR, None, None, ops)
    | true ->
        let%map ops = o make_rs in
        (M.JRHB, None, None, ops)

  let parse_r2clz ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 10 6 with
    | 0b00000 ->
        let%map ops = o make_rdrs in
        (M.CLZ, None, None, ops)
    | _ -> Error `Invalid

  let parse_r6clz ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 20 16, ex ctx 10 6) with
    | 0b00000, 0b00001 ->
        let%map ops = o make_rdrs in
        (M.CLZ, None, None, ops)
    | _ -> Error `Invalid

  let parse_mfhi ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (is_rel6 ctx, ex ctx 25 16, ex ctx 10 6) with
    | false, 0, 0 ->
        let%map ops = o make_rd in
        (M.MFHI, None, None, ops)
    | true, _, 1 -> parse_r6clz ctx
    | _ -> Error `Invalid

  let parse_mthi ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (is_rel6 ctx, ex ctx 20 16, ex ctx 15 11, ex ctx 10 6) with
    | false, 0, 0, 0 ->
        let%map ops = o make_rs in
        (M.MTHI, None, None, ops)
    | true, 0, _, 1 ->
        let%map ops = o make_rdrs in
        (M.CLO, None, None, ops)
    | _ -> Error `Invalid

  let parse_r2dclz ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 10 6 with
    | 0b00000 ->
        let%map ops = o make_rdrs in
        (M.DCLZ, None, None, ops)
    | _ -> Error `Invalid

  let parse_r6dclz ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 20 16, ex ctx 10 6) with
    | 0b00000, 0b00001 ->
        let%map ops = o make_rdrs in
        (M.DCLZ, None, None, ops)
    | _ -> Error `Invalid

  let parse_mflo ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (is_rel6 ctx, ex ctx 25 16, ex ctx 10 6) with
    | false, 0, 0 ->
        let%map ops = o make_rd in
        (M.MFLO, None, None, ops)
    | true, _, 1 -> parse_r6dclz ctx
    | _ -> Error `Invalid

  let parse_mtlo ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (is_rel6 ctx, ex ctx 25 16, ex ctx 10 6) with
    | false, 0, 0 ->
        let%map ops = o make_rd in
        (M.MFLO, None, None, ops)
    | true, _, 1 -> parse_r6dclz ctx
    | _ -> Error `Invalid

  let parse_mthi ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 20 16, ex ctx 15 11, ex ctx 10 6) with
    | 0, 0, 0 when not (is_rel6 ctx) ->
        let%map ops = o make_rs in
        (M.MTLO, None, None, ops)
    | 0, _, 1 when Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o make_rdrs in
        (M.DCLO, None, None, ops)
    | _ -> Error `Invalid

  let parse_movci ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (nd ctx, tf ctx) with
    | false, false ->
        let%map ops = o make_rdrscc in
        (M.MOVF, None, None, ops)
    | false, true ->
        let%map ops = o make_rdrscc in
        (M.MOVT, None, None, ops)
    | _ -> Error `Invalid

  let parse_special ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 -> parse_dsub_sll ctx
    | 0b000001 when chk10to6 ctx 0 -> parse_movci ctx
    | 0b000010 when chk25to21 ctx 0 ->
        let%map ops = o make_rdrtsa in
        (M.SRL, None, None, ops)
    | 0b000010 when chk25to21 ctx 1 ->
        let%map ops = o make_rdrtsa in
        (M.ROTR, None, None, ops)
    | 0b000011 when chk25to21 ctx 0 ->
        let%map ops = o make_rdrtsa in
        (M.SRA, None, None, ops)
    | 0b000100 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrtrs in
        (M.SLLV, None, None, ops)
    | 0b000101 when ex ctx 10 8 = 0 && is_rel6 ctx ->
        let%map ops = o make_rdrsrtsa in
        (M.LSA, None, None, ops)
    | 0b000110 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrtrs in
        (M.SRLV, None, None, ops)
    | 0b000101 when chk10to6 ctx 1 && is_rel2or6 ctx ->
        let%map ops = o make_rdrtrs in
        (M.ROTRV, None, None, ops)
    | 0b000111 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrtrs in
        (M.SRAV, None, None, ops)
    | 0b001000 -> parse_jr ctx
    | 0b001001 -> parse_jalr ctx
    | 0b001010 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.MOVZ, None, None, ops)
    | 0b001011 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.MOVN, None, None, ops)
    | 0b001100 -> Ok (M.SYSCALL, None, None, e)
    | 0b001101 ->
        let value = Int64.of_int @@ ex ctx 25 6 in
        let imm = O.Immediate Imm.{value; kind= Literal; size= 20} in
        Ok (M.BREAK, None, None, [|imm|])
    | 0b001110 when is_rel6 ctx ->
        let value = Int64.of_int @@ ex ctx 25 6 in
        let imm = O.Immediate Imm.{value; kind= Literal; size= 20} in
        Ok (M.SDBBP, None, None, [|imm|])
    | 0b001111 when ex ctx 25 11 = 0 ->
        let value = Int64.of_int @@ ex ctx 10 6 in
        let imm = O.Immediate Imm.{value; kind= Literal; size= 20} in
        Ok (M.SYNC, None, None, [|imm|])
    | 0b010000 -> parse_mfhi ctx
    | 0b010001 -> parse_mthi ctx
    | 0b010010 -> parse_mflo ctx
    | 0b010011 -> parse_mtlo ctx
    | 0b010100 when chk10to6 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtrs in
          (M.DSLLV, None, None, ops)
      | _ -> Error `Invalid )
    | 0b010101 when ex ctx 10 8 = 0 && Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o make_rdrsrtsa in
        (M.DLSA, None, None, ops)
    | 0b010110 when chk10to6 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtrs in
          (M.DSRLV, None, None, ops)
      | _ -> Error `Invalid )
    | 0b010110 when chk10to6 ctx 1 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtrs in
          (M.DROTRV, None, None, ops)
      | _ -> Error `Invalid )
    | 0b010111 when chk10to6 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtrs in
          (M.DSRAV, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011000 when chk15to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rsrt in
        (M.MULT, None, None, ops)
    | 0b011000 when chk10to6 ctx 0b00010 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.MUL, None, None, ops)
    | 0b011000 when chk10to6 ctx 0b00011 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.MUH, None, None, ops)
    | 0b011001 when chk15to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rsrt in
        (M.MULTU, None, None, ops)
    | 0b011001 when chk10to6 ctx 0b00010 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.MULU, None, None, ops)
    | 0b011001 when chk10to6 ctx 0b00011 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.MUHU, None, None, ops)
    | 0b011010 when chk15to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rsrt in
        (M.DIV, None, None, ops)
    | 0b011010 when chk10to6 ctx 0b00010 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.DIV, None, None, ops)
    | 0b011010 when chk10to6 ctx 0b00011 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.MOD, None, None, ops)
    | 0b011011 when chk15to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rsrt in
        (M.DIVU, None, None, ops)
    | 0b011011 when chk10to6 ctx 0b00010 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.DIVU, None, None, ops)
    | 0b011011 when chk10to6 ctx 0b00011 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.MODU, None, None, ops)
    | 0b011100 -> (
      match ctx.mode with
      | (Mode.Mips64 | Mode.Mips64r2) when chk15to6 ctx 0 ->
          let%map ops = o make_rsrt in
          (M.DMULT, None, None, ops)
      | Mode.Mips64r6 when chk10to6 ctx 0b00010 ->
          let%map ops = o make_rdrsrt in
          (M.DMUL, None, None, ops)
      | Mode.Mips64r6 when chk10to6 ctx 0b00011 ->
          let%map ops = o make_rdrsrt in
          (M.DMUH, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011101 -> (
      match ctx.mode with
      | (Mode.Mips64 | Mode.Mips64r2) when chk15to6 ctx 0 ->
          let%map ops = o make_rsrt in
          (M.DMULTU, None, None, ops)
      | Mode.Mips64r6 when chk10to6 ctx 0b00010 ->
          let%map ops = o make_rdrsrt in
          (M.DMULU, None, None, ops)
      | Mode.Mips64r6 when chk10to6 ctx 0b00011 ->
          let%map ops = o make_rdrsrt in
          (M.DMUHU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011110 -> (
      match ctx.mode with
      | (Mode.Mips64 | Mode.Mips64r2) when chk15to6 ctx 0 ->
          let%map ops = o make_rsrt in
          (M.DDIV, None, None, ops)
      | Mode.Mips64r6 when chk10to6 ctx 0b00010 ->
          let%map ops = o make_rdrsrt in
          (M.DDIV, None, None, ops)
      | Mode.Mips64r6 when chk10to6 ctx 0b00011 ->
          let%map ops = o make_rdrsrt in
          (M.DMOD, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011111 -> (
      match ctx.mode with
      | (Mode.Mips64 | Mode.Mips64r2) when chk15to6 ctx 0 ->
          let%map ops = o make_rsrt in
          (M.DDIVU, None, None, ops)
      | Mode.Mips64r6 when chk10to6 ctx 0b00010 ->
          let%map ops = o make_rdrsrt in
          (M.DDIVU, None, None, ops)
      | Mode.Mips64r6 when chk10to6 ctx 0b00011 ->
          let%map ops = o make_rdrsrt in
          (M.DMODU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b100000 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.ADD, None, None, ops)
    | 0b100001 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.ADDU, None, None, ops)
    | 0b100011 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.SUBU, None, None, ops)
    | 0b100100 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.AND, None, None, ops)
    | 0b100101 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.OR, None, None, ops)
    | 0b100110 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.XOR, None, None, ops)
    | 0b100111 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.NOR, None, None, ops)
    | 0b101010 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.SLT, None, None, ops)
    | 0b101011 when chk10to6 ctx 0 ->
        let%map ops = o make_rdrsrt in
        (M.SLTU, None, None, ops)
    | 0b101100 when chk10to6 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrsrt in
          (M.DADD, None, None, ops)
      | _ -> Error `Invalid )
    | 0b101101 when chk10to6 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrsrt in
          (M.DADDU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b101111 when chk10to6 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrsrt in
          (M.DSUBU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b110000 ->
        let%map ops = o make_rsrt in
        (M.TGE, None, None, ops)
    | 0b110001 ->
        let%map ops = o make_rsrt in
        (M.TGEU, None, None, ops)
    | 0b110010 ->
        let%map ops = o make_rsrt in
        (M.TLT, None, None, ops)
    | 0b110011 ->
        let%map ops = o make_rsrt in
        (M.TLTU, None, None, ops)
    | 0b110100 ->
        let%map ops = o make_rsrt in
        (M.TEQ, None, None, ops)
    | 0b110101 when chk10to6 ctx 0 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.SELEQZ, None, None, ops)
    | 0b110110 ->
        let%map ops = o make_rsrt in
        (M.TNE, None, None, ops)
    | 0b110111 when chk10to6 ctx 0 && is_rel6 ctx ->
        let%map ops = o make_rdrsrt in
        (M.SELNEZ, None, None, ops)
    | 0b111000 when chk25to21 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtsa in
          (M.DSLL, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111010 when chk25to21 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtsa in
          (M.DSRL, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111010 when chk25to21 ctx 1 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtsa in
          (M.DROTR, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111011 when chk25to21 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtsa in
          (M.DSRA, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111100 when chk25to21 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtsa in
          (M.DSLL32, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111110 when chk25to21 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtsa in
          (M.DSRL32, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111110 when chk25to21 ctx 1 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtsa in
          (M.DROTR32, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111111 when chk25to21 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrtsa in
          (M.DSRA32, None, None, ops)
      | _ -> Error `Invalid )
    | _ -> Error `Invalid

  let parse_bal ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 21 with
    | 0 when is_rel6 ctx -> Ok (M.BAL, None, None, make_rel16 ctx)
    | _ when not (is_rel6 ctx) ->
        let%map ops = o make_rsrel16 in
        (M.BGEZAL, None, None, ops)
    | _ -> Error `Invalid

  let parse_regimm ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 20 16 with
    | 0b00000 ->
        let%map ops = o make_rsrel16 in
        (M.BLTZ, None, None, ops)
    | 0b00001 ->
        let%map ops = o make_rsrel16 in
        (M.BGEZ, None, None, ops)
    | 0b00010 when not (is_rel6 ctx) ->
        let%map ops = o make_rsrel16 in
        (M.BLTZL, None, None, ops)
    | 0b00011 when not (is_rel6 ctx) ->
        let%map ops = o make_rsrel16 in
        (M.BGEZL, None, None, ops)
    | 0b00110 when Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o (make_rsimm16 64) in
        (M.DAHI, None, None, ops)
    | 0b01000 when not (is_rel6 ctx) ->
        let%map ops = o (make_rsimm16 ctx.opsz) in
        (M.TGEI, None, None, ops)
    | 0b01001 when not (is_rel6 ctx) ->
        let%map ops = o (make_rsimm16 ctx.opsz) in
        (M.TGEIU, None, None, ops)
    | 0b01010 when not (is_rel6 ctx) ->
        let%map ops = o (make_rsimm16 ctx.opsz) in
        (M.TLTI, None, None, ops)
    | 0b01011 when not (is_rel6 ctx) ->
        let%map ops = o (make_rsimm16 ctx.opsz) in
        (M.TLTIU, None, None, ops)
    | 0b01100 when not (is_rel6 ctx) ->
        let%map ops = o (make_rsimm16 ctx.opsz) in
        (M.TEQI, None, None, ops)
    | 0b01110 when not (is_rel6 ctx) ->
        let%map ops = o (make_rsimm16 ctx.opsz) in
        (M.TNEI, None, None, ops)
    | 0b10000 when not (is_rel6 ctx) ->
        let%map ops = o make_rsrel16 in
        (M.BLTZAL, None, None, ops)
    | 0b10000 when is_rel6 ctx -> Ok (M.NAL, None, None, e)
    | 0b10001 -> parse_bal ctx
    | 0b10010 when not (is_rel6 ctx) ->
        let%map ops = o make_rsrel16 in
        (M.BLTZALL, None, None, ops)
    | 0b10011 when not (is_rel6 ctx) ->
        let%map ops = o make_rsrel16 in
        (M.BGEZALL, None, None, ops)
    | 0b10111 when chk25to21 ctx 0 && is_rel6 ctx ->
        Ok (M.SIGRIE, None, None, [|imm16 16 ctx|])
    | 0b11110 when Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o (make_rsimm16 64) in
        (M.DATI, None, None, ops)
    | 0b11111 when is_rel2or6 ctx ->
        let%map ops = o (make_membaseoff None) in
        (M.SYNCI, None, None, ops)
    | _ -> Error `Invalid

  let parse_special2 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 when chk15to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rsrt in
        (M.MADD, None, None, ops)
    | 0b000001 when chk15to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rsrt in
        (M.MADDU, None, None, ops)
    | 0b000010 when chk10to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rdrsrt in
        (M.MUL, None, None, ops)
    | 0b000100 when chk15to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rsrt in
        (M.MSUB, None, None, ops)
    | 0b000101 when chk15to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rsrt in
        (M.MSUBU, None, None, ops)
    | 0b100000 -> parse_r2clz ctx
    | 0b100001 when chk10to6 ctx 0 && not (is_rel6 ctx) ->
        let%map ops = o make_rdrs in
        (M.CLO, None, None, ops)
    | 0b100100 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 -> parse_r2dclz ctx
      | _ -> Error `Invalid )
    | 0b100101 -> (
      match ctx.mode with
      | (Mode.Mips64 | Mode.Mips64r2) when chk10to6 ctx 0 ->
          let%map ops = o make_rdrs in
          (M.DCLO, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111111 when not (is_rel6 ctx) ->
        let value = Int64.of_int @@ ex ctx 25 6 in
        let imm = O.Immediate Imm.{value; kind= Literal; size= 20} in
        Ok (M.SDBBP, None, None, [|imm|])
    | _ -> Error `Invalid

  let parse_signext ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 21, ex ctx 10 8, ex ctx 7 6) with
    | 0, 0, 0 when is_rel6 ctx ->
        ctx.opsz <- 32;
        let%map ops = o make_rdrt in
        (M.BITSWAP, None, None, ops)
    | _, 2, _ when is_rel6 ctx ->
        ctx.opsz <- 32;
        let%map ops = o make_rdrsrtbp in
        (M.ALIGN, None, None, ops)
    | 0, 4, 0 ->
        let%map ops = o make_rdrt in
        (M.SEB, None, None, ops)
    | 0, 6, 0 ->
        let%map ops = o make_rdrt in
        (M.SEH, None, None, ops)
    | 0, 0, 2 when is_rel2or6 ctx ->
        let%map ops = o make_rdrt in
        (M.WSBH, None, None, ops)
    | _ -> Error `Invalid

  let parse_special3 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 ->
        let%map ops = o make_rtrspossize2 in
        (M.EXT, None, None, ops)
    | 0b000001 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rtrspossize5 in
          (M.DEXTM, None, None, ops)
      | _ -> Error `Invalid )
    | 0b000010 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rtrspossize6 in
          (M.DEXTU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b000011 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rtrspossize2 in
          (M.DEXT, None, None, ops)
      | _ -> Error `Invalid )
    | 0b000100 ->
        let%map ops = o make_rtrspossize in
        (M.INS, None, None, ops)
    | 0b000101 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rtrspossize3 in
          (M.DINSM, None, None, ops)
      | _ -> Error `Invalid )
    | 0b000110 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rtrspossize4 in
          (M.DINSU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b000111 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rtrspossize in
          (M.DINS, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011001 when (not (tst ctx 6)) && not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff9 32) in
        (M.LWLE, None, None, ops)
    | 0b011010 when (not (tst ctx 6)) && not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff9 32) in
        (M.LWRE, None, None, ops)
    | 0b011011 when not (tst ctx 6) ->
        let%map ops = o make_hintmembaseoff9 in
        (M.CACHEE, None, None, ops)
    | 0b011100 ->
        let%map ops = o (make_rtmembaseoff9 8) in
        ctx.opsz <- 8;
        (M.SBE, None, None, ops)
    | 0b011101 ->
        let%map ops = o (make_rtmembaseoff9 16) in
        ctx.opsz <- 16;
        (M.SHE, None, None, ops)
    | 0b011110 when tst ctx 6 && ex ctx 10 7 = 0 && is_rel6 ctx ->
        let%map ops = o (make_rtrdmembase 64) in
        ctx.opsz <- 64;
        (M.SCWPE, None, None, ops)
    | 0b011110 ->
        let%map ops = o (make_rtmembaseoff9 32) in
        ctx.opsz <- 32;
        (M.SCE, None, None, ops)
    | 0b011111 ->
        let%map ops = o (make_rtmembaseoff9 32) in
        ctx.opsz <- 32;
        (M.SWE, None, None, ops)
    | 0b100000 -> parse_signext ctx
    | 0b100001 when (not (tst ctx 6)) && not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff9 32) in
        ctx.opsz <- 32;
        (M.SWLE, None, None, ops)
    | 0b100010 when (not (tst ctx 6)) && not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff9 32) in
        ctx.opsz <- 32;
        (M.SWRE, None, None, ops)
    | 0b100011 when not (tst ctx 6) ->
        let%map ops = o make_hintmembaseoff9 in
        (M.PREFE, None, None, ops)
    | 0b100100
      when chk25to21 ctx 0 && chk10to6 ctx 0
           && Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o make_rdrt in
        (M.DBITSWAP, None, None, ops)
    | 0b100100 when chk25to21 ctx 0 && chk10to6 ctx 0b00010 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrt in
          (M.DSBH, None, None, ops)
      | _ -> Error `Invalid )
    | 0b100100 when chk25to21 ctx 0 && chk10to6 ctx 0b00101 -> (
      match ctx.mode with
      | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rdrt in
          (M.DSHD, None, None, ops)
      | _ -> Error `Invalid )
    | 0b100100 when ex ctx 10 9 = 1 && Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o make_rdrsrtbp in
        (M.DALIGN, None, None, ops)
    | 0b100101 when (not (tst ctx 6)) && is_rel6 ctx ->
        let%map ops = o make_hintmembaseoff9 in
        (M.CACHE, None, None, ops)
    | 0b100110 when (not (tst ctx 6)) && is_rel6 ctx ->
        let%map ops = o (make_rtmembaseoff9 32) in
        ctx.opsz <- 32;
        (M.SC, None, None, ops)
    | 0b100110 when tst ctx 6 && ex ctx 10 7 = 0 && is_rel6 ctx ->
        let%map ops = o (make_rtrdmembase 64) in
        ctx.opsz <- 64;
        (M.SCWP, None, None, ops)
    | 0b100111 when (not (tst ctx 6)) && is_rel6 ctx ->
        let%map ops = o (make_rtmembaseoff9 64) in
        ctx.opsz <- 64;
        (M.SCD, None, None, ops)
    | 0b100111 when tst ctx 6 && ex ctx 10 7 = 0 && is_rel6 ctx ->
        let%map ops = o (make_rtrdmembase 128) in
        ctx.opsz <- 128;
        (M.SCDP, None, None, ops)
    | 0b101000 when not (tst ctx 6) ->
        let%map ops = o (make_rtmembaseoff9 8) in
        (M.LBUE, None, None, ops)
    | 0b101001 when not (tst ctx 6) ->
        let%map ops = o (make_rtmembaseoff9 16) in
        (M.LHUE, None, None, ops)
    | 0b101100 when not (tst ctx 6) ->
        let%map ops = o (make_rtmembaseoff9 8) in
        (M.LBE, None, None, ops)
    | 0b101101 when not (tst ctx 6) ->
        let%map ops = o (make_rtmembaseoff9 16) in
        (M.LHE, None, None, ops)
    | 0b101110 when not (tst ctx 6) ->
        let%map ops = o (make_rtmembaseoff9 32) in
        (M.LLE, None, None, ops)
    | 0b101110 when chk10to6 ctx 0b00001 && is_rel6 ctx ->
        let%map ops = o (make_rtrdmembase 64) in
        (M.LLWPE, None, None, ops)
    | 0b110101 when (not (tst ctx 6)) && is_rel6 ctx ->
        let%map ops = o make_hintmembaseoff9 in
        (M.PREF, None, None, ops)
    | 0b110110 when (not (tst ctx 6)) && is_rel6 ctx ->
        let%map ops = o (make_rtmembaseoff9 32) in
        (M.LL, None, None, ops)
    | 0b110110 when chk10to6 ctx 0b00001 && is_rel6 ctx ->
        let%map ops = o (make_rtrdmembase 64) in
        (M.LLWP, None, None, ops)
    | 0b110111 when (not (tst ctx 6)) && Mode.equal ctx.mode Mips64r6 ->
        let%map ops = o (make_rtmembaseoff9 64) in
        (M.LLD, None, None, ops)
    | 0b110111 when chk10to6 ctx 0b00001 && Mode.equal ctx.mode Mips64r6 ->
        let%map ops = o (make_rtrdmembase 128) in
        (M.LLDP, None, None, ops)
    | 0b111011 when chk25to21 ctx 0 && ex ctx 10 9 = 0 && is_rel2or6 ctx ->
        let%map ops = o (make_rtrdimmsel 8 6) in
        (M.RDHWR, None, None, ops)
    | _ -> Error `Invalid

  let parse_beq ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 16 with
    | 0 -> Ok (M.B, None, None, make_rel16 ctx)
    | _ ->
        let%map ops = o make_rsrtrel16 in
        (M.BEQ, None, None, ops)

  let parse_luiaui ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 21 with
    | 0 when is_rel6 ctx ->
        let%map ops = o (make_rtimm16 ctx.opsz) in
        (M.AUI, None, None, ops)
    | 0 ->
        let%map ops = o (make_rtimm16 ctx.opsz) in
        (M.LUI, None, None, ops)
    | _ when is_rel6 ctx ->
        let%map ops = o (make_rtrsimm16 ctx.opsz) in
        (M.AUI, None, None, ops)
    | _ -> Error `Invalid

  let parse_pop06 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 21, ex ctx 20 16) with
    | _, 0 ->
        let%map ops = o make_rsrel16 in
        (M.BLEZ, None, None, ops)
    | 0, n when n <> 0 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BLEZALC, None, None, ops)
    | n1, n2 when n1 <> 0 && n1 = n2 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BGEZALC, None, None, ops)
    | n1, n2 when n1 <> 0 && n2 <> 0 && n1 <> n2 && is_rel6 ctx ->
        let%map ops = o make_rsrtrel16 in
        (M.BGEUC, None, None, ops)
    | _ -> Error `Invalid

  let parse_pop07 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 21, ex ctx 20 16) with
    | _, 0 ->
        let%map ops = o make_rsrel16 in
        (M.BGTZ, None, None, ops)
    | 0, n when n <> 0 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BGTZALC, None, None, ops)
    | n1, n2 when n1 <> 0 && n1 = n2 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BLTZALC, None, None, ops)
    | n1, n2 when n1 <> 0 && n2 <> 0 && n1 <> n2 && is_rel6 ctx ->
        let%map ops = o make_rsrtrel16 in
        (M.BLTUC, None, None, ops)
    | _ -> Error `Invalid

  let parse_addi_pop10 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    let is_rel6 = is_rel6 ctx in
    match (ex ctx 25 21, ex ctx 20 16) with
    | 0, n when n <> 0 && is_rel6 ->
        let%map ops = o make_rtrel16 in
        (M.BEQZALC, None, None, ops)
    | n1, n2 when n1 <> 0 && n2 <> 0 && n1 < n2 && is_rel6 ->
        let%map ops = o make_rsrtrel16 in
        (M.BEQC, None, None, ops)
    | n1, n2 when n1 >= n2 && is_rel6 ->
        let%map ops = o make_rsrtrel16 in
        (M.BOVC, None, None, ops)
    | _ when not is_rel6 ->
        let%map ops = o (make_rtrsimm16 32) in
        (M.ADDI, None, None, ops)
    | _ -> Error `Invalid

  let parse_mfmc0 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 15 11, ex ctx 10 6, tst ctx 5, ex ctx 4 3, ex ctx 2 0) with
    | 12, 0, false, 0, 0 ->
        let%map ops = o make_rt in
        (M.DI, None, None, ops)
    | 12, 0, true, 0, 0 ->
        let%map ops = o make_rt in
        (M.EI, None, None, ops)
    | _ -> Error `Invalid

  let parse_cop0_when_rs_c0 ctx =
    match ex ctx 5 0 with
    | 0b000001 when tst ctx 25 && ex ctx 24 6 = 0 ->
        Ok (M.TLBR, None, None, e)
    | 0b000010 when tst ctx 25 && ex ctx 24 6 = 0 ->
        Ok (M.TLBWI, None, None, e)
    | 0b000011 when tst ctx 25 && ex ctx 24 6 = 0 ->
        Ok (M.TLBINV, None, None, e)
    | 0b000100 when tst ctx 25 && ex ctx 24 6 = 0 ->
        Ok (M.TLBINVF, None, None, e)
    | 0b000110 when tst ctx 25 && ex ctx 24 6 = 0 ->
        Ok (M.TLBWR, None, None, e)
    | 0b001000 when tst ctx 25 && ex ctx 24 6 = 0 ->
        Ok (M.TLBP, None, None, e)
    | 0b011000 when tst ctx 25 && ex ctx 24 6 = 0 ->
        Ok (M.ERET, None, None, e)
    | 0b011111 when tst ctx 25 && ex ctx 24 6 = 0 ->
        Ok (M.DERET, None, None, e)
    | 0b100000 when tst ctx 25 -> Ok (M.WAIT, None, None, e)
    | _ -> Error `Invalid

  let parse_cop0 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 21 with
    | 0b00000 when chk10to3 ctx 0 ->
        let%map ops = o (make_rtrdimmsel 2 0) in
        (M.MFC0, None, None, ops)
    | 0b00001 when chk10to3 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtrdimmsel 2 0) in
          (M.DMFC0, None, None, ops)
      | _ -> Error `Invalid )
    | 0b00010 when chk10to3 ctx 0 && is_rel6 ctx ->
        let%map ops = o (make_rtrdimmsel 2 0) in
        (M.MFHC0, None, None, ops)
    | 0b00100 when chk10to3 ctx 0 ->
        let%map ops = o (make_rtrdimmsel 2 0) in
        (M.MTC0, None, None, ops)
    | 0b00101 when chk10to3 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtrdimmsel 2 0) in
          (M.DMTC0, None, None, ops)
      | _ -> Error `Invalid )
    | 0b00110 when chk10to3 ctx 0 && is_rel6 ctx ->
        let%map ops = o (make_rtrdimmsel 2 0) in
        (M.MTHC0, None, None, ops)
    | 0b01010 when chk10to0 ctx 0 && is_rel2or6 ctx ->
        let%map ops = o make_rdrt in
        (M.RDPGPR, None, None, ops)
    | 0b01011 when is_rel2or6 ctx -> parse_mfmc0 ctx
    | 0b01110 when chk10to0 ctx 0 && is_rel2or6 ctx ->
        let%map ops = o make_rdrt in
        (M.WRPGPR, None, None, ops)
    | _ -> parse_cop0_when_rs_c0 ctx

  let parse_cop1_when_rs_s ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 ->
        let%map ops = o make_fdfsft in
        (M.ADD, None, Some F.S, ops)
    | 0b000001 ->
        let%map ops = o make_fdfsft in
        (M.SUB, None, Some F.S, ops)
    | 0b000010 ->
        let%map ops = o make_fdfsft in
        (M.MUL, None, Some F.S, ops)
    | 0b000011 ->
        let%map ops = o make_fdfsft in
        (M.DIV, None, Some F.S, ops)
    | 0b000100 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.SQRT, None, Some F.S, ops)
    | 0b000101 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.ABS, None, Some F.S, ops)
    | 0b000110 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.MOV, None, Some F.S, ops)
    | 0b000111 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.NEG, None, Some F.S, ops)
    | 0b001000 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.ROUNDL, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b001001 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.TRUNCL, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b001010 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.CEILL, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b001011 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.FLOORL, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b001100 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.ROUNDW, None, Some F.S, ops)
    | 0b001101 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.TRUNCW, None, Some F.S, ops)
    | 0b001110 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.CEILW, None, Some F.S, ops)
    | 0b001111 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.FLOORW, None, Some F.S, ops)
    | 0b010000 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.SEL, None, Some F.S, ops)
    | 0b010001 when (not (is_rel6 ctx)) && (not (nd ctx)) && not (tf ctx) ->
        let%map ops = o make_fdfscc in
        (M.MOVF, None, Some F.S, ops)
    | 0b010001 when (not (is_rel6 ctx)) && (not (nd ctx)) && tf ctx ->
        let%map ops = o make_fdfscc in
        (M.MOVT, None, Some F.S, ops)
    | 0b010010 when not (is_rel6 ctx) ->
        let%map ops = o make_fdfsrt in
        (M.MOVZ, None, Some F.S, ops)
    | 0b010011 when not (is_rel6 ctx) ->
        let%map ops = o make_fdfsrt in
        (M.MOVN, None, Some F.S, ops)
    | 0b010100 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.SELEQZ, None, Some F.S, ops)
    | 0b010101 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.RECIP, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b010110 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.RSQRT, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b010111 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.SELNEZ, None, Some F.S, ops)
    | 0b011000 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MADDF, None, Some F.S, ops)
    | 0b011001 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MSUBF, None, Some F.S, ops)
    | 0b011010 when chk20to16 ctx 0 && is_rel6 ctx ->
        let%map ops = o make_fdfs in
        (M.RINT, None, Some F.S, ops)
    | 0b011011 when chk20to16 ctx 0 && is_rel6 ctx ->
        let%map ops = o make_fdfs in
        (M.CLASS, None, Some F.S, ops)
    | 0b011100 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MIN, None, Some F.S, ops)
    | 0b011101 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MINA, None, Some F.S, ops)
    | 0b011110 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MAX, None, Some F.S, ops)
    | 0b011111 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MAXA, None, Some F.S, ops)
    | 0b100001 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.CVTD, None, Some F.S, ops)
    | 0b100100 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.CVTW, None, Some F.S, ops)
    | 0b100101 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.CVTL, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b100110 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfsft in
          (M.CVTPS, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b110000 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.F, Some F.S, ops)
    | 0b110001 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.UN, Some F.S, ops)
    | 0b110010 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.EQ, Some F.S, ops)
    | 0b110011 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.UEQ, Some F.S, ops)
    | 0b110100 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.OLT, Some F.S, ops)
    | 0b110101 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.ULT, Some F.S, ops)
    | 0b110110 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.OLE, Some F.S, ops)
    | 0b110111 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.ULE, Some F.S, ops)
    | 0b111000 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.SF, Some F.S, ops)
    | 0b111001 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.NGLE, Some F.S, ops)
    | 0b111010 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.SEQ, Some F.S, ops)
    | 0b111011 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.NGL, Some F.S, ops)
    | 0b111100 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.LT, Some F.S, ops)
    | 0b111101 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.NGE, Some F.S, ops)
    | 0b111110 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.LE, Some F.S, ops)
    | 0b111111 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.NGT, Some F.S, ops)
    | _ -> Error `Invalid

  let parse_cop1_when_rs_d ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 ->
        let%map ops = o make_fdfsft in
        (M.ADD, None, Some F.D, ops)
    | 0b000001 ->
        let%map ops = o make_fdfsft in
        (M.SUB, None, Some F.D, ops)
    | 0b000010 ->
        let%map ops = o make_fdfsft in
        (M.MUL, None, Some F.D, ops)
    | 0b000011 ->
        let%map ops = o make_fdfsft in
        (M.DIV, None, Some F.D, ops)
    | 0b000100 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.SQRT, None, Some F.D, ops)
    | 0b000101 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.ABS, None, Some F.D, ops)
    | 0b000110 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.MOV, None, Some F.D, ops)
    | 0b000111 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.NEG, None, Some F.D, ops)
    | 0b001000 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.ROUNDL, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b001001 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.TRUNCL, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b001010 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.CEILL, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b001011 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.FLOORL, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b001100 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.ROUNDW, None, Some F.D, ops)
    | 0b001101 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.TRUNCW, None, Some F.D, ops)
    | 0b001110 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.CEILW, None, Some F.D, ops)
    | 0b001111 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.FLOORW, None, Some F.D, ops)
    | 0b010000 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.SEL, None, Some F.D, ops)
    | 0b010001 when (not (is_rel6 ctx)) && (not (nd ctx)) && not (tf ctx) ->
        let%map ops = o make_fdfscc in
        (M.MOVF, None, Some F.D, ops)
    | 0b010001 when (not (is_rel6 ctx)) && (not (nd ctx)) && tf ctx ->
        let%map ops = o make_fdfscc in
        (M.MOVT, None, Some F.D, ops)
    | 0b010010 when not (is_rel6 ctx) ->
        let%map ops = o make_fdfsrt in
        (M.MOVZ, None, Some F.D, ops)
    | 0b010011 when not (is_rel6 ctx) ->
        let%map ops = o make_fdfsrt in
        (M.MOVN, None, Some F.D, ops)
    | 0b010100 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.SELEQZ, None, Some F.D, ops)
    | 0b010101 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.RECIP, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b010110 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6
        when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.RSQRT, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b010111 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.SELNEZ, None, Some F.D, ops)
    | 0b011000 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MADDF, None, Some F.D, ops)
    | 0b011001 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MSUBF, None, Some F.D, ops)
    | 0b011010 when chk20to16 ctx 0 && is_rel6 ctx ->
        let%map ops = o make_fdfs in
        (M.RINT, None, Some F.D, ops)
    | 0b011011 when chk20to16 ctx 0 && is_rel6 ctx ->
        let%map ops = o make_fdfs in
        (M.CLASS, None, Some F.D, ops)
    | 0b011100 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MIN, None, Some F.D, ops)
    | 0b011101 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MINA, None, Some F.D, ops)
    | 0b011110 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MAX, None, Some F.D, ops)
    | 0b011111 when is_rel6 ctx ->
        let%map ops = o make_fdfsft in
        (M.MAXA, None, Some F.D, ops)
    | 0b100000 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.CVTS, None, Some F.D, ops)
    | 0b100100 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.CVTW, None, Some F.D, ops)
    | 0b100101 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.CVTL, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b110000 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.F, Some F.D, ops)
    | 0b110001 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.UN, Some F.D, ops)
    | 0b110010 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.EQ, Some F.D, ops)
    | 0b110011 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.UEQ, Some F.D, ops)
    | 0b110100 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.OLT, Some F.D, ops)
    | 0b110101 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.ULT, Some F.D, ops)
    | 0b110110 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.OLE, Some F.D, ops)
    | 0b110111 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.ULE, Some F.D, ops)
    | 0b111000 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.SF, Some F.D, ops)
    | 0b111001 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.NGLE, Some F.D, ops)
    | 0b111010 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.SEQ, Some F.D, ops)
    | 0b111011 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.NGL, Some F.D, ops)
    | 0b111100 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.LT, Some F.D, ops)
    | 0b111101 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.NGE, Some F.D, ops)
    | 0b111110 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.LE, Some F.D, ops)
    | 0b111111 when ex ctx 7 6 = 0 && not (is_rel6 ctx) ->
        let%map ops = o make_ccfsft in
        (M.C, Some C.NGT, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_cmp ctx cond =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 21, is_rel6 ctx) with
    | 0b10100, true ->
        let%map ops = o make_fdfsft in
        (M.CMP, Some cond, Some F.S, ops)
    | 0b10101, true ->
        let%map ops = o make_fdfsft in
        (M.CMP, Some cond, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_cop1_when_rs_w ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 -> parse_cmp ctx C.AF
    | 0b000001 -> parse_cmp ctx C.UN
    | 0b000010 -> parse_cmp ctx C.EQ
    | 0b000011 -> parse_cmp ctx C.UEQ
    | 0b000100 -> parse_cmp ctx C.OLT
    | 0b000101 -> parse_cmp ctx C.ULT
    | 0b000110 -> parse_cmp ctx C.OLE
    | 0b000111 -> parse_cmp ctx C.ULE
    | 0b001000 -> parse_cmp ctx C.SAF
    | 0b001001 -> parse_cmp ctx C.SUB
    | 0b001010 -> parse_cmp ctx C.SEQ
    | 0b001011 -> parse_cmp ctx C.SUEQ
    | 0b001100 -> parse_cmp ctx C.SLT
    | 0b001101 -> parse_cmp ctx C.SULT
    | 0b001110 -> parse_cmp ctx C.SLE
    | 0b001111 -> parse_cmp ctx C.SULE
    | 0b010001 -> parse_cmp ctx C.OR
    | 0b010010 -> parse_cmp ctx C.UNE
    | 0b010011 -> parse_cmp ctx C.NE
    | 0b011001 -> parse_cmp ctx C.SOR
    | 0b011010 -> parse_cmp ctx C.SUNE
    | 0b011011 -> parse_cmp ctx C.SNE
    | 0b100000 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.CVTS, None, Some F.W, ops)
    | 0b100001 when chk20to16 ctx 0 ->
        let%map ops = o make_fdfs in
        (M.CVTD, None, Some F.W, ops)
    | _ -> Error `Invalid

  let parse_cop1_when_rs_l ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 -> parse_cmp ctx C.AF
    | 0b000001 -> parse_cmp ctx C.UN
    | 0b000010 -> parse_cmp ctx C.EQ
    | 0b000011 -> parse_cmp ctx C.UEQ
    | 0b000100 -> parse_cmp ctx C.OLT
    | 0b000101 -> parse_cmp ctx C.ULT
    | 0b000110 -> parse_cmp ctx C.OLE
    | 0b000111 -> parse_cmp ctx C.ULE
    | 0b001000 -> parse_cmp ctx C.SAF
    | 0b001001 -> parse_cmp ctx C.SUB
    | 0b001010 -> parse_cmp ctx C.SEQ
    | 0b001011 -> parse_cmp ctx C.SUEQ
    | 0b001100 -> parse_cmp ctx C.SLT
    | 0b001101 -> parse_cmp ctx C.SULT
    | 0b001110 -> parse_cmp ctx C.SLE
    | 0b001111 -> parse_cmp ctx C.SULE
    | 0b010001 -> parse_cmp ctx C.OR
    | 0b010010 -> parse_cmp ctx C.UNE
    | 0b010011 -> parse_cmp ctx C.NE
    | 0b011001 -> parse_cmp ctx C.SOR
    | 0b011010 -> parse_cmp ctx C.SUNE
    | 0b011011 -> parse_cmp ctx C.SNE
    | 0b100000 when chk20to16 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6 ->
          let%map ops = o make_fdfs in
          (M.CVTS, None, Some F.L, ops)
      | _ -> Error `Invalid )
    | 0b100001 when chk20to16 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips32r2
       |Mode.Mips32r6
       |Mode.Mips64
       |Mode.Mips64r2
       |Mode.Mips64r6 ->
          let%map ops = o make_fdfs in
          (M.CVTD, None, Some F.L, ops)
      | _ -> Error `Invalid )
    | _ -> Error `Invalid

  let parse_cop1_when_rs_ps ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfsft in
          (M.ADD, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b000001 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfsft in
          (M.SUB, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b000010 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfsft in
          (M.MUL, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b000101 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.ABS, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b000110 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.MOV, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b000111 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.NEG, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b010001 -> (
      match ctx.mode with
      | (Mode.Mips64 | Mode.Mips64r2) when (not (nd ctx)) && not (tf ctx) ->
          let%map ops = o make_fdfscc in
          (M.MOVF, None, Some F.PS, ops)
      | (Mode.Mips64 | Mode.Mips64r2) when (not (nd ctx)) && tf ctx ->
          let%map ops = o make_fdfscc in
          (M.MOVT, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b010010 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfsrt in
          (M.MOVZ, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b010011 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfsrt in
          (M.MOVN, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b100000 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.CVTSPU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b101000 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when chk20to16 ctx 0 ->
          let%map ops = o make_fdfs in
          (M.CVTSPL, None, None, ops)
      | _ -> Error `Invalid )
    | 0b110000 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.F, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b110001 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.UN, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b110010 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.EQ, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b110011 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.UEQ, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b110100 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.OLT, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b110101 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.ULT, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b110110 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.OLE, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b110111 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.ULE, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111000 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.SF, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111001 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.NGLE, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111010 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.SEQ, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111011 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.NGL, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111100 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.LT, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111101 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.NGE, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111110 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.LE, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111111 -> (
      match ctx.mode with
      | (Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2) when ex ctx 7 6 = 0 ->
          let%map ops = o make_ccfsft in
          (M.C, Some C.NGT, Some F.PS, ops)
      | _ -> Error `Invalid )
    | _ -> Error `Invalid

  let parse_cop1 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 21 with
    | 0b00000 when chk10to0 ctx 0 ->
        let%map ops = o make_rtfs in
        (M.MFC1, None, None, ops)
    | 0b00001 when chk10to0 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o make_rtfs in
          (M.DMFC1, None, None, ops)
      | _ -> Error `Invalid )
    | 0b00010 when chk10to0 ctx 0 ->
        let%map ops = o make_rtfs in
        (M.CFC1, None, None, ops)
    | 0b00011 when chk10to0 ctx 0 && is_rel2or6 ctx ->
        let%map ops = o make_rtfs in
        (M.MFHC1, None, None, ops)
    | 0b00100 when chk10to0 ctx 0 ->
        let%map ops = o make_rtfs in
        (M.MTC1, None, None, ops)
    | 0b00101 when chk10to0 ctx 0 ->
        let%map ops = o make_rtfs in
        (M.DMTC1, None, None, ops)
    | 0b00110 when chk10to0 ctx 0 ->
        let%map ops = o make_rtfs in
        (M.CTC1, None, None, ops)
    | 0b00111 when chk10to0 ctx 0 && is_rel2or6 ctx ->
        let%map ops = o make_rtfs in
        (M.MTHC1, None, None, ops)
    | 0b01000 when (not (nd ctx)) && (not (tf ctx)) && not (is_rel6 ctx) ->
        let reg = Some (reg_int R.fcc_reg_of_int) in
        let%bind ops = o (make_ccoff ~reg 20 18) in
        Ok (M.BC1F, None, None, ops)
    | 0b01000 when nd ctx && (not (tf ctx)) && not (is_rel6 ctx) ->
        let reg = Some (reg_int R.fcc_reg_of_int) in
        let%bind ops = o (make_ccoff ~reg 20 18) in
        Ok (M.BC1FL, None, None, ops)
    | 0b01000 when (not (nd ctx)) && tf ctx && not (is_rel6 ctx) ->
        let reg = Some (reg_int R.fcc_reg_of_int) in
        let%bind ops = o (make_ccoff ~reg 20 18) in
        Ok (M.BC1T, None, None, ops)
    | 0b01000 when nd ctx && tf ctx && not (is_rel6 ctx) ->
        let reg = Some (reg_int R.fcc_reg_of_int) in
        let%bind ops = o (make_ccoff ~reg 20 18) in
        Ok (M.BC1TL, None, None, ops)
    | 0b01001 when is_rel6 ctx ->
        let%map ops = o make_ftrel16 in
        (M.BC1EQZ, None, None, ops)
    | 0b01101 when is_rel6 ctx ->
        let%map ops = o make_ftrel16 in
        (M.BC1NEZ, None, None, ops)
    | 0b01011 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BZ, None, Some F.V, ops)
    | 0b01111 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BNZ, None, Some F.V, ops)
    | 0b10000 -> parse_cop1_when_rs_s ctx
    | 0b10001 -> parse_cop1_when_rs_d ctx
    | 0b10100 -> parse_cop1_when_rs_w ctx
    | 0b10101 -> parse_cop1_when_rs_l ctx
    | 0b10110 -> parse_cop1_when_rs_ps ctx
    | 0b11000 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BZ, None, Some F.B, ops)
    | 0b11001 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BZ, None, Some F.H, ops)
    | 0b11010 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BZ, None, Some F.W, ops)
    | 0b11011 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BZ, None, Some F.D, ops)
    | 0b11100 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BNZ, None, Some F.B, ops)
    | 0b11101 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BNZ, None, Some F.H, ops)
    | 0b11110 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BNZ, None, Some F.W, ops)
    | 0b11111 when is_rel2or6 ctx ->
        let%map ops = o make_wtrel16 in
        (M.BNZ, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_cop2 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 21 with
    | 0b00000 ->
        let%map ops = o (make_rtimm16 16) in
        (M.MFC2, None, None, ops)
    | 0b00001 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtimm16 16) in
          (M.DMFC2, None, None, ops)
      | _ -> Error `Invalid )
    | 0b00010 ->
        let%map ops = o (make_rtimm16 16) in
        (M.CFC2, None, None, ops)
    | 0b00011 when is_rel2or6 ctx ->
        let%map ops = o (make_rtimm16 16) in
        (M.MFHC2, None, None, ops)
    | 0b00100 ->
        let%map ops = o (make_rtimm16 16) in
        (M.MTC2, None, None, ops)
    | 0b00101 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtimm16 16) in
          (M.DMTC2, None, None, ops)
      | _ -> Error `Invalid )
    | 0b00110 ->
        let%map ops = o (make_rtimm16 16) in
        (M.CTC2, None, None, ops)
    | 0b00111 when is_rel2or6 ctx ->
        let%map ops = o (make_rtimm16 16) in
        (M.MTHC2, None, None, ops)
    | 0b01000 when (not (is_rel6 ctx)) && (not (nd ctx)) && not (tf ctx) ->
        let%bind ops = o (make_ccoff 20 18) in
        Ok (M.BC2F, None, None, ops)
    | 0b01000 when (not (is_rel6 ctx)) && nd ctx && not (tf ctx) ->
        let%bind ops = o (make_ccoff 20 18) in
        Ok (M.BC2FL, None, None, ops)
    | 0b01000 when (not (is_rel6 ctx)) && (not (nd ctx)) && tf ctx ->
        let%bind ops = o (make_ccoff 20 18) in
        Ok (M.BC2T, None, None, ops)
    | 0b01000 when (not (is_rel6 ctx)) && nd ctx && tf ctx ->
        let%bind ops = o (make_ccoff 20 18) in
        Ok (M.BC2TL, None, None, ops)
    | 0b01001 when is_rel6 ctx ->
        let%bind ops = o (make_ccoff 20 16) in
        Ok (M.BC2EQZ, None, None, ops)
    | 0b01010 when is_rel6 ctx ->
        let%map ops = o (make_rtmembaseoff11 32) in
        (M.LWC2, None, None, ops)
    | 0b01011 when is_rel6 ctx ->
        let%map ops = o (make_rtmembaseoff11 32) in
        ctx.opsz <- 32;
        (M.SWC2, None, None, ops)
    | 0b01101 when is_rel6 ctx ->
        let%bind ops = o (make_ccoff 20 16) in
        Ok (M.BC2NEZ, None, None, ops)
    | 0b01110 when is_rel6 ctx ->
        let%map ops = o (make_rtmembaseoff11 64) in
        (M.LDC2, None, None, ops)
    | 0b01111 when is_rel6 ctx ->
        let%map ops = o (make_rtmembaseoff11 64) in
        ctx.opsz <- 64;
        (M.SDC2, None, None, ops)
    | _ -> Error `Invalid

  let parse_cop1x ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 5 0 with
    | 0b000000 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_fdmembaseindex 32) in
          (M.LWXC1, None, None, ops)
      | _ -> Error `Invalid )
    | 0b000001 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_fdmembaseindex 64) in
          (M.LDXC1, None, None, ops)
      | _ -> Error `Invalid )
    | 0b000101 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_fdmembaseindex 64) in
          (M.LUXC1, None, None, ops)
      | _ -> Error `Invalid )
    | 0b001000 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_fdmembaseindex 32) in
          ctx.opsz <- 32;
          (M.SWXC1, None, None, ops)
      | _ -> Error `Invalid )
    | 0b001001 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_fdmembaseindex 64) in
          ctx.opsz <- 64;
          (M.SDXC1, None, None, ops)
      | _ -> Error `Invalid )
    | 0b001101 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_fdmembaseindex 64) in
          ctx.opsz <- 64;
          (M.SUXC1, None, None, ops)
      | _ -> Error `Invalid )
    | 0b001111 when chk10to6 ctx 0 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_hintmembaseindex in
          (M.PREFX, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011110 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfsftrs in
          (M.ALNV, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b100000 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.MADD, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b100001 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.MADD, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b100110 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.MADD, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b101000 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.MSUB, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b101001 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.MSUB, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b101110 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.MSUB, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b110000 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.NMADD, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b110001 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.NMADD, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b110110 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.NMADD, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | 0b111000 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.NMSUB, None, Some F.S, ops)
      | _ -> Error `Invalid )
    | 0b111001 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.NMSUB, None, Some F.D, ops)
      | _ -> Error `Invalid )
    | 0b111110 -> (
      match ctx.mode with
      | Mode.Mips32r2 | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o make_fdfrfsft in
          (M.NMSUB, None, Some F.PS, ops)
      | _ -> Error `Invalid )
    | _ -> Error `Invalid

  let parse_blezl_pop26 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 21, ex ctx 20 16) with
    | 0, n when n <> 0 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BLEZC, None, None, ops)
    | n1, n2 when n1 <> 0 && n2 <> 0 && n1 = n2 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BGEZC, None, None, ops)
    | n1, n2 when n1 <> 0 && n2 <> 0 && n1 <> n2 && is_rel6 ctx ->
        let%map ops = o make_rsrtrel16 in
        (M.BGEC, None, None, ops)
    | _, 0 when not (is_rel6 ctx) ->
        let%map ops = o make_rsrel16 in
        (M.BLEZL, None, None, ops)
    | _ -> Error `Invalid

  let parse_bgtzl_pop27 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 21, ex ctx 20 16) with
    | 0, n when n <> 0 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BGTZC, None, None, ops)
    | n1, n2 when n1 <> 0 && n2 <> 0 && n1 = n2 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BLTZC, None, None, ops)
    | n1, n2 when n1 <> 0 && n2 <> 0 && n1 <> n2 && is_rel6 ctx ->
        let%map ops = o make_rsrtrel16 in
        (M.BLTC, None, None, ops)
    | _, 0 when not (is_rel6 ctx) ->
        let%map ops = o make_rsrel16 in
        (M.BGTZL, None, None, ops)
    | _ -> Error `Invalid

  let parse_daddi_pop30 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 21, ex ctx 20 16) with
    | 0, n when n > 0 && is_rel6 ctx ->
        let%map ops = o make_rtrel16 in
        (M.BNEZALC, None, None, ops)
    | n1, n2 when n1 <> 0 && n2 <> 0 && n1 < n2 && is_rel6 ctx ->
        let%map ops = o make_rsrtrel16 in
        (M.BNEC, None, None, ops)
    | n1, n2 when n1 > n2 && is_rel6 ctx ->
        let%map ops = o make_rsrtrel16 in
        (M.BNVC, None, None, ops)
    | _ -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_rtrsimm16 64) in
          (M.DADDI, None, None, ops)
      | _ -> Error `Invalid )

  let parse_jalx_daui ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 21 with
    | n when n <> 0 && Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o (make_rtrsimm16 64) in
        (M.DAUI, None, None, ops)
    | _ when not (is_rel6 ctx) -> Ok (M.JALX, None, None, make_abs26 ctx)
    | _ -> Error `Invalid

  let parse_pcrel ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 20 18, ex ctx 17 16) with
    | n1, _ when (n1 = 0 || n1 = 1) && is_rel6 ctx ->
        let%map ops = o (make_rsimm19 ctx.opsz) in
        (M.ADDIUPC, None, None, ops)
    | n1, _ when (n1 = 2 || n1 = 3) && is_rel6 ctx ->
        let%map ops = o (make_rsimm19 ctx.opsz) in
        (M.LWPC, None, None, ops)
    | n1, _ when (n1 = 4 || n1 = 5) && Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o (make_rsimm19 ctx.opsz) in
        (M.LWUPC, None, None, ops)
    | n1, _ when n1 = 6 && Mode.(equal ctx.mode Mips64r6) ->
        let%map ops = o (make_rsimm18 ctx.opsz) in
        (M.LDPC, None, None, ops)
    | n1, n2 when n1 = 7 && n2 = 2 && is_rel6 ctx ->
        let%map ops = o (make_rsimm16 ctx.opsz) in
        (M.AUIPC, None, None, ops)
    | n1, n2 when n1 = 7 && n2 = 3 && is_rel6 ctx ->
        let%map ops = o (make_rsimm16 ctx.opsz) in
        (M.ALUIPC, None, None, ops)
    | _ -> Error `Invalid

  let parse_msa_mi10 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 5 2, ex ctx 1 0) with
    | 0b1000, 0b00 ->
        let%map ops = o (make_mems10rswd 8) in
        (M.LD, None, Some F.B, ops)
    | 0b1000, 0b01 ->
        let%map ops = o (make_mems10rswd 16) in
        (M.LD, None, Some F.H, ops)
    | 0b1000, 0b10 ->
        let%map ops = o (make_mems10rswd 32) in
        (M.LD, None, Some F.W, ops)
    | 0b1000, 0b11 ->
        let%map ops = o (make_mems10rswd 64) in
        (M.LD, None, Some F.D, ops)
    | 0b1001, 0b00 ->
        let%map ops = o (make_mems10rswd 8) in
        (M.ST, None, Some F.B, ops)
    | 0b1001, 0b01 ->
        let%map ops = o (make_mems10rswd 16) in
        (M.ST, None, Some F.H, ops)
    | 0b1001, 0b10 ->
        let%map ops = o (make_mems10rswd 32) in
        (M.ST, None, Some F.W, ops)
    | 0b1001, 0b11 ->
        let%map ops = o (make_mems10rswd 64) in
        (M.ST, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_msa_i5 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 23, ex ctx 5 0, ex ctx 22 21) with
    | 0b000, 0b000110, 0b00 ->
        let%map ops = o (make_wdwsu5 8) in
        (M.ADDVI, None, Some F.B, ops)
    | 0b000, 0b000110, 0b01 ->
        let%map ops = o (make_wdwsu5 16) in
        (M.ADDVI, None, Some F.H, ops)
    | 0b000, 0b000110, 0b10 ->
        let%map ops = o (make_wdwsu5 32) in
        (M.ADDVI, None, Some F.W, ops)
    | 0b000, 0b000110, 0b11 ->
        let%map ops = o (make_wdwsu5 64) in
        (M.ADDVI, None, Some F.D, ops)
    | 0b000, 0b000111, 0b00 ->
        let%map ops = o (make_wdwss5 8) in
        (M.CEQI, None, Some F.B, ops)
    | 0b000, 0b000111, 0b01 ->
        let%map ops = o (make_wdwss5 16) in
        (M.CEQI, None, Some F.H, ops)
    | 0b000, 0b000111, 0b10 ->
        let%map ops = o (make_wdwss5 32) in
        (M.CEQI, None, Some F.W, ops)
    | 0b000, 0b000111, 0b11 ->
        let%map ops = o (make_wdwss5 64) in
        (M.CEQI, None, Some F.D, ops)
    | 0b001, 0b000110, 0b00 ->
        let%map ops = o (make_wdwsu5 8) in
        (M.SUBVI, None, Some F.B, ops)
    | 0b001, 0b000110, 0b01 ->
        let%map ops = o (make_wdwsu5 16) in
        (M.SUBVI, None, Some F.H, ops)
    | 0b001, 0b000110, 0b10 ->
        let%map ops = o (make_wdwsu5 32) in
        (M.SUBVI, None, Some F.W, ops)
    | 0b001, 0b000110, 0b11 ->
        let%map ops = o (make_wdwsu5 64) in
        (M.SUBVI, None, Some F.D, ops)
    | 0b010, 0b000110, 0b00 ->
        let%map ops = o (make_wdwss5 8) in
        (M.MAXI_S, None, Some F.B, ops)
    | 0b010, 0b000110, 0b01 ->
        let%map ops = o (make_wdwss5 16) in
        (M.MAXI_S, None, Some F.H, ops)
    | 0b010, 0b000110, 0b10 ->
        let%map ops = o (make_wdwss5 32) in
        (M.MAXI_S, None, Some F.W, ops)
    | 0b010, 0b000110, 0b11 ->
        let%map ops = o (make_wdwss5 64) in
        (M.MAXI_S, None, Some F.D, ops)
    | 0b010, 0b000111, 0b00 ->
        let%map ops = o (make_wdwss5 8) in
        (M.CLTI_S, None, Some F.B, ops)
    | 0b010, 0b000111, 0b01 ->
        let%map ops = o (make_wdwss5 16) in
        (M.CLTI_S, None, Some F.H, ops)
    | 0b010, 0b000111, 0b10 ->
        let%map ops = o (make_wdwss5 32) in
        (M.CLTI_S, None, Some F.W, ops)
    | 0b010, 0b000111, 0b11 ->
        let%map ops = o (make_wdwss5 64) in
        (M.CLTI_S, None, Some F.D, ops)
    | 0b011, 0b000110, 0b00 ->
        let%map ops = o (make_wdwsu5 8) in
        (M.MAXI_U, None, Some F.B, ops)
    | 0b011, 0b000110, 0b01 ->
        let%map ops = o (make_wdwsu5 16) in
        (M.MAXI_U, None, Some F.H, ops)
    | 0b011, 0b000110, 0b10 ->
        let%map ops = o (make_wdwsu5 32) in
        (M.MAXI_U, None, Some F.W, ops)
    | 0b011, 0b000110, 0b11 ->
        let%map ops = o (make_wdwsu5 64) in
        (M.MAXI_U, None, Some F.D, ops)
    | 0b011, 0b000111, 0b00 ->
        let%map ops = o (make_wdwsu5 8) in
        (M.CLTI_U, None, Some F.B, ops)
    | 0b011, 0b000111, 0b01 ->
        let%map ops = o (make_wdwsu5 16) in
        (M.CLTI_U, None, Some F.H, ops)
    | 0b011, 0b000111, 0b10 ->
        let%map ops = o (make_wdwsu5 32) in
        (M.CLTI_U, None, Some F.W, ops)
    | 0b011, 0b000111, 0b11 ->
        let%map ops = o (make_wdwsu5 64) in
        (M.CLTI_U, None, Some F.D, ops)
    | 0b100, 0b000110, 0b00 ->
        let%map ops = o (make_wdwss5 8) in
        (M.MINI_S, None, Some F.B, ops)
    | 0b100, 0b000110, 0b01 ->
        let%map ops = o (make_wdwss5 16) in
        (M.MINI_S, None, Some F.H, ops)
    | 0b100, 0b000110, 0b10 ->
        let%map ops = o (make_wdwss5 32) in
        (M.MINI_S, None, Some F.W, ops)
    | 0b100, 0b000110, 0b11 ->
        let%map ops = o (make_wdwss5 64) in
        (M.MINI_S, None, Some F.D, ops)
    | 0b100, 0b000111, 0b00 ->
        let%map ops = o (make_wdwss5 8) in
        (M.CLEI_S, None, Some F.B, ops)
    | 0b100, 0b000111, 0b01 ->
        let%map ops = o (make_wdwss5 16) in
        (M.CLEI_S, None, Some F.H, ops)
    | 0b100, 0b000111, 0b10 ->
        let%map ops = o (make_wdwss5 32) in
        (M.CLEI_S, None, Some F.W, ops)
    | 0b100, 0b000111, 0b11 ->
        let%map ops = o (make_wdwss5 64) in
        (M.CLEI_S, None, Some F.D, ops)
    | 0b101, 0b000110, 0b00 ->
        let%map ops = o (make_wdwsu5 8) in
        (M.MINI_U, None, Some F.B, ops)
    | 0b101, 0b000110, 0b01 ->
        let%map ops = o (make_wdwsu5 16) in
        (M.MINI_U, None, Some F.H, ops)
    | 0b101, 0b000110, 0b10 ->
        let%map ops = o (make_wdwsu5 32) in
        (M.MINI_U, None, Some F.W, ops)
    | 0b101, 0b000110, 0b11 ->
        let%map ops = o (make_wdwsu5 64) in
        (M.MINI_U, None, Some F.D, ops)
    | 0b101, 0b000111, 0b00 ->
        let%map ops = o (make_wdwsu5 8) in
        (M.CLEI_U, None, Some F.B, ops)
    | 0b101, 0b000111, 0b01 ->
        let%map ops = o (make_wdwsu5 16) in
        (M.CLEI_U, None, Some F.H, ops)
    | 0b101, 0b000111, 0b10 ->
        let%map ops = o (make_wdwsu5 32) in
        (M.CLEI_U, None, Some F.W, ops)
    | 0b101, 0b000111, 0b11 ->
        let%map ops = o (make_wdwsu5 64) in
        (M.CLEI_U, None, Some F.D, ops)
    | 0b110, 0b000111, 0b00 ->
        let%map ops = o (make_wds10 8) in
        (M.LDI, None, Some F.B, ops)
    | 0b110, 0b000111, 0b01 ->
        let%map ops = o (make_wds10 16) in
        (M.LDI, None, Some F.H, ops)
    | 0b110, 0b000111, 0b10 ->
        let%map ops = o (make_wds10 32) in
        (M.LDI, None, Some F.W, ops)
    | 0b110, 0b000111, 0b11 ->
        let%map ops = o (make_wds10 64) in
        (M.LDI, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_msa_i8 ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 24, ex ctx 5 0) with
    | 0b00, 0b000000 ->
        let%map ops = o make_wdwsi8 in
        (M.ANDI, None, Some F.B, ops)
    | 0b00, 0b000001 ->
        let%map ops = o make_wdwsi8 in
        (M.BMNZI, None, Some F.B, ops)
    | 0b00, 0b000010 ->
        let%map ops = o make_wdwsi8 in
        (M.SHF, None, Some F.B, ops)
    | 0b01, 0b000000 ->
        let%map ops = o make_wdwsi8 in
        (M.ORI, None, Some F.B, ops)
    | 0b01, 0b000001 ->
        let%map ops = o make_wdwsi8 in
        (M.BMZI, None, Some F.B, ops)
    | 0b01, 0b000010 ->
        let%map ops = o make_wdwsi8 in
        (M.SHF, None, Some F.H, ops)
    | 0b10, 0b000000 ->
        let%map ops = o make_wdwsi8 in
        (M.NORI, None, Some F.B, ops)
    | 0b10, 0b000001 ->
        let%map ops = o make_wdwsi8 in
        (M.BSELI, None, Some F.B, ops)
    | 0b10, 0b000010 ->
        let%map ops = o make_wdwsi8 in
        (M.SHF, None, Some F.W, ops)
    | 0b11, 0b000000 ->
        let%map ops = o make_wdwsi8 in
        (M.XORI, None, Some F.B, ops)
    | _ -> Error `Invalid

  let parse_msa_2r ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 20 18, ex ctx 17 16) with
    | 0b000, 0b00 ->
        let%map ops = o make_wdrs in
        (M.FILL, None, Some F.B, ops)
    | 0b000, 0b01 ->
        let%map ops = o make_wdrs in
        (M.FILL, None, Some F.H, ops)
    | 0b000, 0b10 ->
        let%map ops = o make_wdrs in
        (M.FILL, None, Some F.W, ops)
    | 0b000, 0b11 ->
        let%map ops = o make_wdrs in
        (M.FILL, None, Some F.D, ops)
    | 0b001, 0b00 ->
        let%map ops = o make_wdws in
        (M.PCNT, None, Some F.B, ops)
    | 0b001, 0b01 ->
        let%map ops = o make_wdws in
        (M.PCNT, None, Some F.H, ops)
    | 0b001, 0b10 ->
        let%map ops = o make_wdws in
        (M.PCNT, None, Some F.W, ops)
    | 0b001, 0b11 ->
        let%map ops = o make_wdws in
        (M.PCNT, None, Some F.D, ops)
    | 0b010, 0b00 ->
        let%map ops = o make_wdws in
        (M.NLOC, None, Some F.B, ops)
    | 0b010, 0b01 ->
        let%map ops = o make_wdws in
        (M.NLOC, None, Some F.H, ops)
    | 0b010, 0b10 ->
        let%map ops = o make_wdws in
        (M.NLOC, None, Some F.W, ops)
    | 0b010, 0b11 ->
        let%map ops = o make_wdws in
        (M.NLOC, None, Some F.D, ops)
    | 0b011, 0b00 ->
        let%map ops = o make_wdws in
        (M.NLZC, None, Some F.B, ops)
    | 0b011, 0b01 ->
        let%map ops = o make_wdws in
        (M.NLZC, None, Some F.H, ops)
    | 0b011, 0b10 ->
        let%map ops = o make_wdws in
        (M.NLZC, None, Some F.W, ops)
    | 0b011, 0b11 ->
        let%map ops = o make_wdws in
        (M.NLZC, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_msa_2rf ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 20 17, tst ctx 16) with
    | 0b0000, false ->
        let%map ops = o make_wdws in
        (M.FCLASS, None, Some F.W, ops)
    | 0b0000, true ->
        let%map ops = o make_wdws in
        (M.FCLASS, None, Some F.D, ops)
    | 0b0001, false ->
        let%map ops = o make_wdws in
        (M.FTRUNC_S, None, Some F.W, ops)
    | 0b0001, true ->
        let%map ops = o make_wdws in
        (M.FTRUNC_S, None, Some F.D, ops)
    | 0b0010, false ->
        let%map ops = o make_wdws in
        (M.FTRUNC_U, None, Some F.W, ops)
    | 0b0010, true ->
        let%map ops = o make_wdws in
        (M.FTRUNC_U, None, Some F.D, ops)
    | 0b0011, false ->
        let%map ops = o make_wdws in
        (M.FSQRT, None, Some F.W, ops)
    | 0b0011, true ->
        let%map ops = o make_wdws in
        (M.FSQRT, None, Some F.D, ops)
    | 0b0100, false ->
        let%map ops = o make_wdws in
        (M.FRSQRT, None, Some F.W, ops)
    | 0b0100, true ->
        let%map ops = o make_wdws in
        (M.FRSQRT, None, Some F.D, ops)
    | 0b0101, false ->
        let%map ops = o make_wdws in
        (M.FRCP, None, Some F.W, ops)
    | 0b0101, true ->
        let%map ops = o make_wdws in
        (M.FRCP, None, Some F.D, ops)
    | 0b0110, false ->
        let%map ops = o make_wdws in
        (M.FRINT, None, Some F.W, ops)
    | 0b0110, true ->
        let%map ops = o make_wdws in
        (M.FRINT, None, Some F.D, ops)
    | 0b0111, false ->
        let%map ops = o make_wdws in
        (M.FLOG2, None, Some F.W, ops)
    | 0b0111, true ->
        let%map ops = o make_wdws in
        (M.FLOG2, None, Some F.D, ops)
    | 0b1000, false ->
        let%map ops = o make_wdws in
        (M.FEXUPL, None, Some F.W, ops)
    | 0b1000, true ->
        let%map ops = o make_wdws in
        (M.FEXUPL, None, Some F.D, ops)
    | 0b1001, false ->
        let%map ops = o make_wdws in
        (M.FEXUPR, None, Some F.W, ops)
    | 0b1001, true ->
        let%map ops = o make_wdws in
        (M.FEXUPR, None, Some F.D, ops)
    | 0b1010, false ->
        let%map ops = o make_wdws in
        (M.FFQL, None, Some F.W, ops)
    | 0b1010, true ->
        let%map ops = o make_wdws in
        (M.FFQL, None, Some F.D, ops)
    | 0b1011, false ->
        let%map ops = o make_wdws in
        (M.FFQR, None, Some F.W, ops)
    | 0b1011, true ->
        let%map ops = o make_wdws in
        (M.FFQR, None, Some F.D, ops)
    | 0b1100, false ->
        let%map ops = o make_wdws in
        (M.FTINT_S, None, Some F.W, ops)
    | 0b1100, true ->
        let%map ops = o make_wdws in
        (M.FTINT_S, None, Some F.D, ops)
    | 0b1101, false ->
        let%map ops = o make_wdws in
        (M.FTINT_U, None, Some F.W, ops)
    | 0b1101, true ->
        let%map ops = o make_wdws in
        (M.FTINT_U, None, Some F.D, ops)
    | 0b1110, false ->
        let%map ops = o make_wdws in
        (M.FFINT_S, None, Some F.W, ops)
    | 0b1110, true ->
        let%map ops = o make_wdws in
        (M.FFINT_S, None, Some F.D, ops)
    | 0b1111, false ->
        let%map ops = o make_wdws in
        (M.FFINT_U, None, Some F.W, ops)
    | 0b1111, true ->
        let%map ops = o make_wdws in
        (M.FFINT_U, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_msa_vec2r2rf ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 21 with
    | 0b00000 ->
        let%map ops = o make_wdwswt in
        (M.AND, None, Some F.V, ops)
    | 0b00001 ->
        let%map ops = o make_wdwswt in
        (M.OR, None, Some F.V, ops)
    | 0b00010 ->
        let%map ops = o make_wdwswt in
        (M.NOR, None, Some F.V, ops)
    | 0b00011 ->
        let%map ops = o make_wdwswt in
        (M.XOR, None, Some F.V, ops)
    | 0b00100 ->
        let%map ops = o make_wdwswt in
        (M.BMNZ, None, Some F.V, ops)
    | 0b00101 ->
        let%map ops = o make_wdwswt in
        (M.BMZ, None, Some F.V, ops)
    | 0b00110 ->
        let%map ops = o make_wdwswt in
        (M.BSEL, None, Some F.V, ops)
    | 0b11000 -> parse_msa_2r ctx
    | 0b11001 -> parse_msa_2rf ctx
    | _ -> Error `Invalid

  let parse_msa_3r ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 23, ex ctx 5 0, ex ctx 22 21) with
    | 0b000, 0b001101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SLL, None, Some F.B, ops)
    | 0b000, 0b001101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SLL, None, Some F.H, ops)
    | 0b000, 0b001101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SLL, None, Some F.W, ops)
    | 0b000, 0b001101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SLL, None, Some F.D, ops)
    | 0b000, 0b001110, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ADDV, None, Some F.B, ops)
    | 0b000, 0b001110, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ADDV, None, Some F.H, ops)
    | 0b000, 0b001110, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ADDV, None, Some F.W, ops)
    | 0b000, 0b001110, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ADDV, None, Some F.D, ops)
    | 0b000, 0b001111, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.CEQ, None, Some F.B, ops)
    | 0b000, 0b001111, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.CEQ, None, Some F.H, ops)
    | 0b000, 0b001111, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.CEQ, None, Some F.W, ops)
    | 0b000, 0b001111, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.CEQ, None, Some F.D, ops)
    | 0b000, 0b010000, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ADD_A, None, Some F.B, ops)
    | 0b000, 0b010000, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ADD_A, None, Some F.H, ops)
    | 0b000, 0b010000, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ADD_A, None, Some F.W, ops)
    | 0b000, 0b010000, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ADD_A, None, Some F.D, ops)
    | 0b000, 0b010001, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SUBS_S, None, Some F.B, ops)
    | 0b000, 0b010001, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SUBS_S, None, Some F.H, ops)
    | 0b000, 0b010001, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SUBS_S, None, Some F.W, ops)
    | 0b000, 0b010001, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SUBS_S, None, Some F.D, ops)
    | 0b000, 0b010010, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MULV, None, Some F.B, ops)
    | 0b000, 0b010010, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MULV, None, Some F.H, ops)
    | 0b000, 0b010010, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MULV, None, Some F.W, ops)
    | 0b000, 0b010010, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MULV, None, Some F.D, ops)
    | 0b000, 0b010011, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.DOTP_S, None, Some F.H, ops)
    | 0b000, 0b010011, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.DOTP_S, None, Some F.W, ops)
    | 0b000, 0b010011, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.DOTP_S, None, Some F.D, ops)
    | 0b000, 0b010100, 0b00 ->
        let%map ops = o make_slidewdwsrt in
        (M.SLD, None, Some F.B, ops)
    | 0b000, 0b010100, 0b01 ->
        let%map ops = o make_slidewdwsrt in
        (M.SLD, None, Some F.H, ops)
    | 0b000, 0b010100, 0b10 ->
        let%map ops = o make_slidewdwsrt in
        (M.SLD, None, Some F.W, ops)
    | 0b000, 0b010100, 0b11 ->
        let%map ops = o make_slidewdwsrt in
        (M.SLD, None, Some F.D, ops)
    | 0b000, 0b010101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.VSHF, None, Some F.B, ops)
    | 0b000, 0b010101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.VSHF, None, Some F.H, ops)
    | 0b000, 0b010101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.VSHF, None, Some F.W, ops)
    | 0b000, 0b010101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.VSHF, None, Some F.D, ops)
    | 0b001, 0b001101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SRA, None, Some F.B, ops)
    | 0b001, 0b001101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SRA, None, Some F.H, ops)
    | 0b001, 0b001101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SRA, None, Some F.W, ops)
    | 0b001, 0b001101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SRA, None, Some F.D, ops)
    | 0b001, 0b001110, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SUBV, None, Some F.B, ops)
    | 0b001, 0b001110, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SUBV, None, Some F.H, ops)
    | 0b001, 0b001110, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SUBV, None, Some F.W, ops)
    | 0b001, 0b001110, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SUBV, None, Some F.D, ops)
    | 0b001, 0b010000, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_A, None, Some F.B, ops)
    | 0b001, 0b010000, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_A, None, Some F.H, ops)
    | 0b001, 0b010000, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_A, None, Some F.W, ops)
    | 0b001, 0b010000, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_A, None, Some F.D, ops)
    | 0b001, 0b010001, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SUBS_U, None, Some F.B, ops)
    | 0b001, 0b010001, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SUBS_U, None, Some F.H, ops)
    | 0b001, 0b010001, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SUBS_U, None, Some F.W, ops)
    | 0b001, 0b010001, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SUBS_U, None, Some F.D, ops)
    | 0b001, 0b010010, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MSUBV, None, Some F.B, ops)
    | 0b001, 0b010010, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MSUBV, None, Some F.H, ops)
    | 0b001, 0b010010, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MSUBV, None, Some F.W, ops)
    | 0b001, 0b010010, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MSUBV, None, Some F.D, ops)
    | 0b001, 0b010011, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.DOTP_U, None, Some F.H, ops)
    | 0b001, 0b010011, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.DOTP_U, None, Some F.W, ops)
    | 0b001, 0b010011, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.DOTP_U, None, Some F.D, ops)
    | 0b001, 0b010100, 0b00 ->
        let%map ops = o make_slidewdwsrt in
        (M.SPLAT, None, Some F.B, ops)
    | 0b001, 0b010100, 0b01 ->
        let%map ops = o make_slidewdwsrt in
        (M.SPLAT, None, Some F.H, ops)
    | 0b001, 0b010100, 0b10 ->
        let%map ops = o make_slidewdwsrt in
        (M.SPLAT, None, Some F.W, ops)
    | 0b001, 0b010100, 0b11 ->
        let%map ops = o make_slidewdwsrt in
        (M.SPLAT, None, Some F.D, ops)
    | 0b001, 0b010101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SRAR, None, Some F.B, ops)
    | 0b001, 0b010101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SRAR, None, Some F.H, ops)
    | 0b001, 0b010101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SRAR, None, Some F.W, ops)
    | 0b001, 0b010101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SRAR, None, Some F.D, ops)
    | 0b010, 0b001101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SRL, None, Some F.B, ops)
    | 0b010, 0b001101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SRL, None, Some F.H, ops)
    | 0b010, 0b001101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SRL, None, Some F.W, ops)
    | 0b010, 0b001101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SRL, None, Some F.D, ops)
    | 0b010, 0b001110, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MAX_S, None, Some F.B, ops)
    | 0b010, 0b001110, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MAX_S, None, Some F.H, ops)
    | 0b010, 0b001110, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MAX_S, None, Some F.W, ops)
    | 0b010, 0b001110, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MAX_S, None, Some F.D, ops)
    | 0b010, 0b001111, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.CLT_S, None, Some F.B, ops)
    | 0b010, 0b001111, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.CLT_S, None, Some F.H, ops)
    | 0b010, 0b001111, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.CLT_S, None, Some F.W, ops)
    | 0b010, 0b001111, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.CLT_S, None, Some F.D, ops)
    | 0b010, 0b010000, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_S, None, Some F.B, ops)
    | 0b010, 0b010000, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_S, None, Some F.H, ops)
    | 0b010, 0b010000, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_S, None, Some F.W, ops)
    | 0b010, 0b010000, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_S, None, Some F.D, ops)
    | 0b010, 0b010001, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SUBSUS_U, None, Some F.B, ops)
    | 0b010, 0b010001, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SUBSUS_U, None, Some F.H, ops)
    | 0b010, 0b010001, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SUBSUS_U, None, Some F.W, ops)
    | 0b010, 0b010001, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SUBSUS_U, None, Some F.D, ops)
    | 0b010, 0b010010, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MSUBV, None, Some F.B, ops)
    | 0b010, 0b010010, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MSUBV, None, Some F.H, ops)
    | 0b010, 0b010010, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MSUBV, None, Some F.W, ops)
    | 0b010, 0b010010, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MSUBV, None, Some F.D, ops)
    | 0b010, 0b010011, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.DPADD_S, None, Some F.H, ops)
    | 0b010, 0b010011, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.DPADD_S, None, Some F.W, ops)
    | 0b010, 0b010011, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.DPADD_S, None, Some F.D, ops)
    | 0b010, 0b010100, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.PCKEV, None, Some F.B, ops)
    | 0b010, 0b010100, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.PCKEV, None, Some F.H, ops)
    | 0b010, 0b010100, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.PCKEV, None, Some F.W, ops)
    | 0b010, 0b010100, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.PCKEV, None, Some F.D, ops)
    | 0b010, 0b010101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SRLR, None, Some F.B, ops)
    | 0b010, 0b010101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SRLR, None, Some F.H, ops)
    | 0b010, 0b010101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SRLR, None, Some F.W, ops)
    | 0b010, 0b010101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SRLR, None, Some F.D, ops)
    | 0b011, 0b001101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.BCLR, None, Some F.B, ops)
    | 0b011, 0b001101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.BCLR, None, Some F.H, ops)
    | 0b011, 0b001101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.BCLR, None, Some F.W, ops)
    | 0b011, 0b001101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.BCLR, None, Some F.D, ops)
    | 0b011, 0b001110, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MAX_U, None, Some F.B, ops)
    | 0b011, 0b001110, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MAX_U, None, Some F.H, ops)
    | 0b011, 0b001110, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MAX_U, None, Some F.W, ops)
    | 0b011, 0b001110, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MAX_U, None, Some F.D, ops)
    | 0b011, 0b001111, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.CLT_U, None, Some F.B, ops)
    | 0b011, 0b001111, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.CLT_U, None, Some F.H, ops)
    | 0b011, 0b001111, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.CLT_U, None, Some F.W, ops)
    | 0b011, 0b001111, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.CLT_U, None, Some F.D, ops)
    | 0b011, 0b010000, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_U, None, Some F.B, ops)
    | 0b011, 0b010000, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_U, None, Some F.H, ops)
    | 0b011, 0b010000, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_U, None, Some F.W, ops)
    | 0b011, 0b010000, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ADDS_U, None, Some F.D, ops)
    | 0b011, 0b010001, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.SUBSUU_S, None, Some F.B, ops)
    | 0b011, 0b010001, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.SUBSUU_S, None, Some F.H, ops)
    | 0b011, 0b010001, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.SUBSUU_S, None, Some F.W, ops)
    | 0b011, 0b010001, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.SUBSUU_S, None, Some F.D, ops)
    | 0b011, 0b010011, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.DPADD_U, None, Some F.H, ops)
    | 0b011, 0b010011, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.DPADD_U, None, Some F.W, ops)
    | 0b011, 0b010011, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.DPADD_U, None, Some F.D, ops)
    | 0b011, 0b010100, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.PCKOD, None, Some F.B, ops)
    | 0b011, 0b010100, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.PCKOD, None, Some F.H, ops)
    | 0b011, 0b010100, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.PCKOD, None, Some F.W, ops)
    | 0b011, 0b010100, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.PCKOD, None, Some F.D, ops)
    | 0b100, 0b001101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.BSET, None, Some F.B, ops)
    | 0b100, 0b001101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.BSET, None, Some F.H, ops)
    | 0b100, 0b001101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.BSET, None, Some F.W, ops)
    | 0b100, 0b001101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.BSET, None, Some F.D, ops)
    | 0b100, 0b001110, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MIN_S, None, Some F.B, ops)
    | 0b100, 0b001110, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MIN_S, None, Some F.H, ops)
    | 0b100, 0b001110, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MIN_S, None, Some F.W, ops)
    | 0b100, 0b001110, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MIN_S, None, Some F.D, ops)
    | 0b100, 0b001111, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.CLE_S, None, Some F.B, ops)
    | 0b100, 0b001111, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.CLE_S, None, Some F.H, ops)
    | 0b100, 0b001111, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.CLE_S, None, Some F.W, ops)
    | 0b100, 0b001111, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.CLE_S, None, Some F.D, ops)
    | 0b100, 0b010000, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.AVE_S, None, Some F.B, ops)
    | 0b100, 0b010000, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.AVE_S, None, Some F.H, ops)
    | 0b100, 0b010000, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.AVE_S, None, Some F.W, ops)
    | 0b100, 0b010000, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.AVE_S, None, Some F.D, ops)
    | 0b100, 0b010001, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ASUB_S, None, Some F.B, ops)
    | 0b100, 0b010001, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ASUB_S, None, Some F.H, ops)
    | 0b100, 0b010001, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ASUB_S, None, Some F.W, ops)
    | 0b100, 0b010001, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ASUB_S, None, Some F.D, ops)
    | 0b100, 0b010010, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.DIV_S, None, Some F.B, ops)
    | 0b100, 0b010010, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.DIV_S, None, Some F.H, ops)
    | 0b100, 0b010010, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.DIV_S, None, Some F.W, ops)
    | 0b100, 0b010010, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.DIV_S, None, Some F.D, ops)
    | 0b100, 0b010011, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.DPSUB_S, None, Some F.H, ops)
    | 0b100, 0b010011, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.DPSUB_S, None, Some F.W, ops)
    | 0b100, 0b010011, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.DPSUB_S, None, Some F.D, ops)
    | 0b100, 0b010100, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ILVL, None, Some F.B, ops)
    | 0b100, 0b010100, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ILVL, None, Some F.H, ops)
    | 0b100, 0b010100, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ILVL, None, Some F.W, ops)
    | 0b100, 0b010100, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ILVL, None, Some F.D, ops)
    | 0b100, 0b010101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.HADD_S, None, Some F.B, ops)
    | 0b100, 0b010101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.HADD_S, None, Some F.H, ops)
    | 0b100, 0b010101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.HADD_S, None, Some F.W, ops)
    | 0b100, 0b010101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.HADD_S, None, Some F.D, ops)
    | 0b101, 0b001101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.BNEG, None, Some F.B, ops)
    | 0b101, 0b001101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.BNEG, None, Some F.H, ops)
    | 0b101, 0b001101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.BNEG, None, Some F.W, ops)
    | 0b101, 0b001101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.BNEG, None, Some F.D, ops)
    | 0b101, 0b001110, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MIN_U, None, Some F.B, ops)
    | 0b101, 0b001110, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MIN_U, None, Some F.H, ops)
    | 0b101, 0b001110, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MIN_U, None, Some F.W, ops)
    | 0b101, 0b001110, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MIN_U, None, Some F.D, ops)
    | 0b101, 0b001111, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.CLE_U, None, Some F.B, ops)
    | 0b101, 0b001111, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.CLE_U, None, Some F.H, ops)
    | 0b101, 0b001111, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.CLE_U, None, Some F.W, ops)
    | 0b101, 0b001111, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.CLE_U, None, Some F.D, ops)
    | 0b101, 0b010000, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.AVE_U, None, Some F.B, ops)
    | 0b101, 0b010000, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.AVE_U, None, Some F.H, ops)
    | 0b101, 0b010000, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.AVE_U, None, Some F.W, ops)
    | 0b101, 0b010000, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.AVE_U, None, Some F.D, ops)
    | 0b101, 0b010001, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ASUB_U, None, Some F.B, ops)
    | 0b101, 0b010001, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ASUB_U, None, Some F.H, ops)
    | 0b101, 0b010001, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ASUB_U, None, Some F.W, ops)
    | 0b101, 0b010001, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ASUB_U, None, Some F.D, ops)
    | 0b101, 0b010010, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.DIV_U, None, Some F.B, ops)
    | 0b101, 0b010010, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.DIV_U, None, Some F.H, ops)
    | 0b101, 0b010010, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.DIV_U, None, Some F.W, ops)
    | 0b101, 0b010010, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.DIV_U, None, Some F.D, ops)
    | 0b101, 0b010011, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.DPSUB_U, None, Some F.H, ops)
    | 0b101, 0b010011, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.DPSUB_U, None, Some F.W, ops)
    | 0b101, 0b010011, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.DPSUB_U, None, Some F.D, ops)
    | 0b101, 0b010100, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ILVR, None, Some F.B, ops)
    | 0b101, 0b010100, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ILVR, None, Some F.H, ops)
    | 0b101, 0b010100, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ILVR, None, Some F.W, ops)
    | 0b101, 0b010100, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ILVR, None, Some F.D, ops)
    | 0b101, 0b010101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.HADD_U, None, Some F.B, ops)
    | 0b101, 0b010101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.HADD_U, None, Some F.H, ops)
    | 0b101, 0b010101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.HADD_U, None, Some F.W, ops)
    | 0b101, 0b010101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.HADD_U, None, Some F.D, ops)
    | 0b110, 0b001101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.BINSL, None, Some F.B, ops)
    | 0b110, 0b001101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.BINSL, None, Some F.H, ops)
    | 0b110, 0b001101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.BINSL, None, Some F.W, ops)
    | 0b110, 0b001101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.BINSL, None, Some F.D, ops)
    | 0b110, 0b001110, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MAX_A, None, Some F.B, ops)
    | 0b110, 0b001110, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MAX_A, None, Some F.H, ops)
    | 0b110, 0b001110, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MAX_A, None, Some F.W, ops)
    | 0b110, 0b001110, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MAX_A, None, Some F.D, ops)
    | 0b110, 0b010000, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.AVER_S, None, Some F.B, ops)
    | 0b110, 0b010000, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.AVER_S, None, Some F.H, ops)
    | 0b110, 0b010000, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.AVER_S, None, Some F.W, ops)
    | 0b110, 0b010000, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.AVER_S, None, Some F.D, ops)
    | 0b110, 0b010010, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MOD_S, None, Some F.B, ops)
    | 0b110, 0b010010, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MOD_S, None, Some F.H, ops)
    | 0b110, 0b010010, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MOD_S, None, Some F.W, ops)
    | 0b110, 0b010010, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MOD_S, None, Some F.D, ops)
    | 0b110, 0b010100, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ILVEV, None, Some F.B, ops)
    | 0b110, 0b010100, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ILVEV, None, Some F.H, ops)
    | 0b110, 0b010100, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ILVEV, None, Some F.W, ops)
    | 0b110, 0b010100, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ILVEV, None, Some F.D, ops)
    | 0b110, 0b010101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.HSUB_S, None, Some F.B, ops)
    | 0b110, 0b010101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.HSUB_S, None, Some F.H, ops)
    | 0b110, 0b010101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.HSUB_S, None, Some F.W, ops)
    | 0b110, 0b010101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.HSUB_S, None, Some F.D, ops)
    | 0b111, 0b001101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.BINSR, None, Some F.B, ops)
    | 0b111, 0b001101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.BINSR, None, Some F.H, ops)
    | 0b111, 0b001101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.BINSR, None, Some F.W, ops)
    | 0b111, 0b001101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.BINSR, None, Some F.D, ops)
    | 0b111, 0b001110, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MIN_A, None, Some F.B, ops)
    | 0b111, 0b001110, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MIN_A, None, Some F.H, ops)
    | 0b111, 0b001110, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MIN_A, None, Some F.W, ops)
    | 0b111, 0b001110, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MIN_A, None, Some F.D, ops)
    | 0b111, 0b010000, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.AVER_U, None, Some F.B, ops)
    | 0b111, 0b010000, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.AVER_U, None, Some F.H, ops)
    | 0b111, 0b010000, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.AVER_U, None, Some F.W, ops)
    | 0b111, 0b010000, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.AVER_U, None, Some F.D, ops)
    | 0b111, 0b010010, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.MOD_U, None, Some F.B, ops)
    | 0b111, 0b010010, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.MOD_U, None, Some F.H, ops)
    | 0b111, 0b010010, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.MOD_U, None, Some F.W, ops)
    | 0b111, 0b010010, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.MOD_U, None, Some F.D, ops)
    | 0b111, 0b010100, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.ILVOD, None, Some F.B, ops)
    | 0b111, 0b010100, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.ILVOD, None, Some F.H, ops)
    | 0b111, 0b010100, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.ILVOD, None, Some F.W, ops)
    | 0b111, 0b010100, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.ILVOD, None, Some F.D, ops)
    | 0b111, 0b010101, 0b00 ->
        let%map ops = o make_wdwswt in
        (M.HSUB_U, None, Some F.B, ops)
    | 0b111, 0b010101, 0b01 ->
        let%map ops = o make_wdwswt in
        (M.HSUB_U, None, Some F.H, ops)
    | 0b111, 0b010101, 0b10 ->
        let%map ops = o make_wdwswt in
        (M.HSUB_U, None, Some F.W, ops)
    | 0b111, 0b010101, 0b11 ->
        let%map ops = o make_wdwswt in
        (M.HSUB_U, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_msa_elm ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 25 22 with
    | 0b0000 when ex ctx 21 20 = 0b00 ->
        let%map ops = o (make_slidewdwsn 19 16) in
        (M.SLDI, None, Some F.B, ops)
    | 0b0000 when ex ctx 21 19 = 0b100 ->
        let%map ops = o (make_slidewdwsn 18 16) in
        (M.SLDI, None, Some F.H, ops)
    | 0b0000 when ex ctx 21 18 = 0b1100 ->
        let%map ops = o (make_slidewdwsn 17 16) in
        (M.SLDI, None, Some F.W, ops)
    | 0b0000 when ex ctx 21 17 = 0b11100 ->
        let%map ops = o (make_slidewdwsn 16 16) in
        (M.SLDI, None, Some F.D, ops)
    | 0b0000 when ex ctx 21 16 = 0b111110 ->
        let%map ops = o make_cdrs in
        (M.CTCMSA, None, None, ops)
    | 0b0001 when ex ctx 21 20 = 0b00 ->
        let%map ops = o (make_slidewdwsn 19 16) in
        (M.SPLATI, None, Some F.B, ops)
    | 0b0001 when ex ctx 21 19 = 0b100 ->
        let%map ops = o (make_slidewdwsn 18 16) in
        (M.SPLATI, None, Some F.H, ops)
    | 0b0001 when ex ctx 21 18 = 0b1100 ->
        let%map ops = o (make_slidewdwsn 17 16) in
        (M.SPLATI, None, Some F.W, ops)
    | 0b0001 when ex ctx 21 17 = 0b11100 ->
        let%map ops = o (make_slidewdwsn 16 16) in
        (M.SPLATI, None, Some F.D, ops)
    | 0b0001 when ex ctx 21 16 = 0b111110 ->
        let%map ops = o make_rdcs in
        (M.CFCMSA, None, None, ops)
    | 0b0010 when ex ctx 21 20 = 0b00 ->
        let%map ops = o (make_sliderdwsn 19 16) in
        (M.COPY_S, None, Some F.B, ops)
    | 0b0010 when ex ctx 21 19 = 0b100 ->
        let%map ops = o (make_sliderdwsn 18 16) in
        (M.COPY_S, None, Some F.H, ops)
    | 0b0010 when ex ctx 21 18 = 0b1100 ->
        let%map ops = o (make_sliderdwsn 17 16) in
        (M.COPY_S, None, Some F.W, ops)
    | 0b0010 when ex ctx 21 17 = 0b11100 && Mode.word_size_of ctx.mode = 64
      ->
        let%map ops = o (make_sliderdwsn 16 16) in
        (M.COPY_S, None, Some F.D, ops)
    | 0b0010 when ex ctx 21 16 = 0b111110 ->
        let%map ops = o make_wdws in
        (M.MOVE, None, Some F.V, ops)
    | 0b0011 when ex ctx 21 20 = 0b00 ->
        let%map ops = o (make_sliderdwsn 19 16) in
        (M.COPY_U, None, Some F.B, ops)
    | 0b0011 when ex ctx 21 19 = 0b100 ->
        let%map ops = o (make_sliderdwsn 18 16) in
        (M.COPY_U, None, Some F.H, ops)
    | 0b0011 when ex ctx 21 18 = 0b1100 ->
        let%map ops = o (make_sliderdwsn 17 16) in
        (M.COPY_U, None, Some F.W, ops)
    | 0b0011 when ex ctx 21 17 = 0b11100 && Mode.word_size_of ctx.mode = 64
      ->
        let%map ops = o (make_sliderdwsn 16 16) in
        (M.COPY_U, None, Some F.D, ops)
    | 0b0100 when ex ctx 21 20 = 0b00 ->
        let%map ops = o (make_slidewdnrs 19 16) in
        (M.INSERT, None, Some F.B, ops)
    | 0b0100 when ex ctx 21 19 = 0b100 ->
        let%map ops = o (make_slidewdnrs 18 16) in
        (M.INSERT, None, Some F.H, ops)
    | 0b0100 when ex ctx 21 18 = 0b1100 ->
        let%map ops = o (make_slidewdnrs 17 16) in
        (M.INSERT, None, Some F.W, ops)
    | 0b0100 when ex ctx 21 17 = 0b11100 && Mode.word_size_of ctx.mode = 64
      ->
        let%map ops = o (make_slidewdnrs 16 16) in
        (M.INSERT, None, Some F.D, ops)
    | 0b0101 when ex ctx 21 20 = 0b00 ->
        let%map ops = o (make_slidewdnws0 19 16) in
        (M.INSVE, None, Some F.B, ops)
    | 0b0101 when ex ctx 21 19 = 0b100 ->
        let%map ops = o (make_slidewdnws0 18 16) in
        (M.INSVE, None, Some F.H, ops)
    | 0b0101 when ex ctx 21 18 = 0b1100 ->
        let%map ops = o (make_slidewdnws0 17 16) in
        (M.INSVE, None, Some F.W, ops)
    | 0b0101 when ex ctx 21 17 = 0b11100 && Mode.word_size_of ctx.mode = 64
      ->
        let%map ops = o (make_slidewdnws0 16 16) in
        (M.INSVE, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_msa_3rf ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 22, ex ctx 5 0, tst ctx 21) with
    | 0b0000, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FCAF, None, Some F.W, ops)
    | 0b0000, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FCAF, None, Some F.D, ops)
    | 0b0000, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FADD, None, Some F.W, ops)
    | 0b0000, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FADD, None, Some F.D, ops)
    | 0b0001, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FCUN, None, Some F.W, ops)
    | 0b0001, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FCUN, None, Some F.D, ops)
    | 0b0001, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FSUB, None, Some F.W, ops)
    | 0b0001, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FSUB, None, Some F.D, ops)
    | 0b0001, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.FCOR, None, Some F.W, ops)
    | 0b0001, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.FCOR, None, Some F.D, ops)
    | 0b0010, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FCEQ, None, Some F.W, ops)
    | 0b0010, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FCEQ, None, Some F.D, ops)
    | 0b0010, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FMUL, None, Some F.W, ops)
    | 0b0010, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FMUL, None, Some F.D, ops)
    | 0b0010, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.FCUNE, None, Some F.W, ops)
    | 0b0010, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.FCUNE, None, Some F.D, ops)
    | 0b0011, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FCUEQ, None, Some F.W, ops)
    | 0b0011, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FCUEQ, None, Some F.D, ops)
    | 0b0011, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FDIV, None, Some F.W, ops)
    | 0b0011, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FDIV, None, Some F.D, ops)
    | 0b0011, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.FCNE, None, Some F.W, ops)
    | 0b0011, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.FCNE, None, Some F.D, ops)
    | 0b0100, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FCLT, None, Some F.W, ops)
    | 0b0100, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FCLT, None, Some F.D, ops)
    | 0b0100, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FMADD, None, Some F.W, ops)
    | 0b0100, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FMADD, None, Some F.D, ops)
    | 0b0100, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.MUL_Q, None, Some F.W, ops)
    | 0b0100, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.MUL_Q, None, Some F.D, ops)
    | 0b0101, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FCULT, None, Some F.W, ops)
    | 0b0101, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FCULT, None, Some F.D, ops)
    | 0b0101, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FMSUB, None, Some F.W, ops)
    | 0b0101, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FMSUB, None, Some F.D, ops)
    | 0b0101, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.MADD_Q, None, Some F.W, ops)
    | 0b0101, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.MADD_Q, None, Some F.D, ops)
    | 0b0110, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FCLE, None, Some F.W, ops)
    | 0b0110, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FCLE, None, Some F.D, ops)
    | 0b0110, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.MSUB_Q, None, Some F.W, ops)
    | 0b0110, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.MSUB_Q, None, Some F.D, ops)
    | 0b0111, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FCULE, None, Some F.W, ops)
    | 0b0111, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FCULE, None, Some F.D, ops)
    | 0b0111, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FEXP2, None, Some F.W, ops)
    | 0b0111, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FEXP2, None, Some F.D, ops)
    | 0b1000, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FSAF, None, Some F.W, ops)
    | 0b1000, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FSAF, None, Some F.D, ops)
    | 0b1000, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FEXDO, None, Some F.W, ops)
    | 0b1000, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FEXDO, None, Some F.D, ops)
    | 0b1001, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FSUN, None, Some F.W, ops)
    | 0b1001, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FSUN, None, Some F.D, ops)
    | 0b1001, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.FSOR, None, Some F.W, ops)
    | 0b1001, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.FSOR, None, Some F.D, ops)
    | 0b1010, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FSEQ, None, Some F.W, ops)
    | 0b1010, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FSEQ, None, Some F.D, ops)
    | 0b1010, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FTQ, None, Some F.W, ops)
    | 0b1010, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FTQ, None, Some F.D, ops)
    | 0b1010, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.FSUNE, None, Some F.W, ops)
    | 0b1010, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.FSUNE, None, Some F.D, ops)
    | 0b1011, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FSUEQ, None, Some F.W, ops)
    | 0b1011, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FSUEQ, None, Some F.D, ops)
    | 0b1011, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.FSNE, None, Some F.W, ops)
    | 0b1011, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.FSNE, None, Some F.D, ops)
    | 0b1100, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FSLT, None, Some F.W, ops)
    | 0b1100, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FSLT, None, Some F.D, ops)
    | 0b1100, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FMIN, None, Some F.W, ops)
    | 0b1100, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FMIN, None, Some F.D, ops)
    | 0b1100, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.MULR_Q, None, Some F.W, ops)
    | 0b1100, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.MULR_Q, None, Some F.D, ops)
    | 0b1101, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FSULT, None, Some F.W, ops)
    | 0b1101, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FSULT, None, Some F.D, ops)
    | 0b1101, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FMIN_A, None, Some F.W, ops)
    | 0b1101, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FMIN_A, None, Some F.D, ops)
    | 0b1101, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.MADDR_Q, None, Some F.W, ops)
    | 0b1101, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.MADDR_Q, None, Some F.D, ops)
    | 0b1110, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FSLE, None, Some F.W, ops)
    | 0b1110, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FSLE, None, Some F.D, ops)
    | 0b1110, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FMAX, None, Some F.W, ops)
    | 0b1110, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FMAX, None, Some F.D, ops)
    | 0b1110, 0b011100, false ->
        let%map ops = o make_wdwswt in
        (M.MSUBR_Q, None, Some F.W, ops)
    | 0b1110, 0b011100, true ->
        let%map ops = o make_wdwswt in
        (M.MSUBR_Q, None, Some F.D, ops)
    | 0b1111, 0b011010, false ->
        let%map ops = o make_wdwswt in
        (M.FSULE, None, Some F.W, ops)
    | 0b1111, 0b011010, true ->
        let%map ops = o make_wdwswt in
        (M.FSULE, None, Some F.D, ops)
    | 0b1111, 0b011011, false ->
        let%map ops = o make_wdwswt in
        (M.FMAX_A, None, Some F.W, ops)
    | 0b1111, 0b011011, true ->
        let%map ops = o make_wdwswt in
        (M.FMAX_A, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_msa_bit ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match (ex ctx 25 23, ex ctx 5 0) with
    | 0b000, 0b001001 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.SLLI, None, Some F.B, ops)
    | 0b000, 0b001001 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.SLLI, None, Some F.H, ops)
    | 0b000, 0b001001 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.SLLI, None, Some F.W, ops)
    | 0b000, 0b001001 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.SLLI, None, Some F.D, ops)
    | 0b000, 0b001010 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.SAT_S, None, Some F.B, ops)
    | 0b000, 0b001010 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.SAT_S, None, Some F.H, ops)
    | 0b000, 0b001010 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.SAT_S, None, Some F.W, ops)
    | 0b000, 0b001010 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.SAT_S, None, Some F.D, ops)
    | 0b001, 0b001001 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.SRAI, None, Some F.B, ops)
    | 0b001, 0b001001 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.SRAI, None, Some F.H, ops)
    | 0b001, 0b001001 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.SRAI, None, Some F.W, ops)
    | 0b001, 0b001001 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.SRAI, None, Some F.D, ops)
    | 0b001, 0b001010 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.SAT_U, None, Some F.B, ops)
    | 0b001, 0b001010 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.SAT_U, None, Some F.H, ops)
    | 0b001, 0b001010 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.SAT_U, None, Some F.W, ops)
    | 0b001, 0b001010 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.SAT_U, None, Some F.D, ops)
    | 0b010, 0b001001 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.SRLI, None, Some F.B, ops)
    | 0b010, 0b001001 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.SRLI, None, Some F.H, ops)
    | 0b010, 0b001001 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.SRLI, None, Some F.W, ops)
    | 0b010, 0b001001 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.SRLI, None, Some F.D, ops)
    | 0b010, 0b001010 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.SRARI, None, Some F.B, ops)
    | 0b010, 0b001010 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.SRARI, None, Some F.H, ops)
    | 0b010, 0b001010 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.SRARI, None, Some F.W, ops)
    | 0b010, 0b001010 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.SRARI, None, Some F.D, ops)
    | 0b011, 0b001001 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.BCLRI, None, Some F.B, ops)
    | 0b011, 0b001001 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.BCLRI, None, Some F.H, ops)
    | 0b011, 0b001001 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.BCLRI, None, Some F.W, ops)
    | 0b011, 0b001001 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.BCLRI, None, Some F.D, ops)
    | 0b011, 0b001010 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.SRLRI, None, Some F.B, ops)
    | 0b011, 0b001010 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.SRLRI, None, Some F.H, ops)
    | 0b011, 0b001010 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.SRLRI, None, Some F.W, ops)
    | 0b011, 0b001010 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.SRLRI, None, Some F.D, ops)
    | 0b100, 0b001001 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.BSETI, None, Some F.B, ops)
    | 0b100, 0b001001 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.BSETI, None, Some F.H, ops)
    | 0b100, 0b001001 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.BSETI, None, Some F.W, ops)
    | 0b100, 0b001001 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.BSETI, None, Some F.D, ops)
    | 0b101, 0b001001 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.BNEGI, None, Some F.B, ops)
    | 0b101, 0b001001 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.BNEGI, None, Some F.H, ops)
    | 0b101, 0b001001 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.BNEGI, None, Some F.W, ops)
    | 0b101, 0b001001 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.BNEGI, None, Some F.D, ops)
    | 0b110, 0b001001 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.BINSLI, None, Some F.B, ops)
    | 0b110, 0b001001 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.BINSLI, None, Some F.H, ops)
    | 0b110, 0b001001 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.BINSLI, None, Some F.W, ops)
    | 0b110, 0b001001 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.BINSLI, None, Some F.D, ops)
    | 0b111, 0b001001 when ex ctx 22 19 = 0b1110 ->
        let%map ops = o (make_wdwsm 18 16 8) in
        (M.BINSRI, None, Some F.B, ops)
    | 0b111, 0b001001 when ex ctx 22 20 = 0b110 ->
        let%map ops = o (make_wdwsm 19 16 16) in
        (M.BINSRI, None, Some F.H, ops)
    | 0b111, 0b001001 when ex ctx 22 21 = 0b10 ->
        let%map ops = o (make_wdwsm 20 16 32) in
        (M.BINSRI, None, Some F.W, ops)
    | 0b111, 0b001001 when ex ctx 22 22 = 0b0 ->
        let%map ops = o (make_wdwsm 21 16 64) in
        (M.BINSRI, None, Some F.D, ops)
    | _ -> Error `Invalid

  let parse_msa ctx =
    let open Result.Let_syntax in
    match ex ctx 5 0 with
    | 0b000000 | 0b000001 | 0b000010 -> parse_msa_i8 ctx
    | 0b000110 | 0b000111 -> parse_msa_i5 ctx
    | 0b001001 | 0b001010 -> parse_msa_bit ctx
    | 0b001101
     |0b001110
     |0b001111
     |0b010000
     |0b010001
     |0b010010
     |0b010011
     |0b010100
     |0b010101 -> parse_msa_3r ctx
    | 0b011001 -> parse_msa_elm ctx
    | 0b011010 | 0b011011 | 0b011100 -> parse_msa_3rf ctx
    | 0b011110 -> parse_msa_vec2r2rf ctx
    | 0b100000
     |0b100001
     |0b100010
     |0b100011
     |0b100100
     |0b100101
     |0b100110
     |0b100111 -> parse_msa_mi10 ctx
    | _ -> Error `Invalid

  let parse_opcode ctx =
    let open Result.Let_syntax in
    let o = ops_or_error ctx in
    match ex ctx 31 26 with
    | 0b000000 -> parse_special ctx
    | 0b000001 -> parse_regimm ctx
    | 0b000010 -> Ok (M.J, None, None, make_abs26 ctx)
    | 0b000011 -> Ok (M.JAL, None, None, make_abs26 ctx)
    | 0b000100 -> parse_beq ctx
    | 0b000101 ->
        let%map ops = o make_rsrtrel16 in
        (M.BNE, None, None, ops)
    | 0b000110 -> parse_pop06 ctx
    | 0b000111 -> parse_pop07 ctx
    | 0b001000 -> parse_addi_pop10 ctx
    | 0b001001 ->
        let%map ops = o (make_rtrsimm16 32) in
        (M.ADDIU, None, None, ops)
    | 0b001010 ->
        let%map ops = o (make_rtrsimm16 ctx.opsz) in
        (M.SLTI, None, None, ops)
    | 0b001011 ->
        let%map ops = o (make_rtrsimm16 ctx.opsz) in
        (M.SLTIU, None, None, ops)
    | 0b001100 ->
        let%map ops = o (make_rtrsimm16 ctx.opsz) in
        (M.ANDI, None, None, ops)
    | 0b001101 ->
        let%map ops = o (make_rtrsimm16 ctx.opsz) in
        (M.ORI, None, None, ops)
    | 0b001110 ->
        let%map ops = o (make_rtrsimm16 ctx.opsz) in
        (M.XORI, None, None, ops)
    | 0b001111 -> parse_luiaui ctx
    | 0b010000 -> parse_cop0 ctx
    | 0b010001 -> parse_cop1 ctx
    | 0b010010 -> parse_cop2 ctx
    | 0b010011 -> parse_cop1x ctx
    | 0b010100 ->
        let%map ops = o make_rsrtrel16 in
        (M.BEQL, None, None, ops)
    | 0b010101 ->
        let%map ops = o make_rsrtrel16 in
        (M.BNEL, None, None, ops)
    | 0b010110 -> parse_blezl_pop26 ctx
    | 0b010111 -> parse_bgtzl_pop27 ctx
    | 0b011000 -> parse_daddi_pop30 ctx
    | 0b011001 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtrsimm16 64) in
          (M.DADDIU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011010 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtmembaseoff 64) in
          (M.LDL, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011011 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtmembaseoff 64) in
          (M.LDR, None, None, ops)
      | _ -> Error `Invalid )
    | 0b011100 -> parse_special2 ctx
    | 0b011101 -> parse_jalx_daui ctx
    | 0b011110 when is_rel2or6 ctx -> parse_msa ctx
    | 0b011111 -> parse_special3 ctx
    | 0b100000 ->
        let%map ops = o (make_rtmembaseoff 8) in
        (M.LB, None, None, ops)
    | 0b100001 ->
        let%map ops = o (make_rtmembaseoff 16) in
        (M.LH, None, None, ops)
    | 0b100010 ->
        let%map ops = o (make_rtmembaseoff 32) in
        (M.LWL, None, None, ops)
    | 0b100011 ->
        let%map ops = o (make_rtmembaseoff 32) in
        (M.LW, None, None, ops)
    | 0b100100 ->
        let%map ops = o (make_rtmembaseoff 8) in
        (M.LBU, None, None, ops)
    | 0b100101 ->
        let%map ops = o (make_rtmembaseoff 16) in
        (M.LHU, None, None, ops)
    | 0b100110 ->
        let%map ops = o (make_rtmembaseoff 32) in
        (M.LWR, None, None, ops)
    | 0b100111 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtmembaseoff 32) in
          (M.LWU, None, None, ops)
      | _ -> Error `Invalid )
    | 0b101000 ->
        let%map ops = o (make_rtmembaseoff 8) in
        ctx.opsz <- 8;
        (M.SB, None, None, ops)
    | 0b101001 ->
        let%map ops = o (make_rtmembaseoff 16) in
        ctx.opsz <- 16;
        (M.SH, None, None, ops)
    | 0b101010 when not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff 32) in
        ctx.opsz <- 32;
        (M.SWL, None, None, ops)
    | 0b101011 ->
        let%map ops = o (make_rtmembaseoff 32) in
        ctx.opsz <- 32;
        (M.SW, None, None, ops)
    | 0b101100 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_rtmembaseoff 64) in
          ctx.opsz <- 64;
          (M.SDL, None, None, ops)
      | _ -> Error `Invalid )
    | 0b101101 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 ->
          let%map ops = o (make_rtmembaseoff 64) in
          ctx.opsz <- 64;
          (M.SDR, None, None, ops)
      | _ -> Error `Invalid )
    | 0b101110 when not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff 32) in
        ctx.opsz <- 32;
        (M.SWR, None, None, ops)
    | 0b101111 when not (is_rel6 ctx) ->
        let%map ops = o make_hintmembaseoff in
        (M.CACHE, None, None, ops)
    | 0b110000 ->
        let%map ops = o (make_rtmembaseoff 32) in
        (M.LL, None, None, ops)
    | 0b110001 ->
        let%map ops = o (make_ftmembaseoff 32) in
        (M.LWC1, None, None, ops)
    | 0b110010 when is_rel6 ctx -> Ok (M.BC, None, None, make_rel26 ctx)
    | 0b110010 ->
        let%map ops = o (make_rtmembaseoff 32) in
        (M.LWC2, None, None, ops)
    | 0b110011 when not (is_rel6 ctx) ->
        let%map ops = o make_hintmembaseoff in
        (M.PREF, None, None, ops)
    | 0b110100 when not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff 64) in
        (M.LLD, None, None, ops)
    | 0b110101 ->
        let%map ops = o (make_ftmembaseoff 64) in
        (M.LDC1, None, None, ops)
    | 0b110110 when not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff 64) in
        (M.LDC2, None, None, ops)
    | 0b110110 when chk25to21 ctx 0 && is_rel6 ctx ->
        let%map ops = o (make_rtimm16 ctx.opsz) in
        (M.JIC, None, None, ops)
    | 0b110110 when is_rel6 ctx ->
        let%map ops = o make_rsrel21 in
        (M.BEQZC, None, None, ops)
    | 0b110111 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtmembaseoff 64) in
          (M.LD, None, None, ops)
      | _ -> Error `Invalid )
    | 0b111000 when not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff 32) in
        (M.SC, None, None, ops)
    | 0b111001 ->
        let%map ops = o (make_ftmembaseoff 32) in
        ctx.opsz <- 32;
        (M.SWC1, None, None, ops)
    | 0b111010 when not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff 32) in
        ctx.opsz <- 32;
        (M.SWC2, None, None, ops)
    | 0b111010 when is_rel6 ctx -> Ok (M.BALC, None, None, make_rel26 ctx)
    | 0b111011 -> parse_pcrel ctx
    | 0b111100 when not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff 64) in
        ctx.opsz <- 64;
        (M.SCD, None, None, ops)
    | 0b111101 ->
        let%map ops = o (make_ftmembaseoff 64) in
        ctx.opsz <- 64;
        (M.SDC1, None, None, ops)
    | 0b111110 when not (is_rel6 ctx) ->
        let%map ops = o (make_rtmembaseoff 64) in
        ctx.opsz <- 64;
        (M.SDC2, None, None, ops)
    | 0b111110 when chk25to21 ctx 0 && is_rel6 ctx ->
        let%map ops = o (make_rtimm16 ctx.opsz) in
        (M.JIALC, None, None, ops)
    | 0b111110 when is_rel6 ctx ->
        let%map ops = o make_rsrel21 in
        (M.BNEZC, None, None, ops)
    | 0b111111 -> (
      match ctx.mode with
      | Mode.Mips64 | Mode.Mips64r2 | Mode.Mips64r6 ->
          let%map ops = o (make_rtmembaseoff 64) in
          ctx.opsz <- 64;
          (M.SD, None, None, ops)
      | _ -> Error `Invalid )
    | _ -> Error `Invalid
end

let decode mode endian bytes =
  let open Result.Let_syntax in
  (* this will fail on a 32-bit machine *)
  assert (Int.num_bits > 31);
  let%bind bytes =
    Option.try_with (fun () -> Bytes.subo bytes ~len:I.length)
    |> Result.of_option ~error:`OOB
  in
  let b1 = Char.to_int @@ Bytes.get bytes 0 in
  let b2 = Char.to_int @@ Bytes.get bytes 1 in
  let b3 = Char.to_int @@ Bytes.get bytes 2 in
  let b4 = Char.to_int @@ Bytes.get bytes 3 in
  let enc =
    match endian with
    | `BE -> (b1 lsl 24) lor (b2 lsl 16) lor (b3 lsl 8) lor b4
    | `LE -> b1 lor (b2 lsl 8) lor (b3 lsl 16) lor (b4 lsl 24)
  in
  let enc = enc land 0xFFFFFFFF in
  let opsz = Mode.word_size_of mode in
  let ctx = Context.{enc; mode; opsz} in
  let%map mnemonic, cond, fmt, operands = Context.parse_opcode ctx in
  let opsz = ctx.opsz in
  I.{mode; endian; mnemonic; bytes; cond; fmt; operands; opsz}

let decode_exn mode endian bytes =
  match decode mode endian bytes with
  | Ok instr -> instr
  | Error err -> raise (Mips_decode_error err)
