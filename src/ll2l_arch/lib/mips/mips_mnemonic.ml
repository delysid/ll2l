open Base

type t =
  | ABS
  | ADD
  | ADDI
  | ADDIU
  | ADDIUPC
  | ADDU
  | ADDV
  | ADD_A
  | ADDS_A
  | ADDS_S
  | ADDS_U
  | ADDVI
  | ALIGN
  | ALNV
  | ALUIPC
  | AND
  | ANDI
  | ASUB_S
  | ASUB_U
  | AUI
  | AUIPC
  | AVE_S
  | AVE_U
  | AVER_S
  | AVER_U
  | B
  | BAL
  | BALC
  | BC
  | BC1EQZ
  | BC1F
  | BC1FL
  | BC1NEZ
  | BC1T
  | BC1TL
  | BC2EQZ
  | BC2F
  | BC2FL
  | BC2NEZ
  | BC2T
  | BC2TL
  | BCLR
  | BCLRI
  | BEQ
  | BEQC
  | BEQL
  | BEQZALC
  | BEQZC
  | BGEC
  | BGEUC
  | BGEZ
  | BGEZAL
  | BGEZALC
  | BGEZALL
  | BGEZC
  | BGEZL
  | BGTZ
  | BGTZALC
  | BGTZC
  | BGTZL
  | BINSL
  | BINSLI
  | BINSR
  | BINSRI
  | BITSWAP
  | BLEZ
  | BLEZALC
  | BLEZC
  | BLEZL
  | BLTC
  | BLTUC
  | BLTZ
  | BLTZAL
  | BLTZALC
  | BLTZALL
  | BLTZC
  | BLTZL
  | BMNZ
  | BMNZI
  | BMZ
  | BMZI
  | BNE
  | BNEC
  | BNEG
  | BNEGI
  | BNEL
  | BNEZALC
  | BNEZC
  | BNVC
  | BNZ
  | BOVC
  | BREAK
  | BSEL
  | BSELI
  | BSET
  | BSETI
  | BZ
  | C
  | CACHE
  | CACHEE
  | CEILL
  | CEILW
  | CEQ
  | CEQI
  | CFC1
  | CFC2
  | CFCMSA
  | CLASS
  | CLE_S
  | CLE_U
  | CLEI_S
  | CLEI_U
  | CLO
  | CLT_S
  | CLT_U
  | CLTI_S
  | CLTI_U
  | CLZ
  | CMP
  | COPY_S
  | COPY_U
  | CTC1
  | CTC2
  | CTCMSA
  | CVTD
  | CVTL
  | CVTPS
  | CVTS
  | CVTSPL
  | CVTSPU
  | CVTW
  | DADD
  | DADDI
  | DADDIU
  | DADDU
  | DAHI
  | DALIGN
  | DATI
  | DAUI
  | DBITSWAP
  | DCLO
  | DCLZ
  | DDIV
  | DDIVU
  | DERET
  | DEXT
  | DEXTM
  | DEXTU
  | DI
  | DINS
  | DINSM
  | DINSU
  | DIV
  | DIVU
  | DIV_S
  | DIV_U
  | DLSA
  | DMFC0
  | DMFC1
  | DMFC2
  | DMOD
  | DMODU
  | DMTC0
  | DMTC1
  | DMTC2
  | DMUH
  | DMUHU
  | DMUL
  | DMULU
  | DMULT
  | DMULTU
  | DOTP_S
  | DOTP_U
  | DPADD_S
  | DPADD_U
  | DPSUB_S
  | DPSUB_U
  | DROTR
  | DROTR32
  | DROTRV
  | DSBH
  | DSHD
  | DSLL
  | DSLL32
  | DSLLV
  | DSRA
  | DSRA32
  | DSRAV
  | DSRL
  | DSRL32
  | DSRLV
  | DSUB
  | DSUBU
  | EHB
  | EI
  | ERET
  | EXT
  | FADD
  | FCAF
  | FCEQ
  | FCLASS
  | FCLE
  | FCLT
  | FCNE
  | FCOR
  | FCULE
  | FCUN
  | FCUNE
  | FCUEQ
  | FCULT
  | FDIV
  | FEXP2
  | FEXDO
  | FEXUPL
  | FEXUPR
  | FFINT_S
  | FFINT_U
  | FFQL
  | FFQR
  | FILL
  | FLOG2
  | FLOORL
  | FLOORW
  | FMADD
  | FMAX
  | FMAX_A
  | FMIN
  | FMIN_A
  | FMSUB
  | FMUL
  | FRCP
  | FRINT
  | FRSQRT
  | FSAF
  | FSEQ
  | FSLE
  | FSLT
  | FSNE
  | FSOR
  | FSUB
  | FSUEQ
  | FSULE
  | FSULT
  | FSUN
  | FSUNE
  | FSQRT
  | FTINT_S
  | FTINT_U
  | FTQ
  | FTRUNC_S
  | FTRUNC_U
  | HADD_S
  | HADD_U
  | HSUB_S
  | HSUB_U
  | ILVEV
  | ILVL
  | ILVOD
  | ILVR
  | INS
  | INSERT
  | INSVE
  | J
  | JAL
  | JALR
  | JALRHB
  | JALX
  | JIALC
  | JIC
  | JR
  | JRHB
  | LB
  | LBE
  | LBU
  | LBUE
  | LD
  | LDC1
  | LDC2
  | LDI
  | LDL
  | LDPC
  | LDR
  | LDXC1
  | LH
  | LHE
  | LHU
  | LHUE
  | LL
  | LLD
  | LLDP
  | LLE
  | LLWP
  | LLWPE
  | LSA
  | LUI
  | LUXC1
  | LW
  | LWC1
  | LWC2
  | LWE
  | LWL
  | LWLE
  | LWPC
  | LWR
  | LWRE
  | LWU
  | LWUPC
  | LWXC1
  | MADD
  | MADDF
  | MADDR_Q
  | MADDU
  | MADDV
  | MADD_Q
  | MAX
  | MAXA
  | MAXI_S
  | MAXI_U
  | MAX_A
  | MAX_S
  | MAX_U
  | MFC0
  | MFC1
  | MFC2
  | MFHC0
  | MFHC1
  | MFHC2
  | MFHI
  | MFLO
  | MIN
  | MINA
  | MINI_S
  | MINI_U
  | MIN_A
  | MIN_S
  | MIN_U
  | MSUB
  | MSUBF
  | MSUBR_Q
  | MSUBU
  | MSUBV
  | MSUB_Q
  | MOD
  | MODU
  | MOD_S
  | MOD_U
  | MOV
  | MOVE
  | MOVF
  | MOVN
  | MOVT
  | MOVZ
  | MTC0
  | MTC1
  | MTC2
  | MTHC0
  | MTHC1
  | MTHC2
  | MTHI
  | MTLO
  | MUH
  | MUHU
  | MUL
  | MULR_Q
  | MULT
  | MULTU
  | MULU
  | MULV
  | MUL_Q
  | NAL
  | NEG
  | NLOC
  | NLZC
  | NMADD
  | NMSUB
  | NOP
  | NOR
  | NORI
  | OR
  | ORI
  | PAUSE
  | PCKEV
  | PCKOD
  | PCNT
  | PREF
  | PREFE
  | PREFX
  | RDHWR
  | RDPGPR
  | RECIP
  | RINT
  | ROTR
  | ROTRV
  | ROUNDL
  | ROUNDW
  | RSQRT
  | SAT_S
  | SAT_U
  | SB
  | SBE
  | SC
  | SCD
  | SCDP
  | SCE
  | SCWP
  | SCWPE
  | SD
  | SDBBP
  | SDC1
  | SDC2
  | SDL
  | SDR
  | SDXC1
  | SEB
  | SEH
  | SEL
  | SELEQZ
  | SELNEZ
  | SH
  | SHE
  | SHF
  | SIGRIE
  | SLD
  | SLDI
  | SLL
  | SLLI
  | SLLV
  | SLT
  | SLTI
  | SLTIU
  | SLTU
  | SPLAT
  | SPLATI
  | SQRT
  | SRA
  | SRAI
  | SRAR
  | SRARI
  | SRAV
  | SRL
  | SRLI
  | SRLR
  | SRLRI
  | SRLV
  | SSNOP
  | ST
  | SUB
  | SUBS_S
  | SUBS_U
  | SUBSUS_U
  | SUBSUU_S
  | SUBU
  | SUBV
  | SUBVI
  | SUXC1
  | SW
  | SWC1
  | SWC2
  | SWE
  | SWL
  | SWLE
  | SWR
  | SWRE
  | SWXC1
  | SYNC
  | SYNCI
  | SYSCALL
  | TEQ
  | TEQI
  | TGE
  | TGEI
  | TGEIU
  | TGEU
  | TLBINV
  | TLBINVF
  | TLBP
  | TLBR
  | TLBWI
  | TLBWR
  | TLT
  | TLTI
  | TLTIU
  | TLTU
  | TNE
  | TNEI
  | TRUNCL
  | TRUNCW
  | VSHF
  | WAIT
  | WRPGPR
  | WSBH
  | XOR
  | XORI
[@@deriving equal]

let no_fallthrough = function
  | B | BC | J | JAL | JALR | JALRHB | JALX | JIALC | JIC | JR | JRHB -> true
  | _ -> false

let may_fallthrough = function
  | BAL
   |BALC
   |BC1EQZ
   |BC1F
   |BC1FL
   |BC1NEZ
   |BC1T
   |BC1TL
   |BC2EQZ
   |BC2F
   |BC2FL
   |BC2NEZ
   |BC2T
   |BC2TL
   |BEQ
   |BEQC
   |BEQL
   |BEQZALC
   |BEQZC
   |BGEC
   |BGEUC
   |BGEZ
   |BGEZAL
   |BGEZALC
   |BGEZALL
   |BGEZC
   |BGEZL
   |BGTZ
   |BGTZC
   |BGTZALC
   |BGTZL
   |BLEZ
   |BLEZALC
   |BLEZC
   |BLEZL
   |BLTC
   |BLTUC
   |BLTZ
   |BLTZAL
   |BLTZALC
   |BLTZALL
   |BLTZC
   |BLTZL
   |BNE
   |BNEC
   |BNEL
   |BNEZALC
   |BNEZC
   |BNVC
   |BNZ
   |BOVC
   |BZ
   |SIGRIE
   |TEQ
   |TEQI
   |TGE
   |TGEI
   |TGEIU
   |TGEU
   |TLT
   |TLTI
   |TLTIU
   |TLTU
   |TNE
   |TNEI -> true
  | _ -> false

let must_fallthrough m = not (no_fallthrough m || may_fallthrough m)

let delay_slot_size = function
  | B
   |BAL
   |BC1EQZ
   |BC1F
   |BC1FL
   |BC1NEZ
   |BC1T
   |BC1TL
   |BC2EQZ
   |BC2F
   |BC2FL
   |BC2NEZ
   |BC2T
   |BC2TL
   |BEQ
   |BEQL
   |BGEZ
   |BGEZAL
   |BGEZALL
   |BGEZL
   |BGTZ
   |BGTZL
   |BLEZ
   |BLEZL
   |BLTZ
   |BLTZAL
   |BLTZALL
   |BLTZL
   |BNE
   |BNEL
   |BNZ
   |BZ
   |NAL
   |J
   |JAL
   |JALR
   |JALRHB
   |JALX
   |JR
   |JRHB -> 1
  | _ -> 0

let has_conditional_delay = function
  | BC1FL
   |BC1TL
   |BC2FL
   |BC2TL
   |BEQL
   |BGEZALL
   |BGEZL
   |BGTZL
   |BLEZL
   |BLTZALL
   |BLTZL
   |BNEL -> true
  | _ -> false

let is_conditional_branch = function
  | BC1EQZ
   |BC1F
   |BC1FL
   |BC1NEZ
   |BC1T
   |BC1TL
   |BC2EQZ
   |BC2F
   |BC2FL
   |BC2NEZ
   |BC2T
   |BC2TL
   |BEQ
   |BEQC
   |BEQL
   |BEQZALC
   |BEQZC
   |BGEC
   |BGEUC
   |BGEZ
   |BGEZAL
   |BGEZALC
   |BGEZALL
   |BGEZC
   |BGEZL
   |BGTZ
   |BGTZC
   |BGTZALC
   |BGTZL
   |BLEZ
   |BLEZALC
   |BLEZC
   |BLEZL
   |BLTC
   |BLTUC
   |BLTZ
   |BLTZAL
   |BLTZALC
   |BLTZALL
   |BLTZC
   |BLTZL
   |BNE
   |BNEC
   |BNEL
   |BNEZALC
   |BNEZC
   |BNVC
   |BNZ
   |BOVC
   |BZ -> true
  | _ -> false

let is_terminator = function
  | BAL
   |BC
   |BEQC
   |BEQZALC
   |BEQZC
   |BGEC
   |BGEUC
   |BGEZALC
   |BGEZC
   |BGTZC
   |BLEZALC
   |BLEZC
   |BLTC
   |BLTUC
   |BLTZC
   |BNEC
   |BNEZALC
   |BNEZC
   |BNVC
   |BOVC
   |BREAK
   |JIALC
   |JIC
   |SDBBP
   |SIGRIE
   |SYSCALL
   |TEQ
   |TEQI
   |TGE
   |TGEI
   |TGEIU
   |TGEU
   |TLT
   |TLTI
   |TLTIU
   |TLTU
   |TNE
   |TNEI -> true
  | m -> delay_slot_size m > 0

let to_string = function
  | ABS -> "ABS"
  | ADD -> "ADD"
  | ADDI -> "ADDI"
  | ADDIU -> "ADDIU"
  | ADDIUPC -> "ADDIUPC"
  | ADDS_A -> "ADDS_A"
  | ADDS_S -> "ADDS_S"
  | ADDS_U -> "ADDS_U"
  | ADDU -> "ADDU"
  | ADDV -> "ADDV"
  | ADDVI -> "ADDVI"
  | ADD_A -> "ADD_A"
  | ALIGN -> "ALIGN"
  | ALNV -> "ALNV"
  | ALUIPC -> "ALUIPC"
  | AND -> "AND"
  | ANDI -> "ANDI"
  | ASUB_S -> "ASUB_S"
  | ASUB_U -> "ASUB_U"
  | AUI -> "AUI"
  | AUIPC -> "AUIPC"
  | AVE_S -> "AVE_S"
  | AVE_U -> "AVE_U"
  | AVER_S -> "AVER_S"
  | AVER_U -> "AVER_U"
  | B -> "B"
  | BAL -> "BAL"
  | BALC -> "BALC"
  | BC -> "BC"
  | BC1EQZ -> "BC1EQZ"
  | BC1F -> "BC1F"
  | BC1FL -> "BC1FL"
  | BC1NEZ -> "BC1NEZ"
  | BC1T -> "BC1T"
  | BC1TL -> "BC1TL"
  | BC2EQZ -> "BC2EQZ"
  | BC2F -> "BC2F"
  | BC2FL -> "BC2FL"
  | BC2NEZ -> "BC2NEZ"
  | BC2T -> "BC2T"
  | BC2TL -> "BC2TL"
  | BCLR -> "BCLR"
  | BCLRI -> "BCLRI"
  | BEQ -> "BEQ"
  | BEQC -> "BEQC"
  | BEQL -> "BEQL"
  | BEQZALC -> "BEQZALC"
  | BEQZC -> "BEQZC"
  | BGEC -> "BGEC"
  | BGEUC -> "BGEUC"
  | BGEZ -> "BGEZ"
  | BGEZAL -> "BGEZAL"
  | BGEZALC -> "BGEZALC"
  | BGEZALL -> "BGEZALL"
  | BGEZC -> "BGEZC"
  | BGEZL -> "BGEZL"
  | BGTZ -> "BGTZ"
  | BGTZC -> "BGTZC"
  | BGTZALC -> "BGTZALC"
  | BGTZL -> "BGTZL"
  | BINSL -> "BINSL"
  | BINSLI -> "BINSLI"
  | BINSR -> "BINSR"
  | BINSRI -> "BINSRI"
  | BITSWAP -> "BITSWAP"
  | BLEZ -> "BLEZ"
  | BLEZALC -> "BLEZALC"
  | BLEZC -> "BLEZC"
  | BLEZL -> "BLEZL"
  | BLTC -> "BLTC"
  | BLTUC -> "BLTUC"
  | BLTZ -> "BLTZ"
  | BLTZAL -> "BLTZAL"
  | BLTZALC -> "BLTZALC"
  | BLTZALL -> "BLTZALL"
  | BLTZC -> "BLTZC"
  | BLTZL -> "BLTZL"
  | BMNZ -> "BMNZ"
  | BMNZI -> "BMNZI"
  | BMZ -> "BMZ"
  | BMZI -> "BMZI"
  | BNE -> "BNE"
  | BNEC -> "BNEC"
  | BNEG -> "BNEG"
  | BNEGI -> "BNEGI"
  | BNEL -> "BNEL"
  | BNEZALC -> "BNEZALC"
  | BNEZC -> "BNEZC"
  | BNVC -> "BNVC"
  | BNZ -> "BNZ"
  | BOVC -> "BOVC"
  | BREAK -> "BREAK"
  | BSEL -> "BSEL"
  | BSELI -> "BSELI"
  | BSET -> "BSET"
  | BSETI -> "BSETI"
  | BZ -> "BZ"
  | C -> "C"
  | CACHE -> "CACHE"
  | CACHEE -> "CACHEE"
  | CEILL -> "CEIL.L"
  | CEILW -> "CEIL.W"
  | CEQ -> "CEQ"
  | CEQI -> "CEQI"
  | CFC1 -> "CFC1"
  | CFC2 -> "CFC2"
  | CFCMSA -> "CFCMSA"
  | CLASS -> "CLASS"
  | CLE_S -> "CLE_S"
  | CLE_U -> "CLE_U"
  | CLEI_S -> "CLEI_S"
  | CLEI_U -> "CLEI_U"
  | CLO -> "CLO"
  | CLT_S -> "CLT_S"
  | CLT_U -> "CLT_U"
  | CLTI_S -> "CLTI_S"
  | CLTI_U -> "CLTI_U"
  | CLZ -> "CLZ"
  | CMP -> "CMP"
  | COPY_S -> "COPY_S"
  | COPY_U -> "COPY_U"
  | CTC1 -> "CTC1"
  | CTC2 -> "CTC2"
  | CTCMSA -> "CTCMSA"
  | CVTD -> "CVT.D"
  | CVTL -> "CVT.L"
  | CVTPS -> "CVT.PS"
  | CVTS -> "CVT.S"
  | CVTSPL -> "CVT.S.PL"
  | CVTSPU -> "CVT.S.PU"
  | CVTW -> "CVT.W"
  | DADD -> "DADD"
  | DADDI -> "DADDI"
  | DADDIU -> "DADDIU"
  | DADDU -> "DADDU"
  | DAHI -> "DAHI"
  | DALIGN -> "DALIGN"
  | DATI -> "DATI"
  | DAUI -> "DAUI"
  | DBITSWAP -> "DBITSWAP"
  | DCLO -> "DCLO"
  | DCLZ -> "DCLZ"
  | DDIV -> "DDIV"
  | DDIVU -> "DDIVU"
  | DERET -> "DERET"
  | DEXT -> "DEXT"
  | DEXTM -> "DEXTM"
  | DEXTU -> "DEXTU"
  | DI -> "DI"
  | DINS -> "DINS"
  | DINSM -> "DINSM"
  | DINSU -> "DINSU"
  | DIV -> "DIV"
  | DIVU -> "DIVU"
  | DIV_S -> "DIV_S"
  | DIV_U -> "DIV_U"
  | DLSA -> "DLSA"
  | DMFC0 -> "DMFC0"
  | DMFC1 -> "DMFC1"
  | DMFC2 -> "DMFC2"
  | DMOD -> "DMOD"
  | DMODU -> "DMODU"
  | DMTC0 -> "DMTC0"
  | DMTC1 -> "DMTC1"
  | DMTC2 -> "DMTC2"
  | DMUH -> "DMUH"
  | DMUHU -> "DUHU"
  | DMUL -> "DMUL"
  | DMULU -> "DMULU"
  | DMULT -> "DMULT"
  | DMULTU -> "DMULTU"
  | DOTP_S -> "DOTP_S"
  | DOTP_U -> "DOTP_U"
  | DPADD_S -> "DPADD_S"
  | DPADD_U -> "DPADD_U"
  | DPSUB_S -> "DPSUB_S"
  | DPSUB_U -> "DPSUB_U"
  | DROTR -> "DROTR"
  | DROTR32 -> "DROTR32"
  | DROTRV -> "DROTRV"
  | DSBH -> "DSBH"
  | DSHD -> "DSHD"
  | DSLL -> "DSLL"
  | DSLL32 -> "DSLL32"
  | DSLLV -> "DSLLV"
  | DSRA -> "DSRA"
  | DSRA32 -> "DSRA32"
  | DSRAV -> "DSRAV"
  | DSRL -> "DSRL"
  | DSRL32 -> "DSRL32"
  | DSRLV -> "DSRLV"
  | DSUB -> "DSUB"
  | DSUBU -> "DSUBU"
  | EHB -> "EHB"
  | EI -> "EI"
  | ERET -> "ERET"
  | EXT -> "EXT"
  | FADD -> "FADD"
  | FCAF -> "FCAF"
  | FCEQ -> "FCEQ"
  | FCLASS -> "FCLASS"
  | FCLE -> "FCLE"
  | FCLT -> "FCLT"
  | FCNE -> "FCNE"
  | FCOR -> "FCOR"
  | FCULE -> "FCULE"
  | FCUN -> "FCUN"
  | FCUNE -> "FCUNE"
  | FCUEQ -> "FCUEQ"
  | FCULT -> "FCULT"
  | FDIV -> "FDIV"
  | FEXP2 -> "FEXP2"
  | FEXDO -> "FEXDO"
  | FEXUPL -> "FEXUPL"
  | FEXUPR -> "FEXUPR"
  | FFINT_S -> "FFINT_S"
  | FFINT_U -> "FFINT_U"
  | FFQL -> "FFQL"
  | FFQR -> "FFQR"
  | FILL -> "FILL"
  | FLOG2 -> "FLOG2"
  | FLOORL -> "FLOOR.L"
  | FLOORW -> "FLOOR.W"
  | FMADD -> "FMADD"
  | FMAX -> "FMAX"
  | FMAX_A -> "FMAX_A"
  | FMIN -> "FMIN"
  | FMIN_A -> "FMIN_A"
  | FMSUB -> "FMSUB"
  | FMUL -> "FMUL"
  | FRCP -> "FRCP"
  | FRINT -> "FRINT"
  | FRSQRT -> "FRSQRT"
  | FSAF -> "FSAF"
  | FSEQ -> "FSEQ"
  | FSLE -> "FSLE"
  | FSLT -> "FSLT"
  | FSNE -> "FSNE"
  | FSOR -> "FSOR"
  | FSQRT -> "FSQRT"
  | FSUB -> "FSUB"
  | FSUEQ -> "FSUEQ"
  | FSULE -> "FSULE"
  | FSULT -> "FSULT"
  | FSUN -> "FSUN"
  | FSUNE -> "FSUNE"
  | FTINT_S -> "FTINT_S"
  | FTINT_U -> "FTINT_U"
  | FTQ -> "FTQ"
  | FTRUNC_S -> "FTRUNC_S"
  | FTRUNC_U -> "FTRUNC_U"
  | HADD_S -> "HADD_S"
  | HADD_U -> "HADD_U"
  | HSUB_S -> "HSUB_S"
  | HSUB_U -> "HSUB_U"
  | ILVEV -> "ILVEV"
  | ILVL -> "ILVL"
  | ILVOD -> "ILVOD"
  | ILVR -> "ILVR"
  | INS -> "INS"
  | INSERT -> "INSERT"
  | INSVE -> "INSVE"
  | J -> "J"
  | JAL -> "JAL"
  | JALR -> "JALR"
  | JALRHB -> "JALR.HB"
  | JALX -> "JALX"
  | JIALC -> "JIALC"
  | JIC -> "JIC"
  | JR -> "JR"
  | JRHB -> "JR.HB"
  | LB -> "LB"
  | LBE -> "LBE"
  | LBU -> "LBU"
  | LBUE -> "LBUE"
  | LD -> "LD"
  | LDC1 -> "LDC1"
  | LDC2 -> "LDC2"
  | LDI -> "LDI"
  | LDL -> "LDL"
  | LDPC -> "LDPC"
  | LDR -> "LDR"
  | LDXC1 -> "LDXC1"
  | LH -> "LH"
  | LHE -> "LHE"
  | LHU -> "LHU"
  | LHUE -> "LHUE"
  | LL -> "LL"
  | LLD -> "LLD"
  | LLDP -> "LLDP"
  | LLE -> "LLE"
  | LLWP -> "LLWP"
  | LLWPE -> "LLWPE"
  | LSA -> "LSA"
  | LUI -> "LUI"
  | LUXC1 -> "LUXC1"
  | LW -> "LW"
  | LWC1 -> "LWC1"
  | LWC2 -> "LWC2"
  | LWE -> "LWE"
  | LWL -> "LWL"
  | LWLE -> "LWLE"
  | LWPC -> "LWPC"
  | LWR -> "LWR"
  | LWRE -> "LWRE"
  | LWU -> "LWU"
  | LWUPC -> "LWUPC"
  | LWXC1 -> "LWXC1"
  | PCKEV -> "PCKEV"
  | PCKOD -> "PCKOD"
  | PCNT -> "PCNT"
  | MADD -> "MADD"
  | MADDF -> "MADDF"
  | MADDR_Q -> "MADDR_Q"
  | MADDU -> "MADDU"
  | MADDV -> "MADDV"
  | MADD_Q -> "MADD_Q"
  | MAX -> "MAX"
  | MAXA -> "MAXA"
  | MAXI_S -> "MAXI_S"
  | MAXI_U -> "MAXI_U"
  | MAX_A -> "MAX_A"
  | MAX_S -> "MAX_S"
  | MAX_U -> "MAX_U"
  | MFC0 -> "MFC0"
  | MFC1 -> "MFC1"
  | MFC2 -> "MFC2"
  | MFHC0 -> "MFHC0"
  | MFHC1 -> "MFHC1"
  | MFHC2 -> "MFHC2"
  | MFHI -> "MFHI"
  | MFLO -> "MFLO"
  | MIN -> "MIN"
  | MINA -> "MINA"
  | MINI_S -> "MINI_S"
  | MINI_U -> "MINI_U"
  | MIN_A -> "MIN_A"
  | MIN_S -> "MIN_S"
  | MIN_U -> "MIN_U"
  | MSUB -> "MSUB"
  | MSUBF -> "MSUBF"
  | MSUBR_Q -> "MSUBR_Q"
  | MSUBU -> "MSUBU"
  | MSUBV -> "MSUBV"
  | MSUB_Q -> "MSUB_Q"
  | MOD -> "MOD"
  | MODU -> "MODU"
  | MOD_S -> "MOD_S"
  | MOD_U -> "MOD_U"
  | MOV -> "MOV"
  | MOVE -> "MOVE"
  | MOVF -> "MOVF"
  | MOVN -> "MOVN"
  | MOVT -> "MOVT"
  | MOVZ -> "MOVZ"
  | MTC0 -> "MTC0"
  | MTC1 -> "MTC1"
  | MTC2 -> "MTC2"
  | MTHC0 -> "MTHC0"
  | MTHC1 -> "MTHC1"
  | MTHC2 -> "MTHC2"
  | MTHI -> "MTHI"
  | MTLO -> "MTLO"
  | MUH -> "MUH"
  | MUHU -> "MUHU"
  | MUL -> "MUL"
  | MULR_Q -> "MULR_Q"
  | MULU -> "MULU"
  | MULT -> "MULT"
  | MULTU -> "MULTU"
  | MULV -> "MULV"
  | MUL_Q -> "MUL_Q"
  | NAL -> "NAL"
  | NEG -> "NEG"
  | NLOC -> "NLOC"
  | NLZC -> "NLZC"
  | NMADD -> "NMADD"
  | NMSUB -> "NMSUB"
  | NOP -> "NOP"
  | NOR -> "NOR"
  | NORI -> "NORI"
  | OR -> "OR"
  | ORI -> "ORI"
  | PAUSE -> "PAUSE"
  | PREF -> "PREF"
  | PREFE -> "PREFE"
  | PREFX -> "PREFX"
  | RDHWR -> "RDHWR"
  | RDPGPR -> "RDPGPR"
  | RECIP -> "RECIP"
  | RINT -> "RINT"
  | ROTR -> "ROTR"
  | ROTRV -> "ROTRV"
  | ROUNDL -> "ROUND.L"
  | ROUNDW -> "ROUND.W"
  | RSQRT -> "RSQRT"
  | SAT_S -> "SAT_S"
  | SAT_U -> "SAT_U"
  | SB -> "SB"
  | SBE -> "SBE"
  | SC -> "SC"
  | SCD -> "SCD"
  | SCDP -> "SCDP"
  | SCE -> "SCE"
  | SCWP -> "SCWP"
  | SCWPE -> "SCWPE"
  | SD -> "SD"
  | SDBBP -> "SDBBP"
  | SDC1 -> "SDC1"
  | SDC2 -> "SDC2"
  | SDL -> "SDL"
  | SDR -> "SDR"
  | SDXC1 -> "SDXC1"
  | SEB -> "SEB"
  | SEH -> "SEH"
  | SEL -> "SEL"
  | SELEQZ -> "SELEQZ"
  | SELNEZ -> "SELNEZ"
  | SH -> "SH"
  | SHE -> "SHE"
  | SHF -> "SHF"
  | SIGRIE -> "SIGRIE"
  | SLD -> "SLD"
  | SLDI -> "SLDI"
  | SLL -> "SLL"
  | SLLI -> "SLLI"
  | SLLV -> "SLLV"
  | SLT -> "SLT"
  | SLTI -> "SLTI"
  | SLTIU -> "SLTIU"
  | SLTU -> "SLTU"
  | SPLAT -> "SPLAT"
  | SPLATI -> "SPLATI"
  | SQRT -> "SQRT"
  | SRA -> "SRA"
  | SRAI -> "SRAI"
  | SRAR -> "SRAR"
  | SRARI -> "SRARI"
  | SRAV -> "SRAV"
  | SRL -> "SRL"
  | SRLI -> "SRLI"
  | SRLR -> "SRLR"
  | SRLRI -> "SRLRI"
  | SRLV -> "SRLV"
  | SSNOP -> "SSNOP"
  | ST -> "ST"
  | SUB -> "SUB"
  | SUBS_S -> "SUBS_S"
  | SUBS_U -> "SUBS_U"
  | SUBSUS_U -> "SUBSUS_U"
  | SUBSUU_S -> "SUBSUU_S"
  | SUBU -> "SUBU"
  | SUBV -> "SUBV"
  | SUBVI -> "SUBVI"
  | SUXC1 -> "SUXC1"
  | SW -> "SW"
  | SWC1 -> "SWC1"
  | SWC2 -> "SWC2"
  | SWE -> "SWE"
  | SWL -> "SWL"
  | SWLE -> "SWLE"
  | SWR -> "SWR"
  | SWRE -> "SWRE"
  | SWXC1 -> "SWXC1"
  | SYNC -> "SYNC"
  | SYNCI -> "SYNCI"
  | SYSCALL -> "SYSCALL"
  | TEQ -> "TEQ"
  | TEQI -> "TEQI"
  | TGE -> "TGE"
  | TGEI -> "TGEI"
  | TGEIU -> "TEGIU"
  | TGEU -> "TGEU"
  | TLBINV -> "TLBINV"
  | TLBINVF -> "TLBINVF"
  | TLBP -> "TLBP"
  | TLBR -> "TLBR"
  | TLBWI -> "TLBWI"
  | TLBWR -> "TLBWR"
  | TLT -> "TLT"
  | TLTI -> "TLTI"
  | TLTIU -> "TLTIU"
  | TLTU -> "TLTU"
  | TNE -> "TNE"
  | TNEI -> "TNEI"
  | TRUNCL -> "TRUNCL"
  | TRUNCW -> "TRUNCW"
  | VSHF -> "VSHF"
  | WAIT -> "WAIT"
  | WRPGPR -> "WRPGPR"
  | WSBH -> "WSBH"
  | XOR -> "XOR"
  | XORI -> "XORI"
