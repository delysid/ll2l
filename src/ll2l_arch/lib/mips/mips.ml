(* this code is borrowed (and extended/improved) from the B2R2 project *)

open Core_kernel
open Ll2l_std
module Register = Mips_register
module Mnemonic = Mips_mnemonic

module Mode = struct
  type t = Mips32 | Mips32r2 | Mips32r6 | Mips64 | Mips64r2 | Mips64r6
  [@@deriving equal]

  let word_size_of = function
    | Mips32 | Mips32r2 | Mips32r6 -> 32
    | Mips64 | Mips64r2 | Mips64r6 -> 64

  let of_string = function
    | "mips32" -> Mips32
    | "mips32r2" -> Mips32r2
    | "mips32r6" -> Mips32r6
    | "mips64" -> Mips64
    | "mips64r2" -> Mips64r2
    | "mips64r6" -> Mips64r6
    | _ -> invalid_arg "bad MIPS mode"

  let to_string = function
    | Mips32 -> "mips32"
    | Mips32r2 -> "mips32r2"
    | Mips32r6 -> "mips32r6"
    | Mips64 -> "mips64"
    | Mips64r2 -> "mips64r2"
    | Mips64r6 -> "mips64r6"

  let is_rel2 = function
    | Mips32r2 | Mips64r2 -> true
    | _ -> false

  let is_rel6 = function
    | Mips32r6 | Mips64r6 -> true
    | _ -> false

  let is_rel2or6 m = is_rel2 m || is_rel6 m [@@inline]
end

module Condition = struct
  type t =
    | F
    | UN
    | EQ
    | UEQ
    | GE
    | LT
    | OLT
    | ULT
    | LE
    | OLE
    | ULE
    | NE
    | UNE
    | SF
    | NGLE
    | SEQ
    | NGL
    | NGE
    | NGT
    | SLT
    | SULT
    | SLE
    | AF
    | SAF
    | SUB
    | SUEQ
    | SULE
    | SNE
    | SUNE
    | OR
    | SOR
  [@@deriving equal]

  let to_string = function
    | F -> "F"
    | UN -> "UN"
    | EQ -> "EQ"
    | UEQ -> "UEQ"
    | GE -> "GE"
    | LT -> "LT"
    | OLT -> "OLT"
    | ULT -> "ULT"
    | LE -> "LE"
    | OLE -> "OLE"
    | ULE -> "ULE"
    | NE -> "NE"
    | UNE -> "UNE"
    | SF -> "SF"
    | NGLE -> "NGLE"
    | SEQ -> "SEQ"
    | NGL -> "NGL"
    | NGE -> "NGE"
    | NGT -> "NGT"
    | SLT -> "SLT"
    | SULT -> "SULT"
    | SLE -> "SLE"
    | AF -> "AF"
    | SAF -> "SAF"
    | SUB -> "SUB"
    | SUEQ -> "SUEQ"
    | SULE -> "SULE"
    | SNE -> "SNE"
    | SUNE -> "SUNE"
    | OR -> "OR"
    | SOR -> "SOR"
end

module Format = struct
  type t = S | D | W | L | PS | OB | QH | V | B | H [@@deriving equal]

  let to_string = function
    | S -> "S"
    | D -> "D"
    | W -> "W"
    | L -> "L"
    | PS -> "PS"
    | OB -> "OB"
    | QH -> "QH"
    | V -> "V"
    | B -> "B"
    | H -> "H"
end

module Immediate = struct
  type kind = Relative | Absolute | Shift | Literal [@@deriving equal]

  type t = {value: int64; kind: kind; size: int}

  let is_relative imm = equal_kind imm.kind Relative

  let is_absolute imm = equal_kind imm.kind Absolute

  let is_shift imm = equal_kind imm.kind Shift

  let is_literal imm = equal_kind imm.kind Literal
end

module Operand = struct
  type addend = Disp of int64 | Index of Register.t

  type slide = Slide_n of int | Slide_r of Register.t

  type t =
    | Register of Register.t
    | Immediate of Immediate.t
    | Memory of {base: Register.t; addend: addend option; size: int option}
    | Slide of {vector: Register.t; column: slide}

  let to_string ~addr ~opsz = function
    | Register r -> Register.to_string r
    | Immediate imm -> (
      match imm.kind with
      | Immediate.Relative ->
          let value = Addr.(addr + int64 imm.value) in
          if opsz = 64 then Printf.sprintf "0x%016LX" (Addr.to_int64 value)
          else Printf.sprintf "0x%08lX" (Addr.to_int32 value)
      | Immediate.Absolute ->
          let addr = Addr.to_int64 addr in
          if opsz = 64 then
            let hi_bits = Int64.((addr + 4L) land 0x7FFFFFFFF0000000L) in
            let value = Int64.(hi_bits lor imm.value) in
            Printf.sprintf "0x%016LX" value
          else
            let hi_bits = Int64.((addr + 4L) land 0xF0000000L) in
            let value = Int64.(hi_bits lor imm.value) in
            Printf.sprintf "0x%08LX" Int64.(value land 0xFFFFFFFFL)
      | Immediate.Shift | Immediate.Literal ->
          Printf.sprintf "0x%LX" imm.value )
    | Memory mem -> (
        let b = Register.to_string mem.base in
        match mem.addend with
        | Some (Disp disp) ->
            if Int64.is_negative disp then
              let disp = Int64.(lnot disp + 1L) in
              Printf.sprintf "-0x%LX(%s)" disp b
            else Printf.sprintf "0x%LX(%s)" disp b
        | Some (Index idx) ->
            Printf.sprintf "%s(%s)" (Register.to_string idx) b
        | None -> Printf.sprintf "(%s)" b )
    | Slide sl ->
        let column =
          match sl.column with
          | Slide_n n -> Int.to_string n
          | Slide_r r -> Register.to_string r
        in
        Printf.sprintf "%s[%s]" (Register.to_string sl.vector) column
end

module Instruction = struct
  type t =
    { mode: Mode.t
    ; endian: Endian.t
    ; mnemonic: Mnemonic.t
    ; bytes: bytes
    ; cond: Condition.t option
    ; fmt: Format.t option
    ; operands: Operand.t array
    ; opsz: int }

  let length = 4

  let min_length = 4

  let max_length = 4

  let end_addr ~addr = Addr.(addr + int length) [@@inline]

  let opcode instr =
    let b1 = Char.to_int @@ Bytes.get instr.bytes 0 in
    let b2 = Char.to_int @@ Bytes.get instr.bytes 1 in
    let b3 = Char.to_int @@ Bytes.get instr.bytes 2 in
    let b4 = Char.to_int @@ Bytes.get instr.bytes 3 in
    let n =
      match instr.endian with
      | `BE -> (b1 lsl 24) lor (b2 lsl 16) lor (b3 lsl 8) lor b4
      | `LE -> b1 lor (b2 lsl 8) lor (b3 lsl 16) lor (b4 lsl 24)
    in
    n land 0xFFFFFFFF

  let no_fallthrough instr =
    Mnemonic.no_fallthrough instr.mnemonic
    [@@inline]

  let may_fallthrough instr =
    Mnemonic.may_fallthrough instr.mnemonic
    [@@inline]

  let must_fallthrough instr =
    Mnemonic.must_fallthrough instr.mnemonic
    [@@inline]

  let is_terminator instr = Mnemonic.is_terminator instr.mnemonic [@@inline]

  let delay_slot_size instr =
    Mnemonic.delay_slot_size instr.mnemonic
    [@@inline]

  let is_conditional_branch instr =
    Mnemonic.is_conditional_branch instr.mnemonic
    [@@inline]

  let has_conditional_delay instr =
    Mnemonic.has_conditional_delay instr.mnemonic
    [@@inline]

  let branch_address instr ~addr =
    if not (is_terminator instr) then None
    else
      Array.find_map instr.operands ~f:(function
        | Operand.(Immediate {value; kind= Relative}) ->
            Some Addr.(int64 value + addr)
        | Operand.(Immediate {value; kind= Absolute}) ->
            let value =
              let addr = Addr.to_int64 addr in
              let mask =
                match instr.opsz with
                | 64 -> 0x7FFFFFFFF0000000L
                (* assume 32 *)
                | _ -> 0xF0000000L
              in
              Int64.((addr + 4L) land mask lor value)
            in
            Some Addr.(int64 value + addr)
        | _ -> None)

  let to_string ~addr instr =
    let m_str = Mnemonic.to_string instr.mnemonic in
    let cond_str =
      match instr.cond with
      | None -> ""
      | Some c -> "." ^ Condition.to_string c
    in
    let fmt_str =
      match instr.fmt with
      | None -> ""
      | Some f -> "." ^ Format.to_string f
    in
    let op_str =
      if Array.is_empty instr.operands then ""
      else
        List.mapi (Array.to_list instr.operands) ~f:(fun _ op ->
            Operand.to_string op ~addr ~opsz:instr.opsz)
        |> String.concat ~sep:", " |> Printf.sprintf " %s"
    in
    m_str ^ cond_str ^ fmt_str ^ op_str

  let to_string_full ~addr instr =
    let addr_str =
      match Mode.word_size_of instr.mode with
      | 64 -> Printf.sprintf "0x%016LX" (Addr.to_int64 addr)
      (* 32 *)
      | _ -> Printf.sprintf "0x%08lX" (Addr.to_int32 addr)
    in
    let bytes_str =
      Bytes.to_list instr.bytes |> List.map ~f:Char.to_int
      |> List.map ~f:(Printf.sprintf "%02X")
      |> String.concat ~sep:" "
    in
    Printf.sprintf "%s:  %s  %s" addr_str bytes_str (to_string instr ~addr)
end
