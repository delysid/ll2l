open Core_kernel
open Ll2l_ir
open Ll2l_std

type error = [`BadInstr | `BadDelay | `BadOperand of int]

val string_of_error : error -> string

exception Mips_lift_error of error

val lift :
     ?delay:Mips.Instruction.t option
  -> Mips.Instruction.t
  -> Addr.t
  -> Il.Context.t
  -> (Il.t, error) Result.t

val lift_exn :
     ?delay:Mips.Instruction.t option
  -> Mips.Instruction.t
  -> Addr.t
  -> Il.Context.t
  -> Il.t
