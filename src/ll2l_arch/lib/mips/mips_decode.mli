open Core_kernel
open Ll2l_std

type error = [`OOB | `Invalid | `BadOperand | `Unimpl]

val string_of_error : error -> string

exception Mips_decode_error of error

val decode :
  Mips.Mode.t -> Endian.t -> bytes -> (Mips.Instruction.t, error) Result.t

val decode_exn : Mips.Mode.t -> Endian.t -> bytes -> Mips.Instruction.t
