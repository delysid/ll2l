# `ll2l-ir`

This library provides two core modules for representing the semantics of binary programs:

- The `Il` module, which is used for declaring the raw semantics of instructions.
- The `Ir` module, which is a graphical representation of `Il` programs.

A concrete evaluator for `Ir` programs is provided in the `Ir_eval` module.
Additionally, the `Vsa` module provides an adaption of the value-set analysis (VSA) seen in [CBAT](https://github.com/draperlaboratory/cbat_tools).
Future work will use `Vsa` in order to resolve indirect control-flow targets.
