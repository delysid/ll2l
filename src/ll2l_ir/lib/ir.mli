open Core_kernel
open Ll2l_std

type error = [`BadBlock | `BadLabel | `BadDef | `BadCtrl]

val string_of_error : error -> string

exception Ir_exception of error

module Label : sig
  type t = private int

  type comparator_witness

  val comparator : (t, comparator_witness) Comparator.comparator

  val compare : t -> t -> int

  val equal : t -> t -> bool

  val sexp_of_t : t -> Sexp.t

  val t_of_sexp : Sexp.t -> t

  val hash : t -> int

  val hash_fold_t : Hash.state -> t -> Hash.state

  val to_string : t -> string
end

module Label_set : module type of Set.Make (Label)

module Label_map : module type of Map.Make (Label)

module Def : sig
  module Ord : module type of Label

  module Ord_set : module type of Set.Make (Ord)

  module Ord_map : module type of Map.Make (Ord)

  type t = Il.var * Il.expr [@@deriving equal]

  val to_string : t -> string
end

module Ctrl : sig
  type dest = Direct of Label.t | Indirect of Il.expr [@@deriving equal]

  val string_of_dest : dest -> string

  type switch = string option option [@@deriving equal]

  type t =
    | Call of dest * dest option * switch
    | Jmp of dest * switch
    | JmpIf of Il.expr * dest * dest
    | Special of Il.special * dest option
  [@@deriving equal]

  val to_string : t -> string
end

module Block : sig
  type t

  val label_of : t -> Label.t

  val addr_of : t -> Addr.t

  val alist_of_defs : t -> (Def.Ord.t * Def.t) list

  val iter_defs : t -> f:(Def.t -> unit) -> unit

  val iteri_defs : t -> f:(key:Def.Ord.t -> data:Def.t -> unit) -> unit

  val foldi_defs :
    t -> init:'acc -> f:('acc -> Def.Ord.t -> Def.t -> 'acc) -> 'acc

  val fold_defs : t -> init:'acc -> f:('acc -> Def.t -> 'acc) -> 'acc

  val find_def : t -> Def.Ord.t -> Def.t option

  val ctrl_of : t -> Ctrl.t

  val defs_of_var : t -> Il.var -> (Def.Ord.t * Def.t) list

  val first_def_of_var : t -> Il.var -> (Def.Ord.t * Def.t) option

  val last_def_of_var : t -> Il.var -> (Def.Ord.t * Def.t) option

  val last_def_of_var_before :
    t -> Il.var -> Def.Ord.t -> (Def.Ord.t * Def.t) option

  val map_defs : t -> f:(key:Def.Ord.t -> data:Def.t -> Def.t) -> unit

  val filter_map_defs :
    t -> f:(key:Def.Ord.t -> data:Def.t -> Def.t option) -> unit

  val append_def : t -> Def.t -> unit

  val set_def : t -> Def.Ord.t -> Def.t -> (unit, error) Result.t

  val is_delay : t -> bool
end

module Loc : sig
  type t = Def of Label.t * Def.Ord.t | Ctrl of Label.t
  [@@deriving equal, compare, hash, sexp]

  type comparator_witness

  val comparator : (t, comparator_witness) Comparator.t

  val to_string : t -> string

  val label_of : t -> Label.t
end

module Loc_map : module type of Map.Make (Loc)

module Loc_set : module type of Set.Make (Loc)

module Edge : sig
  type t = Direct | Indirect | Fallthrough | Return
  [@@deriving equal, compare, hash, sexp]

  val to_string : t -> string
end

(* since this CFG is mutable, it is possible for
 * it to go out of sync with the block table *)
module Cfg : module type of Unordered_graph.Make (Label) (Edge)

module Sub : sig
  type t

  val create : Label.t -> t

  val set_entry : t -> Label.t -> (unit, error) Result.t

  val set_entry_exn : t -> Label.t -> unit

  val entry_of : t -> Label.t

  val cfg_of : t -> Cfg.t

  val labels_of : t -> Label.t list

  val has_label : t -> Label.t -> bool

  val iter : t -> f:(Label.t -> unit) -> unit

  val fold : t -> init:'a -> f:('a -> Label.t -> 'a) -> 'a

  val is_exit : t -> Label.t -> bool

  val exits : t -> Label.t list
end

type subs =
  < subroutines: Sub.t list
  ; sub_of_block: Label.t -> Sub.t option
  ; sub_of_block_exn: Label.t -> Sub.t
  ; sub_of_entry: Label.t -> Sub.t option
  ; sub_of_entry_exn: Label.t -> Sub.t >

type t = private
  { cfg: Cfg.t
  ; blocks: (Label.t, Block.t) Hashtbl.t
  ; a2bs: (Addr.t, Label_set.t) Hashtbl.t
  ; mutable next_label: Label.t }

val create : unit -> t

val cfg_of : t -> Cfg.t

val block : t -> Label.t -> Block.t option

val block_exn : t -> Label.t -> Block.t

(* modify the control flow of a particular block *)
val map_ctrl_direct :
  t -> Label.t -> f:(Ctrl.t -> Ctrl.t) -> (unit, error) Result.t

val insert_edge_indirect :
  t -> src:Label.t -> dst:Label.t -> (unit, error) Result.t

val remove_edge_indirect :
  t -> src:Label.t -> dst:Label.t -> (unit, error) Result.t

val blocks_of : t -> Block.t list

val iter_blocks : t -> f:(Block.t -> unit) -> unit

val fold_blocks : t -> init:'a -> f:('a -> Block.t -> 'a) -> 'a

val has_block : t -> Label.t -> bool

val def_of : t -> Label.t -> Def.Ord.t -> Def.t option

val ctrl_of : t -> Label.t -> Ctrl.t option

val is_valid_loc : t -> Loc.t -> bool

val has_addr : t -> Addr.t -> bool

val blocks_of_addr : t -> Addr.t -> Block.t list

val iter_blocks_of_addr : t -> Addr.t -> f:(Block.t -> unit) -> unit

val addr_of_block : t -> Label.t -> Addr.t option

val addr_of_block_exn : t -> Label.t -> Addr.t

val defs_of_var : t -> Il.var -> (Label.t * (int * Def.t) list) list

(* remove labels from the CFG which have no associated block *)
val synchronize : t -> unit

val new_label : t -> Label.t

val lift :
     ?delay:Addr.t list
  -> t
  -> Il.t
  -> word_sz:int
  -> endaddr:Addr.t
  -> (unit, error) Result.t

val lift_exn :
  ?delay:Addr.t list -> t -> Il.t -> word_sz:int -> endaddr:Addr.t -> unit

val resolve_const_indirects : t -> bool

type invalidate_result =
  {invalidated: Addr.Set.t; returns: Label_set.t; indirects: Label_set.t}

val invalidate_block : t -> Label.t -> invalidate_result

val invalidate_addr : t -> Addr.t -> invalidate_result option

module Slice_graph : module type of Unordered_graph.Make (Label) (Unit)

type slice = {g: Slice_graph.t; defs: (Label.t, Def.Ord_set.t) Hashtbl.t}

val slice_backward :
     ?follow_calls:bool
  -> ?max_depth:int
  -> t
  -> Label.t
  -> Il.Var_set.t
  -> slice
