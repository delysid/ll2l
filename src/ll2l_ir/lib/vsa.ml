(* this is adapted from the CBAT project by Draper Laboratory *)

open Core_kernel
open Ll2l_std
module L = Ir.Label
module Lmap = Ir.Label_map
module S = Ir.Sub
module C = Ir.Ctrl
module B = Ir.Block

module Bitv_lattice = struct
  type t = Interval of Interval.t | Bottom [@@deriving equal, sexp]

  let top ~size = Interval (Interval.create_full ~size)

  let interval i = if Interval.is_empty i then Bottom else Interval i

  let interval_of = function
    | Interval i -> Some i
    | _ -> None

  let join l1 l2 =
    match (l1, l2) with
    | Interval i1, Interval i2 ->
        Interval.union i1 i2 ~range:Unsigned |> interval
    | Interval _, Bottom -> l1
    | Bottom, Interval _ -> l2
    | Bottom, Bottom -> Bottom

  let widen_join l1 l2 =
    if equal l1 l2 then l1
    else
      match (l1, l2) with
      | Bottom, Bottom -> Bottom
      | Interval i, Bottom -> top ~size:(Interval.size_of i)
      | _, Interval i -> top ~size:(Interval.size_of i)

  let meet l1 l2 =
    match (l1, l2) with
    | Interval i1, Interval i2 ->
        Interval.intersect i1 i2 ~range:Unsigned |> interval
    | Bottom, _ -> l1
    | _, Bottom -> l2

  let to_string = function
    | Interval i -> Interval.to_string i
    | Bottom -> "bottom"
end

module Bvl = Bitv_lattice

module Mem_lattice = struct
  module Env = struct
    type t = Bvl.t Addr.Map.t [@@deriving equal, sexp]

    let empty = Addr.Map.empty

    let put env ~endian ~addr ~bytes ~value =
      let reorder l =
        match endian with
        | `LE -> l
        | `BE -> List.rev l
      in
      let i =
        match value with
        | Bvl.Interval i -> i
        | Bvl.Bottom -> Interval.create_empty ~size:(bytes lsl 3)
      in
      List.init bytes ~f:(fun n ->
          let lo = n lsl 3 in
          let hi = lo + 7 in
          Interval.extract i ~hi ~lo |> Bvl.interval )
      |> reorder
      |> List.foldi ~init:env ~f:(fun i env data ->
             let key = Addr.(addr + int i) in
             Map.set env ~key ~data )

    let get env ~endian ~addr ~bytes =
      let rec aux i res =
        let open Option.Let_syntax in
        if i >= bytes then
          match endian with
          | `BE -> return (List.rev res)
          | `LE -> return res
        else
          let%bind v = Map.find env Addr.(addr + int i) in
          aux (succ i) (v :: res)
      in
      match aux 0 [] with
      | None -> Bvl.Bottom
      | Some [] -> Bvl.Bottom
      | Some (init :: rest) ->
          List.fold rest ~init ~f:(fun l l' ->
              match (l', l) with
              | Bvl.Bottom, Bvl.Bottom -> Bvl.Bottom
              | Bvl.Interval i, Bvl.Bottom ->
                  Interval.concat i Interval.(create_empty ~size:(size_of i))
                  |> Bvl.interval
              | Bvl.Bottom, Bvl.Interval i ->
                  Interval.concat Interval.(create_empty ~size:(size_of i)) i
                  |> Bvl.interval
              | Bvl.Interval i1, Bvl.Interval i2 ->
                  Interval.concat i1 i2 |> Bvl.interval )

    let join a b =
      Map.merge a b ~f:(fun ~key:_ vs ->
          match vs with
          | `Left _ -> None
          | `Right _ -> None
          | `Both (l1, l2) -> Some (Bvl.join l1 l2) )

    let widen_join a b =
      Map.merge a b ~f:(fun ~key:_ vs ->
          match vs with
          | `Left _ -> None
          | `Right _ -> None
          | `Both (l1, l2) -> Some (Bvl.widen_join l1 l2) )

    let meet a b =
      Map.merge a b ~f:(fun ~key:_ vs ->
          match vs with
          | `Left l -> Some l
          | `Right l -> Some l
          | `Both (l1, l2) -> Some (Bvl.meet l1 l2) )
  end

  type t = Mem of Env.t | Bottom [@@deriving equal, sexp]

  let of_concrete_env env =
    Mem
      ( Memory.Env.to_alist env
      |> List.fold ~init:Env.empty ~f:(fun m (key, value) ->
             let data =
               Interval.create_single ~value ~size:8 |> Bitv_lattice.interval
             in
             Map.set m ~key ~data ) )

  let top = Mem Env.empty

  let mem_of = function
    | Mem mem -> Some mem
    | _ -> None

  let join a b =
    match (a, b) with
    | Mem a, Mem b -> Mem (Env.join a b)
    | Mem _, Bottom -> a
    | Bottom, Mem _ -> b
    | Bottom, Bottom -> Bottom

  let widen_join a b =
    match (a, b) with
    | Mem a, Mem b -> Mem (Env.widen_join a b)
    | Mem _, Bottom -> a
    | Bottom, Mem _ -> b
    | Bottom, Bottom -> Bottom

  let meet a b =
    match (a, b) with
    | Mem a, Mem b -> Mem (Env.meet a b)
    | Bottom, _ -> Bottom
    | _, Bottom -> Bottom
end

module Meml = Mem_lattice

module Env = struct
  type t =
    | Env of {bitv: Bvl.t Il.Var_map.t; memory: Meml.t Il.Var_map.t}
    | Bottom
  [@@deriving equal, sexp]

  let top = Env {bitv= Il.Var_map.empty; memory= Il.Var_map.empty}

  let find_bitv env v =
    match env with
    | Env env -> Map.find env.bitv v
    | Bottom -> None

  let set_bitv env v l =
    match env with
    | Env env -> Env {env with bitv= Map.set env.bitv v l}
    | Bottom -> Bottom

  let find_mem env v =
    match env with
    | Env env -> Map.find env.memory v
    | Bottom -> None

  let set_mem env v l =
    match env with
    | Env env -> Env {env with memory= Map.set env.memory v l}
    | Bottom -> Bottom

  let join_bitv a b =
    Map.merge a b ~f:(fun ~key:_ vs ->
        match vs with
        | `Left _ -> None
        | `Right _ -> None
        | `Both (l1, l2) -> Some (Bvl.join l1 l2) )

  let widen_join_bitv a b =
    Map.merge a b ~f:(fun ~key:_ vs ->
        match vs with
        | `Left _ -> None
        | `Right _ -> None
        | `Both (l1, l2) -> Some (Bvl.widen_join l1 l2) )

  let meet_bitv a b =
    Map.merge a b ~f:(fun ~key:_ vs ->
        match vs with
        | `Left l -> Some l
        | `Right l -> Some l
        | `Both (l1, l2) -> Some (Bvl.meet l1 l2) )

  let join_mem a b =
    Map.merge a b ~f:(fun ~key:_ vs ->
        match vs with
        | `Left _ -> None
        | `Right _ -> None
        | `Both (l1, l2) -> Some (Meml.join l1 l2) )

  let widen_join_mem a b =
    Map.merge a b ~f:(fun ~key:_ vs ->
        match vs with
        | `Left _ -> None
        | `Right _ -> None
        | `Both (l1, l2) -> Some (Meml.widen_join l1 l2) )

  let meet_mem a b =
    Map.merge a b ~f:(fun ~key:_ vs ->
        match vs with
        | `Left l -> Some l
        | `Right l -> Some l
        | `Both (l1, l2) -> Some (Meml.meet l1 l2) )

  let join a b =
    match (a, b) with
    | Env env1, Env env2 ->
        Env
          { bitv= join_bitv env1.bitv env2.bitv
          ; memory= join_mem env1.memory env2.memory }
    | Env _, Bottom -> a
    | Bottom, Env _ -> b
    | Bottom, Bottom -> Bottom

  let widen_join a b =
    match (a, b) with
    | Env env1, Env env2 ->
        Env
          { bitv= widen_join_bitv env1.bitv env2.bitv
          ; memory= widen_join_mem env1.memory env2.memory }
    | Env _, Bottom -> a
    | Bottom, Env _ -> b
    | Bottom, Bottom -> Bottom

  let meet a b =
    match (a, b) with
    | Env env1, Env env2 ->
        Env
          { bitv= meet_bitv env1.bitv env2.bitv
          ; memory= meet_mem env1.memory env2.memory }
    | Bottom, _ -> Bottom
    | _, Bottom -> Bottom
end

let binop (b : Il.binop) i1 i2 =
  let size = Interval.size_of i1 in
  assert (size = Interval.size_of i2);
  match b with
  | ADD -> Interval.(i1 + i2) |> Bvl.interval
  | SUB -> Interval.(i1 - i2) |> Bvl.interval
  | MUL -> Interval.(i1 * i2) |> Bvl.interval
  | DIV -> Interval.(i1 / i2) |> Bvl.interval
  | MOD -> Interval.(i1 % i2) |> Bvl.interval
  | SDIV -> Interval.(i1 /$ i2) |> Bvl.interval
  | SMOD -> Interval.(i1 %^ i2) |> Bvl.interval
  | AND -> Interval.(i1 & i2) |> Bvl.interval
  | OR -> Interval.(i1 lor i2) |> Bvl.interval
  | XOR -> Interval.(i1 ^ i2) |> Bvl.interval
  | SHR -> Interval.(i1 >> i2) |> Bvl.interval
  | SAR -> Interval.(i1 >>> i2) |> Bvl.interval
  | SHL -> Interval.(i1 << i2) |> Bvl.interval
  | EQ ->
      if Interval.is_empty i1 || Interval.is_empty i2 then Bvl.Bottom
      else if size = 1 then Interval.(equal i1 i2 |> boolean) |> Bvl.interval
      else if Interval.(is_empty (intersect i1 i2 ~range:Unsigned)) then
        Interval.boolean_full |> Bvl.interval
      else Interval.boolean_false |> Bvl.interval
  | NEQ ->
      if Interval.is_empty i1 || Interval.is_empty i2 then Bvl.Bottom
      else if size = 1 then
        Interval.(equal i1 i2 |> not |> boolean) |> Bvl.interval
      else if Interval.(is_empty (intersect i1 i2 ~range:Unsigned)) then
        Interval.boolean_full |> Bvl.interval
      else Interval.boolean_true |> Bvl.interval
  | LT ->
      let max1 = Interval.unsigned_max i1 in
      let min1 = Interval.unsigned_min i1 in
      let max2 = Interval.unsigned_max i2 in
      let min2 = Interval.unsigned_min i2 in
      if Bitvec.(max1 < min2) then Interval.boolean_true |> Bvl.interval
      else if Bitvec.(min1 >= max2) then
        Interval.boolean_false |> Bvl.interval
      else Interval.boolean_full |> Bvl.interval
  | GT ->
      let max1 = Interval.unsigned_max i1 in
      let min1 = Interval.unsigned_min i1 in
      let max2 = Interval.unsigned_max i2 in
      let min2 = Interval.unsigned_min i2 in
      if Bitvec.(max1 > min2) then Interval.boolean_true |> Bvl.interval
      else if Bitvec.(min1 <= max2) then
        Interval.boolean_false |> Bvl.interval
      else Interval.boolean_full |> Bvl.interval
  | LEQ ->
      let max1 = Interval.unsigned_max i1 in
      let min1 = Interval.unsigned_min i1 in
      let max2 = Interval.unsigned_max i2 in
      let min2 = Interval.unsigned_min i2 in
      if Bitvec.(max1 <= min2) then Interval.boolean_true |> Bvl.interval
      else if Bitvec.(min1 > max2) then
        Interval.boolean_false |> Bvl.interval
      else Interval.boolean_full |> Bvl.interval
  | GEQ ->
      let max1 = Interval.unsigned_max i1 in
      let min1 = Interval.unsigned_min i1 in
      let max2 = Interval.unsigned_max i2 in
      let min2 = Interval.unsigned_min i2 in
      if Bitvec.(max1 >= min2) then Interval.boolean_true |> Bvl.interval
      else if Bitvec.(min1 < max2) then
        Interval.boolean_false |> Bvl.interval
      else Interval.boolean_full |> Bvl.interval
  | SLT ->
      let max1 = Interval.signed_max i1 in
      let min1 = Interval.signed_min i1 in
      let max2 = Interval.signed_max i2 in
      let min2 = Interval.signed_min i2 in
      let m = Bitvec.modulus size in
      if Bitvec.signed_compare max1 min2 m < 0 then
        Interval.boolean_true |> Bvl.interval
      else if Bitvec.signed_compare min1 max2 m >= 0 then
        Interval.boolean_false |> Bvl.interval
      else Interval.boolean_full |> Bvl.interval
  | SGT ->
      let max1 = Interval.signed_max i1 in
      let min1 = Interval.signed_min i1 in
      let max2 = Interval.signed_max i2 in
      let min2 = Interval.signed_min i2 in
      let m = Bitvec.modulus size in
      if Bitvec.signed_compare max1 min2 m > 0 then
        Interval.boolean_true |> Bvl.interval
      else if Bitvec.signed_compare min1 max2 m <= 0 then
        Interval.boolean_false |> Bvl.interval
      else Interval.boolean_full |> Bvl.interval
  | SLEQ ->
      let max1 = Interval.signed_max i1 in
      let min1 = Interval.signed_min i1 in
      let max2 = Interval.signed_max i2 in
      let min2 = Interval.signed_min i2 in
      let m = Bitvec.modulus size in
      if Bitvec.signed_compare max1 min2 m <= 0 then
        Interval.boolean_true |> Bvl.interval
      else if Bitvec.signed_compare min1 max2 m > 0 then
        Interval.boolean_false |> Bvl.interval
      else Interval.boolean_full |> Bvl.interval
  | SGEQ ->
      let max1 = Interval.signed_max i1 in
      let min1 = Interval.signed_min i1 in
      let max2 = Interval.signed_max i2 in
      let min2 = Interval.signed_min i2 in
      let m = Bitvec.modulus size in
      if Bitvec.signed_compare max1 min2 m >= 0 then
        Interval.boolean_true |> Bvl.interval
      else if Bitvec.signed_compare min1 max2 m < 0 then
        Interval.boolean_false |> Bvl.interval
      else Interval.boolean_full |> Bvl.interval
  | FLEQ | FLT | FEQ | FNEQ | FUNORD | FGT | FGEQ ->
      Interval.boolean_full |> Bvl.interval
  | _ -> Bvl.top ~size

let unop (u : Il.unop) i =
  let size = Interval.size_of i in
  match u with
  | NOT -> Interval.(~~i) |> Bvl.interval
  | NEG -> Interval.(~-i) |> Bvl.interval
  | _ -> Bvl.top ~size

let cast (k : Il.cast) s i =
  let size = Interval.size_of i in
  match k with
  | (ZEXT | SEXT) when s <= size -> Bvl.interval i
  | (LOW | HIGH) when s >= size -> Bvl.interval i
  | ZEXT -> Interval.zext i ~size:s |> Bvl.interval
  | SEXT -> Interval.sext i ~size:s |> Bvl.interval
  | LOW -> Interval.trunc i ~size:s |> Bvl.interval
  | HIGH ->
      let m = Bitvec.modulus size in
      let sh = size - s in
      let sh = Interval.create_single ~value:Bitvec.(int sh mod m) ~size in
      Interval.(trunc (i >> sh) ~size:s) |> Bvl.interval
  | _ -> Bvl.top ~size:s

let rec expr e env =
  match e with
  | Il.Var v -> (
    match v.hint with
    | Il.Var_hint.Mem ->
        `Mem (Option.value ~default:Meml.Bottom (Env.find_mem env v))
    | _ -> `Bitv (Option.value ~default:Bvl.Bottom (Env.find_bitv env v)) )
  | Il.Const {value; size} ->
      `Bitv (Interval.create_single ~value ~size |> Bvl.interval)
  | Il.(Load {endian; bytes; loc; mem}) -> (
    match (expr mem env, expr loc env) with
    | `Mem Meml.Bottom, `Bitv _ -> `Bitv Bvl.Bottom
    | `Mem _, `Bitv Bvl.Bottom -> `Bitv Bvl.Bottom
    | `Mem (Meml.Mem m), `Bitv (Bvl.Interval i) -> (
      match Interval.single_of i with
      | None ->
          (* this may seem unsound, since what if `loc` actually
           * falls outside of any mapped memory segment?
           * for now, let's go with the assumption that there is
           * indeed a value stored at `loc`, and it could be anything. *)
          `Bitv (Bvl.top ~size:(bytes lsl 3))
      | Some addr -> `Bitv (Meml.Env.get m ~endian ~addr ~bytes) )
    | _ -> assert false )
  | Il.(Store {r= {endian; bytes; loc; mem}; src}) -> (
    match (expr mem env, expr loc env, expr src env) with
    | `Mem Meml.Bottom, `Bitv _, `Bitv _ -> `Mem Meml.Bottom
    | `Mem _, `Bitv Bvl.Bottom, `Bitv _ -> `Mem Meml.Bottom
    | `Mem (Meml.Mem m), `Bitv (Bvl.Interval i), `Bitv value -> (
      match Interval.single_of i with
      | None -> `Mem Meml.top
      | Some addr ->
          `Mem (Meml.Mem (Meml.Env.put m ~endian ~addr ~bytes ~value)) )
    | _ -> assert false )
  | Il.Binop (b, lhs, rhs) -> (
    match (expr lhs env, expr rhs env) with
    | `Bitv (Bvl.Interval i1), `Bitv (Bvl.Interval i2) ->
        `Bitv (binop b i1 i2)
    | `Bitv (Bvl.Interval i), `Bitv Bvl.Bottom ->
        `Bitv (binop b i Interval.(create_empty ~size:(size_of i)))
    | `Bitv Bvl.Bottom, `Bitv (Bvl.Interval i) ->
        `Bitv (binop b Interval.(create_empty ~size:(size_of i)) i)
    | `Bitv Bvl.Bottom, `Bitv Bvl.Bottom -> `Bitv Bvl.Bottom
    | _ -> assert false )
  | Il.Unop (u, e) -> (
    match expr e env with
    | `Bitv (Bvl.Interval i) -> `Bitv (unop u i)
    | `Bitv Bvl.Bottom -> (
      match Il.size_of e with
      | None -> `Bitv Bvl.Bottom
      | Some size -> `Bitv (unop u Interval.(create_empty ~size)) )
    | _ -> assert false )
  | Il.Cast (k, s, e) -> (
    match expr e env with
    | `Bitv (Bvl.Interval i) -> `Bitv (cast k s i)
    | `Bitv Bvl.Bottom -> (
      match Il.size_of e with
      | None -> `Bitv Bvl.Bottom
      | Some size -> `Bitv (cast k s Interval.(create_empty ~size)) )
    | _ -> assert false )
  | Il.Extract (hi, lo, e) -> (
    match expr e env with
    | `Bitv (Bvl.Interval i) ->
        `Bitv (Interval.extract ~hi ~lo i |> Bvl.interval)
    | `Bitv Bvl.Bottom -> (
      match Il.size_of e with
      | None -> `Bitv Bvl.Bottom
      | Some size ->
          `Bitv
            (Interval.(extract ~hi ~lo (create_empty ~size)) |> Bvl.interval)
      )
    | _ -> assert false )
  | Il.Concat (lhs, rhs) -> (
    match (expr lhs env, expr rhs env) with
    | `Bitv (Bvl.Interval i1), `Bitv (Bvl.Interval i2) ->
        `Bitv (Interval.concat i1 i2 |> Bvl.interval)
    | `Bitv (Bvl.Interval i), `Bitv Bvl.Bottom ->
        `Bitv
          ( Interval.(concat i (create_empty ~size:(size_of i)))
          |> Bvl.interval )
    | `Bitv Bvl.Bottom, `Bitv (Bvl.Interval i) ->
        `Bitv
          ( Interval.(concat (create_empty ~size:(size_of i)) i)
          |> Bvl.interval )
    | `Bitv Bvl.Bottom, `Bitv Bvl.Bottom -> `Bitv Bvl.Bottom
    | _ -> assert false )
  | Il.Undefined size -> `Bitv (Bvl.top ~size)

let def ~dst ~src env =
  (* initialize vars we haven't seen yet *)
  let env =
    List.fold (Il.vars_of src) ~init:env ~f:(fun env v ->
        match v.size with
        | None -> (
          match v.hint with
          | Il.Var_hint.Mem -> (
            match Env.find_mem env v with
            | None -> Env.set_mem env v Meml.top
            | Some _ -> env )
          | _ -> env )
        | Some size -> (
          match Env.find_bitv env v with
          | None -> Env.set_bitv env v (Bvl.top ~size)
          | Some _ -> env ) )
  in
  (* TODO: handle memory environment *)
  match Il.(dst.hint) with
  | Il.Var_hint.Mem -> (
    match expr src env with
    | `Mem l -> Env.set_mem env dst l
    | _ -> assert false )
  | _ -> (
    match expr src env with
    | `Bitv l -> Env.set_bitv env dst l
    | _ -> assert false )

let defs blk =
  B.fold_defs blk
    ~init:(fun x -> x)
    ~f:(fun d (dst, src) env -> def ~dst ~src (d env))

let ctrl call blk env ~target =
  match B.ctrl_of blk with
  | C.Call (d, dr, _) -> (
    match dr with
    | None -> Env.Bottom
    | Some (C.Direct l) when not (L.equal l target) -> Env.Bottom
    | _ -> (
      match d with
      | C.Indirect _ -> Env.top
      | C.Direct sub -> call ~sub env ~target ) )
  | C.Jmp (d, _) -> (
    match d with
    | C.Indirect _ -> env
    | C.Direct l when L.equal l target -> env
    | C.Direct _ -> Env.Bottom )
  | C.JmpIf (c, dt, df) -> (
    match expr c env with
    | `Bitv Bvl.Bottom -> Env.Bottom
    | `Bitv (Bvl.Interval i) -> (
        let trans = function
          | C.Indirect _ -> env
          | C.Direct l -> if L.equal l target then env else Env.Bottom
        in
        (* TODO: collect constraints from comparison operators
         * and propagate them accordingly to the targets *)
        let t = Interval.contains_value i Bitvec.(bool true) in
        let f = Interval.contains_value i Bitvec.(bool false) in
        match (t, f) with
        | false, false -> Env.Bottom
        | true, false -> trans dt
        | false, true -> trans df
        | true, true -> Env.join (trans dt) (trans df) )
    | _ -> assert false )
  | C.Special (_, dr) -> Env.top

let block call ir ~source env =
  match Ir.block ir source with
  | None -> assert false
  | Some blk ->
      let postcond = defs blk env in
      ctrl call blk postcond

module Fixpoint = Ir.Cfg.Fixpoint

module Contextual_fixpoint = struct
  type ('n, 'd) t = Dep of ('n -> 'd) | Const of 'd

  let app = function
    | Dep f -> f
    | Const c -> fun _ -> c

  let fixpoint ?steps ?(rev = false) ?step ~init ~start ~equal ~merge ~f g =
    let step = Option.value ~default:(fun _ _ _ x -> x) step in
    let step i n d d' = Const (step i n (app d n) (app d' n)) in
    let default = Fixpoint.default init in
    let init =
      Fixpoint.derive init ~f:(fun _ d -> Some (Const d)) (Const default)
    in
    let equal d1 d2 =
      match (d1, d2) with
      | Const c1, Const c2 -> equal c1 c2
      | Dep _, _ | _, Dep _ -> false
    in
    let merge d1 d2 =
      Dep (fun target -> merge (app d1 target) (app d2 target))
    in
    let f n d = Dep (fun target -> f ~source:n (app d n) ~target) in
    Fixpoint.derive
      ~f:(fun n d -> Some (app d n))
      (Ir.Cfg.fixpoint ?steps ~rev ~step ~init ~start ~equal ~merge ~f g)
      default
end

type t = (L.t, Env.t) Fixpoint.t

let create ~entry ~start =
  let base_map = Lmap.singleton start entry in
  Fixpoint.create base_map Env.Bottom

let call_stack_max_size = 1

let back_edge_max_steps = 10

let vsa ?steps ?rev ?(follow_calls = false) init ir ~subs ~start =
  let widening_points = Hash_set.create (module L) in
  subs#subroutines
  |> List.iter ~f:(fun sub ->
         if follow_calls || S.has_label sub start then
           let entry = S.entry_of sub in
           let cfg = S.cfg_of sub in
           Ir.Cfg.iter_dfs cfg entry ~f:(fun _ l' k ->
               match k with
               | `Back -> Hash_set.add widening_points l'
               | _ -> () ) );
  let rec run stack init ~start =
    (* XXX: if we follow calls, the running time of our
     * algorithm explodes, so find a way to improve this *)
    let rec call stack ~sub env ~target =
      (* a call can have unpredictable side-effects, so we
       * return an over-approximation to maintain soundness
       * if we're not actually following calls *)
      if not follow_calls then Env.top
      else if
        List.mem stack start ~equal:L.equal
        && List.length stack > call_stack_max_size
      then Env.top
      else
        let stack = sub :: stack in
        let sol = run stack (create ~entry:env ~start:sub) ~start:sub in
        match subs#sub_of_entry sub with
        | None -> assert false
        | Some sub' ->
            S.fold sub' ~init:Env.Bottom ~f:(fun acc source ->
                let pre = Fixpoint.get sol source in
                block (call stack) ir ~source pre ~target |> Env.join acc )
    in
    let sub =
      match subs#sub_of_entry start with
      | None -> assert false
      | Some sub -> sub
    in
    let cfg = S.cfg_of sub in
    Contextual_fixpoint.fixpoint ?steps ?rev ~init ~start ~equal:Env.equal
      ~merge:Env.join
      ~f:(block (call stack) ir)
      cfg
      ~step:(fun i n ->
        if i > back_edge_max_steps && Hash_set.mem widening_points n then
          Env.widen_join
        else fun _ x' -> x' )
  in
  run [] init ~start
