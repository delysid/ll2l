open Core_kernel
open Ll2l_std

type t =
  { var_env: Il.var_env
  ; mem_env: Il.mem_env
  ; loads: Addr.Set.t
  ; stores: Addr.Set.t }

let create ?(var_env = Il.Var_map.empty) ?(mem_env = Il.Var_map.empty) () =
  {var_env; mem_env; loads= Addr.Set.empty; stores= Addr.Set.empty}

(* result of evaluating an expression *)
type eval =
  {e: Il.expr; mu: Memory.Env.t option; loads: Addr.Set.t; stores: Addr.Set.t}

let rec eval_expr t e =
  let make ?(mu = None) ?(loads = Addr.Set.empty) ?(stores = Addr.Set.empty)
      e =
    {e; mu; loads; stores}
  in
  let mark m a n =
    List.init n ~f:(fun i -> Addr.(a + int i))
    |> List.fold ~init:m ~f:Set.add
  in
  match e with
  | Il.Const _ -> make e
  | Il.Var v ->
      let e' =
        match Map.find t.var_env v with
        | None -> e
        | Some c -> Il.Const c
      in
      make e' ~mu:(Map.find t.mem_env v)
  | Il.Load r -> (
      let mem = eval_expr t r.mem in
      let loc = eval_expr t r.loc in
      let loads = Addr.Set.union mem.loads loc.loads in
      let stores = Addr.Set.union mem.stores loc.stores in
      let default =
        make (Il.Load {r with loc= loc.e; mem= mem.e}) ~loads ~stores
      in
      match loc.e with
      | Const c ->
          let addr = c.value in
          let bytes = r.bytes in
          let size = bytes lsl 3 in
          let mu = mem.mu in
          Option.(
            value ~default
              ( mu
              >>= fun store ->
              Memory.Env.get store ~endian:r.endian ~addr ~bytes
              >>| fun value ->
              { default with
                e= Il.Const {value; size}
              ; loads= mark loads addr bytes } ))
      | _ -> default )
  | Il.Store s ->
      let mem = eval_expr t s.r.mem in
      let loc = eval_expr t s.r.loc in
      let src = eval_expr t s.src in
      let loads = Addr.Set.union_list [mem.loads; loc.loads; src.loads] in
      let stores =
        Addr.Set.union_list [mem.stores; loc.stores; src.stores]
      in
      let r = {s.r with loc= loc.e; mem= mem.e} in
      let mu, stores =
        match mem.mu with
        | None -> (None, stores)
        | Some mu' as mu -> (
          match (loc.e, src.e) with
          | Const c1, Const c2 ->
              let addr = c1.value in
              let bytes = s.r.bytes in
              ( Some
                  (Memory.Env.put mu' ~endian:s.r.endian ~addr ~bytes
                     ~value:c2.value)
              , mark stores addr bytes )
          | _ -> (mu, stores) )
      in
      make (Il.Store {r; src= src.e}) ~mu ~loads ~stores
  | Il.Binop (b, lhs, rhs) -> (
      let lhs = eval_expr t lhs in
      let rhs = eval_expr t rhs in
      let loads = Addr.Set.union lhs.loads rhs.loads in
      let stores = Addr.Set.union lhs.stores rhs.stores in
      let default = make (Il.Binop (b, lhs.e, rhs.e)) ~loads ~stores in
      match (lhs.e, rhs.e) with
      | Const c1, Const c2 ->
          let sz = max c1.size c2.size in
          let m = Bitvec.modulus sz in
          let module B = Bitvec.Make (struct
            let modulus = m
          end) in
          let res =
            let make value size = Some (Il.Const {value; size}) in
            match b with
            | ADD -> make B.(c1.value + c2.value) sz
            | SUB -> make B.(c1.value - c2.value) sz
            | MUL -> make B.(c1.value * c2.value) sz
            | DIV -> (
              try make B.(c1.value / c2.value) sz
              with Division_by_zero -> Some (Il.Undefined sz) )
            | MOD -> (
              try make B.(c1.value % c2.value) sz
              with Division_by_zero -> Some (Il.Undefined sz) )
            | SDIV -> (
              try make B.(c1.value /$ c2.value) sz
              with Division_by_zero -> Some (Il.Undefined sz) )
            | SMOD -> (
              try make B.(c1.value %^ c2.value) sz
              with Division_by_zero -> Some (Il.Undefined sz) )
            | AND -> make B.(c1.value land c2.value) sz
            | OR -> make B.(c1.value lor c2.value) sz
            | XOR -> make B.(c1.value lxor c2.value) sz
            | SHR -> make B.(c1.value lsr c2.value) sz
            | SAR -> make B.(c1.value asr c2.value) sz
            | SHL -> make B.(c1.value lsl c2.value) sz
            | LEQ -> make B.(bool (c1.value <= c2.value)) 1
            | LT -> make B.(bool (c1.value < c2.value)) 1
            | SLEQ ->
                let cmp = Bitvec.signed_compare c1.value c2.value m in
                make (B.bool (cmp <= 0)) 1
            | SLT ->
                let cmp = Bitvec.signed_compare c1.value c2.value m in
                make (B.bool (cmp < 0)) 1
            | EQ -> make B.(bool (c1.value = c2.value)) 1
            | NEQ -> make B.(bool (c1.value <> c2.value)) 1
            | GT -> make B.(bool (c1.value > c2.value)) 1
            | GEQ -> make B.(bool (c1.value >= c2.value)) 1
            | SGT ->
                let cmp = Bitvec.signed_compare c1.value c2.value m in
                make (B.bool (cmp > 0)) 1
            | SGEQ ->
                let cmp = Bitvec.signed_compare c1.value c2.value m in
                make (B.bool (cmp >= 0)) 1
            | _ -> None
          in
          Option.(value ~default (res >>| fun e -> {default with e}))
      | _ -> default )
  | Il.Unop (u, e') -> (
      let e' = eval_expr t e' in
      let default = {e' with e= Il.Unop (u, e'.e); mu= None} in
      match e'.e with
      | Const c ->
          let module B = Bitvec.Make (struct
            let modulus = Bitvec.modulus c.size
          end) in
          let v = c.value in
          let res =
            match u with
            | NOT -> Some (B.(~~v), c.size)
            | NEG -> Some (B.(~-v), c.size)
            | _ -> None
          in
          Option.(
            value ~default
              ( res
              >>| fun (value, size) ->
              {default with e= Il.Const {value; size}} ))
      | _ -> default )
  | Il.Cast (k, s, e') -> (
      let e' = eval_expr t e' in
      let default = {e' with e= Il.Cast (k, s, e'.e); mu= None} in
      match e'.e with
      | Const c ->
          let module B = Bitvec.Make (struct
            let modulus = Bitvec.modulus s
          end) in
          let res =
            match k with
            | (ZEXT | SEXT) when s <= c.size -> Some (c.value, c.size)
            | ZEXT -> Some (c.value, s)
            | SEXT ->
                let sh = c.size - 1 in
                let sh = B.int sh in
                let mask = B.(one lsl sh) in
                let v = c.value in
                let v = B.(v lxor mask) in
                let v = B.(v - mask) in
                Some (v, s)
            | (HIGH | LOW) when s >= c.size -> Some (c.value, c.size)
            | HIGH ->
                let hi = c.size - 1 in
                let lo = c.size - s in
                let v = Bitvec.extract ~hi ~lo c.value in
                Some (v, s)
            | LOW ->
                let hi = s - 1 in
                let lo = 0 in
                let v = Bitvec.extract ~hi ~lo c.value in
                Some (v, s)
            | _ -> None
          in
          Option.(
            value ~default
              ( res
              >>| fun (value, size) ->
              {default with e= Il.Const {value; size}} ))
      | _ -> default )
  | Il.Extract (hi, lo, e') -> (
      let e' = eval_expr t e' in
      let default = {e' with e= Il.Extract (hi, lo, e'.e); mu= None} in
      match e'.e with
      | Const c ->
          let size = hi - lo + 1 in
          let e =
            if size >= c.size then Il.Const c
            else Il.Const {value= Bitvec.extract ~hi ~lo c.value; size}
          in
          {default with e}
      | e' -> default )
  | Il.Concat (lhs, rhs) -> (
      let lhs = eval_expr t lhs in
      let rhs = eval_expr t rhs in
      let loads = Addr.Set.union lhs.loads rhs.loads in
      let stores = Addr.Set.union lhs.stores rhs.stores in
      let default = make (Il.Concat (lhs.e, rhs.e)) ~loads ~stores in
      match (lhs.e, rhs.e) with
      | Const c1, Const c2 ->
          let size = c1.size + c2.size in
          let value = Bitvec.append c1.size c2.size c1.value c2.value in
          {default with e= Il.Const {value; size}}
      | _ -> default )
  | Il.Undefined _ -> make e

let eval_def t ~dst ~src =
  (* initialize stores with mem vars we haven't seen yet *)
  let mem_env =
    List.filter (dst :: Il.vars_of src) ~f:(fun v ->
        Il.Var_hint.(equal v.hint Mem))
    |> List.dedup_and_sort ~compare:Il.compare_var
    |> List.fold ~init:t.mem_env ~f:(fun mu v ->
           match Map.find mu v with
           | None -> Map.set mu v Memory.Env.empty
           | Some _ -> mu)
  in
  (* simplify given the known values *)
  let {e; mu; loads; stores} = eval_expr t src in
  let loads = Addr.Set.union t.loads loads in
  let stores = Addr.Set.union t.stores stores in
  let e = Il.fold_constants e in
  (* update constants *)
  let var_env =
    match e with
    | Il.Const c -> Map.set t.var_env dst c
    | _ -> Map.remove t.var_env dst
  in
  (* update stores *)
  let mem_env =
    match mu with
    | Some mu when Il.Var_hint.(equal dst.hint Mem) ->
        Map.set t.mem_env dst mu
    | _ -> mem_env
  in
  {var_env; mem_env; loads; stores}
