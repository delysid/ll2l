open Core_kernel
open Ll2l_std

type error = [`BadBlock | `BadLabel | `BadDef | `BadCtrl]

let string_of_error = function
  | `BadBlock -> "bad block"
  | `BadLabel -> "bad label"
  | `BadDef -> "bad definition"
  | `BadCtrl -> "bad control statement"

exception Ir_exception of error

module Label = struct
  include Int

  let to_string l = Printf.sprintf "L%d" l [@@inline]
end

module Label_set = Set.Make (Label)
module Label_map = Map.Make (Label)

module Def = struct
  module Ord = struct
    include Int

    (* XXX: is it likely that there will be more
     * than 65535 defs in a single block? *)
    let to_string i = Printf.sprintf "%04X" i
  end

  module Ord_set = Set.Make (Ord)
  module Ord_map = Map.Make (Ord)

  type t = Il.var * Il.expr [@@deriving equal]

  let to_string (v, e) =
    Printf.sprintf "%s := %s"
      (Il.string_of_expr (Il.Var v))
      (Il.string_of_expr e)
    [@@inline]
end

module Ctrl = struct
  type dest = Direct of Label.t | Indirect of Il.expr [@@deriving equal]

  let string_of_dest = function
    | Direct l -> Label.to_string l
    | Indirect e -> Il.string_of_expr e
    [@@inline]

  type switch = string option option [@@deriving equal]

  type t =
    | Call of dest * dest option * switch
    | Jmp of dest * switch
    | JmpIf of Il.expr * dest * dest
    | Special of Il.special * dest option
  [@@deriving equal]

  let to_string = function
    | Call (d, r, s) ->
        let s_str =
          match s with
          | Some (Some s) -> Printf.sprintf ", switch %s" s
          | Some None -> ", switch"
          | None -> ""
        in
        let r_str =
          match r with
          | Some d -> Printf.sprintf ", ret %s" (string_of_dest d)
          | None -> ""
        in
        Printf.sprintf "call %s%s%s" (string_of_dest d) s_str r_str
    | Jmp (d, s) ->
        let s_str =
          match s with
          | Some (Some s) -> Printf.sprintf ", switch %s" s
          | Some None -> ", switch"
          | None -> ""
        in
        Printf.sprintf "jmp %s%s" (string_of_dest d) s_str
    | JmpIf (e, dt, df) ->
        Printf.sprintf "jmp (%s, %s, %s)" (Il.string_of_expr e)
          (string_of_dest dt) (string_of_dest df)
    | Special (s, d) ->
        let d_str =
          match d with
          | Some d -> Printf.sprintf " ret %s" (string_of_dest d)
          | None -> ""
        in
        Il.string_of_special s ^ d_str
end

module Block = struct
  type t =
    { label: Label.t
    ; addr: Addr.t
    ; mutable defs: Def.t Def.Ord.Map.t
    ; mutable ctrl: Ctrl.t
    ; delay: bool }

  let label_of blk = blk.label [@@inline]

  let addr_of blk = blk.addr [@@inline]

  let alist_of_defs blk = Map.to_alist blk.defs [@@inline]

  let iter_defs blk ~f = Map.iter blk.defs ~f [@@inline]

  let iteri_defs blk ~f = Map.iteri blk.defs ~f [@@inline]

  let foldi_defs blk ~init ~f =
    Map.fold blk.defs ~init ~f:(fun ~key ~data acc -> f acc key data)
    [@@inline]

  let fold_defs blk ~init ~f =
    foldi_defs blk ~init ~f:(fun acc _ def -> f acc def)
    [@@inline]

  let find_def blk l = Map.find blk.defs l [@@inline]

  let ctrl_of blk = blk.ctrl [@@inline]

  let defs_of_var blk v =
    Map.to_sequence blk.defs
    |> Sequence.filter ~f:(fun (_, (v', _)) -> Il.equal_var v v')
    |> Sequence.to_list

  let first_def_of_var blk v =
    Map.filter blk.defs ~f:(fun (v', _) -> Il.equal_var v v') |> Map.min_elt
    [@@inline]

  let last_def_of_var blk v =
    Map.filter blk.defs ~f:(fun (v', _) -> Il.equal_var v v') |> Map.max_elt
    [@@inline]

  let last_def_of_var_before blk v i =
    let defs = Map.filter blk.defs ~f:(fun (v', _) -> Il.equal_var v v') in
    Map.closest_key defs `Less_than i
    [@@inline]

  let map_defs blk ~f = blk.defs <- Map.mapi blk.defs ~f [@@inline]

  let filter_map_defs blk ~f =
    blk.defs <- Map.filter_mapi blk.defs ~f
    [@@inline]

  let append_def blk def =
    let key =
      match Map.max_elt blk.defs with
      | None -> 0
      | Some (i, _) -> succ i
    in
    blk.defs <- Map.set blk.defs ~key ~data:def

  let set_def blk i def =
    if Map.mem blk.defs i then Ok (blk.defs <- Map.set blk.defs i def)
    else Error `BadDef
    [@@inline]

  let is_delay blk = blk.delay [@@inline]
end

module Loc = struct
  module T = struct
    type t = Def of Label.t * Def.Ord.t | Ctrl of Label.t
    [@@deriving equal, compare, hash, sexp]
  end

  include T
  include Comparable.Make (T)

  let to_string = function
    | Def (l, i) ->
        Printf.sprintf "def(%s, %s)" (Label.to_string l)
          (Def.Ord.to_string i)
    | Ctrl l -> Printf.sprintf "ctrl(%s)" (Label.to_string l)

  let label_of = function
    | Def (l, _) -> l
    | Ctrl l -> l
end

module Loc_map = Map.Make (Loc)
module Loc_set = Set.Make (Loc)

module Edge = struct
  type t = Direct | Indirect | Fallthrough | Return
  [@@deriving equal, compare, hash, sexp]

  let to_string = function
    | Direct -> "direct"
    | Indirect -> "indirect"
    | Fallthrough -> "fallthrough"
    | Return -> "return"
end

module Cfg = Unordered_graph.Make (Label) (Edge)

module Sub = struct
  type t = {mutable entry: Label.t; cfg: Cfg.t}

  let create entry =
    let cfg = Cfg.create () in
    Cfg.insert_vertex cfg entry;
    {entry; cfg}

  let set_entry s l =
    if Cfg.has_vertex s.cfg l && not (Cfg.has_predecessors s.cfg l) then
      Ok (s.entry <- l)
    else Error `BadLabel

  let set_entry_exn s l =
    match set_entry s l with
    | Ok () -> ()
    | Error err -> raise (Ir_exception err)

  let entry_of s = s.entry [@@inline]

  let cfg_of s = s.cfg [@@inline]

  let labels_of s = Cfg.vertices s.cfg [@@inline]

  let has_label s = Cfg.has_vertex s.cfg [@@inline]

  let iter s ~f = Cfg.iter_vertices s.cfg ~f [@@inline]

  let fold s ~init ~f = Cfg.fold_vertices s.cfg ~init ~f [@@inline]

  let is_exit s l =
    if Cfg.has_vertex s.cfg l then Cfg.has_successors s.cfg l |> not
    else false

  let exits s =
    fold s ~init:[] ~f:(fun acc l ->
        if Cfg.has_successors s.cfg l then acc else l :: acc )
end

type subs =
  < subroutines: Sub.t list
  ; sub_of_block: Label.t -> Sub.t option
  ; sub_of_block_exn: Label.t -> Sub.t
  ; sub_of_entry: Label.t -> Sub.t option
  ; sub_of_entry_exn: Label.t -> Sub.t >

type t =
  { cfg: Cfg.t
  ; blocks: (Label.t, Block.t) Hashtbl.t
  ; a2bs: (Addr.t, Label_set.t) Hashtbl.t
  ; mutable next_label: Label.t }

let create () =
  { cfg= Cfg.create ()
  ; blocks= Hashtbl.create (module Label)
  ; a2bs= Hashtbl.create (module Addr)
  ; next_label= Label.zero }
  [@@inline]

let cfg_of ir = ir.cfg [@@inline]

let block ir l = Hashtbl.find ir.blocks l [@@inline]

let block_exn ir l = Hashtbl.find_exn ir.blocks l [@@inline]

let has_block ir l = Hashtbl.mem ir.blocks l [@@inline]

let def_of ir l i =
  let open Option.Let_syntax in
  let%bind blk = block ir l in
  Block.find_def blk i

let ctrl_of ir l =
  let open Option.Let_syntax in
  let%map blk = block ir l in
  blk.ctrl

let is_valid_loc ir = function
  | Loc.Def (l, i) -> (
    match block ir l with
    | None -> false
    | Some blk -> Map.mem blk.defs i )
  | Loc.Ctrl l -> has_block ir l

let map_ctrl_direct ir l ~f =
  match block ir l with
  | None -> Error `BadLabel
  | Some blk ->
      let open Result.Let_syntax in
      let ctrl' = f blk.ctrl in
      let removed = Vector.create () in
      let inserted = Vector.create () in
      let remove d e =
        match d with
        | Ctrl.Direct l' -> Vector.push_back removed (l, e, l')
        | _ -> ()
      in
      ( match blk.ctrl with
      | Ctrl.Call (d, dr, _) ->
          remove d Edge.Direct;
          Option.iter dr ~f:(fun dr -> remove dr Edge.Return)
      | Ctrl.Jmp (d, _) -> remove d Edge.Direct
      | Ctrl.JmpIf (_, dt, df) ->
          remove dt Edge.Direct;
          remove df Edge.Fallthrough
      | Ctrl.Special (_, dr) ->
          Option.iter dr ~f:(fun dr -> remove dr Edge.Return) );
      let insert d e =
        match d with
        | Ctrl.Direct l' ->
            if has_block ir l' then
              return (Vector.push_back inserted (l, e, l'))
            else Error `BadCtrl
        | _ -> return ()
      in
      let%map _ =
        match ctrl' with
        | Ctrl.Call (d, dr, _) -> (
            let%bind _ = insert d Edge.Direct in
            match dr with
            | None -> return ()
            | Some dr -> insert dr Edge.Return )
        | Ctrl.Jmp (d, _) -> insert d Edge.Direct
        | Ctrl.JmpIf (_, dt, df) ->
            let%bind _ = insert dt Edge.Direct in
            insert df Edge.Fallthrough
        | Ctrl.Special (_, dr) -> (
          match dr with
          | None -> return ()
          | Some dr -> insert dr Edge.Return )
      in
      Vector.iter removed ~f:(fun (l, e, l') ->
          Cfg.remove_edge ir.cfg l e l' );
      Vector.iter inserted ~f:(fun (l, e, l') ->
          Cfg.insert_edge ir.cfg l e l' );
      blk.ctrl <- ctrl'

let verify_indirect ir ~src ~dst =
  match (block ir src, has_block ir dst) with
  | Some blk, true -> (
      let verify = function
        | Ctrl.Indirect _ -> Ok ()
        | _ -> Error `BadCtrl
      in
      match blk.ctrl with
      | Ctrl.Call (d, _, _) | Ctrl.Jmp (d, _) -> verify d
      (* JmpIf and Special edges cannot be indirect *)
      | _ -> Error `BadCtrl )
  | _ -> Error `BadLabel

let insert_edge_indirect ir ~src ~dst =
  let open Result.Let_syntax in
  let%map _ = verify_indirect ir ~src ~dst in
  Cfg.insert_edge ir.cfg src Edge.Indirect dst

let remove_edge_indirect ir ~src ~dst =
  let open Result.Let_syntax in
  let%map _ = verify_indirect ir ~src ~dst in
  Cfg.remove_edge ir.cfg src Edge.Indirect dst

let blocks_of ir =
  Hashtbl.to_alist ir.blocks
  |> List.sort ~compare:(fun (l, _) (l', _) -> Label.compare l l')
  |> List.map ~f:snd

let iter_blocks ir ~f = Hashtbl.iter ir.blocks ~f [@@inline]

let fold_blocks ir ~init ~f =
  Hashtbl.fold ir.blocks ~init ~f:(fun ~key:_ ~data acc -> f acc data)
  [@@inline]

let has_addr ir a =
  match Hashtbl.find ir.a2bs a with
  | None -> false
  | Some bs -> not @@ Set.is_empty bs

let blocks_of_addr ir a =
  match Hashtbl.find ir.a2bs a with
  | None -> []
  | Some s ->
      Set.fold s ~init:[] ~f:(fun blocks l ->
          match block ir l with
          | Some blk -> blk :: blocks
          | None -> blocks )
      |> List.rev

let iter_blocks_of_addr ir a ~f =
  match Hashtbl.find ir.a2bs a with
  | None -> ()
  | Some s ->
      Set.iter s ~f:(fun l ->
          match block ir l with
          | None -> ()
          | Some blk -> f blk )

let addr_of_block ir l =
  let open Option.Let_syntax in
  let%map blk = block ir l in
  blk.addr

let addr_of_block_exn ir l =
  let blk = block_exn ir l in
  blk.addr

let defs_of_var ir v =
  List.filter_map (blocks_of ir) ~f:(fun blk ->
      match Block.defs_of_var blk v with
      | [] -> None
      | defs -> Some (blk.label, defs) )

let synchronize ir =
  let f = has_block ir in
  Cfg.filter_vertices ir.cfg ~f

let verify_il il =
  let open Result.Let_syntax in
  let rec aux = function
    | [] -> Ok ()
    | blk :: rest ->
        let%bind m =
          match blk with
          | Il.Label _ :: rest when not (List.is_empty rest) ->
              Result.ok_if_true ~error:`BadBlock
                (List.last_exn rest |> Il.is_terminator)
          | _ -> Error `BadBlock
        in
        aux rest
  in
  aux il

let new_label ir =
  let l = ir.next_label in
  ir.next_label <- Label.succ l;
  l

let lift_def ir dst src =
  match dst with
  | Il.Var v -> Ok (v, src)
  | _ -> Error `BadDef

let assoc_label ctx l =
  List.Assoc.find ctx l ~equal:Il.equal_label
  [@@inline]

let lift_ctrl ir ctx stmt l endaddr word_sz =
  match stmt with
  | Il.Goto l' -> (
    match assoc_label ctx l' with
    | Some l' ->
        Cfg.insert_edge ir.cfg l Edge.Direct l';
        Ok Ctrl.(Jmp (Direct l', None))
    | None -> Error `BadLabel )
  | Il.GotoIf (e, tl, fl) -> (
    match (assoc_label ctx tl, assoc_label ctx fl) with
    | Some tl', Some fl' ->
        Cfg.insert_edge ir.cfg l Edge.Direct tl';
        Cfg.insert_edge ir.cfg l Edge.Fallthrough fl';
        Ok Ctrl.(JmpIf (e, Direct tl', Direct fl'))
    | _ -> Error `BadLabel )
  | Il.Jmp (e, h) -> (
    match h with
    | Il.Jmp_hint.Jmp -> Ok Ctrl.(Jmp (Indirect e, None))
    | Il.Jmp_hint.Jmp_switch s -> Ok Ctrl.(Jmp (Indirect e, Some s))
    | Il.Jmp_hint.Call ->
        let d = Ctrl.Indirect e in
        let r = Some Ctrl.(Indirect Il.(bitv endaddr word_sz)) in
        Ok Ctrl.(Call (d, r, None))
    | Il.Jmp_hint.Call_switch s ->
        let d = Ctrl.Indirect e in
        let r = Some Ctrl.(Indirect Il.(bitv endaddr word_sz)) in
        Ok Ctrl.(Call (d, r, Some s)) )
  | Il.JmpIf (e, te, fe) -> Ok Ctrl.(JmpIf (e, Indirect te, Indirect fe))
  | Il.Special s ->
      let ret =
        match s with
        | Il.Wait | Il.Interrupt _ | Il.Syscall ->
            Some (Ctrl.Indirect Il.(bitv endaddr word_sz))
        | _ -> None
      in
      Ok Ctrl.(Special (s, ret))
  | _ -> Error `BadCtrl

let lift ?(delay = []) ir il ~word_sz ~endaddr =
  let open Result.Let_syntax in
  let%bind addr' = verify_il il in
  let ctx =
    List.fold il ~init:[] ~f:(fun ctx blk ->
        match blk with
        | Il.Label l :: _ -> (l, new_label ir) :: ctx
        (* unreachable if verify_il is correct! *)
        | _ -> assert false )
  in
  let rec lift_blk defs l stmts =
    match stmts with
    | Il.Assign (dst, src) :: rest ->
        let%bind def = lift_def ir dst src in
        lift_blk (def :: defs) l rest
    | [ctrl] ->
        let%bind ctrl = lift_ctrl ir ctx ctrl l endaddr word_sz in
        Ok (List.rev defs, ctrl)
    | Il.Unknown :: rest -> lift_blk defs l rest
    | _ -> Error `BadBlock
  in
  let rec lift_il blocks = function
    | [] -> Ok blocks
    | blk :: rest -> (
      match blk with
      | Il.Label l :: rest' -> (
        match assoc_label ctx l with
        | None -> Error `BadLabel
        | Some l' ->
            let%bind defs, ctrl = lift_blk [] l' rest' in
            let defs = List.mapi defs ~f:(fun i def -> (i, def)) in
            let defs = Int.Map.of_alist_exn defs in
            let addr = fst l in
            let delay = List.mem delay addr ~equal:Addr.equal in
            let blk = Block.{label= l'; addr; defs; ctrl; delay} in
            lift_il (blk :: blocks) rest )
      | _ -> Error `BadBlock )
  in
  let%map blocks = lift_il [] il in
  List.iter blocks ~f:(fun blk ->
      Hashtbl.set ir.blocks blk.label blk;
      match Hashtbl.find ir.a2bs blk.addr with
      | None -> Hashtbl.set ir.a2bs blk.addr Label_set.(singleton blk.label)
      | Some s -> Hashtbl.set ir.a2bs blk.addr (Set.add s blk.label) )

let lift_exn ?(delay = []) ir il ~word_sz ~endaddr =
  match lift ir il ~word_sz ~endaddr ~delay with
  | Ok () -> ()
  | Error err -> raise (Ir_exception err)

let resolve_const_indirects ir =
  let changed = ref false in
  let resolve_dest l d e =
    match d with
    | Ctrl.Direct _ -> d
    | Ctrl.Indirect exp -> (
      match exp with
      | Il.Const c -> (
          let addr = c.value in
          match Hashtbl.find ir.a2bs addr with
          | None -> d
          | Some s -> (
              (* IMPORTANT: we assume that the entry block
               * for this address has the smallest label
               * and is not a delay slot *)
              let f l' =
                match block ir l' with
                | None -> false
                | Some blk -> not blk.delay
              in
              match List.find (Set.to_list s) ~f with
              | None -> d
              | Some l' ->
                  Cfg.insert_edge ir.cfg l e l';
                  changed := true;
                  Ctrl.Direct l' ) )
      | _ -> d )
  in
  Hashtbl.map_inplace ir.blocks ~f:(fun blk ->
      let resolve = resolve_dest blk.label in
      let ctrl =
        match blk.ctrl with
        | Ctrl.Call (d, dr, s) ->
            let d' = resolve d Edge.Direct in
            let dr' =
              match dr with
              | Some dr -> Some (resolve dr Edge.Return)
              | None -> dr
            in
            Ctrl.Call (d', dr', s)
        | Ctrl.Jmp (d, s) -> Ctrl.Jmp (resolve d Edge.Direct, s)
        | Ctrl.JmpIf (e, dt, df) ->
            let dt' = resolve dt Edge.Direct in
            let df' = resolve df Edge.Fallthrough in
            Ctrl.JmpIf (e, dt', df')
        | Ctrl.Special (s, d) ->
            let d' =
              match d with
              | Some d -> Some (resolve d Edge.Return)
              | None -> d
            in
            Ctrl.Special (s, d')
      in
      {blk with ctrl} );
  !changed

type invalidate_result =
  {invalidated: Addr.Set.t; returns: Label_set.t; indirects: Label_set.t}

let new_invalidate invalidated returns indirects =
  {invalidated; returns; indirects}
  [@@inline]

let empty_invalidate () =
  { invalidated= Addr.Set.empty
  ; returns= Label_set.empty
  ; indirects= Label_set.empty }
  [@@inline]

let union_invalidate res res' =
  { invalidated= Addr.Set.union res.invalidated res'.invalidated
  ; returns= Label_set.union res.returns res'.returns
  ; indirects= Label_set.union res.indirects res'.indirects }
  [@@inline]

let filter_labels ir returns =
  Set.filter returns ~f:(has_block ir)
  [@@inline]

let invalidate_block_aux ir visited l =
  let invalidated = ref Addr.Set.empty in
  let returns = ref Label_set.empty in
  let indirects = ref Label_set.empty in
  let wl = Stack.singleton l in
  let rec loop () =
    match Stack.pop wl with
    | None -> ()
    | Some l -> (
      match Hash_set.strict_add visited l with
      | Error _ -> ()
      | Ok _ -> (
        match block ir l with
        | None -> ()
        | Some blk ->
            Cfg.iter_preds ir.cfg l ~f:(fun l e ->
                match e with
                | Edge.Return ->
                    (* if the edge was a return from a call/special
                     * then don't invalidate the predecessor,
                     * but mark it as a noreturn instead *)
                    returns := Set.add !returns l
                | Edge.Indirect ->
                    (* an indirect edge may have been incorrectly 
                     * resolved, but we should stop at the actual
                     * transfer of control *)
                    indirects := Set.add !indirects l
                | _ -> Stack.push wl l );
            ( match Hashtbl.find ir.a2bs blk.addr with
            | None -> ()
            | Some s ->
                let s = Set.remove s l in
                if Set.is_empty s then (
                  Hashtbl.remove ir.a2bs blk.addr;
                  invalidated := Set.add !invalidated blk.addr )
                else Hashtbl.set ir.a2bs blk.addr s );
            Cfg.remove_vertex ir.cfg l;
            Hashtbl.remove ir.blocks l;
            loop () ) )
  in
  loop ();
  let returns = filter_labels ir !returns in
  let indirects = filter_labels ir !indirects in
  new_invalidate !invalidated returns indirects

let invalidate_block ir l =
  let visited = Label.Hash_set.create () in
  invalidate_block_aux ir visited l

let invalidate_addr ir a =
  let open Option.Let_syntax in
  let%map bs = Hashtbl.find ir.a2bs a in
  let init = empty_invalidate () in
  (* invalidate starting at all the blocks
   * that correspond to this address *)
  let res =
    let visited = Label.Hash_set.create () in
    Set.to_sequence bs
    |> Sequence.map ~f:(invalidate_block_aux ir visited)
    |> Sequence.fold ~init ~f:union_invalidate
  in
  let returns = filter_labels ir res.returns in
  let indirects = filter_labels ir res.indirects in
  (* now, filter out the returns/indirects which correspond
   * to previously invalidated addresses *)
  let res =
    let visited = Label.Hash_set.create () in
    Set.union returns indirects
    |> Set.to_sequence
    |> Sequence.filter ~f:(fun l ->
           match addr_of_block ir l with
           | None -> false
           | Some a -> Set.mem res.invalidated a )
    |> Sequence.map ~f:(invalidate_block_aux ir visited)
    |> Sequence.fold ~init ~f:union_invalidate
    |> union_invalidate res
  in
  (* the remaining return edges shall be marked as noreturn *)
  let returns = filter_labels ir res.returns in
  let indirects = filter_labels ir res.indirects in
  Set.iter returns ~f:(fun l ->
      map_ctrl_direct ir l ~f:(function
        | Ctrl.Call (d, _, s) -> Ctrl.Call (d, None, s)
        | Ctrl.Special (s, _) -> Ctrl.Special (s, None)
        | ctrl -> ctrl )
      |> ignore );
  {res with returns; indirects}

module Slice_graph = Unordered_graph.Make (Label) (Unit)

type slice = {g: Slice_graph.t; defs: (Label.t, Def.Ord_set.t) Hashtbl.t}

(* do backward slicing to build a dependency graph
 * and collect relevant defs along the way *)
let slice_backward ?(follow_calls = false) ?(max_depth = 256) ir l vars =
  let cfg = ir.cfg in
  let g = Slice_graph.create () in
  let defs = Hashtbl.create (module Label) in
  let visited = Hash_set.create (module Label) in
  let rec slice vs blk depth =
    let open Option.Let_syntax in
    let%bind _ = Option.some_if (max_depth < 0 || depth <= max_depth) () in
    let l = Block.label_of blk in
    let%map _ = Hash_set.strict_add visited l |> Result.ok in
    let slice_pred vs blk =
      Option.iter
        (slice vs blk (succ depth))
        ~f:(fun _ ->
          let l' = Block.label_of blk in
          Slice_graph.insert_edge g l' () l )
    in
    let preds =
      Cfg.predecessors cfg l
      |> List.filter_map ~f:(fun (l, es) ->
             let%bind blk = block ir l in
             if not follow_calls then
               match Block.ctrl_of blk with
               | Call _
                 when List.for_all es ~f:(function
                        | Edge.Direct | Edge.Indirect -> true
                        | _ -> false ) -> None
               | _ -> return blk
             else return blk )
    in
    (* collect all vars that affect control flow since we will
     * need these later to infer constraints on variables *)
    let vs =
      match Block.ctrl_of blk with
      | JmpIf (e, _, _) ->
          Il.vars_of e |> Il.Var_set.of_list |> Il.Var_set.union vs
      | _ -> vs
    in
    (* collect relevant definitions given the
     * current set of vars we are tracking *)
    let vs =
      Set.fold vs ~init:Il.Var_set.empty ~f:(fun vs v ->
          match Block.last_def_of_var blk v with
          | None -> Set.add vs v
          | Some (i, (_, src)) ->
              let missing = ref Il.Var_set.empty in
              let rec collect i v =
                match Block.last_def_of_var_before blk v i with
                | None ->
                    missing := Set.add !missing v;
                    Def.Ord_set.empty
                | Some (i', (_, src)) ->
                    missing := Set.remove !missing v;
                    join src i'
              and join src i =
                List.map (Il.vars_of src) ~f:(collect i)
                |> Def.Ord_set.union_list
                |> Def.Ord_set.(union (singleton i))
              in
              let s = join src i in
              ( match Hashtbl.find defs l with
              | None -> Hashtbl.set defs l s
              | Some s' -> Hashtbl.set defs l (Set.union s' s) );
              Set.union vs !missing )
    in
    if Set.is_empty vs |> not then List.iter preds ~f:(slice_pred vs)
  in
  Slice_graph.insert_vertex g l;
  ( if Set.is_empty vars |> not then
    let blk = block_exn ir l in
    slice vars blk 0 |> ignore );
  {g; defs}
