open Core_kernel
open Ll2l_std

type t =
  { var_env: Il.var_env
  ; mem_env: Il.mem_env
  ; loads: Addr.Set.t
  ; stores: Addr.Set.t }

val create : ?var_env:Il.var_env -> ?mem_env:Il.mem_env -> unit -> t

type eval =
  {e: Il.expr; mu: Memory.Env.t option; loads: Addr.Set.t; stores: Addr.Set.t}

val eval_expr : t -> Il.expr -> eval

val eval_def : t -> dst:Il.var -> src:Il.expr -> t
