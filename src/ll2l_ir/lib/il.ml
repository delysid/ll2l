open Core_kernel
open Ll2l_std

module Type = struct
  type t = Bitv of int | Mem [@@deriving equal]

  let to_string = function
    | Bitv size -> Printf.sprintf "bitv%d\n" size
    | Mem -> "mem"
end

type const = {value: Bitvec.t; size: int} [@@deriving equal, compare, sexp]

module Var_hint = struct
  type t = Reg | Mem | Tmp [@@deriving equal, compare, sexp, hash]
end

type var = {name: string; size: int option; hint: Var_hint.t}
[@@deriving equal, compare, sexp, hash]

let string_of_var {name; size; hint} = name [@@inline]

module Var_comparable = struct
  module T = struct
    type t = var

    let equal = equal_var

    let compare = compare_var

    let sexp_of_t = sexp_of_var

    let t_of_sexp = var_of_sexp
  end

  include T
  include Comparable.Make (T)
end

module Var_set = Set.Make (Var_comparable)
module Var_map = Map.Make (Var_comparable)

type var_env = const Var_map.t

type mem_env = Memory.Env.t Var_map.t

module Var_hashable = struct
  include Var_comparable

  let hash = hash_var
end

type binop =
  | ADD
  | SUB
  | MUL
  | DIV
  | MOD
  | SDIV
  | SMOD
  | AND
  | OR
  | XOR
  | SHR
  | SAR
  | SHL
  | LEQ
  | LT
  | SLEQ
  | SLT
  | EQ
  | NEQ
  | GT
  | GEQ
  | SGT
  | SGEQ
  | FADD
  | FSUB
  | FMUL
  | FDIV
  | FMOD
  | FLEQ
  | FLT
  | FEQ
  | FNEQ
  | FUNORD
  | FGT
  | FGEQ
  | FATAN2
[@@deriving equal, compare, sexp]

let string_of_binop = function
  | ADD -> "+"
  | SUB -> "-"
  | MUL -> "*"
  | DIV -> "/"
  | MOD -> "%"
  | SDIV -> "/$"
  | SMOD -> "%$"
  | AND -> "&"
  | OR -> "|"
  | XOR -> "^"
  | SHR -> ">>"
  | SAR -> ">>>"
  | SHL -> "<<"
  | LEQ -> "<="
  | LT -> "<"
  | SLEQ -> "<=$"
  | SLT -> "<$"
  | EQ -> "="
  | NEQ -> "<>"
  | GT -> ">"
  | GEQ -> ">="
  | SGT -> ">$"
  | SGEQ -> ">=$"
  | FADD -> "+."
  | FSUB -> "-."
  | FMUL -> "*."
  | FDIV -> "/."
  | FMOD -> "%."
  | FLEQ -> "<=."
  | FLT -> "<."
  | FEQ -> "=."
  | FNEQ -> "<>."
  | FUNORD -> "<=>."
  | FGT -> ">."
  | FGEQ -> ">=."
  | FATAN2 -> "atan2"

type unop =
  | NOT
  | NEG
  | FNEG
  | FSIN
  | FCOS
  | FTAN
  | FATAN
  | FSQRT
  | FTRUNC
  | FROUND
  | FCEIL
  | FFLOOR
  | FEXP2
  | FISNAN
  | FISINF
  | FLOG
  | FLOG2
  | FLOG10
  | FABS
[@@deriving equal, compare, sexp]

let string_of_unop = function
  | NOT -> "~"
  | NEG -> "-"
  | FNEG -> "-."
  | FSIN -> "sin"
  | FCOS -> "cos"
  | FTAN -> "tan"
  | FATAN -> "atan"
  | FSQRT -> "sqrt"
  | FTRUNC -> "ftrunc"
  | FROUND -> "round"
  | FCEIL -> "ceil"
  | FFLOOR -> "floor"
  | FEXP2 -> "exp2"
  | FISNAN -> "isnan"
  | FISINF -> "isinf"
  | FLOG -> "log"
  | FLOG2 -> "log2"
  | FLOG10 -> "log10"
  | FABS -> "fabs"

type cast = ZEXT | SEXT | LOW | HIGH | FTOI | FTOF | ITOF | FPTOF | FTOFP
[@@deriving equal, compare, sexp]

type expr =
  | Const of const
  | Var of var
  | Load of reference
  | Store of store
  | Binop of binop * expr * expr
  | Unop of unop * expr
  | Cast of cast * int * expr
  | Extract of int * int * expr
  | Concat of expr * expr
  | Undefined of int
[@@deriving equal, compare, sexp]

and reference =
  { endian: Endian.t
  ; bytes: int
  ; (* assume byte addressing *)
    mem: expr
  ; loc: expr }
[@@deriving equal, compare, sexp]

and store = {r: reference; src: expr} [@@deriving equal, compare, sexp]

module Expr_comparable = struct
  module T = struct
    type t = expr

    let equal = equal_expr

    let compare = compare_expr

    let sexp_of_t = sexp_of_expr

    let t_of_sexp = expr_of_sexp
  end

  include T
  include Comparable.Make (T)
end

let is_fp_op = function
  | Binop (op, _, _) -> (
    match op with
    | FADD
     |FSUB
     |FMUL
     |FDIV
     |FMOD
     |FLEQ
     |FLT
     |FEQ
     |FNEQ
     |FUNORD
     |FGT
     |FGEQ
     |FATAN2 -> true
    | _ -> false )
  | Unop (op, _) -> (
    match op with
    | FNEG
     |FSIN
     |FCOS
     |FTAN
     |FATAN
     |FSQRT
     |FTRUNC
     |FROUND
     |FCEIL
     |FFLOOR
     |FEXP2
     |FISNAN
     |FISINF
     |FLOG
     |FLOG2
     |FLOG10 -> true
    | _ -> false )
  | Cast (k, _, _) -> (
    match k with
    | FTOI | FTOF | ITOF | FPTOF | FTOFP -> true
    | _ -> false )
  | _ -> false

let substitute_expr e ~sub =
  let a, b = sub in
  let rec aux e =
    if equal_expr e a then b
    else
      match e with
      | Const _ | Var _ | Undefined _ -> e
      | Load r ->
          let mem = aux r.mem in
          let loc = aux r.loc in
          Load {r with mem; loc}
      | Store s ->
          let mem = aux s.r.mem in
          let loc = aux s.r.loc in
          let r = {s.r with mem; loc} in
          let src = aux s.src in
          Store {r; src}
      | Binop (op, l, r) -> Binop (op, aux l, aux r)
      | Unop (op, e) -> Unop (op, aux e)
      | Cast (c, s, e) -> Cast (c, s, aux e)
      | Extract (h, l, e) -> Extract (h, l, aux e)
      | Concat (eh, el) -> Concat (aux eh, aux el)
  in
  aux e

let rec vars_of e =
  match e with
  | Var v -> [v]
  | Load r ->
      List.dedup_and_sort
        (vars_of r.mem @ vars_of r.loc)
        ~compare:compare_var
  | Store s ->
      List.dedup_and_sort
        (vars_of s.src @ vars_of s.r.mem @ vars_of s.r.loc)
        ~compare:compare_var
  | Binop (_, l, r) ->
      List.dedup_and_sort (vars_of l @ vars_of r) ~compare:compare_var
  | Unop (_, e) -> List.dedup_and_sort (vars_of e) ~compare:compare_var
  | Cast (_, _, e) -> List.dedup_and_sort (vars_of e) ~compare:compare_var
  | Extract (_, _, e) -> List.dedup_and_sort (vars_of e) ~compare:compare_var
  | Concat (h, l) ->
      List.dedup_and_sort (vars_of h @ vars_of l) ~compare:compare_var
  | _ -> []

let rec size_of = function
  | Const c -> Some c.size
  | Var v -> v.size
  | Load r -> Some (r.bytes lsl 3)
  | Store _ -> None
  | Binop (op, e1, e2) -> (
    match op with
    | LEQ | LT | SLEQ | SLT | EQ | NEQ | GT | GEQ | SGT | SGEQ -> Some 1
    | _ ->
        Option.(size_of e1 >>= fun s1 -> size_of e2 >>| fun s2 -> max s1 s2)
    )
  | Unop (_, e1) -> size_of e1
  | Cast (_, s, e1) -> Option.(size_of e1 >>| fun s1 -> max s s1)
  | Extract (hi, lo, e1) ->
      let s = hi - lo + 1 in
      Option.(size_of e1 >>| fun s1 -> min s s1)
  | Concat (e1, e2) ->
      Option.(size_of e1 >>= fun s1 -> size_of e2 >>| fun s2 -> s1 + s2)
  | Undefined s -> Some s

let rec type_of e =
  match size_of e with
  | Some size -> Some (Type.Bitv size)
  | None -> (
    match e with
    | Var v when Var_hint.(equal v.hint Mem) -> Some Type.Mem
    | Store _ -> Some Type.Mem
    | _ -> None )

let rec fold_constants ?(env = Var_map.empty) e =
  match e with
  | Var v -> Option.(value ~default:e (Map.find env v >>| fun c -> Const c))
  | Load r -> Load {r with loc= fold_constants r.loc}
  | Store s ->
      let r = {s.r with loc= fold_constants s.r.loc} in
      let s = {src= fold_constants s.src; r} in
      Store s
  | Binop (op, Const c1, Const c2) -> (
      let size = max c1.size c2.size in
      let m = Bitvec.modulus size in
      match op with
      | ADD ->
          let value = Bitvec.((c1.value + c2.value) mod m) in
          Const {value; size}
      | SUB ->
          let value = Bitvec.((c1.value - c2.value) mod m) in
          Const {value; size}
      | MUL ->
          let value = Bitvec.(c1.value * c2.value mod m) in
          Const {value; size}
      | DIV when Bitvec.(c2.value <> zero) ->
          let value = Bitvec.(c1.value / c2.value mod m) in
          Const {value; size}
      | SDIV when Bitvec.(c2.value <> zero) ->
          let value = Bitvec.(c1.value /$ c2.value mod m) in
          Const {value; size}
      | MOD when Bitvec.(c2.value <> zero) ->
          let value = Bitvec.(c1.value % c2.value mod m) in
          Const {value; size}
      | SMOD when Bitvec.(c2.value <> zero) ->
          let value = Bitvec.(c1.value %$ c2.value mod m) in
          Const {value; size}
      | DIV | MOD | SDIV | SMOD -> Undefined size
      | AND ->
          let value = Bitvec.(c1.value land c2.value mod m) in
          Const {value; size}
      | OR ->
          let value = Bitvec.(c1.value lor c2.value mod m) in
          Const {value; size}
      | XOR ->
          let value = Bitvec.(c1.value lxor c2.value mod m) in
          Const {value; size}
      | SHR ->
          let value = Bitvec.((c1.value lsr c2.value) mod m) in
          Const {value; size}
      | SAR ->
          let value = Bitvec.((c1.value asr c2.value) mod m) in
          Const {value; size}
      | SHL ->
          let value = Bitvec.((c1.value lsl c2.value) mod m) in
          Const {value; size}
      | LEQ ->
          let value = Bitvec.(bool (c1.value <= c2.value)) in
          Const {value; size= 1}
      | LT ->
          let value = Bitvec.(bool (c1.value <= c2.value)) in
          Const {value; size= 1}
      | SLEQ ->
          let cmp = Bitvec.signed_compare c1.value c2.value m in
          let value = Bitvec.bool (cmp <= 0) in
          Const {value; size= 1}
      | SLT ->
          let cmp = Bitvec.signed_compare c1.value c2.value m in
          let value = Bitvec.bool (cmp < 0) in
          Const {value; size= 1}
      | EQ ->
          let value = Bitvec.(bool (c1.value = c2.value)) in
          Const {value; size= 1}
      | NEQ ->
          let value = Bitvec.(bool (c1.value <> c2.value)) in
          Const {value; size= 1}
      | GT ->
          let value = Bitvec.(bool (c1.value > c2.value)) in
          Const {value; size= 1}
      | GEQ ->
          let value = Bitvec.(bool (c1.value >= c2.value)) in
          Const {value; size= 1}
      | SGT ->
          let cmp = Bitvec.signed_compare c1.value c2.value m in
          let value = Bitvec.bool (cmp > 0) in
          Const {value; size= 1}
      | SGEQ ->
          let cmp = Bitvec.signed_compare c1.value c2.value m in
          let value = Bitvec.bool (cmp >= 0) in
          Const {value; size= 1}
      | _ -> e )
  | Binop (op, Const c1, e2) when Bitvec.(c1.value = zero) -> (
    match op with
    | ADD | OR | SHR | SAR | SHL -> fold_constants e2
    | MUL | DIV | MOD | SDIV | SMOD | AND -> Const c1
    | SUB -> fold_constants (Unop (NEG, e2))
    | _ -> e )
  | Binop (op, e1, Const c2) when Bitvec.(c2.value = zero) -> (
    match op with
    | ADD | SUB | OR | SHR | SAR | SHL -> fold_constants e1
    | MUL | AND -> Const c2
    | DIV | MOD | SDIV | SMOD -> Undefined c2.size
    | _ -> e )
  | (Binop (AND, e1, e2) | Binop (OR, e1, e2)) when equal_expr e1 e2 -> e1
  | Binop (XOR, e1, e2) when equal_expr e1 e2 ->
      Option.(
        value ~default:e
          ( size_of e1
          >>= fun s1 ->
          size_of e2
          >>| fun s2 ->
          let size = max s1 s2 in
          let m = Bitvec.modulus size in
          Const {value= Bitvec.(int 0 mod m); size} ))
  | Binop (LEQ, e1, e2)
   |Binop (SLEQ, e1, e2)
   |Binop (EQ, e1, e2)
   |Binop (GEQ, e1, e2)
   |Binop (SGEQ, e1, e2)
    when equal_expr e1 e2 -> Const {value= Bitvec.bool true; size= 1}
  | Binop (LT, e1, e2)
   |Binop (SLT, e1, e2)
   |Binop (NEQ, e1, e2)
   |Binop (GT, e1, e2)
   |Binop (SGT, e1, e2)
    when equal_expr e1 e2 -> Const {value= Bitvec.bool false; size= 1}
  | Binop (op, e1, e2) ->
      let e1' = fold_constants e1 in
      let e2' = fold_constants e2 in
      let e' = Binop (op, e1', e2') in
      if not (equal_expr e e') then fold_constants e' else e'
  | Unop (op, Const c) -> (
      let m = Bitvec.modulus c.size in
      match op with
      | NOT ->
          let value = Bitvec.(~~(c.value) mod m) in
          Const {c with value}
      | NEG ->
          let value = Bitvec.(~-(c.value) mod m) in
          Const {c with value}
      | _ -> e )
  | Unop (op, e1) ->
      let e1' = fold_constants e1 in
      let e' = Unop (op, e1') in
      if not (equal_expr e e') then fold_constants e' else e'
  | Cast (k, s, Const c) -> (
      let m = Bitvec.modulus s in
      match k with
      | (ZEXT | SEXT) when s <= c.size -> Const c
      | ZEXT -> Const {c with size= s}
      | SEXT ->
          let sh = s - 1 in
          let sh = Bitvec.(int sh mod m) in
          let mask = Bitvec.((one lsl sh) mod m) in
          let v = c.value in
          let v = Bitvec.(v lxor mask mod m) in
          let v = Bitvec.((v - mask) mod m) in
          Const {value= v; size= s}
      | (HIGH | LOW) when s >= c.size -> Const c
      | HIGH ->
          let hi = c.size - 1 in
          let lo = c.size - s in
          let v = Bitvec.extract ~hi ~lo c.value in
          Const {value= v; size= s}
      | LOW ->
          let hi = s - 1 in
          let lo = 0 in
          let v = Bitvec.extract ~hi ~lo c.value in
          Const {value= v; size= s}
      | _ -> e )
  | Cast (k, s, e1) ->
      let e1' = fold_constants e1 in
      let e' = Cast (k, s, e1') in
      if not (equal_expr e e') then fold_constants e' else e'
  | Extract (hi, lo, Const c) ->
      let size = hi - lo + 1 in
      if size >= c.size then Const c
      else Const {value= Bitvec.extract ~hi ~lo c.value; size}
  | Extract (hi, lo, e1) ->
      let e1' = fold_constants e1 in
      let e' = Extract (hi, lo, e1') in
      if not (equal_expr e e') then fold_constants e' else e'
  | Concat (Const c1, Const c2) ->
      let size = c1.size + c2.size in
      let value = Bitvec.append c1.size c2.size c1.value c2.value in
      Const {value; size}
  | Concat (e1, e2) ->
      let e1' = fold_constants e1 in
      let e2' = fold_constants e2 in
      let e' = Concat (e1', e2') in
      if not (equal_expr e e') then fold_constants e' else e'
  | _ -> e

let rec string_of_expr = function
  | Const c ->
      let m = Bitvec.modulus c.size in
      let v = Bitvec.bigint c.value in
      let v_str = Bitvec.(to_string (v mod m)) in
      if c.size > 1 then "0x" ^ v_str else v_str
  | Var v -> string_of_var v
  | Load r -> string_of_reference r
  | Store s ->
      let ref_str = string_of_reference s.r in
      let src_str = string_of_expr s.src in
      Printf.sprintf "%s <- %s" ref_str src_str
  | Binop (op, l, r) -> (
      let op_str = string_of_binop op in
      let l_str = string_of_expr l in
      let r_str = string_of_expr r in
      match op with
      | FATAN2 -> Printf.sprintf "%s(%s, %s)" op_str l_str r_str
      | _ -> Printf.sprintf "(%s %s %s)" l_str op_str r_str )
  | Unop (op, e) -> (
      let op_str = string_of_unop op in
      let e_str = string_of_expr e in
      match op with
      | FSIN
       |FCOS
       |FTAN
       |FATAN
       |FSQRT
       |FTRUNC
       |FROUND
       |FCEIL
       |FFLOOR
       |FEXP2
       |FISNAN
       |FISINF
       |FLOG
       |FLOG2
       |FLOG10 -> Printf.sprintf "%s(%s)" op_str e_str
      | _ ->
          let e_str =
            match e with
            | Cast _ | Extract _ | Load _ -> Printf.sprintf "(%s)" e_str
            | _ -> e_str
          in
          string_of_unop op ^ e_str )
  | Cast (c, s, e) ->
      let e_str =
        match e with
        | Cast _ | Extract _ | Unop _ | Concat _ | Load _ ->
            Printf.sprintf "(%s)" (string_of_expr e)
        | _ -> string_of_expr e
      in
      let cast_str =
        match c with
        | ZEXT -> "ze"
        | SEXT -> "se"
        | LOW -> "lo"
        | HIGH -> "hi"
        | FTOI -> "ftoi"
        | FTOF -> "ftof"
        | ITOF -> "itof"
        | FPTOF -> "fptof"
        | FTOFP -> "ftofp"
      in
      Printf.sprintf "%s{%d}:%s" cast_str s e_str
  | Extract (hi, lo, e) ->
      let e_str =
        match e with
        | Cast _ | Extract _ | Unop _ | Concat _ | Load _ ->
            Printf.sprintf "(%s)" (string_of_expr e)
        | _ -> string_of_expr e
      in
      Printf.sprintf "ex{%d,%d}:%s" hi lo e_str
  | Concat (eh, el) ->
      let eh_str =
        let s = string_of_expr eh in
        match eh with
        | Cast _ | Extract _ | Load _ | Unop _ -> Printf.sprintf "(%s)" s
        | _ -> s
      in
      let el_str =
        match el with
        | Cast _ | Extract _ | Unop _ | Load _ ->
            Printf.sprintf "(%s)" (string_of_expr el)
        | _ -> string_of_expr el
      in
      Printf.sprintf "%s.%s" eh_str el_str
  | Undefined sz -> Printf.sprintf "undefined{%d}" sz

and string_of_reference r =
  let estr =
    (* endianness specifies byte order,
     * but since we're referencing only
     * a single byte then specifying
     * the order doesn't matter *)
    if r.bytes = 1 then "" else Endian.to_string r.endian
  in
  let mem_str = string_of_expr r.mem in
  let loc_str = string_of_expr r.loc in
  Printf.sprintf "%s[%s%d, %s]" mem_str estr (r.bytes lsl 3) loc_str

type label = Addr.t * int [@@deriving equal, compare, sexp]

module Label_comparable = struct
  module T = struct
    type t = label

    let equal = equal_label

    let compare = compare_label

    let sexp_of_t = sexp_of_label

    let t_of_sexp = label_of_sexp
  end

  include T
  include Comparable.Make (T)
end

module Label_set = Set.Make (Label_comparable)

let string_of_label (addr, n) =
  Printf.sprintf "L%s.%d" (Bitvec.to_string addr) n
  [@@inline]

module Jmp_hint = struct
  type t =
    | Jmp
    | Jmp_switch of string option
    | Call
    | Call_switch of string option
  [@@deriving equal, compare]
end

type special =
  | Breakpoint
  | Halt
  | Wait
  | Interrupt of int
  | Syscall
  | Exception
[@@deriving equal, compare]

type stmt =
  | Assign of expr * expr
  | Label of label
  | Goto of label
  | GotoIf of expr * label * label
  | JmpIf of expr * expr * expr
  | Jmp of expr * Jmp_hint.t
  | Unknown
  | Special of special
[@@deriving equal, compare]

type stmts = stmt list

type t = stmts list

let substitute_expr_in_stmt stmt ~sub =
  match stmt with
  | Assign (e1, e2) ->
      let e1 = substitute_expr e1 ~sub in
      let e2 = substitute_expr e2 ~sub in
      Assign (e1, e2)
  | GotoIf (e, tl, fl) ->
      let e = substitute_expr e ~sub in
      GotoIf (e, tl, fl)
  | JmpIf (e1, e2, e3) ->
      let e1 = substitute_expr e1 ~sub in
      let e2 = substitute_expr e2 ~sub in
      let e3 = substitute_expr e3 ~sub in
      JmpIf (e1, e2, e3)
  | Jmp (e, h) ->
      let e = substitute_expr e ~sub in
      Jmp (e, h)
  | _ -> stmt

let used_vars = function
  | Assign (_, src) -> vars_of src
  | JmpIf (e1, e2, e3) ->
      List.dedup_and_sort
        (vars_of e1 @ vars_of e2 @ vars_of e3)
        ~compare:compare_var
  | Jmp (e, _) -> vars_of e
  | _ -> []
  [@@inline]

let is_terminator = function
  | Goto _ | GotoIf _ | Jmp _ | JmpIf _ -> true
  | Special s -> (
    match s with
    | Breakpoint | Halt | Wait | Interrupt _ | Syscall | Exception -> true )
  | _ -> false
  [@@inline]

let string_of_special = function
  | Breakpoint -> "breakpoint"
  | Halt -> "halt"
  | Wait -> "wait"
  | Interrupt i -> Printf.sprintf "interrupt %d" i
  | Syscall -> "syscall"
  | Exception -> "exception"
  [@@inline]

let string_of_stmt = function
  | Assign (dst, src) ->
      Printf.sprintf "%s := %s" (string_of_expr dst) (string_of_expr src)
  | Label l -> Printf.sprintf "%s:" (string_of_label l)
  | Goto l -> Printf.sprintf "goto %s" (string_of_label l)
  | GotoIf (cond, tl, fl) ->
      Printf.sprintf "goto (%s, %s, %s)" (string_of_expr cond)
        (string_of_label tl) (string_of_label fl)
  | JmpIf (cond, te, fe) ->
      Printf.sprintf "jmp (%s, %s, %s)" (string_of_expr cond)
        (string_of_expr te) (string_of_expr fe)
  | Jmp (dst, hint) ->
      let hint_str, sw =
        match hint with
        | Jmp_hint.Jmp -> ("jmp", "")
        | Jmp_hint.Jmp_switch (Some s) -> ("jmp", " switch " ^ s)
        | Jmp_hint.Jmp_switch None -> ("jmp", " switch")
        | Jmp_hint.Call -> ("call", "")
        | Jmp_hint.Call_switch (Some s) -> ("call", " switch " ^ s)
        | Jmp_hint.Call_switch None -> ("call", " switch")
      in
      Printf.sprintf "%s %s%s" hint_str (string_of_expr dst) sw
  | Unknown -> "unknown"
  | Special s -> string_of_special s

module Syntax = struct
  let bit b = Const {value= Bitvec.bool b; size= 1} [@@inline]

  let bitv value size = Const {value; size} [@@inline]

  let num n size =
    let m = Bitvec.modulus size in
    Const {value= Bitvec.(int n mod m); size}
    [@@inline]

  let num32 i size =
    let m = Bitvec.modulus size in
    Const {value= Bitvec.(int32 i mod m); size}
    [@@inline]

  let num64 i size =
    let m = Bitvec.modulus size in
    Const {value= Bitvec.(int64 i mod m); size}
    [@@inline]

  let num_big s size = Const {value= Bitvec.of_string s; size} [@@inline]

  let reg name size =
    let size = Some size in
    let hint = Var_hint.Reg in
    Var {name; size; hint}
    [@@inline]

  let mem name =
    let size = None in
    let hint = Var_hint.Mem in
    Var {name; size; hint}
    [@@inline]

  let tmp n size =
    let name = Printf.sprintf "#%d" n in
    let size = Some size in
    let hint = Var_hint.Tmp in
    Var {name; size; hint}
    [@@inline]

  let load endian bytes mem loc = Load {endian; bytes; mem; loc} [@@inline]

  let store endian bytes mem loc src =
    let r = {endian; bytes; mem; loc} in
    Store {r; src}
    [@@inline]

  let store_r r src = Store {r; src} [@@inline]

  let bop op lhs rhs = Binop (op, lhs, rhs) [@@inline]

  let ( + ) = bop ADD

  let ( +. ) = bop FADD

  let ( - ) = bop SUB

  let ( -. ) = bop FSUB

  let ( * ) = bop MUL

  let ( *. ) = bop FMUL

  let ( / ) = bop DIV

  let ( /. ) = bop FDIV

  let ( % ) = bop MOD

  let ( %. ) = bop FMOD

  let ( /$ ) = bop SDIV

  let ( %$ ) = bop SMOD

  let ( & ) = bop AND

  let ( || ) = bop OR

  let ( ^ ) = bop XOR

  let ( >> ) = bop SHR

  let ( >>> ) = bop SAR

  let ( << ) = bop SHL

  let ( <= ) = bop LEQ

  let ( <=. ) = bop FLEQ

  let ( < ) = bop LT

  let ( <. ) = bop FLT

  let ( <=$ ) = bop SLEQ

  let ( <$ ) = bop SLT

  let ( = ) = bop EQ

  let ( =. ) = bop FEQ

  let ( <> ) = bop NEQ

  let ( <>. ) = bop FNEQ

  let ( <=>. ) = bop FUNORD

  let ( > ) = bop GT

  let ( >. ) = bop FGT

  let ( >= ) = bop GEQ

  let ( >=. ) = bop FGEQ

  let ( >$ ) = bop SGT

  let ( >=$ ) = bop SGEQ

  let atan2 = bop FATAN2

  let uop op e = Unop (op, e) [@@inline]

  let ( ~~ ) = uop NOT

  let ( ~- ) = uop NEG

  let ( ~-. ) = uop FNEG

  let sin = uop FSIN

  let cos = uop FCOS

  let tan = uop FTAN

  let atan = uop FATAN

  let sqrt = uop FSQRT

  let ftrunc = uop FTRUNC

  let round = uop FROUND

  let ceil = uop FCEIL

  let floor = uop FFLOOR

  let exp2 = uop FEXP2

  let is_nan = uop FISNAN

  let is_inf = uop FISINF

  let flog = uop FLOG

  let flog2 = uop FLOG2

  let flog10 = uop FLOG10

  let fabs = uop FABS

  let cst op size e = Cast (op, size, e) [@@inline]

  let ze = cst ZEXT

  let se = cst SEXT

  let lo = cst LOW

  let hi = cst HIGH

  let ftoi = cst FTOI

  let ftof = cst FTOF

  let itof = cst ITOF

  let fptof = cst FPTOF

  let ftofp = cst FTOFP

  let ex ~hi ~lo e = Extract (hi, lo, e) [@@inline]

  let ( @@ ) e1 e2 = Concat (e1, e2) [@@inline]

  let undefined size = Undefined size [@@inline]

  let ( := ) dst src = Assign (dst, src) [@@inline]

  let ( @ ) lbl = Label lbl [@@inline]

  let goto lbl = Goto lbl [@@inline]

  let goto_if cond tl fl = GotoIf (cond, tl, fl) [@@inline]

  let jmp_if cond te fe = JmpIf (cond, te, fe) [@@inline]

  let jmp e = Jmp (e, Jmp_hint.Jmp) [@@inline]

  let jmp_switch e s = Jmp (e, Jmp_hint.Jmp_switch s) [@@inline]

  let call e = Jmp (e, Jmp_hint.Call) [@@inline]

  let call_switch e s = Jmp (e, Jmp_hint.Call_switch s) [@@inline]

  let unk () = Unknown [@@inline]

  let breakpoint () = Special Breakpoint [@@inline]

  let halt () = Special Halt [@@inline]

  let wait () = Special Wait [@@inline]

  let interrupt i = Special (Interrupt i) [@@inline]

  let syscall () = Special Syscall [@@inline]

  let exn () = Special Exception [@@inline]
end

let default_mem = {name= "mem"; size= None; hint= Var_hint.Mem}

(* generic name for a memory *)
let mu = Var default_mem

module Context = struct
  type t =
    { mutable tmp_count: int
    ; tmp_nums: (int, int) Hashtbl.t
    ; labels: (Addr.t, int) Hashtbl.t }

  let create () =
    { tmp_count= 1
    ; tmp_nums= Hashtbl.create (module Int)
    ; labels= Hashtbl.create (module Addr) }
    [@@inline]

  let tmp ctx size =
    (* if we reset tmp_count then we can reuse variables of
     * the same size instead of having ridiculously long names.
     * this is also based on the fact that temporary vars we
     * created when lifting an instruction aren't used outside of
     * that scope, so those definitions are free for us to clobber *)
    let rec find n =
      ctx.tmp_count <- n + 1;
      match Hashtbl.find ctx.tmp_nums n with
      | Some size' -> if size = size' then Syntax.tmp n size else find (n + 1)
      | None ->
          Hashtbl.set ctx.tmp_nums ~key:n ~data:size;
          Syntax.tmp n size
    in
    find ctx.tmp_count
    [@@inline]

  let reset_tmp ctx = ctx.tmp_count <- 1 [@@inline]

  let label ctx addr =
    let n =
      match Hashtbl.find ctx.labels addr with
      | Some n -> n
      | None -> 1
    in
    Hashtbl.set ctx.labels addr (n + 1);
    (addr, n)
    [@@inline]
end

let make_blocks stmts =
  let rec aux res = function
    | [] -> List.rev res
    | stmt :: rest ->
        if is_terminator stmt then aux (stmt :: res) []
        else aux (stmt :: res) rest
  in
  let rec aux2 res stmts =
    if List.is_empty stmts then List.rev res
    else
      let blk = aux [] stmts in
      if List.is_empty blk then List.rev res
      else aux2 (blk :: res) List.(drop stmts (length blk))
  in
  aux2 [] stmts

let unmake_blocks = List.concat

let fold_constants_in_stmts stmts =
  let rec aux res lbls = function
    | [] -> (List.rev res, lbls)
    | stmt :: rest -> (
      match stmt with
      | Assign (dst, src) ->
          aux (Assign (dst, fold_constants src) :: res) lbls rest
      | GotoIf (e, tl, fl) -> (
          let e' = fold_constants e in
          match e' with
          | Const c when c.size <= 64 ->
              let stmt', lbl =
                match Bitvec.to_int64 c.value with
                | 0L -> (Goto fl, tl)
                | _ -> (Goto tl, fl)
              in
              aux (stmt' :: res) (Set.add lbls lbl) rest
          | _ -> aux (GotoIf (e', tl, fl) :: res) lbls rest )
      | Jmp (e, hint) -> aux (Jmp (fold_constants e, hint) :: res) lbls rest
      | JmpIf (e1, e2, e3) ->
          let e1 = fold_constants e1 in
          let e2 = fold_constants e2 in
          let e3 = fold_constants e3 in
          aux (JmpIf (e1, e2, e3) :: res) lbls rest
      | _ -> aux (stmt :: res) lbls rest )
  in
  let stmts', lbls = aux [] Label_set.empty stmts in
  (* get the labels that we folded out
   * and are no longer referred to in the code
   * (assume that it is self-contained) *)
  let lbls =
    List.fold stmts' ~init:lbls ~f:(fun lbls stmt ->
        match stmt with
        | Goto l -> Set.remove lbls l
        | GotoIf (_, tl, fl) -> Set.(remove (remove lbls tl) fl)
        | _ -> lbls)
  in
  make_blocks stmts'
  |> List.filter ~f:(function
       | Label l :: _ -> not (Set.mem lbls l)
       (* this fallthrough case means that
        * the block is actually not well-formed,
        * but we will catch these errors later *)
       | _ -> true)
  |> unmake_blocks

let normalize_end endaddr wordsz ctx stmts =
  match List.last stmts with
  | None -> Syntax.[jmp (bitv endaddr wordsz)]
  | Some stmt -> (
    match stmt with
    | Jmp (Const _, _) -> stmts
    | _ when is_terminator stmt -> stmts
    | _ -> stmts @ Syntax.[jmp (bitv endaddr wordsz)] )

let normalize_conditions ctx stmts =
  let rec aux res = function
    | [] -> List.rev res
    | stmt :: rest -> (
      match stmt with
      | GotoIf (Var _, _, _) | JmpIf (Var _, _, _) -> aux (stmt :: res) rest
      | GotoIf (e, tl, fl) ->
          let t = Context.tmp ctx 1 in
          let a = Assign (t, e) in
          let stmt' = GotoIf (t, tl, fl) in
          aux (stmt' :: a :: res) rest
      | JmpIf (e, te, fe) ->
          let t = Context.tmp ctx 1 in
          let a = Assign (t, e) in
          let stmt' = JmpIf (t, te, fe) in
          aux (stmt' :: a :: res) rest
      | _ -> aux (stmt :: res) rest )
  in
  aux [] stmts

let remove_identity_assigns stmts =
  let rec aux res = function
    | [] -> List.rev res
    | stmt :: rest -> (
      match stmt with
      | Assign (dst, src) ->
          let res = if equal_expr dst src then res else stmt :: res in
          aux res rest
      | _ -> aux (stmt :: res) rest )
  in
  aux [] stmts

include Syntax
