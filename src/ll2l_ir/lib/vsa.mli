open Core_kernel
open Ll2l_std

module Bitv_lattice : sig
  type t = Interval of Interval.t | Bottom [@@deriving equal, sexp]

  val interval : Interval.t -> t

  val interval_of : t -> Interval.t option

  val join : t -> t -> t

  val widen_join : t -> t -> t

  val meet : t -> t -> t

  val to_string : t -> string
end

module Mem_lattice : sig
  module Env : sig
    type t = Bitv_lattice.t Addr.Map.t [@@deriving equal, sexp]

    val empty : t

    val put :
         t
      -> endian:Endian.t
      -> addr:Addr.t
      -> bytes:int
      -> value:Bitv_lattice.t
      -> t

    val get :
      t -> endian:Endian.t -> addr:Addr.t -> bytes:int -> Bitv_lattice.t

    val join : t -> t -> t

    val widen_join : t -> t -> t

    val meet : t -> t -> t
  end

  type t = Mem of Env.t | Bottom [@@deriving equal, sexp]

  val of_concrete_env : Memory.Env.t -> t

  val top : t

  val mem_of : t -> Env.t option

  val join : t -> t -> t

  val widen_join : t -> t -> t

  val meet : t -> t -> t
end

module Env : sig
  type t =
    | Env of
        { bitv: Bitv_lattice.t Il.Var_map.t
        ; memory: Mem_lattice.t Il.Var_map.t }
    | Bottom
  [@@deriving equal, sexp]

  val top : t

  val find_bitv : t -> Il.var -> Bitv_lattice.t option

  val set_bitv : t -> Il.var -> Bitv_lattice.t -> t

  val find_mem : t -> Il.var -> Mem_lattice.t option

  val set_mem : t -> Il.var -> Mem_lattice.t -> t

  val join : t -> t -> t

  val widen_join : t -> t -> t

  val meet : t -> t -> t
end

type t = (Ir.Label.t, Env.t) Ir.Cfg.Fixpoint.t

val create : entry:Env.t -> start:Ir.Label.t -> t

val vsa :
     ?steps:int
  -> ?rev:bool
  -> ?follow_calls:bool
  -> t
  -> Ir.t
  -> subs:Ir.subs
  -> start:Ir.Label.t
  -> t
