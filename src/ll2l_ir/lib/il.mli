open Core_kernel
open Ll2l_std

module Type : sig
  type t = Bitv of int | Mem [@@deriving equal]

  val to_string : t -> string
end

type const = {value: Bitvec.t; size: int} [@@deriving equal, compare, sexp]

module Var_hint : sig
  type t = Reg | Mem | Tmp [@@deriving equal, compare, sexp, hash]
end

type var = {name: string; size: int option; hint: Var_hint.t}
[@@deriving equal, compare, sexp, hash]

val string_of_var : var -> string

module Var_comparable : sig
  type t = var

  type comparator_witness

  val comparator : (t, comparator_witness) Comparator.t

  val equal : t -> t -> bool

  val compare : t -> t -> int

  val sexp_of_t : t -> Sexp.t

  val t_of_sexp : Sexp.t -> t
end

module Var_set : sig
  include module type of Set.Make (Var_comparable)
end

module Var_map : sig
  include module type of Map.Make (Var_comparable)
end

type var_env = const Var_map.t

type mem_env = Memory.Env.t Var_map.t

module Var_hashable : sig
  include module type of Var_comparable

  val hash : t -> int
end

type binop =
  | ADD
  | SUB
  | MUL
  | DIV
  | MOD
  | SDIV
  | SMOD
  | AND
  | OR
  | XOR
  | SHR
  | SAR
  | SHL
  | LEQ
  | LT
  | SLEQ
  | SLT
  | EQ
  | NEQ
  | GT
  | GEQ
  | SGT
  | SGEQ
  | FADD
  | FSUB
  | FMUL
  | FDIV
  | FMOD
  | FLEQ
  | FLT
  | FEQ
  | FNEQ
  | FUNORD
  | FGT
  | FGEQ
  | FATAN2
[@@deriving equal, compare, sexp]

type unop =
  | NOT
  | NEG
  | FNEG
  | FSIN
  | FCOS
  | FTAN
  | FATAN
  | FSQRT
  | FTRUNC
  | FROUND
  | FCEIL
  | FFLOOR
  | FEXP2
  | FISNAN
  | FISINF
  | FLOG
  | FLOG2
  | FLOG10
  | FABS
[@@deriving equal, compare, sexp]

type cast = ZEXT | SEXT | LOW | HIGH | FTOI | FTOF | ITOF | FPTOF | FTOFP
[@@deriving equal, compare, sexp]

type expr =
  | Const of const
  | Var of var
  | Load of reference
  | Store of store
  | Binop of binop * expr * expr
  | Unop of unop * expr
  | Cast of cast * int * expr
  | Extract of int * int * expr
  | Concat of expr * expr
  | Undefined of int
[@@deriving equal, compare, sexp]

and reference = {endian: Endian.t; bytes: int; mem: expr; loc: expr}
[@@deriving equal, compare, sexp]

and store = {r: reference; src: expr} [@@deriving equal, compare, sexp]

module Expr_comparable : sig
  type t = expr

  type comparator_witness

  val comparator : (t, comparator_witness) Comparator.t

  val equal : t -> t -> bool

  val compare : t -> t -> int

  val sexp_of_t : t -> Sexp.t

  val t_of_sexp : Sexp.t -> t
end

val is_fp_op : expr -> bool

val substitute_expr : expr -> sub:expr * expr -> expr

val vars_of : expr -> var list

val size_of : expr -> int option

val type_of : expr -> Type.t option

val fold_constants : ?env:var_env -> expr -> expr

val string_of_expr : expr -> string

type label = Addr.t * int [@@deriving equal, compare, sexp]

module Label_comparable : sig
  type t = label

  type comparator_witness

  val comparator : (t, comparator_witness) Comparator.t

  val equal : t -> t -> bool

  val compare : t -> t -> int

  val sexp_of_t : t -> Sexp.t

  val t_of_sexp : Sexp.t -> t
end

module Label_set : sig
  include module type of Set.Make (Label_comparable)
end

val string_of_label : label -> string

module Jmp_hint : sig
  type t =
    | Jmp
    | Jmp_switch of string option
    | Call
    | Call_switch of string option
  [@@deriving equal, compare]
end

type special =
  | Breakpoint
  | Halt
  | Wait
  | Interrupt of int
  | Syscall
  | Exception
[@@deriving equal, compare]

val string_of_special : special -> string

type stmt =
  | Assign of expr * expr
  | Label of label
  | Goto of label
  | GotoIf of expr * label * label
  | JmpIf of expr * expr * expr
  | Jmp of expr * Jmp_hint.t
  | Unknown
  | Special of special
[@@deriving equal, compare]

type stmts = stmt list

type t = stmts list

val substitute_expr_in_stmt : stmt -> sub:expr * expr -> stmt

val used_vars : stmt -> var list

val is_terminator : stmt -> bool

val string_of_stmt : stmt -> string

val fold_constants_in_stmts : stmts -> stmts

module Syntax : sig
  val bit : bool -> expr

  val bitv : Bitvec.t -> int -> expr

  val num : int -> int -> expr

  val num32 : int32 -> int -> expr

  val num64 : int64 -> int -> expr

  val num_big : string -> int -> expr

  val reg : string -> int -> expr

  val mem : string -> expr

  val tmp : int -> int -> expr

  val load : [`LE | `BE] -> int -> expr -> expr -> expr

  val store : [`LE | `BE] -> int -> expr -> expr -> expr -> expr

  val store_r : reference -> expr -> expr

  val ( + ) : expr -> expr -> expr

  val ( +. ) : expr -> expr -> expr

  val ( - ) : expr -> expr -> expr

  val ( -. ) : expr -> expr -> expr

  val ( * ) : expr -> expr -> expr

  val ( *. ) : expr -> expr -> expr

  val ( / ) : expr -> expr -> expr

  val ( /. ) : expr -> expr -> expr

  val ( % ) : expr -> expr -> expr

  val ( %. ) : expr -> expr -> expr

  val ( /$ ) : expr -> expr -> expr

  val ( %$ ) : expr -> expr -> expr

  val ( & ) : expr -> expr -> expr

  val ( || ) : expr -> expr -> expr

  val ( ^ ) : expr -> expr -> expr

  val ( >> ) : expr -> expr -> expr

  val ( >>> ) : expr -> expr -> expr

  val ( << ) : expr -> expr -> expr

  val ( <= ) : expr -> expr -> expr

  val ( <=. ) : expr -> expr -> expr

  val ( < ) : expr -> expr -> expr

  val ( <. ) : expr -> expr -> expr

  val ( <=$ ) : expr -> expr -> expr

  val ( <$ ) : expr -> expr -> expr

  val ( = ) : expr -> expr -> expr

  val ( =. ) : expr -> expr -> expr

  val ( <> ) : expr -> expr -> expr

  val ( <>. ) : expr -> expr -> expr

  val ( <=>. ) : expr -> expr -> expr

  val ( > ) : expr -> expr -> expr

  val ( >. ) : expr -> expr -> expr

  val ( >= ) : expr -> expr -> expr

  val ( >=. ) : expr -> expr -> expr

  val ( >$ ) : expr -> expr -> expr

  val ( >=$ ) : expr -> expr -> expr

  val atan2 : expr -> expr -> expr

  val ( ~~ ) : expr -> expr

  val ( ~- ) : expr -> expr

  val ( ~-. ) : expr -> expr

  val sin : expr -> expr

  val cos : expr -> expr

  val tan : expr -> expr

  val atan : expr -> expr

  val sqrt : expr -> expr

  val ftrunc : expr -> expr

  val round : expr -> expr

  val ceil : expr -> expr

  val floor : expr -> expr

  val exp2 : expr -> expr

  val is_nan : expr -> expr

  val is_inf : expr -> expr

  val flog : expr -> expr

  val flog2 : expr -> expr

  val flog10 : expr -> expr

  val fabs : expr -> expr

  val ze : int -> expr -> expr

  val se : int -> expr -> expr

  val lo : int -> expr -> expr

  val hi : int -> expr -> expr

  val ftoi : int -> expr -> expr

  val ftof : int -> expr -> expr

  val itof : int -> expr -> expr

  val fptof : int -> expr -> expr

  val ftofp : int -> expr -> expr

  val ex : hi:int -> lo:int -> expr -> expr

  val ( @@ ) : expr -> expr -> expr

  val undefined : int -> expr

  val ( := ) : expr -> expr -> stmt

  val ( @ ) : label -> stmt

  val goto : label -> stmt

  val goto_if : expr -> label -> label -> stmt

  val jmp_if : expr -> expr -> expr -> stmt

  val jmp : expr -> stmt

  val jmp_switch : expr -> string option -> stmt

  val call : expr -> stmt

  val call_switch : expr -> string option -> stmt

  val unk : unit -> stmt

  val breakpoint : unit -> stmt

  val halt : unit -> stmt

  val wait : unit -> stmt

  val interrupt : int -> stmt

  val syscall : unit -> stmt

  val exn : unit -> stmt
end

val default_mem : var

val mu : expr

module Context : sig
  type t

  val create : unit -> t

  val tmp : t -> int -> expr

  val reset_tmp : t -> unit

  val label : t -> Addr.t -> label
end

val make_blocks : stmts -> t

val unmake_blocks : t -> stmts

val normalize_end : Addr.t -> int -> Context.t -> stmts -> stmts

val normalize_conditions : Context.t -> stmts -> stmts

val remove_identity_assigns : stmts -> stmts

include module type of Syntax
