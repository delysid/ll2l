open Core_kernel
open Ll2l_arch
open Ll2l_ir
open Ll2l_program
open Ll2l_std

let hex_digit_to_int c =
  match c with
  | '0' .. '9' -> Char.to_int c - Char.to_int '0'
  | 'a' .. 'f' -> Char.to_int c - Char.to_int 'a' + 10
  | 'A' .. 'F' -> Char.to_int c - Char.to_int 'A' + 10
  | _ -> invalid_arg "bad hex string (invalid char)"

let format_hex_string s =
  let s = String.concat ~sep:"" (String.split s ~on:' ') in
  if String.length s mod 2 <> 0 then
    invalid_arg "bad hex string (odd length)"
  else
    let chars = String.to_list s in
    let rec aux res = function
      | [] -> Bytes.of_char_list (List.rev res)
      | x :: y :: rest ->
          let b1 = hex_digit_to_int x in
          let b2 = hex_digit_to_int y in
          let b = Char.of_int_exn ((b1 lsl 4) lor b2 land 0xFF) in
          aux (b :: res) rest
      | _ -> invalid_arg "bad hex string (odd number of nibbles)"
    in
    aux [] chars

let format_addr addr = function
  | 64 -> Printf.sprintf "0x%016LX" addr
  | 32 -> Printf.sprintf "0x%08LX" addr
  | 16 -> Printf.sprintf "0x%04LX" addr
  | 8 -> Printf.sprintf "0x%02LX" addr
  | _ -> Printf.sprintf "0x%LX" addr

let dump_ir ?(subs = None) ?(chan = None) ?(starts = Addr.Set.empty)
    (p : Program.t) =
  let print fmt =
    match chan with
    | None -> printf fmt
    | Some chan -> Out_channel.fprintf chan fmt
  in
  let cfg = Ir.cfg_of p.ir in
  let dump_ir_block blk =
    let l = Ir.Block.label_of blk in
    let defs = Ir.Block.alist_of_defs blk in
    let ctrl = Ir.Block.ctrl_of blk in
    let addr_str =
      match Ir.addr_of_block p.ir l with
      | Some addr -> Printf.sprintf "0x%s" (Bitvec.to_string addr)
      | None -> "?"
    in
    let del_str = if Ir.Block.is_delay blk then " (delay)" else "" in
    print "[%s @ %s]%s\n" Ir.Label.(to_string l) addr_str del_str;
    print "pred: {%s}\n"
      (String.concat ~sep:", "
         (List.map (Ir.Cfg.predecessors cfg l) ~f:(fun (l', es) ->
              let es_str =
                String.concat ~sep:", "
                  (List.map es ~f:(fun e -> Ir.Edge.to_string e))
              in
              Printf.sprintf "%s (%s)" (Ir.Label.to_string l') es_str ) ) );
    print "succ: {%s}\n"
      (String.concat ~sep:", "
         (List.map (Ir.Cfg.successors cfg l) ~f:(fun (l', es) ->
              let es_str =
                String.concat ~sep:", "
                  (List.map es ~f:(fun e -> Ir.Edge.to_string e))
              in
              Printf.sprintf "%s (%s)" (Ir.Label.to_string l') es_str ) ) );
    List.iter defs ~f:(fun (i, def) ->
        print "%s: %s\n" Ir.Def.Ord.(to_string i) Ir.Def.(to_string def) );
    print "ctrl: %s\n" Ir.Ctrl.(to_string ctrl);
    print "\n"
  in
  match subs with
  | Some subs ->
      subs#subroutines
      |> List.iter ~f:(fun sub ->
             let addr = Ir.Sub.entry_of sub |> Ir.addr_of_block_exn p.ir in
             print "====================================================\n";
             print "sub_%s (entry: %s)\n\n" (Bitvec.to_string addr)
               (Ir.Sub.entry_of sub |> Ir.Label.to_string);
             Ir.Sub.cfg_of sub |> Ir.Cfg.vertices
             |> List.sort ~compare:(fun l1 l2 ->
                    let a1 = Ir.addr_of_block_exn p.ir l1 in
                    let a2 = Ir.addr_of_block_exn p.ir l2 in
                    match Addr.compare a1 a2 with
                    | 0 -> Ir.Label.compare l1 l2
                    | n -> n )
             |> List.iter ~f:(fun l -> Ir.block_exn p.ir l |> dump_ir_block) )
  | None ->
      Ir.blocks_of p.ir
      |> List.sort ~compare:(fun blk1 blk2 ->
             match
               Addr.compare (Ir.Block.addr_of blk1) (Ir.Block.addr_of blk2)
             with
             | 0 ->
                 Ir.(
                   Label.compare (Block.label_of blk1) (Block.label_of blk2))
             | n -> n )
      |> List.iter ~f:dump_ir_block

let dump_native ?(subs = None) ?(chan = None) ?(starts = Addr.Set.empty)
    (p : Program.t) =
  let print fmt =
    match chan with
    | None -> printf fmt
    | Some chan -> Out_channel.fprintf chan fmt
  in
  if Option.is_some subs then
    let subs = Program.native_subroutines p ~subs ~starts in
    Map.iteri subs ~f:(fun ~key:entry ~data:blks ->
        print "====================================================\n";
        print "sub_%s\n\n" (Bitvec.to_string entry);
        Map.iter blks ~f:(fun blk ->
            List.iter (Code.Block.addrs_of blk) ~f:(fun addr ->
                match Code.instr_of_addr p.code addr with
                | None -> ()
                | Some Arch.(I (arch, instr)) ->
                    print "%s\n" (Arch.string_of_instr_full arch instr ~addr) );
            print "\n" ) )
  else
    Code.blocks_of p.code
    |> Map.iter ~f:(fun blk ->
           List.iter (Code.Block.addrs_of blk) ~f:(fun addr ->
               match Code.instr_of_addr p.code addr with
               | None -> ()
               | Some Arch.(I (arch, instr)) ->
                   print "%s\n" (Arch.string_of_instr_full arch instr ~addr) );
           print "\n" )
