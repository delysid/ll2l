open Core

let command =
  let commands =
    [ ("decode", Decode.command)
    ; ("lift", Lift.command)
    ; ("disasm", Disasm.command)
    ; ("image", Imgdump.command) ]
  in
  Command.group ~summary:"ll2l" commands

let () = Command.run ~version:"0.1" ~build_info:"ll2l" command
