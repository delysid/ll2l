open Core
open Stdio
open Ll2l_arch
open Ll2l_std

let decode arch addr bytes =
  let Arch.(T arch) = Arch.of_string arch in
  let addr =
    match addr with
    | None -> Addr.int64 0x4000L
    | Some s -> Bitvec.of_string s
  in
  let bytes = Util.format_hex_string bytes in
  let decode_ctx = Arch.create_decode_ctx arch in
  let rec aux i addr =
    if i < Bytes.length bytes then (
      let instr =
        match Arch.decode arch decode_ctx (Bytes.subo bytes ~pos:i) with
        | Ok instr -> instr
        | Error err ->
            failwith
            @@ Printf.sprintf "decoder: %s"
                 (Arch.string_of_decode_err arch err)
      in
      printf "%s\n" (Arch.string_of_instr_full arch instr ~addr);
      let len = Arch.instr_length arch instr in
      let end_addr = Arch.instr_end_addr arch instr ~addr in
      aux (i + len) end_addr )
    else ()
  in
  aux 0 addr

let command =
  Command.basic
    ~summary:
      "Decode and pretty-print an instruction for a given architecture"
    (let%map_open.Command arch =
       flag "arch" (required string) ~doc:" architecture name"
     and addr =
       flag "addr" (optional string)
         ~doc:" starting address (default: 0x4000)"
     and bytes = flag "bytes" (required string) ~doc:" instruction bytes" in
     fun () -> decode arch addr bytes )
