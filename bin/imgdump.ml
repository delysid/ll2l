open Core
open Stdio
open Ll2l_arch
open Ll2l_image
open Ll2l_std

let dump_seg (img : Image.t) seg =
  let Arch.(T arch) = img.arch in
  let word_sz = Arch.word_size_of arch in
  let bytes = Memory.Segment.to_bytes seg in
  let addr = Memory.Segment.address seg in
  let seg_len = Memory.Segment.length seg in
  let min_len = Arch.min_instr_length arch in
  let max_len = Arch.max_instr_length arch in
  let decode_ctx = Arch.create_decode_ctx arch in
  let rec aux i addr =
    if i >= seg_len then ()
    else
      match Map.find img.relocs addr with
      | Some rel ->
          printf "%s: (data%d)\n"
            (Util.format_addr (Addr.to_int64 addr) word_sz)
            (rel.size lsl 3);
          aux (i + rel.size) Addr.(addr + int rel.size)
      | None -> (
          let len = Int.min max_len (seg_len - i) in
          let bytes' = Bytes.sub bytes ~pos:i ~len in
          match Arch.decode arch decode_ctx bytes' with
          | Ok instr ->
              printf "%s\n" (Arch.string_of_instr_full arch instr ~addr);
              let len = Arch.instr_length arch instr in
              let end_addr = Arch.instr_end_addr arch instr ~addr in
              aux (i + len) end_addr
          | _ ->
              let len = Int.min min_len (seg_len - i) in
              let b = Bytes.sub bytes ~pos:i ~len in
              let s =
                String.concat ~sep:" "
                @@ List.map (Bytes.to_list b) ~f:(fun b ->
                       Printf.sprintf "%02X" (Char.to_int b) )
              in
              printf "%s: (bad %s)\n"
                (Util.format_addr (Addr.to_int64 addr) word_sz)
                s;
              aux (i + min_len) Addr.(addr + int min_len) )
  in
  aux 0 addr

let command =
  Command.basic ~summary:"Dump information about an image file"
    (let%map_open.Command filename = anon ("filename" %: string)
     and show_type = flag "type" no_arg ~doc:" show the file type"
     and show_base = flag "base" no_arg ~doc:" show the virtual base address"
     and show_arch = flag "arch" no_arg ~doc:" show the architecture"
     and show_segs = flag "segs" no_arg ~doc:" show the loaded segments"
     and show_start =
       flag "start" no_arg ~doc:" show the start (entry point) address"
     and show_entries =
       flag "entries" no_arg
         ~doc:" show known starting points of executable code"
     and show_relocs = flag "relocs" no_arg ~doc:" show relocations"
     and show_canonical_values =
       flag "canonical-values" no_arg ~doc:" show canonical values"
     and show_code_pointers =
       flag "code-pointers" no_arg
         ~doc:
           " show all word-sized values which point to an executable segment"
     and show_disasm =
       flag "disasm" no_arg
         ~doc:" disassemble all executable segments in linear-sweep fashion"
     and show_all = flag "all" no_arg ~doc:" show everything" in
     fun () ->
       printf "file: '%s'\n" filename;
       let img = Load_image.load_exn filename in
       let segs = Memory.segments img.mem in
       let Arch.(T arch) = img.arch in
       let word_sz = Arch.word_size_of arch in
       let format_addr addr = Util.format_addr addr word_sz in
       if show_type || show_all then (
         printf "type: ";
         ( match img.meta with
         | Image.Meta.PE pe -> (
             printf "PE";
             match pe.kind with
             | `PE32 -> printf ", 32-bit"
             | `PE64 -> printf ", 64-bit" )
         | Image.Meta.ELF elf -> (
             printf "ELF";
             match elf.eclass with
             | `ELF32 -> printf ", 32-bit"
             | `ELF64 -> printf ", 64-bit" )
         | Image.Meta.MachO mach ->
             printf "Mach-O";
             if mach.is64 then printf ", 64-bit" else printf ", 32-bit" );
         match img.endian with
         | `LE -> printf ", little-endian\n"
         | `BE -> printf ", big-endian\n" );
       if show_base || show_all then
         printf "virtual base: %s\n"
           Option.(
             value ~default:"none"
               ( Image.virtual_base img
               >>| fun addr -> Addr.to_int64 addr |> format_addr ));
       if show_arch || show_all then
         printf "architecture: %s\n" (Arch.to_string arch);
       if show_segs || show_all then (
         printf "\nsegments:\n\n";
         if List.is_empty segs then printf "none\n"
         else
           List.iter segs ~f:(fun seg ->
               printf "  ";
               let r, w, x =
                 Memory.Segment.
                   (is_readable seg, is_writable seg, is_executable seg)
               in
               if r then printf "R";
               if w then printf "W";
               if x then printf "X";
               (* this is dumb *)
               if
                 (r && w && not x)
                 || (r && (not w) && x)
                 || ((not r) && w && x)
               then printf " "
               else if
                 (r && (not w) && not x)
                 || ((not r) && w && not x)
                 || ((not r) && (not w) && x)
               then printf "  ";
               printf " %s -> %s (0x%X bytes)"
                 (Memory.Segment.address seg |> Addr.to_int64 |> format_addr)
                 ( Memory.Segment.end_address seg
                 |> Addr.to_int64 |> format_addr )
                 (Memory.Segment.length seg);
               printf "\n" );
         printf "\n" );
       if show_start || show_all then
         printf "start address: %s\n"
           Option.(
             value ~default:"(none)"
               (img.start >>| fun a -> Addr.to_int64 a |> format_addr));
       if show_entries || show_all then (
         let entries = Image.entries img in
         printf "\nentries:\n\n";
         if Map.is_empty entries then printf "none\n"
         else
           Map.iteri entries ~f:(fun ~key ~data:Arch.(T arch) ->
               printf "  %s (%s)\n"
                 (Addr.to_int64 key |> format_addr)
                 (Arch.to_string arch) );
         printf "\n" );
       if show_relocs || show_all then (
         printf "\nrelocs:\n\n";
         if Map.is_empty img.relocs then printf "none\n"
         else
           Map.iter img.relocs ~f:(fun rel ->
               printf "  addr = %s, size = %d"
                 (Addr.to_int64 rel.addr |> format_addr)
                 rel.size;
               Option.iter rel.value ~f:(fun v -> printf ", value = 0x%LX" v);
               Option.iter rel.name ~f:(fun n -> printf ", name = \"%s\"" n);
               printf "\n" );
         printf "\n" );
       if show_canonical_values || show_all then (
         printf "\ncanonical values:\n\n";
         if Map.is_empty img.canonical_values then printf "none\n"
         else
           Map.iteri img.canonical_values ~f:(fun ~key ~data ->
               printf " %s = 0x%s (%d)\n" key
                 (Bitvec.to_string data.value)
                 data.size );
         printf "\n" );
       if show_code_pointers || show_all then (
         let code_pointers = Image.code_pointers img in
         printf "\ncode pointers:\n\n";
         if Map.is_empty code_pointers then printf "none\n"
         else
           Map.iteri code_pointers ~f:(fun ~key ~data:Arch.(T arch) ->
               let format_addr addr =
                 Util.format_addr addr (Arch.word_size_of arch)
               in
               printf "  %s (%s)\n"
                 (Addr.to_int64 key |> format_addr)
                 (Arch.to_string arch) );
         printf "\n" );
       if show_disasm || show_all then (
         printf "\ndisassembly:\n\n";
         List.filter segs ~f:Memory.Segment.is_executable
         |> List.iter ~f:(dump_seg img) ) )
