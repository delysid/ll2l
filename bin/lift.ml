open Core
open Stdio
open Ll2l_arch
open Ll2l_ir
open Ll2l_std

let lift arch addr bytes =
  let Arch.(T arch) = Arch.of_string arch in
  let addr =
    match addr with
    | None -> Addr.int64 0x4000L
    | Some s -> Bitvec.of_string s
  in
  let bytes = Util.format_hex_string bytes in
  let decode_ctx = Arch.create_decode_ctx arch in
  let ctx = Il.Context.create () in
  let rec aux i addr =
    if i < Bytes.length bytes then (
      let instr =
        let bytes = Bytes.subo bytes ~pos:i in
        match Arch.decode arch decode_ctx bytes with
        | Ok instr -> instr
        | Error err ->
            failwith
            @@ Printf.sprintf "decoder: %s"
                 (Arch.string_of_decode_err arch err)
      in
      let len = Arch.instr_length arch instr in
      let end_addr = Arch.instr_end_addr arch instr ~addr in
      let delay, len, end_addr =
        let sz = Arch.delay_slot_size arch instr in
        let rec aux_d n res =
          if n >= sz then List.rev res
          else
            let pos = i + (len * (n + 1)) in
            let instr =
              let bytes = Bytes.subo bytes ~pos in
              match Arch.decode arch decode_ctx bytes with
              | Ok instr -> instr
              | Error err ->
                  failwith
                  @@ Printf.sprintf "decoder: %s"
                       (Arch.string_of_decode_err arch err)
            in
            aux_d (n + 1) (instr :: res)
        in
        let delay = aux_d 0 [] in
        let len' =
          List.fold delay ~init:0 ~f:(fun l instr ->
              l + Arch.instr_length arch instr )
        in
        (delay, len + len', Addr.(end_addr + int len'))
      in
      printf "%s\n" (Arch.string_of_instr_full arch instr ~addr);
      let addr' = ref addr in
      List.iter delay (fun instr ->
          (addr' := Addr.(!addr' + int (Arch.instr_length arch instr)));
          printf "%s\n" (Arch.string_of_instr_full arch instr ~addr:!addr') );
      let blocks =
        match Arch.lift arch instr addr ctx ~delay with
        | Ok blocks -> blocks
        | Error err ->
            failwith
            @@ Printf.sprintf "lifter: %s" (Arch.string_of_lift_err arch err)
      in
      printf "{\n%s\n}\n"
        (String.concat ~sep:"\n"
           ( List.map blocks ~f:(fun stmts ->
                 List.map stmts ~f:(fun stmt ->
                     let s = Il.string_of_stmt stmt in
                     match stmt with
                     | Il.Label _ -> s
                     | _ -> "   " ^ s ) )
           |> List.concat ) );
      aux (i + len) end_addr )
    else ()
  in
  aux 0 addr

let command =
  Command.basic
    ~summary:"Lift and pretty-print the semantics of an instruction"
    (let%map_open.Command arch =
       flag "arch" (required string) ~doc:" architecture name"
     and addr =
       flag "addr" (optional string)
         ~doc:" starting address (default: 0x4000)"
     and bytes = flag "bytes" (required string) ~doc:" instruction bytes" in
     fun () -> lift arch addr bytes )
