open Core
open Stdio
open Ll2l_arch
open Ll2l_ir
open Ll2l_image
open Ll2l_program
open Ll2l_std

let dump_report subs filename (p : Program.t) =
  let Arch.(T arch) = p.img.arch in
  let word_sz = Arch.word_size_of arch in
  printf "%s\n\nfunctions:\n\n" filename;
  let total_size =
    Map.fold subs ~init:0 ~f:(fun ~key:entry ~data:sub total_size ->
        let sub_size =
          Map.fold sub ~init:0 ~f:(fun ~key:_ ~data:blk sub_size ->
              Code.Block.fold blk ~init:sub_size ~f:(fun sub_size a ->
                  match Code.instr_of_addr p.code a with
                  | None -> sub_size
                  | Some Arch.(I (arch, instr)) ->
                      sub_size + Arch.instr_length arch instr ) )
        in
        printf "%s\t%d\n"
          (Util.format_addr (Addr.to_int64 entry) word_sz)
          sub_size;
        total_size + sub_size )
  in
  let exec_size =
    Memory.fold p.img.mem ~init:0 ~f:(fun exec_size _ seg ->
        if Memory.Segment.is_executable seg then
          exec_size + Memory.Segment.length seg
        else exec_size )
  in
  printf "\npercent of executable segments: %f%%\n"
    (Float.of_int total_size /. Float.of_int exec_size *. 100.0)

let dump ?(subs = false) ?(use_code_pointers = false) ?(spec = false)
    ?(noreturn_analysis = false) ?(ir = false) ?(native = false)
    ?(report = false) filename img =
  let Disasm.{p; starts} =
    Disasm.run img ~use_code_pointers ~spec ~noreturn_analysis
  in
  let basename = Filename.basename filename in
  let arch_str =
    let Arch.(T arch) = img.arch in
    Arch.to_string arch |> String.substr_replace_all ~pattern:"." ~with_:"_"
  in
  let subs =
    if subs || report then Some (Program.subroutines p ~starts) else None
  in
  if ir then (
    let filename = basename ^ "_" ^ arch_str ^ "_ir" in
    let chan = Out_channel.create filename in
    Util.dump_ir p ~subs ~chan:(Some chan) ~starts;
    Out_channel.close chan );
  if native then (
    let filename = basename ^ "_" ^ arch_str ^ "_native" in
    let chan = Out_channel.create filename in
    Util.dump_native p ~subs ~chan:(Some chan) ~starts;
    Out_channel.close chan );
  if report then
    let subs = Program.native_subroutines p ~subs in
    dump_report subs filename p

let command =
  Command.basic
    ~summary:"Disassemble a binary file and optionally output the results"
    (let%map_open.Command filename = anon ("filename" %: string)
     and ir = flag "ir" no_arg ~doc:" pretty-print IR to file"
     and native =
       flag "native" no_arg ~doc:" pretty-print native code to file"
     and report =
       flag "report" no_arg
         ~doc:" print a summary of the disassembly (implies -subs)"
     and subs = flag "subs" no_arg ~doc:" reconstruct subroutines"
     and use_code_pointers =
       flag "code-pointers" no_arg ~doc:" explore code pointers"
     and spec = flag "spec" no_arg ~doc:" speculate unexplored regions (WIP)"
     and noreturn_analysis =
       flag "noreturn" no_arg
         ~doc:" propagate noreturn information across the CFG"
     in
     fun () ->
       Load_image.load_exn filename
       |> dump filename ~subs ~use_code_pointers ~spec ~noreturn_analysis ~ir
            ~native ~report )
